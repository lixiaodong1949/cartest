﻿using Sx.Course.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Repositories
{
    /// <summary>
    /// 课程科目相关
    /// </summary>
    public interface ICourseSubjectRepository
    {
        /// <summary>
        /// 获取科目模板列表
        /// </summary>
        /// <returns></returns>
        List<CourseSubjectInfo> GetSubjectTempList();

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="cid">课程Id</param>
        /// <param name="list"></param>
        void AddCourseSubject(long cid,List<CourseSubjectInfo> list);

        /// <summary>
        /// 检测是课程否已经存在科目列表
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        bool ExistSubjectById(long cid);
    }
}
