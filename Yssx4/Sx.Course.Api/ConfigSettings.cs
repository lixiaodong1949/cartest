﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Sx.Course.Api
{
    public class ConfigSettings
    {
        public static int NameServerPort { get; set; }

        public static IPAddress NameServerAddress { get; set; }

        public static IPAddress CommandResultAddress { get; set; }

        public static int CommandResultPort { get; set; }
    }
}
