﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommon.Components;
using ECommon.Extensions;
using ECommon.Utilities;
using ENode.Commanding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sx.Course.Commands;
using Yssx.Framework;

namespace Sx.Course.Api.Controllers
{
    [Route("sxcourse/[controller]/[action]")]
    [ApiController]
    public class TestEnodeController : ControllerBase
    {
        private ICommandService _commandService;

        public TestEnodeController(ICommandService service)
        {
           // _commandService = ObjectContainer.Resolve<ICommandService>();
             _commandService = service;
        }

        [HttpPost]
        public async Task<ResponseContext<bool>> Create(CreateCourse info)
        {
            if (null == info) return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = "对象是空的", Data = false };

            string massge = "OK";
            try
            {
                info.Id = ObjectId.GenerateNewStringId();
                //info.AggregateRootId = Guid.NewGuid().ToString("N");
                var result = await ExecuteCommandAsync(info);
                if (result.Status != CommandStatus.Success)
                {
                    return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = "远程服务处理失败", Data = false };
                }
            }
            catch (Exception ex)
            {
                massge = ex.Message;
                return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = massge, Data = false };
            }


            return new ResponseContext<bool> { Code = (int)CommandStatus.Success, Msg = massge, Data = true };
        }

        [HttpGet]
        public string GetTest()
        {
            return "11111111111";
        }

        private Task<CommandResult> ExecuteCommandAsync(ICommand command, int millisecondsDelay = 5000)
        {
            return _commandService.ExecuteAsync(command, CommandReturnType.CommandExecuted).TimeoutAfter(millisecondsDelay);
        }
    }
}