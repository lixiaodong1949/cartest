﻿using Autofac.Extensions.DependencyInjection;
using ECommon.Autofac;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Socketing;
using ENode.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Yssx.Swagger;

namespace Sx.Course.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "云上实训",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(System.AppContext.BaseDirectory, "Yssx.*.xml").ToList();
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            InitIPAddress();
            InitializeECommon();
            InitializeENode(services);
            return new AutofacServiceProvider(((AutofacObjectContainer)ObjectContainer.Current).Container);
        }

        void InitIPAddress()
        {
            var address = Configuration["NameServerAddress"];
            var resultAddress = Configuration["CommandResultIpAddress"];
            ConfigSettings.NameServerAddress = string.IsNullOrEmpty(address) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(address);
            ConfigSettings.NameServerPort = int.Parse(Configuration["NameServerPort"]);
            ConfigSettings.CommandResultAddress = string.IsNullOrEmpty(resultAddress) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(resultAddress);
            ConfigSettings.CommandResultPort= int.Parse(Configuration["CommandResultPort"]);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors("AnyOrigin");
            #region cosul
            ServiceEntity serviceEntity = new ServiceEntity
            {
                IP = "localhost",   //服务运行地址
                Port = Convert.ToInt32(Configuration["Consul:ServicePort"]), //服务运行端口
                ServiceName = Configuration["Consul:Name"], //服务标识,Ocelot中会用到
                ConsulIP = Configuration["Consul:IP"], //Consul运行地址
                ConsulPort = Convert.ToInt32(Configuration["Consul:Port"])  //Consul运行端口（默认8500）
            };
            //app.RegisterConsul(lifetime, serviceEntity);
            #endregion
            //app.UseHttpsRedirection();
            app.UseMvc().UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
        }

        private ILogger _logger;
        private ECommon.Configurations.Configuration _ecommonConfiguration;
        private ENodeConfiguration _enodeConfiguration;

        private void InitializeECommon()
        {
            _ecommonConfiguration = ECommon.Configurations.Configuration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog()
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler();
        }
        private void InitializeENode(IServiceCollection services)
        {

            var assemblies = new[]
            {
                Assembly.Load("Sx.Course.Commands"),
                Assembly.Load("Sx.Course.QueryServices.FreeSqlDb"),
                Assembly.GetExecutingAssembly()
            };

            _enodeConfiguration = _ecommonConfiguration
                .CreateENode()
                .RegisterENodeComponents()
                .RegisterBusinessComponents(assemblies)
                .UseEQueue()
                .RegisterMvcServices(services)
                .BuildContainer()
                .InitializeBusinessAssemblies(assemblies)
                .StartEQueue();
        }
    }
}
