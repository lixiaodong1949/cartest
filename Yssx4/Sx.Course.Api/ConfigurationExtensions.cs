﻿using Autofac.Extensions.DependencyInjection;
using ECommon.Autofac;
using ECommon.Components;
using ENode.Configurations;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sx.Course.Api
{
    public static class ConfigurationExtensions
    {
        public static ENodeConfiguration RegisterMvcServices(this ENodeConfiguration configuration, IServiceCollection services)
        {
            var containerBuilder = (ObjectContainer.Current as AutofacObjectContainer).ContainerBuilder;
            services.AddMvc();
            services.AddAntiforgery();
            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            //        .AddCookie(options =>
            //        {
            //            options.Cookie.HttpOnly = true;
            //            options.Cookie.Path = "/";
            //            options.LoginPath = "/Account/Login";
            //            options.LogoutPath = "/Account/Logout";
            //            options.SlidingExpiration = true;
            //        });
            containerBuilder.Populate(services);
            return configuration;
        }
    }
}
