﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.Repository.Base
{
    public interface IRepository<T, TKey> where T : BaseEntity<TKey>
    {
        T Add(T t);

        void Add(IEnumerable<T> t);

        T GetByKey(TKey key);

        List<T> Get(Expression<Func<T, bool>> predExpr);

        List<T> GetAll();

        decimal Sum(Expression<Func<T, decimal>> sumExpr);

        decimal Sum(Expression<Func<T, bool>> predExpr, Expression<Func<T, decimal>> sumExpr);

        long Count(Expression<Func<T, bool>> predExpr);

        bool Any(Expression<Func<T, bool>> predExpr);

        bool Any();

        int Delete(TKey key);

        int Delete(Expression<Func<T, bool>> predExpr);

        int Update(T t);
        int Update(T t, Expression<Func<T, object>> columns);
        int UpdateIgnoreColumns(T t, Expression<Func<T, object>> columns);
        int UpdateDefineColumns(T t, IEnumerable<Expression<Func<T, object>>> columns);


    }
}
