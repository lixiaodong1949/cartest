﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Base;

namespace Yssx.Repository
{
    public class Repository<TEntity, TKey> : Base.IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        protected readonly FreeSqlRepository<TEntity, TKey> _rep;
        public Repository()
        {
            _rep = new FreeSqlRepository<TEntity, TKey>();
        }
        public TEntity GetByKey(TKey key)
        {
            return _rep.Get(key);
        }
        public List<TEntity> Get(Expression<Func<TEntity, bool>> predExpr)
        {
            return _rep.Select.Where(predExpr).ToList();
        }
        public ISelect<TEntity> Where(Expression<Func<TEntity, bool>> predExpr)
        {
            return _rep.Select.Where(predExpr);
        }
        public List<TEntity> GetAll()
        {
            return _rep.Select.ToList();
        }
        public TEntity First(Expression<Func<TEntity, bool>> predExpr)
        {
            return _rep.Select.Where(predExpr).First();
        }



        //public PageResponse<TEntity> GetByPage(int pageIndex, int pageSize, string whereSql, params object[] sqlParas)
        //{
        //    PageResponse<TEntity> page;
        //    whereSql = whereSql?.FormatMySql(sqlParas);
        //    var query = _rep.Select.Where(whereSql);
        //    var totalCount = query.Count();
        //    var entites = query.Page(pageIndex, pageSize).ToList();

        //    page = new PageResponse<TEntity>(pageIndex, pageSize, totalCount, entites);
        //    return page;
        //}
        public PageResponse<TEntity> GetByPage(int pageIndex, int pageSize, string whereSql, object sqlParaObject)
        {
            PageResponse<TEntity> page;

            var query = _rep.Select.Where(whereSql, sqlParaObject);
            var totalCount = query.Count();
            var entites = query.Page(pageIndex, pageSize).ToList();

            page = new PageResponse<TEntity>(pageIndex, pageSize, totalCount, entites);
            return page;
        }
        public TEntity Add(TEntity t)
        {
            return _rep.Insert(t);
        }

        public void Add(IEnumerable<TEntity> t)
        {
            _rep.Insert(t);
        }
        public ISelect<TEntity> GetTableSelect()
        {
            return _rep.Select;
        }
        public bool Any(Expression<Func<TEntity, bool>> predExpr)
        {

            //var sql = _rep.Select.Where(predExpr).Any();
            return _rep.Select.Any(predExpr);
        }

        public bool Any()
        {
            return _rep.Select.Any();
        }

        public long Count(Expression<Func<TEntity, bool>> predExpr)
        {
            return _rep.Select.Where(predExpr).Count();
        }

        public int Delete(TKey key)
        {
            return _rep.Delete(key);
        }

        public int Delete(Expression<Func<TEntity, bool>> predExpr)
        {
            return _rep.Delete(predExpr);
        }


        public decimal Sum(Expression<Func<TEntity, decimal>> sumExpr)
        {
            return _rep.Select.Sum<decimal>(sumExpr);
        }

        public decimal Sum(Expression<Func<TEntity, bool>> predExpr, Expression<Func<TEntity, decimal>> sumExpr)
        {
            return _rep.Select.Where(predExpr).Sum<decimal>(sumExpr);
        }

        public int Update(TEntity t)
        {
            return _rep.Update(t);
        }
        public int Update(IEnumerable<TEntity>  t)
        {
            return _rep.Update(t);
        }
        public int SetSource(TEntity t)
        {
            return _rep.UpdateDiy.SetSource(t).ExecuteAffrows();
            
        }
        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="t"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public int Update(TEntity t, Expression<Func<TEntity, object>> columns)
        {
            return _rep.Update(t, columns);
        }
        public int UpdateIgnoreColumns(TEntity t, Expression<Func<TEntity, object>> columns)
        {
            return _rep.UpdateIgnoreColumns(t,columns);
        }

        public int UpdateDefineColumns(TEntity t, IEnumerable<Expression<Func<TEntity, object>>> columns)
        {
            throw new NotImplementedException();
            //return _rep.UpdateDefineColumns(t, columns);
        }

        
    }
}
