using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 案例财务数据 Dto
    /// </summary>
    public class CaseFinancialDataDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        [Required()]
        public long CaseId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Required()]
        public string FilePath { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}