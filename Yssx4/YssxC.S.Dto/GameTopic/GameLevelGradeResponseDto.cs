﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 关卡作答提交返回值
    /// </summary>
    public class GameLevelGradeResponseDto
    {
        /// <summary>
        /// 本关卡本次作答 得分等信息
        /// </summary>
        public GameGradeInfoDto CurrentGameLevelGrade { get; set; }

        /// <summary>
        /// 用户当前关卡历史最佳记录
        /// </summary>
        public GameGradeInfoDto UserGameLevelGradeNo1 { get; set; }

        /// <summary>
        /// 当前关卡历史最佳记录
        /// </summary>
        public CurrentGameLevelGradeNo1 CurrentGameLevelGradeNo1 { get; set; }

        /// <summary>
        /// 用户技能
        /// </summary>
        public List<GameGradeUserSkillDto> UserSkills { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GameGradeInfoDto
    {
        /// <summary>
        /// 关卡最大记录id
        /// </summary>
        public long GameGradeId { get; set; }

        /// <summary>
        /// 考试得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 考试用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 本次获得积分
        /// </summary>
        public decimal Integral { get; set; }

        ///// <summary>
        ///// 试卷总分
        ///// </summary>
        //public decimal ExamScore { get; set; }

        ///// <summary>
        ///// 答对题数
        ///// </summary>
        //public int CorrectCount { get; set; }

        ///// <summary>
        ///// 答错题数
        ///// </summary>
        //public int ErrorCount { get; set; }

        ///// <summary>
        ///// 部分对数量
        ///// </summary>
        //public int PartRightCount { get; set; }

        ///// <summary>
        ///// 未答题数
        ///// </summary>
        //public int BlankCount { get; set; }

        ///// <summary>
        ///// 正确率
        ///// </summary>
        //public string CorrectRate
        //{
        //    get
        //    {
        //        var total = CorrectCount + ErrorCount;
        //        if (total == 0)
        //            return "--";
        //        else
        //        {
        //            return CorrectCount * 100 / total + "%";
        //        }
        //    }

        //}
        ///// <summary>
        ///// 是否及格
        ///// </summary>
        //public bool IsPass { get; set; }

        ///// <summary>
        ///// 考试状态
        ///// </summary>
        //public GroupStatus GroupStatus { get; set; }

        ///// <summary>
        ///// 考试状态
        ///// </summary>
        //public StudentExamStatus StudentExamStatus { get; set; }
        ///// <summary>
        ///// 等级
        ///// </summary>
        //public GradeLevel Level
        //{
        //    get
        //    {
        //        var rQualifiedScore = ExamScore * (decimal)0.6;
        //        var rGoodScore = ExamScore * (decimal)0.7;
        //        var rExcellentScore = ExamScore * (decimal)0.8;
        //        if (GradeScore >= rExcellentScore)
        //            return GradeLevel.Excellent;
        //        else if (GradeScore >= rGoodScore && GradeScore < rExcellentScore)
        //            return GradeLevel.Good;
        //        else if (GradeScore >= rQualifiedScore && GradeScore < rGoodScore)
        //            return GradeLevel.Qualified;
        //        else
        //            return GradeLevel.Unqualified;
        //    }
        //}
        ///// <summary>
        ///// 老师评语
        ///// </summary>
        //public string TeacherComments { get; set; }
    }

    /// <summary>
    /// 当前关卡历史最佳记录
    /// </summary>
    public class CurrentGameLevelGradeNo1
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 关卡最大记录id
        /// </summary>
        public long GameGradeId { get; set; }

        /// <summary>
        /// 考试得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 考试用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 本次获得积分
        /// </summary>
        public decimal Integral { get; set; }

        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary> 
        public string Photo { get; set; }
    }

    public class GameGradeUserSkillDto
    {
        ///// <summary>
        ///// 技能Id
        ///// </summary>
        //[JsonIgnore]
        //public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string SkillName { get; set; }

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }

        /// <summary>
        /// 总技能值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public int Max { get; set; }

        /// <summary>
        /// 当前增加的技能值
        /// </summary>
        public decimal AddScore { get; set; }

    }
}
