using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡题目分页查询 请求Dto
    /// </summary>
    public class GameTopicPageRequestDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string TopicName { get; set; }
    }
}