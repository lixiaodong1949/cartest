﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户日历下题目查询实体
    /// </summary>
    public class UserCalendarTopicQuery: CalendarTopicRequest
    {
        /// <summary>
        /// 月度任务Id
        /// </summary>
        public long MonthTaskId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 票据状态 0:未审核 1:已审核 2:已退回
        /// </summary>
        public int BillStatus { get; set; }

        /*
        /// <summary>
        /// 业务名称(冗余字段不可用)
        /// </summary>
        public string BusinessName { get; set; }
        */
    }
}
