﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 申报表主表
    /// </summary>
    public class DeclarationParentDto
    {
        /// <summary>
        /// 申报表主表id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 主表名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 子表数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 所属开始日期
        /// </summary>
        [JsonConverter(typeof(DateConverter))]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 所属终止日期
        /// </summary>
        [JsonConverter(typeof(DateConverter))]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// 申报日期
        /// </summary>
        [JsonConverter(typeof(DateConverter))]
        public DateTime DeclarationDate { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
                
        /// <summary>
        /// 申报状态 0:未填写 1:待申报 2:已申报
        /// </summary>
        public int DeclareStatus { get; set; }

        /// <summary>
        /// 子表
        /// </summary>
        public List<DeclarationChildDto> Children { get; set; }
    }

    public class CreateDeclarationParentInput
    {
        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 主表模板id
        /// </summary>
        public long ParentTemplateId { get; set; }

        /// <summary>
        /// 主表模板名称
        /// </summary>
        public string Name { get; set; }

        public List<CreateDeclarationChildInput> Children { get; set; }
    }

    public class CreateDeclarationChildInput
    {
        /// <summary>
        /// 子表模板id
        /// </summary>
        public long ChildTemplateId { get; set; }

        /// <summary>
        /// 子表模板名称
        /// </summary>
        public string Name { get; set; }
    }

    public class GetDeclarationParentInput
    {
        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }
    }

    public class UpdateDeclarationParentInput
    {
        /// <summary>
        /// 主表id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 所属开始日期
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 所属终止日期
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// 申报日期
        /// </summary>
        public DateTime DeclarationDate { get; set; }
    }

    public class DateConverter : IsoDateTimeConverter
    {
        public DateConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
