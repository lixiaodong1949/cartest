﻿using System;
using System.Collections.Generic;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    public class TaskDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 任务日期
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// 任务目标
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 任务详情
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 审票模式 0:每日 1:每周 2:每月
        /// </summary>
        public WorkPattern WorkPattern { get; set; }

        /// <summary>
        /// 是否需要新建账套
        /// </summary>
        public bool RequireAccountSet { get; set; }

        /// <summary>
        /// 任务点数
        /// </summary>
        public int TaskItemCount { get; set; }

        /// <summary>
        /// 题目数
        /// </summary>
        public int TopicCount { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Integral { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 建议时长
        /// </summary>
        public string SuggestDuration { get; set; }

        /// <summary>
        /// 任务项
        /// </summary>
        public List<TaskItemDto> TaskItems { get; set; }
    }

    public class GetTaskInput : PageRequest
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
    }
}
