﻿using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 查询模板列表入参类
    /// </summary>
    public class GetTemplatesRequest : PageRequest
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 模板类型ID
        /// </summary>
        public long TypeId { get; set; }
    }
}
