﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 查询题目列表Dto
    /// </summary>
   public class TopicQueryPageDto : PageRequest
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 业务开始日期
        /// </summary>
        public DateTime? BusinessStartDate { get; set; }
        /// <summary>
        /// 业务结束日期
        /// </summary>
        public DateTime? BusinessEndDate { get; set; }
        /// <summary>
        /// 题目分组Id
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType? QuestionType { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
    }
}
