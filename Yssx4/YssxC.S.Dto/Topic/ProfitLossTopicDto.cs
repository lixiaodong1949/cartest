﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 结转损益题
    /// </summary>
    public class ProfitLossTopicDto : BaseQuestion
    {
        /// <summary>
        /// 题目提示（分开结转提示）
        /// </summary>
        public string TopicHint { get; set; }
        /// <summary>
        /// 查看答案
        /// </summary>
        public bool IsShowAnswer { get; set; }
        /// <summary>
        /// 查看解析
        /// </summary>
        public bool IsShowHint { get; set; }

        #region 分录题专用
        /// <summary>
        /// 凭证字
        /// </summary>
        public CertificateWord CertificateWord { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
        /// <summary>
        /// 单据数据记录
        /// </summary>
        public List<CertificateDataRecord> DataRecords { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        public Status IsDisableCreator { get; set; }

        /// <summary>
        /// 答案（凭证分开结转每张凭证的答案）
        /// </summary>
        public List<CertificateAnswerDto> Answers { get; set; }
        /// <summary>
        /// 科目辅助核算信息
        /// </summary>
        public List<TopicAssistAccounting> AssistAccountings { get; set; }

        /// <summary>
        /// 摘要是否计分
        /// </summary>
        public bool IsSummaryCalculation { get; set; }

        /// <summary>
        /// 合并后的摘要信息
        /// </summary>
        public string SummaryInfos { get; set; }

        #endregion
    }
    /// <summary>
    /// 分录答案
    /// </summary>
    public class CertificateAnswerDto
    {
        /// <summary>
        /// 答案 内容（json等）
        /// </summary>
        public string AnswerValue { get; set; }
        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfos { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
