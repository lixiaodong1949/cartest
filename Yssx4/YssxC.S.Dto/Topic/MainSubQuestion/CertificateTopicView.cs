﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class CertificateTopicView
    {
        /// <summary>
        /// 题库Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }

        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }

        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        public Status IsDisableAM { get; set; }

        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        public Status IsDisableCashier { get; set; }

        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        public Status IsDisableAuditor { get; set; }

        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        public Status IsDisableCreator { get; set; }

        /// <summary>
        /// 凭证字(综合分录专用)
        /// </summary>
        public CertificateWord CertificateWord { get; set; }
        /// <summary>
        /// 标准答案的摘要信息
        /// </summary>
        public string SummaryInfos { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }

    }
}
