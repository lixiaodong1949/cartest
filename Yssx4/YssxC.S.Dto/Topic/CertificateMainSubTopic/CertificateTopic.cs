﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 记账凭证（分录题）信息
    /// </summary>
    public class CertificateTopic
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        public string Hint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }
        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }
        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerValue { get; set; }
        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfos { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }

        #region 分录题专用
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 凭证字
        /// </summary>
        public CertificateWord CertificateWord { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 会计主管分数
        /// </summary>
        public decimal DisableAMScore { get; set; }
        /// <summary>
        /// 出纳分数
        /// </summary>
        public decimal DisableCashierScore { get; set; }
        /// <summary>
        /// 单据数据记录
        /// </summary>
        public List<CertificateDataRecord> DataRecords { get; set; }
        /// <summary>
        /// 摘要是否计分
        /// </summary>
        public bool IsSummaryCalculation { get; set; }
        #endregion

        /// <summary>
        /// 科目辅助核算信息
        /// </summary>
        public List<TopicAssistAccounting> AssistAccountings { get; set; }

        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; } = 0;

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }
    }
    /// <summary>
    /// 题目的科目辅助核算信息
    /// </summary>
    public class TopicAssistAccounting
    {
        //topicid caseid typeid id  subjectid
        /// <summary>
        /// 辅助核算类型
        /// </summary>
        public AssistAccountingType AssistAccountingType { get; set; }
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }
    }
}