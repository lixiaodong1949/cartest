﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 题目附件
    /// </summary>
    public class TopicFile
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 链接地址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 封面底面：1封面 2底面
        /// </summary>
        public TopicFileCoverBack? CoverBack { get; set; }
        /// <summary>
        /// 水印
        /// </summary>
        public bool IsWatermark { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        public TopicFileType Type { get; set; }
    }
}
