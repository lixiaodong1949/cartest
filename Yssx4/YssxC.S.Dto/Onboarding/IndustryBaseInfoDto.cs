using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 根据行业id获取行业基本信息 Dto
    /// </summary>
    public class IndustryBaseInfoDto
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 地图
        /// </summary>
        public string Map { get; set; }
    }
}