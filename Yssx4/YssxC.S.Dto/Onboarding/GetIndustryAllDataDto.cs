﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 行业 - 下拉框数据
    /// </summary>
    public class GetIndustryAllDataDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 地图
        /// </summary>
        public string Map { get; set; }

    }

    /// <summary>
    /// 行业 - 地图数据
    /// </summary>
    public class GetIndustryInMapDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// X坐标
        /// </summary>
        public int Xcoordinate { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public int Ycoordinate { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 地图logo
        /// </summary>
        public string MapLogo3D { get; set; }

    }
}
