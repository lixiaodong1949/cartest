using Yssx.Framework.AutoMapper;
using YssxC.S.Dto.ExamPaper;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Exam;

namespace YssxC.S.Dto
{
    public class DtoAutoMapperProfile : AutoMapperProfile
    {
        public DtoAutoMapperProfile()
        {
            //this.BidirectMapIgnoreAllNonExisting<YssxCase, CaseRequest>().
            this.BidirectMapIgnoreAllNonExisting<SxCase, CaseDto>()
                .BidirectMapIgnoreAllNonExisting<SxTag, TagDto>()
                .BidirectMapIgnoreAllNonExisting<SxAbstract, AbstractDto>()
                .BidirectMapIgnoreAllNonExisting<SxBankAccount, BankAccountDto>()
                .BidirectMapIgnoreAllNonExisting<SxTask, TaskDto>()
                .BidirectMapIgnoreAllNonExisting<SxTaskItem, TaskItemDto>()
                .BidirectMapIgnoreAllNonExisting<SxIndustry, IndustryDto>()
                .BidirectMapIgnoreAllNonExisting<SxCase, GetCaseInfoOnboardingDto>()
                .BidirectMapIgnoreAllNonExisting<SxDeclarationTemplateParent, DeclarationTemplateParentDto>()
                .BidirectMapIgnoreAllNonExisting<SxDeclarationTemplateChild, DeclarationTemplateChildDto>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, SimpleQuestionRequest>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, ComplexQuestionRequest>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, FinancialStatementsRequest>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, MainSubQuestionRequest>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, SubQuestion>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, CertificateMainTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, TeticketTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, CertificateTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, ProfitLossTopicDto>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, TopicInfoDto>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, PayTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, ReceiptTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopic, ExamQuestionInfo>()
                .BidirectMapIgnoreAllNonExisting<SxTopicFile, TopicFile>()
                .BidirectMapIgnoreAllNonExisting<SxAnswerOption, ChoiceOption>()
                .BidirectMapIgnoreAllNonExisting<SxCertificateAnswer, CertificateAnswerDto>()
                .BidirectMapIgnoreAllNonExisting<SxTopicPayAccountInfo, PayTopic>()
                .BidirectMapIgnoreAllNonExisting<SxTopicPayAccountInfo, ReceiptTopic>()
                .BidirectMapIgnoreAllNonExisting<SxAssistAccounting, AssistAccountingDto>()
                .BidirectMapIgnoreAllNonExisting<SxCertificateTopic, CertificateTopic>()
                .BidirectMapIgnoreAllNonExisting<SxCertificateTopic, ProfitLossTopicDto>()
                .BidirectMapIgnoreAllNonExisting<SxCertificateTopic, SubQuestion>()
                .BidirectMapIgnoreAllNonExisting<SxCertificateDataRecord, CertificateDataRecord>()
                .BidirectMapIgnoreAllNonExisting<SxDeclarationParent, DeclarationParentDto>()
                .BidirectMapIgnoreAllNonExisting<SxRegulation, RegulationDto>()
                .BidirectMapIgnoreAllNonExisting<SxCaseRegulation, CaseRegulationDto>()
                .BidirectMapIgnoreAllNonExisting<SxCaseRegulation, RegulationDto>()
                .BidirectMapIgnoreAllNonExisting<SxDeclarationChild, DeclarationChildDto>()
                .BidirectMapIgnoreAllNonExisting<SxExamCertificateDataRecord, AccountEntryDTO>()
                .BidirectMapIgnoreAllNonExisting<SxRefusalRecord, RefusalRecordDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameLevel, GameLevelDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameLevel, GameLevelBaseInfoDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameTopic, GameTopicDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameSceneCatgory, GameSceneCatgoryDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameScene, GameSceneDto>()
                .BidirectMapIgnoreAllNonExisting<SxSkill, SkillDto>()
                .BidirectMapIgnoreAllNonExisting<SxGameResourceConfig, GameResourceConfigDto>()
                .BidirectMapIgnoreAllNonExisting<SxCaseFinancialData, CaseFinancialDataDto>();
        }
    }
}
