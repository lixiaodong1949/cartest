﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 问题信息
    /// </summary>
    public class GradeDtailDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public AnswerResultStatus Status { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
