﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 实习分录dto
    /// </summary>
    public class AccountingEntryTopicDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 业务名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }
        /// <summary>
        /// 所有正确附件id集合
        /// </summary>
        public string TopicFileIds { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime? BusinessDate { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public List<QuestionFile> QuestionFiles { get; set; }
        /// <summary>
        /// 分录题状态
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }
        /// <summary>
        /// 是否已作答
        /// </summary>

        public bool IsAnswered { get; set; } = false;

    }

    /// <summary>
    /// 实习分录dto
    /// </summary>
    public class AccountingEntryTopicAPPDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 业务名称
        /// </summary>
        public string Title { get; set; }
        ///// <summary>
        ///// 师傅提示
        ///// </summary>
        //public string TeacherHint { get; set; }
        /// <summary>
        /// 所有正确附件id集合 （内部计算使用）
        /// </summary>
        [JsonIgnore]
        public string TopicFileIds { get; set; }
        ///// <summary>
        ///// 业务日期
        ///// </summary>
        //public DateTime? BusinessDate { get; set; }
        /// <summary>
        /// 封面
        /// </summary>
        public string CoverUrl { get; set; }
        /// <summary>
        /// 分录题状态
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }
        /// <summary>
        /// 是否已作答
        /// </summary>

        public bool IsAnswered { get; set; } = false;

    }

}
