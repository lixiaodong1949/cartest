﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;
using Yssx.Framework.AnswerCompare.EntitysV2;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 提交答案结果
    /// </summary>
    public class QuestionResult
    {
        /// <summary>
        /// 题目编号
        /// </summary>
        public int No { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 答案结果 2-对，3-部分对，4-错
        /// </summary>
        public AnswerResultStatus AnswerResult { get; set; }

        /// <summary>
        /// 答题得分
        /// </summary>
        public decimal AnswerScore { get; set; }

        /// <summary>
        /// 返回的json数据
        /// </summary>
        public string AnswerResultJson { get; set; }

        /// <summary>
        /// 多题型考试结果
        /// </summary>
        public List<QuestionResult> MultiQuestionResult { get; set; }

        /// <summary>
        /// 学生当前测试的作答状态，通过该状态是否为end来判断是否显示成绩统计页面
        /// </summary>
        public StudentExamStatus Status { get; set; }

        /// <summary>
        /// 学生当前测试作答状态为end时，用于显示成绩统计页面
        /// </summary>
        public GradeInfo GradeInfo { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        public List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>> AccountEntryStatusDic { get; set; }

        /// <summary>
        /// 不必json序列化
        /// </summary>
        [JsonIgnore]
        public AnswerCountInfo AnswerCountInfo { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
    }
}
