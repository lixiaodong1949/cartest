﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 问题信息
    /// </summary>
    public class QuestionInfo
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 岗位名称 
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 是否属于当前答题人的题目（团队赛题型专用）
        /// </summary>
        public bool IsOwner { get; set; } = true;

        /// <summary>
        /// 是否标记
        /// </summary>
        [Obsolete]
        [JsonIgnore]
        public bool IsMark { get; set; }

        /// <summary>
        /// 题目类型:0单选题,1多选题，2判断题，3表格题，4分录题，5填空题，6图表题，7综合题，8岗位核心技能，9表格填空题
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 作答状态(0未作答,1已作答,2答对，3答错 4部分正确)
        /// </summary>

        public AnswerDTOStatus QuestionAnswerStatus { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>> AccountEntryStatusDic { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 答题得分
        /// </summary>
        public decimal AnswerScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 纳税申报表Id
        /// </summary>
        public long DeclarationId { get; set; }
        /// <summary>
        /// 纳税申报表父Id
        /// </summary>
        public long ParentDeclarationId { get; set; }
        
    }
}
