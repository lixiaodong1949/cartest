﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回收款题列表实体
    /// </summary>
    public class ReceiveTopicInfoViewModel
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 收款题目Id
        /// </summary>
        public long ReceiveQuestionId { get; set; }

        /// <summary>
        /// 收款开户行
        /// </summary>
        public string ReceivingOpenBank { get; set; }

        /// <summary>
        /// 收款支行
        /// </summary>
        public string ReceivingBranchBank { get; set; }

        /// <summary>
        /// 收款账号
        /// </summary>
        public string ReceivingAccount { get; set; }
        
        /// <summary>
        /// 收款账户余额
        /// </summary>
        public decimal ReceivingBalance { get; set; }

        /// <summary>
        /// 付款单位名称
        /// </summary>
        public string PaymentUnitName { get; set; }

        /// <summary>
        /// 付款银行
        /// </summary>
        public string PaymentOpenBank { get; set; }

        /// <summary>
        /// 付款支行
        /// </summary>
        public string PaymentSubBank { get; set; }

        /// <summary>
        /// 付款账号
        /// </summary>
        public string PaymentAccount { get; set; }

        /// <summary>
        /// 付款金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 回执单
        /// </summary>
        public string ReceiptUrl { get; set; }

        /// <summary>
        /// 是否已收款 false:未收 true:已收
        /// </summary>
        public bool IsReceived { get; set; }
    }
}
