﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户票据题列表实体
    /// </summary>
    public class UserBillTopicViewModel
    {
        /// <summary>
        /// 父题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 票据题目Id
        /// </summary>
        public long BillQuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string TopicName { get; set; }

        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }

        /// <summary>
        /// 题目是否被标记 false:未标记 true:已标记
        /// </summary>
        public bool IsMark { get; set; }

        /// <summary>
        /// 票据题目来源(审核票据使用) 0:未审核票据 1:已作答票据(推送/已作答 只能审核通过)
        /// </summary>
        public int BillQuestionSource { get; set; }

        /// <summary>
        /// 是否已做凭证(已审票据使用) false:未做 true:已做
        /// </summary>
        public bool IsDoCertificate { get; set; }

        /// <summary>
        /// 答案内容(已退票据使用)
        /// </summary>
        public string AnswerValue { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 票据详情列表
        /// </summary>
        public List<BillFileViewModel> BillFileList { get; set; } = new List<BillFileViewModel>();

    }

    /// <summary>
    /// 票据详情实体
    /// </summary>
    public class BillFileViewModel
    {
        /// <summary>
        /// 图片路径
        /// </summary>
        public string Url { get; set; }

        /*
        /// <summary>
        /// 是否水印
        /// </summary>
        public bool IsWatermark { get; set; }
        */

        /// <summary>
        /// 封面底面：1:封面 2:底面
        /// </summary>        
        public Nullable<int> CoverBack { get; set; }

    }

}
