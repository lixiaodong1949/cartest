﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto.Subject
{
    /// <summary>
    /// 凭证余额对象
    /// </summary>
    public class CertificateBalanceDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 辅助Id
        /// </summary>
        public long AssistacId { get; set; }

        /// <summary>
        /// 科目代码
        /// </summary>

        public long SubjectCode { get; set; }

        /// <summary>
        /// 父级ID 最多四级
        /// </summary>

        public long ParentId { get; set; }

        /// <summary>
        /// 企业ID
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>

        public string SubjectName { get; set; }

        /// <summary>
        /// 科目类型(1:资产,2:负债)
        /// </summary>
        public int SubjectType { get; set; }

        /// <summary>
        /// 期初借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        public string BorrowAmountStr { get; set; }

        /// <summary>
        /// 期初贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }

        public string CreditorAmountStr { get; set; }

        /// <summary>
        /// 本期借方金额
        /// </summary>
        public double CurrentBorrowAmount { get; set; }

        public string CurrentBorrowAmountStr { get; set; }

        /// <summary>
        /// 本期贷方金额
        /// </summary>
        public double CurrentCreditorAmount { get; set; }

        public string CurrentCreditorAmountStr { get; set; }

        /// <summary>
        /// 期末借方金额
        /// </summary>
        public double FinalBorrowAmount { get; set; }

        public string FinalBorrowAmountStr { get; set; }

        /// <summary>
        /// 期末贷方金额
        /// </summary>
        public double FinalCreditorAmount { get; set; }

        public string FinalCreditorAmountStr { get; set; }

        /// <summary>
        /// 是否辅助核算
        /// </summary>
        public int IsHaveCheck { get; set; }
    }
}
