﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto.Subject
{
    public class TrialBalancingDto
    {
        /// <summary>
        /// 贷方总额
        /// </summary>
        public decimal BorrowAmountSummer { get; set; }

        /// <summary>
        /// 借方总额
        /// </summary>
        public decimal CreditorAmountSummer { get; set; }
    }
}
