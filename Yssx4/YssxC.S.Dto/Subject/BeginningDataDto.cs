﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto.Subject
{
    public class BeginningDataDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 是否正确(平衡)
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// 误差数据
        /// </summary>
        public decimal ErrorData { get; set; }

        public long AssistacId { get; set; }
    }

    public class SubjectValueDto
    {
        public List<BeginningDataDto> BeginningDatas { get; set; }
    }
}
