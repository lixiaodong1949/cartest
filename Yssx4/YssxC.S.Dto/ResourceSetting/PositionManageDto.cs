﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 保存修改获取岗位数据入参类（查询）输出类
    /// </summary>
    public class PositionManageDto
    {
        /// <summary>
        /// 岗位ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缩略图标文字
        /// </summary>
        public string ThumbnailText { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 岗位图标地址
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 实习模式
        /// </summary>
        public PositionType PositionType { get; set; } = PositionType.composite;
    }

    /// <summary>
    /// 获取岗位数据入参类
    /// </summary>
    public class PositionManageRequest : PageRequest
    {
        /// <summary>
        /// 搜索名称
        /// </summary>
        public string Name { get; set; }
    }
}
