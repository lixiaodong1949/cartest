﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程备课相关Api [Obsolete]
    /// </summary>
    public class LessonPlanningControlle : BaseController
    {
        ILessonPlanningService _service;

        public LessonPlanningControlle(ILessonPlanningService service)
        {
            _service = service;
        }

        #region 新增/编辑课程备课
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditLessonPlanning(LessonPlanningRequestModel dto)
        {
            return await _service.AddOrEditLessonPlanning(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课列表
        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxLessonPlanningViewModel>>> GetLessonPlanningList([FromQuery]YssxLessonPlanningQuery query)
        {
            return await _service.GetLessonPlanningList(query);
        }
        #endregion

        #region 删除课程备课
        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteLessonPlanning(List<long> ids)
        {
            return await _service.DeleteLessonPlanning(ids, this.CurrentUser);
        }
        #endregion

        #region 新增课程备课素材、教案
        /// <summary>
        /// 新增课程备课素材、教案
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddLessonPlanningFiles(LessonPlanningFilesRequestModel dto)
        {
            return await _service.AddLessonPlanningFiles(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课素材、教案列表
        /// <summary>
        /// 获取课程备课素材、教案列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxLessonPlanningFilesViewModel>>> GetLessonPlanningFilesList([FromQuery]YssxLessonPlanningFilesQuery query)
        {
            return await _service.GetLessonPlanningFilesList(query);
        }
        #endregion

        #region 删除课程备课素材、教案
        /// <summary>
        /// 删除课程备课素材、教案
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RemoveLessonPlanningFiles(long id)
        {
            return await _service.RemoveLessonPlanningFiles(id, this.CurrentUser);
        }
        #endregion

        #region 新增课程备课习题
        /// <summary>
        /// 新增课程备课习题
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddLessonPlanningTopic(LessonPlanningTopicRequestModel dto)
        {
            return await _service.AddLessonPlanningTopic(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课习题列表
        /// <summary>
        /// 获取课程备课习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxLessonPlanningTopicViewModel>>> GetLessonPlanningTopicList([FromQuery]YssxLessonPlanningInfoQuery query)
        {
            return await _service.GetLessonPlanningTopicList(query);
        }
        #endregion

        #region 删除课程备课习题
        /// <summary>
        /// 删除课程备课习题
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RemoveLessonPlanningTopic(long id)
        {
            return await _service.RemoveLessonPlanningTopic(id, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课素材、习题列表
        /// <summary>
        /// 获取课程备课素材、习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxLessonPlanningCdrAndTopicViewModel>>> GetLessonPlanningCdrAndTopicList([FromQuery]YssxLessonPlanningInfoQuery query)
        {
            return await _service.GetLessonPlanningCdrAndTopicList(query);
        }
        #endregion

        #region 更新课程备课素材、习题排序
        /// <summary>
        /// 更新课程备课素材、习题排序
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateLessonPlanningCdrAndTopicSort(List<LessonPlanningCdrAndTopicRequestModel> list)
        {
            return await _service.UpdateLessonPlanningCdrAndTopicSort(list, this.CurrentUser);
        }
        #endregion

    }
}