using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户收货地址
    /// </summary>
    public class UserAddressController : BaseController
    {
        private IUserAddressService _service;

        public UserAddressController(IUserAddressService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(UserAddressDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            model.UserId = CurrentUser.Id;
            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        private async Task<ResponseContext<UserAddressDto>> Get(long id)
        {
            var response = new ResponseContext<UserAddressDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<UserAddressListResponseDto>> List()
        {
            var response = await _service.List(CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 设置默认收货地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetDefault(long id)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (id <= 0)
            {
                response.Msg = "参数无效";
                return response;
            }

            response = await _service.SetDefault(id, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取用户默认收货地址
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<UserDefaultAddressDto>> GetUserDefaultAddress()
        {
            var response = new ResponseContext<UserDefaultAddressDto> { Code = CommonConstants.BadRequest };
            //if (query == null)
            //{
            //    response.Msg = "参数不能为空";
            //    return response;
            //}

            response = await _service.GetUserDefaultAddress(CurrentUser.Id);

            return response;
        }
    }
}