﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 摘要Api
    /// </summary>
    public class SummaryController : BaseController
    {
        ISummaryService service;
        ICourseSummaryService courseSummaryService;
        public SummaryController(ISummaryService s1, ICourseSummaryService s2)
        {
            service = s1;
            courseSummaryService = s2;
        }

        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model)
        {
            return await service.AddSummary(model);
        }

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long id)
        {
            return await service.GetSummaryList(id);
        }

        /// <summary>
        /// App端获取摘要列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="option">>0：岗位实训,1：课程实训</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SummaryDto>>> GetAppSummaryList(long id,int option)
        {
            if (option == 0)
            {
                return await service.GetSummaryList(id);
            }
            else
            {
                return await courseSummaryService.GetSummaryList(id);
            }
        }

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSummary(long id)
        {
            return await service.RemoveSummary(id);
        }


        /// <summary>
        /// 新增课程摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<SummaryDto>> AddCourseSummary(SummaryDto model)
        {
            model.CreateBy = CurrentUser.Id;
            return await courseSummaryService.AddSummary(model);
        }

        /// <summary>
        /// 获取课程摘要列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SummaryDto>>> GetCourseSummaryList(long cid)
        {
            return await courseSummaryService.GetSummaryList(cid);
        }

        /// <summary>
        /// 删除课程摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveCourseSummary(long id)
        {
            return await courseSummaryService.RemoveSummary(id);
        }
    }
}