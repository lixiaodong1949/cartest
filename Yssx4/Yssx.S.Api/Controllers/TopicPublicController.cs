﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 公共 - 题目
    /// </summary>
    public class TopicPublicController : BaseController
    {
        private ITopicPublicService _topicPubService;
        private ICertificateTopicPublicService _certificateTopicPublicService;
        /// <summary>
        /// 
        /// </summary>
        public TopicPublicController(ITopicPublicService topicPubService, ICertificateTopicPublicService certificateTopicPublicService)
        {
            _topicPubService = topicPubService;
            _certificateTopicPublicService = certificateTopicPublicService;
        }

        #region 添加题目
        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> SaveSimpleQuestion(SimpleQuestionPubRequest model)
        {
            return await _topicPubService.SaveSimpleQuestion(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 复杂题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> SaveComplexQuestion(ComplexQuestionPubRequest model)
        {
            return await _topicPubService.SaveComplexQuestion(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> SaveMainSubQuestion(MainSubQuestionPubRequest model)
        {
            return await _topicPubService.SaveMainSubQuestion(model, CurrentUser.Id);
        }

        #endregion

        #region 获取题目信息
        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TopicPubInfoDto>> GetQuestion(long id)
        {
            return await _topicPubService.GetQuestion(id);
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TopicPubListView>> GetQuestionByPage(GetQuestionByPageRequest model)
        {
            return await _topicPubService.GetQuestionByPage(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询题目列表 - 所有人创建的
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TopicPubListView>> GetAllQuestionByPage(GetPubQuestionByPageRequest model)
        {
            //验证账号是否有【专业组】角色
            if (CurrentUser.Type != 6)
                return new PageResponse<TopicPubListView> { Code = CommonConstants.ErrorCode, Msg = "非指定权限账号，不可查询此数据", Data = new List<TopicPubListView>() };

            return await _topicPubService.GetAllQuestionByPage(model);
        }


        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteQuestion(long id)
        {
            return await _topicPubService.DeleteQuestion(id, CurrentUser.Id);
        }

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTopicFile(long id)
        {
            return await _topicPubService.DeleteTopicFile(id, CurrentUser.Id);
        }

        /// <summary>
        /// 删除选项
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteOption(long topicId, long id)
        {
            return await _topicPubService.DeleteOption(topicId, id, CurrentUser.Id);
        }

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="status">0：禁用,1:启用</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status)
        {
            return await _topicPubService.SetQuestionStatuts(topicId, status, CurrentUser.Id);
        }
        #endregion

        #region 分录题操作
        /// <summary>
        /// 添加凭证（分录题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> SaveCertificateTopic(CertificateTopicPubRequest model)
        {
            return await _certificateTopicPublicService.SaveCertificateTopic(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取单个分录题
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CertificateTopicPubRequest>> GetCoreCertificateTopic(long id)
        {
            return await _certificateTopicPublicService.GetCoreCertificateTopic(id);
        }

        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCertificateTopicById(long id)
        {
            return await _certificateTopicPublicService.DeleteCertificateTopicById(id, CurrentUser.Id);
        }

        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCertificateTopicByTopicId(long id)
        {
            return await _certificateTopicPublicService.DeleteCertificateTopicByTopicId(id, CurrentUser.Id);
        }
        #endregion

    }
}