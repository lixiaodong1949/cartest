﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课件相关Api
    /// </summary>
    public class CourceFilesController : BaseController
    {
        ICourceFilesService service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s1"></param>
        public CourceFilesController(ICourceFilesService s1)
        {
            service = s1;
        }


        /// <summary>
        /// 新增课件
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditCourceFile(YssxCourceFilesDto dto)
        {
            return await service.AddOrEditCourceFile(dto, this.CurrentUser);
        }

        /// <summary>
        /// 获取课件列表
        /// </summary>
        /// 
        /// <param name="userType">0:普通用户,1:审核用户</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxCourceFilesDto>>> GetCourceFilesList(int userType)
        {
            return await service.GetCourceFilesList(this.CurrentUser, userType);
        }

        /// <summary>
        ///  获取课件列表带分页
        /// </summary>
        /// <param name="keyword">课件搜索关键字</param>
        /// <param name="fileType">文件类型</param>
        /// <param name="status">课件状态</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourceFilesDto>> GetCourceFilesListByPage(string keyword, int fileType, int status, int pageIndex, int pageSize)
        {
            return await service.GetCourceFilesListByPage(this.CurrentUser, keyword, fileType, status, pageIndex, pageSize);
        }

        /// <summary>
        /// 获取单个课件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxCourceFilesDto>> GetCourceFileOne(long id)
        {
            return await service.GetCourceFileOne(id);
        }

        /// <summary>
        /// 删除课件
        /// </summary>
        /// <param name="ids">批量删除</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveCourceFile(string ids)
        {
            return await service.RemoveCourceFile(this.CurrentUser.Id, ids);
        }

        /// <summary>
        /// 课件搜索
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileType"></param>
        /// <param name="klgId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourceFilesDto>> SearchFilesListByPage(string fileName, int fileType, long klgId, int pageIndex, int pageSize)
        {
            return await service.SearchFilesListByPage(this.CurrentUser, fileName, fileType, klgId, pageIndex, pageSize);
        }

        /// <summary>
        /// 获取课件列表(购买/上传)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseFilesViewModel>> GetCourseFilesByBuyOrOneself([FromQuery]YssxCourseFilesQuery query)
        {
            return await service.GetCourseFilesByBuyOrOneself(query, this.CurrentUser);
        }

        /// <summary>
        /// 新增资源文件
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditResourceFile(YssxCourceFilesDto dto)
        {
            return await service.AddOrEditResourceFile(dto, this.CurrentUser);
        }
    }
}