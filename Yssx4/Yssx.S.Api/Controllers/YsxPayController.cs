﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Aop.Api.Util;
using AutoMapper.Configuration;using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Yssx.Framework.Logger;

namespace Yssx.S.Api.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    /// <summary>
    /// 支付回调
    /// </summary>
    public class YsxPayController : BaseController
    {
        public IConfigurationSection Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        public YsxPayController()
        {

        }

        /// <summary>
        /// 支付同步回调
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void SyncCallback()
        {
            Dictionary<string, string> sArray = GetRequestPost();
            if (sArray.Count != 0)
            {
                bool flag = AlipaySignature.RSACheckV1(sArray, Configuration["Alipay.PublicKey"], "UTF-8", "RSA2", false);
                if (flag)
                {
                    string trade_status = Request.Form["trade_status"];
                    Console.WriteLine("success");
                    CommonLogger.Info("订单："+ sArray["out_trade_no"] + " "+ trade_status);
                }
                else
                {
                    CommonLogger.Info($"订单 {sArray["out_trade_no"]} ：fail");
                }
            }
        }

        private Dictionary<string, string> GetRequestPost()
        {
            int i = 0;
            Dictionary<string, string> sArray = new Dictionary<string, string>();

            var coll = Request.Form;
            var requestItem = coll.Keys;
            for (i = 0; i < requestItem.Count; i++)
            {
                sArray.Add(requestItem.ElementAt(i), Request.Form[requestItem.ElementAt(i)]);
            }
            return sArray;

        }

        /// <summary>
		/// 支付异步回调
		/// </summary>
		[HttpGet]
        public string Callback()        {
            /* 实际验证过程建议商户添加以下校验。
			1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
			2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
			3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
			4、验证app_id是否为该商户本身。
			*/
            Dictionary<string, string> sArray = GetRequestGet();
            string no = string.Empty;
            if (sArray.Count != 0)
            {
                no = sArray["out_trade_no"];
                Console.WriteLine($"同步验证通过，订单号：{sArray["out_trade_no"]}");
                //  bool flag = _alipayService.RSACheckV1(sArray);
                //if (flag)
                //{
                //    Console.WriteLine($"同步验证通过，订单号：{sArray["out_trade_no"]}");
                //    ViewData["PayResult"] = "同步验证通过";
                //}
                //else
                //{
                //    Console.WriteLine($"同步验证失败，订单号：{sArray["out_trade_no"]}");
                //    ViewData["PayResult"] = "同步验证失败";
                //}
            }
            return no;
        }

        private Dictionary<string, string> GetRequestGet()
        {
            Dictionary<string, string> sArray = new Dictionary<string, string>();

            ICollection<string> requestItem = Request.Query.Keys;
            foreach (var item in requestItem)
            {
                sArray.Add(item, Request.Query[item]);

            }
            return sArray;
        }
    }
}