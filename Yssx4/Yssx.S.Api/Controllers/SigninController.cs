using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 签到
    /// </summary>
    public class SigninController : BaseController
    {
        private ISigninService _service;

        public SigninController(ISigninService service)
        {
            _service = service;
        }

        /// <summary>
        /// 签到
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SaveSignin()
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };

            result = await _service.SaveSignin(CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 是否签到
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> IsSignin()
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            //if (query == null)
            //{
            //    response.Msg = "参数不能为空";
            //    return response;
            //}

            response = await _service.IsSignin(CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 根据Id获取签到详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        private async Task<ResponseContext<SigninDto>> GetSignin(long id)
        {
            var response = new ResponseContext<SigninDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.GetSignin(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除签到
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        private async Task<ResponseContext<bool>> DeleteSignin(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.DeleteSignin(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询签到列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<PageResponse<SigninPageResponseDto>> GetSigninListPage(SigninPageRequestDto query)
        {
            var response = new PageResponse<SigninPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetSigninListPage(query);

            return response;
        }

        /// <summary>
        /// 根据条件查询签到列表信息 (不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<ListResponse<SigninListResponseDto>> GetSigninList(SigninListRequestDto query)
        {
            var response = new ListResponse<SigninListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetSigninList(query);

            return response;
        }


    }
}