﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 资源接口
    /// </summary>
    public class ResourcesController : BaseController
    {
        private IResourceSettingService _service;
        public ResourcesController(IResourceSettingService service)
        {
            this._service = service;
        }


        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            return await _service.GetIndustryList(model);
        }

    }
}