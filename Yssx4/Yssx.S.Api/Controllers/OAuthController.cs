﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.S.Api.Auth;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 认证
    /// </summary>
    public class OAuthController : BaseController
    {
        /// <summary>
        /// 自定义策略参数
        /// </summary>
        PermissionRequirement _requirement;
        IAuthenticateService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requirement"></param>
        public OAuthController(PermissionRequirement requirement, IAuthenticateService service)
        {
            _requirement = requirement;
            _service = service;
        }

        /// <summary>
        /// 前端系统认证
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>      
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UserAuthentication(UserAuthentication model)
        {
            return await _service.UserAuthentication(model, HttpContext.GetClientUserIp(), _requirement);
        }

        /// <summary>
        /// 电话号码登陆
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="loginType">登录方式 0:PC 1:App 2.小程序</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> NumberUserPhoneAuthentication(string mb, string verificationCode, int loginType)
        {
            return await _service.NumberUserPhoneAuthentication(mb, verificationCode, HttpContext.GetClientUserIp(), _requirement, loginType);
        }

        /// <summary>
        /// 微信登陆
        /// </summary>
        /// <param name="wx">微信openid</param>
        /// <param name="unionid">微信平台唯一标识</param>
        /// <param name="nickname">微信昵称</param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<WeChartTicket>> WeChartAuthentication(string wx, string unionid, string nickname, int loginType)
        {
            return await _service.WeChartAuthentication(wx, unionid, nickname, HttpContext.GetClientUserIp(), _requirement, loginType);
        }

        /// <summary>
        /// 小程序登陆
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<WeChartTicket>> WeChartAppletsAuthentication(WeChartAppletsDto model)
        {
            return await _service.WeChartAppletsAuthentication(model, HttpContext.GetClientUserIp(), _requirement);
        }

        /// <summary>
        ///前端退出登录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResponseContext<bool> UserLoginOut()
        {
            AuthenticateService.LoginOut(CurrentUser);
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 工学号登陆获取学校
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<DropdownListDto>>> DropdownList()
        {
            return await _service.DropdownList();
        }

        /// <summary>
        /// 找回密码
        /// </summary>
        /// <param name="mb">电话</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次密码</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RetrievePassword(string mb, string verificationCode, string password, string againPassword)
        {
            return await _service.RetrievePassword(mb, verificationCode, password, againPassword);
        }

        /// <summary>
        ///  设置密码
        /// </summary>
        /// <param name="dto">密码</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetPassword(SetPasswordDto dto)
        {
            var result = new ResponseContext<bool>();

            return await _service.SetPassword(dto.Password, CurrentUser);
        }

        /// <summary>
        /// 工学号登陆
        /// </summary>
        /// <param name="schoolId">学校Id</param>
        /// <param name="engineeringnumber">工学号</param>
        /// <param name="password">密码</param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> EngineeringNumberLogin(long schoolId, string engineeringnumber, string password, int loginType)
        {
            return await _service.EngineeringNumberLogin(schoolId, engineeringnumber, password, HttpContext.GetClientUserIp(), _requirement, loginType);
        }

        /// <summary>
        /// 首页体验Token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<string>> GetExperienceToken()
        {
            return await _service.GetExperienceToken(_requirement);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> GetExperienceToken2()
        {
            return await _service.GetExperienceToken2(_requirement);
        }

        /// <summary>
        /// 获取新token - 当前用户
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetCurrentUserToken(int loginType)
        {
            return await _service.GetCurrentUserToken(_requirement, CurrentUser.Id, HttpContext.GetClientUserIp(), loginType);
        }

        /// <summary>
        /// 检查当前用户的用户类型是否改变
        /// </summary>
        /// <param name="loginType">登录类型：</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CheckUserRoleResponseDto>> CheckUserRole(int loginType)
        {
            return await _service.CheckUserRole(CurrentUser, HttpContext.GetClientUserIp(), loginType);
        }

        /// <summary>
        /// 获取新token - 当前用户 - new
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserTicket>> GetCurrentUserNewToken(int loginType)
        {
            return await _service.GetCurrentUserNewToken(_requirement, CurrentUser.Id, HttpContext.GetClientUserIp(), loginType);
        }

        #region 用户密码登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户密码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UserPasswordLoginByInvite(UserLoginRequestModel dto)
        {
            return await _service.UserPasswordLoginByInvite(dto, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

        #region 用户验证码登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户验证码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UserVerificationCodeLoginByInvite(UserLoginRequestModel dto)
        {
            return await _service.UserVerificationCodeLoginByInvite(dto, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

        #region 用户微信登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户微信登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<WeChartTicket>> UserWeChatLoginByInvite(UserLoginRequestModel dto)
        {
            return await _service.UserWeChatLoginByInvite(dto, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

    }
}