using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 积分商品兑换
    /// </summary>
    public class IntegralProductExchangeController : BaseController
    {
        private IIntegralProductExchangeService _service;

        public IntegralProductExchangeController(IIntegralProductExchangeService service)
        {
            _service = service;
        }

        /// <summary>
        /// 兑换商品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(IntegralProductExchangeDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.Save(model, CurrentUser);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        private async Task<ResponseContext<IntegralProductExchangeDto>> Get(long id)
        {
            var response = new ResponseContext<IntegralProductExchangeDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IntegralProductExchangePageByUserIdResponseDto>> ListPage(IntegralProductExchangePageRequestDto query)
        {
            var response = new PageResponse<IntegralProductExchangePageByUserIdResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }
            query.UserId = CurrentUser.Id;
            response = await _service.ListPageByUserId(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<ListResponse<IntegralProductExchangeListResponseDto>> List(IntegralProductExchangeListRequestDto query)
        {
            var response = new ListResponse<IntegralProductExchangeListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }
    }
}