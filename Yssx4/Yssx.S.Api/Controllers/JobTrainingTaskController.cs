﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 岗位实训任务相关Api
    /// </summary>
    public class JobTrainingTaskController : BaseController
    {
        IJobTrainingTaskService _service;

        public JobTrainingTaskController(IJobTrainingTaskService service)
        {
            _service = service;
        }

        #region 发布或修改岗位实训任务
        /// <summary>
        /// 发布或修改岗位实训任务
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> PublishOrUpdateJobTraingingTask(JobTrainingTaskDto dto)
        {
            return await _service.PublishOrUpdateJobTraingingTask(dto, this.CurrentUser);
        }
        #endregion

        #region 获取教师岗位实训任务列表
        /// <summary>
        /// 获取教师岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxJobTrainingTaskTeacherViewModel>> GetJobTrainingTaskTeacherList([FromQuery]YssxJobTrainingTaskTeacherQuery query)
        {
            return await _service.GetJobTrainingTaskTeacherList(query, this.CurrentUser);
        }
        #endregion

        #region 获取学生岗位实训任务列表
        /// <summary>
        /// 获取学生岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxJobTrainingTaskStudentViewModel>> GetJobTrainingTaskStudentList([FromQuery]YssxJobTrainingTaskStudentQuery query)
        {
            return await _service.GetJobTrainingTaskStudentList(query, this.CurrentUser);
        }
        #endregion

        #region 验证岗位实训任务密码
        /// <summary>
        /// 验证岗位实训任务密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ValidateJobTraingingTaskPassword(long id, string password)
        {
            return await _service.ValidateJobTraingingTaskPassword(id, password);
        }
        #endregion

        #region 结束岗位实训任务
        /// <summary>
        /// 结束岗位实训任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> FinishJobTraingingTask(long id)
        {
            return await _service.FinishJobTraingingTask(id, this.CurrentUser);
        }
        #endregion

        #region 删除岗位实训任务
        /// <summary>
        /// 删除岗位实训任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveJobTraingingTask(long id)
        {
            return await _service.RemoveJobTraingingTask(id, this.CurrentUser);
        }
        #endregion

        #region 获取岗位实训任务试卷作答记录
        /// <summary>
        /// 获取岗位实训任务试卷作答记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxJobTrainingTaskExamAnswerRecordViewModel>>> GetJobTrainingTaskExamAnswerRecord([FromQuery]YssxJobTrainingTaskExamAnswerRecordQuery query)
        {
            return await _service.GetJobTrainingTaskExamAnswerRecord(query);
        }
        #endregion

        #region 根据岗位实训任务获取绑定班级的学生列表
        /// <summary>
        /// 根据岗位实训任务获取绑定班级的学生列表
        /// </summary>
        /// <param name="jobTrainingTaskId">任务Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxJobTrainingTaskBindingStudentViewModel>> GetJobTrainingTaskBindingStudentList(long jobTrainingTaskId)
        {
            return await _service.GetJobTrainingTaskBindingStudentList(jobTrainingTaskId);
        }
        #endregion

        #region 根据试卷Id统一交卷、统计分数
        /// <summary>
        /// 根据试卷Id统一交卷、统计分数
        /// </summary>
        /// <param name="examId"></param>
        [HttpGet]
        public async Task<ResponseContext<bool>> SubmitPaperAndCalculateScoreByExamId(long examId)
        {
            return await _service.SubmitPaperAndCalculateScoreByExamId(examId, this.CurrentUser);
        }


        #endregion
        /// <summary>
        /// 重启任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="endDate"></param>
        /// <param name="isCanCheckExam"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam)
        {
            return await _service.SetTaskStatus(taskId, endDate, isCanCheckExam);
        }

        /// <summary>
        /// 重启任务 APP
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetTaskStatusApp(SetTaskStatusDto dto)
        {
            return await _service.SetTaskStatus(dto.TaskId, dto.EndDate, dto.IsCanCheckExam);
        }

        /// <summary>
        /// 重启学生做题
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetStudentStatus(long taskId, long studentId)
        {
            return await _service.SetStudentStatus(taskId, studentId);
        }

        /// <summary>
        /// 设置单个学生
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetStudentStatusById(long gradeId, StudentExamStatus status)
        {
            return await _service.SetStudentStatus(gradeId, status);
        }


        #region 获取岗位实训任务套题(案例)列表
        /// <summary>
        /// 获取岗位实训任务套题(案例)列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<JobTrainingCaseViewModel>>> GetJobTrainingCaseList(long taskId)
        {
            return await _service.GetJobTrainingCaseList(taskId);
        }
        #endregion

        #region 获取岗位实训任务试卷学生实时作答详情
        /// <summary>
        /// 获取岗位实训任务试卷学生实时作答详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<JobTrainingTaskExamStudentAnswerRecordViewModel>> GetJobTrainingTaskExamStudentAnswerRecord([FromQuery]JobTrainingTaskExamStudentAnswerRecordQuery query)
        {
            return await _service.GetJobTrainingTaskExamStudentAnswerRecord(query);
        }
        #endregion

        #region 取消交卷
        /// <summary>
        /// 取消交卷
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CancelExamSubmit(CancelSubmitRequestModel dto)
        {
            return await _service.CancelExamSubmit(dto, this.CurrentUser);
        }
        #endregion

        #region 打开或关闭学生查看试卷入口
        /// <summary>
        /// 打开或关闭学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> OpenOrCloseJobTrainingTaskCheckExam(CloseCheckExamRequestModel request)
        {
            return await _service.OpenOrCloseJobTrainingTaskCheckExam(request, this.CurrentUser);
        }
        #endregion

        #region 获取岗位实训任务套题(案例)列表【学生端】
        /// <summary>
        /// 获取岗位实训任务套题(案例)列表【学生端】
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetStudentTaskCaseList(long taskId)
        {
            return await _service.GetStudentTaskCaseList(taskId, this.CurrentUser);
        }
        #endregion

    }
}