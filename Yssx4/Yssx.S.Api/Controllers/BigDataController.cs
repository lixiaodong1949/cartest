﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using Yssx.Framework;
using Yssx.S.Dto.BigDatas;
using Yssx.S.IServices.BigData;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 大数据 重复GV的双方各付
    /// </summary>
    public class BigDataController : BaseController
    {
        private IBigDataService _service;
        public BigDataController(IBigDataService service)
        {
            _service = service;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        /// <param name="id">表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页数</param>
        /// <param name="year">年份</param>
        /// <param name="parentId">模块id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<BigDataDto>> GetObjectList(int id, int pageIndex, int pageSize, string year, int parentId)
        {
            return await _service.GetObjectList(id, pageIndex, pageSize, year, parentId);
        }

        /// <summary>
        /// 搜索列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<BigDataDto>> GetSearchList(SearchDto model)
        {
            return await _service.GetSearchList(model);
        }

        /// <summary>
        /// 搜索下拉列表
        /// </summary>
        /// <param name="id">表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)</param>
        /// <returns></returns>
        [HttpGet]
        public ResponseContext<BigDataDropDownDto> GetDropDownList(int id)
        {
            return _service.GetDropDownList(id);
        }

        /// <summary>
        /// 加载模块列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ResponseContext<BigDataParentListDto> GetParentList(string year)
        {
            return _service.GetParentList(year);
        }

        /// <summary>
        /// 一键复制
        /// </summary>
        /// <param name="model">搜索结果返回的对象数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveAnswer(ExcelContentModel model)
        {
            return await _service.SaveAnswer(model);
        }

        /// <summary>
        /// 获取答案
        /// </summary>
        /// <param name="examId">试卷id</param>
        /// <param name="gradeId">关联id</param>
        /// <param name="questionId">题目id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<AnswerDto>> GetCase(string examId, string gradeId, string questionId)
        {
            return await _service.GetCase(examId, gradeId, questionId);
        }

        /// <summary>
        /// 获取搜索条件下拉
        /// </summary>
        /// <returns></returns>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ConditionsDto>>> GetSearchonditionsList()
        {
            return await _service.GetSearchonditionsList();
        }

        /// <summary>
        /// excel导出
        /// </summary>
        /// <param name="search">对象json格式</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<FileResult> EcxelExport(string search)
        {
            var newAnswerValue = HttpUtility.UrlDecode(search);
            ExcelDto sear = JsonConvert.DeserializeObject<ExcelDto>(newAnswerValue);
            MemoryStream stream = await _service.EcxelExport(sear);
            var bytes = stream.ToArray();
            var extention = ".xls";
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.Mappings.TryGetValue(extention, out var memi))
            {
                throw new Exception("找不到可导出的文件类型");
            }
            return File(bytes, memi, $"{sear.Name}{DateTime.Now.ToString("yyyyMMddHHmmssffff")}{extention}");
        }
    }
}