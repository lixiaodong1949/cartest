﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices.Mall;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 商城
    /// </summary>
    public class MallController : BaseController
    {
        private readonly IMallOrderService _mallOrderService;
        private readonly IMallProductService _mallProductService;

        public MallController(IMallOrderService mallOrderService,IMallProductService mallProductService)
        {
            _mallOrderService = mallOrderService;
            _mallProductService = mallProductService;
        }

        #region 订单
        /// <summary>
        /// 根据订单编号获取订单
        /// </summary>
        /// <param name="orderNo">订单编号</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<MallOrderDto>> GetUserOrder(long orderNo)
        {
            return await _mallOrderService.GetUserOrder(orderNo);
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<MallOrderDto>> GetOrderList(GetMallOrderDto input)
        {
            input.UserId = CurrentUser.Id;
            return await _mallOrderService.GetOrderList(input);
        }

        /// <summary>
        /// 迁移
        /// </summary>
        [HttpGet]
        public void Migrate()
        {
            _mallOrderService.MigrateAsync();
        }

        #endregion

        #region 商品

        /// <summary>
        /// 获取商品列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<MallProductDto>> GetProductList(GetMallProductDto input)
        {
            return await _mallProductService.GetProductList(input);
        }

        /// <summary>
        /// 获取商品价格
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<List<GetProductPriceOutputDto>>> GetProductPrices(GetProductPriceInputDto input)
        {
            return await _mallProductService.GetProductPrices(input);
        }

        /// <summary>
        /// 商品点赞
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetProductLikesNumber(long id)
        {
            return await _mallProductService.SetProductLikesNumber(id, CurrentUser.Id);
        }

        #endregion

        #region APP商城
        /// <summary>
        /// 获取商品分类列表 - APP
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryListApp()
        {
            return await _mallProductService.GetMallCategoryListApp();
        }

        /// <summary>
        /// 获取商品二级分类列表 - APP
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<GetMallCategoryListDetailDto>> GetMallCategoryByParentIdApp(long id)
        {
            return await _mallProductService.GetMallCategoryByParentIdApp(id);
        }


        /// <summary>
        /// 首页获取一级分类下TOP部分商品 - APP
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetCategoryProductsTopToHomeResponseDto>>> GetCategoryProductsTopToHomeApp(int top = 4)
        {
            return await _mallProductService.GetCategoryProductsTopToHomeApp(top);
        }

        /// <summary>
        /// 商品列表 - APP
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ProductBaseResponseDto>> QueryProductsApp(QueryProductsRequestDto model)
        {
            return await _mallProductService.QueryProductsApp(model);
        }

        /// <summary>
        /// 获取指定商品数据 - APP
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SaveProductDto>> GetProductInfoByIdApp(long id)
        {
            return await _mallProductService.GetProductInfoById(id);
        }


        /// <summary>
        /// 查询用户订单 - APP
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetUserOrderListAppDto>> GetUserOrderListApp(GetUserOrderListAppRequest model)
        {
            return await _mallOrderService.GetUserOrderListApp(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询指定订单 - APP
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<MallOrderDto>> GetOrderByIdApp(long id)
        {
            return await _mallOrderService.GetOrderByIdApp(id, CurrentUser.Id);
        }

        #endregion
    }
}
