﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户首页信息Api
    /// </summary>
    public class UserHomeController : BaseController
    {
        IUserHomeService _service;

        public UserHomeController(IUserHomeService service)
        {
            _service = service;
        }

        #region 获取用户自主训练
        /// <summary>
        /// 获取用户自主训练
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxUserAutoTrainingViewModel>> GetUserAutoTrainingList()
        {
            return await _service.GetUserAutoTrainingList(this.CurrentUser);
        }
        #endregion

        #region 获取学生任务列表
        /// <summary>
        /// 获取学生任务列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxStudentTaskInfoViewModel>>> GetStudentTaskList()
        {
            return await _service.GetStudentTaskList(this.CurrentUser);
        }
        #endregion

        #region 获取教师任务列表
        /// <summary>
        /// 获取教师任务列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherTaskInfoViewModel>>> GetTeacherTaskList()
        {
            return await _service.GetTeacherTaskList(this.CurrentUser);
        }
        #endregion

        #region 获取教师资源详情
        /// <summary>
        /// 获取教师资源详情
        /// </summary> 
        /// <returns></returns>
        //[HttpGet]
        //public async Task<ResponseContext<List<YssxTeacherResourceViewModel>>> GetTeacherResourceList()
        //{
        //    return await _service.GetTeacherResourceList(this.CurrentUser);
        //}
        #endregion

        /// <summary>
        /// 获取教师实训资源详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherTrainResourceViewModel>>> GetTeacherTrainResourceList()
        {
            return await _service.GetTeacherTrainResourceList(this.CurrentUser);
        }

        #region 获取首页案例列表
        /// <summary>
        /// 获取首页案例列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserCaseViewModel>>> GetUserCaseList()
        {
            return await _service.GetUserCaseList();
        }
        #endregion

    }
}