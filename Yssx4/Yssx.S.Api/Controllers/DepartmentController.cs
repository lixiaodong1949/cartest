﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 部门相关Api
    /// </summary>
    public class DepartmentController : BaseController
    {
        IDepartmentService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public DepartmentController(IDepartmentService service)
        {
            _service = service;
        }



        /// <summary>
        /// 保存部门
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveDepartment(DepartmentRequest model)
        {
            return await _service.SaveDepartment(model,CurrentUser.Id);
        }

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteDepartment(long id)
        {
            return await _service.DeleteDepartment(id);
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<DepartmentDto>> GetDepartmentList([FromQuery]DepartmentListRequest model)
        {
            return await _service.GetDepartmentList(model);
        }
        /// <summary>
        /// 调整顺序  isAscending:true升序   false降序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ChangeSort(long id, bool isAscending = true)
        {
            return await _service.ChangeSort(id, isAscending);
        }
    }
}