﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 章节相关Api
    /// </summary>
    public class SectionController : BaseController
    {
        ISectionService _service;
        public SectionController(ISectionService service)
        {
            _service = service;
        }

        /// <summary>
        /// 新增,编辑章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSection(YssxSectionDto dto)
        {
            return await _service.AddOrEditSection(dto);
        }

        /// <summary>
        /// 删除章节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSection(long id)
        {
            return await _service.RemoveSection(id);
        }


        /// <summary>
        /// 获取课程下面的所有章节
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionDto>>> GetSectionList(long courseId)
        {
            return await _service.GetSectionList(courseId);
        }

        /// <summary>
        /// 新增,编辑教材
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSectionTextBook(YssxSectionTextBookDto dto)
        {
            return await _service.AddOrEditSectionTextBook(dto);
        }

        /// <summary>
        /// 删除教材
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSectionTextBook(long id)
        {
            return await _service.RemoveSectionTextBook(id);
        }

        /// <summary>
        /// 获取教材内容
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionTextBookDto>>> GetectionTextBookList(long sid)
        {
            return await _service.GetSectionTextBookList(sid);
        }

        /// <summary>
        /// 获取章节下面的汇总
        /// </summary>
        /// <param name="sid">章节Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionSummaryDto>>> GetCourseSummaryList(long sid)
        {
            return await _service.GetCourseSummaryList(sid);
        }

        /// <summary>
        ///  新增,编辑课程课件,教案
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSectionFile(YssxSectionFilesDto dto)
        {
            return await _service.AddOrEditSectionFile(dto, this.CurrentUser);
        }
        /// <summary>
        /// 新增,编辑课程习题
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSectionTopic(YssxSectionTopicDto dto)
        {
            return await _service.AddOrEditSectionTopic(dto);
        }

        /// <summary>
        /// 获取章节下面的课件带文件名搜索
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="sid">节Id</param>
        /// <param name="fileName">fileName</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesList(long courseId, long sid)
        {
            return await _service.GetSectionFilesList(courseId, sid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sid"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SectionFileListDto>> GetSectionFilesListByType(long courseId, long sid, int fileType)
        {
            return await _service.GetSectionFilesListByType(courseId, sid, fileType);
        }

        /// <summary>
        /// 获取课程下所有的课件,教案,视频按类型筛选 （包含课程章节下的）
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByCourseId(long courseId)
        {
            return await _service.GetSectionFilesListByCourseId(courseId);
        }
        /// <summary>
        /// 新增，编辑课程案例
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSectionCase(YssxSectionCaseDto dto)
        {
            return await _service.AddOrEditSectionCase(dto);
        }

        /// <summary>
        /// 删除课程案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSectionCase(long id)
        {
            return await _service.RemoveSectionCase(id);
        }

        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSectionTopic(long id)
        {
            return await _service.RemoveSectionTopic(id);
        }

        /// <summary>
        /// 删除课件，教案，视频
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSectionFile(long id)
        {
            return await _service.RemoveSectionFile(id);
        }

        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicList(long sid)
        {
            return await _service.GetYssxSectionTopicList(sid);
        }

        /// <summary>
        /// 根据节获取习题
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicistForPC([FromQuery]SectionTopicQuery query)
        {
            return await _service.GetYssxSectionTopicistForPC(query);
        }

        /// <summary>
        /// 新增、编辑课程场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditSectionSceneTraining(YssxSectionSceneTrainingDto dto)
        {
            //如果新增场景实训,则需要场景实训详情信息
            return await _service.AddOrEditSectionSceneTraining(dto);
        }

        /// <summary>
        /// 删除课程场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSectionSceneTraininge(long id)
        {
            return await _service.RemoveSectionSceneTraininge(id);
        }

        /// <summary>
        /// 获取课程场景实训列表(根据章节Id)
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionSceneTrainingViewModel>>> GetYssxSectionSceneTrainingList(long sid)
        {
            return await _service.GetYssxSectionSceneTrainingList(sid);
        }


        /// <summary>
        /// 根据课程id查询章节列表信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SectionListDto>>> GetSectionListByCourseId(long courseId)
        {
            return await _service.GetSectionListByCourseId(courseId);
        }

        /// <summary>
        /// 根据章节id获取附件集合
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SectionFileListNewDto>> GetSectionFilesListBySectionId(long sid)
        {
            return await _service.GetSectionFilesListBySectionId(sid);
        }
        /// <summary>
        /// 根据课程id获取教材列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxSectionFilesDto>> GetCourseFilesListByCourseId(long courseId)
        {
            return await _service.GetCourseFilesListByCourseId(courseId);
        }
        /// <summary>
        /// 新增/修改课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveSection(YssxSectionDto dto)
        {
            return await _service.SaveSection(dto);
        }
    }
}