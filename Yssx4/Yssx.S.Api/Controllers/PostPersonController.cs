﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 岗位人员相关Api
    /// </summary>
    public class PostPersonController : BaseController
    {
        IPostPersonService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public PostPersonController(IPostPersonService service)
        {
            _service = service;
        }



        /// <summary>
        /// 保存岗位人员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SavePostPerson(PostPersonRequest model)
        {
            return await _service.SavePostPerson(model,CurrentUser.Id);
        }

        /// <summary>
        /// 删除岗位人员
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeletePostPerson(long id)
        {
            return await _service.DeletePostPerson(id);
        }

        /// <summary>
        /// 获取岗位人员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<PostPersonDto>> GetPostPersonList([FromQuery]PostPersonListRequest model)
        {
            return await _service.GetPostPersonList(model);
        }
        /// <summary>
        /// 调整顺序  isAscending:true升序   false降序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ChangeSort(long id, bool isAscending = true)
        {
            return await _service.ChangeSort(id, isAscending);
        }
    }
}