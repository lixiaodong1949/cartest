﻿
namespace Yssx.S.Api
{
    /// <summary>
    /// 基本的 App 配置对象，可扩展或继承
    /// </summary>
    public partial class BaseAppsettings
    {
        /// <summary>
        /// 服务发现 Consul 配置
        /// </summary>
        public ConsulConfig ConsulConfig { get; set; }

    }
}
