﻿
using System.Configuration;
namespace Yssx.Common
{
    public class ConfigSettings
    {
        public static string TrainingCloudConnectionString { get; set; }
        public static string ENodeConnectionString { get; set; }



        public static void Initialize()
        {
            if (ConfigurationManager.ConnectionStrings["yssx"] != null)
            {
                TrainingCloudConnectionString = ConfigurationManager.ConnectionStrings["yssx"].ConnectionString;

            }

            if (ConfigurationManager.ConnectionStrings["enode"] != null)
            {
                ENodeConnectionString = ConfigurationManager.ConnectionStrings["enode"].ConnectionString;
            }
        }
    }
}
    