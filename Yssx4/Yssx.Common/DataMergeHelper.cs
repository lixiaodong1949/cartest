﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Yssx.Common
{
    public class DataMergeHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static ConcurrentDictionary<string, Action<List<object>>> keyValuePairs = new ConcurrentDictionary<string, Action<List<object>>>();
        /// <summary>
        /// 
        /// </summary>
        private static ConcurrentDictionary<string, ArrayList> keyValuePairDatas = new ConcurrentDictionary<string, ArrayList>();

        private static KeyValuePair<string, ArrayList> newKvp = new KeyValuePair<string, ArrayList>();

        private static readonly object locker = new object();

        private static bool _isStart;
        public static void Start()
        {
            if (_isStart)
                return;
            _isStart = true;
            new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(800);
                    try
                    {
                        //foreach (var kvp in keyValuePairDatas)
                        //{
                        //    if (keyValuePairs.TryGetValue(kvp.Key, out var action))
                        //    {
                        //        if (kvp.Value.Count == 0)
                        //            continue;
                        //        var json = string.Empty;
                        //        lock (kvp.Value.SyncRoot)
                        //        {
                        //            List<object> items;
                        //            var batchNum = 400;
                        //            if (kvp.Value.Count >= batchNum)
                        //            {
                        //                items = kvp.Value.Cast<object>().Take(batchNum).ToList();
                        //                kvp.Value.RemoveRange(0, batchNum);
                        //            }
                        //            else
                        //            {
                        //                items = kvp.Value.Cast<object>().ToList();
                        //                kvp.Value.Clear();
                        //            }
                        //            if (items != null)
                        //            {
                        //                action(items);
                        //            }
                        //            //Task.Factory.StartNew(() =>
                        //            //{
                        //            //    action(items);
                        //            //});
                        //        }
                        //    }
                        //}
                        List<string> keys = new List<string>(keyValuePairDatas.Keys);

                        for (int i = 0; i < keyValuePairDatas.Count; i++)
                        {
                            string key = keys[i];
                            ArrayList value = keyValuePairDatas[key];
                            ArrayList arrayList = new ArrayList();
                            arrayList.AddRange(value);
                            var count = arrayList.Count;
                            if (keyValuePairs.TryGetValue(key, out var action))
                            {
                                if (count == 0)
                                    continue;
                                var json = string.Empty;
                                lock (value.SyncRoot)
                                {
                                    List<object> items;
                                    var batchNum = 100;

                                    if (count >= batchNum)
                                    {
                                        items = arrayList.Cast<object>().Take(batchNum).ToList();
                                        value.RemoveRange(0, batchNum);
                                    }
                                    else
                                    {
                                        items = arrayList.Cast<object>().ToList();
                                        value.RemoveRange(0, count);
                                    }
                                    if (items != null)
                                    {
                                        action(items);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace + "---------------" + ex.Message);
                        throw new Exception("数据合并执行异常:", ex);
                    }
                }
            }).Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataKey"></param>
        /// <param name="data"></param>
        public static void PushData(string dataKey, object data, Action<List<object>> action)
        {
            if (data == null)
                return;
            if (!keyValuePairs.ContainsKey(dataKey))
                keyValuePairs.TryAdd(dataKey, action);

            if (!keyValuePairDatas.ContainsKey(dataKey))
            {
                var list = new ArrayList();
                ArrayList.Synchronized(list);
                list.Add(data);
                keyValuePairDatas.TryAdd(dataKey, list);
                return;
            }
            if (keyValuePairDatas.TryGetValue(dataKey, out var bags))
            {
                bags.Add(data);
            }
        }
    }
}
