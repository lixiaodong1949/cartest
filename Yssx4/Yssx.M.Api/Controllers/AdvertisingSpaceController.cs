﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 广告位
    /// </summary>
    public class AdvertisingSpaceBackController : BaseController
    {
        private IAdvertisingSpaceService _service;

        public AdvertisingSpaceBackController(IAdvertisingSpaceService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取 广告位列表
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<AdvertisingSpaceListResponse>> GetAdvertisingSpaceList(AdvertisingSpaceListDao md)
        {
            return await _service.GetAdvertisingSpaceList(md);
        }

        /// <summary>
        /// 添加 更新 广告位
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateAdvertisingSpace(AdvertisingSpaceDao md)
        {
            return await _service.AddUpdateAdvertisingSpace(md, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 广告位列表-详情
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<AdvertisingSpaceDetailsListResponse>> GetAdvertisingSpaceDetailList(AdvertisingSpaceDetailsListDao md)
        {
            return await _service.GetAdvertisingSpaceDetailList(md, CurrentUser.Id);
        }

        /// <summary>
        ///  添加 更新 广告位-详情
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateAdvertisingSpaceDetails(AdvertisingSpaceDetailsDao md)
        {
            return await _service.AddUpdateAdvertisingSpaceDetails(md, CurrentUser.Id);
        }

        /// <summary>
        ///  删除 广告位-详情
        /// </summary>
        /// <param name="Id">详情主键Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long Id)
        {
            return await _service.Delete(Id, CurrentUser.Id);
        }

        /// <summary>
        ///  状态 广告位-详情
        /// </summary>
        /// <param name="Id">详情主键Id</param>
        /// <param name="State">状态 0生效 1失效</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> State(long Id, int State)
        {
            return await _service.State(Id, State, CurrentUser.Id);
        }
    }
}