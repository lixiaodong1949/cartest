﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 公告
    /// </summary>
    public class NoticeController : BaseController
    {
        private INoticeService _service;
        public NoticeController(INoticeService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEdit(NoticeDto dto)
        {
            return await _service.AddOrEdit(dto);
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<NoticeDto>>> GetList()
        {
            return await _service.GetList();
        }

        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetStatus(long id, Status status)
        {
            return await _service.SetStatus(id, status);
        }
    }
}
