﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 公司信息发布
    /// </summary>
    public class InformationController : BaseController
    {
        private IInformation _service;
        public InformationController(IInformation information)
        {
            _service = information;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AddOrEditInfo(InfoDto dto)
        {
            return await _service.AddOrEditInfo(dto);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<InfoDto>>> GetInformationList()
        {
            return await _service.GetInformationList();
        }
    }
}
