using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 案例财务数据表
    /// </summary>
    [Table(Name = "sx_case_financial_data")]
    public class SxCaseFinancialData : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)", IsNullable = true)]
        public string Name { get; set; }
       
        /// <summary>
        /// 案例Id
        /// </summary>
        [Column(Name = "CaseId", DbType = "bigint(20)")]
        public long CaseId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "FilePath", DbType = "varchar(255)", IsNullable = true)]
        public string FilePath { get; set; }
        
        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

    }
}