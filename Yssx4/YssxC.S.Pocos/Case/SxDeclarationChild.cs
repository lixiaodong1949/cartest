﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 申报表子表
    /// </summary>
    [Table(Name = "sx_declaration_child")]
    public class SxDeclarationChild : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 主表id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }

        /// <summary>
        /// 3:单元格计分  4:自由计分
        /// </summary>
        public int CalculationType { get; set; }

        /// <summary>
        /// 报表内容包含答案
        /// </summary>
        [Column(Name = "FullContent", DbType = "LongText", IsNullable = true)]
        public string FullContent { get; set; }

        /// <summary>
        /// 表头内容
        /// </summary>
        [Column(Name = "HeaderContent", DbType = "LongText", IsNullable = true)]
        public string HeaderContent { get; set; }
    }
}
