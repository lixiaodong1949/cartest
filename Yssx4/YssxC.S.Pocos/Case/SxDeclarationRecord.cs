﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 申报记录表
    /// </summary>
    [Table(Name = "sx_declaration_record")]
    public class SxDeclarationRecord : BizBaseEntity<long>
    {
        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 申报主表Id
        /// </summary>
        public long DeclareParentId { get; set; }

        /// <summary>
        /// 申报状态 0:未填写 1:待申报 2:已申报
        /// </summary>
        public int DeclareStatus { get; set; }
    }
}
