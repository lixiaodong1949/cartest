﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目标记表
    /// </summary>
    [Table(Name = "sx_topic_mark")]
    public class SxTopicMark: BaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
