﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 致用户信
    /// </summary>
    [Table(Name = "sx_letter_to_users")]
    public class SxLetterToUsers : BizBaseEntity<long>
    {
        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
    }
}
