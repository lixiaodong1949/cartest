﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    [Table(Name = "sx_positionManage")]
    public class SxPositionManage : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缩略图标文字
        /// </summary>
        public string ThumbnailText { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 岗位图标地址
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// 岗位类型
        /// </summary>
        public PositionType PositionType { get; set; } = PositionType.composite;


    }
}
