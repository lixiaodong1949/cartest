﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 分录题答案（结转损益题 分开结转时保存该数据 ）
    /// </summary>
    [Table(Name = "sx_certificate_answer")]
    public class SxCertificateAnswer : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 答案 内容（json等）
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }
        /// <summary>
        ///摘要信息
        /// </summary>
        [Column(Name = "SummaryInfos", DbType = "LongText", IsNullable = true)]
        public string SummaryInfos { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
