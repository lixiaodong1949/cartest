﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 分录题凭证信息
    /// </summary>
    [Table(Name = "sx_certificate_topic")]
    public class SxCertificateTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 发生日期
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// 题干
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 凭证字
        /// </summary>
        [Column(DbType = "int")]
        public CertificateWord CertificateWord { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }

        /// <summary>
        /// 单据张数
        /// </summary>
        public int InvoicesNum { get; set; }

        /// <summary>
        /// 题库Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 图片文件Id
        /// </summary>
        public string ImageIds { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 0:核心，1:B模块
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryCalculationType CalculationScoreType { get; set; }

        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }

        /// <summary>
        /// 摘要是否计分
        /// </summary>
        public bool IsSummaryCalculation { get; set; }
        /// <summary>
        /// 摘要信息
        /// </summary>
        [Column(Name = "SummaryInfos", DbType = "LongText", IsNullable = true)]
        public string SummaryInfos { get; set; }
    }
}
