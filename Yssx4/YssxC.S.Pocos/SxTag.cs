﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 标签
    /// </summary>
    [Table(Name = "sx_tag")]
    public class SxTag : BizBaseEntity<long>
    {
        /// <summary>
        /// 标签名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 启用或禁用标签
        /// </summary>
        public bool IsActive { get; set; }
    }
}
