﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏资源设置
    /// </summary>
    [Table(Name = "sx_game_resource_config")]
    public class SxGameResourceConfig
    {
        public long Id { get; set; }
        /// <summary>
        /// 资源key
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public GameResourceKey GameResourceKey { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string KeyName { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsDelete { get; set; } = CommonConstants.IsNotDelete;

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

    }
}
