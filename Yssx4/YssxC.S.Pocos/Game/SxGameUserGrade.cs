﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏关卡用户闯关作答记录表
    /// </summary>
    [Table(Name = "sx_game_user_grade")]
    public class SxGameUserGrade : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 关卡Id
        /// </summary>
        public long GameLevelId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "GameLevelSort", DbType = "int(11)")]
        public int GameLevelSort { get; set; } = 0;

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        [Column(Name = "Point")]
        public int Point { get; set; } = 0;

        /// <summary>
        /// 得分
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,2)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        [Column(Name = "TotalScore", DbType = "decimal(10,2)")]
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 用时 单位：s
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(Name = "Status", MapType = typeof(int), DbType = "int(11)")]
        public StudentExamStatus Status { get; set; } = StudentExamStatus.Started;

        ///// <summary>
        ///// 用户当前在做题目节点
        ///// </summary>
        //public GameTopicNode CurrentTopicNode { get; set; }

        ///// <summary>
        ///// 最新作答记录
        ///// </summary>
        //public string GameLevelTopicNodes { get; set; }

        [Column(Name = "IsNew")]
        public bool IsNew { get; set; } = true;
        /// <summary>
        /// 账套创建状态 0:不需要创建 1:未创建 2:已创建
        /// </summary>
        [Column(Name = "CreateSetBookStatus", DbType = "int", MapType = typeof(int))]
        public CreateSetBookStatus CreateSetBookStatus { get; set; } = CreateSetBookStatus.NotCreate;

        /// <summary>
        /// 关卡总积分
        /// </summary>
        [Column(Name = "TotalPoint", DbType = "int(11)")]
        public int TotalPoint { get; set; } = 0;

        /// <summary>
        /// 关卡建议总用时(单位：s)
        /// </summary>
        public int TotalMinutes { get; set; }
    }
}
