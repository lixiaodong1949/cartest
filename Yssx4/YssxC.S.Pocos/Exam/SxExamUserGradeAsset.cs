﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 用户作答信息资产表
    /// </summary>
    [Table(Name = "sx_exam_user_grade_asset")]
    public class SxExamUserGradeAsset : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 银行账户Id
        /// </summary>
        public long BankAccountId { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 支行
        /// </summary>
        public string SubBranch { get; set; }

        /// <summary>
        /// 银行账户
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 账户初始额度
        /// </summary>
        public decimal InitialAccountAmount { get; set; }

        /// <summary>
        /// 账户总额
        /// </summary>
        public decimal TotalAccount { get; set; }

        /// <summary>
        /// 账户已用金额
        /// </summary>
        public decimal AccountUsedAmount { get; set; }

        /// <summary>
        /// 账户余额
        /// </summary>
        public decimal AccountBalance { get; set; }
    }
}
