﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 申报表子表模板
    /// </summary>
    [Table(Name = "sx_declaration_template_child")]
    public class SxDeclarationTemplateChild : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 申报表主表模板id
        /// </summary>
        public long DeclarationTypeTemplateId { get; set; }

        /// <summary>
        /// 表头内容
        /// </summary>
        [Column(Name = "HeaderContent", DbType = "LongText", IsNullable = true)]
        public string HeaderContent { get; set; }
    }
}
