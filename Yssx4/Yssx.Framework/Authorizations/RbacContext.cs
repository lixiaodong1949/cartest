﻿using System;
using System.Linq;

namespace Yssx.Framework.Authorizations
{
    /// <summary>
    /// 
    /// </summary>
    public static class RbacContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <returns></returns>
        public static bool Assert(UserTicket user, PermissionEnum permission)
        {
            if (user == null)
                throw new Exception($"用户未登陆！");

            var permissions = user.Roles.SelectMany(m => m.Permissions.Select(p => p.PermissionId)).Distinct();
            if (permissions.Any(p => p == (long)permission))
                return true;
            throw new Exception($"用户:{user.Name} 验证权限[{permission}]失败！");

        }

    }
}
