﻿using System;
using System.Collections.Generic;

namespace Yssx.Framework.Authorizations
{
    /// <summary>
    /// 
    /// </summary>
    public class UserTicket
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>

        public string RealName { get; set; }

        ///// <summary>
        ///// 客户端id
        ///// </summary>
        //public string ClientId { get; set; }
        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 用户类型 0 管理员 1 学生2 教师 3 教务 4 专家 5 开发 6 专业组 7 普通用户 8 审核员
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<RbacRole> Roles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<RbacMenu> Menus { get; set; }
        /// <summary>
        /// false首次登录，true非首次登录(默认true 非首次登陆)
        /// </summary>
        public bool FirstLanding { get; set; } = true;
        /// <summary>
        /// 电话号码
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }
        /// <summary>
        /// 学校名字
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 学校类型 -1:无效值 0:中专 1:大专 2:本科
        /// </summary>
        public int SchoolType { get; set; }

        /// <summary>
        /// 登录方式 0:PC 1:App 2.小程序
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 是否已激活 false:未激活 true:已激活
        /// </summary>
        public string IsVip { get; set; }
    }
}
