﻿using System;

namespace Yssx.Framework.Authorizations
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PermissionAttribute:Attribute
    {
        public PermissionEnum Permission { get; set; }
        public PermissionAttribute(PermissionEnum permission)
        {
            Permission = permission;
        }
    }
}
