﻿namespace Yssx.Framework.Authorizations
{
    public class RbacPermission
    {
        /// <summary>
        /// 
        /// </summary>
        public long PermissionId { get; set; }
        /// <summary>
        ///  权限项 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 元素的页面路径
        /// </summary>
        public string PathName { get; set; }

        /// <summary>
        /// 元素Id
        /// </summary>
        public string ControlId { get; set; }
    }
}
