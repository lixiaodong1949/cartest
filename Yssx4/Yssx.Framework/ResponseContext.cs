﻿using Yssx.Framework.Entity;

namespace Yssx.Framework
{
    /// <summary>
    /// 返回上下文
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseContext<T> : BaseResponse
    {
        public ResponseContext() { }
        public ResponseContext(int code, string msg)
        {
            Code = code;
            Msg = msg;
        }
        public ResponseContext(int code, string msg, T data)
        {
            Code = code;
            Msg = msg;
            Data = data;
        }
        public ResponseContext(T data)
        {
            Code = CommonConstants.SuccessCode;
            Msg = string.Empty;
            Data = data;
        }

        /// <summary>
        /// 返回数据
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 设置成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        public virtual void SetSuccess(T data, string msg = null)
        {
            Data = data;
            SetSuccess(msg);
        }

        /// <summary>
        /// 设置失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="data"></param>
        /// <param name="code"></param>
        public virtual void SetError(string msg, T data, int code = CommonConstants.BadRequest)
        {
            Data = data;
            SetError(msg, code);
        }
    }



    public interface IResponseContext
    {

    }
}
