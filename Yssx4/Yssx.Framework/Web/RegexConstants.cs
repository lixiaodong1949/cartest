﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Web
{
    /// <summary>
    /// 正则表达式规则
    /// </summary>
    public class RegexConstants
    {
        /// <summary>
        /// 身份证验证
        /// </summary>
        //public const string IdCardNumber = @"^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$";
        public const string IdCardNumberSimple = @"(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)";
    }
}
