﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.AnswerCompare.EntitysV2
{
    /// <summary>
    /// 
    /// </summary>
    public class Answer
    {
        /// <summary>
        /// 
        /// </summary>
        public AnswerRow HeaderCtrlRow { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<AnswerRow> BodyCtrlRows { get; set; }

        /// <summary>
        /// 数据列（按列记分）
        /// </summary>
        public List<AnswerRow> BodyCtrlColumns { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AnswerRow ItemCtrlRow { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AnswerRow FooterCtrlRow { get; set; }


        /// <summary>
        /// 盖章
        /// </summary>
        public AnswerRow SealCtrlRow { get; set; }
    }
}
