﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    public class WeiDianHttpClient
    {
        private readonly HttpClient client;
        private readonly string api;

        public WeiDianHttpClient()
        {
            client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(20);
            api = "https://api.vdian.com/api?param={0}&public={1}";
        }

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <param name="appkey"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public GetTokenResponse GetAccessToken(string appkey, string secret)
        {
            var url = $"https://oauth.open.weidian.com/token?grant_type=client_credential&appkey={appkey}&secret={secret}";
            var result = client.GetStringAsync(url).Result;
            GetTokenResponse response = JsonConvert.DeserializeObject<GetTokenResponse>(result);
            return response;
        }

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="request"></param>
        /// <param name="accessToken">访问凭证</param>
        /// <returns></returns>
        public GetOrderListResponse GetOrderList(GetOrderListRequest request, string accessToken)
        {
            CommonArgs args = new CommonArgs()
            {
                Method = "vdian.order.list.get",
                AccessToken = accessToken,
                Version = "1.2"
            };
            var url = string.Format(api, JsonConvert.SerializeObject(request), JsonConvert.SerializeObject(args));
            var result = client.GetStringAsync(url).Result;
            GetOrderListResponse response = JsonConvert.DeserializeObject<GetOrderListResponse>(result);
            return response;
        }

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="request"></param>
        /// <param name="accessToken">访问凭证</param>
        /// <returns></returns>
        public GetOrderResponse GetOrder(GetOrderRequest request, string accessToken)
        {
            CommonArgs args = new CommonArgs()
            {
                Method = "vdian.order.get",
                AccessToken = accessToken,
                Version = "1.0"
            };
            var url = string.Format(api, JsonConvert.SerializeObject(request), JsonConvert.SerializeObject(args));
            var result = client.GetStringAsync(url).Result;
            GetOrderResponse response = JsonConvert.DeserializeObject<GetOrderResponse>(result);
            return response;
        }
    }
}
