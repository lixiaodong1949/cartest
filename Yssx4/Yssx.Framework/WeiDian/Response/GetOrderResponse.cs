﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    public class GetOrderResponse : ResponseBase
    {
        /// <summary>
        /// 响应结果
        /// </summary>
        [JsonProperty(PropertyName = "result")]
        public Order Result { get; set; }
    }

    public class Order
    {
        /// <summary>
        /// 买家信息
        /// </summary>
        [JsonProperty(PropertyName = "buyer_info")]
        public BuyerInfo BuyerInfo { get; set; }

        /// <summary>
        /// 订单ID
        /// </summary>
        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }

        /// <summary>
        /// 卖家id
        /// </summary>
        [JsonProperty(PropertyName = "seller_id")]
        public string SellerId { get; set; }

        /// <summary>
        /// 分销商手机号
        /// </summary>
        [JsonProperty(PropertyName = "f_phone")]
        public string Fphone { get; set; }

        /// <summary>
        /// 订单是否可关闭
        /// 0（表示不可关闭） 1（表示货到付款订单，可关闭）
        /// </summary>
        [JsonProperty(PropertyName = "is_close")]
        public int is_close { get; set; }

        /// <summary>
        /// 退货单号
        /// </summary>
        [JsonProperty(PropertyName = "return_code")]
        public string ReturnCode { get; set; }

        /// <summary>
        /// 商品数组
        /// </summary>
        [JsonProperty(PropertyName = "items")]
        public ProductInfo[] Items { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// 下单时间
        /// </summary>
        [JsonProperty(PropertyName = "add_time")]
        public DateTime? AddTime { get; set; }

        [JsonProperty(PropertyName = "express_fee_num")]
        public string ExpressFeeNum { get; set; }

        /// <summary>
        /// 买家手机号
        /// </summary>
        [JsonProperty(PropertyName = "user_phone")]
        public string UserPhone { get; set; }

        /// <summary>
        /// 商品总价格，不包含运费
        /// </summary>
        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }

        /// <summary>
        /// 买家备注
        /// </summary>
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        /// <summary>
        /// 卖家手机号
        /// </summary>
        [JsonProperty(PropertyName = "seller_phone")]
        public string SellerPhone { get; set; }

        /// <summary>
        /// 订单状态说明
        /// </summary>
        [JsonProperty(PropertyName = "status2")]
        public string Status2 { get; set; }

        /// <summary>
        /// 分成推广佣金
        /// </summary>
        [JsonProperty(PropertyName = "total_fee")]
        public string TotalFee { get; set; }

        /// <summary>
        /// 卖家微店名称
        /// </summary>
        [JsonProperty(PropertyName = "seller_name")]
        public string SellerName { get; set; }

        /// <summary>
        /// 订单类型描述
        /// </summary>
        [JsonProperty(PropertyName = "order_type_des")]
        public string OrderTypeDesc { get; set; }

        /// <summary>
        /// 是否投诉
        /// </summary>
        [JsonProperty(PropertyName = "argue_flag")]
        public int ArgueFlag { get; set; }

        /// <summary>
        /// 分销商店铺名称
        /// </summary>
        [JsonProperty(PropertyName = " f_shop_name")]
        public string FshopName { get; set; }

        /// <summary>
        /// 是否可以改价
        /// 0（表示不可改价）1（表示可以改价）
        /// </summary>
        [JsonProperty(PropertyName = "modify_price_enable")]
        public string ModifyPriceEnable { get; set; }

        /// <summary>
        /// 推广信息
        /// </summary>
        [JsonProperty(PropertyName = "sk")]
        public string Sk { get; set; }

        /// <summary>
        /// 分销商获取的分成金额
        /// </summary>
        [JsonProperty(PropertyName = "fx_fee_value")]
        public String FxFeeValue { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        [JsonProperty(PropertyName = "express_no")]
        public string ExpressNo { get; set; }

        /// <summary>
        /// 发货时间
        /// </summary>
        [JsonProperty(PropertyName = "send_time")]
        public DateTime? SendTime { get; set; }

        /// <summary>
        /// 商品总件数
        /// </summary>
        [JsonProperty(PropertyName = "quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// 快递费用
        /// </summary>
        [JsonProperty(PropertyName = "express_fee")]
        public string ExpressFee { get; set; }

        /// <summary>
        /// 分销商ID
        /// </summary>
        [JsonProperty(PropertyName = "f_seller_id")]
        public string FsellerId { get; set; }

        /// <summary>
        /// 担保交易过期时间
        /// </summary>
        [JsonProperty(PropertyName = "confirm_expire")]
        public string ComfirmExpire { get; set; }

        /// <summary>
        /// 订单类型编号，1为货到付款，2为直接交易，3为担保交易
        /// </summary>
        [JsonProperty(PropertyName = "order_type")]
        public string OrderType { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        [JsonProperty(PropertyName = "pay_time")]
        public DateTime? PayTime { get; set; }

        /// <summary>
        /// 订单状态详细描述
        /// </summary>
        [JsonProperty(PropertyName = "status_desc")]
        public string StatusDesc { get; set; }

        /// <summary>
        /// 订单备注（卖家）
        /// </summary>
        [JsonProperty(PropertyName = "express_note")]
        public string ExpressNote { get; set; }

        /// <summary>
        /// 快递公司名称
        /// </summary>
        [JsonProperty(PropertyName = "express")]
        public string Express { get; set; }

        /// <summary>
        /// 订单原价格，包括运费 （此字段用于记录订单被创建时的价格,包含运费，减去优惠后的价格）
        /// </summary>
        [JsonProperty(PropertyName = "original_total_price")]
        public string OriginalTotalPrice { get; set; }

        /// <summary>
        /// 订单总价 包含运费
        /// </summary>
        [JsonProperty(PropertyName = "total")]
        public string Total { get; set; }

        /// <summary>
        /// 快递公司编号
        /// </summary>
        [JsonProperty(PropertyName = "express_type")]
        public string ExpressType { get; set; }

        /// <summary>
        /// 买家备注微信号
        /// </summary>
        [JsonProperty(PropertyName = "weixin")]
        public string WeiXin { get; set; }

        /// <summary>
        /// 只在待发货列表下生效，用于判断微团购订单是否已成团。其中 1已成团，0 未成团，-1 无意义新增此字段
        /// </summary>
        [JsonProperty(PropertyName = "group_status")]
        public int GroupStatus { get; set; }

        /// <summary>
        /// 优惠信息列表
        /// </summary>
        [JsonProperty(PropertyName = "discount_list")]
        public Discount[] DiscountList { get; set; }

        /// <summary>
        /// 订单退款相关状态
        /// </summary>
        [JsonProperty(PropertyName = "refund_status_ori")]
        public string RefundStatusOri { get; set; }

        /// <summary>
        /// 订单状态
        /// 10 待付款
        /// 20 已付款，待发货
        /// 21 部分付款
        /// 30 已发货
        /// 31 部分发货
        /// 40 已确认收货
        /// 50 已完成
        /// 60 已关闭
        /// </summary>
        [JsonProperty(PropertyName = "status_ori")]
        public string StatusOri { get; set; }

        /// <summary>
        /// 满赠优惠的赠品
        /// </summary>
        [JsonProperty(PropertyName = "full_gift_items")]
        public Gift[] FullGiftItems { get; set; }
    }

    public class ProductInfo
    {
        /// <summary>
        /// 商品id
        /// </summary>
        [JsonProperty(PropertyName = "item_id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        [JsonProperty(PropertyName = "item_name")]
        public string ItemName { get; set; }

        /// <summary>
        /// 商品对应总价，total_price=price*quantity
        /// </summary>
        [JsonProperty(PropertyName = "total_price")]
        public string TotalPrice { get; set; }

        /// <summary>
        /// 商品型号名称
        /// </summary>
        [JsonProperty(PropertyName = "sku_title")]
        public string SkuTitle { get; set; }

        /// <summary>
        /// 商品型号编码
        /// </summary>
        [JsonProperty(PropertyName = "sku_merchant_code")]
        public string SkuMerchantCode { get; set; }

        /// <summary>
        /// 分销分成比例
        /// </summary>
        [JsonProperty(PropertyName = "fx_fee_rate")]
        public string FxFeeRate { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>
        [JsonProperty(PropertyName = "merchant_code")]
        public string MerchantCode { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        [JsonProperty(PropertyName = "quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// 商品URL
        /// </summary>
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        /// <summary>
        /// 商品图URL
        /// </summary>
        [JsonProperty(PropertyName = "img")]
        public string Img { get; set; }

        /// <summary>
        /// 商品型号ID
        /// </summary>
        [JsonProperty(PropertyName = "sku_id")]
        public string SkuId { get; set; }

        /// <summary>
        /// 退款信息
        /// </summary>
        [JsonProperty(PropertyName = "refund_info")]
        public RefundInfo RefundFnfo { get; set; }

        /// <summary>
        /// 商品发货状态：待发货 | 已发货 ｜ ""（不在页面展示）
        /// </summary>
        [JsonProperty(PropertyName = "deliver_status_desc")]
        public string DeliverStatusDesc { get; set; }

        /// <summary>
        /// 发货批次编号，代表一次发货（注：不是物流单号）
        /// </summary>
        [JsonProperty(PropertyName = "deliver_id")]
        public string DeliverId { get; set; }

        /// <summary>
        /// 是否发货 1是0非
        /// </summary>
        [JsonProperty(PropertyName = "is_delivered")]
        public int IsDelivered { get; set; }

        /// <summary>
        /// 商品是否可以发货 1是0非
        /// </summary>
        [JsonProperty(PropertyName = "can_deliver")]
        public int CanDeliver { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [JsonProperty(PropertyName = "item_desc")]
        public string Desc { get; set; }
    }

    /// <summary>
    /// 退款信息
    /// </summary>
    public class RefundInfo
    {
        /// <summary>
        /// 标识这个sku是否可发起商品退款
        /// </summary>
        [JsonProperty(PropertyName = "can_refund")]
        public string CanRefund { get; set; }

        /// <summary>
        /// 状态描述
        /// </summary>
        [JsonProperty(PropertyName = "refund_status_desc")]
        public String RefundStatusDesc { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [JsonProperty(PropertyName = "item_id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 退款金额
        /// </summary>
        [JsonProperty(PropertyName = "refund_fee")]
        public string RefundFee { get; set; }

        /// <summary>
        /// 商品退款运费金额
        /// </summary>
        [JsonProperty(PropertyName = "refund_express_fee")]
        public string RefundExpressFee { get; set; }

        /// <summary>
        /// 退款商品金额
        /// </summary>
        [JsonProperty(PropertyName = "refund_item_fee")]
        public string RefundItemFee { get; set; }

        /// <summary>
        /// 退款状态
        /// 0 未发起退款
        /// 1 申请退款或退款
        /// 3 拒绝退款
        /// 4 退货流程同意退货
        /// 5 退货流程拒绝退货
        /// 6 退货流程已提交退货物流信息
        /// 9 退款取消
        /// 10 退款完成（同意退款或收到退货后同意退款，退款流程完成，资金流程开始）
        /// </summary>
        [JsonProperty(PropertyName = "refund_status")]
        public string RefundStatus { get; set; }

        /// <summary>
        /// 状态描述
        /// </summary>
        [JsonProperty(PropertyName = "refund_status_str")]
        public string RefundStatusStr { get; set; }

        /// <summary>
        /// 退款唯一标识
        /// </summary>
        [JsonProperty(PropertyName = "refund_no")]
        public string RefundNo { get; set; }

        /// <summary>
        /// sku ID
        /// </summary>
        [JsonProperty(PropertyName = "item_sku_id")]
        public string ItemSkuId { get; set; }

        /// <summary>
        /// 退款类型，1表示退款，2表示退货退款
        /// </summary>
        [JsonProperty(PropertyName = "refund_kind")]
        public string RefundKind { get; set; }
    }

    /// <summary>
    /// 优惠信息
    /// </summary>
    public class Discount
    {
        /// <summary>
        /// 优惠类型信息
        /// </summary>
        [JsonProperty(PropertyName = "discount_info")]
        public string DiscountInfo { get; set; }

        /// <summary>
        /// 100(私密优惠)
        /// 101(店铺满包邮)
        /// 102(满减)
        /// 70(平台优惠目前跨店券和平台券用这个)
        /// 71(微店分销优惠券类型)
        /// 72(平台补贴)
        /// 103(优惠券)
        /// 104(代金卡)
        /// 105(微积分)
        /// 106(代金券)
        /// 200(限时打折)
        /// 201(微团购)
        /// 206(砍价)
        /// 202(评测中心)
        /// 203(秒杀)
        /// 207(官方限时折扣)
        /// 50(微币积分优惠)
        /// 51(买家版签到积分)
        /// 204(等级会员价)
        /// 205(微中心会员折扣)
        /// 205(一元夺宝)
        /// 53(多品满减)
        /// 54(组合优惠)
        /// 55(优惠码)
        /// 56(优惠套餐)
        /// 57(满金额包邮)
        /// 58(满件包邮)
        /// 59(输入法改价优惠)
        /// 60(满送)
        /// 61(预售)
        /// 62(群团购)
        /// 63(新客专享)
        /// 64(供货商满减)
        /// 65(黑卡免单优惠)
        /// 66(刷脸券)
        /// 67(新人价)
        /// 68(店铺红包)
        /// 69(回头客群专享)
        /// 300(平台会员折扣)
        /// 301(定向用户专享优惠)
        /// 302(付费会员价)
        /// 304(积分五折购)
        /// 303(团长免单)
        /// </summary>
        [JsonProperty(PropertyName = "discount_type")]
        public string DiscountType { get; set; }

        /// <summary>
        /// 优惠价格描述
        /// </summary>
        [JsonProperty(PropertyName = "discount_price")]
        public string DiscountPrice { get; set; }
    }

    public class Gift
    {
        /// <summary>
        /// 商品名称
        /// </summary>
        [JsonProperty(PropertyName = "item_name")]
        public string ItemName { get; set; }

        /// <summary>
        /// 商品总件数
        /// </summary>
        [JsonProperty(PropertyName = "quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [JsonProperty(PropertyName = "item_id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 商品链接
        /// </summary>
        [JsonProperty(PropertyName = "item_url")]
        public string ItemUrl { get; set; }

        /// <summary>
        /// head_img
        /// </summary>
        [JsonProperty(PropertyName = "head_img")]
        public string HeadImg { get; set; }
    }
}
