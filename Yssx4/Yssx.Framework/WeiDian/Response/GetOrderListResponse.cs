﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    /// <summary>
    /// 订单列表请求响应对象
    /// </summary>
    public class GetOrderListResponse : ResponseBase
    {
        /// <summary>
        /// 响应结果
        /// </summary>
        [JsonProperty(PropertyName = "result")]
        public OrderListResponse Result { get; set; }
    }

    public class OrderListResponse
    {
        /// <summary>
        /// 列表中订单条数
        /// </summary>
        [JsonProperty(PropertyName = "order_num")]
        public int OrderNum { get; set; }

        /// <summary>
        /// 查询到的订单总条数
        /// </summary>
        [JsonProperty(PropertyName = "total_num")]
        public int TotalNum { get; set; }

        /// <summary>
        /// 订单列表
        /// </summary>
        [JsonProperty(PropertyName = "orders")]
        public OrderListItem[] Orders { get; set; }
    }

    /// <summary>
    /// 订单列表项
    /// </summary>
    public class OrderListItem
    {
        /// <summary>
        /// 订单总价
        /// </summary>
        [JsonProperty(PropertyName = "total")]
        public string Total { get; set; }

        /// <summary>
        /// 只在待发货列表下生效，用于判断微团购订单是否已成团。其中 1已成团，0 未成团，-1 无意义新增此字段
        /// </summary>
        [JsonProperty(PropertyName = "group_status")]
        public int GroupStatus { get; set; }

        /// <summary>
        /// 订单更新时间
        /// </summary>
        [JsonProperty(PropertyName = "update_time")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// is_wei_order 参数 不传 或者 传=0 时 ，能返回 普通订单、微中心订单、品牌供货订单；is_wei_order 参数=1 时，返回 微中心订单 、品牌供货订单；is_wei_order 参数=2 时，返回 普通订单。
        /// </summary>
        [JsonProperty(PropertyName = "is_wei_order")]
        public int IsWeiOrder { get; set; }

        /// <summary>
        /// 订单ID
        /// </summary>
        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }

        /// <summary>
        /// 买家备注
        /// </summary>
        [JsonProperty(PropertyName = "buyer_note")]
        public string BuyerNote { get; set; }

        /// <summary>
        /// 下单时间
        /// </summary>
        [JsonProperty(PropertyName = "time")]
        public DateTime Time { get; set; }

        /// <summary>
        /// 分销商手机号
        /// </summary>
        [JsonProperty(PropertyName = "f_phone")]
        public string Fphone { get; set; }

        /// <summary>
        /// 卖家备注
        /// </summary>
        [JsonProperty(PropertyName = "seller_note")]
        public string SellerNote { get; set; }

        /// <summary>
        /// 分销商ID
        /// </summary>
        [JsonProperty(PropertyName = "f_seller_id")]
        public string FsellerId { get; set; }

        /// <summary>
        /// 快递公司
        /// </summary>
        [JsonProperty(PropertyName = "express_type")]
        public string ExpressType { get; set; }

        /// <summary>
        /// 订单状态编号
        /// 0：下单 1：未付款 2：已付款 3：已发货 7：退款 8：订单关闭
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// 商品图URL
        /// </summary>
        [JsonProperty(PropertyName = "img")]
        public string Img { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        [JsonProperty(PropertyName = "express_no")]
        public string ExpressNo { get; set; }

        /// <summary>
        /// 快递费用
        /// </summary>
        [JsonProperty(PropertyName = "express_fee")]
        public string ExpressFee { get; set; }

        /// <summary>
        /// 买家信息
        /// </summary>
        [JsonProperty(PropertyName = "buyer_info")]
        public BuyerInfo BuyerInfo { get; set; }
    }

    /// <summary>
    /// 买家信息
    /// </summary>
    public class BuyerInfo
    {
        /// <summary>
        /// 收货人的街道
        /// </summary>
        [JsonProperty(PropertyName = "self_address")]
        public string SelfAddress { get; set; }

        /// <summary>
        /// 收货人所在市
        /// </summary>
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        /// <summary>
        /// 买家姓名
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        /// <summary>
        /// 收货人所在省
        /// </summary>
        [JsonProperty(PropertyName = "province")]
        public string Province { get; set; }

        /// <summary>
        /// 收货人所在区
        /// </summary>
        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        [JsonProperty(PropertyName = "post")]
        public string Post { get; set; }

        /// <summary>
        /// 海带商品需要 身份证号
        /// </summary>
        [JsonProperty(PropertyName = "idCardNo")]
        public string IdCardNo { get; set; }
    }
}
