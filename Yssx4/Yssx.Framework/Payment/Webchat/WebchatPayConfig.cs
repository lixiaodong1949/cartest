﻿

using Microsoft.Extensions.Configuration;
using Yssx.Framework.Logger;

namespace Yssx.Framework.Payment.Webchat
{
    /// <summary>
    /// 微信支付配置参数
    /// </summary>
    public class WebchatPayConfig : IConfig
    {
        public static IConfiguration Configuration;

        /// <summary>
        /// 获取绑定支付的APPID（必须配置 - PC端支付）
        /// </summary>
        /// <returns></returns>
        public string GetAppID()
        {
            var appid = Configuration["Payment:Webchat:app_id"];
            return appid;
        }

        /// <summary>
        /// 获取绑定支付的APPID（必须配置 - 移动端支付）
        /// </summary>
        /// <returns></returns>
        public string GetMobileAppID()
        {
            var appid = Configuration["Payment:Webchat:mobile_app_id"];
            return appid;
        }

        /// <summary>
        /// 获取绑定支付的APPID（必须配置 - 3D移动端支付）
        /// </summary>
        /// <returns></returns>
        public string GetMobile3dAppID()
        {
            var appid = Configuration["Payment:Webchat:mobile_3d_app_id"];
            return appid;
        }

        /// <summary>
        /// 获取绑定支付的APPID（必须配置 - 小程序支付）
        /// </summary>
        /// <returns></returns>
        public string GetMiniProgramsAppID()
        {
            var appid = Configuration["Payment:Webchat:mini_app_id"];
            return appid;
        }

        /// <summary>
        /// 获取商户号（必须配置）
        /// </summary>
        /// <returns></returns>
        public string GetMchID()
        {
            var mchid = Configuration["Payment:Webchat:mch_id"];
            return mchid;
        }

        /// <summary>
        /// 获取商户支付密钥
        /// </summary>
        /// <returns></returns>
        public string GetKey()
        {
            var key = Configuration["Payment:Webchat:key"];
            return key;
        }

        /// <summary>
        /// 支付网关
        /// </summary>
        /// <returns></returns>
        public string GetGateway()
        {
            return Configuration["Payment:Webchat:gateway"];
        }

        /// <summary>
        /// 分账支持
        /// </summary>
        /// <returns></returns>
        public string GetProfitSharing()
        {
            return Configuration["Payment:Webchat:profit_sharing"];
        }

        /// <summary>
        /// 分账网关
        /// </summary>
        /// <returns></returns>
        public string GetProfitSharingGetway()
        {
            return Configuration["Payment:Webchat:profit_sharing_getway"];
        }

        /// <summary>
        /// 获取公众帐号secert（仅JSAPI支付的时候需要配置）
        /// </summary>
        /// <returns></returns>
        public string GetAppSecret()
        {
            return "";
        }



        //=======【证书路径设置】===================================== 
        /* 证书路径,注意应该填写绝对路径（仅退款、撤销订单时需要）
         * 1.证书文件不能放在web服务器虚拟目录，应放在有访问权限控制的目录中，防止被他人下载；
         * 2.建议将证书文件名改为复杂且不容易猜测的文件
         * 3.商户服务器要做好病毒和木马防护工作，不被非法侵入者窃取证书文件。
        */
        public string GetSSlCertPath()
        {
            return "";
        }
        public string GetSSlCertPassword()
        {
            return "";
        }

        /// <summary>
        /// 获取支付结果通知回调url，用于商户接收支付结果
        /// </summary>
        /// <returns></returns>
        public string GetNotifyUrl()
        {
            return Configuration["Payment:Webchat:notify_url"];
        }

        public string GetWapReturnUrl()
        {
            return Configuration["Payment:Webchat:wap_return_url"];
        }

        //=======【商户系统后台机器IP】===================================== 
        /* 此参数可手动配置也可在程序中自动获取
        */
        public string GetIp()
        {
            return "0.0.0.0";
        }


        //=======【代理服务器设置】===================================
        /* 默认IP和端口号分别为0.0.0.0和0，此时不开启代理（如有需要才设置）
        */
        public string GetProxyUrl()
        {
            return "";
        }


        //=======【上报信息配置】===================================
        /* 测速上报等级，0.关闭上报; 1.仅错误时上报; 2.全量上报
        */
        public int GetReportLevel()
        {
            return 1;
        }


        //=======【日志级别】===================================
        /* 日志等级，0.不输出日志；1.只输出错误信息; 2.输出错误和正常信息; 3.输出错误信息、正常信息和调试信息
        */
        public int GetLogLevel()
        {
            return 1;
        }
    }
}
