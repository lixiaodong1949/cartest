﻿using System.Xml;

namespace Yssx.Framework.Payment.Webchat
{
    public class SafeXmlDocument : XmlDocument
    {
        public SafeXmlDocument()
        {
            this.XmlResolver = null;
        }
    }
}
