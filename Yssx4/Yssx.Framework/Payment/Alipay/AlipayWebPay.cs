﻿using Aop.Api;
using Aop.Api.Domain;
using Aop.Api.Request;
using Aop.Api.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Payment.Alipay
{
    /// <summary>
    /// 支付宝
    /// </summary>
    public class AlipayWebPay
    {
        private Order _order;

        public AlipayWebPay(Order order)
        {
            _order = order;
        }

        /// <summary>
        /// H5支付
        /// </summary>
        /// <returns></returns>
        public PaymentResult PayWap()
        {
            DefaultAopClient client = new DefaultAopClient(AlipayConfig.gatewayUrl, AlipayConfig.appId, AlipayConfig.privateKey, "json", "1.0", AlipayConfig.signType, AlipayConfig.publicKey, AlipayConfig.charset, false);

            // 组装业务参数model
            AlipayTradePagePayModel model = new AlipayTradePagePayModel();
            model.Body = _order.Body;
            model.Subject = _order.Name;
            model.TotalAmount = _order.Amount.ToString();
            model.OutTradeNo = _order.OrderNo;
            //model.ProductCode = _order.ProductCode;
            model.ProductCode = "QUICK_WAP_PAY";

            AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
            // 设置同步回调地址
            request.SetReturnUrl(AlipayConfig.wapReturnUrl);
            // 设置异步通知接收地址
            request.SetNotifyUrl(AlipayConfig.notifyUrl);
            // 将业务model载入到request
            request.SetBizModel(model);

            try
            {
                PaymentResult result = new PaymentResult();
                result.Content = client.pageExecute(request).Body;
                result.Code = "SUCCESS";
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 面对面二维码支付
        /// </summary>
        /// <returns></returns>
        public PaymentResult Pay()
        {
            DefaultAopClient client = new DefaultAopClient(AlipayConfig.gatewayUrl, AlipayConfig.appId, AlipayConfig.privateKey, "json", "1.0", AlipayConfig.signType, AlipayConfig.publicKey, AlipayConfig.charset, false);

            // 组装业务参数model


            AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
            model.Body = _order.Body;
            model.Subject = _order.Name;
            model.TotalAmount = _order.Amount.ToString();
            model.OutTradeNo = _order.OrderNo;
            //model.ProductCode = _order.ProductCode;
            model.ProductCode = "FACE_TO_FACE_PAYMENT";

            AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
            // 设置同步回调地址
            request.SetReturnUrl(AlipayConfig.returnUrl);
            // 设置异步通知接收地址
            request.SetNotifyUrl(AlipayConfig.notifyUrl);
            // 将业务model载入到request
            request.SetBizModel(model);

            AlipayTradePrecreateResponse response = null;
            try
            {
                response = client.Execute(request);
                PaymentResult result = new PaymentResult();
                result.Code = "SUCCESS";
                result.Msg = response.Msg;
                result.Content = response.QrCode;
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        /// <summary>
        /// APP支付
        /// </summary>
        /// <returns></returns>
        public PaymentResult PayApp(string appId, string notifyUrl, string privateKey, string publicKey)
        {
            DefaultAopClient client = new DefaultAopClient(AlipayConfig.gatewayUrl, appId, privateKey, "json", "1.0", AlipayConfig.signType, publicKey, AlipayConfig.charset, false);

            // 组装业务参数model
            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.Body = _order.Body;
            model.Subject = _order.Name;
            model.TotalAmount = _order.Amount.ToString();
            model.OutTradeNo = _order.OrderNo;
            model.TimeoutExpress = "90m";
            model.ProductCode = "QUICK_MSECURITY_PAY";

            AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
            // 设置同步回调地址
            request.SetReturnUrl(AlipayConfig.returnUrl);
            // 设置异步通知接收地址
            request.SetNotifyUrl(notifyUrl);
            // 将业务model载入到request
            request.SetBizModel(model);

            try
            {
                AlipayTradeAppPayResponse response = client.SdkExecute(request);
                
                PaymentResult result = new PaymentResult();
                result.Code = "SUCCESS";
                result.Msg = response.Msg;
                result.Content = response.Body;
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }



    }
}
