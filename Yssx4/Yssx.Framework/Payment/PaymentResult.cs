﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Payment
{
    public class PaymentResult
    {
        /// <summary>
        /// 返回状态码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 微信：支付二维码地址  支付宝：支付表单
        /// </summary>
        public string Content { get; set; }
    }

    public class PaymentWechatResult : PaymentResult
    {
        /// <summary>
        /// 应用ID
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 商户号
        /// </summary>
        public string MchId { get; set; }

        /// <summary>
        /// 随机字符串
        /// </summary>
        public string NonceStr { get; set; }

        /// <summary>
        /// 签名
        /// </summary>
        public string Sign { get; set; }

        /// <summary>
        /// 预支付交易会话标识
        /// </summary>
        public string PrepayId { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; }

    }


}
