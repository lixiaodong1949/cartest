﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Auth
{
    /// <summary>
    /// 必要参数类
    /// </summary>
    public class PermissionRequirement
    {
        /// <summary>
        /// 签名验证
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }


        /// <summary>
        /// 构造
        /// </summary>  
        /// <param name="signingCredentials">签名验证实体</param>
        public PermissionRequirement(SigningCredentials signingCredentials)
        {
            SigningCredentials = signingCredentials;
        }
    }
}
