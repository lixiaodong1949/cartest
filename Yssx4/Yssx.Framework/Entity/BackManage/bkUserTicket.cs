﻿using System.Collections.Generic;

namespace Yssx.Framework.Authorizations
{
    /// <summary>
    /// 
    /// </summary>
    public class bkUserTicket
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        ///// <summary>
        ///// 租户Id
        ///// </summary>
        //public long TenantId { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        public string Name { get; set; }
        ///// <summary>
        ///// 真实姓名
        ///// </summary>

        //public string RealName { get; set; }

        ///// <summary>
        ///// 客户端id
        ///// </summary>
        //public string ClientId { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public int Type { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public List<RbacRole> Roles { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public List<RbacMenu> Menus { get; set; }

        /// <summary>
        /// 登录方式 0:PC 1:App 2.小程序
        /// </summary>
        public int LoginType { get; set; }


    }
}
