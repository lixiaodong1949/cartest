﻿using System;

namespace Yssx.Framework.Entity
{
    public  interface IModificationAudited 
    {
        /// <summary>
        /// 
        /// </summary>
        long UpdateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        DateTime? UpdateTime { get; set; }
    }
}
