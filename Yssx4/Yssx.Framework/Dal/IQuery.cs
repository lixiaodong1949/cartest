﻿using System.Linq;
using Report.Framework.Entity;

namespace Report.Framework.Dal
{
    public interface IQuery<out TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        ///   获取数据
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();
    }
}
