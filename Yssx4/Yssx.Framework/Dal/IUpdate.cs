﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Report.Framework.Entity;

namespace Report.Framework.Dal
{
    public interface IUpdate<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// 更新实体
        /// </summary> 
        /// <param name="entity"></param>
        /// <returns></returns>
        int Update(TEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>  
        /// <param name="entity"></param>
        /// <param name="updateCheck"></param>
        /// <returns></returns>
        int Update(TEntity entity, Expression<Func<TEntity, bool>> updateCheck);

        /// <summary>
        /// 更新实体指定列  不带条件，默认根据主键更新
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="express">列集合</param>
        /// <param name="isInclude"></param> 
        /// <returns></returns>
        int Update(TEntity entity, Expression<Func<TEntity, object>> express, bool isInclude);

        /// <summary>
        /// 更新实体指定列  待条件的
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="express">列集合</param>
        /// <param name="updateCheck">条件</param>
        /// <param name="isInclude"></param>
        /// <returns></returns>
        int Update(TEntity entity, Expression<Func<TEntity, object>> express, Expression<Func<TEntity, bool>> updateCheck, bool isInclude);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="entitys"></param>
        /// <returns></returns>
        int BatchUpdate(IList<TEntity> entitys);
       
    }
}
