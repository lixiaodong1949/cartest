﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Redis;

namespace OneAndX.Api.Auth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthorizationFilter : IAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param> 
        /// <param name="claims"></param> 
        private static UserTicket SetUserTicket(HttpContext httpContext, IEnumerable<Claim> claims)
        {
            var dicClaim = claims.ToDictionary(m => m.Type, m => m.Value);
            dicClaim.TryGetValue(JwtClaimTypes.Name, out var name);
            dicClaim.TryGetValue(JwtClaimTypes.Id, out var id);
            dicClaim.TryGetValue(CommonConstants.ClaimsTenantId, out var tenantId);
            dicClaim.TryGetValue(CommonConstants.ClaimsUserType, out var userType);
            //dicClaim.TryGetValue(CommonConstants.ClaimsMobile, out var mobile);
            dicClaim.TryGetValue(CommonConstants.ClaimsRealName, out var realName);
            dicClaim.TryGetValue(CommonConstants.ClaimsIdNumber, out var idNumber);
            dicClaim.TryGetValue(CommonConstants.LoginType, out var loginType);
            dicClaim.TryGetValue(CommonConstants.SchoolType, out var schoolType);
            dicClaim.TryGetValue(CommonConstants.SchoolName, out var schoolName);
            dicClaim.TryGetValue(CommonConstants.Account, out var moblie);
            int.TryParse(userType, out var usertypeInt);
            var userTicket = new UserTicket
            {
                Name = name,
                Id = long.Parse(id),
                TenantId = tenantId != null ? long.Parse(tenantId) : 0,
                //Mobile = mobile,
                RealName = realName,
                Type = usertypeInt,
                //IdNumber = idNumber
                LoginType = loginType != null ? int.Parse(loginType) : 0,
                MobilePhone = moblie,
                SchoolType = schoolType != null ? int.Parse(schoolType) : -1,
                Education = schoolType != null ? int.Parse(schoolType) : 0,
                SchoolName = schoolType != null ? schoolName : null,
            };
            httpContext.Items[CommonConstants.ContextUserPropertyKey] = userTicket;
            return userTicket;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var anonymous = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(AllowAnonymousAttribute), true);
            if (anonymous.Any())
            {
                return;
            }

            var controllerAnonymous = ((ControllerActionDescriptor)context.ActionDescriptor).ControllerTypeInfo.GetCustomAttributes(typeof(AllowAnonymousAttribute), true);
            if (controllerAnonymous.Any())//Controller 允许匿名访问
            {
                var actionAuthorize = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
                if (!actionAuthorize.Any())//Action 上没有增加必须验证的Attribute,跳过验证
                {
                    return;
                }
            }

            if (!context.HttpContext.Request.Headers.TryGetValue("token", out var token) || string.IsNullOrEmpty(token))
            {
                context.Result = new ObjectResult(new { Code = CommonConstants.Unauthorized, Sub_msg = "请求缺少token", Msg = "请求缺少token" });
                return;
            }

            var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var ticket = SetUserTicket(context.HttpContext, jwt.Claims);
            var userId = ticket.Id.ToString();
            var cacheToken = RedisHelperExtension.SxInstance.HGet(CacheKeys.UserLoginTokenHashKey, $"{userId}-{ticket.LoginType}");

            if (ticket.MobilePhone.Trim() == "15812345678")
                cacheToken = RedisHelperExtension.SxInstance.HGet(CacheKeys.UserLoginTokenHashKey, $"{userId}-0");

            if (string.IsNullOrEmpty(cacheToken))  //测试环境先注释
            {
                //var log = UserService.GetLastLoginLog(ticket.Id);
                //if (log.Status == Status.Disable || !Equals(log.AccessToken, token))
                //{
                //    context.Result = new ObjectResult(new { code = CommonConstants.Unauthorized, sub_msg = "无效的token请求", msg = "无效的token请求" });
                //    return;
                //}
                RedisHelperExtension.SxInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}-{ticket.LoginType}", token);
            }
            else if (!Equals(token, cacheToken))
            {
                RedisHelperExtension.SxInstance.HSet(CacheKeys.UserLoginTokenHashKey, "loginoutinfo", $"filed:{userId}-{ticket.LoginType} token:{token} cachetoken：{cacheToken}");
                context.Result = new ObjectResult(new
                {
                    Code = CommonConstants.Unauthorized,
                    Sub_msg = $"此账号在另一处登录，您已被迫下线!",
                    Msg = "此账号在另一处登录，您已被迫下线!"
                });
                return;
            }


        }
    }
}
