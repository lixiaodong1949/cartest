﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 新闻（前台）
    /// </summary>
    public class NewsController : BaseController
    {
        private IResourceManag _service;

        public NewsController(IResourceManag service)
        {
            _service = service;
        }

        /// <summary>
        /// 查找 新闻
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<NewsHomeResponse>>> FindNewsHome()
        {
            return await _service.FindNewsHome();
        }
    }
}