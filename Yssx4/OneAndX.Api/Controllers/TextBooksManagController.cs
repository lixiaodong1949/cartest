﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 教材管理（后台）
    /// </summary>
    public class TextBooksManagController : BaseController
    {
        private IResourceManag _service;

        public TextBooksManagController(IResourceManag service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加 更新 教材
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<long>> AddUpdateTextBook(TextBookDto model)
        {
            return await _service.AddUpdateTextBook(model, 1);//CurrentUser.Id
        }

        /// <summary>
        /// 添加 更新 章
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<List<TextBookDetails>>> AddUpdateChapter(TextBookChapterDto model)
        {
            return await _service.AddUpdateChapter(model, 1);//CurrentUser.Id
        }

        /// <summary>
        /// 添加 更新 节
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<List<TextBookDetails>>> AddUpdateSection(TextBookChapterDto model)
        {
            return await _service.AddUpdateChapter(model, 1);//CurrentUser.Id
        }

        /// <summary>
        /// 查找 教材
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<TextBookResponse>> FindTextBook(TextBookRequest model)
        {
            return await _service.FindTextBook(model);
        }

        /// <summary>
        /// 编辑 预览教材 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<TextBookPreviewResponse>> FindTextBookPreview(long Id)
        {
            return await _service.FindTextBookPreview(Id);
        }

        /// <summary>
        ///  删除 教材
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RemoveTextBook(long Id)
        {
            return await _service.RemoveTextBook(Id, 2);// CurrentUser.Id
        }

        /// <summary>
        ///  删除 章节
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RemoveChapter(long Id)
        {
            return await _service.RemoveChapter(Id, 2);// CurrentUser.Id

        }

        }
    }