﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 教材（前台）
    /// </summary>
    public class TextBooksController : BaseController
    {
        private IResourceManag _service;

        public TextBooksController(IResourceManag service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据下拉模块带出数据
        /// </summary>
        /// <param name="Module">
        /// 0显示全部模块(对应菜单模块)
        /// 1,2,3显示对应模块</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<TextBooksAllModuleDto>>> TeachersAllClasses(int Module)
        {
            return await _service.TeachersAllClasses(Module);
        }

        /// <summary>
        /// 查找 教材
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<TextBooksResponse>> FindTextBooks(TextBooksRequest model)
        {
            return await _service.FindTextBooks(model);
        }
    }
}