﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 自然人
    /// </summary>
    public class NaturalPersonsService : INaturalPersonsService
    {
        /// <summary>
        /// 获取列表-
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetListResponse>> GetPersonnelList(PersonnelListDao model, long oId)
        {//加基础下拉控制，姓名和身份证带出
            var obj = new GetListResponse();
            var list = new List<GetListResponse>();
            long totalCount = 0;
            return await Task.Run(() =>
            {
                switch (model.Type)
                {
                    case 1:
                        var select = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.PersonnelId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.PersonnelDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select.Count();
                        var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,Sex,PersonnelStatus,b.PersonnelSubmitted,Authentication,MobilePhone,DoesItExist,Employee,Nationality,Remarks");//,b.CaseId
                        var items = DbContext.FreeSql.Ado.Query<PersonnelList>(sql);
                        obj.TotalPeople = totalCount;
                        obj.PersonnelList = items;
                        list.Add(obj);
                        break;

                    case 2://&& a.ChildrenSign == false && a.ContinueSign== false && a.InterestSign== false && a.RentSign== false && a.SupportSign == false
                        var select1 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.ChildrenId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.ChildrenDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select1.Count();
                        var sql1 = select1.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,b.ChildrenSubmitted Submitted,b.UpdateTime");
                        var items1 = DbContext.FreeSql.Ado.Query<DeductionList>(sql1);
                        obj.TotalPeople = totalCount;
                        obj.DeductionList = items1;
                        list.Add(obj);
                        break;

                    case 3:
                        var select2 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.ContinueId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.ContinueDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select2.Count();
                        var sql2 = select2.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,b.ContinueSubmitted Submitted,b.UpdateTime");
                        var items2 = DbContext.FreeSql.Ado.Query<DeductionList>(sql2);
                        obj.TotalPeople = totalCount;
                        obj.DeductionList = items2;
                        list.Add(obj);
                        break;

                    case 4:
                        var select3 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.InterestId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.InterestDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select3.Count();
                        var sql3 = select3.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,b.InterestSubmitted Submitted,b.UpdateTime");
                        var items3 = DbContext.FreeSql.Ado.Query<DeductionList>(sql3);
                        obj.TotalPeople = totalCount;
                        obj.DeductionList = items3;
                        list.Add(obj);
                        break;

                    case 5:
                        var select4 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.RentId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.RentDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select4.Count();
                        var sql4 = select4.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,b.RentSubmitted Submitted,b.UpdateTime");
                        var items4 = DbContext.FreeSql.Ado.Query<DeductionList>(sql4);
                        obj.TotalPeople = totalCount;
                        obj.DeductionList = items4;
                        list.Add(obj);
                        break;

                    case 6:
                        var select5 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.SupportId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.SupportDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select5.Count();
                        var sql5 = select5.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,b.SupportSubmitted Submitted,b.UpdateTime");
                        var items5 = DbContext.FreeSql.Ado.Query<DeductionList>(sql5);
                        obj.TotalPeople = totalCount;
                        obj.DeductionList = items5;
                        list.Add(obj);
                        break;

                    case 7://还要关联 综合所得申报
                        var select6 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                        .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.IncomeId))
                        .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CaseId == model.CaseId && b.CreateBy == oId && b.IncomeDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);
                        totalCount = select6.Count();
                        var sql6 = select6.Page(model.PageIndex, model.PageSize).ToSql(@"b.Id,JobNo,Name,DocumentType,CertificateNum,StartTime,EndTime");
                        var items6 = DbContext.FreeSql.Ado.Query<IncomeList>(sql6);
                        obj.TotalPeople = totalCount;
                        obj.IncomeList = items6;
                        list.Add(obj);
                        break;

                    default:
                        break;
                }

                return new PageResponse<GetListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
            });
        }

        /// <summary>
        /// 查看资料
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<ViewInfoResponse>> GetPersonnelDataList(PersonnelListDao model)
        {
            var obj = new ViewInfoResponse();
            var list = new List<ViewInfoResponse>();
            long totalCount = 0;
            return await Task.Run(() =>
            {
                switch (model.Type)
                {
                    case 1:
                        var select = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(a => a.CaseId == model.CaseId && a.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select.Count();
                        var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,Id PersonnelId,Department,Name,DocumentType,CertificateNum,MobilePhone,Nationality,Employee,Lonely,Investor,SpecificIndustries,AngelInvestors,PersonnelStatus");
                        var items = DbContext.FreeSql.Ado.Query<PersonnelDataList>(sql);
                        obj.PersonnelDataList = items;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 2://下面要关联人员显示人员信息，还要显示配偶
                        var select1 = DbContext.FreeSql.Select<YssxNaturalpersonsChildren>()
                        .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select1.Count();
                        var sql1 = select1.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Name,DocumentType,CertificateNum,Spouse,SpouseName,SpouseDocumentType,SpouseCertificateNum,ChildrenName,ChildNumber,a.DateOfBirth,a.Nationality,CurrentEducation,EducationBegins,EducationEnd,CountryStudy,CurrentSchool,DeductionProportion");
                        var items1 = DbContext.FreeSql.Ado.Query<ChildrenDataList>(sql1);
                        obj.ChildrenDataList = items1;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 3:
                        var select2 = DbContext.FreeSql.Select<YssxNaturalpersonsContinue>()
                        .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select2.Count();
                        var sql2 = select2.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Name,DocumentType,CertificateNum,AdmissionTime,GraduationTime,EducationStage,EducationTypes,IssueDate,CertificateName,CertificateNumber,IssuingAuthority");
                        var items2 = DbContext.FreeSql.Ado.Query<ContinueDataList>(sql2);
                        obj.ContinueDataList = items2;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 4:
                        var select3 = DbContext.FreeSql.Select<YssxNaturalpersonsInterest>()
                        .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select3.Count();
                        var sql3 = select3.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Name,DocumentType,CertificateNum,Spouse,SpouseName,SpouseDocumentType,SpouseCertificateNum,LocationHouse,BuildingNumber,HouseType,HousingCertificate,Borrower,Premarital,LoanType,LoanContractNo,LendingBank,FirstRepaymentDate,LoanTerm");
                        var items3 = DbContext.FreeSql.Ado.Query<InterestDataList>(sql3);
                        obj.InterestDataList = items3;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 5:
                        var select4 = DbContext.FreeSql.Select<YssxNaturalpersonsRent>()
                         .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select4.Count();
                        var sql4 = select4.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Name,DocumentType,CertificateNum,Spouse,SpouseName,SpouseDocumentType,SpouseCertificateNum,WorkingCity,RentalType,HouseLocation,BuildingBrand,FromLeaseDate,EndLeaseDate");
                        var items4 = DbContext.FreeSql.Ado.Query<RentDataList>(sql4);
                        obj.RentDataList = items4;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 6:
                        var select5 = DbContext.FreeSql.Select<YssxNaturalpersonsSupport>()
                        .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select5.Count();
                        var sql5 = select5.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Name,DocumentType,CertificateNum,Spouse,SpouseName,SpouseDocumentType,SpouseCertificateNum,Only,Share,MonthlyDeduction,FosterName,FosterDocumentType,FosterCertificateNum,FosterNationality,Relationship,a.DateOfBirth,CoCultureName,CoCultureTypes,CoCultureNum,CoCultureNationality");
                        var items5 = DbContext.FreeSql.Ado.Query<SupportDataList>(sql5);
                        obj.SupportDataList = items5;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    case 7:
                        var select6 = DbContext.FreeSql.Select<YssxNaturalpersonsIncome>()
                        .From<YssxNaturalpersonsPersonnel>((a, b) => a.LeftJoin(aa => aa.PersonnelId == b.Id && aa.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.CaseId == model.CaseId && b.IsDelete == CommonConstants.IsNotDelete);

                        totalCount = select6.Count();
                        var sql6 = select6.Page(model.PageIndex, model.PageSize).ToSql(@"a.Id,PersonnelId,Department,Name,CertificateNum,Wages,Pension,MedicalCare,Unemployment,AccumulationFund,AccumulatedChildren,AccumulatedInterest,AccumulatedRental,AccumulatedSupport,AccumulatedContinue,WithholdingTax,NetSalary");
                        var items6 = DbContext.FreeSql.Ado.Query<IncomeDataList>(sql6);
                        obj.IncomeDataList = items6;
                        list.Add(obj);
                        obj.Header = Header(model.Type);
                        break;

                    default:
                        break;
                }

                return new PageResponse<ViewInfoResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
            });
        }

        /// <summary>
        /// 查看资料  表头
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        public string Header(int Type)
        {
            var s = string.Empty;
            switch (Type)
            {
                case 1:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"部门\",\"property\":\"Department\"},{\"label\": \"名称\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"手机号码\",\"property\":\"MobilePhone\"},{\"label\": \"国籍（地区）\",\"property\":\"Nationality\"}," +
                        "{\"label\": \"雇员\",\"property\":\"Employee\"},{\"label\": \"残疾烈属孤老\",\"property\":\"Lonely\"},{\"label\": \"股东、投资人\",\"property\":\"Investor\"},{\"label\": \"特定行业\",\"property\":\"SpecificIndustries\"},{\"label\": \"天使投资人\",\"property\":\"AngelInvestors\"},{\"label\": \"人员状态\",\"property\":\"PersonnelStatus\"}]";
                    break;

                case 2:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"配偶情况\",\"property\":\"Spouse\"},{\"label\": \"名称\",\"property\":\"SpouseName\"},{\"label\": \"证件类型\",\"property\":\"SpouseDocumentType\"},{\"label\": \"证件号码\",\"property\":\"SpouseCertificateNum\"}" +
                        ",{\"label\": \"子女姓名\",\"property\":\"ChildrenName\"},{\"label\": \"子女姓名\",\"property\":\"ChildNumber\"},{\"label\": \"出生年月\",\"property\":\"DateOfBirth\"},{\"label\": \"国籍（地区）\",\"property\":\"Nationality\"},{\"label\": \"当前受教育阶段\",\"property\":\"CurrentEducation\"},{\"label\": \"受教育日期起\",\"property\":\"EducationBegins\"},{\"label\": \"教育终止时间\",\"property\":\"EducationEnd\"},{\"label\": \"就读国家\",\"property\":\"CountryStudy\"},{\"label\": \"当前就读学校\",\"property\":\"CurrentSchool\"},{\"label\": \"本人扣除比例\",\"property\":\"DeductionProportion\"}]";
                    break;

                case 3:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"入学时间起\",\"property\":\"AdmissionTime\"},{\"label\": \"(预计)毕业时间\",\"property\":\"GraduationTime\"},{\"label\": \"教育阶段\",\"property\":\"EducationStage\"},{\"label\": \"继续教育类型\",\"property\":\"EducationTypes\"},{\"label\": \"发证(批准)日期\",\"property\":\"IssueDate\"},{\"label\": \"证书名称\",\"property\":\"CertificateName\"},{\"label\": \"证书编号\",\"property\":\"CertificateNumber\"},{\"label\": \"发证机关\",\"property\":\"IssuingAuthority\"}]";
                    break;

                case 4:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"配偶情况\",\"property\":\"Spouse\"},{\"label\": \"名称\",\"property\":\"SpouseName\"},{\"label\": \"证件类型\",\"property\":\"SpouseDocumentType\"},{\"label\": \"证件号码\",\"property\":\"SpouseCertificateNum\"}" +
                        ",{\"label\": \"房屋坐落地址\",\"property\":\"LocationHouse\"},{\"label\": \"房屋楼牌号\",\"property\":\"BuildingNumber\"},{\"label\": \"房屋证书类型\",\"property\":\"HouseType\"},{\"label\": \"房屋证书号\",\"property\":\"HousingCertificate\"},{\"label\": \"本人是否借款人\",\"property\":\"Borrower\"},{\"label\": \"是否婚前各自首套贷款\",\"property\":\"Premarital\"},{\"label\": \"贷款类型\",\"property\":\"LoanType\"},{\"label\": \"贷款合同编号\",\"property\":\"LoanContractNo\"},{\"label\": \"贷款银行\",\"property\":\"LendingBank\"},{\"label\": \"首次还款日期\",\"property\":\"FirstRepaymentDate\"},{\"label\": \"贷款期限(月数)\",\"property\":\"LoanTerm\"}]";
                    break;

                case 5:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"配偶情况\",\"property\":\"Spouse\"},{\"label\": \"名称\",\"property\":\"SpouseName\"},{\"label\": \"证件类型\",\"property\":\"SpouseDocumentType\"},{\"label\": \"证件号码\",\"property\":\"SpouseCertificateNum\"}" +
                        ",{\"label\": \"工作城市\",\"property\":\"WorkingCity\"},{\"label\": \"出租房类型\",\"property\":\"RentalType\"},{\"label\": \"房屋坐落地址\",\"property\":\"HouseLocation\"},{\"label\": \"房屋坐落楼牌号\",\"property\":\"BuildingBrand\"},{\"label\": \"租赁日期起\",\"property\":\"FromLeaseDate\"},{\"label\": \"租赁日期止\",\"property\":\"EndLeaseDate\"}]";
                    break;

                case 6:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件类型\",\"property\":\"DocumentType\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"配偶情况\",\"property\":\"Spouse\"},{\"label\": \"名称\",\"property\":\"SpouseName\"},{\"label\": \"证件类型\",\"property\":\"SpouseDocumentType\"},{\"label\": \"证件号码\",\"property\":\"SpouseCertificateNum\"}" +
                        ",{\"label\": \"是否独生子女\",\"property\":\"Only\"},{\"label\": \"分摊方式\",\"property\":\"Share\"},{\"label\": \"本年度月扣除金额\",\"property\":\"MonthlyDeduction\"},{\"label\": \"被养姓名\",\"property\":\"FosterName\"},{\"label\": \"被养身份证件类型\",\"property\":\"FosterDocumentType\"},{\"label\": \"被养身份证号码\",\"property\":\"FosterCertificateNum\"},{\"label\": \"被养国籍地区\",\"property\":\"FosterNationality\"},{\"label\": \"关系\",\"property\":\"Relationship\"},{\"label\": \"出生日期\",\"property\":\"DateOfBirth\"},{\"label\": \"共养姓名\",\"property\":\"CoCultureName\"},{\"label\": \"共养身份证件类型\",\"property\":\"CoCultureTypes\"},{\"label\": \"共养身份证件号码\",\"property\":\"CoCultureNum\"},{\"label\": \"共养国籍（地区）\",\"property\":\"CoCultureNationality\"}]";
                    break;

                case 7:
                    s = "[{\"label\": \"序号\",\"property\":\"Id\"},{\"label\": \"部门\",\"property\":\"Department\"},{\"label\": \"姓名\",\"property\":\"Name\"},{\"label\": \"证件号码\",\"property\":\"CertificateNum\"},{\"label\": \"应发工资合计\",\"property\":\"Wages\"},{\"label\": \"养老保险\",\"property\":\"Pension\"},{\"label\": \"医疗保险\",\"property\":\"MedicalCare\"},{\"label\": \"失业保险\",\"property\":\"Unemployment\"},{\"label\": \"住房公积金\",\"property\":\"AccumulationFund\"}," +
                        "{\"label\": \"累计子女教育\",\"property\":\"AccumulatedChildren\"},{\"label\": \"累计住房贷款利息\",\"property\":\"AccumulatedInterest\"},{\"label\": \"累计租房租金\",\"property\":\"AccumulatedRental\"},{\"label\": \"累计赡养老人\",\"property\":\"AccumulatedSupport\"},{\"label\": \"累计继续教育\",\"property\":\"AccumulatedContinue\"},{\"label\": \"当月个税预扣预缴税额\",\"property\":\"WithholdingTax\"},{\"label\": \"实发工资\",\"property\":\"NetSalary\"}]";
                    break;

                default:
                    break;
            }
            return s;
        }

        /// <summary>
        /// 添加 更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdate(NotesDao model, long oId)
        {
            return await Task.Run(() =>
            {
                switch (model.Type)//报送,获取反馈，删除，清空（重新走流程）
                {
                    case 1:
                        #region 人员信息采集
                        //检查是否添加基础数据
                        var select1 = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql1 = select1.ToSql(@"CaseId");//后续加?
                        var oldBasics1 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsPersonnel>(sql1).FirstOrDefault();
                        if (oldBasics1 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)

                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.PersonnelDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.PersonnelSubmitted == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该人员采集已报送!" };

                            //oldinfo.CaseId = model.CaseId;
                            oldinfo.PersonnelId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.PersonnelId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();//m.CaseId,
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }
                        //检查人员是否在添加列表
                        var personnel1 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.PersonnelDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel1 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该人员采集已在列表，无须重复操作!" };
                        //添加
                        var newinfo = new YssxNaturalpersonsAnswerNotes
                        {
                            Id = IdWorker.NextId(),
                            CaseId = oldBasics1.CaseId,
                            PersonnelId = model.PersonnelId,
                            CreateBy = oId
                        };
                        DbContext.FreeSql.GetRepository<YssxNaturalpersonsAnswerNotes>().Insert(newinfo);
                        //是否同步基础数据

                        #endregion
                        break;

                    case 2:
                        #region 子女教育支出
                        //检查是否添加基础数据
                        var select2 = DbContext.FreeSql.Select<YssxNaturalpersonsChildren>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql2 = select2.ToSql(@"CaseId");//后续加?
                        var oldBasics2 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsChildren>(sql2).FirstOrDefault();
                        if (oldBasics2 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel2 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.ChildrenDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel2 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel2.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.ChildrenDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.ChildrenSubmitted == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该扣除人员已报送!" };

                            oldinfo.ChildrenId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.ChildrenId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }

                        personnel2.ChildrenId = model.PersonnelId;
                        personnel2.UpdateBy = oId;
                        personnel2.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel2).UpdateColumns(m => new { m.ChildrenId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    case 3:
                        #region 继续教育支出
                        //检查是否添加基础数据
                        var select3 = DbContext.FreeSql.Select<YssxNaturalpersonsContinue>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql3 = select3.ToSql(@"CaseId");//后续加?
                        var oldBasics3 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsContinue>(sql3).FirstOrDefault();
                        if (oldBasics3 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel3 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.ContinueDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel3 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel3.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.ContinueDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.ContinueFeedback == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该扣除人员已报送!" };

                            oldinfo.ContinueId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.ContinueId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }

                        personnel3.ContinueId = model.PersonnelId;
                        personnel3.UpdateBy = oId;
                        personnel3.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel3).UpdateColumns(m => new { m.ContinueId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    case 4:
                        #region 住房贷款利息支出
                        //检查是否添加基础数据
                        var select4 = DbContext.FreeSql.Select<YssxNaturalpersonsInterest>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql4 = select4.ToSql(@"CaseId");//后续加?
                        var oldBasics4 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsInterest>(sql4).FirstOrDefault();
                        if (oldBasics4 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel4 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.InterestDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel4 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel4.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.InterestDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.InterestSubmitted == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该扣除人员已报送!" };

                            oldinfo.InterestId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.InterestId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }

                        personnel4.InterestId = model.PersonnelId;
                        personnel4.UpdateBy = oId;
                        personnel4.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel4).UpdateColumns(m => new { m.InterestId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    case 5:
                        #region 住房租金支出
                        //检查是否添加基础数据
                        var select5 = DbContext.FreeSql.Select<YssxNaturalpersonsRent>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql5 = select5.ToSql(@"CaseId");//后续加?
                        var oldBasics5 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsRent>(sql5).FirstOrDefault();
                        if (oldBasics5 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel5 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.RentDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel5 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel5.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.RentDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.RentSubmitted == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该扣除人员已报送!" };

                            oldinfo.RentId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.RentId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }

                        personnel5.RentId = model.PersonnelId;
                        personnel5.UpdateBy = oId;
                        personnel5.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel5).UpdateColumns(m => new { m.RentId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    case 6:
                        #region 赡养老人支出
                        //检查是否添加基础数据
                        var select6 = DbContext.FreeSql.Select<YssxNaturalpersonsSupport>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql6 = select6.ToSql(@"CaseId");//后续加?
                        var oldBasics6 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsSupport>(sql6).FirstOrDefault();
                        if (oldBasics6 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel6 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.SupportDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel6 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel6.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.SupportDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                            if (oldinfo.SupportSubmitted == 1)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，该扣除人员已报送!" };

                            oldinfo.SupportId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.SupportId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }

                        personnel6.SupportId = model.PersonnelId;
                        personnel6.UpdateBy = oId;
                        personnel6.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel6).UpdateColumns(m => new { m.SupportId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    case 7:
                        #region 综合所得申报（调）
                        //检查是否添加基础数据
                        var select7 = DbContext.FreeSql.Select<YssxNaturalpersonsIncome>().Where(a => a.Id == model.ViewInfoId && a.IsDelete == CommonConstants.IsNotDelete);
                        var sql7 = select7.ToSql(@"CaseId");//后续加?
                        var oldBasics7 = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsIncome>(sql7).FirstOrDefault();
                        if (oldBasics7 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，查看资料没有这条数据！" };
                        //model跟基础数据对比是否正确(前台要拦截对比)?

                        //检查人员是否在添加列表
                        var personnel7 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.PersonnelId == model.PersonnelId && m.CreateBy == oId && m.IncomeDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (personnel7 == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，该人员未采集！" };
                        //检查人员获取反馈
                        if (personnel7.PersonnelFeedback == 0)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败,该人员采集未获取反馈！" };
                        //提交答案
                        //修改
                        if (model.Id > 0)
                        {
                            var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => m.Id == model.Id && m.IncomeDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (oldinfo == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                            oldinfo.IncomeId = model.PersonnelId;
                            oldinfo.UpdateBy = oId;
                            oldinfo.UpdateTime = DateTime.Now;
                            var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(oldinfo).UpdateColumns(m => new { m.IncomeId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                            if (i == 0)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                        }


                        personnel7.IncomeId = model.PersonnelId;
                        personnel7.UpdateBy = oId;
                        personnel7.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(personnel7).UpdateColumns(m => new { m.IncomeId, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //是否同步基础数据?

                        #endregion
                        break;

                    default:
                        break;
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 报送
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Submitted(IdsDao model, long oId)
        {
            if (model.Ids.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "报送失败，找不到原始信息!" };

            var list = new List<YssxNaturalpersonsAnswerNotes>();
            foreach (var id in model.Ids)
            {
                list.Add(new YssxNaturalpersonsAnswerNotes()
                {
                    Id = id
                });
            }

            return await Task.Run(() =>
            {
                var i = 0;
                switch (model.Type)
                {
                    case 1:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.PersonnelSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 2:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ChildrenSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 3:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ContinueSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 4:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.InterestSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 5:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.RentSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 6:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.SupportSubmitted, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 7:

                        break;

                    default:
                        break;
                }

                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "报送失败!" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "人员信息报送成功,请稍后获取反馈。" };
            });
        }

        /// <summary>
        /// 获取反馈
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Feedback(IdsDao model, long oId)
        {
            if (model.Ids.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "获取反馈失败，找不到原始信息!" };

            var list = new List<YssxNaturalpersonsAnswerNotes>();
            foreach (var id in model.Ids)
            {
                list.Add(new YssxNaturalpersonsAnswerNotes()
                {
                    Id = id
                });
            }

            var str = new StringBuilder();
            var i = 0;


            return await Task.Run(() =>
            {
                //检查是否已报送
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>()
                .From<YssxNaturalpersonsAnswerNotes>((a, b) => a.LeftJoin(aa => aa.Id == b.PersonnelId && aa.IsDelete == CommonConstants.IsNotDelete));
                switch (model.Type)
                {
                    case 1:
                        var sql1 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.PersonnelDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                                    .ToSql("Name,DocumentType,CertificateNum,PersonnelSubmitted Submitted");
                        var items1 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql1);

                        foreach (var obj in items1)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted1 = items1.Where(s => s.Submitted == 0).ToList();
                        if (Submitted1.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.PersonnelFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 2:
                        var sql2 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.ChildrenDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                                    .ToSql("Name,DocumentType,CertificateNum,ChildrenSubmitted Submitted");
                        var items2 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql2);

                        foreach (var obj in items2)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted2 = items2.Where(s => s.Submitted == 0).ToList();
                        if (Submitted2.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ChildrenFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 3:
                        var sql3 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.ContinueDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                        .ToSql("Name,DocumentType,CertificateNum,ContinueSubmitted Submitted");
                        var items3 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql3);

                        foreach (var obj in items3)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted3 = items3.Where(s => s.Submitted == 0).ToList();
                        if (Submitted3.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ContinueFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 4:
                        var sql4 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.InterestDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                                    .ToSql("Name,DocumentType,CertificateNum,InterestSubmitted Submitted");
                        var items4 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql4);

                        foreach (var obj in items4)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted4 = items4.Where(s => s.Submitted == 0).ToList();
                        if (Submitted4.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.InterestFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 5:
                        var sql5 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.RentDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                        .ToSql("Name,DocumentType,CertificateNum,RentSubmitted Submitted");
                        var items5 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql5);

                        foreach (var obj in items5)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted5 = items5.Where(s => s.Submitted == 0).ToList();
                        if (Submitted5.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.RentFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 6:
                        var sql6 = select.Where((a, b) => model.Ids.Contains(b.Id) && b.SupportDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                        .ToSql("Name,DocumentType,CertificateNum,SupportSubmitted Submitted");
                        var items6 = DbContext.FreeSql.Ado.Query<FeedbackTips>(sql6);

                        foreach (var obj in items6)
                        {
                            var state = obj.Submitted == 0 ? "失败" : "成功";
                            str.Append($"姓名：{obj.Name} 证件类型：{obj.DocumentType} 证件号码：{obj.CertificateNum} 报送{state}");
                        }

                        var Submitted6 = items6.Where(s => s.Submitted == 0).ToList();
                        if (Submitted6.Count == 0)
                            i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.SupportFeedback, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 7:

                        break;

                    default:
                        break;
                }

                //if (i == 0)
                //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "获取反馈失败!" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = str.ToString() };
            });
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(IdsDao model, long oId)
        {
            if (model.Ids.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            var list = new List<YssxNaturalpersonsAnswerNotes>();
            foreach (var id in model.Ids)
            {
                list.Add(
                    new YssxNaturalpersonsAnswerNotes()
                    {
                        Id = id
                    });
            }

            return await Task.Run(() =>
            {
                var i = 0;
                switch (model.Type)
                {
                    case 1:
                        //检查是否已报送
                        var oldinfo1 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.PersonnelSubmitted == 1 && m.PersonnelDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo1 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.PersonnelDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 2:
                        //检查是否已报送
                        var oldinfo2 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.ChildrenSubmitted == 1 && m.ChildrenDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo2 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ChildrenDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 3:
                        //检查是否已报送
                        var oldinfo3 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.ContinueSubmitted == 1 && m.ContinueDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo3 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.ContinueDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 4:
                        //检查是否已报送
                        var oldinfo4 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.InterestSubmitted == 1 && m.InterestDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo4 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.InterestDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 5:
                        //检查是否已报送
                        var oldinfo5 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.RentSubmitted == 1 && m.RentDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo5 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.RentDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 6:
                        //检查是否已报送
                        var oldinfo6 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.SupportSubmitted == 1 && m.SupportDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (oldinfo6 != null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.SupportDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 7:
                        ////检查是否已报送
                        //var oldinfo7 = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(m => model.Ids.Contains(m.Id) && m.IncomeDelete == CommonConstants.IsNotDelete && m.IsDelete == CommonConstants.IsNotDelete).First();
                        //if (oldinfo7 != null)
                        //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，有人员已报送，请选报送!" };

                        i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.IncomeDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    default:
                        break;
                }

                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "删除失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 清空重做
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Redo(long CaseId, long oId)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsAnswerNotes>().Where(a => a.CreateBy == oId && a.CaseId == CaseId && a.IsDelete == CommonConstants.IsNotDelete);
                var sql = select.ToSql("Id");
                var list = DbContext.FreeSql.Ado.Query<YssxNaturalpersonsAnswerNotes>(sql);
                if (list.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "还未作答，不需要重新填写。" };

                var i = DbContext.FreeSql.Update<YssxNaturalpersonsAnswerNotes>().SetSource(list).Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "清空重做失败" };

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }



        /// <summary>
        /// 获取 人员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<PersonnelListResponse>> GetPersonnelbkList(PersonnelListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(a => a.CaseId == model.CaseId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,Department,Name,DocumentType,CertificateNum,MobilePhone,Nationality,Employee,Lonely,Investor,SpecificIndustries,AngelInvestors
                                                                               ,Spouse,SpouseName,SpouseDocumentType,SpouseCertificateNum,StartTime,Withholding");
                var items = DbContext.FreeSql.Ado.Query<PersonnelListResponse>(sql);

                return new PageResponse<PersonnelListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 子女教育列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<ChildrenListResponse>> GetChildrenbkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsChildren>().Where(a => a.CaseId == model.CaseId && a.PersonnelId==model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,ChildrenName,ChildType,ChildNumber,DateOfBirth,Nationality,CurrentEducation,EducationBegins,EducationEnd,EducationTimeEnd,CountryStudy,CurrentSchool,DeductionProportion");
                var items = DbContext.FreeSql.Ado.Query<ChildrenListResponse>(sql);

                return new PageResponse<ChildrenListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 继续教育支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<ContinueListResponse>> GetContinuebkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsContinue>().Where(a => a.CaseId == model.CaseId && a.PersonnelId == model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,AdmissionTime,GraduationTime,EducationStage,EducationTypes,IssueDate,CertificateName,CertificateNumber,IssuingAuthority");
                var items = DbContext.FreeSql.Ado.Query<ContinueListResponse>(sql);

                return new PageResponse<ContinueListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 住房贷款利息支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<InterestListResponse>> GetInterestbkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsInterest>().Where(a => a.CaseId == model.CaseId && a.PersonnelId == model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,LocationHouse,BuildingNumber,HouseType,HousingCertificate,Borrower,Premarital,LoanType,LoanContractNo,LendingBank,FirstRepaymentDate,LoanTerm");
                var items = DbContext.FreeSql.Ado.Query<InterestListResponse>(sql);

                return new PageResponse<InterestListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 住房租金支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<RentListResponse>> GetRentbkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsRent>().Where(a => a.CaseId == model.CaseId && a.PersonnelId == model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,WorkingCity,RentalType,HouseLocation,BuildingBrand,FromLeaseDate,EndLeaseDate");
                var items = DbContext.FreeSql.Ado.Query<RentListResponse>(sql);

                return new PageResponse<RentListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 赡养老人支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<SupportListResponse>> GetSupportbkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsSupport>().Where(a => a.CaseId == model.CaseId && a.PersonnelId == model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,Only,Share,MonthlyDeduction,FosterName,FosterDocumentType,FosterCertificateNum,FosterNationality,Relationship,DateOfBirth,CoCultureName,CoCultureTypes,CoCultureNum,CoCultureNationality");
                var items = DbContext.FreeSql.Ado.Query<SupportListResponse>(sql);

                return new PageResponse<SupportListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 获取 综合所得申报列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<IncomeListResponse>> GetIncomebkList(DeductionIncomeListBkDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxNaturalpersonsIncome>().Where(a => a.CaseId == model.CaseId && a.PersonnelId == model.PersonnelId && a.IsDelete == CommonConstants.IsNotDelete);
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql(@"Id,CaseId,PersonnelId,Wages,Pension,MedicalCare,Unemployment,AccumulationFund,AccumulatedChildren,AccumulatedInterest,AccumulatedRental,AccumulatedSupport, AccumulatedContinue,WithholdingTax,NetSalary");
                var items = DbContext.FreeSql.Ado.Query<IncomeListResponse>(sql);

                return new PageResponse<IncomeListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }
        


        /// <summary>
        /// 添加 更新 人员
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdatePersonnel(PersonnelDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.Department = model.Department;
                    oldinfo.Name = model.Name;
                    oldinfo.DocumentType = model.DocumentType;
                    oldinfo.CertificateNum = model.CertificateNum;
                    oldinfo.MobilePhone = model.MobilePhone;
                    oldinfo.Nationality = model.Nationality;
                    oldinfo.Employee = model.Employee;
                    oldinfo.Lonely = model.Lonely;
                    oldinfo.Investor = model.Investor;
                    oldinfo.SpecificIndustries = model.SpecificIndustries;
                    oldinfo.AngelInvestors = model.AngelInvestors;

                    oldinfo.Spouse = model.Spouse;
                    oldinfo.SpouseName = model.SpouseName;
                    oldinfo.SpouseDocumentType = model.SpouseDocumentType;
                    oldinfo.SpouseCertificateNum = model.SpouseCertificateNum;

                    oldinfo.StartTime = model.StartTime;
                    oldinfo.Withholding = model.Withholding;

                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsPersonnel>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId,m.Department,m.Name,m.DocumentType,m.CertificateNum,m.MobilePhone,m.Nationality,m.Employee,m.Lonely,m.Investor,m.SpecificIndustries,m.AngelInvestors,m.Spouse,m.SpouseName,m.SpouseDocumentType,m.SpouseCertificateNum,m.StartTime,m.Withholding,m.UpdateBy,m.UpdateTime}).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsPersonnel
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    Department = model.Department,
                    Name = model.Name,
                    DocumentType = model.DocumentType,
                    CertificateNum = model.CertificateNum,
                    MobilePhone = model.MobilePhone,
                    Nationality = model.Nationality,
                    Employee = model.Employee,
                    Lonely = model.Lonely,
                    Investor = model.Investor,
                    SpecificIndustries = model.SpecificIndustries,
                    AngelInvestors = model.AngelInvestors,

                    Spouse = model.Spouse,
                    SpouseName = model.SpouseName,
                    SpouseDocumentType = model.SpouseDocumentType,
                    SpouseCertificateNum = model.SpouseCertificateNum,

                    StartTime = model.StartTime,
                    Withholding = model.Withholding,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsPersonnel>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 子女教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateChildren(ChildrenDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsChildren>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.ChildrenName = model.ChildrenName;
                    oldinfo.ChildType = model.ChildType;
                    oldinfo.ChildNumber = model.ChildNumber;
                    oldinfo.DateOfBirth = model.DateOfBirth;
                    oldinfo.Nationality = model.Nationality;
                    oldinfo.CurrentEducation = model.CurrentEducation;
                    oldinfo.EducationBegins = model.EducationBegins;
                    oldinfo.EducationEnd = model.EducationEnd;
                    oldinfo.EducationTimeEnd = model.EducationTimeEnd;
                    oldinfo.CountryStudy = model.CountryStudy;
                    oldinfo.CurrentSchool = model.CurrentSchool;
                    oldinfo.DeductionProportion = model.DeductionProportion;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsChildren>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.ChildrenName, m.ChildType, m.ChildNumber, m.DateOfBirth, m.Nationality, m.CurrentEducation, m.EducationBegins, m.EducationEnd, m.EducationTimeEnd, m.CountryStudy, m.CurrentSchool, m.DeductionProportion, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsChildren
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    ChildrenName = model.ChildrenName,
                    ChildType = model.ChildType,
                    ChildNumber = model.ChildNumber,
                    DateOfBirth = model.DateOfBirth,
                    Nationality = model.Nationality,
                    CurrentEducation = model.CurrentEducation,
                    EducationBegins = model.EducationBegins,
                    EducationEnd = model.EducationEnd,
                    EducationTimeEnd = model.EducationTimeEnd,
                    CountryStudy = model.CountryStudy,
                    CurrentSchool = model.CurrentSchool,
                    DeductionProportion = model.DeductionProportion,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsChildren>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 继续教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateContinue(ContinueDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsContinue>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.AdmissionTime = model.AdmissionTime;
                    oldinfo.GraduationTime = model.GraduationTime;
                    oldinfo.EducationStage = model.EducationStage;
                    oldinfo.EducationTypes = model.EducationTypes;
                    oldinfo.IssueDate = model.IssueDate;
                    oldinfo.CertificateName = model.CertificateName;
                    oldinfo.CertificateNumber = model.CertificateNumber;
                    oldinfo.IssuingAuthority = model.IssuingAuthority;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsContinue>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.AdmissionTime, m.GraduationTime, m.EducationStage, m.EducationTypes, m.IssueDate, m.CertificateName, m.CertificateNumber, m.IssuingAuthority, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsContinue
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    AdmissionTime = model.AdmissionTime,
                    GraduationTime = model.GraduationTime,
                    EducationStage = model.EducationStage,
                    EducationTypes = model.EducationTypes,
                    IssueDate = model.IssueDate,
                    CertificateName = model.CertificateName,
                    CertificateNumber = model.CertificateNumber,
                    IssuingAuthority = model.IssuingAuthority,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsContinue>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 住房贷款利息支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateInterest(InterestDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsInterest>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.LocationHouse = model.LocationHouse;
                    oldinfo.BuildingNumber = model.BuildingNumber;
                    oldinfo.HouseType = model.HouseType;
                    oldinfo.HousingCertificate = model.HousingCertificate;
                    oldinfo.Borrower = model.Borrower;
                    oldinfo.Premarital = model.Premarital;
                    oldinfo.LoanType = model.LoanType;
                    oldinfo.LoanContractNo = model.LoanContractNo;
                    oldinfo.LendingBank = model.LendingBank;
                    oldinfo.FirstRepaymentDate = model.FirstRepaymentDate;
                    oldinfo.LoanTerm = model.LoanTerm;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsInterest>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.LocationHouse, m.BuildingNumber, m.HouseType, m.HousingCertificate, m.Borrower, m.Premarital, m.LoanType, m.LoanContractNo, m.LendingBank, m.FirstRepaymentDate, m.LoanTerm, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsInterest
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    LocationHouse = model.LocationHouse,
                    BuildingNumber = model.BuildingNumber,
                    HouseType = model.HouseType,
                    HousingCertificate = model.HousingCertificate,
                    Borrower = model.Borrower,
                    Premarital = model.Premarital,
                    LoanType = model.LoanType,
                    LoanContractNo = model.LoanContractNo,
                    LendingBank = model.LendingBank,
                    FirstRepaymentDate = model.FirstRepaymentDate,
                    LoanTerm = model.LoanTerm,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsInterest>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 住房租金支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateRent(RentDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsRent>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.WorkingCity = model.WorkingCity;
                    oldinfo.RentalType = model.RentalType;
                    oldinfo.HouseLocation = model.HouseLocation;
                    oldinfo.BuildingBrand = model.BuildingBrand;
                    oldinfo.FromLeaseDate = model.FromLeaseDate;
                    oldinfo.EndLeaseDate = model.EndLeaseDate;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsRent>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.WorkingCity, m.RentalType, m.HouseLocation, m.BuildingBrand, m.FromLeaseDate, m.EndLeaseDate, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsRent
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    WorkingCity = model.WorkingCity,
                    RentalType = model.RentalType,
                    HouseLocation = model.HouseLocation,
                    BuildingBrand = model.BuildingBrand,
                    FromLeaseDate = model.FromLeaseDate,
                    EndLeaseDate = model.EndLeaseDate,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsRent>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 赡养老人支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateSupport(SupportDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsSupport>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.Only = model.Only;
                    oldinfo.Share = model.Share;
                    oldinfo.MonthlyDeduction = model.MonthlyDeduction;
                    oldinfo.FosterName = model.FosterName;
                    oldinfo.FosterDocumentType = model.FosterDocumentType;
                    oldinfo.FosterCertificateNum = model.FosterCertificateNum;
                    oldinfo.FosterNationality = model.FosterNationality;
                    oldinfo.Relationship = model.Relationship;
                    oldinfo.DateOfBirth = model.DateOfBirth;
                    oldinfo.CoCultureName = model.CoCultureName;
                    oldinfo.CoCultureTypes = model.CoCultureTypes;
                    oldinfo.CoCultureNum = model.CoCultureNum;
                    oldinfo.CoCultureNationality = model.CoCultureNationality;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsSupport>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.Only, m.Share, m.MonthlyDeduction, m.FosterName, m.FosterDocumentType, m.FosterCertificateNum, m.FosterNationality, m.Relationship, m.DateOfBirth, m.CoCultureName, m.CoCultureTypes, m.CoCultureNum, m.CoCultureNationality, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsSupport
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    Only = model.Only,
                    Share = model.Share,
                    MonthlyDeduction = model.MonthlyDeduction,
                    FosterName = model.FosterName,
                    FosterDocumentType = model.FosterDocumentType,
                    FosterCertificateNum = model.FosterCertificateNum,
                    FosterNationality = model.FosterNationality,
                    Relationship = model.Relationship,
                    DateOfBirth = model.DateOfBirth,
                    CoCultureName = model.CoCultureName,
                    CoCultureTypes = model.CoCultureTypes,
                    CoCultureNum = model.CoCultureNum,
                    CoCultureNationality = model.CoCultureNationality,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsSupport>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加 更新 综合所得申报
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateIncome(IncomeDao model, long oid)
        {
            return await Task.Run(() =>
            {
                //修改
                if (model.Id > 0)
                {
                    var oldinfo = DbContext.FreeSql.Select<YssxNaturalpersonsIncome>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.CaseId = model.CaseId;
                    oldinfo.PersonnelId = model.PersonnelId;
                    oldinfo.Wages = model.Wages;
                    oldinfo.Pension = model.Pension;
                    oldinfo.MedicalCare = model.MedicalCare;
                    oldinfo.Unemployment = model.Unemployment;
                    oldinfo.AccumulationFund = model.AccumulationFund;
                    oldinfo.AccumulatedChildren = model.AccumulatedChildren;
                    oldinfo.AccumulatedInterest = model.AccumulatedInterest;
                    oldinfo.AccumulatedRental = model.AccumulatedRental;
                    oldinfo.AccumulatedSupport = model.AccumulatedSupport;
                    oldinfo.AccumulatedContinue = model.AccumulatedContinue;
                    oldinfo.WithholdingTax = model.WithholdingTax;
                    oldinfo.NetSalary = model.NetSalary;
                    oldinfo.UpdateBy = oid;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxNaturalpersonsIncome>().SetSource(oldinfo).UpdateColumns(m => new { m.CaseId, m.PersonnelId, m.Wages, m.Pension, m.MedicalCare, m.Unemployment, m.AccumulationFund, m.AccumulatedChildren, m.AccumulatedInterest, m.AccumulatedRental, m.AccumulatedSupport, m.AccumulatedContinue, m.WithholdingTax, m.NetSalary, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxNaturalpersonsIncome
                {
                    Id = IdWorker.NextId(),
                    CaseId = model.CaseId,
                    PersonnelId = model.PersonnelId,
                    Wages = model.Wages,
                    Pension = model.Pension,
                    MedicalCare = model.MedicalCare,
                    Unemployment = model.Unemployment,
                    AccumulationFund = model.AccumulationFund,
                    AccumulatedChildren = model.AccumulatedChildren,
                    AccumulatedInterest = model.AccumulatedInterest,
                    AccumulatedRental = model.AccumulatedRental,
                    AccumulatedSupport = model.AccumulatedSupport,
                    AccumulatedContinue = model.AccumulatedContinue,
                    WithholdingTax = model.WithholdingTax,
                    NetSalary = model.NetSalary,
                    CreateBy = oid
                };
                DbContext.FreeSql.GetRepository<YssxNaturalpersonsIncome>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }



        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BkDelete(IdDao model, long oId)
        {
            if (model.Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            return await Task.Run(() =>
            {
                var i = 0;
                switch (model.Type)
                {
                    case 1:
                    i = DbContext.FreeSql.Update<YssxNaturalpersonsPersonnel>()
                        .SetSource(new YssxNaturalpersonsPersonnel { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                        .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 2:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsChildren>()
                            .SetSource(new YssxNaturalpersonsChildren { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                            .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 3:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsContinue>()
                           .SetSource(new YssxNaturalpersonsContinue { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                           .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 4:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsInterest>()
                           .SetSource(new YssxNaturalpersonsInterest { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                           .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 5:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsRent>()
                           .SetSource(new YssxNaturalpersonsRent { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                           .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 6:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsSupport>()
                           .SetSource(new YssxNaturalpersonsSupport { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                           .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    case 7:
                        i = DbContext.FreeSql.Update<YssxNaturalpersonsIncome>()
                           .SetSource(new YssxNaturalpersonsIncome { Id = model.Id, IsDelete = CommonConstants.IsDelete, UpdateBy = oId, UpdateTime = DateTime.Now })
                           .Set(x => x.IsDelete, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();
                        break;

                    default:
                        break;
                }

                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "删除失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }



        /// <summary>
        /// 批量导入 自然人各模块
        /// </summary>
        /// <param name="form"></param>
        /// <param name="caseid"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ImportNaturalPersons(IFormFile form, long caseid, long oid)
        {
            #region 取文导入件数据
            var file = form;//?.FirstOrDefault();
            if (file == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "上传失败" };
            }

            string ss = file.FileName.Split('.')[1];
            var rNaturalPersonsData = new List<NaturalPersons>();
            using (var ms = file.OpenReadStream())
            {
                rNaturalPersonsData = NPOIHelper<NaturalPersons>.ImportNaturalPersons(ms, ss);
            }

            if (rNaturalPersonsData == null || !rNaturalPersonsData.Any())
            {
                return new ResponseContext<bool> { Code = CommonConstants.NotFund, Msg = "Excel中未存在任何有效数据" };
            }
            #endregion

            #region 初始化
            DateTime dtNow = DateTime.Now;
            var sb = new StringBuilder();
            var bMse = true;
            var PersonnelList = new List<YssxNaturalpersonsPersonnel>();
            var ChildrenList = new List<YssxNaturalpersonsChildren>();

            var ContinueList = new List<YssxNaturalpersonsContinue>();
            var InterestList = new List<YssxNaturalpersonsInterest>();
            var RentList = new List<YssxNaturalpersonsRent>();
            var SupportList = new List<YssxNaturalpersonsSupport>();
            var IncomeList = new List<YssxNaturalpersonsIncome>();
            #endregion

            foreach (var model in rNaturalPersonsData)
            {
                #region 校验
                if (bMse)
                {
                    if (string.IsNullOrWhiteSpace(model.Name))
                        sb.Append("姓名【不能为空】");

                    if (string.IsNullOrWhiteSpace(model.CertificateNum))
                        sb.Append("证件号码【不能为空】");

                    //检查是否有配偶
                    if (model.Spouse == 1)
                    {
                        if (string.IsNullOrWhiteSpace(model.SpouseName))
                            sb.Append("配偶姓名【不能为空】");

                        if (string.IsNullOrWhiteSpace(model.SpouseCertificateNum))
                            sb.Append("配偶证件号码【不能为空】");
                    }

                    bMse = false;
                }

                if (!string.IsNullOrEmpty(model.CertificateNum))
                {
                    var certificateNum = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(a => a.DocumentType == model.DocumentType && a.CertificateNum == model.CertificateNum && a.IsDelete == CommonConstants.IsNotDelete).First();
                    if (certificateNum != null)
                    {
                        sb.Append($"证件号码【{model.CertificateNum}已有】 ");
                    }
                }

                //检查配偶
                if (model.Spouse == 1)
                {
                    var SpousecertificateNum = DbContext.FreeSql.Select<YssxNaturalpersonsPersonnel>().Where(a => a.SpouseDocumentType == model.SpouseDocumentType && a.SpouseCertificateNum == model.SpouseCertificateNum && a.IsDelete == CommonConstants.IsNotDelete).First();
                    if (SpousecertificateNum != null)
                    {
                        sb.Append($"配偶证件号码【{model.CertificateNum}已有】 ");
                    }
                }

                //检查子女
                if (!string.IsNullOrEmpty(model.ChildNumber))
                {
                    var SpousecertificateNum = DbContext.FreeSql.Select<YssxNaturalpersonsChildren>().Where(a => a.ChildType == model.ChildType && a.ChildNumber == model.ChildNumber && a.IsDelete == CommonConstants.IsNotDelete).First();
                    if (SpousecertificateNum != null)
                    {
                        sb.Append($"子女证件号码【{model.CertificateNum}已有】 ");
                    }
                }
                #endregion

                #region 添加有效数据
                if (string.IsNullOrEmpty(sb.ToString()))
                {
                    var personnel = new YssxNaturalpersonsPersonnel()
                    {
                        Id = IdWorker.NextId(),
                        CaseId=caseid,
                        Department= model.Department,
                        Name = model.Name,
                        DocumentType= model.DocumentType,
                        CertificateNum= model.CertificateNum,

                        MobilePhone = model.MobilePhone,
                        Nationality = model.Nationality,
                        Employee = model.Employee,
                        Lonely = model.Lonely,
                        Investor = model.Investor,
                        SpecificIndustries = model.SpecificIndustries,
                        AngelInvestors = model.AngelInvestors,

                        Spouse = model.Spouse,
                        SpouseName= model.SpouseName,
                        SpouseDocumentType= model.SpouseDocumentType,
                        SpouseCertificateNum= model.SpouseCertificateNum,
                        StartTime= model.StartTime,
                        Withholding= model.Withholding,

                        CreateBy = oid,
                        CreateTime = dtNow,
                    };

                    var children = new YssxNaturalpersonsChildren()
                    {
                        Id = IdWorker.NextId(),
                        CaseId=caseid,
                        PersonnelId=personnel.Id,

                        ChildrenName= model.ChildrenName,
                        ChildType= model.ChildType,
                        ChildNumber= model.ChildNumber,
                        DateOfBirth = model.DateOfBirth,
                        Nationality = model.Nationality,
                        CurrentEducation = model.CurrentEducation,
                        EducationBegins = model.EducationBegins,
                        EducationEnd = model.EducationEnd,
                        EducationTimeEnd = model.EducationTimeEnd,
                        CountryStudy = model.CountryStudy,
                        CurrentSchool = model.CurrentSchool,
                        DeductionProportion = model.DeductionProportion,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    var Continue = new YssxNaturalpersonsContinue()
                    {
                        Id = IdWorker.NextId(),
                        CaseId = caseid,
                        PersonnelId = personnel.Id,

                        AdmissionTime = model.AdmissionTime,
                        GraduationTime = model.GraduationTime,
                        EducationStage = model.EducationStage,
                        EducationTypes = model.EducationTypes,
                        IssueDate = model.IssueDate,
                        CertificateName = model.CertificateName,
                        CertificateNumber = model.CertificateNumber,
                        IssuingAuthority = model.IssuingAuthority,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    var Interest = new YssxNaturalpersonsInterest()
                    {
                        Id = IdWorker.NextId(),
                        CaseId = caseid,
                        PersonnelId = personnel.Id,

                        LocationHouse = model.LocationHouse,
                        BuildingNumber = model.BuildingNumber,
                        HouseType = model.HouseType,
                        HousingCertificate = model.HousingCertificate,
                        Borrower = model.Borrower,
                        Premarital = model.Premarital,
                        LoanType = model.LoanType,
                        LoanContractNo = model.LoanContractNo,
                        LendingBank = model.LendingBank,
                        FirstRepaymentDate = model.FirstRepaymentDate,
                        LoanTerm = model.LoanTerm,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    var Rent = new YssxNaturalpersonsRent()
                    {
                        Id = IdWorker.NextId(),
                        CaseId = caseid,
                        PersonnelId = personnel.Id,

                        WorkingCity = model.WorkingCity,
                        RentalType = model.RentalType,
                        HouseLocation = model.HouseLocation,
                        BuildingBrand = model.BuildingBrand,
                        FromLeaseDate = model.FromLeaseDate,
                        EndLeaseDate = model.EndLeaseDate,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    var Support = new YssxNaturalpersonsSupport()
                    {
                        Id = IdWorker.NextId(),
                        CaseId = caseid,
                        PersonnelId = personnel.Id,

                        Only = model.Only,
                        Share = model.Share,
                        MonthlyDeduction = model.MonthlyDeduction,
                        FosterName = model.FosterName,
                        FosterDocumentType = model.FosterDocumentType,
                        FosterCertificateNum = model.FosterCertificateNum,
                        FosterNationality = model.FosterNationality,
                        Relationship = model.Relationship,
                        DateOfBirth = model.DateOfBirth,
                        CoCultureName = model.CoCultureName,
                        CoCultureTypes = model.CoCultureTypes,
                        CoCultureNum = model.CoCultureNum,
                        CoCultureNationality = model.CoCultureNationality,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    var Income = new YssxNaturalpersonsIncome()
                    {
                        Id = IdWorker.NextId(),
                        CaseId = caseid,
                        PersonnelId = personnel.Id,

                        Wages = model.Wages,
                        Pension = model.Pension,
                        MedicalCare = model.MedicalCare,
                        Unemployment = model.Unemployment,
                        AccumulationFund = model.AccumulationFund,
                        AccumulatedChildren = model.AccumulatedChildren,
                        AccumulatedInterest = model.AccumulatedInterest,
                        AccumulatedRental = model.AccumulatedRental,
                        AccumulatedSupport = model.AccumulatedSupport,
                        AccumulatedContinue = model.AccumulatedContinue,
                        WithholdingTax = model.WithholdingTax,
                        NetSalary = model.NetSalary,

                        CreateBy = oid,
                        CreateTime = dtNow
                    };

                    PersonnelList.Add(personnel);
                    ChildrenList.Add(children);

                    ContinueList.Add(Continue);
                    InterestList.Add(Interest);
                    RentList.Add(Rent);
                    SupportList.Add(Support);
                    IncomeList.Add(Income);
                }
                #endregion
            }
            
            return await Task.Run(() =>
            {
                if (PersonnelList.Any())
                {
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        DbContext.FreeSql.Insert<YssxNaturalpersonsPersonnel>().AppendData(PersonnelList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxNaturalpersonsChildren>().AppendData(ChildrenList).ExecuteAffrows();

                        DbContext.FreeSql.Insert<YssxNaturalpersonsContinue>().AppendData(ContinueList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxNaturalpersonsInterest>().AppendData(InterestList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxNaturalpersonsRent>().AppendData(RentList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxNaturalpersonsSupport>().AppendData(SupportList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxNaturalpersonsIncome>().AppendData(IncomeList).ExecuteAffrows();
                        uow.Commit();
                    }
                }
                var sbMsg = sb.ToString();
                var strMsg = (string.IsNullOrEmpty(sbMsg) ? string.Empty : $"错误：{sbMsg}") + $"成功导入{PersonnelList.Count()}条,失败{rNaturalPersonsData.Count() - PersonnelList.Count()}条,共{rNaturalPersonsData.Count()}条数据.";
                return new ResponseContext<bool> { Code = sbMsg == string.Empty ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = sbMsg == string.Empty ? true :false, Msg = strMsg };
            });
        }
    }
}

