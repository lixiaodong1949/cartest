﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.BackManage;

namespace Yssx.M.ServiceImpl
{
    public class bkUserService : IbkUserService
    {
        #region 前端用户
        /// <summary>
        /// 新增 更新 前端用户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUserinfo(YssxbkUserDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID
                if (md.Id.HasValue)
                {
                    //修改
                    var oldUserinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldUserinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    if (oldUserinfo.UserName != md.UserName)
                    {
                        var isExist = DbContext.FreeSql.Select<YssxUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete);
                        if (isExist)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改账号重复" };
                    }
                    oldUserinfo.UserName = md.UserName;
                    oldUserinfo.Password = md.Password.Md5();
                    oldUserinfo.UserType = md.UserType;
                    //oldUserinfo.ClientId = md.ClientId;
                    oldUserinfo.Email = md.Email;
                    oldUserinfo.MobilePhone = md.MobilePhone;
                    oldUserinfo.NikeName = System.Web.HttpUtility.UrlEncode(md.NikeName, Encoding.UTF8);
                    oldUserinfo.Photo = md.Photo;
                    oldUserinfo.QQ = md.QQ;
                    oldUserinfo.RealName = md.RealName;
                    oldUserinfo.Sex = md.Sex;
                    oldUserinfo.Status = md.Status;
                    oldUserinfo.WeChat = md.WeChat;
                    oldUserinfo.IdNumber = md.IdNumber;
                    oldUserinfo.TenantId = md.School;
                    oldUserinfo.UpdateBy = opreationId;
                    oldUserinfo.UpdateTime = DateTime.Now;

                    var i = DbContext.FreeSql.Update<YssxUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.UserName, m.Password, m.UserType, m.Email, m.MobilePhone, m.NikeName, m.Photo, m.QQ, m.RealName, m.Sex, m.Status, m.WeChat, m.IdNumber, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }

                //添加
                if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "添加账号重复" };
                var yssxUser = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    UserName = md.UserName,
                    Password = md.Password.Md5(),
                    UserType = md.UserType,
                    ClientId = Guid.NewGuid(),
                    Email = md.Email,
                    MobilePhone = md.MobilePhone,
                    NikeName = System.Web.HttpUtility.UrlEncode(md.NikeName, Encoding.UTF8),
                    Photo = md.Photo,
                    QQ = md.QQ,
                    RealName = md.RealName,
                    Sex = md.Sex,
                    Status = md.Status,
                    WeChat = md.WeChat,
                    IdNumber = md.IdNumber,
                    TenantId = md.School,
                    CreateBy = opreationId,
                    CreateTime = DateTime.Now
                };
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    var m = tran.GetRepository<YssxUser>().Insert(yssxUser);
                    if (m != null) tran.Commit();
                    else
                    {
                        tran.Rollback();
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false };
                    }
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 删除 前端用户
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteUserinfo(long Id, long currentUserId)
        {
            var dtNow = DateTime.Now;
            var userEntity = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (userEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该课业任务!" };
            userEntity.IsDelete = CommonConstants.IsDelete;
            userEntity.UpdateBy = currentUserId;
            userEntity.UpdateTime = dtNow;
            var studentEntity = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (studentEntity != null)
            {
                studentEntity.IsDelete = CommonConstants.IsDelete;
                studentEntity.UpdateBy = currentUserId;
                studentEntity.UpdateTime = dtNow;
            }
            var teacherEntity = await DbContext.FreeSql.GetRepository<YssxTeacher>().Where(x => x.UserId == Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (teacherEntity != null)
            {
                teacherEntity.IsDelete = CommonConstants.IsDelete;
                teacherEntity.UpdateBy = currentUserId;
                teacherEntity.UpdateTime = dtNow;
            }

            return await Task.Run(() =>
            {
                bool state = true;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //用户
                    state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.SetSource(userEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    //学生
                    if (studentEntity != null)
                        DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.SetSource(studentEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    //老师
                    if (teacherEntity != null)
                        DbContext.FreeSql.Update<YssxTeacher>().SetSource(teacherEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();

                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取 前端用户【废弃】
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<YssxUserResponse>>> FindUserByName(YssxQuerybkUserDao model)
        {
            var select = DbContext.FreeSql.Select<YssxUser>().From<YssxActivationInformation, YssxSchool, YssxCollege, YssxProfession, YssxClass>
                ((a, b, c, d, e, f) => a.LeftJoin(aa => aa.Id == b.CreateBy && b.IsDelete == CommonConstants.IsNotDelete).LeftJoin(aa => b.TenantId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(aa => b.DepartmentsId == d.Id && d.IsDelete == CommonConstants.IsNotDelete).LeftJoin(aa => b.ProfessionalId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(aa => b.ClassId == f.Id && f.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b, c, d, e, f) => a.CreateTime);
            //.LeftJoin(aa => aa.DepartmentsId == c.Id)
            //.LeftJoin(aa => aa.ProfessionalId == d.Id)
            //.LeftJoin(aa => aa.ClassId == e.Id))
            //.Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b, c, d, e,f) => a.Id);

            //筛选 用户
            if (model.SchoolId != null)
                select = select.Where((a, b, c, d, e, f) => a.TenantId == model.SchoolId);
            if (model.UserType != null)
                select = select.Where((a, b, c, d, e, f) => a.UserType == model.UserType);
            if (model.Status != null)
                select = select.Where((a, b, c, d, e, f) => a.Status == model.Status);
            if (!string.IsNullOrEmpty(model.UserName))
                select = select.Where((a, b, c, d, e, f) => a.RealName.Contains(model.UserName) || a.MobilePhone.Contains(model.UserName));

            var totalCount = select.Count();
            var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,c.Name as SchoolName,d.Name as PresidentName,e.Name as CollegeIdName,f.Name as ProfessionName,b.WorkNumber StudentNo,f.EntranceDateTime");
            var items = DbContext.FreeSql.Ado.Query<YssxUserResponse>(sql);

            var res = new PageResponse<YssxUserResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return new ResponseContext<PageResponse<YssxUserResponse>> { Code = CommonConstants.SuccessCode, Data = res };
        }

        /// <summary>
        /// 获取用户列表（分页）
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<UserDto>> GetUserList(GetUserListRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxUser>()
                    .From<YssxSchool, YssxStudent, YssxTeacher>((a, b, c, d) =>
                    a.InnerJoin(aa => aa.TenantId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.Id == c.UserId && c.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.Id == d.UserId && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, b, c, d) => a.CreateTime);

                #region 筛选条件
                if (model.TenantId != null && model.TenantId.HasValue)
                    select = select.Where((a, b, c, d) => a.TenantId == model.TenantId);
                if (model.UserType != null && model.UserType.HasValue)
                    select = select.Where((a, b, c, d) => a.UserType == model.UserType);
                if (model.Status != null && model.Status.HasValue)
                    select = select.Where((a, b, c, d) => a.Status == model.Status);
                if (!string.IsNullOrEmpty(model.UserName))
                    select = select.Where((a, b, c, d) => a.RealName.Contains(model.UserName) || a.MobilePhone.Contains(model.UserName));
                if (model.IsDisplay > -1)
                    select = select.Where((a, b, c, d) => b.IsDisplay == model.IsDisplay);
                #endregion

                #region 取数据
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id, a.RealName, a.Sex, b.Name SchoolName, c.StudentNo, d.TeacherNo, a.Email, a.IdNumber, a.MobilePhone, a.CreateTime RegisterTime, a.UserType, a.Status");
                var selectData = DbContext.FreeSql.Ado.Query<UserDto>(sql).ToList();
                #endregion

                return new PageResponse<UserDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询用户统计列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GetUserCountByPageDto>> GetUserCountByPage(GetUserCountByPageRequest model)
        {
            return await Task.Run(() =>
            {
                GetUserCountByPageDto resData = new GetUserCountByPageDto();
                resData.TotalCount = DbContext.FreeSql.Select<YssxUser>()
                    .From<YssxSchool>((a, b) => a.InnerJoin(aa => aa.TenantId == b.Id && b.IsDisplay == 0 && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).Count();
                //取学校数据
                var selectData = DbContext.FreeSql.Select<YssxSchool>().Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.IsDisplay == 0)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), a => a.Name == model.Name)
                    .OrderByDescending(a => a.CreateTime)
                    .ToList(a => new GetUserCountByPageDetail
                    {
                        Id = a.Id,
                        SchoolName = a.Name,
                        UserCount = 0,
                        StudentCount = 0,
                        TeacherCount = 0
                    });
                var recordCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                //取学生、老师统计数据
                foreach (var item in selectData)
                {
                    var rSchoolUser = DbContext.FreeSql.Select<YssxUser>().Where(m => m.TenantId == item.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    item.UserCount = rSchoolUser.Count;
                    item.StudentCount = rSchoolUser.Where(m => m.UserType == (int)UserTypeEnums.Student).Count();
                    item.TeacherCount = rSchoolUser.Where(m => m.UserType == (int)UserTypeEnums.Teacher).Count();
                }
                resData.SchoolDetail = selectData;

                resData.PageSize = model.PageSize;
                resData.PageIndex = model.PageIndex;
                resData.RecordCount = recordCount;

                return new ResponseContext<GetUserCountByPageDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }


        /// <summary>
        /// 重置 前端用户密码
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ResetPassword(ResetPassword md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID
                //修改
                var oldUserinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (oldUserinfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "重置失败，找不到原始信息!" };
                oldUserinfo.Password = md.Password.Md5();
                oldUserinfo.UpdateBy = opreationId;
                oldUserinfo.UpdateTime = DateTime.Now;

                var state = DbContext.FreeSql.Update<YssxUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.Password, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                if (state)
                {
                    RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{oldUserinfo.MobilePhone}");
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "成功!" : "重置失败!", Data = state };
            });
        }

        /// <summary>
        /// 前端用户状态 启用禁用
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> StateControl(StateControl md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID
                //修改
                var oldUserinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == md.Id).First();
                if (oldUserinfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "状态修改失败，找不到原始信息!" };
                oldUserinfo.Status = md.Status;
                oldUserinfo.UpdateBy = opreationId;
                oldUserinfo.UpdateTime = DateTime.Now;

                var i = DbContext.FreeSql.Update<YssxUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.Status, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "状态修改失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 批量生成 前端用户===
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddBatchUserinfo(YssxbkUserDao md)
        {
            return await Task.Run(() =>
            {
                var YssxbkUser = new YssxbkUser
                {
                    //Id = mb.Id,
                    UserName = md.UserName,
                    Password = md.Password,
                    UserType = (int)md.UserType
                    //IsDelete = md.IsDelete
                };
                DbContext.FreeSql.Insert<YssxbkUser>(YssxbkUser);

                return new ResponseContext<bool> { Data = true };
            });
        }
        #endregion

        #region 激活码
        /// <summary>
        /// 随机 生成学校激活码（输入12位机构代号(数字+字母)）
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxRandomCodeDao>> GetRandomCodeDao()
        {
            return await Task.Run(() =>
            {
                var yssxRandomCode = new YssxRandomCodeDao
                {
                    RandomCodeDao = RandomChars.NumChar(12, Framework.Utils.RandomChars.UppLowType.upper)
                };
                return new ResponseContext<YssxRandomCodeDao> { Code = CommonConstants.SuccessCode, Data = yssxRandomCode };
            });
        }

        public async Task<ResponseContext<bool>> AddStudentActivCode(YssxActivationCodeDto dto, long uid)
        {
            if (dto.ResourceIds == null || dto.ResourceIds.Count == 0)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"请选择至少一个套餐资源包！" };
            }
            bool isExist = await DbContext.FreeSql.Select<YssxActivationCode>().Where(x => x.Code == dto.Code).AnyAsync();
            if (isExist)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"激活码已存在!" };
            }

            string rids = string.Join(",", dto.ResourceIds);

            var codeType = new YssxActivationCode
            {
                Id = IdWorker.NextId(),
                ActivatedNumber = 0,
                Code = dto.Code,
                EndTime = dto.EndTime,
                Number = dto.Number,
                SchoolId = dto.SchoolId,
                StartTime = dto.StartTime,
                TypeId = dto.TypeId,
                State = 1,
                CreateBy = uid,
                CreateTime = DateTime.Now,
                TypeName = dto.TypeName,
                SchoolName = dto.SchoolName,
                Year = dto.Year,
                ResourceId = rids,
                ActivationDeadline = Convert.ToDateTime(dto.ActivationDeadline.ToString("yyyy-MM-dd") + " 23:59:59")
            };
            await DbContext.FreeSql.GetRepository<YssxActivationCode>().InsertAsync(codeType);
            await AddExamPapers(dto, uid);
            return new ResponseContext<bool> { Data = true };

        }

        private async Task<bool> AddExamPapers(YssxActivationCodeDto dto, long uid)
        {
            #region 处理数据

            //订单-套餐案例
            var listResourceCase = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && dto.ResourceIds.Contains(x.ResourceId)).ToListAsync();
            var listSchoolCase = listResourceCase.Select(m => new YssxSchoolCase
            {
                Id = IdWorker.NextId(),
                OrderId = 0,
                SchoolId = dto.SchoolId,
                CaseId = m.CaseId,
                CaseName = m.CaseName,
                //Education = dto.e,
                ModuleId = m.ModuleType,
                CaseUserType = CaseUserTypeEnums.Educational,
                UserName = "activ_case",
                OrderUser = OrderUserEnum.StudentAndTeacher,
                OpenTime = DateTime.Now,
                ExpireTime = dto.EndTime,
                PayStatus = PayStatus.Paid,
                PersonInCharge = "专业组",
                OpenStatus = Status.Enable,
                TestStatus = 1,
                OfficialStatus = 1,
                Sort = 1,
                CreateBy = uid,

            }).ToList();
            //试卷
            var rCaseInfoData = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Select.From<YssxCase, YssxTopic>((a, b, c) => a
                .LeftJoin(aa => aa.CaseId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(aa => b.Id == c.CaseId && c.ParentId == 0 && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { a.Id, a.CaseId, b.Sort, b.CompetitionType, b.TotalTime })
                .ToListAsync(a => new
                {
                    a.Key.CaseId,
                    a.Key.Sort,
                    a.Value.Item2.CompetitionType,
                    a.Value.Item2.TotalTime,
                    TotalQuestionCount = "Count(c.id)",
                    TotalScore = a.Sum(a.Value.Item3.Score)
                });
            var listExamPaper = new List<ExamPaper>();
            var setExamPaper = listSchoolCase.Where(x => x.ModuleId == 1 || x.ModuleId == 3 || x.ModuleId == 4 || x.ModuleId == 5).ToList();

            foreach (var item in setExamPaper)
            {
                var rCaseInfo = rCaseInfoData.First(m => m.CaseId == item.CaseId);
                if (rCaseInfo == null)
                    continue;

                var testExamPaper = new ExamPaper()
                {
                    Id = IdWorker.NextId(),
                    CanShowAnswerBeforeEnd = true,
                    CaseId = item.CaseId,
                    Name = item.CaseName,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = rCaseInfo.CompetitionType,
                    TotalMinutes = rCaseInfo.TotalTime,
                    TotalScore = rCaseInfo.TotalScore,
                    TotalQuestionCount = Convert.ToInt32(rCaseInfo.TotalQuestionCount),
                    TenantId = dto.SchoolId,
                    ExamSourceType = ExamSourceType.Order,
                    Sort = rCaseInfo.Sort,
                    IsRelease = true,
                    Year = dto.Year,
                    CreateBy = uid,
                };
                listExamPaper.Add(testExamPaper);
                //案例是团队赛时, 生成全真模拟试卷
                if (rCaseInfo.CompetitionType == CompetitionType.TeamCompetition)
                {
                    var emulationExamPaper = testExamPaper.DeepClone();
                    emulationExamPaper.Id = IdWorker.NextId();
                    emulationExamPaper.ExamType = ExamType.EmulationTest;
                    emulationExamPaper.CanShowAnswerBeforeEnd = false;
                    listExamPaper.Add(emulationExamPaper);
                }
            }

            await DbContext.FreeSql.GetRepository<YssxSchoolCase>().InsertAsync(listSchoolCase);
            await DbContext.FreeSql.GetRepository<ExamPaper>().InsertAsync(listExamPaper);
            #endregion

            return true;
        }

        /// <summary>
        /// 添加 更新 禁用 学校激活码
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddActivationCode(YssxActivationCodeListDto md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                foreach (var item in md.CodeList)
                {
                    if (item.Code.Length < 12)
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"激活码小于12位，类型{item.TypeName}!" };
                    }

                    if (item.Id.HasValue)
                    {
                        //修改
                        var oldActivationCode = DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.Id == item.Id).First();//&& m.IsDelete == CommonConstants.IsNotDelete
                        if (oldActivationCode == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                        oldActivationCode.SchoolId = item.SchoolId;
                        oldActivationCode.State = item.State;
                        oldActivationCode.Remarks = item.Remarks;
                        oldActivationCode.UpdateTime = DateTime.Now;
                        oldActivationCode.UpdateBy = opreationId;
                        oldActivationCode.Number = item.Number;
                        oldActivationCode.StartTime = item.StartTime;
                        oldActivationCode.EndTime = item.EndTime;
                        oldActivationCode.ActivationDeadline = item.ActivationDeadline;
                        oldActivationCode.Code = item.Code;

                        DbContext.FreeSql.Update<YssxActivationCode>().SetSource(oldActivationCode).UpdateColumns(m => new { m.State, m.Remarks, m.UpdateTime, m.UpdateBy, m.Code, m.ActivationDeadline, m.EndTime, m.StartTime, m.Number }).ExecuteAffrows();
                        return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                    }
                    var code = DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.Code == item.Code).First();
                    if (code != null)
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"激活码已存在，类型{item.TypeName}!" };
                    }
                    using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        var codeType = new YssxActivationCode
                        {
                            Id = IdWorker.NextId(),
                            ActivatedNumber = 0,
                            Code = item.Code,
                            EndTime = item.EndTime,
                            Number = item.Number,
                            SchoolId = item.SchoolId,
                            StartTime = item.StartTime,
                            TypeId = item.TypeId,
                            State = 1,
                            CreateBy = opreationId,
                            CreateTime = DateTime.Now,
                            TypeName = item.TypeName,
                            SchoolName = item.SchoolName,
                            ActivationDeadline = Convert.ToDateTime(item.ActivationDeadline.ToString("yyyy-MM-dd") + " 23:59:59")
                        };
                        var m = tran.GetRepository<YssxActivationCode>().Insert(codeType);

                        if (m != null) tran.Commit();
                        else
                        {
                            tran.Rollback();
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false };
                        }
                    }
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };

            });
        }

        /// <summary>
        /// 获取 学校激活码
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCode(YssxQueryActivationCode model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxActivationCode>().From<YssxSchool>((a, b) => a.LeftJoin(aa => aa.SchoolId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b) => a.Id);
                //筛选 学校
                if (model.schoolId != null)
                    select = select.Where((a, b) => a.SchoolId == (int)model.schoolId);
                if (!string.IsNullOrEmpty(model.schoolName))
                    select = select.Where((a, b) => b.Name.Contains(model.schoolName));
                if (!string.IsNullOrEmpty(model.Code))
                    select = select.Where((a, b) => a.Code.Contains(model.Code));
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id, a.Remarks, a.TypeId, a.TypeName, a.Code, a.StartTime, a.EndTime, a.SchoolId, a.Number, a.ActivatedNumber, a.ActivationDeadline, a.State, b.Name as SchoolName");
                var selectData = DbContext.FreeSql.Ado.Query<YssxActivationCodeDto>(sql);

                var res = new PageResponse<YssxActivationCodeDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
                return new ResponseContext<PageResponse<YssxActivationCodeDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        public async Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCodeByTypeId(YssxQueryActivationCode model)
        {
            var select = DbContext.FreeSql.Select<YssxActivationCode>().From<YssxSchool>((a, b) => a.LeftJoin(aa => aa.SchoolId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.TypeId == 1).OrderByDescending((a, b) => a.Id);
            //筛选 学校
            if (model.schoolId != null)
                select = select.Where((a, b) => a.SchoolId == (int)model.schoolId);
            if (!string.IsNullOrEmpty(model.schoolName))
                select = select.Where((a, b) => b.Name.Contains(model.schoolName));
            if (!string.IsNullOrEmpty(model.Code))
                select = select.Where((a, b) => a.Code.Contains(model.Code));
            var totalCount = select.Count();
            var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id, a.Remarks, a.TypeId, a.TypeName, a.Code, a.StartTime, a.EndTime, a.SchoolId, a.Number, a.ActivatedNumber, a.ActivationDeadline, a.State, b.Name as SchoolName");
            var selectData =await DbContext.FreeSql.Ado.QueryAsync<YssxActivationCodeDto>(sql);

            var res = new PageResponse<YssxActivationCodeDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            return new ResponseContext<PageResponse<YssxActivationCodeDto>> { Code = CommonConstants.SuccessCode, Data = res };
        }

        /// <summary>
        /// 删除 学校激活码
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteActivationCode(long Id, long oId)
        {
            long opreationId = oId;//操作人ID

            //TODO 租户过滤
            var code = DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.Id == Id).First();
            if (code == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var activationCode = new YssxActivationCode
            {
                Id = Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxActivationCode>().SetSource(activationCode).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 导出 学校激活码
        /// </summary>
        /// <returns></returns>      
        public async Task<IActionResult> ExportActivationCode()//string name
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxActivationCode>().Where((a) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a) => a.Id);
                //筛选学校、院系
                //if (!String.IsNullOrEmpty(name))
                //    select = select.Where((a, b, c) => a.Name.Contains(name));

                var totalCount = select.Count();
                var sql = select.ToSql("a.school,a.EducationalCode,a.EducationalNumber,a.TeacherCode,a.TeacherNumber,a.StudentCode,a.StudentNumber,a.ActivationDeadline,a.Remarks");


                var selectData = DbContext.FreeSql.Ado.Query<ExportActivationC>(sql);
                byte[] ms = NPOIHelper<ExportActivationC>.OutputExcel(selectData, new string[] { "激活码" });

                var provider = new FileExtensionContentTypeProvider();
                if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
                {
                    throw new Exception("找不到可导出的文件类型");
                }
                //return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
                return new FileContentResult(ms, memi);
            });
        }
        #endregion

        #region 后端用户
        /// <summary>
        /// 添加 更新 后端用户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddBackUserinfo(YssxbkbkUserDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (md.Id.HasValue)
                {
                    //修改
                    var oldUserinfo = DbContext.FreeSql.Select<YssxbkUser>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldUserinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    if (oldUserinfo.UserName != md.UserName)
                    {
                        var isExist = DbContext.FreeSql.Select<YssxbkUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete);
                        if (isExist)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改账号重复" };
                    }
                    oldUserinfo.UserName = md.UserName;
                    oldUserinfo.Password = md.Password.Md5();
                    oldUserinfo.UpdateBy = opreationId;
                    oldUserinfo.UpdateTime = DateTime.Now;


                    DbContext.FreeSql.Update<YssxbkUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.UserName, m.Password, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }

                //添加
                if (DbContext.FreeSql.Select<YssxbkUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "添加账号重复" };
                var yssxbkUser = new YssxbkUser
                {
                    Id = IdWorker.NextId(),
                    UserName = md.UserName,
                    Password = md.Password.Md5()
                };
                DbContext.FreeSql.GetRepository<YssxbkUser>().Insert(yssxbkUser);
                //DbContext.FreeSql.Insert<YssxbkUser>(yssxbkUser).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 重置 后端用户密码
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BkResetPassword(BkResetPassword md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID
                                      //修改
                var oldUserinfo = DbContext.FreeSql.Select<YssxbkUser>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (oldUserinfo == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "重置失败，找不到原始信息!" };
                else
                {
                    if (oldUserinfo.Password != md.Password.Md5()) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "原始密码不正确!" };
                    if (md.NewPassword != md.ConfirmPassword) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "新密码和确定密码不一致!" };
                }
                oldUserinfo.Password = md.NewPassword.Md5();
                oldUserinfo.UpdateBy = opreationId;
                oldUserinfo.UpdateTime = DateTime.Now;

                var i = DbContext.FreeSql.Update<YssxbkUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.Password, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "重置失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 查找 用户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> FindUser(YssxbkbkUserDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (md.Id.HasValue)
                {
                    //修改
                    var oldUserinfo = DbContext.FreeSql.Select<YssxbkUser>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldUserinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    if (oldUserinfo.UserName != md.UserName)
                    {
                        var isExist = DbContext.FreeSql.Select<YssxbkUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete);
                        if (isExist)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改账号重复" };
                    }
                    oldUserinfo.UserName = md.UserName;
                    oldUserinfo.Password = md.Password.Md5();
                    oldUserinfo.UpdateBy = opreationId;
                    oldUserinfo.UpdateTime = DateTime.Now;


                    DbContext.FreeSql.Update<YssxbkUser>().SetSource(oldUserinfo).UpdateColumns(m => new { m.UserName, m.Password, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }

                //添加
                if (DbContext.FreeSql.Select<YssxbkUser>().Any(m => m.UserName == md.UserName && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "添加账号重复" };
                var yssxbkUser = new YssxbkUser
                {
                    Id = IdWorker.NextId(),
                    UserName = md.UserName,
                    Password = md.Password.Md5()
                };
                DbContext.FreeSql.GetRepository<YssxbkUser>().Insert(yssxbkUser);
                //DbContext.FreeSql.Insert<YssxbkUser>(yssxbkUser).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #endregion

        #region 绑定手机号、微信
        /// <summary>
        /// 绑定手机号
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindPhoneByUser(string mobilePhone, string verificationCode, long currentUserId)
        {
            //校验验证码
            var rVerifyCode = DbContext.FreeSql.Select<YssxVerifyCode>().Where(m => m.MobilePhone == mobilePhone).OrderByDescending(x => x.CreateTime).First();
            if (rVerifyCode == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码，请确认手机号码是否正确", Data = false };
            else if (rVerifyCode.Code != verificationCode)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码错误", Data = false };
            else if (DateTime.Now > rVerifyCode.ExpirTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码过期", Data = false };
            //校验手机号重复
            var rIsRegistered = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id != currentUserId && m.MobilePhone == mobilePhone && m.IsDelete == CommonConstants.IsNotDelete).Any();
            if (rIsRegistered)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该手机号已绑定其他账号，如要绑定新账号，请将手机号与旧账号解绑", Data = false };

            var userEntity = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
            var rOldMobilePhone = userEntity.MobilePhone;
            if (rOldMobilePhone == mobilePhone)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该手机号已绑定当前账号，无需重复绑定", Data = false };

            userEntity.UserName = mobilePhone;
            userEntity.MobilePhone = mobilePhone;
            userEntity.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                bool state = DbContext.FreeSql.Update<YssxUser>().SetSource(userEntity).UpdateColumns(m => new { m.UserName, m.UpdateTime, m.MobilePhone }).ExecuteAffrows() > 0;
                if (!string.IsNullOrEmpty(rOldMobilePhone))
                    RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{rOldMobilePhone}");

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "绑定成功" : "绑定失败!" };
            });
        }

        /// <summary>
        /// 绑定微信
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindWechartByUser(string wechart, string unionid, long currentUserId)
        {
            //校验微信号
            if (string.IsNullOrEmpty(wechart) || string.IsNullOrEmpty(unionid))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "绑定失败，请重新授权" };
            var rIsRegistered = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id != currentUserId && m.IsDelete == CommonConstants.IsNotDelete && (m.WeChat == wechart || m.Unionid == unionid)).Any();
            if (rIsRegistered)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该微信已绑定其他账号，如要绑定新账号，请将微信与旧账号解绑", Data = false };

            var userEntity = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
            var rOldUnionid = userEntity.Unionid;
            var rOldWeChat = userEntity.WeChat;
            if (rOldUnionid == unionid || rOldWeChat == wechart)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该微信已绑定当前账号，无需重复绑定", Data = false };

            userEntity.Unionid = unionid;
            userEntity.WeChat = wechart;
            userEntity.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                bool state = DbContext.FreeSql.Update<YssxUser>().SetSource(userEntity).UpdateColumns(m => new { m.UpdateTime, m.WeChat, m.Unionid }).ExecuteAffrows() > 0;
                if (!string.IsNullOrEmpty(rOldWeChat))
                    RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{rOldWeChat}");

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "绑定成功" : "绑定失败!" };
            });
        }



        #endregion


        public async Task<ResponseContext<List<PartnerDto>>> GetPartnerList()
        {
            List<PartnerDto> list = await DbContext.FreeSql.GetRepository<YssxPartner>().Where(x => x.Id > 0).ToListAsync<PartnerDto>();

            return new ResponseContext<List<PartnerDto>> { Data = list };
        }

        public async Task<ResponseContext<bool>> SetPartnerStatus(long id)
        {
            await DbContext.FreeSql.GetRepository<YssxPartner>().UpdateDiy.Set(x => x.Status, 1).Where(x => x.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<bool>> AddUserList(UserListInfoDto dto)
        {
            List<YssxUser> yssxUsers = new List<YssxUser>();
            List<YssxTeacher> yssxStudents = new List<YssxTeacher>();
            List<YssxStudent> students = new List<YssxStudent>();
            for (int i = 1; i <= dto.Number; i++)
            {
                YssxUser u = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    ClientId = Guid.NewGuid(),
                    MobilePhone = $"{dto.UserName}{i}",
                    NikeName = $"{dto.UserName}{i}",
                    Password = dto.Password.Md5(),//"E10ADC3949BA59ABBE56E057F20F883E", //x.Password,
                    TenantId = dto.SchoolId,
                    UserType = dto.UserType,
                    Status = 1,
                    RegistrationType = 2,
                    IsDelete = 0,
                    CreateTime = DateTime.Now,
                    CreateBy = 0
                };

                yssxUsers.Add(u);

                if (dto.UserType == 2)
                {
                    YssxTeacher s = new YssxTeacher
                    {
                        Id = IdWorker.NextId(),
                        UserId = u.Id,
                        Name = u.NikeName,
                        TenantId = dto.SchoolId,
                        CreateTime = DateTime.Now

                    };
                    yssxStudents.Add(s);
                }
                else if (dto.UserType==1)
                {
                    YssxStudent student = new YssxStudent
                    {
                        Id = IdWorker.NextId(),
                        UserId = u.Id,
                        Name = u.NikeName,
                        TenantId = dto.SchoolId,
                        CreateTime = DateTime.Now

                    };
                    students.Add(student);
                }
            }


            List<YssxUser> e = await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(yssxUsers);
            if (dto.UserType == 2)
            {
                List<YssxTeacher> f = await DbContext.FreeSql.GetRepository<YssxTeacher>().InsertAsync(yssxStudents);
            }
            else if (dto.UserType == 1)
            {
                await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(students);
            }

            return new ResponseContext<bool> { };
        }
    }
}