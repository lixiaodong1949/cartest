﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 学生管理
    /// </summary>
    public class StudentService : IStudentService
    {
        /// <summary>
        /// 学生信息审核
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> StudentAudit(StudentDto model, long Id)
        {
            long opreationId = Id;//CurrentUserTicket.Id;//操作人ID

            if (!model.Id.HasValue)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生ID信息为空!" };
            }
            var user = DbContext.FreeSql.Select<YssxUser>().InnerJoin<YssxStudent>((a, b) => a.Id == b.UserId && b.Id == model.Id && a.IsDelete == CommonConstants.IsNotDelete).First();
            if (user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到学生信息!" };
            user.UpdateTime = DateTime.Now;
            user.UpdateBy = opreationId;
            user.Status = (int)model.Status;
            return await Task.Run(() =>
            {
                var rst = DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows() > 0;
                if (rst)
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
            });

        }

        /// <summary>
        /// 添加 更新 学生
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateStudent(StudentDto model, long Id)
        {
            long opreationId = Id;//操作人ID
            DateTime dtNow = DateTime.Now;
            #region 验证
            if (model.TenantId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校不能为空" };
            if (model.CollegeId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "院系不能为空" };
            if (model.ProfessionId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "专业不能为空" };
            if (model.ClassId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空" };

            if (String.IsNullOrEmpty(model.RealName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "真实姓名不能为空" };
            if (String.IsNullOrEmpty(model.Email))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "邮箱不能为空" };
            if (String.IsNullOrEmpty(model.MobilePhone))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码不能为空" };
            if (String.IsNullOrEmpty(model.IdNumber))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "身份证号码不能为空" };
            if (String.IsNullOrEmpty(model.StudentNo))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学号不能为空" };

            if (!model.Id.HasValue || model.Id == 0)
            {
                if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == model.MobilePhone && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码重复" };
                if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.IdNumber == model.IdNumber && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "身份证号码重复" };
                //if (DbContext.FreeSql.Select<YssxStudent>().Any(m => m.StudentNo == model.StudentNo && m.IsDelete == CommonConstants.IsNotDelete))
                //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学号重复" };
            }

            #endregion

            if (model.Id.HasValue && model.Id > 0)
            {
                return await Task.Run(() =>
                {
                    return UpdateStudent(model, Id);
                });
            }

            var user = new YssxUser
            {
                Id = IdWorker.NextId(),
                MobilePhone = model.MobilePhone,
                IdNumber = model.IdNumber,
                RealName = model.RealName,
                UserName = model.UserName,
                Password = CommonConstants.NewUserDefaultPwd.Md5(),
                TenantId = model.TenantId,
                Status = (int)Status.Enable,
                UserType = (int)UserTypeEnums.Student,
                Email = model.Email,
                ClientId = Guid.NewGuid(),
                CreateBy = opreationId,
                CreateTime = dtNow
            };
            var stu = new YssxStudent
            {
                Id = IdWorker.NextId(),
                TenantId = model.TenantId,
                UserId = user.Id,
                ProfessionId = model.ProfessionId,
                ClassId = model.ClassId,
                CollegeId = model.CollegeId,
                StudentNo = model.StudentNo,
                Name = model.RealName,
                CreateBy = opreationId,
                CreateTime = dtNow
            };
            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    tran.GetRepository<YssxUser>().Insert(user);
                    tran.GetRepository<YssxStudent>().Insert(stu);
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdateStudent(StudentDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            var student = DbContext.FreeSql.Select<YssxStudent>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (student == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            var user = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == student.UserId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生基本信息异常!" };

            if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == model.MobilePhone && m.Id != student.UserId && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码重复" };

            if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.IdNumber == model.IdNumber && m.Id != student.UserId && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "身份证号码重复" };

            if (DbContext.FreeSql.Select<YssxStudent>().Any(m => m.StudentNo == model.StudentNo && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学号重复" };

            var now = DateTime.Now;

            student.UpdateTime = DateTime.Now;
            student.UpdateBy = opreationId;
            student.Name = model.RealName;
            student.ClassId = model.ClassId;
            student.StudentNo = model.StudentNo;

            user.UpdateBy = opreationId;
            user.UpdateTime = now;
            user.MobilePhone = model.MobilePhone;
            user.IdNumber = model.IdNumber;
            user.RealName = model.RealName;
            user.UserName = model.UserName;
            user.Email = model.Email;
            user.Sex = model.Sex == Sex.Man ? "男" : "女";

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxStudent>().SetSource(student).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.Name, m.ClassId, m.StudentNo }).ExecuteAffrows();
                DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.MobilePhone, m.IdNumber, m.RealName, m.UserName, m.Email, m.Sex }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 获取学生列表
        /// </summary>
        public async Task<ResponseContext<StudentDtoSummary>> GetStudents(GetStudentRequest model, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID
                long opreationType = 0;//操作人-用户类型

                #region 取当前老师的学生数据
                if (opreationType == (int)UserTypeEnums.Teacher)//添加教师ID过滤,2019-7-15
                {
                    var teacherId = DbContext.FreeSql.Select<YssxTeacher>().Where(a => a.UserId == opreationId).First(a => a.Id);
                    if (teacherId == 0)
                    {
                        return new ResponseContext<StudentDtoSummary>(CommonConstants.ErrorCode, "未找到相关教师");
                    }
                    model.TeacherId = teacherId;
                }

                if (model.TeacherId > 0)
                {
                    return GetStudentsByTid(model);
                }
                #endregion
                //Done 租户过滤
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxClass, YssxProfession, YssxCollege, YssxSchool, YssxUser>((a, b, c, d, e, f) =>
                    a.InnerJoin(aa => aa.ClassId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.ProfessionId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.CollegeId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.TenantId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.UserId == f.Id && f.IsDelete == CommonConstants.IsNotDelete))
                    .WhereIf(model.TenantId > 0, (a, b, c, d, e, f) => a.TenantId == model.TenantId)
                    .WhereIf(model.CollegeId > 0, (a, b, c, d, e, f) => a.CollegeId == model.CollegeId)
                    .WhereIf(model.ProfessionId > 0, (a, b, c, d, e, f) => a.ProfessionId == model.ProfessionId)
                    .WhereIf(model.ClassId > 0, (a, b, c, d, e, f) => a.ClassId == model.ClassId)
                    .WhereIf(model.EntranceYear > 0, (a, b, c, d, e, f) => b.EntranceDateTime.Year == model.EntranceYear)
                    .Where((a, b, c, d, e, f) => f.UserType == (int)UserTypeEnums.Student && a.IsDelete == CommonConstants.IsNotDelete);

                if (!string.IsNullOrWhiteSpace(model.KeyWord))
                    select = select.Where((a, b, c, d, e, f) => f.RealName.Contains(model.KeyWord) || f.MobilePhone.Contains(model.KeyWord) || a.StudentNo.Contains(model.KeyWord));

                var totalCount = select.Count();
                var classIdsArry = select.ToList((a, b, c, d, e, f) => a.ClassId);
                var classCount = classIdsArry.Distinct().Count();
                var sql = select.OrderByDescending((a, b, c, d, e, f) => a.CreateTime).Page(model.PageIndex, model.PageSize).ToSql("a.*,b.Name ClassName,c.Name ProfessionName," +
                    "d.Name CollegeName,e.Name SchoolName,f.RealName,f.MobilePhone,f.IdNumber,f.Sex,f.Email,f.OtherContacts,b.EntranceDateTime,f.Status");

                var selectData = DbContext.FreeSql.Ado.Query<StudentDto>(sql);
                var pageData = new PageResponse<StudentDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
                var summary = new StudentDtoSummary()
                {
                    PageData = pageData,
                    ClassCount = (int)classCount
                };
                return new ResponseContext<StudentDtoSummary> { Data = summary };
            });

        }

        /// <summary>
        /// 获取指定老师管理的学生信息
        /// </summary>
        /// <returns></returns>
        private ResponseContext<StudentDtoSummary> GetStudentsByTid(GetStudentRequest model)
        {
            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxClass, YssxProfession, YssxCollege, YssxSchool, YssxUser, YssxTeacherClass>((a, b, c, d, e, f, t) =>
                a.InnerJoin(aa => aa.ClassId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.ProfessionId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.CollegeId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.TenantId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.UserId == f.Id && f.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.ClassId == t.ClassId && t.IsDelete == CommonConstants.IsNotDelete)
               )

            .WhereIf(model.ClassId > 0, (a, b, c, d, e, f, t) => a.ClassId == model.ClassId)
            .WhereIf(model.EntranceYear > 0, (a, b, c, d, e, f, t) => b.EntranceDateTime.Year == model.EntranceYear)
            .Where((a, b, c, d, e, f, t) => f.UserType == (int)UserTypeEnums.Student && a.IsDelete == CommonConstants.IsNotDelete);

            if (model.TeacherId > 0)
                select = select.Where((a, b, c, d, e, f, t) => t.TeacherId == model.TeacherId);
            if (!string.IsNullOrWhiteSpace(model.KeyWord))
                select = select.Where((a, b, c, d, e, f, t) => f.RealName.Contains(model.KeyWord) || f.MobilePhone.Contains(model.KeyWord) || a.StudentNo.Contains(model.KeyWord));

            var totalCount = select.Count();
            var classIdsArry = select.ToList((a, b, c, d, e, f, t) => a.ClassId);
            var classCount = classIdsArry.Distinct().Count();
            var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,b.Name ClassName,c.Name ProfessionName,d.Name CollegeName," +
                "e.Name SchoolName,f.RealName,f.MobilePhone,f.IdNumber,f.Sex,f.Email,b.EntranceDateTime");

            var items = DbContext.FreeSql.Ado.Query<StudentDto>(sql);
            var pageData = new PageResponse<StudentDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            var summary = new StudentDtoSummary() { PageData = pageData };
            summary.ClassCount = (int)classCount;
            return new ResponseContext<StudentDtoSummary> { Data = summary };
        }

        /// <summary>
        /// 删除学生
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteStudent(StudentDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，入参信息有误!" };
            var student = DbContext.FreeSql.Select<YssxStudent>().Where(m => m.Id == model.Id).First();
            if (student == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            student.IsDelete = CommonConstants.IsDelete;
            student.UpdateBy = opreationId;
            student.UpdateTime = DateTime.Now;

            var user = new YssxUser { Id = student.UserId, IsDelete = CommonConstants.IsDelete, UpdateBy = opreationId, UpdateTime = DateTime.Now };
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxStudent>().SetSource(student).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 导出学生
        /// </summary>
        /// <returns></returns>
        //public async Task<IActionResult> ExportStudent(long classId, long entranceYear, string keyWord, string mobilePhone)
        //{
        //    return await Task.Run(() =>
        //    {
        //        //Done 租户过滤
        //        var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxClass, YssxProfession, YssxCollege, YssxSchool, YssxUser>((a, b, c, d, e, f) =>
        //            a.InnerJoin(aa => aa.ClassId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
        //            .InnerJoin(aa => aa.ProfessionId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
        //            .InnerJoin(aa => aa.CollegeId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
        //            .InnerJoin(aa => aa.TenantId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
        //            .InnerJoin(aa => aa.UserId == f.Id && f.IsDelete == CommonConstants.IsNotDelete))
        //         .WhereIf(classId > 0, (a, b, c, d, e, f) => a.ClassId == classId)
        //         .WhereIf(entranceYear > 0, (a, b, c, d, e, f) => b.EntranceDateTime.Year == entranceYear)
        //         .Where((a, b, c, d, e, f) => f.UserType == UserTypeEnums.Student && a.IsDelete == CommonConstants.IsNotDelete);

        //        if (!string.IsNullOrWhiteSpace(mobilePhone))
        //            select = select.Where((a, b, c, d, e, f) => f.MobilePhone.Contains(mobilePhone) || a.StudentNo.Contains(mobilePhone));
        //        if (!string.IsNullOrWhiteSpace(keyWord))
        //            select = select.Where((a, b, c, d, e, f) => f.RealName.Contains(keyWord) || f.MobilePhone.Contains(keyWord) || a.StudentNo.Contains(keyWord));

        //        var selectData = select.ToList((a, b, c, d, e, f) => new ExportStudentDto
        //        {
        //            RealName = f.RealName,
        //            Email = f.Email,
        //            Sex = f.Sex.GetDescription(),
        //            UserName = f.UserName,
        //            MobilePhone = f.MobilePhone,
        //            IdNumber = f.IdNumber,
        //            StudentNo = a.StudentNo,
        //            ProfessionName = c.Name,
        //            CollegeName = d.Name,
        //            ClassName = b.Name
        //        });

        //        byte[] ms = NPOIHelper<ExportStudentDto>.OutputExcel(selectData, new string[] { "111" });
        //        var provider = new FileExtensionContentTypeProvider();
        //        if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
        //        {
        //            throw new Exception("找不到可导出的文件类型");
        //        }
        //        return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
        //    });
        //}

        /// <summary>
        /// 导入学生
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<ImportStudentResponse>>> ImportStudent(IFormFile form, long Id)
        {
            long opreationId = Id;//操作人ID
            DateTime dtNow = DateTime.Now;

            #region 取文导入件数据
            var file = form;//?.FirstOrDefault();
            if (file == null)
            {
                return new ResponseContext<List<ImportStudentResponse>> { Code = CommonConstants.ErrorCode, Msg = "上传失败" };
            }
            string ss = file.FileName.Split('.')[1];
            var rStudentData = new List<ImportStudentUser>();
            using (var ms = file.OpenReadStream())
            {
                rStudentData = NPOIHelper<ImportStudentUser>.ImportExcelData(ms, ss);
            }
            //using (var ms = new MemoryStream(file.File))
            //{
            //    rStudentData = NPOIHelper<ImportStudent>.ImportExcelData(ms);
            //}
            #endregion

            if (rStudentData == null || !rStudentData.Any())
            {
                return new ResponseContext<List<ImportStudentResponse>> { Code = CommonConstants.NotFund, Msg = "Excel中未存在任何有效数据" };
            }
            var rResData = new List<ImportStudentResponse>();
            var sb = new StringBuilder();
            var loop = 0;
            var studentList = new List<YssxStudent>();
            var userList = new List<YssxUser>();
            //var activationList = new List<YssxActivationInformation>();
            //var schoolList = new List<YssxSchool>();
            //TODO加锁
            var errorLoop = 0;
            foreach (var item in rStudentData)
            {
                loop++;
                sb.Clear();
                YssxSchool school = new YssxSchool();
                YssxActivationCode code = new YssxActivationCode();
                long collegelId = 0;
                long professionId = 0;
                long classId = 0;
                #region 校验

                if (string.IsNullOrWhiteSpace(item.Class))
                    sb.Append("班级不能为空.");
                if (string.IsNullOrWhiteSpace(item.Code))
                    sb.Append("激活码不能为空.");
                if (string.IsNullOrWhiteSpace(item.EngineeringNumber))
                    sb.Append("工学号不能为空.");
                if (string.IsNullOrWhiteSpace(item.FacultyDepartment))
                    sb.Append("院系不能为空.");

                if (string.IsNullOrWhiteSpace(item.Name))
                    sb.Append("姓名不能为空.");
                if (string.IsNullOrWhiteSpace(item.Major))
                    sb.Append("专业不能为空.");
                if (string.IsNullOrWhiteSpace(item.Role))
                    sb.Append("角色不能为空.");
                if (string.IsNullOrWhiteSpace(item.School))
                    sb.Append("学校不能为空.");
                //if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == item.MobilePhone && m.IsDelete == CommonConstants.IsNotDelete))
                //    sb.Append($"【{item.SchoolName}】手机号码重复.");

                if (!string.IsNullOrEmpty(item.School))
                {
                    school = DbContext.FreeSql.Select<YssxSchool>().Where(a => a.Name == item.School && a.IsDelete == CommonConstants.IsNotDelete).First();
                    if (school == null)
                    {
                        sb.Append($"不存在此学校【{item.School}】");
                    }
                }

                if (!string.IsNullOrEmpty(item.Code))
                {
                    code = DbContext.FreeSql.Select<YssxActivationCode>().Where(a => a.Code == item.Code && a.IsDelete == CommonConstants.IsNotDelete && a.EndTime > DateTime.Now && a.ActivationDeadline > DateTime.Now && a.State == 1).First();
                    if (code == null)
                    {
                        sb.Append($"验证码无效【{item.Code}】");
                    }
                }
                if (!string.IsNullOrEmpty(item.FacultyDepartment))
                {
                    collegelId = DbContext.FreeSql.Select<YssxCollege>().Where(a => a.TenantId == school.Id && a.Name == item.FacultyDepartment && a.IsDelete == CommonConstants.IsNotDelete).First(a => a.Id);

                    if (collegelId == 0)
                    {
                        sb.Append($"不存在此院系【{item.FacultyDepartment}】");
                    }
                }
                if (!string.IsNullOrEmpty(item.Major))
                {
                    professionId = DbContext.FreeSql.Select<YssxProfession>().Where(a => a.TenantId == school.Id && a.CollegeId == collegelId && a.Name == item.Major && a.IsDelete == CommonConstants.IsNotDelete).First(a => a.Id);
                    if (professionId == 0)
                    {
                        sb.Append($"不存在此专业【{item.Major}】");
                    }
                }
                if (!string.IsNullOrEmpty(item.Class))
                {
                    classId = DbContext.FreeSql.Select<YssxClass>().Where(a => a.TenantId == school.Id && a.CollegeId == collegelId && a.ProfessionId == professionId && a.Name == item.Class && a.IsDelete == CommonConstants.IsNotDelete).First(a => a.Id);
                    if (classId == 0)
                    {
                        sb.Append($"不存在此班级【{item.Class}】");
                    }
                }
                if (!string.IsNullOrEmpty(item.EngineeringNumber))
                {
                    if (DbContext.FreeSql.Select<YssxActivationInformation>().Where(a => a.TenantId == school.Id && a.WorkNumber == item.EngineeringNumber).Any())
                    {
                        sb.Append($"该学校【{item.School}】已存在该【{item.EngineeringNumber}】工学号");
                    }
                }
                #endregion
                #region 添加有效数据
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    rResData.Add(new ImportStudentResponse()
                    {
                        RowIndex = loop,
                        FacultyDepartment = item.FacultyDepartment,
                        Name = item.Name,
                        //IdNumber = item.IdNumber,
                        ErrorMsg = sb.ToString().TrimEnd(',')
                    });
                    errorLoop++;
                }
                else
                {
                    var pwd = CommonConstants.NewUserDefaultPwd.Md5();
                    var user = new YssxUser()
                    {
                        Id = IdWorker.NextId(),
                        TenantId = school.Id,
                        NikeName = item.Name,
                        RealName = item.Name,
                        UserName = item.Name,
                        Status = (int)Status.Enable,
                        Password = pwd,
                        IdNumber = "",
                        MobilePhone = "",
                        Email = "",
                        ClientId = Guid.NewGuid(),
                        CreateBy = opreationId,
                        CreateTime = dtNow,
                        Sex = "男",
                        RegistrationType = 2,
                        //ActivationCodeId=code.Id,
                        //ActivationTime=DateTime.Now,
                        //ClassId=classId,
                        //DepartmentsId= collegelId,
                        //ProfessionalId= professionId,
                        //WorkNumber=item.EngineeringNumber
                    };

                    var entity = new YssxStudent()
                    {
                        Id = IdWorker.NextId(),
                        TenantId = school.Id,
                        Name = item.Name,
                        UserId = user.Id,
                        CollegeId = collegelId,
                        ProfessionId = professionId,
                        ClassId = classId,
                        StudentNo = item.EngineeringNumber,
                        CreateBy = opreationId,
                        CreateTime = dtNow
                    };

                    if (item.Role == "学生")
                        user.UserType = 1;
                    if (item.Role == "教师")
                        user.UserType = 2;
                    if (item.Role == "教务")
                        user.UserType = 3;

                    studentList.Add(entity);
                    userList.Add(user);
                }
                #endregion

            }
            //入库
            return await Task.Run(() =>
            {
                if (studentList.Any())
                {
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        DbContext.FreeSql.Insert<YssxStudent>().AppendData(studentList).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxUser>().AppendData(userList).ExecuteAffrows();
                        uow.Commit();
                    }
                }
                var sbMsg = sb.ToString();
                var rResMsg = (string.IsNullOrEmpty(sbMsg) ? string.Empty : $"错误：{sbMsg}") + $"成功导入{studentList.Count()}条,失败{rStudentData.Count() - studentList.Count()}条,共{rStudentData.Count()}条数据.";
                return new ResponseContext<List<ImportStudentResponse>> { Code = CommonConstants.SuccessCode, Data = rResData, Msg = rResMsg };
            });
        }

        public async Task<ResponseContext<List<StudentClassListDto>>> GetStudentsByClassIds(List<long> ids)
        {
            if (ids == null || ids.Count == 0) return new ResponseContext<List<StudentClassListDto>> { };
            ResponseContext<List<StudentClassListDto>> response = new ResponseContext<List<StudentClassListDto>>();
            List<StudentClassListDto> names = await DbContext.FreeSql.GetRepository<YssxStudent>().Select.From<YssxClass, YssxProfession, YssxCollege>((a, b, c, d) => a.InnerJoin(x => x.ClassId == b.Id).InnerJoin(x => x.ProfessionId == c.Id).InnerJoin(x => x.CollegeId == d.Id)).Where((a, b, c, d) => ids.Contains(a.ClassId)).ToListAsync((a, b, c, d) => new StudentClassListDto
            {
                UserId = a.UserId,
                ClassName = b.Name,
                CollegeName = d.Name,
                ProfessionName = c.Name,
                Name = a.Name,
                StudentNo=a.StudentNo,
            });
            response.Data = names;
            return response;
        }
    }
}
