using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;

namespace Yssx.S.Service
{
    /// <summary>
    /// 用户收货地址服务
    /// </summary>
    public class UserAddressService : IUserAddressService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(UserAddressDto model, long currentUserId)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUserId);
            }
            else //修改
            {
                return await Update(model, currentUserId);
            }
        }

        /// <summary>
        /// 用户收货地址 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(UserAddressDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            //var hasName = await repo
            //             .Where(e => e.Name == model.Name)
            //             .AnyAsync();

            //if (hasName)
            //{
            //    result.Msg = $"用户收货地址名称重复。";
            //    return result;
            //}

            #endregion

            var obj = model.MapTo<YssxUserAddress>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.CreateBy = currentUserId;

            #region 保存到数据库中

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo_f = uow.GetRepository<YssxUserAddress>();
                    if (obj.IsDefault) //当前地址设置为默认地址，把其他地址设置为非默认
                    {
                        await repo_f.UpdateDiy.Set(c => new YssxUserAddress
                        {
                            IsDefault = false,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUserId,
                        }).Where(e => e.UserId == currentUserId && e.IsDefault == true).ExecuteAffrowsAsync();
                    }
                    await repo_f.InsertAsync(obj);//添加到数据库

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 用户收货地址 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(UserAddressDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"用户收货地址不存在，Id:{model.Id}", false);

            //var hasName = await repo
            //             .Where(e => e.Name == model.Name && e.Id != model.Id)
            //             .AnyAsync();

            //if (hasName)
            //    return new ResponseContext<bool>(CommonConstants.BadRequest, $"用户收货地址名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<YssxUserAddress>();
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;

            #region 保存到数据库中

            //await repo.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元    
            {
                try
                {
                    var repo_f = uow.GetRepository<YssxUserAddress>();

                    if (updateObj.IsDefault)//当前地址设置为默认地址，把其他地址设置为非默认
                    {
                        await repo_f.UpdateDiy.Set(c => new YssxUserAddress
                        {
                            IsDefault = false,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUserId,
                        }).Where(e => e.UserId == currentUserId && e.IsDefault == true && e.Id != updateObj.Id).ExecuteAffrowsAsync();
                    }
                    await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserAddressDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<UserAddressDto>();

            return new ResponseContext<UserAddressDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new YssxUserAddress
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new YssxUserAddress
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<UserAddressListResponseDto>> List(long userId)
        {
            var entitys = await DbContext.FreeSql.GetRepository<YssxUserAddress>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.UserId == userId)
                   .OrderByDescending(e => e.IsDefault)
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<UserAddressListResponseDto>();

            return new ListResponse<UserAddressListResponseDto>(entitys);
        }

        /// <summary>
        /// 设置默认收货地址
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetDefault(long id, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "");
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo_t = uow.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    await repo_t.UpdateDiy.Set(c => new YssxUserAddress
                    {
                        IsDefault = false,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUserId,
                    }).Where(e => e.UserId == currentUserId && e.IsDefault == true).ExecuteAffrowsAsync();

                    await repo_t.UpdateDiy.Set(c => new YssxUserAddress
                    {
                        IsDefault = true,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUserId,
                    }).Where(e => e.Id == id).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return result;
        }

        /// <summary>
        /// 获取用户默认收货地址
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<UserDefaultAddressDto>> GetUserDefaultAddress(long userId)
        {
            //获取详情
            var entity = await DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.UserId == userId)
                      .OrderByDescending(e => e.IsDefault)
                      .FirstAsync<UserDefaultAddressDto>();

            return new ResponseContext<UserDefaultAddressDto>(entity);


        }
    }
}