﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 公共题目管理
    /// </summary>
    public class TopicPublicService : ITopicPublicService
    {
        #region 添加题目
        /// <summary>
        /// 添加简单类型题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> SaveSimpleQuestion(SimpleQuestionPubRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                if (!model.Options.Any())
                {
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "请添加选项" };
                }
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                }

                #endregion
                var topic = model.MapTo<YssxTopicPublic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = currentUserId;
                topic.CreateTime = dtNow;
                if (model.Id == 0) { topic.Id = IdWorker.NextId(); }

                #region 处理选项排序
                var loop = 0;
                //选项信息
                var options = new List<YssxPublicAnswerOption>();
                foreach (var item in model.Options)
                {
                    loop++;
                    options.Add(new YssxPublicAnswerOption()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        TopicId = topic.Id,
                        AnswerOption = item.Text.Trim(),
                        Sort = loop,
                        CreateBy = currentUserId,
                        CreateTime = dtNow,

                        AnswerKeysContent = "",
                        AnswerFileUrl = ""
                    });
                }
                //知识点
                List<YssxKnowledgePoint> rAddKnowledgePointData = new List<YssxKnowledgePoint>();
                topic.KnowledgePointIds = "";
                if (model.KnowledgePointDetail!=null&& model.KnowledgePointDetail.Count > 0)
                {
                    var maxSort = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First(x => x.Sort);
                    foreach (var itemKp in model.KnowledgePointDetail)
                    {
                        if (itemKp.Id == 0)
                        {
                            var entityKp = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Name == itemKp.Name && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entityKp != null)
                            {
                                itemKp.Id = entityKp.Id;
                            }
                            else
                            {
                                itemKp.Id = IdWorker.NextId();
                                rAddKnowledgePointData.Add(new YssxKnowledgePoint()
                                {
                                    Id = itemKp.Id,
                                    GroupId = itemKp.Id,
                                    Name = itemKp.Name,
                                    ParentId = 0,
                                    Sort = maxSort + rAddKnowledgePointData.Count + 1,
                                    CreateBy = currentUserId
                                });
                            }
                        }
                        topic.KnowledgePointIds = string.IsNullOrEmpty(topic.KnowledgePointIds) ? itemKp.Id.ToString() : topic.KnowledgePointIds + "," + itemKp.Id.ToString();
                    }
                }
                #endregion

                if (model.Id == 0)
                {
                    #region 添加题目，选项，题目类别关联信息
                    options.ForEach(a => a.Id = IdWorker.NextId());

                    //添加到数据库
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxPublicAnswerOption>().AppendData(options).ExecuteAffrows();
                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                        uow.Commit();
                    }
                    #endregion
                }
                else
                {
                    topic.UpdateTime = dtNow;
                    topic.UpdateBy = currentUserId;

                    #region 新增选项--添加ID
                    var addOption = options.Where(a => a.Id == 0).ToList();
                    var updateOption = options.Where(a => a.Id > 0).ToList();
                    addOption.ForEach(a =>
                    {
                        a.Id = IdWorker.NextId();
                    });

                    //取删除的选项
                    var existOptionId = options.Select(a => a.Id).ToArray();
                    var delOption = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(m => m.TopicId == topic.Id && m.IsDelete == CommonConstants.IsNotDelete && !existOptionId.Contains(m.Id)).ToList();
                    if (delOption.Count > 0)
                    {
                        delOption.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = currentUserId;
                            a.UpdateTime = dtNow;
                        });
                    }
                    #endregion

                    #region 更新题目,选项(更新，添加)。题目类别关联信息不允许修改
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //更新题目
                        DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrows();

                        #region 选项(更新，添加)
                        //添加
                        if (addOption.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxPublicAnswerOption>().AppendData(addOption).ExecuteAffrows();
                        }
                        //删除的选项
                        if (delOption.Count > 0)
                        {
                            DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(delOption).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        }
                        //更新选项
                        if (updateOption.Count > 0)
                        {
                            foreach (var item in updateOption)
                            {
                                DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.AnswerOption,
                                    a.Name,
                                    a.AnswerFileUrl,
                                    a.Sort,
                                    a.UpdateBy,
                                    a.UpdateTime
                                }).ExecuteAffrows();
                            }
                        }
                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                        #endregion

                        uow.Commit();
                    }
                    #endregion

                }
                return new ResponseContext<long> { Code = CommonConstants.SuccessCode, Data = topic.Id };
            });
        }

        /// <summary>
        ///  添加复杂类型题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> SaveComplexQuestion(ComplexQuestionPubRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                var oldTopic = new YssxTopicPublic();
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                    oldTopic = rYssxTopic.First();
                }
                if (model.PartialScore > 0 && model.PartialScore >= model.Score)
                {
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，非完整性得分不能大于等于完整性得分" };
                }
                #endregion

                var topic = model.MapTo<YssxTopicPublic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = currentUserId;
                topic.CreateTime = dtNow;
                if (model.Id == 0)
                {
                    topic.Id = IdWorker.NextId();
                }

                #region 文件排序
                //添加附件--用于排序需要
                var files = new List<YssxTopicPublicFile>();
                var loop = 0;
                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        loop++;
                        //TODO上传附件获取URL地址
                        files.Add(new YssxTopicPublicFile()
                        {
                            Id = IdWorker.NextId(),
                            Name = item.Name,
                            Url = item.Url,
                            Sort = loop
                        });
                    }
                }
                //知识点
                List<YssxKnowledgePoint> rAddKnowledgePointData = new List<YssxKnowledgePoint>();
                topic.KnowledgePointIds = "";
                if (model.KnowledgePointDetail!=null&& model.KnowledgePointDetail.Count > 0)
                {
                    var maxSort = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First(x => x.Sort);
                    foreach (var itemKp in model.KnowledgePointDetail)
                    {
                        if (itemKp.Id == 0)
                        {
                            var entityKp = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Name == itemKp.Name && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entityKp != null)
                            {
                                itemKp.Id = entityKp.Id;
                            }
                            else
                            {
                                itemKp.Id = IdWorker.NextId();
                                rAddKnowledgePointData.Add(new YssxKnowledgePoint()
                                {
                                    Id = itemKp.Id,
                                    GroupId = itemKp.Id,
                                    Name = itemKp.Name,
                                    ParentId = 0,
                                    Sort = maxSort + rAddKnowledgePointData.Count + 1,
                                    CreateBy = currentUserId
                                });
                            }
                        }
                        topic.KnowledgePointIds = string.IsNullOrEmpty(topic.KnowledgePointIds) ? itemKp.Id.ToString() : topic.KnowledgePointIds + "," + itemKp.Id.ToString();
                    }
                }
                #endregion

                if (model.Id == 0)
                {
                    //添加到数据库
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicPublicFile>().AppendData(files).ExecuteAffrows();
                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                        uow.Commit();
                    }
                }
                else
                {
                    topic.UpdateTime = DateTime.Now;
                    topic.UpdateBy = currentUserId;

                    #region 取删除的文件
                    var delTopicFile = new List<YssxTopicPublicFile>();
                    if (!String.IsNullOrEmpty(oldTopic.TopicFileIds))
                    {
                        var rOldfileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                        var rNewFileIds = files.Select(m => m.Id);
                        var rDelFileIds = rOldfileIds.Where(m => !rNewFileIds.Contains(m));
                        if (rDelFileIds.Count() > 0)
                        {
                            delTopicFile = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rDelFileIds.Contains(m.Id)).ToList();
                            if (delTopicFile.Count > 0)
                            {
                                delTopicFile.ForEach(a =>
                                {
                                    a.IsDelete = CommonConstants.IsDelete;
                                    a.UpdateBy = currentUserId;
                                    a.UpdateTime = dtNow;
                                });
                            }
                        }
                    }
                    #endregion

                    #region 更新题目，更新文件顺序字段，添加文件顺序
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        if (delTopicFile.Count > 0)
                        {
                            DbContext.FreeSql.Update<YssxTopicPublicFile>().SetSource(delTopicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        }
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicPublicFile>().AppendData(files).ExecuteAffrows();

                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //更新题目
                        DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrows();
                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                        uow.Commit();
                    }
                    #endregion

                }

                return new ResponseContext<long> { Code = CommonConstants.SuccessCode, Data = topic.Id };
            });
        }

        /// <summary>
        /// 添加多题型题目（综合题）--------------------------暂时只给【专业组】用2020.01.04-----------------------------------
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> SaveMainSubQuestion(MainSubQuestionPubRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                var oldTopic = new YssxTopicPublic();
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                    oldTopic = rYssxTopic.First();
                }
                if (model.SubQuestion.Count == 0)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目为0" };
                //验证题目类型是否有效
                var rIsInvalidQuestionType = model.SubQuestion
                    .Where(m => m.QuestionType == QuestionType.SingleChoice
                            || m.QuestionType == QuestionType.MultiChoice
                            || m.QuestionType == QuestionType.Judge
                            || m.QuestionType == QuestionType.FillGrid
                            || m.QuestionType == QuestionType.AccountEntry
                            || m.QuestionType == QuestionType.FillBlank
                            || m.QuestionType == QuestionType.FillGraphGrid
                            || m.QuestionType == QuestionType.UndefinedTermChoice).Count();
                if (rIsInvalidQuestionType != model.SubQuestion.Count)
                {
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目类型有误" };
                }
                #endregion

                #region 
                //支持选择题-选项
                var options = new List<YssxPublicAnswerOption>();
                var topic = model.MapTo<YssxTopicPublic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = currentUserId;
                topic.CreateTime = dtNow;
                if (model.Id == 0) { topic.Id = IdWorker.NextId(); }
                //知识点
                List<YssxKnowledgePoint> rAddKnowledgePointData = new List<YssxKnowledgePoint>();
                topic.KnowledgePointIds = "";
                if (model.KnowledgePointDetail!=null&& model.KnowledgePointDetail.Count > 0)
                {
                    var maxSort = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First(x => x.Sort);
                    foreach (var itemKp in model.KnowledgePointDetail)
                    {
                        if (itemKp.Id == 0)
                        {
                            var entityKp = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Name == itemKp.Name && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entityKp != null)
                            {
                                itemKp.Id = entityKp.Id;
                            }
                            else
                            {
                                itemKp.Id = IdWorker.NextId();
                                rAddKnowledgePointData.Add(new YssxKnowledgePoint()
                                {
                                    Id = itemKp.Id,
                                    GroupId = itemKp.Id,
                                    Name = itemKp.Name,
                                    ParentId = 0,
                                    Sort = maxSort + rAddKnowledgePointData.Count + 1,
                                    CreateBy = currentUserId
                                });
                            }
                        }
                        topic.KnowledgePointIds = string.IsNullOrEmpty(topic.KnowledgePointIds) ? itemKp.Id.ToString() : topic.KnowledgePointIds + "," + itemKp.Id.ToString();
                    }
                }
                #endregion

                #region 文件排序
                //添加附件--用于排序需要
                var files = new List<YssxTopicPublicFile>();
                var loop = 0;
                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        loop++;
                        files.Add(new YssxTopicPublicFile()
                        {
                            Id = IdWorker.NextId(),
                            Name = item.Name,
                            Url = item.Url,
                            Sort = loop
                        });
                    }
                }

                #endregion

                if (model.Id == 0)
                {
                    //子题目 排序 添加父节点ID
                    #region 处理数据
                    model.SubQuestion.ForEach(a =>
                    {
                        a.Id = IdWorker.NextId();
                    });
                    var subQuestion = model.SubQuestion.Select(a => a.MapTo<YssxTopicPublic>()).ToList();
                    subQuestion.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                    });
                    //分录题
                    #region 
                    var rAccountEntryData = model.SubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopicPublic>()).ToList();
                    var rYssxCertificateData = new List<YssxPubCertificateDataRecord>();
                    if (rAccountEntryData.Count > 0)
                    {
                        rAccountEntryData.ForEach(a =>
                        {
                            a.TopicId = a.Id;
                            a.Id = IdWorker.NextId();
                        });
                        var rListSubQuestion = model.SubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).ToList();
                        if (rListSubQuestion.Count > 0)
                        {
                            foreach (var itemSub in rListSubQuestion)
                            {
                                var rCertificateTopic = rAccountEntryData.Where(m => m.TopicId == itemSub.Id).First();
                                List<YssxPubCertificateDataRecord> rListRecord = itemSub.DataRecords.MapTo<List<YssxPubCertificateDataRecord>>();
                                foreach (var itemRecord in rListRecord)
                                {
                                    itemRecord.Id = IdWorker.NextId();
                                    itemRecord.CertificateNo = itemSub.CertificateNo;
                                    itemRecord.CertificateDate = itemSub.CertificateDate;
                                    itemRecord.CertificateTopicId = rCertificateTopic.Id;
                                    itemRecord.CreateTime = dtNow;
                                    rYssxCertificateData.Add(itemRecord);
                                }
                            }
                        }
                    }
                    #endregion
                    foreach (var item in model.SubQuestion)
                    {
                        if (item.Options != null && item.Options.Any())
                        {
                            item.Options.ForEach(a =>
                            {
                                var option = new YssxPublicAnswerOption()
                                {
                                    TopicId = item.Id,
                                    Name = a.Name,
                                    AnswerOption = a.Text,
                                    AnswerFileUrl = a.AttatchImgUrl,
                                    AnswerKeysContent = item.AnswerValue,
                                };
                                options.Add(option);
                            });
                        }

                    }
                    options.ForEach(a => { a.Id = IdWorker.NextId(); });

                    DbContext.FreeSql.Transaction(() =>
                    {
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicPublicFile>().AppendData(files).ExecuteAffrows();
                        //添加子题目
                        DbContext.FreeSql.Insert<YssxTopicPublic>().AppendData(subQuestion).ExecuteAffrows();
                        //添加子题目-选项
                        if (options.Any())
                        {
                            DbContext.FreeSql.Insert<YssxPublicAnswerOption>().AppendData(options).ExecuteAffrows();
                        }
                        //添加分录题内容
                        if (rAccountEntryData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxCertificateTopicPublic>().AppendData(rAccountEntryData).ExecuteAffrows();
                            DbContext.FreeSql.Insert<YssxPubCertificateDataRecord>().AppendData(rYssxCertificateData).ExecuteAffrows();
                        }
                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                    });
                    #endregion
                }
                else
                {
                    //子题目 排序 添加父节点ID
                    #region 处理数据
                    var newSubQuestion = model.SubQuestion;
                    topic.UpdateTime = dtNow;
                    topic.UpdateBy = currentUserId;
                    //子题目
                    var addSubQuestion = newSubQuestion.Where(a => a.Id == 0).ToList();
                    var updateSubQuestion = newSubQuestion.Where(a => a.Id > 0);
                    //先取出添加或修改的题目数据，再设置添加题目的ID
                    newSubQuestion.ForEach(a =>
                    {
                        if (a.Id == 0) { a.Id = IdWorker.NextId(); }
                    });

                    var addSubTopic = addSubQuestion.Select(a => a.MapTo<YssxTopicPublic>()).ToList();
                    var updateSubTopic = updateSubQuestion.Select(a => a.MapTo<YssxTopicPublic>()).ToList();

                    addSubTopic.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                    });
                    updateSubTopic.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                    });

                    #region 分录题
                    List<long> rCertificateTopicIdData = new List<long>();
                    var rAddAccountEntryData = addSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopicPublic>()).ToList();
                    var rUpdAccountEntryData = updateSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopicPublic>()).ToList();

                    var rYssxCertificateData = new List<YssxPubCertificateDataRecord>();
                    var rIsHaveAccountEntry = newSubQuestion.Any(m => m.QuestionType == QuestionType.AccountEntry);
                    if (rIsHaveAccountEntry)
                    {
                        //新增题目ID设置
                        rAddAccountEntryData.ForEach(a =>
                        {
                            a.TopicId = a.Id;
                            a.Id = IdWorker.NextId();
                        });
                        //修改题目ID设置
                        if (rUpdAccountEntryData.Count > 0)
                        {
                            var rUpdCertificateTopicId = rUpdAccountEntryData.Select(a => a.Id).ToList();
                            var rUpdYssxCertificateData = DbContext.FreeSql.Select<YssxCertificateTopicPublic>().Where(x => rUpdCertificateTopicId.Contains(x.TopicId)).ToList();
                            foreach (var itemCertificate in rUpdAccountEntryData)
                            {
                                var rUpdEntity = rUpdYssxCertificateData.Where(m => m.TopicId == itemCertificate.Id).FirstOrDefault();
                                if (rUpdEntity != null)
                                {
                                    itemCertificate.TopicId = rUpdEntity.TopicId;
                                    itemCertificate.Id = rUpdEntity.Id;
                                }
                            }
                        }
                        //录题凭证数据记录
                        var rListSubQuestion = newSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).ToList();
                        if (rListSubQuestion.Count > 0)
                        {
                            //取凭证题目ID
                            var rSubQuestionId = rListSubQuestion.Select(m => m.Id).ToList();
                            rCertificateTopicIdData = DbContext.FreeSql.Select<YssxCertificateTopicPublic>().Where(x => rSubQuestionId.Contains(x.TopicId)).ToList(m => m.Id);
                            //取凭证记录数据
                            foreach (var itemSub in rListSubQuestion)
                            {
                                var rAddCertificateTopic = rAddAccountEntryData.Where(m => m.TopicId == itemSub.Id).FirstOrDefault();
                                var rUpdCertificateTopic = rUpdAccountEntryData.Where(m => m.TopicId == itemSub.Id).FirstOrDefault();
                                List<YssxPubCertificateDataRecord> rListRecord = itemSub.DataRecords.MapTo<List<YssxPubCertificateDataRecord>>();
                                foreach (var itemRecord in rListRecord)
                                {
                                    itemRecord.Id = IdWorker.NextId();
                                    itemRecord.CertificateNo = itemSub.CertificateNo;
                                    itemRecord.CertificateDate = itemSub.CertificateDate;
                                    itemRecord.CertificateTopicId = rUpdCertificateTopic == null ? rAddCertificateTopic.Id : rUpdCertificateTopic.Id;
                                    itemRecord.CreateTime = dtNow;
                                    rYssxCertificateData.Add(itemRecord);
                                }
                            }
                        }
                    }
                    #endregion

                    #endregion

                    var existIds = updateSubQuestion.Select(a => a.Id);
                    //子题目-选择题-选项
                    foreach (var item in newSubQuestion)
                    {
                        if (item.Options != null && item.Options.Any())
                        {
                            item.Options.ForEach(a =>
                            {
                                var option = new YssxPublicAnswerOption()
                                {
                                    Id = a.Id,
                                    TopicId = item.Id,
                                    Name = a.Name,
                                    AnswerOption = a.Text,
                                    AnswerFileUrl = a.AttatchImgUrl,
                                    AnswerKeysContent = item.AnswerValue,
                                };
                                options.Add(option);
                            });
                        }

                    }
                    var addOption = options.Where(a => a.Id == 0).ToList();
                    var updateOption = options.Where(a => a.Id > 0);
                    addOption.ForEach(a => a.Id = IdWorker.NextId());

                    #region 取待删除数据
                    //取子题目
                    var rDelSubQuestion = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.ParentId == topic.Id && !existIds.Contains(m.Id)).ToList();
                    rDelSubQuestion.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = currentUserId;
                        a.UpdateTime = dtNow;
                    });
                    //取子题目-选项
                    var topicIds = options.Select(a => a.TopicId).Distinct().ToArray();
                    var optionIds = options.Select(a => a.Id).ToArray();
                    var rDelSubOption = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(a => topicIds.Contains(a.TopicId) && !optionIds.Contains(a.Id)).ToList();
                    rDelSubOption.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = currentUserId;
                        a.UpdateTime = dtNow;
                    });

                    ////取文件
                    //var delTopicFile = new List<YssxTopicPublicFile>();
                    //if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                    //{
                    //    var idArr = oldTopic.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    //    if (idArr.Any())
                    //    {
                    //        delTopicFile = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(m => idArr.Contains(m.Id)).ToList();
                    //    }
                    //}
                    #endregion

                    #region 更新题目，更新文件顺序字段，添加文件顺序
                    DbContext.FreeSql.Transaction(() =>
                    {
                        #region 附件 处理
                        if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                        {
                            var fileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                            DbContext.FreeSql.Delete<YssxTopicPublicFile>().Where(a => fileIds.Contains(a.Id)).ExecuteAffrows();
                        }
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicPublicFile>().AppendData(files).ExecuteAffrows();
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        #endregion

                        //更新主题目
                        DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.QuestionContentType }).ExecuteAffrows();

                        #region 子题目操作
                        //删除不存在的题目
                        if (rDelSubQuestion.Any())
                        {
                            DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(rDelSubQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        }
                        //添加新题目
                        if (addSubTopic.Any())
                        {
                            DbContext.FreeSql.Insert<YssxTopicPublic>().AppendData(addSubTopic).ExecuteAffrows();
                        }
                        //更新已存在题目
                        if (updateSubTopic.Any())
                        {
                            foreach (var item in updateSubTopic)
                            {
                                //更新题目内容
                                DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.Sort,
                                    a.Content,
                                    a.Hint,
                                    a.FullContent,
                                    a.TopicContent,
                                    a.AnswerValue,
                                    a.Score,
                                    a.PartialScore,
                                    a.CalculationType,
                                    a.IsCopy,
                                    a.IsCopyBigData,
                                    a.IsDisorder
                                }).ExecuteAffrows();
                            }
                        }
                        #endregion

                        #region 子题目-选择题-选项
                        if (options.Any())
                        {
                            #region 选项(更新，添加)
                            //删除选项
                            if (rDelSubOption.Any())
                            {
                                DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(rDelSubOption).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            }
                            //添加
                            if (addOption.Any())
                            {
                                DbContext.FreeSql.Insert<YssxPublicAnswerOption>().AppendData(addOption).ExecuteAffrows();
                            }
                            //更新选项
                            foreach (var item in updateOption)
                            {
                                DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.AnswerOption,
                                    a.Name,
                                    a.AnswerFileUrl,
                                    a.Sort,
                                    a.UpdateBy,
                                    a.UpdateTime
                                }).ExecuteAffrows();
                            }
                            #endregion
                        }
                        #endregion

                        //添加分录题内容
                        if (rIsHaveAccountEntry)
                        {
                            if (rCertificateTopicIdData.Count > 0)
                                DbContext.FreeSql.Delete<YssxPubCertificateDataRecord>().Where(x => rCertificateTopicIdData.Contains(x.CertificateTopicId)).ExecuteAffrows();
                            if (rAddAccountEntryData.Count > 0)
                                DbContext.FreeSql.Insert<YssxCertificateTopicPublic>().AppendData(rAddAccountEntryData).ExecuteAffrows();
                            if (rUpdAccountEntryData.Count > 0)
                            {
                                foreach (var itemUpd in rUpdAccountEntryData)
                                {
                                    DbContext.FreeSql.Update<YssxCertificateTopicPublic>().SetSource(itemUpd).UpdateColumns(a => new
                                    {
                                        a.IsDisableAM,
                                        a.IsDisableCashier,
                                        a.IsDisableAuditor,
                                        a.IsDisableCreator,
                                        a.AccountingManager,
                                        a.CalculationScoreType,
                                        a.InVal,
                                        a.OutVal,
                                        a.Auditor,
                                        a.Cashier,
                                        a.Creator
                                    }).ExecuteAffrows();
                                }
                            }
                            if (rYssxCertificateData.Count > 0)
                                DbContext.FreeSql.Insert<YssxPubCertificateDataRecord>().AppendData(rYssxCertificateData).ExecuteAffrows();
                        }

                        //添加新的知识点
                        if (rAddKnowledgePointData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxKnowledgePoint>().AppendData(rAddKnowledgePointData).ExecuteAffrows();
                        }
                    });
                    #endregion
                }
                return new ResponseContext<long> { Code = CommonConstants.SuccessCode, Data = topic.Id };
            });
        }

        #endregion

        #region 获取题目信息

        /// <summary>
        /// 获取指定题目
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicPubInfoDto>> GetQuestion(long id)
        {
            return await Task.Run(() =>
            {
                var question = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (question == null)
                {
                    return new ResponseContext<TopicPubInfoDto> { Code = CommonConstants.ErrorCode, Msg = "未找到相关题目" };
                }

                var questionDTO = question.MapTo<TopicPubInfoDto>();
                //取知识点名称
                questionDTO.KnowledgePointDetail = new List<TopicPubKnowledgePointDto>();
                if (!string.IsNullOrEmpty(question.KnowledgePointIds))
                {
                    var rKnowledgePointIds = question.KnowledgePointIds.SplitToArray<long>(',');
                    var rKpData = DbContext.FreeSql.Select<YssxKnowledgePoint>().Where(a => rKnowledgePointIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
                    if (rKnowledgePointIds.Count() > 0 && rKpData.Count > 0)
                    {
                        foreach (var item in rKnowledgePointIds)
                        {
                            if (rKpData.Any(m => m.Id == item))
                            {
                                questionDTO.KnowledgePointDetail.Add(new TopicPubKnowledgePointDto
                                {
                                    Id = item,
                                    Name = rKpData.First(m => m.Id == item).Name
                                });
                            }
                        }
                    }
                }

                if (questionDTO.QuestionType == QuestionType.SingleChoice || questionDTO.QuestionType == QuestionType.MultiChoice || questionDTO.QuestionType == QuestionType.Judge || questionDTO.QuestionType == QuestionType.NewMultiChoice)
                {
                    //简单题型
                    //添加选项信息
                    var options = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    questionDTO.Options = new List<ChoiceOptionPub>();
                    foreach (var item in options)
                    {
                        questionDTO.Options.Add(new ChoiceOptionPub()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Text = item.AnswerOption,
                            AttatchImgUrl = item.AnswerFileUrl
                        });
                    }
                }
                else
                {
                    //复杂题型
                    //添加附件信息
                    questionDTO.QuestionFile = new List<QuestionPubFile>();
                    if (!string.IsNullOrEmpty(question.TopicFileIds))
                    {
                        var fileIds = question.TopicFileIds.SplitToArray<long>(',');
                        var files = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(a => fileIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
                        foreach (var item in files)
                        {
                            questionDTO.QuestionFile.Add(new QuestionPubFile()
                            {
                                Url = item.Url,
                                Name = item.Name
                                //Sort = item.Sort
                            });
                        }
                    }

                    if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                    {
                        var subQuestions = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.ParentId == questionDTO.Id && m.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort)
                            .ToList(a => new SubQuestionPub(a.Id, a.QuestionType, a.QuestionContentType, "", a.Hint, a.Content, a.TopicContent, a.FullContent, a.AnswerValue, a.Sort, a.Score, a.PartialScore, a.CalculationType));
                        var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                            || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge
                            || a.QuestionType == QuestionType.UndefinedTermChoice).Select(a => a.Id).ToArray();
                        if (choiceQuestionIds.Any())
                        {
                            var optionAll = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(a => choiceQuestionIds.Contains(a.TopicId) && a.IsDelete == CommonConstants.IsNotDelete).ToList();
                            subQuestions.ForEach(a =>
                            {
                                var options = optionAll.Where(c => c.TopicId == a.Id).Select(d => new ChoiceOption()
                                {
                                    Id = d.Id,
                                    Name = d.Name,
                                    Text = d.AnswerOption,
                                    AttatchImgUrl = d.AnswerFileUrl
                                });
                                if (options.Any())
                                {
                                    a.Options = new List<ChoiceOption>();
                                    a.Options.AddRange(options);
                                }
                            });
                        }
                        //查询分录题相关数据
                        if (subQuestions.Any(m => m.QuestionType == QuestionType.AccountEntry))
                        {
                            subQuestions.ForEach(a =>
                            {
                                if (a.QuestionType == QuestionType.AccountEntry)
                                {
                                    var rCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopicPublic>().Where(m => m.TopicId == a.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                                    var rDataRecord = DbContext.FreeSql.Select<YssxPubCertificateDataRecord>().Where(m => m.CertificateTopicId == rCertificateTopic.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList(m => new PubCertificateDataRecord
                                    {
                                        SummaryInfo = m.SummaryInfo,
                                        SubjectId = m.SubjectId,
                                        BorrowAmount = m.BorrowAmount,
                                        CreditorAmount = m.CreditorAmount
                                    });
                                    a.CertificateNo = rCertificateTopic.CertificateNo;
                                    a.CertificateDate = rCertificateTopic.CertificateDate;
                                    a.DataRecords = rDataRecord;
                                    a.AccountingManager = rCertificateTopic.AccountingManager;
                                    a.Cashier = rCertificateTopic.Cashier;
                                    a.Auditor = rCertificateTopic.Auditor;
                                    a.Creator = rCertificateTopic.Creator;
                                    a.IsDisableAM = rCertificateTopic.IsDisableAM;
                                    a.IsDisableCashier = rCertificateTopic.IsDisableCashier;
                                    a.IsDisableAuditor = rCertificateTopic.IsDisableAuditor;
                                    a.IsDisableCreator = rCertificateTopic.IsDisableCreator;
                                    a.CalculationScoreType = rCertificateTopic.CalculationScoreType;
                                    a.OutVal = rCertificateTopic.OutVal;
                                    a.InVal = rCertificateTopic.InVal;

                                    // add 2019 12 27
                                    a.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(t => t.TopicId == a.Id && t.IsDelete == CommonConstants.IsNotDelete)
                                    .Select(t => new CertificateTopicView())
                                    .First($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{a.Id}", true, 10 * 60);
                                }
                            });
                        }

                        questionDTO.SubQuestion = new List<SubQuestionPub>();
                        questionDTO.SubQuestion.AddRange(subQuestions);
                    }
                    //add 2019 12 27
                    else if (question.QuestionType == QuestionType.AccountEntry)//分录题
                    {
                        questionDTO.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                            .Select(s => new CertificateTopicView())
                            .First($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{id}", true, 10 * 60);
                    }
                }
                return new ResponseContext<TopicPubInfoDto> { Code = CommonConstants.SuccessCode, Data = questionDTO };
            });
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TopicPubListView>> GetQuestionByPage(GetQuestionByPageRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.ParentId == 0 && m.IsSceneTraining == CommonConstants.IsNo).OrderBy(m => m.Sort);
                #region 搜索条件
                if (model.KnowledgePointId.HasValue && model.KnowledgePointId > 0)
                {
                    selectData = selectData.Where(m => m.KnowledgePointIds.Contains(model.KnowledgePointId.Value.ToString()));
                }
                if (!string.IsNullOrEmpty(model.Title))
                {
                    selectData = selectData.Where(m => m.Title.Contains(model.Title));
                }
                if (model.QuestionType.HasValue)
                {
                    selectData = selectData.Where(m => m.QuestionType == (QuestionType)model.QuestionType.Value);
                }
                if (model.Status.HasValue)
                {
                    selectData = selectData.Where(m => m.Status == (Status)model.Status.Value);
                }
                if (model.BeginDate.HasValue)
                {
                    selectData = selectData.Where(m => m.CreateTime >= model.BeginDate.Value);
                }
                if (model.EndDate.HasValue)
                {
                    selectData = selectData.Where(m => m.CreateTime <= model.EndDate.Value);
                }
                #endregion
                //查询题目数据
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).OrderByDescending(s => s.CreateTime).ToSql("Id, Title, KnowledgePointIds, IsGzip, Content, QuestionType, Education, Score, PartialScore, Status, CreateTime, UpdateTime, Sort");
                var topicData = DbContext.FreeSql.Ado.Query<TopicPubListView>(sql).ToList();
                //查询凭证单据张数
                var rTopicIdArr = topicData.Select(m => m.Id).ToList();
                var rCertificateData = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rTopicIdArr.Contains(m.Id)).ToList();
                foreach (var item in topicData)
                {
                    if (rCertificateData.Count > 0 && item.Id > 0)
                    {
                        item.Certificate = rCertificateData.Any(m => m.TopicId == item.Id) ? rCertificateData.First(m => m.TopicId == item.Id).InvoicesNum : 0;
                    }
                }
                return new PageResponse<TopicPubListView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = topicData };
            });
        }

        /// <summary>
        /// 查询题目列表 - 所有人创建的
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TopicPubListView>> GetAllQuestionByPage(GetPubQuestionByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                    .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.ParentId == 0 && m.IsSceneTraining == CommonConstants.IsNo)
                    .OrderBy(m => m.QuestionType == QuestionType.MainSubQuestion ? 0 :
                                (m.QuestionType == QuestionType.AccountEntry ? 1 :
                                (m.QuestionType == QuestionType.FillGrid ? 2 :
                                (m.QuestionType == QuestionType.FillGraphGrid ? 3 :
                                (m.QuestionType == QuestionType.FillBlank ? 4 :
                                (m.QuestionType == QuestionType.SingleChoice ? 5 :
                                (m.QuestionType == QuestionType.MultiChoice ? 6 :
                                (m.QuestionType == QuestionType.Judge ? 7 : 100))))))));
                //综合题7    分录题4  表格题 3  票据题6   填空题5   单选题0  多选题 1  判断题2
                #region 搜索条件
                if (!string.IsNullOrEmpty(model.Title))
                {
                    selectData = selectData.Where(m => m.Title.Contains(model.Title));
                }
                if (model.QuestionType.HasValue)
                {
                    selectData = selectData.Where(m => m.QuestionType == (QuestionType)model.QuestionType.Value);
                }
                if (model.Status.HasValue)
                {
                    selectData = selectData.Where(m => m.Status == (Status)model.Status.Value);
                }
                if (model.DataSources.HasValue && model.DataSources > 0)
                {
                    //获取专业组人员ID（云自营 = 专业组）
                    var selectUser = DbContext.FreeSql.Select<YssxUser>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.UserType == 6).ToList(m => m.Id);
                    if (model.DataSources == 1)
                        selectData = selectData.Where(m => selectUser.Contains(m.CreateBy));
                    else
                        selectData = selectData.Where(m => !selectUser.Contains(m.CreateBy));
                }
                #endregion
                //查询题目数据
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id, Title, KnowledgePointIds, IsGzip, Content, QuestionType, Education, Score, PartialScore, Status, CreateTime, UpdateTime, Sort");
                var topicData = DbContext.FreeSql.Ado.Query<TopicPubListView>(sql).ToList();

                return new PageResponse<TopicPubListView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = topicData };
            });
        }

        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteQuestion(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now;
                //校验

                #region 取数据【单独的分录题调单独的删除接口】
                //取题目
                var question = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (question == null) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到题目信息" }; }
                question.IsDelete = CommonConstants.IsDelete;
                question.UpdateBy = currentUserId;
                question.UpdateTime = dtNow;
                //取选项
                var option = new List<YssxPublicAnswerOption>();
                if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
                {
                    option = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (option.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到选项信息" }; }
                    option.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = currentUserId;
                        a.UpdateTime = dtNow;
                    });
                }
                //取子题目（题目列表、题目选项、题目附件）
                var subQuestion = new List<YssxTopicPublic>();
                var subOptionList = new List<YssxPublicAnswerOption>();
                var subTopicFileList = new List<YssxTopicPublicFile>();
                var subCertificateTopicList = new List<YssxCertificateTopicPublic>();
                var rCertificateTopic = new YssxCertificateTopicPublic();
                //分录题
                if (question.QuestionType == QuestionType.AccountEntry)
                {
                    //凭证题目
                    rCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopicPublic>().Where(m => m.TopicId == question.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (rCertificateTopic != null)
                    {
                        rCertificateTopic.IsDelete = CommonConstants.IsDelete;
                        rCertificateTopic.UpdateBy = currentUserId;
                        rCertificateTopic.UpdateTime = dtNow;
                    }
                    //凭证数据记录
                }
                //综合题
                else if (question.QuestionType == QuestionType.MainSubQuestion)
                {
                    subQuestion = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.ParentId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (subQuestion.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到子题目信息" }; }
                    foreach (var itemSub in subQuestion)
                    {
                        itemSub.IsDelete = CommonConstants.IsDelete;
                        itemSub.UpdateBy = currentUserId;
                        itemSub.UpdateTime = dtNow;

                        //取子题目 - 选项
                        if (itemSub.QuestionType == QuestionType.SingleChoice || itemSub.QuestionType == QuestionType.MultiChoice || itemSub.QuestionType == QuestionType.Judge)
                        {
                            //简单题
                            var subOption = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                            if (subOption.Count > 0) subOptionList.AddRange(subOption);
                        }
                        else if (itemSub.QuestionType == QuestionType.AccountEntry)
                        {
                            //分录题
                            //凭证题目
                            var subCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopicPublic>().Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (subCertificateTopic != null)
                            {
                                subCertificateTopic.IsDelete = CommonConstants.IsDelete;
                                subCertificateTopic.UpdateBy = currentUserId;
                                subCertificateTopic.UpdateTime = dtNow;
                                subCertificateTopicList.Add(subCertificateTopic);
                            }
                            //凭证数据记录
                        }
                        //取文件
                        if (!string.IsNullOrEmpty(itemSub.TopicFileIds))
                        {
                            var idArr = itemSub.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                            if (idArr.Any())
                            {
                                var subTopicFile = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                                if (subTopicFile.Count > 0) subTopicFileList.AddRange(subTopicFile);
                            }
                        }
                    }
                    //更新字段
                    if (subOptionList.Count > 0)
                    {
                        subOptionList.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = currentUserId;
                            a.UpdateTime = dtNow;
                        });
                    }
                    if (subTopicFileList.Count > 0)
                    {
                        subTopicFileList.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = currentUserId;
                            a.UpdateTime = dtNow;
                        });
                    }
                }
                //取文件
                var topicFile = new List<YssxTopicPublicFile>();
                if (!string.IsNullOrEmpty(question.TopicFileIds))
                {
                    var idArr = question.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (idArr.Any())
                    {
                        topicFile = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    }
                    if (topicFile.Count > 0)
                    {
                        topicFile.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = currentUserId;
                            a.UpdateTime = dtNow;
                        });
                    }
                }
                #endregion
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //删除题目
                    DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(question).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    //删除选项
                    if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
                    {
                        DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(option).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    //删除文件
                    if (topicFile.Count > 0)
                    {
                        DbContext.FreeSql.Update<YssxTopicPublicFile>().SetSource(topicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    //删除【分录题】
                    if (question.QuestionType == QuestionType.AccountEntry)
                    {
                        //删除分录题·凭证题目
                        if (rCertificateTopic != null)
                        {
                            DbContext.FreeSql.Update<YssxCertificateTopicPublic>().SetSource(rCertificateTopic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            //删除分录题·凭证数据记录
                            DbContext.FreeSql.Delete<YssxPubCertificateDataRecord>().Where(x => x.CertificateTopicId == rCertificateTopic.Id).ExecuteAffrows();
                        }
                    }
                    //删除【综合题】
                    else if (question.QuestionType == QuestionType.MainSubQuestion)
                    {
                        //删除题目
                        DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(subQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除简单题·选项
                        if (subOptionList.Count > 0)
                            DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(subOptionList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除分录题·凭证题目
                        if (subCertificateTopicList.Count > 0)
                            DbContext.FreeSql.Update<YssxCertificateTopicPublic>().SetSource(subCertificateTopicList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除分录题·凭证数据记录
                        var rCertificateTopicIdData = subCertificateTopicList.Select(m => m.Id).ToList();
                        if (rCertificateTopicIdData.Count > 0)
                            DbContext.FreeSql.Delete<YssxPubCertificateDataRecord>().Where(x => rCertificateTopicIdData.Contains(x.CertificateTopicId)).ExecuteAffrows();
                        //删除文件
                        if (subTopicFileList.Count > 0)
                            DbContext.FreeSql.Update<YssxTopicPublicFile>().SetSource(subTopicFileList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 删除题目附件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTopicFile(long id, long currentUserId)
        {
            var entity = DbContext.FreeSql.Select<YssxTopicPublicFile>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxTopicPublicFile>().SetSource(entity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 删除选项
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteOption(long topicId, long id, long currentUserId)
        {
            var entity = DbContext.FreeSql.Select<YssxPublicAnswerOption>().Where(m => m.TopicId == topicId && m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxPublicAnswerOption>().SetSource(entity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status, long currentUserId)
        {
            if (topicId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，入参信息有误" };
            var topic = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.Id == topicId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (topic == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息" };

            topic.Status = (Status)status;
            topic.UpdateBy = currentUserId;
            topic.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(topic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        #endregion

        #region 同步源课程下题目内容信息

        /// <summary>
        /// 同步源课程下题目内容信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ResetQuestionInfo(long id, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            #region 验证
            if (id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "同步失败，入参信息有误" };
            //主题目
            var topic = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (topic == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "同步失败，找不到题目信息" };
            var rTopicList = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.OriginalDataId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (rTopicList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "同步失败，找不到引用题目" };
            //子题目
            var subTopicList = new List<YssxTopicPublic>();
            if (topic.QuestionType == QuestionType.MainSubQuestion)
                subTopicList = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.ParentId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
            var rSubTopicList = new List<YssxTopicPublic>(); //子题目的应用题目列表
            var rSubTopicIds = subTopicList.Select(m => m.Id).ToList(); //取子题目ID
            if (subTopicList.Count > 0)
                rSubTopicList = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => rSubTopicIds.Contains(m.OriginalDataId) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
            #endregion

            //主题目
            foreach (var item in rTopicList)
            {
                item.Content = topic.Content;
                item.TopicContent = topic.TopicContent;
                item.FullContent = topic.FullContent;
                item.AnswerValue = topic.AnswerValue;
                item.CalculationType = topic.CalculationType;
                item.Score = topic.Score;
                item.PartialScore = topic.PartialScore;
                item.Sort = topic.Sort;
                item.UpdateBy = currentUserId;
                item.UpdateTime = dtNow;
            }
            //子题目
            //if (topic.QuestionType == QuestionType.MainSubQuestion)
            //{
            //    var rSubQuestionList = DbContext.FreeSql.Select<YssxTopicPublic>().Where(m => m.OriginalDataId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
            //    foreach (var itemSub in rSubQuestionList)
            //    {
            //    }
            //}

            //更新数据
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(topic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }


        #endregion

    }
}
