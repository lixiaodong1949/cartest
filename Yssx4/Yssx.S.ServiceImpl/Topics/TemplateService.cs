﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 模板管理
    /// </summary>
    public class TemplateService: ITemplateService
    {
        /// <summary>
        /// 获取模板类型树列表
        /// </summary>
        public async Task<PageResponse<TemplateTreeItemDto>> GetTemplateTree()
        {
            var types = DbContext.FreeSql.GetRepository<YssxTemplateType>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).ToList(m => new TemplateTreeItemDto
            {
                Id = m.Id,
                Name = m.Name,
                Pid = m.ParentId
            });
            return new PageResponse<TemplateTreeItemDto> { PageSize = types.Count, PageIndex = 1, RecordCount = types.Count, Data = types };
        }

        /// <summary>
        /// 获取指定模板类型
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        public async Task<ResponseContext<TemplateTreeItemDto>> GetTemplateTypeById(long id)
        {
            var rTemplateTypeData = DbContext.FreeSql.GetRepository<YssxTemplateType>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete);
            var res = rTemplateTypeData.Any() ? rTemplateTypeData.First(m => new TemplateTreeItemDto { Id = m.Id, Name = m.Name, Pid = m.ParentId }) : new TemplateTreeItemDto();
            return new ResponseContext<TemplateTreeItemDto> { Code = CommonConstants.SuccessCode, Data = res };
        }
        /// <summary>
        /// 修改模板类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ResponseContext<bool> UpdateTemplateType(TemplateTreeItemDto model, long currentUserId)
        {
            var rep = DbContext.FreeSql.GetRepository<YssxTemplateType>();
            var oldTemplateType = rep.Where(m => m.Id == model.Id).First();
            if (oldTemplateType == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            if (rep.Select.Any(m => (m.Name == model.Name) && m.Id != oldTemplateType.Id))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称重复" };


            var now = DateTime.Now;

            oldTemplateType.Name = model.Name;
            oldTemplateType.UpdateTime = DateTime.Now;
            oldTemplateType.UpdateBy = currentUserId;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateType>().SetSource(oldTemplateType).UpdateColumns(m => new { m.Name, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 添加/编辑模板类型信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateTemplateType(TemplateTreeItemDto model, long currentUserId)
        {
            if (model.Id != 0)
            {
                return UpdateTemplateType(model, currentUserId);
            }
            var templateType = new YssxTemplateType { Name = model.Name, ParentId = model.Pid };
            var now = DateTime.Now;
            templateType.Id = IdWorker.NextId();
            templateType.CreateBy = currentUserId;
            templateType.CreateTime = now;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Insert(templateType).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除模板类型
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTemplateType(long tId, long currentUserId)
        {
            var delType = new YssxTemplateType
            {
                Id = tId,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = DateTime.Now
            };
            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateType>().SetSource(delType).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }



        /// <summary>
        /// 获取模板列表
        /// </summary>
        public async Task<PageResponse<TemplateDto>> GetTemplates(GetTemplatesRequest model)
        {
            var select = DbContext.FreeSql.GetRepository<YssxTemplate>().Select.From<YssxTemplateType>((a, b) => a.InnerJoin(c => c.TemplateTypeId == b.Id)).
                Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.Id == model.TypeId);
            if (!string.IsNullOrEmpty(model.Name))
            {
                select = select.Where((a, b) => a.Name.Contains(model.Name));
            }
            var count = select.Count();
            var templateDtos = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new TemplateDto
            {
                Id = a.Id,
                Name = a.Name,
                TemplateTypeName = b.Name
            });
            return new PageResponse<TemplateDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = count, Data = templateDtos };
        }

        /// <summary>
        /// 获取指定模板
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        public async Task<ResponseContext<TemplateDto>> GetTemplateById(long id)
        {
            var res = DbContext.FreeSql.GetRepository<YssxTemplate>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First(m => new TemplateDto
            {
                Id = m.Id,
                Name = m.Name,
                FullHtml = m.FullHtml,
                ClearStyleHtml = m.ClearStyleHtml,
                AnswerJson = m.AnswerJson,
                TemplateTypeId = m.TemplateTypeId
            });
            return new ResponseContext<TemplateDto> { Code = CommonConstants.SuccessCode, Data = res };
        }
        /// <summary>
        /// 修改模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ResponseContext<bool> UpdateTemplate(TemplateDto model, long currentUserId)
        {
            var rep = DbContext.FreeSql.GetRepository<YssxTemplate>();
            var oldTemplate = rep.Where(m => m.Id == model.Id).First();
            if (oldTemplate == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            if (rep.Select.Any(m => (m.Name == model.Name) && m.Id != oldTemplate.Id))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称重复" };


            var now = DateTime.Now;

            oldTemplate.Name = model.Name;
            oldTemplate.FullHtml = model.FullHtml;
            oldTemplate.ClearStyleHtml = model.ClearStyleHtml;
            oldTemplate.AnswerJson = model.AnswerJson;
            oldTemplate.UpdateTime = DateTime.Now;
            oldTemplate.UpdateBy = currentUserId;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplate>().SetSource(oldTemplate).UpdateColumns(m => new { m.Name, m.FullHtml, m.ClearStyleHtml, m.AnswerJson, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 添加模板信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateTemplate(TemplateDto model, long currentUserId)
        {
            if (model.Id != 0)
            {
                return UpdateTemplate(model, currentUserId);
            }
            var template = new YssxTemplate
            {
                Name = model.Name,
                TemplateTypeId = model.TemplateTypeId,
                FullHtml = model.FullHtml,
                ClearStyleHtml = model.ClearStyleHtml,
                AnswerJson = model.AnswerJson
            };
            var now = DateTime.Now;
            template.Id = IdWorker.NextId();
            template.CreateBy = currentUserId;
            template.CreateTime = now;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Insert(template).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }


        /// <summary>
        /// 删除指定模板
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTemplate(long tId, long currentUserId)
        {
            var delType = new YssxTemplate { Id = tId, IsDelete = CommonConstants.IsDelete, UpdateBy = currentUserId, UpdateTime = DateTime.Now };
            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplate>().SetSource(delType).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }


        /// <summary>
        /// 获取模板树（下拉框）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<LayuiTreeDto>>> GetTemplateSelectTreeDatas()
        {
            var allTypes = DbContext.FreeSql.GetRepository<YssxTemplateType>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).ToList();
            var rootTypes = allTypes.Where(m => m.ParentId == 0).ToList();
            var tree = new List<LayuiTreeDto>();
            foreach (var rootType in rootTypes)
            {
                var dto = new LayuiTreeDto { id = rootType.Id, name = rootType.Name, children = new List<LayuiTreeDto>() };
                tree.Add(dto);
                LoadTemplateSelectTree(allTypes, dto);
            }
            return new ResponseContext<List<LayuiTreeDto>> { Code = CommonConstants.SuccessCode, Data = tree };
        }
        /// <summary>
        /// 递归获取子模板
        /// </summary>
        /// <param name="allTypes"></param>
        /// <param name="item"></param>
        private static void LoadTemplateSelectTree(List<YssxTemplateType> allTypes, LayuiTreeDto item)
        {
            var temTypes = allTypes.Where(m => m.ParentId == item.id).ToList();
            if (temTypes.Count == 0)
            {
                var templates = DbContext.FreeSql.GetRepository<YssxTemplate>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.TemplateTypeId == item.id).ToList();
                item.children = templates.Select(m => new LayuiTreeDto { id = m.Id, name = m.Name }).ToList();
            }
            else
            {
                foreach (var temType in temTypes)
                {
                    var dto = new LayuiTreeDto { id = temType.Id, name = temType.Name, children = new List<LayuiTreeDto>() };
                    item.children.Add(dto);
                    LoadTemplateSelectTree(allTypes, dto);
                }
            }
        }
    }
}
