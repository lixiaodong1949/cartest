﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Redis;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.IServices.Examination;
using Yssx.S.Poco;
using Yssx.Repository.Extensions;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.Topics;

namespace Yssx.S.ServiceImpl.Examination
{
    /// <summary>
    /// 考试分组相关服务 
    /// </summary> 
    public class ExamPaperGroupService : IExamPaperGroupService
    {
        /// <summary>
        /// 无参构造
        /// </summary>
        public ExamPaperGroupService()
        {

        }

        /// <summary>
        /// 获取考试分组列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<ExamPaperGroupListView>> GetExamPaperGroupListByPage(PageRequest<ExamPaperGroupQueryDto> model,long userId)
        {
            var result = new PageResponse<ExamPaperGroupListView>();

            if (model.Query.ExamId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"ExamId不能为0";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == model.Query.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{model.Query.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            var examGroupList = await DbContext.FreeSql.GetRepository<ExamPaperGroup>().Select.From<YssxUser, ExamPaperGroupStudent>((a, b, c) => a.LeftJoin(aa => aa.CreateBy == b.Id).LeftJoin(aa => aa.Id == c.GroupId && c.UserId == userId))
                .Where((a, b, c) => a.ExamId == model.Query.ExamId)
                .WhereIf(exam.ExamType == ExamType.EmulationTest, (a, b, c) => a.Status != GroupStatus.End)
                .WhereIf(model.Query.GroupNo > 0, (a, b, c) => a.GroupNo == model.Query.GroupNo)
                .Count(out var totalCount)
                .OrderBy((a, b, c) => a.GroupNo)
                .Page(model.PageIndex, model.PageSize)
                .ToListAsync((a, b, c) => new ExamPaperGroupListView()
                {
                    GroupId = a.Id,
                    GroupNo = a.GroupNo,
                    GradeId = a.GradeId,
                    Password = a.Password,
                    CreateBy = a.CreateBy,
                    UserName = b.RealName,
                    Status = a.Status
                });

            if (examGroupList.Any())
            {
                var groupIds = examGroupList.Select(s => s.GroupId).ToArray();

                var examPaperGroupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.From<YssxUser, YssxPosition>((a, b, c) => a.InnerJoin(aa => aa.UserId == b.Id).InnerJoin(aa => aa.PostionId == c.Id))
                    .Where((a, b, c) => groupIds.Contains(a.GroupId))
                    .ToList((a, b, c) => new ExamPaperGroupStudentView
                    {
                        GroupId = a.GroupId,
                        PostionId = a.PostionId,
                        UserId = a.UserId,
                        UserName = b.RealName,
                        PostionName = c.Name,
                        IsReady = a.IsReady,
                        StudentExamStatus = a.StudentExamStatus,
                        UsedSeconds = a.UsedSeconds,
                        LeftSeconds = a.LeftSeconds,
                    });

                examGroupList.ForEach(s =>
                {
                    s.ExamPaperGroupStudentList = examPaperGroupStudentList.Where(a => a.GroupId == s.GroupId).ToList();

                    if (!string.IsNullOrEmpty(s.Password))
                    {
                        if (!s.ExamPaperGroupStudentList.Any(a => a.UserId == userId))
                        {
                            s.Password = "******";
                        }
                    }
                });
            }

            result.Data = examGroupList;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = totalCount;
            return result;
        }

        /// <summary>
        /// 获取考试分组信息
        /// </summary>
        /// <param name="model">groupId</param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperGroupListView>> GetExamPaperGroupInfo(PageRequest<long> model,long userId)
        {
            var result = new ResponseContext<ExamPaperGroupListView>();

            if (model.Query == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"GroupId 不能为0";
                return result;
            }
            var groupId = model.Query;

            var examGroup = await DbContext.FreeSql.GetRepository<ExamPaperGroup>().Select.Where(s => s.Id == groupId)
                .FirstAsync(s => new ExamPaperGroupListView()
                {
                    GroupId = s.Id,
                    GroupNo = s.GroupNo,
                    GradeId = s.GradeId,
                    Password = s.Password,
                    Status = s.Status
                });

            if (examGroup == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"没有找到分组";
                return result;
            }

            examGroup.ExamPaperGroupStudentList = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.From<YssxUser, YssxPosition>((a, b, c) => a.InnerJoin(aa => aa.UserId == b.Id).InnerJoin(aa => aa.PostionId == c.Id))
                    .Where((a, b, c) => a.GroupId == groupId)
                    .ToListAsync((a, b, c) => new ExamPaperGroupStudentView
                    {
                        GroupId = groupId,
                        PostionId = a.PostionId,
                        UserId = a.UserId,
                        UserName = b.RealName,
                        PostionName = c.Name,
                    });

            if (!string.IsNullOrEmpty(examGroup.Password))
            {
                if (examGroup.CreateBy != userId)
                {
                    examGroup.Password = "******";
                }
            }

            result.Data = examGroup;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }


        /// <summary>
        /// 创建分组 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperGroupListView>> CreateExamGroup(ExamPaperGroupUpdateDto model, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperGroupListView>();
            
            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            #region 校验

            if (model.ExamId == 0 || model.PostionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"ExamId,PostionId都不能为0 ";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == model.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{model.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            if (exam.CompetitionType != CompetitionType.TeamCompetition)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"只有团队赛才能创建房间 ";
                return result;
            }

            if (exam.ExamType != ExamType.EmulationTest)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"该赛事不能创建房间  ";
                return result;
            }

            if (!DbContext.FreeSql.GetRepository<YssxCasePosition>().Select.Any(s => s.CaseId == exam.CaseId && s.PostionId == model.PostionId && s.IsDelete == CommonConstants.IsNotDelete))
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"岗位信息不存在 ";
                return result;
            }

            #endregion

            return await Task.Run(() =>
            {
                result = RedisLock.WaitLock(userId.ToString(), () =>
                {
                    var lockResult = new ResponseContext<ExamPaperGroupListView>();

                    switch (exam.ExamType)
                    {
                        case ExamType.EmulationTest:
                            if (DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.Any(s => s.ExamId == exam.Id && s.UserId == userId && s.StudentExamStatus != StudentExamStatus.End))//存在正在进行的模拟团队赛
                            {
                                lockResult.Code = CommonConstants.ErrorCode;
                                lockResult.Msg = $"你有正在进行的团队赛，不能创建分组 ";
                                return lockResult;
                            }
                            break;
                        default:
                            if (DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.Any(s => s.ExamId == exam.Id && s.UserId == userId))//存在正在进行的团队赛
                            {
                                lockResult.Code = CommonConstants.ErrorCode;
                                lockResult.Msg = $"你有正在进行的团队赛，不能创建分组 ";
                                return lockResult;
                            }
                            break;
                    }

                    #region 插入数据

                    var maxGroupNo = RedisHelper.IncrBy(CommonConstants.ExamMaxGroupNo + exam.Id);

                    var examGroup = new ExamPaperGroup()
                    {
                        Id = IdWorker.NextId(),
                        ExamId = exam.Id,
                        GroupNo = maxGroupNo,
                        CaseId = exam.CaseId,
                        Password = model.Password,
                        UserId = userId,
                        CreateBy = userId,
                        TenantId = tenantId,
                        CreateTime = DateTime.Now
                    };
                    var groupStudent = new ExamPaperGroupStudent()
                    {
                        Id = IdWorker.NextId(),
                        ExamId = exam.Id,
                        CaseId = exam.CaseId,
                        GroupId = examGroup.Id,
                        UserId = userId,
                        TenantId = tenantId,
                        PostionId = model.PostionId,
                        IsReady = false,
                        CreateBy = userId
                    };

                    DbContext.FreeSql.Transaction(() =>
                    {
                        DbContext.FreeSql.GetRepository<ExamPaperGroup>().Insert(examGroup);
                        DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Insert(groupStudent);
                    });

                    #endregion

                    var postionName = DbContext.FreeSql.GetRepository<YssxPosition>().Where(s => s.Id == groupStudent.PostionId).First($"{CommonConstants.Cache_GetPositionById}{groupStudent.PostionId}", true, 10 * 60).Name;

                    lockResult.Data = new ExamPaperGroupListView()
                    {
                        GroupId = examGroup.Id,
                        GroupNo = examGroup.GroupNo,
                        Password = examGroup.Password,
                        Status = GroupStatus.Started,
                        CreateBy = examGroup.CreateBy,
                        ExamPaperGroupStudentList = new List<ExamPaperGroupStudentView>()
                        {
                            new ExamPaperGroupStudentView()
                            {
                                GroupId= groupStudent.GroupId,
                                PostionId = groupStudent.PostionId,
                                PostionName = postionName,
                                UserId = groupStudent.UserId,
                                IsReady= groupStudent.IsReady,
                                UserName = userTicket.RealName,
                            }
                        }
                    };

                    return lockResult;
                });

                return result;
            });
        }

        /// <summary>
        /// 加入分组 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <param name="password">分组密码</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> JoinExamGroup(long groupId, long postionId, UserTicket userTicket,string password = "")
        {
            if (groupId == 0 || postionId == 0)
            {
                return new ResponseContext<bool>() { Code = CommonConstants.BadRequest, Msg = $"groupId,postionId都不能为0" };
            }

            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            var examgroup = await DbContext.FreeSql.GetRepository<ExamPaperGroup>().Where(s => s.Id == groupId).FirstAsync(s => new { s.Id, s.ExamId, s.Password });

            if (examgroup == null || examgroup.Id == 0)
            {
                return new ResponseContext<bool>() { Code = CommonConstants.NotFund, Msg = $"分组信息不存在 " };
            }

            if (!string.IsNullOrEmpty(examgroup.Password))
            {
                if (!examgroup.Password.Equals(password))
                {
                    return new ResponseContext<bool>() { Code = CommonConstants.NotFund, Msg = $"分组密码不正确  " };
                }
            }

            return await Task.Run(() =>
            {
                var result = RedisLock.WaitLock(userId.ToString(), () =>
                {
                    return RedisLock.WaitLock(groupId.ToString(), () =>
                    {
                        var lockResult = new ResponseContext<bool>(false);

                        var groupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Where(s => s.GroupId == groupId).ToList(s => new { s.ExamId, s.CaseId, s.PostionId, s.Status });

                        if (groupStudentList != null && groupStudentList.Any())
                        {
                            if (groupStudentList.Any(s => s.PostionId == postionId))//判断该岗位已被占用
                            {
                                lockResult.Code = CommonConstants.ErrorCode;
                                lockResult.Msg = $"该岗位已被占用 ";
                                return lockResult;
                            }

                            if (groupStudentList[0].Status != GroupStatus.Started)
                            {
                                lockResult.Code = CommonConstants.ErrorCode;
                                lockResult.Msg = $"不能加入该分组，组员已满 ";
                                return lockResult;
                            }

                            var casePosition = DbContext.FreeSql.GetRepository<YssxCasePosition>().Where(s => s.CaseId == groupStudentList[0].CaseId && s.PostionId == postionId && s.IsDelete == CommonConstants.IsNotDelete)
                            .FirstAsync($"{CommonConstants.Cache_GetCasePosition}{groupStudentList[0].CaseId}-{postionId}", true, 10 * 60, true).Result;
                            if (casePosition == null)
                            {
                                lockResult.Code = CommonConstants.NotFund;
                                lockResult.Msg = $"岗位信息不存在 ";
                                return lockResult;
                            }
                        }

                        var exam = DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == examgroup.ExamId && s.IsDelete == CommonConstants.IsNotDelete).First($"{CommonConstants.Cache_GetExamById}{examgroup.ExamId}", true, 10 * 60);
                        if (exam == null)
                        {
                            lockResult.Code = CommonConstants.NotFund;
                            lockResult.Msg = $"考试信息不存在 ";
                            return lockResult;
                        }

                        switch (exam.ExamType)
                        {
                            case ExamType.EmulationTest:
                                if (DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.Any(s => s.ExamId == exam.Id && s.UserId == userId && s.StudentExamStatus != StudentExamStatus.End))//存在正在进行的模拟团队赛
                                {
                                    lockResult.Code = CommonConstants.ErrorCode;
                                    lockResult.Msg = $"你已经有分组了，退出原来的分组或者交卷后才能参加其它测试 ";
                                    return lockResult;
                                }
                                break;
                            default:
                                if (DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Select.Any(s => s.ExamId == exam.Id && s.UserId == userId))//存在正在进行的团队赛
                                {
                                    lockResult.Code = CommonConstants.ErrorCode;
                                    lockResult.Msg = $"你已经有分组了，不能再次加入分组 ";
                                    return lockResult;
                                }
                                break;
                        }

                        var groupStudent = new ExamPaperGroupStudent()
                        {
                            Id = IdWorker.NextId(),
                            ExamId = exam.Id,
                            CaseId = exam.CaseId,
                            GroupId = groupId,
                            UserId = userId,
                            TenantId = tenantId,
                            PostionId = postionId,
                            CreateBy = userId
                        };
                        DbContext.FreeSql.Transaction(() =>
                        {
                            if (groupStudentList != null && groupStudentList.Count == 3)
                            {
                                lockResult.Data = true;
                                groupStudent.Status = GroupStatus.Full;
                                DbContext.FreeSql.GetRepository<ExamPaperGroup>().UpdateDiy.UpdateColumns(s => s.Status).Set(s => s.Status == GroupStatus.Full).Where(s => s.Id == groupId).ExecuteAffrows();
                                DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy.UpdateColumns(s => s.Status).Set(s => s.Status == GroupStatus.Full).Where(s => s.GroupId == groupId).ExecuteAffrows();
                            }
                            DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Insert(groupStudent);
                        });

                        return lockResult;
                    });
                });

                return result;
            });
        }

        /// <summary>
        /// 切换岗位 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ChangeExamGroupPosition(long groupId, long postionId,UserTicket userTicket)
        {
            if (groupId == 0 || postionId == 0)
            {
                return new ResponseContext<bool>() { Code = CommonConstants.BadRequest, Msg = $"groupId,postionId都不能为0" };
            }

            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            return await Task.Run(() =>
            {
                var result = RedisLock.WaitLock(userId.ToString(), () =>
                {
                    return RedisLock.WaitLock(groupId.ToString(), () =>
                    {
                        var lockResult = new ResponseContext<bool>(false);

                        var groupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Where(s => s.GroupId == groupId).ToList(s => new { s.UserId, s.CaseId, s.ExamId, s.PostionId, s.Status });

                        if (groupStudentList == null || !groupStudentList.Any())
                        {
                            lockResult.Code = CommonConstants.NotFund;
                            lockResult.Msg = $"分组信息不存在 ";
                            return lockResult;
                        }

                        var exam = DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == groupStudentList[0].ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{groupStudentList[0].ExamId}", true, 10 * 60).Result;

                        //if (exam.ExamType != ExamType.EmulationTest)
                        //{
                        //    lockResult.Code = CommonConstants.ErrorCode;
                        //    lockResult.Msg = $"该赛事不能切换岗位  ";
                        //    return lockResult;
                        //}

                        if (groupStudentList[0].Status == GroupStatus.Complete || groupStudentList[0].Status == GroupStatus.End)
                        {
                            lockResult.Code = CommonConstants.NotFund;
                            lockResult.Msg = $"考试已开始，不能再切换岗位  ";
                            return lockResult;
                        }

                        if (!groupStudentList.Any(s => s.UserId == userId))
                        {
                            lockResult.Code = CommonConstants.ErrorCode;
                            lockResult.Msg = $"您还没有加入分组，不能切换岗位 ";
                            return lockResult;
                        }

                        if (groupStudentList.FirstOrDefault(s => s.UserId == userId).PostionId== postionId)
                        {
                            lockResult.Code = CommonConstants.ErrorCode;
                            lockResult.Msg = $"切换的岗位和现有岗位一致 ";
                            return lockResult;
                        }

                        if (groupStudentList.Any(s => s.PostionId == postionId))//判断该岗位已被占用
                        {
                            lockResult.Code = CommonConstants.ErrorCode;
                            lockResult.Msg = $"该岗位已被占用 ";
                            return lockResult;
                        }

                        var casePosition = DbContext.FreeSql.GetRepository<YssxCasePosition>().Where(s => s.CaseId == groupStudentList[0].CaseId && s.PostionId == postionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .FirstAsync($"{CommonConstants.Cache_GetCasePosition}{groupStudentList[0].CaseId}-{postionId}", true, 10 * 60, true).Result;
                        if (casePosition == null)
                        {
                            lockResult.Code = CommonConstants.NotFund;
                            lockResult.Msg = $"岗位信息不存在 ";
                            return lockResult;
                        }

                        DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy
                        .UpdateColumns(s => new { s.PostionId })
                        .Set(s => s.PostionId, postionId).Where(s => s.GroupId == groupId && s.UserId == userId).ExecuteAffrows();

                        return lockResult;
                    });
                });

                return result;
            });
        }

        /// <summary>
        /// 退出分组(如果是创建人退出，则解散分组)
        /// </summary>
        /// <param name="groupId">分组Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ExitExamGroup(long groupId,long userId)
        {

            return await Task.Run(() =>
            {
                var result = RedisLock.WaitLock(groupId.ToString(), () =>
                {
                    var lockResult = new ResponseContext<bool>(true);

                    var examGroup = DbContext.FreeSql.GetRepository<ExamPaperGroup>().Select.Where(s => s.Id == groupId).First(s => new { s.Id, s.ExamId, s.Status, s.CreateBy });
                    if (examGroup == null)
                    {
                        lockResult.Code = CommonConstants.NotFund;
                        lockResult.Msg = $"分组信息不存在 ";
                        return lockResult;
                    }

                    var exam = DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == examGroup.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{examGroup.ExamId}", true, 10 * 60).Result;

                    if (exam.ExamType == ExamType.ProvincialCompetition)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"该赛事不能退出分组   ";
                        return lockResult;
                    }

                    if (examGroup.Status == GroupStatus.Complete || examGroup.Status == GroupStatus.End)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"考试已开始，不能退出 ";
                        return lockResult;
                    }

                    DbContext.FreeSql.Transaction(() =>
                    {
                        if (examGroup.CreateBy == userId)
                        {
                            DbContext.FreeSql.GetRepository<ExamPaperGroup>().Delete(s => s.Id == groupId);
                            DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Delete(s => s.GroupId == groupId);
                        }
                        else
                        {
                            var resultCount = DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Delete(s => s.GroupId == groupId && s.UserId == userId);
                            if (resultCount > 0)
                            {
                                DbContext.FreeSql.GetRepository<ExamPaperGroup>().UpdateDiy
                                .UpdateColumns(s => s.Status)
                                .Set(s => s.Status == GroupStatus.Started).Where(s => s.Id == groupId).ExecuteAffrows();

                                DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy
                                .UpdateColumns(s => s.Status)
                                .Set(s => s.Status == GroupStatus.Started).Where(s => s.GroupId == groupId).ExecuteAffrows();
                            }
                        }
                    });

                    return lockResult;
                });

                return result;
            });
        }

        /// <summary>
        /// 更改准备状态
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="IsReady">true:已准备，false:未准备</param>
        /// <returns>bool,true表示分组成员全部准备完毕，否则未全部准备</returns>
        public async Task<ResponseContext<bool>> UpdateReadyStatus(long groupId, UserTicket userTicket,bool IsReady = false)
        {
            if (groupId == 0)
            {
                return new ResponseContext<bool>() { Code = CommonConstants.BadRequest, Msg = $"groupId不能为0" };
            }
            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            return await Task.Run(() =>
            {
                var result = RedisLock.WaitLock(groupId.ToString(), () =>
                {
                    var lockResult = new ResponseContext<bool>(false);

                    var groupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Where(s => s.GroupId == groupId).ToList(s => new { s.UserId, s.Status, s.IsReady });

                    if (groupStudentList == null || !groupStudentList.Any())
                    {
                        lockResult.Code = CommonConstants.NotFund;
                        lockResult.Msg = $"分组信息不存在 ";
                        return lockResult;
                    }

                    if (!groupStudentList.Any(s => s.UserId == userId))//是否是组员
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"分组信息不存在 ";
                        return lockResult;
                    }

                    if (groupStudentList[0].Status == GroupStatus.Complete || groupStudentList[0].Status == GroupStatus.End)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"考试已开始，不能修改准备状态 ";
                        return lockResult;
                    }

                    var readyCount = groupStudentList.Count(s => s.IsReady == true);

                    DbContext.FreeSql.Transaction(() =>
                    {
                        if (IsReady)
                        {
                            if (groupStudentList.Any(s => s.UserId == userId && s.IsReady == false))//当前人状态为未准备
                            {
                                if (readyCount == 3)
                                {
                                    lockResult.Data = true;
                                    DbContext.FreeSql.GetRepository<ExamPaperGroup>().UpdateDiy
                                    .UpdateColumns(s => s.Status)
                                    .Set(s => s.Status == GroupStatus.AllReady).Where(s => s.Id == groupId).ExecuteAffrows();

                                    DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy
                                    .UpdateColumns(s => s.Status)
                                    .Set(s => s.Status == GroupStatus.AllReady).Set(s => s.IsReady == true).Where(s => s.GroupId == groupId).ExecuteAffrows();
                                }
                                else
                                {
                                    DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy
                                    .UpdateColumns(s => s.IsReady)
                                    .Set(s => s.IsReady == true).Where(s => s.GroupId == groupId && s.UserId == userId).ExecuteAffrows();
                                }
                            }
                            else
                            {
                                if (groupStudentList[0].Status == GroupStatus.AllReady)//准备完毕
                                {
                                    lockResult.Data = true;
                                }
                            }
                        }
                        else//取消准备
                        {
                            if (groupStudentList[0].Status == GroupStatus.AllReady)//准备完毕
                            {
                                DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy.UpdateColumns(s => s.IsReady).Set(s => s.IsReady == false).Where(s => s.GroupId == groupId && s.UserId == userId).ExecuteAffrows();
                                DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy.UpdateColumns(s => s.Status).Set(s => s.Status == GroupStatus.Full).Where(s => s.GroupId == groupId).ExecuteAffrows();
                                DbContext.FreeSql.GetRepository<ExamPaperGroup>().UpdateDiy.UpdateColumns(s => s.Status).Set(s => s.Status == GroupStatus.Full).Where(s => s.Id == groupId).ExecuteAffrows();
                            }
                            else
                            {
                                DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy.UpdateColumns(s => s.IsReady).Set(s => s.IsReady == false).Where(s => s.GroupId == groupId && s.UserId == userId).ExecuteAffrows();
                            }
                        }
                    });

                    return lockResult;
                });

                return result;
            });
        }

    }
}
