﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class NoticeService : INoticeService
    {
        public async Task<ResponseContext<bool>> AddOrEdit(NoticeDto dto)
        {
            YssxNotice entity = new YssxNotice
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                Title = dto.Title,
                Platform = dto.Platform,
                Sort = dto.Sort,
                Status = dto.Status,
                CreateTime = DateTime.Now,
                Content = dto.Content,
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                await DbContext.FreeSql.GetRepository<YssxNotice>().InsertAsync(entity);
            }
            else
            {
                await DbContext.FreeSql.GetRepository<YssxNotice>().UpdateDiy.
                    SetSource(entity).UpdateColumns(x => new
                    {
                        x.Title,
                        x.Platform,
                        x.Sort,
                        x.Status,
                        x.Content,
                    }).ExecuteAffrowsAsync();
            }
            return new ResponseContext<bool> { Data = state };
        }

        public async Task<ResponseContext<List<NoticeDto>>> GetList()
        {
            List<NoticeDto> list = await DbContext.FreeSql.GetRepository<YssxNotice>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync<NoticeDto>();

            return new ResponseContext<List<NoticeDto>> { Data = list };
        }

        public async Task<ResponseContext<List<NoticeDto>>> GetListByPlatform(int platform)
        {
            List<NoticeDto> list = await DbContext.FreeSql.GetRepository<YssxNotice>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.Platform == platform && x.Status == Status.Enable)
                .ToListAsync<NoticeDto>();

            return new ResponseContext<List<NoticeDto>> { Data = list };
        }

        public async Task<ResponseContext<bool>> SetStatus(long id, Status status)
        {
            int x = await DbContext.FreeSql.GetRepository<YssxNotice>()
                .UpdateDiy.Set(a => a.Status, status)
                .Where(a => a.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Data = x > 0 };
        }
    }
}
