﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Extensions;
using Yssx.S.Dto.Home;
using Yssx.S.IServices;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;
using System.Linq;
using System;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;

namespace Yssx.S.ServiceImpl
{
    public class HomeService : IHomeService
    {
        long tenantId = 1017666019115035;
        public HomeService()
        {

        }

        public async Task<PageResponse<AccountantTrainingDto>> GetAccountantTrainingList(PageRequest model)
        {
            return await Task.Run(() =>
            {
                //List<YssxIndustry> industries = DbContext.FreeSql.GetRepository<YssxIndustry>().Where(x => x.IsDelete == 0).ToList("", true, 720 * 60, false);
                List<AccountantTrainingDto> list = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeAccountantList}",
                    () => DbContext.FreeSql.GetRepository<ExamPaper>().Select.From<YssxCase, YssxIndustry>((a, b, c) => a.InnerJoin(x => x.CaseId == b.Id).InnerJoin(x => b.Industry == c.Id)).
                    Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.TenantId == tenantId && a.TaskId == 0 && a.CreateBy == 1).
                    ToList((a, b, c) => new AccountantTrainingDto
                    {
                        CaseId = a.CaseId,
                        ExamId = a.Id,
                        Name = a.Name,
                        CaseYear = b.CaseYear,
                        Education = b.Education,
                        SxType = b.SxType,
                        IndustryName = c.Name,
                        IndustryId = c.Id,
                    }), 720 * 60, true, false);
                return new PageResponse<AccountantTrainingDto> { Data = list };
            });
        }

        public async Task<PageResponse<CaseTrainingDto>> GetCaseTrainingList(PageRequest model)
        {
            return await Task.Run(() =>
            {
                List<CaseTrainingDto> list = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeCaseList}", () => DbContext.FreeSql.GetRepository<YssxUploadCase>().Where(x => x.IsDelete == 0 && x.AuditStatus == 1).ToList(x => new CaseTrainingDto
                {
                    Id = x.Id,
                    Author = x.Uploads,
                    Title = x.Title
                }), 720 * 60, true, false);
                List<long> ids = new List<long> { 1000262922291746, 1005114702006651, 1003386340573484, 1007634833120703, 1000260321823245, 1002843340261759 };
                List<CaseTrainingDto> list2 = list.Where(x => ids.Contains(x.Id)).ToList();
                return new PageResponse<CaseTrainingDto> { Data = list2 };
            });

        }

        public async Task<ResponseContext<CourseTrainingDto>> GetCourseInfoById(long id)
        {
            YssxCourse x = await DbContext.FreeSql.GetRepository<YssxCourse>().Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.Id == id).FirstAsync($"{CommonConstants.Cache_GetCourseById}{id}", true, 30 * 60, true, false);
            if (x == null) return new ResponseContext<CourseTrainingDto> { };
            CourseTrainingDto dto = new CourseTrainingDto
            {
                Id = x.Id,
                CourseName = x.CourseName,
                CourseTitle = x.CourseTitle,
                Education = x.Education,
                Images = x.Images,
                Intro = x.Intro,
                Author = x.Author,
                Description = x.Description,
                PublishingDate = x.PublishingDate,
                UseType = x.CourseType,

            };
            return new ResponseContext<CourseTrainingDto> { Data = dto };
        }

        public async Task<PageResponse<CourseTrainingDto>> GetCourseTrainingList(PageRequest model)
        {
            return await Task.Run(() =>
            {
                List<CourseTrainingDto> list = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeCourseList}", () => DbContext.FreeSql.GetRepository<YssxCourse>().Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.AuditStatus == 1 && s.TenantId == 981239287939076).ToList(x => new CourseTrainingDto
                {
                    Id = x.Id,
                    CourseName = x.CourseName,
                    CourseTitle = x.CourseTitle,
                    Education = x.Education,
                    Images = x.Images,
                    Intro = x.Intro,
                    Author = x.Author,
                    Description = x.Description,
                    PublishingDate = x.PublishingDate,
                    UseType = x.CourseType,
                }), 720 * 60, true, false);
                //List<long> ids = new List<long> { 1016778774003713, 1001685274856250, 1001667621030087, 1000677483413688, 996897164395450, 1007278048842908, 1020282696515763, 1019687317193246 };
                List<long> ids = new List<long> { 1022757867179643, 1001685274856250, 1001667621030087, 1000677483413688, 996897164395450, 1007278048842908, 1020282696515763, 1003274205892305, 993450031071329 };
                List<CourseTrainingDto> list2 = list.Where(x => ids.Contains(x.Id)).OrderBy(x => x.Education).ToList();
                return new PageResponse<CourseTrainingDto> { Data = list2 };
            });
        }



        public async Task<PageResponse<HomeSceneTrainingDto>> GetSceneTrainingList(PageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase>((sc, bu, ca) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
                                                                                                                                 .LeftJoin(aa => aa.SelectCompanyId == ca.Id))
                         .Where((sc, bu, ca) => sc.IsDelete == CommonConstants.IsNotDelete && sc.AuditStatus == 1).OrderBy((sc, bu, ca) => sc.Pxxh);
                var sql = select.OrderByDescending((sc, bu, ca) => sc.CreateTime)
               .Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.Name,a.SelectCompanyId,a.YwcjId,a.Pxxh,a.Ywwl,a.Zzjy,a.Describe,bu.Name YwcjName,ca.Name SelectCompanyName");
                List<HomeSceneTrainingDto> items = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeSceneList}", () => DbContext.FreeSql.Ado.Query<HomeSceneTrainingDto>(sql), 720 * 60, false, false);
                List<long> ids = new List<long> { 1021429947070812, 1023594368779479, 1023758708826987, 1024033239484471, };
                List<HomeSceneTrainingDto> list2 = items.Where(x => ids.Contains(x.Id)).ToList();
                return new PageResponse<HomeSceneTrainingDto> { Data = list2 };
            });

        }

        public async Task<PageResponse<SynthesizeTrainingDto>> GetSynthesizeTrainingList(PageRequest model)
        {
            return await Task.Run(() =>
            {
                //List<YssxIndustry> industries = DbContext.FreeSql.GetRepository<YssxIndustry>().Where(x => x.IsDelete == 0).ToList("", true, 720 * 60, false);
                List<SynthesizeTrainingDto> list = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeAccountantList}",
                    () => DbContext.FreeSql.GetRepository<ExamPaper>().Select.From<YssxCase, YssxIndustry>((a, b, c) => a.InnerJoin(x => x.CaseId == b.Id).InnerJoin(x => b.Industry == c.Id)).
                    Where((a, b, c) => a.IsDelete == CommonConstants.IsDelete && b.IsDelete == CommonConstants.IsDelete && a.TenantId == tenantId).
                    ToList((a, b, c) => new SynthesizeTrainingDto
                    {
                        CaseId = a.CaseId,
                        ExamId = a.Id,
                        Name = a.Name,
                        CaseYear = b.CaseYear,
                        Education = b.Education,
                        SxType = b.SxType,
                        IndustryName = c.Name,
                        IndustryId = c.Id,
                    }), 720 * 60, true, false);
                return new PageResponse<SynthesizeTrainingDto> { Data = list };
            });
        }

        public async Task<ResponseContext<bool>> ReadStatistics(long id, string ip)
        {
            await DbContext.FreeSql.GetRepository<YssxReadStat>().InsertAsync(new YssxReadStat { Id = IdWorker.NextId(), UserType = id, Ip = ip, CreateTime = DateTime.Now });
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<bool>> Remind11(string moblie)
        {
            bool exist = await DbContext.FreeSql.GetRepository<YssxRemind11>().Where(x => x.Moblie == moblie).AnyAsync();
            bool isOk = exist;
            if (!exist)
            {
                isOk = await DbContext.FreeSql.GetRepository<YssxRemind11>().InsertAsync(new YssxRemind11 { Id = IdWorker.NextId(), Moblie = moblie, CreateTime = DateTime.Now }) != null;
            }

            return new ResponseContext<bool> { Data = isOk, Code = isOk ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = isOk ? "OK" : "失败,稍后再试!" };
        }

        public async Task<ResponseContext<long>> GetReadCount()
        {
            long count = await DbContext.FreeSql.GetRepository<YssxReadStat>().Select.CountAsync();
            return new ResponseContext<long> { Data = count };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<ReadStatisticsDto>>> GetReadList()
        {
            string sql = "SELECT a.Name,sum(a.Count) Count FROM yssx_region_statistics a GROUP BY a.Name";
            List<ReadStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<ReadStatisticsDto>(sql);


            return new ResponseContext<List<ReadStatisticsDto>> { Data = list };
        }
    }
}
