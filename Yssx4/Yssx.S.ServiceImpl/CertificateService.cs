﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    public class CertificateService : ICertificateService
    {
        private const string StageBalance = "期初余额", StageTotal = "本期合计", YearStageTotal = "本年累计";

        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type)
        {
            List<YssxSubject> yssxCoreSubjects = await GetSearchSubjects(id, subjectType, keyword);

            return GetBalanceList(yssxCoreSubjects, gradeId, id, type);
        }

        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type, string dateStr)
        {
            List<YssxSubject> yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id).ToListAsync($"{CommonConstants.Cache_GetCertificateListByCaseId}{id}", true, 10 * 60);
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                var enterprise = DbContext.FreeSql.Select<YssxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                stageStr = enterprise.AccountingPeriodDate;
            }
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.ErrorCode, Data = null, Msg = "案例没有设置会计期间!" };
            }
            List<CertificateDto> allCertificateDtos = GetAllSubsidiary(gradeId, id, type);
            var rst = GetSubjectLedgerSummer(yssxCoreSubjects, subjectId, allCertificateDtos, stageStr);
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = rst };

        }

        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr)
        {
            List<YssxSubject> yssxCoreSubjects = await GetSearchSubjects(id, subjectType, keyword);
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                var enterprise = DbContext.FreeSql.Select<YssxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                stageStr = enterprise.AccountingPeriodDate;
            }

            List<YssxSubject> p_list = yssxCoreSubjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            List<CertificateDto> allCertificateDtos = GetAllSubsidiary(gradeId, id, type);

            var summaries = new[] { StageBalance, StageTotal, YearStageTotal };
            var subjects = new List<CertificateDto>();

            if (string.IsNullOrWhiteSpace(stageStr))
            {
                return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.ErrorCode, Data = null, Msg = "案例没有设置会计期间!" };
            }
            foreach (var rootItem in p_list)
            {
                var rst = GetSubjectLedgerSummer(yssxCoreSubjects, rootItem.Id, allCertificateDtos, stageStr);

                foreach (var dto in rst)
                {
                    if (!summaries.Contains(dto.SummaryInfo))
                        continue;
                    dto.Stage = stageStr;
                    subjects.Add(dto);
                }
            }
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = subjects };
        }

        #region 私有方法
        private void FindChildNode(long id, List<YssxSubject> retList, List<YssxSubject> enterpriseSubjects)
        {
            if (id == 0) return;
            List<YssxSubject> yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, retList, enterpriseSubjects);
            }
        }

        /// <summary>
        /// 获取科目余额数据列表
        /// </summary>
        /// <param name="yssxCoreSubjects"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="gradeId"></param>
        /// <param name="type">0：后端，1：前端</param>
        /// <returns></returns>
        private ResponseContext<List<CertificateBalanceDto>> GetBalanceList(List<YssxSubject> yssxCoreSubjects, long gradeId, long enterpriseId, int type)
        {
            ResponseContext<List<CertificateBalanceDto>> response = new ResponseContext<List<CertificateBalanceDto>>();

            //科目余额数据集合
            List<CertificateBalanceDto> yssxSubjetBalances = new List<CertificateBalanceDto>();
            List<YssxExamCertificateDataRecord> exam_certificateDataRecords = new List<YssxExamCertificateDataRecord>();
            List<YssxCertificateDataRecord> certificateDataRecords = new List<YssxCertificateDataRecord>();
            if (type == 1)
            {
                exam_certificateDataRecords = DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecord>().Where(x => x.CaseId == enterpriseId && x.GradeId == gradeId).ToList();
            }
            else
            {
                certificateDataRecords = DbContext.FreeSql.GetRepository<YssxCertificateDataRecord>().Where(x => x.EnterpriseId == enterpriseId).ToList();
            }

            foreach (var item in yssxCoreSubjects)
            {
                CertificateBalanceDto coreSubjetBalance = new CertificateBalanceDto();
                //coreSubjetBalance.CoreSubjectId = item.Id;
                coreSubjetBalance.Id = item.Id;// IdWorker.NextId();
                coreSubjetBalance.SubjectCode = item.SubjectCode;
                coreSubjetBalance.ParentId = item.ParentId;
                coreSubjetBalance.EnterpriseId = item.EnterpriseId;
                var d = GetCertificateData(item.Id, type, exam_certificateDataRecords, certificateDataRecords);
                coreSubjetBalance.CurrentBorrowAmount = d.Item1; //本期借方数据
                coreSubjetBalance.CurrentCreditorAmount = d.Item2;//本期贷方数据
                coreSubjetBalance.BorrowAmount = item.BorrowAmount;
                coreSubjetBalance.CreditorAmount = item.CreditorAmount;

                //计算期末数据
                decimal _finalAmount = (item.BorrowAmount + coreSubjetBalance.CurrentBorrowAmount) - (item.CreditorAmount + coreSubjetBalance.CurrentCreditorAmount);

                if (_finalAmount > 0)
                {
                    coreSubjetBalance.FinalBorrowAmount = _finalAmount;
                }
                else if (_finalAmount < 0)
                {
                    coreSubjetBalance.FinalCreditorAmount = -_finalAmount;
                }

                yssxSubjetBalances.Add(coreSubjetBalance);
            }

            //子级目录相加
            SetChildList(yssxSubjetBalances);

            ///最后数据设置
            List<YssxSubject> retLis = new List<YssxSubject>();
            List<YssxSubject> coreSubjects_temps = yssxCoreSubjects.ToList();
            // List<YssxSubject> subjects = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == enterpriseId && x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            List<YssxSubject> subjects = yssxCoreSubjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();

            foreach (var item in subjects)
            {
                List<YssxSubject> list3 = coreSubjects_temps.Where(o => o.ParentId == item.Id).ToList();
                if (list3.Count > 0)
                {
                    item.BorrowAmount = list3.Sum(b => b.BorrowAmount);
                    item.CreditorAmount = list3.Sum(b => b.CreditorAmount);
                }
                retLis.Add(item);
                FindChildNode(item.Id, retLis, yssxCoreSubjects);
            }

            response.Data = SetLastList(retLis, yssxSubjetBalances);
            response.Code = CommonConstants.SuccessCode;
            return response;
        }

        /// <summary>
        /// 设置子目录相加
        /// </summary>
        private void SetChildList(List<CertificateBalanceDto> yssxSubjetBalances)
        {
            List<CertificateBalanceDto> list1 = yssxSubjetBalances.Where(o => o.ParentId > 0).OrderByDescending(x => x.SubjectCode).ToList();
            var index = list1.FindIndex(x => x.SubjectCode == 112502);
            list1.ForEach(x =>
            {
                List<CertificateBalanceDto> temps = yssxSubjetBalances.Where(o => o.ParentId == x.Id).ToList();

                if (temps.Count > 0)
                {
                    x.CurrentCreditorAmount = temps.Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = temps.Sum(c => c.CurrentBorrowAmount);
                    decimal _finalAmount = (temps.Sum(c => c.BorrowAmount) + temps.Sum(c => c.CurrentBorrowAmount)) - (temps.Sum(c => c.CreditorAmount) + temps.Sum(c => c.CurrentCreditorAmount));
                    if (_finalAmount > 0)
                    {
                        x.FinalBorrowAmount = _finalAmount;
                    }
                    else if (_finalAmount < 0)
                    {
                        x.FinalCreditorAmount = -_finalAmount;
                    }
                    //x.FinalBorrowAmount = temps.Sum(c => c.FinalBorrowAmount);
                    //x.FinalCreditorAmount = temps.Sum(c => c.FinalCreditorAmount);
                }
            });

            List<CertificateBalanceDto> list2 = yssxSubjetBalances.Where(o => o.ParentId == 0).ToList();
            list2.ForEach(x =>
            {
                List<CertificateBalanceDto> temps = list1.Where(o => o.ParentId == x.Id).ToList();

                if (temps.Count > 0)
                {
                    x.CurrentCreditorAmount = temps.Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = temps.Sum(c => c.CurrentBorrowAmount);
                    decimal _finalAmount = (temps.Sum(c => c.BorrowAmount) + temps.Sum(c => c.CurrentBorrowAmount)) - (temps.Sum(c => c.CreditorAmount) + temps.Sum(c => c.CurrentCreditorAmount));
                    if (_finalAmount > 0)
                    {
                        x.FinalBorrowAmount = _finalAmount;
                    }
                    else if (_finalAmount < 0)
                    {
                        x.FinalCreditorAmount = -_finalAmount;
                    }
                }
            });

            yssxSubjetBalances.ForEach(x =>
            {
                if (x.ParentId == 0)
                {
                    x.CurrentCreditorAmount = list2.Where(o => o.Id == x.Id).Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = list2.Where(o => o.Id == x.Id).Sum(c => c.CurrentBorrowAmount);
                }
                else
                {
                    x.CurrentCreditorAmount = list1.Where(o => o.Id == x.Id).Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = list1.Where(o => o.Id == x.Id).Sum(c => c.CurrentBorrowAmount);

                }
            });
        }

        /// <summary>
        /// 计算最后的结果
        /// </summary>
        /// <param name="retLis"></param>
        /// <param name="certificateBalances"></param>
        /// <returns></returns>
        private List<CertificateBalanceDto> SetLastList(List<YssxSubject> retLis, List<CertificateBalanceDto> certificateBalances)
        {
            List<CertificateBalanceDto> _dtoList = DataToModel(retLis);

            _dtoList.ForEach(x =>
            {
                try
                {
                    CertificateBalanceDto yb = certificateBalances.Where(o => o.Id == x.Id).FirstOrDefault();
                    if (yb != null)
                    {
                        x.CurrentBorrowAmount = yb.CurrentBorrowAmount;
                        x.CurrentCreditorAmount = yb.CurrentCreditorAmount;
                        x.FinalBorrowAmount = yb.FinalBorrowAmount;
                        x.FinalCreditorAmount = yb.FinalCreditorAmount;
                        x.CurrentBorrowAmountStr = yb.CurrentBorrowAmount > 0 ? yb.CurrentBorrowAmount.ToString() : null;
                        x.CurrentCreditorAmountStr = yb.CurrentCreditorAmount > 0 ? yb.CurrentCreditorAmount.ToString() : null;
                        if (yb.ParentId == 0)
                        {
                            decimal _finalAmount = (x.BorrowAmount + yb.CurrentBorrowAmount) - (x.CreditorAmount + yb.CurrentCreditorAmount);

                            if (_finalAmount > 0)
                            {
                                x.FinalBorrowAmount = _finalAmount;
                                x.FinalBorrowAmountStr = x.FinalBorrowAmount > 0 ? x.FinalBorrowAmount.ToString() : null;
                            }
                            else if (_finalAmount < 0)
                            {
                                x.FinalCreditorAmount = -_finalAmount;
                                x.FinalCreditorAmountStr = x.FinalCreditorAmount > 0 ? x.FinalCreditorAmount.ToString() : null;
                            }
                        }
                        else
                        {
                            x.FinalBorrowAmountStr = yb.FinalBorrowAmount > 0 ? yb.FinalBorrowAmount.ToString() : null;
                            x.FinalCreditorAmountStr = yb.FinalCreditorAmount > 0 ? yb.FinalCreditorAmount.ToString() : null;
                        }

                        if (x.BorrowAmount > 0) x.BorrowAmountStr = x.BorrowAmount.ToString();
                        else if (x.BorrowAmount < 0) x.BorrowAmountStr = x.BorrowAmount.ToString();

                        if (x.CreditorAmount > 0) x.CreditorAmountStr = x.CreditorAmount.ToString();
                        else if (x.CreditorAmount < 0) x.CreditorAmountStr = x.CreditorAmount.ToString();

                        //x.BorrowAmountStr = x.BorrowAmount > 0 ? x.BorrowAmount.ToString() :  null;
                        //x.CreditorAmountStr = x.CreditorAmount > 0 ? x.CreditorAmount.ToString() : null;
                    }
                }
                catch (Exception ex)
                {

                }


            });
            return _dtoList;
        }

        private List<CertificateBalanceDto> DataToModel(List<YssxSubject> sources)
        {
            if (null == sources) return null;
            return sources.Select(x => new CertificateBalanceDto
            {
                Id = x.Id,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount,
                EnterpriseId = x.EnterpriseId,
                ParentId = x.ParentId,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType,
            }).ToList();
        }

        private Tuple<decimal, decimal> GetCertificateData(long id, int type, List<YssxExamCertificateDataRecord> exam_certificateDataRecords, List<YssxCertificateDataRecord> certificateDataRecords)
        {
            decimal temp_cBorrowAmount = 0, temp_cCreditorAmount = 0;
            if (type == 1)
            {
                List<YssxExamCertificateDataRecord> _dataRecords = exam_certificateDataRecords.Where(x => x.SubjectId == id).ToList();
                if (_dataRecords.Count > 0)
                {
                    temp_cBorrowAmount = _dataRecords.Sum(x => x.BorrowAmount);//本期借方数据
                    temp_cCreditorAmount = _dataRecords.Sum(x => x.CreditorAmount);//本期贷方数据
                }
            }
            else
            {
                List<YssxCertificateDataRecord> temp_list = certificateDataRecords.Where(x => x.SubjectId == id).ToList();
                if (temp_list.Count > 0)
                {
                    temp_cBorrowAmount = temp_list.Sum(x => x.BorrowAmount);//本期借方数据
                    temp_cCreditorAmount = temp_list.Sum(x => x.CreditorAmount);//本期贷方数据
                }
            }

            return new Tuple<decimal, decimal>(temp_cBorrowAmount, temp_cCreditorAmount);
        }

        public ResponseContext<List<CertificateDto>> GetGeneralLedger2(long gradeId, long enterpriseId, int type = 0)
        {

            var rootItems = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete && x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            var enterprise = DbContext.FreeSql.Select<YssxCase>().Where(x => x.Id == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete).First();
            var summaries = new[] { StageBalance, StageTotal, YearStageTotal };
            var subjects = new List<CertificateDto>();
            var stageStr = enterprise.AccountingPeriodDate;//没有默认我会计期间

            var allSubsidiaryLedgerDtos = GetAllSubsidiary(gradeId, enterpriseId, type);
            var allEnterpriseSubjects = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            foreach (var rootItem in rootItems)
            {
                var rst = GetSubjectLedgerSummer(allEnterpriseSubjects, rootItem.Id, allSubsidiaryLedgerDtos, stageStr);

                foreach (var dto in rst)
                {
                    dto.SubjectType = rootItem.SubjectType;
                    dto.Stage = stageStr;
                    subjects.Add(dto);
                }

            }
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = subjects };
        }

        public List<CertificateDto> GetSubjectLedgerSummer(List<YssxSubject> allEnterpriseSubjects, long subjectId, List<CertificateDto> allSubsidiaryLedgerDtos, string stageStr)
        {
            List<CertificateDto> respones = new List<CertificateDto>();

            var rootItem = allEnterpriseSubjects.FirstOrDefault(m => m.Id == subjectId);
            List<YssxSubject> list = allEnterpriseSubjects.Where(x => x.ParentId == subjectId).ToList();

            //期初余额
            CertificateDto rootCoreSubject = new CertificateDto
            {
                BorrowAmount = rootItem.BorrowAmount,
                SubjectName = rootItem.SubjectName,
                SubjectType = rootItem.SubjectType,
                SubjectCode = rootItem.SubjectCode,
                Direction = rootItem.Direction,
                ParentSubjectId = rootItem.ParentId,
                CreditorAmount = rootItem.CreditorAmount,
            };// rootItem.MapTo<SubsidiaryLedgerDto>();
            if (list.Count == 0)
            {
                list.Add(rootItem);
            }
            DateTime rootDate = DateTime.Parse($"{stageStr}-01");
            rootCoreSubject.CertificateNo = "0";
            rootCoreSubject.SubjectId = rootItem.Id;
            rootCoreSubject.SummaryInfo = StageBalance;
            string direction = rootItem.Direction;
            rootCoreSubject.DateStr = $"{stageStr}-01";
            if (rootItem.Direction == "贷")
            {
                rootCoreSubject.Balance = list.Sum(x => x.CreditorAmount) != 0 ? list.Sum(x => x.CreditorAmount) : -list.Sum(x => x.BorrowAmount);
            }
            else
            {
                rootCoreSubject.Balance = list.Sum(x => x.CreditorAmount) != 0 ? -list.Sum(x => x.CreditorAmount) : list.Sum(x => x.BorrowAmount);
            }
            if (rootItem.CreditorAmount == 0 && rootItem.BorrowAmount == 0)
            {
                rootCoreSubject.Direction = "平";
            }
            rootCoreSubject.BorrowAmount = 0;
            rootCoreSubject.CreditorAmount = 0;
            rootCoreSubject.CreditorAmountStr = "";
            rootCoreSubject.BorrowAmountStr = "";
            var coreSubjects = new List<CertificateDto>
            {
                rootCoreSubject
            };
            List<YssxSubject> c_list = new List<YssxSubject>();
            c_list.Add(new YssxSubject { Id = subjectId });
            FindChildNode(subjectId, c_list, allEnterpriseSubjects);
            List<CertificateDto> t_list = new List<CertificateDto>();
            if (c_list.Count > 1)
            {
                foreach (var item in c_list)
                {
                    var temps = allSubsidiaryLedgerDtos.Where(o => o.ParentSubjectId == item.Id).OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();

                    foreach (var item2 in temps)
                    {
                        string c_date = item2.CertificateDate.ToString("yyyy-MM-dd");
                        if (c_date != "0001-01-01")
                        {
                            item2.DateStr = c_date;
                        }
                        item2.CreditorAmountStr = item2.CreditorAmount.ToString();
                        item2.BorrowAmountStr = item2.BorrowAmount.ToString();
                        coreSubjects.Add(item2);
                        var tuple = GetSubjectBalance(coreSubjects);
                        item2.Direction = tuple.Item2 == 0 ? "平" : item2.Direction;
                        item2.Balance = tuple.Item2;
                        t_list.Add(item2);
                    }
                }
            }
            else
            {
                var temps = allSubsidiaryLedgerDtos.Where(o => o.SubjectId == subjectId).OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();

                foreach (var item2 in temps)
                {
                    string c_date = item2.CertificateDate.ToString("yyyy-MM-dd");
                    if (c_date != "0001-01-01")
                    {
                        item2.DateStr = c_date;
                    }
                    item2.CreditorAmountStr = item2.CreditorAmount.ToString();
                    item2.BorrowAmountStr = item2.BorrowAmount.ToString();
                    coreSubjects.Add(item2);
                    var tuple = GetSubjectBalance(coreSubjects);
                    item2.Direction = tuple.Item2 == 0 ? "平" : item2.Direction;
                    item2.Balance = tuple.Item2;
                    t_list.Add(item2);
                }
            }
            string dirction = "";
            decimal _balance = 0;
            if (coreSubjects.Count == 1)
            {
                dirction = coreSubjects[0].Direction;
                _balance = coreSubjects[0].Balance;
            }
            else
            {
                int len = coreSubjects.Count;
                var balance = coreSubjects[len - 1];
                dirction = balance.Direction;
                _balance = balance.Balance;
            }

            coreSubjects = coreSubjects.OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();
            decimal c = t_list.Sum(x => x.CreditorAmount);
            decimal b = t_list.Sum(x => x.BorrowAmount);
            var currentIssue = new CertificateDto  //本期合计
            {
                DateStr = rootDate.GetMonthLastDay().ToString("yyyy-MM-dd"),
                SubjectCode = rootItem.SubjectCode,
                CertificateNo = "",
                SubjectName = rootCoreSubject.SubjectName,
                SummaryInfo = StageTotal,
                Direction = dirction,
                Balance = _balance,
                BorrowAmount = b,
                CreditorAmount = c,
                CertificateDate = rootDate,
                CreditorAmountStr = c.ToString(),
                BorrowAmountStr = b.ToString(),
            };
            coreSubjects.Add(currentIssue);

            var currentYear = new CertificateDto //本年累计
            {
                DateStr = rootDate.GetMonthLastDay().ToString("yyyy-MM-dd"),
                SubjectCode = rootItem.SubjectCode,
                SubjectName = rootCoreSubject.SubjectName,
                SummaryInfo = YearStageTotal,
                CertificateNo = "",
                Direction = dirction,
                Balance = _balance,
                BorrowAmount = b,
                CreditorAmount = c,
                CertificateDate = rootDate,
                CreditorAmountStr = c.ToString(),
                BorrowAmountStr = b.ToString(),
                //CreditorAmount = balance.Item4
            };
            coreSubjects.Add(currentYear);
            coreSubjects.FirstOrDefault(x => x.CertificateNo == "0").CertificateNo = "";
            respones = coreSubjects;
            return respones;
        }

        /// <summary>
        /// 获取所有期数数据
        /// </summary>
        /// <returns></returns>
        private List<CertificateDto> GetAllSubsidiary(long gradeId, long enterpriseId, int type)
        {
            string sql = string.Empty;
            if (type == 1)
            {
                sql = DbContext.FreeSql.Select<YssxExamCertificateDataRecord>().LeftJoin<YssxSubject>((a, b) => a.SubjectId == b.Id && a.GradeId == gradeId
                ).Where(a => a.CaseId == enterpriseId && a.GradeId == gradeId).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,b.Id SubjectId,b.SubjectCode,b.SubjectName,b.Direction,b.ParentId ParentSubjectId");
            }
            else
            {
                sql = DbContext.FreeSql.Select<YssxCertificateDataRecord>().LeftJoin<YssxSubject>((a, b) => a.SubjectId == b.Id).Where(x => x.EnterpriseId == enterpriseId).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,b.Id SubjectId,b.SubjectCode,b.SubjectName,b.Direction,b.ParentId ParentSubjectId");
            }

            var allSubsidiaryLedgerDtos = DbContext.FreeSql.Ado.Query<CertificateDto>(sql).OrderBy(x => x.CertificateNo).ToList();
            allSubsidiaryLedgerDtos.ForEach(x =>
            {
                if (string.IsNullOrWhiteSpace(x.CertificateNo))
                {
                    x.CertificateNo = "0";
                }
            });
            return allSubsidiaryLedgerDtos;
        }

        private Tuple<string, decimal, decimal, decimal> GetSubjectBalance(List<CertificateDto> coreSubjects)
        {
            var sumBorrowAmount = 0m;
            var sumCreditorAmount = 0m;
            int length = coreSubjects.Count;
            CertificateDto tempData = coreSubjects[0];
            decimal blance = 0;
            int index = 0;
            coreSubjects = coreSubjects.OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();
            foreach (var item in coreSubjects)
            {
                sumBorrowAmount += item.BorrowAmount;
                sumCreditorAmount += item.CreditorAmount;

                if (index == 0) { index++; continue; }

                if (tempData.Direction == "借" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "借" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "平" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "平" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;
                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "平")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;
                    }
                }
                else if (tempData.Direction == "借" && item.Direction == "平")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;
                    }
                }

                item.Balance = blance;
                tempData = item;

            }

            return new Tuple<string, decimal, decimal, decimal>(blance > 0 ? "借" : "贷", blance, sumBorrowAmount, sumCreditorAmount);
        }


        private async Task<List<YssxSubject>> GetSearchSubjects(long id, int subjectType, string keyword)
        {
            List<YssxSubject> yssxCoreSubjects = new List<YssxSubject>();
            if (subjectType == 0 && string.IsNullOrWhiteSpace(keyword)) yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id).ToListAsync($"{CommonConstants.Cache_GetCertificateListByCaseId}{id}", true, 10 * 60);
            else
            {
                if (string.IsNullOrWhiteSpace(keyword))
                {
                    yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id && x.SubjectType == subjectType).ToListAsync();
                }
                else
                {
                    long code = 0;
                    if (long.TryParse(keyword, out code))
                    {
                        yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id && x.SubjectCode == code).ToListAsync();
                    }
                    else
                    {
                        yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id && x.SubjectName.Contains(keyword)).ToListAsync();
                    }
                }
            }

            return yssxCoreSubjects;
        }
        #endregion
    }
}
