﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class AppManageService : IAppManageService
    {
        public async Task<ResponseContext<AppNewVersionInfoDto>> NewVersionInfo(AppIdEnum appid, int type)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxAppManage>()
                .Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.IsNew == true && s.AppId == appid && s.Type == type)
                .FirstAsync<AppNewVersionInfoDto>();

            return new ResponseContext<AppNewVersionInfoDto> { Code = CommonConstants.SuccessCode, Data = data, };
        }
    }
}
