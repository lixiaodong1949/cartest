﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Repository.Extensions;
using Tas.Common.IdGenerate;
using System.Linq;
using Yssx.S.Poco;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.Exam;

namespace Yssx.S.ServiceImpl
{
    public class CourseTestOnLineTaskService : ICourseTestOnLineTaskService
    {
        public CourseTestOnLineTaskService()
        {

        }

        public async Task<ResponseContext<AddTestTaskDto>> AddTestTask(TaskDto model, long currentUserId)
        {
            bool isExist = await DbContext.FreeSql.GetRepository<YssxTask>().Where(o => o.Status == LessonsTaskStatus.Started && o.CourseId == model.CourseId && o.CreateBy == currentUserId && o.IsDelete == CommonConstants.IsNotDelete).AnyAsync();
            if (!isExist)
            {
                DateTime date = DateTime.Now;
                long gid = IdWorker.NextId();

                List<YssxTask> tasks = model.ClassIds.Select(x => new YssxTask
                {
                    Id = IdWorker.NextId(),
                    TaskType = TaskType.ClassTest,
                    Name = $"{model.Name}",
                    StartTime = date,
                    CourseId = model.CourseId,
                    Status = LessonsTaskStatus.Started,
                    TenantId = x,
                    GroupId = gid,
                    OriginalCourseId = model.OriginalCourseId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,


                }).ToList();

                //创建任务试题
                List<YssxTaskTopic> TaskTopicList = model.TopicList.Select(o => new YssxTaskTopic
                {
                    Id = IdWorker.NextId(),
                    TaskId = gid,
                    TopicId = o.TopicId,
                    QuestionType = o.QuestionType,
                    Score = o.Score,
                    Sort = o.Sort,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = date
                }).ToList();

                //创建试卷
                ExamPaperCourse paperEntity = new ExamPaperCourse();
                paperEntity.Id = IdWorker.NextId();
                paperEntity.Name = $"{model.Name}";
                paperEntity.TaskId = gid;
                paperEntity.CourseId = model.CourseId;
                paperEntity.OriginalCourseId = model.OriginalCourseId;
                paperEntity.ExamType = CourseExamType.ClassTest;
                paperEntity.CanShowAnswerBeforeEnd = true;
                paperEntity.TotalScore = TaskTopicList.Sum(m => m.Score);
                paperEntity.TotalQuestionCount = TaskTopicList.Count;
                paperEntity.BeginTime = date;
                paperEntity.ReleaseTime = date;
                paperEntity.IsRelease = true;
                paperEntity.Status = ExamStatus.Started;
                paperEntity.CreateBy = currentUserId;
                paperEntity.UpdateTime = date;

                await DbContext.FreeSql.GetRepository<YssxTask>().InsertAsync(tasks);
                await DbContext.FreeSql.GetRepository<YssxTaskTopic>().InsertAsync(TaskTopicList);
                await DbContext.FreeSql.GetRepository<ExamPaperCourse>().InsertAsync(paperEntity);

                return new ResponseContext<AddTestTaskDto> { Data = new AddTestTaskDto { TaskId = gid, ExamId = paperEntity.Id } };

            }

            return new ResponseContext<AddTestTaskDto> { Code = CommonConstants.ErrorCode, Msg = "有任务正在进行中！" };
        }

        public async Task<ResponseContext<List<AnalyzeQuestionDetailsStudentsDto>>> AnalyzeQuestionStudents(long taskId, long examId)
        {
            ResponseContext<List<AnalyzeQuestionDetailsStudentsDto>> response = new ResponseContext<List<AnalyzeQuestionDetailsStudentsDto>>();
            List<AnalyzeQuestionDetailsStudentsDto> dtos = new List<AnalyzeQuestionDetailsStudentsDto>();
            List<YssxTask> tasks = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.GroupId == taskId).ToListAsync($"{CommonConstants.Cache_GetOnlineTaskByTaskId}{taskId}", true, 10 * 60);
            List<long> classIds = tasks.Select(x => x.TenantId).ToList();
            List<YssxStudent> studentList = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(o => classIds.Contains(o.ClassId) && o.IsDelete == CommonConstants.IsNotDelete).ToListAsync($"{CommonConstants.Cache_GetTaskStudentsByTaskId}{taskId}", true, 10 * 60);
            List<ExamCourseUserGradeDetail> gradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(x => x.ExamId == examId).ToListAsync($"{CommonConstants.Cache_GetExamCourseGradeDetailById}{examId}", true, 10 * 60);

            if (gradeDetails.Count > 0)
            {
                var s = gradeDetails.GroupBy(x => x.QuestionId, (a, b) =>
                 {
                     List<long> uids = new List<long>();
                     long questionId = a;
                     foreach (var item in b)
                     {
                         uids.Add(item.UserId);
                     }
                     return new TempData { QuestionId = questionId, UserList = uids };
                 });

                foreach (var item in gradeDetails)
                {
                    AnalyzeQuestionDetailsStudentsDto dto = new AnalyzeQuestionDetailsStudentsDto();
                    dto.QuestionId = item.QuestionId;
                    TempData temps = s.Where(a => a.QuestionId == item.QuestionId).FirstOrDefault();
                    if (temps != null)
                    {
                        dto.Students = studentList.Join(temps.UserList, p => p.UserId, c => c, (f, g) => f.Name).ToList();
                    }

                    dtos.Add(dto);
                }
            }

            response.Data = dtos;

            return response;
        }

        public async Task<ResponseContext<AnalyzeTaskResultDto>> AnalyzeTaskResult(long taskId, long examId)
        {
            ResponseContext<AnalyzeTaskResultDto> response = new ResponseContext<AnalyzeTaskResultDto>();
            AnalyzeTaskResultDto dto = new AnalyzeTaskResultDto();
            List<ExamCourseUserGrade> userGrades = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.ExamId == examId).ToListAsync($"{CommonConstants.Cache_GetExamCourseUserGradeById}{examId}", true, 10 * 60);
            List<ExamCourseUserGradeDetail> gradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(x => x.ExamId == examId).ToListAsync($"{CommonConstants.Cache_GetExamCourseGradeDetailById}{examId}", true, 10 * 60);
            dto.CommitCount = userGrades.Where(x => x.Status == StudentExamStatus.End).Count();
            List<AnalyzeQuestionDetailsDto> analyzeQuestions = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetExamCourseTaskQuestionById}{examId}", () => DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select.From<YssxTopicPublic>((a, b) => a.InnerJoin(x => x.TopicId == b.Id)).Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.TaskId == taskId).ToList((a, b) => new AnalyzeQuestionDetailsDto
            {
                QuestionId = b.Id,
                Title = b.Title,
                Sort = b.Sort,
                ParentQuestionId = b.ParentId
            }), 10 * 60, true, false);

            analyzeQuestions.ForEach(x =>
            {
                x.CommitCount = gradeDetails.Where(o => o.QuestionId == x.QuestionId).Count();
            });
            dto.QuestionDetails = analyzeQuestions;
            response.Data = dto;
            return response;
        }

        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long taskId, long examId)
        {
            return await Task.Run(() =>
            {
                ExamPaperCourseBasicInfo info = new ExamPaperCourseBasicInfo { ExamId = examId };
                List<AnalyzeQuestionDetailsDto> analyzeQuestions = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetExamCourseTaskQuestionById}{examId}", () => DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select.From<YssxTopicPublic>((a, b) => a.InnerJoin(x => x.TopicId == b.Id)).Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.TaskId == taskId).ToList((a, b) => new AnalyzeQuestionDetailsDto
                {
                    QuestionId = b.Id,
                    Sort = b.Sort,
                    Title = b.Title,
                    ParentQuestionId = b.ParentId
                }), 10 * 60, true, false);


                info.QuestionInfoList = analyzeQuestions.Select(b => new CourseQuestionInfo
                {
                    QuestionId = b.QuestionId,
                    Sort = b.Sort,
                    Title = b.Title,
                    ParentQuestionId = b.ParentQuestionId
                }).ToList();


                return new ResponseContext<ExamPaperCourseBasicInfo> { Code = CommonConstants.SuccessCode, Data = info, Msg = "成功" };
            });
        }

        public async Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskList(UserTicket user, int pageIndex, int pageSize, string courseName)
        {
            PageResponse<MyCourseTaskDto> result = new PageResponse<MyCourseTaskDto>();
            long classId = 0;

            List<MyCourseTaskDto> courseTasks = new List<MyCourseTaskDto>();

            classId = DbContext.FreeSql.Select<YssxStudent>().Where(o => o.UserId == user.Id).First(o => o.ClassId);
            long recordCount = 0;
            if (string.IsNullOrWhiteSpace(courseName))
            {
                var select = DbContext.FreeSql.GetRepository<YssxCourse>().Select.From<YssxTask, ExamPaperCourse, ExamCourseUserGrade>((a, b, c, d) => a.InnerJoin(o => o.Id == b.CourseId).InnerJoin(o => b.GroupId == c.TaskId).LeftJoin(o => c.Id == d.ExamId && d.UserId == user.Id)).Where((a, b, c, d) => b.TenantId == classId && b.Status == LessonsTaskStatus.Started && a.IsDelete == CommonConstants.IsNotDelete);
                recordCount = await select.CountAsync();
                courseTasks = select.OrderByDescending((a, b, c, d) => a.Id).Page(pageIndex, pageSize).ToList((a, b, c, d) => new MyCourseTaskDto
                {
                    TaskID = b.GroupId,
                    TaskStatus = b.Status,
                    CourseTitle = a.CourseTitle,
                    Images = a.Images,
                    ExamId = c.Id,
                    CourseID = a.Id,
                    OriginalCourseId = b.OriginalCourseId,
                    Author = a.Author,
                    CourseName = a.CourseName,
                    PublishingDate = a.PublishingDate,
                    PublishingHouse = a.PublishingHouse,
                    CreateByName = a.CreateByName,
                    UpdateTime = a.UpdateTime,
                    GradeId = d.Id,
                });


            }
            else
            {
                var select = DbContext.FreeSql.GetRepository<YssxCourse>().Select.From<YssxTask, ExamPaperCourse, ExamCourseUserGrade>((a, b, c, d) => a.InnerJoin(o => o.Id == b.CourseId).InnerJoin(o => b.GroupId == c.TaskId).LeftJoin(o => c.Id == d.ExamId && d.UserId == user.Id)).Where((a, b, c, d) => b.TenantId == classId &&b.Status== LessonsTaskStatus.Started && a.IsDelete == CommonConstants.IsNotDelete && a.CourseName.Contains(courseName));
                recordCount = await select.CountAsync();
                courseTasks = select.OrderByDescending((a, b, c, d) => a.Id).Page(pageIndex, pageSize).ToList((a, b, c, d) => new MyCourseTaskDto
                {
                    TaskID = b.GroupId,
                    TaskStatus = b.Status,
                    CourseTitle = a.CourseTitle,
                    Images = a.Images,
                    ExamId = c.Id,
                    CourseID = a.Id,
                    OriginalCourseId = b.OriginalCourseId,
                    Author = a.Author,
                    CourseName = a.CourseName,
                    PublishingDate = a.PublishingDate,
                    PublishingHouse = a.PublishingHouse,
                    CreateByName = a.CreateByName,
                    UpdateTime = a.UpdateTime,
                    GradeId = d.Id,
                });
            }
            List<long> cids = courseTasks.Select(m => m.CourseID).ToList();
            var Summary = await DbContext.FreeSql.Select<YssxPrepareSectionSummary>().Where(o => cids.Contains(o.CourseId)).ToListAsync();


            foreach (var item in courseTasks)
            {
                item.Summarys = Summary.Where(x => x.CourseId == item.CourseID).Select(o => new CourseSummary
                {
                    Id = o.Id,
                    Count = o.Count,
                    SummaryType = o.SummaryType,

                }).ToList();
            }


            result.Data = courseTasks;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = pageSize;
            result.PageIndex = pageIndex;
            result.RecordCount = recordCount;
            return result;
        }

        public async Task<ResponseContext<bool>> StopTestTask(long taskId)
        {
            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                bool ExamPaperState = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.Set(x => x.Status == ExamStatus.End).Where(x => x.TaskId == taskId).ExecuteAffrowsAsync() > 0;
                bool TaskState = await DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy.Set(x => x.Status == LessonsTaskStatus.End).Where(x => x.GroupId == taskId).ExecuteAffrowsAsync() > 0;
                if (ExamPaperState && TaskState)
                {
                    uow.Commit();
                    state = true;
                }
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "OK" : "收题失败！" };
        }

        public async Task<ResponseContext<TestTaskDetailsDto>> TestTaskDetails(long taskId, long examId)
        {
            ResponseContext<TestTaskDetailsDto> response = new ResponseContext<TestTaskDetailsDto>();

            long peopleCount = 0, commitCount = 0, doingCount = 0;

            List<YssxTask> tasks = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.GroupId == taskId).ToListAsync($"{CommonConstants.Cache_GetOnlineTaskByTaskId}{taskId}", true, 10 * 60);
            List<long> classIds = tasks.Select(x => x.TenantId).ToList();
            List<YssxStudent> studentList = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(o => classIds.Contains(o.ClassId) && o.IsDelete == CommonConstants.IsNotDelete).ToListAsync($"{CommonConstants.Cache_GetTaskStudentsByTaskId}{taskId}", true, 10 * 60);

            peopleCount = studentList.Count();

            List<string> names = studentList.Select(x => x.Name).ToList();

            var counts = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.ExamId == examId).ToList(x => new
            {
                Id = x.Id,
                Status = x.Status
            });
            commitCount = counts.Where(x => x.Status == StudentExamStatus.End).Count();
            doingCount = counts.Where(x => x.Status == StudentExamStatus.Started).Count();
            long totalTime = (DateTime.Now - tasks.First().CreateTime).Milliseconds;
            TestTaskDetailsDto dto = new TestTaskDetailsDto { PeopleCount = peopleCount, CommitCount = commitCount, Date = tasks.First().CreateTime.ToString("yyyy-MM-dd"), DoingCount = doingCount, Students = names, TotalTime = totalTime };
            response.Data = dto;
            return response;
        }
    }

    public class TempData
    {
        public long QuestionId { get; set; }
        public List<long> UserList { get; set; }
    }


}
