using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;

namespace Yssx.S.Service
{
    /// <summary>
    /// 签到服务
    /// </summary>
    public class SigninService : ISigninService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> SaveSignin(long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            var issign = await IsSignin(userId);
            if (issign.Data)
            {
                return result;
            }

            long number = 1;
            YssxIntegralDetails integralDetails = null;

            integralDetails = new YssxIntegralDetails
            {
                Id = IdWorker.NextId(),
                UserId = userId,
                CreateTime = DateTime.Now,
                Desc = "签到获得奖励积分",
                FromType = 9,
                Number = number,
                StatusType = 1,
                Date = DateTime.Now.ToString("yyyy-MM-dd")
            };

            #region 保存到数据库中

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元    
            {
                try
                {
                    var repo_i = uow.GetRepository<YssxIntegral>();
                    var repo_id = uow.GetRepository<YssxIntegralDetails>();

                    YssxIntegral integral = repo_i.Where(x => x.UserId == userId).First();
                    if (integral == null)
                    {
                        integralDetails.IntegralSum = number;
                        repo_i.Insert(new YssxIntegral { Id = IdWorker.NextId(), UserId = userId, Number = number, UpdateTime = DateTime.Now });
                    }
                    else
                    {
                        integralDetails.IntegralSum = integral.Number + number;
                        repo_i.UpdateDiy.Set(x => new YssxIntegral { Number = x.Number + number }).Where(x => x.Id == integral.Id).ExecuteAffrows();
                    }

                    repo_id.Insert(integralDetails);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }

            #endregion

            //result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 是否签到
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> IsSignin(long userId)
        {
            //获取详情
            var entity = await DbContext.FreeSql.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.UserId == userId && e.FromType == 9 && e.Date == DateTime.Now.ToString("yyyy-MM-dd"))
                      .AnyAsync();

            return new ResponseContext<bool>(entity);
        }


        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SigninDto>> GetSignin(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxSigninDetails>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<SigninDto>();

            return new ResponseContext<SigninDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> DeleteSignin(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxSigninDetails>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new YssxSigninDetails
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<YssxSignin>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new YssxSignin
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<SigninPageResponseDto>> GetSigninListPage(SigninPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxSigninDetails>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   //.WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<SigninPageResponseDto>();

            return new PageResponse<SigninPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<SigninListResponseDto>> GetSigninList(SigninListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<YssxSigninDetails>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   //.WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<SigninListResponseDto>();

            return new ListResponse<SigninListResponseDto>(entitys);
        }


    }
}