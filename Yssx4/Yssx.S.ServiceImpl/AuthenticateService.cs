﻿using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Repository.Extensions;
using System.Text.RegularExpressions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 认证
    /// </summary>
    public class AuthenticateService : IAuthenticateService
    {
        PermissionRequirement _requirement;
        public AuthenticateService(PermissionRequirement requirement)
        {
            _requirement = requirement;
        }

        /// <summary>
        /// 账号密码登陆
        /// </summary>
        /// <param name="dto">用户信息</param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UserAuthentication(UserAuthentication dto, string loginIp, PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var md5 = dto.Password.Md5();
                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.MobilePhone == dto.UserName).First($"{CommonConstants.Cache_GetUserByMobile}{dto.UserName}", true, 30 * 60, true, false);

                if (user == null) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该手机号没有注册!" };
                if (user.Status == 0) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户删除!" };
                if (user.Password != md5) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不正确!" };

                string isVip = "false";

                if (user.UserType == 1)
                {
                    YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (student != null)
                    {
                        bool state = student.ExpiredDate > DateTime.Now;
                        if (state)
                            isVip = "true";
                    }
                }
                if (user.UserType == 2)
                {
                    isVip = "true";
                }
                
                var school = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == user.TenantId).First($"{CommonConstants.Cache_GetSchoolById}{ user.TenantId}", true, 60 * 60, true, true);
                var ticket = user == null ? null : new UserTicket()
                {
                    Photo = user.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                    RealName = user.RealName,
                    ClientId = user.ClientId,
                    Id = user.Id,
                    TenantId = user.TenantId,
                    Type = (int)user.UserType,
                    MobilePhone = user.MobilePhone,
                    SchoolName = school == null ? "" : school.Name,
                    SchoolType = school == null ? -1 : (int)school.Type,
                    LoginType = dto.LoginType,
                    IsVip = isVip
                };

                //var requirement = ServiceLocator.GetService<PermissionRequirement>();
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                DbContext.FreeSql.Insert<YssxUserLoginLog>(log).ExecuteAffrows();
                if (dto.LoginType == 1)
                {
                    var state = DbContext.FreeSql.Select<YssxUserLoginLog>().Where(x => x.UserId == user.Id).Count() > 1;
                    ticket.FirstLanding = state;
                }
                //RedisHelper.Set(string.Format(CacheKeys.UserLoginTokenHashKey, ticket.Id), ticket.AccessToken, 24 * 60 * 60);
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                RedisHelper.Publish("login", user.Id.ToString());
                return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
            });
        }

        /// <summary>
        /// 手机号码登陆
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App 2.小程序</param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> NumberUserPhoneAuthentication(string mb, string verificationCode, string loginIp, PermissionRequirement _requirement, int loginType)
        {
            var code = await DbContext.FreeSql.Select<YssxVerifyCode>().Where(m => m.MobilePhone == mb).OrderByDescending(x => x.CreateTime).FirstAsync();
            if (code == null)
            {
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码，请确认手机号码是否正确!" };
            }

            if (code.Code != verificationCode)
            {
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码错误!" };
            }

            if (code.Status == 1)
            {
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码已验证使用过!" };
            }

            if (code.ExpirTime < DateTime.Now)
            {
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码已过期!" };
            }
            var firstLanding = true;
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.MobilePhone == mb).FirstAsync();
            if (user == null)
            {
                firstLanding = false;
                user = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    UserName = mb,
                    Password = CommonConstants.NewUserDefaultPwd.Md5(),
                    UserType = (int)UserTypeEnums.OrdinaryUser,
                    IsDelete = 0,
                    CreateBy = 0,
                    UpdateBy = 0,
                    Email = "11",
                    MobilePhone = mb,
                    NikeName = mb,
                    Photo = "",
                    QQ = "",
                    RealName = "",
                    Sex = "",
                    Status = (int)Status.Enable,
                    TenantId = CommonConstants.DefaultTenantId,
                    WeChat = "",
                    IdNumber = "",
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                    ClientId = Guid.NewGuid(),
                    RegistrationType = 1
                };
                DbContext.FreeSql.Insert(user).ExecuteAffrows();
            }
            else
            {
                //if (user == null) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该电话号码没有注册!" };
                if (user.Status == 0) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户删除!" };
            }

            string isVip = "false";

            if (user.UserType == 1)
            {
                YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (student != null)
                {
                    bool state = student.ExpiredDate > DateTime.Now;
                    if (state)
                        isVip = "true";
                }
            }
            if (user.UserType == 2)
            {
                isVip = "true";
            }

            DbContext.FreeSql.Update<YssxVerifyCode>(code).Set(x => x.Status == 1).ExecuteAffrows();
            var school = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == user.TenantId).First($"{CommonConstants.Cache_GetSchoolById}{ user.TenantId}", true, 60 * 60, true, true);
            var ticket = new UserTicket()
            {
                Photo = user.Photo,
                Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                RealName = user.RealName,
                ClientId = user.ClientId,
                Id = user.Id,
                TenantId = user.TenantId,
                Type = (int)user.UserType,
                MobilePhone = user.MobilePhone,
                SchoolName = school == null ? "" : school.Name,
                LoginType = loginType,
                SchoolType = school == null ? -1 : (int)school.Type,
                FirstLanding = firstLanding,
                IsVip = isVip,
            };
            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
            var log = new YssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
            await DbContext.FreeSql.Insert(log).ExecuteAffrowsAsync();
            if (loginType == 1)//app首次登陆，app为什么要判断首次登陆？
            {
                var state = await DbContext.FreeSql.Select<YssxUserLoginLog>().Where(x => x.UserId == user.Id).CountAsync() > 1;
                ticket.FirstLanding = state;
            }
            //RedisHelper.Set(string.Format(CacheKeys.UserLoginTokenHashKey, ticket.Id), ticket.AccessToken, 24 * 60 * 60);
            RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
            await RedisHelper.PublishAsync("login", ticket.Id.ToString());
            return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
        }

        /// <summary>
        /// 微信登陆
        /// </summary>
        /// <param name="wx">微信openid</param>
        /// <param name="unionid">微信unionid</param>
        /// <param name="nickname">微信昵称</param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        public async Task<ResponseContext<WeChartTicket>> WeChartAuthentication(string wx, string unionid, string nickname, string loginIp, PermissionRequirement _requirement, int loginType)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                if (string.IsNullOrEmpty(wx) || string.IsNullOrEmpty(unionid))
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权" };

                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.Unionid == unionid && u.IsDelete == CommonConstants.IsNotDelete).First();
                if (user == null)
                {
                    //unionid为空则取openid
                    user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.WeChat == wx && u.IsDelete == CommonConstants.IsNotDelete).First();
                }
                if (user != null && user.Status == (int)Status.Disable)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };

                var firstLanding = true;
                if (user == null)
                {
                    firstLanding = false;
                    //不存在则自动注册
                    var yssxUser = new YssxUser
                    {
                        Id = IdWorker.NextId(),
                        UserName = "",
                        Password = CommonConstants.NewUserDefaultPwd.Md5(),
                        UserType = (int)UserTypeEnums.OrdinaryUser,
                        ClientId = Guid.NewGuid(),
                        Email = "",
                        MobilePhone = "",
                        NikeName = nickname,
                        Photo = "",
                        QQ = "",
                        RealName = "",
                        Sex = "",
                        Status = (int)Status.Enable,
                        WeChat = "",
                        Unionid = unionid,
                        IdNumber = "",
                        TenantId = CommonConstants.DefaultTenantId,
                        CreateBy = -1,
                        RegistrationType = 1
                    };
                    var newUser = DbContext.FreeSql.GetRepository<YssxUser>().Insert(yssxUser);
                    if (newUser == null)
                        return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权" };
                    user = newUser;
                }
                else
                {
                    if (string.IsNullOrEmpty(user.Unionid))
                    {
                        user.Unionid = unionid;
                        user.NikeName = string.IsNullOrEmpty(user.NikeName) ? nickname : user.NikeName;
                        user.UpdateBy = -1;
                        user.UpdateTime = dtNow;
                        var state = DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.Unionid, m.NikeName, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                        if (!state)
                            return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权" };
                    }
                }

                string isVip = "false";

                if (user.UserType == 1)
                {
                    YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (student != null)
                    {
                        bool state = student.ExpiredDate > DateTime.Now;
                        if (state)
                            isVip = "true";
                    }
                }
                if (user.UserType == 2)
                {
                    isVip = "true";
                }

                var school = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == user.TenantId).First($"{CommonConstants.Cache_GetSchoolById}{ user.TenantId}", true, 60 * 60, true, true);
                var ticket = new UserTicket()
                {
                    Photo = user.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                    RealName = user.RealName,
                    ClientId = user.ClientId,
                    Id = user.Id,
                    TenantId = user.TenantId,
                    Type = (int)user.UserType,
                    SchoolName = school == null ? "" : school.Name,
                    SchoolType = school == null ? -1 : (int)school.Type,
                    LoginType = loginType,
                    MobilePhone = string.IsNullOrEmpty(user.MobilePhone) ? user.Unionid : user.MobilePhone,
                    FirstLanding = firstLanding,
                    IsVip = isVip,
                };
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = dtNow,
                    UserId = ticket.Id
                };
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                DbContext.FreeSql.Insert(log).ExecuteAffrows();

                var wxTicket = ticket.MapTo<WeChartTicket>();
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.SuccessCode, Data = wxTicket };
            });
        }

        /// <summary>
        /// 小程序登陆
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<WeChartTicket>> WeChartAppletsAuthentication(WeChartAppletsDto model, string loginIp, PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                if (string.IsNullOrEmpty(model.Openid) || string.IsNullOrEmpty(model.Unionid))
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权" };
                if (string.IsNullOrEmpty(model.MobilePhone))
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权获取手机号" };

                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.MobilePhone == model.MobilePhone && u.IsDelete == CommonConstants.IsNotDelete).First();
                if (user == null)
                {
                    //取unionid用户
                    user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.Unionid == model.Unionid && u.IsDelete == CommonConstants.IsNotDelete).First();
                }
                if (user != null && user.Status == (int)Status.Disable)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };
                if (user == null)
                {
                    //不存在则自动注册
                    var yssxUser = new YssxUser
                    {
                        Id = IdWorker.NextId(),
                        UserName = "",
                        Password = CommonConstants.NewUserDefaultPwd.Md5(),
                        UserType = (int)UserTypeEnums.OrdinaryUser,
                        ClientId = Guid.NewGuid(),
                        Email = "",
                        MobilePhone = model.MobilePhone,
                        NikeName = model.NickName,
                        Photo = "",
                        QQ = "",
                        RealName = "",
                        Sex = "",
                        Status = (int)Status.Enable,
                        WeChat = "",
                        Unionid = model.Unionid,
                        IdNumber = "",
                        TenantId = CommonConstants.DefaultTenantId,
                        CreateBy = -1,
                        RegistrationType = 1
                    };
                    var newUser = DbContext.FreeSql.GetRepository<YssxUser>().Insert(yssxUser);
                    if (newUser == null)
                        return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权!" };
                    user = newUser;
                }
                else
                {
                    if (user.Status == (int)Status.Disable)
                        return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，用户禁用!" };
                    if (string.IsNullOrEmpty(user.Unionid))
                    {
                        user.Unionid = model.Unionid;
                        user.NikeName = string.IsNullOrEmpty(user.NikeName) ? model.NickName : user.NikeName;
                        user.UpdateBy = -1;
                        user.UpdateTime = dtNow;
                        if (string.IsNullOrEmpty(user.MobilePhone))
                        {
                            if (!DbContext.FreeSql.Select<YssxUser>().Any(u => u.MobilePhone == model.MobilePhone && u.IsDelete == CommonConstants.IsNotDelete))
                                user.MobilePhone = model.MobilePhone;
                        }
                        var state = DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.Unionid, m.NikeName, m.MobilePhone, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                        if (!state)
                            return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权!" };
                    }
                }

                string isVip = "false";

                if (user.UserType == 1)
                {
                    YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (student != null)
                    {
                        bool state = student.ExpiredDate > DateTime.Now;
                        if (state)
                            isVip = "true";
                    }
                }
                if (user.UserType == 2)
                {
                    isVip = "true";
                }

                var school = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == user.TenantId).First($"{CommonConstants.Cache_GetSchoolById}{ user.TenantId}", true, 60 * 60, true, true);
                var ticket = new UserTicket()
                {
                    Photo = user.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                    RealName = user.RealName,
                    ClientId = user.ClientId,
                    Id = user.Id,
                    TenantId = user.TenantId,
                    Type = (int)user.UserType,
                    SchoolName = school == null ? "" : school.Name,
                    SchoolType = school == null ? -1 : (int)school.Type,
                    LoginType = 2,
                    MobilePhone = user.MobilePhone,
                    IsVip = isVip,
                };
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = dtNow,
                    UserId = ticket.Id
                };
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                DbContext.FreeSql.Insert(log).ExecuteAffrows();

                var wxTicket = ticket.MapTo<WeChartTicket>();
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.SuccessCode, Data = wxTicket };
            });
        }


        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="ticket"></param>
        public static void LoginOut(UserTicket ticket)
        {
            //DbContext.FreeSql.GetRepository<YssxUserLoginLog, long>().UpdateDiy.Set(a => new YssxUserLoginLog { Status = (int)Status.Disable }).Where(m => m.AccessToken == ticket.AccessToken).ExecuteAffrows();
            //RedisHelper.Del(string.Format(CacheKeys.UserLoginToken, ticket.Id));
            RedisHelper.HDel(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}");
        }

        /// <summary>
        /// 工学号登陆获取学校
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<DropdownListDto>>> DropdownList()
        {
            return await Task.Run(() =>
            {
                List<DropdownListDto> list = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Status == 1).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                return new ResponseContext<List<DropdownListDto>> { Code = CommonConstants.SuccessCode, Msg = "获取成功!", Data = list };
            });
        }

        /// <summary>
        /// 找回密码
        /// </summary>
        /// <param name="mb">电话</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次密码</param>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RetrievePassword(string mb, string verificationCode, string password, string againPassword)
        {
            return await Task.Run(() =>
            {
                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.MobilePhone == mb && u.IsDelete == CommonConstants.IsNotDelete && u.Status == (int)Status.Enable).First();
                var code = DbContext.FreeSql.Select<YssxVerifyCode>().Where(m => m.MobilePhone == mb).OrderByDescending(x => x.CreateTime).First();

                if (user == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到手机号码，请确认手机号码是否正确!", Data = false };

                if (code == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码，请确认手机号码是否正确!", Data = false };

                if (code.Code != verificationCode)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码错误!", Data = false };

                if (DateTime.Now > code.ExpirTime)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码过期!", Data = false };

                if (!user.MobilePhone.Equals(mb))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "输入的手机号与绑定的手机号不一致!", Data = false };

                if (password != againPassword)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "密码不一致!", Data = false };

                bool state = DbContext.FreeSql.GetRepository<YssxUser>()
                    .UpdateDiy.Set(x => new YssxUser { Password = password.Md5(), UpdateTime = DateTime.Now })
                    .Where(x => x.Id == user.Id).ExecuteAffrows() > 0;

                //删除验证码
                RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{mb}");

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
            });
        }


        /// <summary>
        ///  设置密码
        /// </summary>
        /// <param name="verificationCode">验证码</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetPassword(string password, UserTicket user, string verificationCode = null)
        {
            var result = new ResponseContext<bool>();
            var userInfo = DbContext.FreeSql.Select<YssxUser>()
                .Where(u => u.Id == user.Id && u.IsDelete == CommonConstants.IsNotDelete && u.Status == (int)Status.Enable)
                .AnyAsync();

            if (userInfo == null)
            {
                result.Msg = "账户不存在，请重新注册！";
                return result;
            }
            var log = await DbContext.FreeSql.Select<YssxUserLoginLog>().Where(x => x.UserId == user.Id).Limit(2).ToListAsync(e => e.Id);
            if (log.Count > 1)
            {
                result.SetError("用户非首次登录。");
                return result;
            }

            bool state = await DbContext.FreeSql.GetRepository<YssxUser>()
                .UpdateDiy.Set(x => new YssxUser
                {
                    Password = password.Md5(),
                    UpdateTime = DateTime.Now
                })
                .Where(x => x.Id == user.Id).ExecuteAffrowsAsync() > 0;

            result.SetSuccess(true);
            return result;
        }

        /// <summary>
        /// 工学号登陆
        /// </summary>
        /// <param name="engineeringnumber">工学号</param>
        /// <param name="password">密码</param>
        /// <param name="loginIp">IP地址</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> EngineeringNumberLogin(long schoolId, string engineeringnumber, string password, string loginIp, PermissionRequirement _requirement, int loginType)
        {
            return await Task.Run(() =>
            {
                var inform = DbContext.FreeSql.Select<YssxActivationInformation>().Where(u => u.WorkNumber == engineeringnumber && u.IsDelete == CommonConstants.IsNotDelete && u.TenantId == schoolId).First();
                if (inform == null) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "找不到该工学号!" };

                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == inform.CreateBy).First();
                if (user.Password != password.Md5()) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不正确!" };
                if (user.Status == 0) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户删除!" };
                var school = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == user.TenantId).First();
                var ticket = new UserTicket()
                {
                    Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                    RealName = user.RealName,
                    ClientId = user.ClientId,
                    Id = user.Id,
                    TenantId = user.TenantId,
                    Type = (int)user.UserType,
                    MobilePhone = user.MobilePhone,
                    SchoolName = school == null ? "" : school.Name,
                    LoginType = loginType,
                };
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                DbContext.FreeSql.Insert<YssxUserLoginLog>(log).ExecuteAffrows();
                return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
            });
        }

        /// <summary>
        /// 获取token - 游客
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<string>> GetExperienceToken(PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var ticket = new UserTicket()
                {
                    Photo = "",
                    Name = "",
                    RealName = "游客",
                    Id = CommonConstants.DefaultUserId,
                    TenantId = CommonConstants.DefaultTenantId,
                    Type = 1,
                    MobilePhone = "888888",
                    SchoolName = "",
                    SchoolType = -1,
                    LoginType = 0,
                };


                //var requirement = ServiceLocator.GetService<PermissionRequirement>();
                string token = JwtTokenHelper.ExperienceToken(ticket, _requirement);
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", token);
                ticket.AccessToken = token;
                return new ResponseContext<string> { Data = token };
            });
        }

        public async Task<ResponseContext<UserTicket>> GetExperienceToken2(PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var ticket = new UserTicket()
                {
                    Photo = "",
                    Name = "",
                    RealName = "游客",
                    Id = CommonConstants.DefaultUserId,
                    TenantId = CommonConstants.DefaultTenantId,
                    Type = 1,
                    MobilePhone = "888888",
                    SchoolName = "",
                    SchoolType = -1,
                    LoginType = 0,
                };


                //var requirement = ServiceLocator.GetService<PermissionRequirement>();
                string token = JwtTokenHelper.ExperienceToken(ticket, _requirement);
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", token);
                ticket.AccessToken = token;
                return new ResponseContext<UserTicket> { Data = ticket };
            });
        }

        /// <summary>
        /// 检查当前用户的用户类型是否改变
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CheckUserRoleResponseDto>> CheckUserRole(UserTicket user, string loginIp, int loginType)
        {
            var result = new ResponseContext<CheckUserRoleResponseDto>();
            //用户信息
            YssxUser rUserEntity = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (rUserEntity == null)
            {
                result.SetError("获取失败，未找到用户数据");
                return result;
            }
            if (user.Type == rUserEntity.UserType)//类型没有改变
            {
                result.SetSuccess(new CheckUserRoleResponseDto { UserTypeChange = false });
                return result;
            }

            string isVip = "false";

            if (rUserEntity.UserType == 1)
            {
                YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (student != null)
                {
                    bool state = student.ExpiredDate > DateTime.Now;
                    if (state)
                        isVip = "true";
                }
            }
            if (rUserEntity.UserType == 2)
            {
                isVip = "true";
            }

            //用户类型改变后返回新的token
            //学校信息
            var rSchoolEntity = await DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rUserEntity.TenantId && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            var ticket = new UserTicket()
            {
                Id = rUserEntity.Id,
                Photo = rUserEntity.Photo,
                Name = System.Web.HttpUtility.UrlDecode(rUserEntity.NikeName, Encoding.UTF8),
                RealName = rUserEntity.RealName,
                ClientId = rUserEntity.ClientId,
                TenantId = rUserEntity.TenantId,
                Type = (int)rUserEntity.UserType,
                MobilePhone = rUserEntity.MobilePhone,
                SchoolName = rSchoolEntity?.Name,
                SchoolType = (int)rSchoolEntity?.Type,
                LoginType = loginType,
                IsVip = isVip
            };

            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
            var log = new YssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };

            DbContext.FreeSql.Insert(log).ExecuteAffrows();

            RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}-{loginType}", ticket.AccessToken);

            result.SetSuccess(new CheckUserRoleResponseDto { UserTypeChange = true, UserTicket = ticket });
            return result;
        }


        /// <summary>
        /// 获取新token - 当前用户
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<string>> GetCurrentUserToken(PermissionRequirement _requirement, long currentUserId, string loginIp, int loginType)
        {
            return await Task.Run(() =>
            {
                //用户信息
                YssxUser rUserEntity = DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rUserEntity == null)
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "获取失败，未找到用户数据" };

                string isVip = "false";

                if (rUserEntity.UserType == 1)
                {
                    YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == rUserEntity.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (student != null)
                    {
                        bool state = student.ExpiredDate > DateTime.Now;
                        if (state)
                            isVip = "true";
                    }
                }
                if (rUserEntity.UserType == 2)
                {
                    isVip = "true";
                }

                //学校信息
                var rSchoolEntity = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rUserEntity.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSchoolEntity == null)
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "获取失败，学校数据异常" };

                var ticket = new UserTicket()
                {
                    Id = rUserEntity.Id,
                    Photo = rUserEntity.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(rUserEntity.NikeName, Encoding.UTF8),
                    RealName = rUserEntity.RealName,
                    ClientId = rUserEntity.ClientId,
                    TenantId = rUserEntity.TenantId,
                    Type = (int)rUserEntity.UserType,
                    MobilePhone = rUserEntity.MobilePhone,
                    SchoolName = rSchoolEntity.Name,
                    SchoolType = (int)rSchoolEntity.Type,
                    LoginType = loginType,
                    IsVip = isVip
                };
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                DbContext.FreeSql.Insert<YssxUserLoginLog>(log).ExecuteAffrows();
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}-{loginType}", ticket.AccessToken);
                return new ResponseContext<string> { Data = ticket.AccessToken };
            });
        }

        /// <summary>
        /// 获取新token - 当前用户 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> GetCurrentUserNewToken(PermissionRequirement _requirement, long currentUserId, string loginIp, int loginType)
        {
            return await Task.Run(() =>
            {
                //用户信息
                YssxUser rUserEntity = DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rUserEntity == null)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "获取失败，未找到用户数据" };

                string isVip = "false";

                if (rUserEntity.UserType == 1)
                {
                    YssxStudent student = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == rUserEntity.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (student != null)
                    {
                        bool state = student.ExpiredDate > DateTime.Now;
                        if (state)
                            isVip = "true";
                    }
                }
                if (rUserEntity.UserType == 2)
                {
                    isVip = "true";
                }

                //学校信息
                var rSchoolEntity = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rUserEntity.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSchoolEntity == null)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "获取失败，学校数据异常" };

                var ticket = new UserTicket()
                {
                    Id = rUserEntity.Id,
                    Photo = rUserEntity.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(rUserEntity.NikeName, Encoding.UTF8),
                    RealName = rUserEntity.RealName,
                    ClientId = rUserEntity.ClientId,
                    TenantId = rUserEntity.TenantId,
                    Type = (int)rUserEntity.UserType,
                    MobilePhone = rUserEntity.MobilePhone,
                    SchoolName = rSchoolEntity.Name,
                    SchoolType = (int)rSchoolEntity.Type,
                    LoginType = loginType,
                    IsVip = isVip
                };
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                DbContext.FreeSql.Insert<YssxUserLoginLog>(log).ExecuteAffrows();
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}-{loginType}", ticket.AccessToken);
                return new ResponseContext<UserTicket> { Data = ticket };
            });
        }

        #region 用户密码登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户密码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UserPasswordLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement)
        {
            if (string.IsNullOrEmpty(dto.MobilePhone))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "手机号不能为空!", Data = null };
            if (string.IsNullOrEmpty(dto.Password))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不能为空!", Data = null };
            if (dto.ClassId == 0)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空!", Data = null };

            var md5 = dto.Password.Md5();
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == dto.MobilePhone).OrderBy(x => x.CreateTime).FirstAsync();

            if (user == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该手机号没有注册!" };
            if (user.Password != md5)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不正确!" };
            if (user.Status == (int)Status.Disable)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被禁用!" };
            if (user.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被删除!" };
            if (user.UserType != (int)UserTypeEnums.OrdinaryUser && user.UserType != (int)UserTypeEnums.Student)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该用户不是学生!" };

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == dto.ClassId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!", Data = null };

            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == classModel.TenantId).FirstAsync();
            if (school == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "学校不存在!" };
            if (school.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "学校已被删除!" };

            if (user.UserType == (int)UserTypeEnums.Student && user.TenantId != school.Id)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该学生不是教师学校学生!", Data = null };

            var student = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (student != null && student.ClassId > 0)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "您已进班成功,请关闭页面!" };

            //如果不是学生身份,改为学生身份  免激活
            if (user.UserType != (int)UserTypeEnums.Student)
            {
                user.TenantId = school.Id;
                user.UserType = (int)UserTypeEnums.Student;
                user.UpdateTime = DateTime.Now;

                await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.SetSource(user).UpdateColumns(x => new
                {
                    x.TenantId,
                    x.UserType,
                    x.UpdateTime
                }).ExecuteAffrowsAsync();

                //删除缓存
                RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{dto.MobilePhone}");
            }

            if (student == null)
            {
                student = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    UserId = user.Id,
                    Name = user.RealName,
                    TenantId = school.Id,
                    SchoolName = school.Name,
                    CollegeId = classModel.CollegeId,
                    ProfessionId = classModel.ProfessionId,
                    ClassId = classModel.Id,
                    ClassName = classModel.Name,
                    CreateBy = user.Id,
                    UpdateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };

                await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(student);
            }
            else
            {
                student.CollegeId = classModel.CollegeId;
                student.ProfessionId = classModel.ProfessionId;
                student.ClassId = classModel.Id;
                student.ClassName = classModel.Name;
                student.SchoolName = school.Name;
                student.UpdateBy = user.Id;
                student.UpdateTime = DateTime.Now;

                await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.SetSource(student).UpdateColumns(x => new
                {
                    x.CollegeId,
                    x.ProfessionId,
                    x.ClassId,
                    x.ClassName,
                    x.SchoolName,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync();
            }

            var ticket = user == null ? null : new UserTicket()
            {
                Photo = user.Photo,
                Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                RealName = user.RealName,
                ClientId = user.ClientId,
                Id = user.Id,
                TenantId = user.TenantId,
                Type = (int)UserTypeEnums.Student,
                MobilePhone = user.MobilePhone,
                SchoolName = school.Name,
                SchoolType = school == null ? -1 : (int)school.Type,
                LoginType = dto.LoginType,
            };
            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

            var log = new YssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
            await DbContext.FreeSql.GetRepository<YssxUserLoginLog>().InsertAsync(log);

            if (dto.LoginType == 1)
            {
                var state = await DbContext.FreeSql.Select<YssxUserLoginLog>().Where(x => x.UserId == user.Id).CountAsync() > 1;
                ticket.FirstLanding = state;
            }

            RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);

            return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
        }
        #endregion

        #region 用户验证码登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户验证码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UserVerificationCodeLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement)
        {
            if (string.IsNullOrEmpty(dto.MobilePhone))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "手机号不能为空!", Data = null };
            if (string.IsNullOrEmpty(dto.VerificationCode))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码不能为空!", Data = null };
            if (dto.ClassId == 0)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空!", Data = null };

            var code = await DbContext.FreeSql.Select<YssxVerifyCode>().Where(m => m.MobilePhone == dto.MobilePhone).OrderByDescending(x => x.CreateTime).FirstAsync();
            if (code == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码,请确认手机号码是否正确!" };
            if (code.Code != dto.VerificationCode)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码错误!" };
            if (code.Status == 1)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码已验证使用过!" };
            if (code.ExpirTime < DateTime.Now)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "验证码已过期!" };

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == dto.ClassId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!", Data = null };

            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == classModel.TenantId).FirstAsync();
            if (school == null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "学校不存在!" };
            if (school.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "学校已被删除!" };

            var user = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == dto.MobilePhone).OrderBy(x => x.CreateTime).FirstAsync();

            YssxStudent studentModel = null;
            if (user != null)
            {
                if (user.Status == (int)Status.Disable)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被删除!" };
                if (user.UserType != (int)UserTypeEnums.OrdinaryUser && user.UserType != (int)UserTypeEnums.Student)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该用户不是学生!" };

                if (user.UserType == (int)UserTypeEnums.Student && user.TenantId != school.Id)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该学生不是教师学校学生!", Data = null };

                studentModel = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (studentModel != null && studentModel.ClassId > 0)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "您已进班成功,请关闭页面!" };
            }

            YssxStudent student = new YssxStudent();
            if (studentModel == null)
            {
                student = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    CollegeId = classModel.CollegeId,
                    ProfessionId = classModel.ProfessionId,
                    ClassId = classModel.Id,
                    ClassName = classModel.Name,
                    TenantId = school.Id,
                    SchoolName = school.Name,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };
            }

            if (user == null)
            {
                user = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    Password = CommonConstants.NewUserDefaultPwd.Md5(),
                    UserType = (int)UserTypeEnums.Student,
                    MobilePhone = dto.MobilePhone,
                    Status = (int)Status.Enable,
                    TenantId = school.Id,
                    ClientId = Guid.NewGuid(),
                    RegistrationType = 1,
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                    WeChat = "",
                    Unionid = ""
                };

                student.UserId = user.Id;
                student.Name = user.RealName;
                student.CreateBy = user.Id;
                student.UpdateBy = user.Id;

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //用户表
                        var userBack = await uow.GetRepository<YssxUser>().InsertAsync(user);
                        //学生表
                        var studentBack = await uow.GetRepository<YssxStudent>().InsertAsync(student);

                        if (userBack != null && studentBack != null)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
            }
            else
            {
                //如果不是学生身份,改为学生身份  免激活
                if (user.UserType != (int)UserTypeEnums.Student)
                {
                    user.TenantId = school.Id;
                    user.UserType = (int)UserTypeEnums.Student;
                    user.UpdateTime = DateTime.Now;

                    await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.SetSource(user).UpdateColumns(x => new
                    {
                        x.TenantId,
                        x.UserType,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();

                    //删除缓存
                    RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{dto.MobilePhone}");
                }

                if (studentModel == null)
                {
                    student.UserId = user.Id;
                    student.Name = user.RealName;
                    student.CreateBy = user.Id;
                    student.UpdateBy = user.Id;

                    await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(student);
                }
                else
                {
                    studentModel.CollegeId = classModel.CollegeId;
                    studentModel.ProfessionId = classModel.ProfessionId;
                    studentModel.ClassId = classModel.Id;
                    studentModel.ClassName = classModel.Name;
                    studentModel.SchoolName = school.Name;
                    studentModel.UpdateBy = user.Id;
                    studentModel.UpdateTime = DateTime.Now;

                    await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.SetSource(studentModel).UpdateColumns(x => new
                    {
                        x.CollegeId,
                        x.ProfessionId,
                        x.ClassId,
                        x.ClassName,
                        x.SchoolName,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }
            }

            DbContext.FreeSql.Update<YssxVerifyCode>(code).Set(x => x.Status == (int)Status.Enable).ExecuteAffrows();

            var ticket = new UserTicket()
            {
                Photo = user.Photo,
                Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                RealName = user.RealName,
                ClientId = user.ClientId,
                Id = user.Id,
                TenantId = user.TenantId,
                Type = (int)UserTypeEnums.Student,
                MobilePhone = user.MobilePhone,
                SchoolName = school.Name,
                SchoolType = school == null ? -1 : (int)school.Type,
                LoginType = dto.LoginType,
            };
            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

            var log = new YssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
            await DbContext.FreeSql.GetRepository<YssxUserLoginLog>().InsertAsync(log);

            if (dto.LoginType == 1)
            {
                var state = await DbContext.FreeSql.Select<YssxUserLoginLog>().Where(x => x.UserId == user.Id).CountAsync() > 1;
                ticket.FirstLanding = state;
            }

            RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
            return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
        }
        #endregion

        #region 用户微信登录(学生身份) 通过教师邀请
        /// <summary>
        /// 用户微信登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<WeChartTicket>> UserWeChatLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement)
        {
            if (dto.ClassId == 0)
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空!", Data = null };

            if (string.IsNullOrEmpty(dto.WeChat) || string.IsNullOrEmpty(dto.Unionid))
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "登录失败，请重新授权" };

            var user = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.Unionid == dto.Unionid).OrderBy(x => x.CreateTime).FirstAsync();

            if (user == null)
            {
                //unionid为空则取openid
                user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.WeChat == dto.WeChat).OrderBy(x => x.CreateTime).FirstAsync();
            }

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == dto.ClassId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!", Data = null };

            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == classModel.TenantId).FirstAsync();
            if (school == null)
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "学校不存在!" };
            if (school.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "学校已被删除!" };

            YssxStudent studentModel = null;
            if (user != null)
            {
                if (user.Status == (int)Status.Disable)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被删除!" };

                if (user.UserType != (int)UserTypeEnums.OrdinaryUser && user.UserType != (int)UserTypeEnums.Student)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "该用户不是学生!" };

                if (user.UserType == (int)UserTypeEnums.Student && user.TenantId != school.Id)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "该学生不是教师学校学生!", Data = null };

                studentModel = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (studentModel != null && studentModel.ClassId > 0)
                    return new ResponseContext<WeChartTicket> { Code = CommonConstants.ErrorCode, Msg = "您已进班成功,请关闭页面!" };
            }

            YssxStudent student = new YssxStudent();
            if (studentModel == null)
            {
                student = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    CollegeId = classModel.CollegeId,
                    ProfessionId = classModel.ProfessionId,
                    ClassId = classModel.Id,
                    ClassName = classModel.Name,
                    TenantId = school.Id,
                    SchoolName = school.Name,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };
            }

            if (user == null)
            {
                user = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    Password = CommonConstants.NewUserDefaultPwd.Md5(),
                    UserType = (int)UserTypeEnums.Student,
                    NikeName = dto.NikeName,
                    Status = (int)Status.Enable,
                    TenantId = school.Id,
                    ClientId = Guid.NewGuid(),
                    RegistrationType = 1,
                    WeChat = dto.WeChat,
                    Unionid = dto.Unionid,
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                };

                student.UserId = user.Id;
                student.Name = user.RealName;
                student.CreateBy = user.Id;
                student.UpdateBy = user.Id;

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //用户表
                        var userBack = await uow.GetRepository<YssxUser>().InsertAsync(user);
                        //学生表
                        var studentBack = await uow.GetRepository<YssxStudent>().InsertAsync(student);

                        if (userBack != null && studentBack != null)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(user.Unionid))
                {
                    user.Unionid = dto.Unionid;
                    user.NikeName = !string.IsNullOrEmpty(dto.NikeName) ? dto.NikeName : user.NikeName;
                    user.UserType = (int)UserTypeEnums.Student;
                    user.UpdateTime = DateTime.Now;

                    await DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(x => new
                    {
                        x.Unionid,
                        x.NikeName,
                        x.UserType,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }

                //如果不是学生身份,改为学生身份  免激活
                if (user.UserType != (int)UserTypeEnums.Student)
                {
                    user.TenantId = school.Id;
                    user.UserType = (int)UserTypeEnums.Student;
                    user.UpdateTime = DateTime.Now;

                    await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.SetSource(user).UpdateColumns(x => new
                    {
                        x.TenantId,
                        x.UserType,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }

                if (studentModel == null)
                {
                    student.UserId = user.Id;
                    student.Name = user.RealName;
                    student.CreateBy = user.Id;
                    student.UpdateBy = user.Id;

                    await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(student);
                }
                else
                {
                    studentModel.CollegeId = classModel.CollegeId;
                    studentModel.ProfessionId = classModel.ProfessionId;
                    studentModel.ClassId = classModel.Id;
                    studentModel.ClassName = classModel.Name;
                    studentModel.SchoolName = school.Name;
                    studentModel.UpdateBy = user.Id;
                    studentModel.UpdateTime = DateTime.Now;

                    await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.SetSource(studentModel).UpdateColumns(x => new
                    {
                        x.CollegeId,
                        x.ProfessionId,
                        x.ClassId,
                        x.ClassName,
                        x.SchoolName,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }
            }

            var ticket = new UserTicket()
            {
                Photo = user.Photo,
                Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                RealName = user.RealName,
                ClientId = user.ClientId,
                Id = user.Id,
                TenantId = user.TenantId,
                Type = (int)UserTypeEnums.Student,
                SchoolName = school.Name,
                SchoolType = school == null ? -1 : (int)school.Type,
                LoginType = dto.LoginType,
                MobilePhone = string.IsNullOrEmpty(user.MobilePhone) ? user.Unionid : user.MobilePhone
            };
            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

            var log = new YssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
            await DbContext.FreeSql.Insert(log).ExecuteAffrowsAsync();

            RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);

            var wxTicket = ticket.MapTo<WeChartTicket>();

            return new ResponseContext<WeChartTicket> { Code = CommonConstants.SuccessCode, Data = wxTicket };
        }
        #endregion

    }
}
