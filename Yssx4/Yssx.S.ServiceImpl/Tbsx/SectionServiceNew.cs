﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;

namespace Yssx.S.ServiceImpl
{
    public class SectionServiceNew : ISectionServiceNew
    {
        public async Task<ResponseContext<bool>> AddOrEditSection(YssxSectionDtoNew dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionNew entity = new YssxSectionNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                ParentId = dto.ParentId,
                SectionName = dto.SectionName,
                SectionTitle = dto.SectionTitle,
                KnowledgePointId = dto.KnowledgePointId,
                SectionType = dto.SectionType,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionNew course = await DbContext.FreeSql.GetRepository<YssxSectionNew>().InsertAsync(entity);
                state = course != null;
                if (dto.ParentId > 0)
                {
                    await AddSectionSummary(course.CourseId, course.Id, (int)SectionSummaryType.Section);
                }
            }
            else
            {
                state = UpdateSection(entity);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        private bool UpdateSection(YssxSectionNew entity)
        {
            return DbContext.FreeSql.GetRepository<YssxSectionNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.SectionName, x.SectionTitle, x.UpdateTime }).ExecuteAffrows() > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private async Task<bool> AddSectionSummary(long cid, long sid, int summaryType)
        {
            YssxSectionSummaryNew entity = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Where(x => x.CourseId == cid && x.SummaryType == summaryType).FirstAsync();
            bool state = false;
            if (null == entity)
            {
                entity = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().InsertAsync(new YssxSectionSummaryNew { Id = IdWorker.NextId(), CourseId = cid, SectionId = sid, Count = 1, SummaryType = summaryType, UpdateTime = DateTime.Now });
                state = entity != null;
            }
            else
            {
                entity.Count += 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }

        private async Task<bool> RemovSectionSummary(long cid, int summaryType)
        {
            YssxSectionSummaryNew entity = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Where(x => x.CourseId == cid && x.SummaryType == summaryType).FirstAsync();
            bool state = false;
            if (null != entity)
            {
                entity.Count -= 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }


        public async Task<ResponseContext<bool>> AddOrEditSectionTextBook(YssxSectionTextBookDtoNew dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionTextBookNew entity = new YssxSectionTextBookNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                Content = dto.Content,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionTextBookNew course = await DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().InsertAsync(entity);
                //state = course != null;
                state = await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.TextBook);
            }
            else
            {
                state = UpdateSectionTextBook(entity);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        private bool UpdateSectionTextBook(YssxSectionTextBookNew entity)
        {
            return DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Content, x.UpdateTime }).ExecuteAffrows() > 0;
        }

        public async Task<ResponseContext<List<YssxSectionTextBookDtoNew>>> GetSectionTextBookList(long sid)
        {
            ResponseContext<List<YssxSectionTextBookDtoNew>> response = new ResponseContext<List<YssxSectionTextBookDtoNew>>();
            List<YssxSectionTextBookNew> list = await DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().Where(x => x.SectionId == sid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            response.Data = list.Select(x => new YssxSectionTextBookDtoNew
            {
                Id = x.Id,
                Content = x.Content,
                CourseId = x.CourseId,
                SectionId = x.SectionId
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<List<YssxSectionDtoNew>>> GetSectionList(long courseId)
        {
            ResponseContext<List<YssxSectionDtoNew>> response = new ResponseContext<List<YssxSectionDtoNew>>();
            List<YssxSectionNew> list = await DbContext.FreeSql.GetRepository<YssxSectionNew>().Where(x => x.CourseId == courseId && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToListAsync();
            response.Data = list.Select(x => new YssxSectionDtoNew
            {
                Id = x.Id,
                SectionName = x.SectionName,
                SectionTitle = x.SectionTitle,
                ParentId = x.ParentId,
                SectionType = x.SectionType,
                CourseId = x.CourseId,
                Sort = x.Sort
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<bool>> RemoveSection(long id)
        {
            YssxSectionNew entity = await DbContext.FreeSql.GetRepository<YssxSectionNew>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Section);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionTextBook(long id)
        {
            YssxSectionTextBookNew entity = await DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.TextBook);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionSummaryDtoNew>>> GetCourseSummaryList(long sid)
        {
            ResponseContext<List<YssxSectionSummaryDtoNew>> response = new ResponseContext<List<YssxSectionSummaryDtoNew>>();
            List<YssxSectionSummaryNew> list = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Where(x => x.SectionId == sid).ToListAsync();
            response.Data = list.Select(x => new YssxSectionSummaryDtoNew
            {
                Id = x.Id,
                Count = x.Count,
                CourseId = x.CourseId,
                SectionId = x.SectionId,
                SummaryType = x.SummaryType
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionFile(YssxSectionFilesDtoNew dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionFilesNew entity = new YssxSectionFilesNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                KnowledgePointId = dto.KnowledgePointId,
                File = dto.File,
                FileName = dto.FileName,
                FilesType = dto.FilesType,
                Sort = dto.Sort,
                CreateBy = user.Id,
                SectionType = dto.SectionType,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
                CourseFileId = dto.CourseFileId,
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                if (dto.FromType == 1)
                {
                    YssxCourceFilesNew courceFiles = new YssxCourceFilesNew
                    {
                        Id = IdWorker.NextId(),
                        CreateBy = user.Id,

                        TenantId = user.TenantId,
                        File = dto.File,
                        FileName = dto.FileName,
                        FileSize = dto.FileSize,
                        Sort = dto.Sort,
                        FilesType = dto.FilesType,
                    };
                    entity.CourseFileId = courceFiles.Id;
                    YssxCourceFilesNew files = await DbContext.FreeSql.GetRepository<YssxCourceFilesNew>().InsertAsync(courceFiles);
                }

                YssxSectionFilesNew course = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().InsertAsync(entity);
                state = (course != null);

                await AddSectionSummary(course.CourseId, course.SectionId, dto.FilesType == 6 ? (int)SectionSummaryType.Video : (int)SectionSummaryType.Files);
            }
            else
            {
                //YssxCourceFilesNew fileEntity = await DbContext.FreeSql.GetRepository<YssxCourceFilesNew>().Where(x => x.Id == dto.CourseFileId).FirstAsync();
                state = DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.File, x.Sort, x.FileName, x.UpdateTime }).ExecuteAffrows() > 0;
                //bool state2 = DbContext.FreeSql.GetRepository<YssxCourceFilesNew>().UpdateDiy.SetSource(fileEntity).UpdateColumns(x => new { x.File, x.FileName, x.Sort, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }


        public async Task<ResponseContext<bool>> AddOrEditSectionTopic(YssxSectionTopicDtoNew dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionTopicNew entity = new YssxSectionTopicNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                QuestionId = dto.QuestionId,
                QuestionName = dto.QuetionName,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };
            bool isExist = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Where(x => x.CourseId == dto.CourseId && x.QuestionId == dto.QuestionId).AnyAsync();
            if (isExist) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "已经存在相同的题目!" };
            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionTopicNew course = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().InsertAsync(entity);
                state = course != null;
                await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.Exercises);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.QuestionId, x.QuestionName, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionFilesDtoNew>>> GetSectionFilesList(long courseId, long sid)
        {
            ResponseContext<List<YssxSectionFilesDtoNew>> response = new ResponseContext<List<YssxSectionFilesDtoNew>>();
            List<YssxSectionFilesNew> list = new List<YssxSectionFilesNew>();
            list = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.CourseId == courseId && x.SectionId == sid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            response.Data = list.Select(x => new YssxSectionFilesDtoNew
            {
                Id = x.Id,
                File = x.File,
                FileName = x.FileName,
                FilesType = x.FilesType,
                SectionId = x.SectionId,
                CourseFileId = x.CourseFileId,
                KnowledgePointId = x.KnowledgePointId,
                SectionType = x.SectionType,
                CourseId = x.CourseId,

            }).ToList();
            return response;
        }

        private async Task<ResponseContext<List<YssxSectionFilesDtoNew>>> GetSectionFilesList(int fileType, long courseId)
        {
            ResponseContext<List<YssxSectionFilesDtoNew>> response = new ResponseContext<List<YssxSectionFilesDtoNew>>();

            var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Select.From<YssxSectionNew>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType != 6 && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDtoNew
            {
                Id = a.Id,
                File = a.File,
                FileName = a.FileName,
                FilesType = a.FilesType,
                SectionId = a.SectionId,
                SectionName = b.SectionName,
                CourseFileId = a.CourseFileId,
                KnowledgePointId = a.KnowledgePointId,
                SectionType = a.SectionType,
                CourseId = a.CourseId,
            });
            response.Data = selectList;
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionCase(YssxSectionCaseDtoNew dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionCaseNew entity = new YssxSectionCaseNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                CaseId = dto.CaseId,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionCaseNew course = await DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().InsertAsync(entity);
                state = course != null;
                state = await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.Case);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.CaseId, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionCase(long id)
        {
            YssxSectionCaseNew entity = await DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Case);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionTopic(long id)
        {
            YssxSectionTopicNew entity = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Where(x => x.Id == id).FirstAsync();
            bool state = false;
            if (entity != null)
            {
                entity.IsDelete = 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
                await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Case);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionFile(long id)
        {
            YssxSectionFilesNew entity = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;

            await RemovSectionSummary(entity.CourseId, entity.FilesType == 6 ? (int)SectionSummaryType.Video : (int)SectionSummaryType.Files);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionTopicDtoNew>>> GetYssxSectionTopicList(long sid)
        {
            ResponseContext<List<YssxSectionTopicDtoNew>> response = new ResponseContext<List<YssxSectionTopicDtoNew>>();
            List<YssxSectionTopicDtoNew> list = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Select.From<YssxTopicPublicNew>((a, b) => a.LeftJoin(aa => aa.QuestionId == b.Id)).Where((a, b) => a.SectionId == sid && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxSectionTopicDtoNew
            {
                Id = a.Id,
                KnowledgePointId = a.KnowledgePointId,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuetionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
                Content = b.Content,
                Score = b.Score,
            });
            //response.Data = list.Select(x => new YssxSectionTopicDtoNew
            //{
            //    Id = x.Id,
            //    KnowledgePointId = x.KnowledgePointId,
            //    QuestionId = x.QuestionId,
            //    SectionId = x.SectionId,
            //    QuetionName = x.QuestionName,
            //    Sort = x.Sort,
            //    CourseId = x.CourseId,

            //}).ToList();
            response.Data = list;
            return response;
        }

        /// <summary>
        /// 根据节获取习题
        /// 一般情况下，获取到所有的习题后，PC端可以自己筛选数据，不需要再次请求接口进行查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSectionTopicDtoNew>>> GetYssxSectionTopicistForPC(SectionTopicQueryNew query)
        {
            ResponseContext<List<YssxSectionTopicDtoNew>> response = new ResponseContext<List<YssxSectionTopicDtoNew>>();

            var select = DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Select.From<YssxTopicPublicNew>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.SectionId == query.SectionId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

            if (query.QuestionType.HasValue)
                select.Where((a, b) => b.QuestionType == query.QuestionType);
            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b) => b.Title.Contains(query.Keyword));

            List<YssxSectionTopicDtoNew> list = await select.OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxSectionTopicDtoNew
            {
                Id = a.Id,
                KnowledgePointId = a.KnowledgePointId,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuetionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
                Content = b.Content,
                Score = b.Score,
            });

            response.Data = list;
            return response;
        }

        public async Task<ResponseContext<SectionFileListDtoNew>> GetSectionFilesListByType(long courseId, long sid, int fileType)
        {
            ResponseContext<SectionFileListDtoNew> response = new ResponseContext<SectionFileListDtoNew>();
            response.Data = new SectionFileListDtoNew();
            if (fileType == -1)
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Select.From<YssxSectionNew>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDtoNew
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList.Where(x => x.SectionId > 0).ToList(); ;

                response.Data.CourseFileList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.CourseId == courseId && x.IsDelete == CommonConstants.IsNotDelete && x.SectionId == 0).ToListAsync(a => new YssxSectionFilesDtoNew
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });
            }
            else if (fileType != 6)
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Select.From<YssxSectionNew>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType != 6 && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDtoNew
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    SectionTitle = b.SectionTitle,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList.Where(x => x.SectionId > 0).ToList();
                response.Data.CourseFileList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.CourseId == courseId && x.FilesType != 6 && x.IsDelete == CommonConstants.IsNotDelete && x.SectionId == 0).ToListAsync(a => new YssxSectionFilesDtoNew
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });
            }
            else
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Select.From<YssxSectionNew>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType == fileType && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDtoNew
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList;
            }
            return response;
        }

        /// <summary>
        /// 获取课程下所有的课件,教案,视频按类型筛选 （包含课程章节下的）
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSectionFilesDtoNew>>> GetSectionFilesListByCourseId(long courseId)
        {
            var response = new ResponseContext<List<YssxSectionFilesDtoNew>>();

            var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Select
            .From<YssxSectionNew>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
            .Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete)
            .ToListAsync((a, b) => new YssxSectionFilesDtoNew
            {
                Id = a.Id,
                File = a.File,
                FileName = a.FileName,
                FilesType = a.FilesType,
                SectionId = a.SectionId,
                SectionName = b.SectionName,
                CourseFileId = a.CourseFileId,
                KnowledgePointId = a.KnowledgePointId,
                SectionType = a.SectionType,
                CourseId = a.CourseId,
            });

            response.Data = selectList;
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionSceneTraining(YssxSectionSceneTrainingDtoNew dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };
            YssxSectionSceneTrainingNew entity = new YssxSectionSceneTrainingNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                SceneTrainingId = dto.SceneTrainingId,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionSceneTrainingNew sectionSceneTraining = await DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().InsertAsync(entity);
                state = sectionSceneTraining != null;
                state = await AddSectionSummary(sectionSceneTraining.CourseId, sectionSceneTraining.SectionId, (int)SectionSummaryType.SectionSceneTraining);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.SceneTrainingId, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionSceneTraininge(long id)
        {
            YssxSectionSceneTrainingNew entity = await DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源" };
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.SectionSceneTraining);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionSceneTrainingViewModelNew>>> GetYssxSectionSceneTrainingList(long sid)
        {
            ResponseContext<List<YssxSectionSceneTrainingViewModelNew>> response = new ResponseContext<List<YssxSectionSceneTrainingViewModelNew>>();

            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionSceneTrainingViewModelNew>(
                @"SELECT a.Id,a.CourseId,a.SectionId,a.ScenetrainingId,a.KnowledgePointId,x.Name,x.SelectCompanyId,x.Ywcj,x.Pxxh,x.Ywwl,x.Zzjy,x.Describe, x.YwcjName, 
                        x.SelectCompanyName FROM yssx_section_scenetraining a INNER JOIN(select b.*, c.Name AS YwcjName, d.Name AS SelectCompanyName FROM yssx_scene_training b 
                        LEFT JOIN yssx_business_scene c ON b.YwcjId = c.Id LEFT JOIN yssx_case d on b.SelectCompanyId = d.Id) x ON a.ScenetrainingId = x.Id WHERE  
                        a.IsDelete =" + CommonConstants.IsNotDelete + " AND a.SectionId=" + sid + " ");

            response.Data = items;
            return response;
        }

    }
}
