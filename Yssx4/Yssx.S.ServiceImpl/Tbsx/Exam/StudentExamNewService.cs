﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.AnswerCompare;
using Yssx.Framework.AnswerCompare.EntitysV2;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using Yssx.Framework.Utils;
using Yssx.Redis;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.IServices;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 学生考试相关服务 - 同步实训
    /// </summary>
    public class StudentExamNewService : IStudentExamServiceNew
    {

        #region 预览

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperBasicInfoNew>> GetExamPaperBasicInfoView(long gradeId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperBasicInfoNew>();
            var user = userTicket;

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "gradeId不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null || grade.Id == 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"答题记录不存在 ";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            #endregion

            #region 考试基本信息

            var caseInfo = await DbContext.FreeSql.GetRepository<YssxCaseNew>().Select.Where(s => s.Id == exam.CaseId).FirstAsync($"{CommonConstants.Cache_GetCaseById}{exam.CaseId}", true, 10 * 60, true);

            result.Data = new ExamPaperBasicInfoNew()
            {
                ExamId = exam.Id,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,
                StudentExamStatus = grade.Status,

                LeftSeconds = (long)grade.LeftSeconds,
                UsedSeconds = (long)grade.UsedSeconds,
                GradeId = gradeId,

                QuestionInfoList = await GetExamQuestions(exam, grade, null, true),

                StudentGradeInfo = new StudentGradeInfoNew()
                {
                    GradeId = grade.Id,
                    Score = grade.Score,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score >= exam.PassScore,
                    Status = grade.Status,
                }
            };

            #endregion

            return result;
        }

        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamQuestionInfoNew>> GetQuestionInfoView(long gradeId, long questionId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamQuestionInfoNew>(CommonConstants.BadRequest, $"");

            var user = userTicket;

            #region 校验

            if (gradeId == 0 || questionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "所有参数都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            //var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetTopicById}{questionId}", true, 10 * 60, true);

            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"题目不存在";
                return result;
            }

            if (exam.CaseId != question.CaseId)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"题目和试卷信息不匹配";
                return result;
            }

            var validateResult = await ValidateQuestion(exam, grade, user);

            if (validateResult.Code != CommonConstants.SuccessCode)
            {
                result.Code = validateResult.Code;
                result.Msg = validateResult.Msg;
                return result;
            }

            #endregion

            result = await GetQuestionInfoBase(exam, grade, question, user, isView: true);

            return result;
        }

        #endregion

        #region 查询题目相关

        /// <summary>
        /// 获取试卷题目信息
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<List<QuestionInfoNew>> GetExamQuestions(ExamPaperNew exam, ExamStudentGradeNew grade, ExamPaperBasicInfoRequestDto query, bool isView = false)
        {
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicPostionsByCaseId}{exam.CaseId}", () => DbContext.FreeSql.GetRepository<YssxTopicNew>().Select
                 .From<YssxPositionNew, YssxDepartmentNew, YssxFlow, YssxDrillType>((a, b, c, d, e) => a.LeftJoin(aa => aa.PositionId == b.Id).LeftJoin(aa => aa.DepartmentId == c.Id).LeftJoin(aa => aa.FlowId == d.Id).LeftJoin(aa => aa.DrillTypeId == e.Id))
                 .Where((a, b, c, d, e) => a.CaseId == exam.CaseId && a.IsDelete == CommonConstants.IsNotDelete)
                 .ToList((a, b, c, d, e) => new QuestionInfoNew()
                 {
                     QuestionId = a.Id,
                     ParentQuestionId = a.ParentId,
                     QuestionType = a.QuestionType,
                     PositionId = a.PositionId,
                     PositionName = b.Name,
                     AdviceTime = a.AdviceTime,
                     Content = a.Content,
                     DepartmentId = a.DepartmentId,
                     DepartmentName = c.DepartmentName,
                     DrillTypeId = a.DrillTypeId,
                     FlowId = a.FlowId,
                     FlowSort = d.Sort,
                     FlowName = d.Name,
                     Title = a.Title,
                     Score = a.Score,
                     Sort = a.Sort,
                     DrillTypeSort = e.Sort,
                 }), 10 * 60, true, false);

            if (query != null)
            {
                //根据条件进行筛选
                var questionList1 = questionList.Where(e => e.DrillTypeId == query.DrillTypeId);
                if (query.FlowId.HasValue)
                    questionList1 = questionList1.Where(e => e.FlowId == query.FlowId);
                if (query.DepartmentId.HasValue)
                    questionList1 = questionList1.Where(e => e.DepartmentId == query.DepartmentId);
                if (query.QuestionType.HasValue)
                    questionList1 = questionList1.Where(e => e.QuestionType == query.QuestionType);
                if (!string.IsNullOrWhiteSpace(query.Title))
                    questionList1 = questionList1.Where(e => !string.IsNullOrWhiteSpace(e.Title) && e.Title.Contains(query.Title));

                questionList = questionList1.ToList();
            }

            //学生题目作答信息
            var studentGradeDetails = new List<GradeDtailDtoNew>();

            if (grade != null && grade.Id > 0)
            {
                //获取学生作答记录
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>()
                    .Where(a => a.GradeId == grade.Id)
                    .ToListAsync(a => new GradeDtailDtoNew
                    {
                        Id = a.Id,
                        QuestionId = a.QuestionId,
                        ParentQuestionId = a.ParentQuestionId,
                        Status = a.Status,
                        AccountEntryStatus = a.AccountEntryStatus,
                        AccountEntryAuditStatus = a.AccountEntryAuditStatus,
                        Score = a.Score,
                        Sort = -1
                    });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())//存在作答记录标记题目是否已完成状态、学生答案等等
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {
                            if (a.QuestionType == QuestionType.MainSubQuestion)
                            {
                                var details = studentGradeDetails.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionId != a.QuestionId);

                                if (details != null && details.Any())
                                {
                                    var accountQuestions = questionList.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionType == QuestionType.AccountEntry).ToList();

                                    if (accountQuestions != null && accountQuestions.Any())
                                    {
                                        accountQuestions.ForEach(s =>
                                        {
                                            var detail = details.FirstOrDefault(d => d.QuestionId == s.QuestionId);
                                            if (detail != null && detail.Id > 0)
                                            {
                                                detail.Sort = s.Sort;
                                            }
                                        });
                                        a.AccountEntryStatusDic = details.Where(s => s.Sort >= 0).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                                    }
                                }
                            }
                            else if (a.QuestionType == QuestionType.AccountEntry)
                            {
                                a.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() {
                                new Tuple<long, AccountEntryStatus,AccountEntryAuditStatus,int>(gradeDetail.QuestionId,gradeDetail.AccountEntryStatus,gradeDetail.AccountEntryAuditStatus,a.Sort)
                            };
                            }
                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            if (query != null && query.TopicGradeStatus.HasValue)//题目是否已完成过滤
            {
                switch (query.TopicGradeStatus.Value)
                {
                    case AnswerDTOStatus.NoAnswer://获取所有未作答的题目
                        questionList = questionList.Where(s => s.QuestionAnswerStatus == AnswerDTOStatus.NoAnswer).ToList();
                        break;
                    case AnswerDTOStatus.Answered://获取所有已作答的题目
                        questionList.RemoveAll(s => s.QuestionAnswerStatus == AnswerDTOStatus.NoAnswer);//移除未作答的题目。
                        break;
                    case AnswerDTOStatus.Right:
                    case AnswerDTOStatus.PartRight:
                    case AnswerDTOStatus.Error:
                    default:
                        break;
                }
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            questionList = questionList
                .OrderBy(s => s.DrillTypeId)
                .ThenBy(s => s.FlowSort)
                .ThenBy(s => s.Sort)
                .ThenBy(s => s.QuestionId)
                .ToList();

            return questionList;
        }

        /// <summary>
        /// 查看题目时不传question和answer参数
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="user"></param>
        /// <param name="question"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperGroupStudentViewNew>> ValidateQuestion(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user, YssxTopicNew question = null, QuestionAnswerNew answer = null)
        {
            var isStudent = user.Type == 1;//非学生，教师、教务、专家都可以查看
            var result = new ResponseContext<ExamPaperGroupStudentViewNew>();

            var groupStudent = new ExamPaperGroupStudentViewNew();
            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:
                    if (isStudent)
                    {
                        if (grade.UserId != user.Id)
                        {
                            result.Code = CommonConstants.BadRequest;
                        }
                    }
                    break;

                //case ExamType.EmulationTest:
                //case ExamType.SchoolCompetition:
                //case ExamType.ProvincialCompetition:
                //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                //    {
                //        if (isStudent)
                //        {
                //            if (grade.UserId != user.Id)
                //            {
                //                result.Code = CommonConstants.BadRequest;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (isStudent)
                //        {
                //            groupStudent = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.Where(s => s.GradeId == grade.Id && s.UserId == user.Id).FirstAsync(s => new ExamPaperGroupStudentViewNew());
                //        }
                //        else
                //        {
                //            groupStudent = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.Where(s => s.GradeId == grade.Id).FirstAsync(s => new ExamPaperGroupStudentViewNew());
                //        }

                //        if (groupStudent == null || groupStudent.GroupId == 0)
                //        {
                //            result.Code = CommonConstants.BadRequest;
                //            break;
                //        }
                //    }
                //    break;

                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }

            if (result.Code == CommonConstants.SuccessCode && question != null && answer != null)//提交答案 
            {
                if (groupStudent != null && groupStudent.GroupId > 0)
                {
                    if (groupStudent.StudentExamStatus == StudentExamStatus.End)
                    {
                        result.Code = CommonConstants.BadRequest;
                        result.Msg = $"您已交卷！";
                        goto End;
                    }

                    if (question.PositionId != groupStudent.PostionId)
                    {
                        var groupStudentStatus = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.Where(s => s.GroupId == groupStudent.GroupId && s.PostionId == question.PositionId).FirstAsync(s => s.StudentExamStatus);
                        if (groupStudentStatus == StudentExamStatus.End)
                        {
                            result.Code = CommonConstants.BadRequest;
                            result.Msg = $"该题目所属岗位已交卷，不能再提交答案！";
                            goto End;
                        }
                    }
                }

                if (exam.ExamType != ExamType.PracticeTest)
                {
                    if (await ValidateOverdueSubmit(exam, grade, user, new ExamPaperGroupStudentNew() { LeftSeconds = groupStudent.LeftSeconds, RecordTime = groupStudent.RecordTime }))
                    {
                        result.Code = CommonConstants.Overdue;
                        result.Msg = $"考试时间截止，已经自动交卷！";
                        goto End;
                    }
                }

                result = await ValidateSubmitQuestion(exam, grade, question, answer, groupStudent);
            }
            else
            {
                result.Msg = result.Code == CommonConstants.BadRequest ? $"考试信息不存在 " : "";
            }

            End:
            if (result.Code == CommonConstants.SuccessCode)
            {
                result.Data = groupStudent;
            }
            return result;
        }

        private async Task<bool> ValidateOverdueSubmit(ExamPaperNew exam, ExamStudentGradeNew studentGrade, UserTicket userTicket, ExamPaperGroupStudentNew groupStudent = null)
        {
            var isSubmit = false;
            if (exam.CompetitionType == CompetitionType.IndividualCompetition)
            {
                if (GetLeftSeconds(studentGrade.LeftSeconds, studentGrade.RecordTime) <= 0)
                {
                    await SubmitExam(studentGrade.Id, userTicket, isAutoSubmit: true);
                    isSubmit = true;
                }
            }
            else
            {
                if (GetLeftSeconds(groupStudent.LeftSeconds, groupStudent.RecordTime) <= 0)
                {
                    await SubmitExam(studentGrade.Id, userTicket, true);
                    isSubmit = true;
                }
            }

            return isSubmit;
        }

        /// <summary>
        /// 获取剩余考试时间
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private double GetLeftSeconds(double leftSeconds, DateTime recordTime)
        {
            var left = leftSeconds - (DateTime.Now - recordTime).TotalSeconds;
            if (left <= 0)
            {
                return 0;
            }
            return left;
        }

        /// <summary>
        /// 获取剩余考试时间
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private double GetLeftSeconds(ExamPaperNew exam)
        {
            var left = (exam.EndTime.Value - DateTime.Now).TotalSeconds;
            if (left <= 0)
            {
                return 0;
            }
            return left;
        }

        /// <summary>
        /// 提交答案题目验证，由ValidateQuestion调用
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="answer"></param>
        /// <param name="groupStudent"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperGroupStudentViewNew>> ValidateSubmitQuestion(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, QuestionAnswerNew answer, ExamPaperGroupStudentViewNew groupStudent)
        {
            var result = new ResponseContext<ExamPaperGroupStudentViewNew>();

            #region 查询子题目信息

            var multiQuestions = new List<SubQuestionDto>();

            if (question.QuestionType == QuestionType.MainSubQuestion)
            {
                if (answer.MultiQuestionAnswers == null || !answer.MultiQuestionAnswers.Any())
                {
                    result.Code = CommonConstants.BadRequest;
                    result.Msg = $"多题型需传入子题目答题信息";
                    return result;
                }
                multiQuestions = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.ParentId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId}{question.Id}", true, 10 * 60, true).Result
                        .Select(s =>
                        new SubQuestionDto()
                        {
                            Id = s.Id,
                            CaseId = s.CaseId,
                            QuestionType = s.QuestionType,
                            PositionId = s.PositionId
                        }).ToList();
            }

            #endregion

            #region 验证分录题作答权限

            if (groupStudent != null && groupStudent.GroupId > 0)
            {
                var isOwner = groupStudent.PostionId == question.PositionId;

                //非分录题或者综合题下无分录题
                if ((question.QuestionType != QuestionType.MainSubQuestion && question.QuestionType != QuestionType.AccountEntry) || (question.QuestionType == QuestionType.MainSubQuestion && !multiQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry)))
                {
                    if (!isOwner)
                    {
                        result.Code = CommonConstants.ErrorCode;
                        result.Msg = $"没有权限作答该题目！";
                        return result;
                    }
                }
                else
                {
                    answer.IsIgnore = !isOwner;
                    if (question.QuestionType == QuestionType.AccountEntry)
                    {
                        await IgnoreQuestion(answer, question, groupStudent.PostionId);

                        if (answer.IsIgnore)
                        {
                            result.Code = CommonConstants.ErrorCode;
                            result.Msg = $"没有权限作答该题目！";
                            return result;
                        }

                    }
                    else if (question.QuestionType == QuestionType.MainSubQuestion)
                    {
                        answer.IsIgnore = false;
                        await IgnoreMainQuestion(answer, multiQuestions, question, groupStudent.PostionId);

                        if (!answer.MultiQuestionAnswers.Any(s => s.IsIgnore == false))
                        {
                            result.Code = CommonConstants.ErrorCode;
                            result.Msg = $"没有权限作答该题目！";
                            return result;
                        }
                    }
                }
            }

            #endregion

            switch (question.QuestionType)
            {
                case QuestionType.AccountEntry:

                    answer.QuestionType = QuestionType.AccountEntry;
                    if (answer.AccountEntryList == null || !answer.AccountEntryList.Any())
                    {
                        result.Code = CommonConstants.BadRequest;
                        result.Msg = $"分录题需要传入AccountEntryList数据";
                    }

                    if (grade.IsSettled)//如果已结帐，分录题不能再提交答案
                    {
                        result.Code = CommonConstants.ErrorCode;
                        result.Msg = "已结帐，分录题不能再提交答案 ";
                    }
                    break;

                case QuestionType.MainSubQuestion:

                    foreach (var item in answer.MultiQuestionAnswers)
                    {
                        var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                        if (subQuestion == null || subQuestion.Id == 0)
                        {
                            result.Msg = "子题目信息不存在";
                            result.Code = CommonConstants.NotFund;
                            goto End;
                        }
                        if (subQuestion.QuestionType == QuestionType.AccountEntry)
                        {
                            item.QuestionType = QuestionType.AccountEntry;
                            if (item.AccountEntryList == null || !item.AccountEntryList.Any())
                            {
                                result.Code = CommonConstants.BadRequest;
                                result.Msg = $"分录题需要传入AccountEntryList数据";
                                goto End;
                            }
                        }
                    }

                    if (grade.IsSettled) //如果已结帐，分录题不能再提交答案
                    {
                        if (multiQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                        {
                            result.Code = CommonConstants.ErrorCode;
                            result.Msg = "已结帐，分录题不能再提交答案 ";
                        }
                    }
                    break;

                case QuestionType.FinancialStatements:
                    if (!grade.IsSettled)//如果未结账，则财务报表题不能提交答案
                    {
                        result.Code = CommonConstants.ErrorCode;
                        result.Msg = "未结帐，财务报表题不能提交答案 ";
                    }
                    break;

                default:
                    break;
            }

            End:

            return result;
        }

        private async Task IgnoreQuestion(QuestionAnswerNew answer, YssxTopicNew question, long positionId)
        {
            answer.QuestionType = question.QuestionType;
            var accountEntryStatusDic = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(s => s.GradeId == answer.GradeId && s.QuestionId == answer.QuestionId).FirstAsync(s => new { s.AccountEntryStatus, s.AccountEntryAuditStatus });

            var accountEntryStatus = accountEntryStatusDic?.AccountEntryStatus ?? AccountEntryStatus.None;
            var accountEntryAuditStatus = accountEntryStatusDic?.AccountEntryAuditStatus ?? AccountEntryAuditStatus.None;

            var casePositions = await DbContext.FreeSql.GetRepository<YssxCasePositionNew>().Where(s => s.CaseId == question.CaseId && s.IsDelete == CommonConstants.IsNotDelete)
                        .ToListAsync($"{CommonConstants.Cache_GetCasePositionsByCaseId}{question.CaseId}", true, 10 * 60, true);

            switch (accountEntryStatus)
            {
                case AccountEntryStatus.None:
                    answer.IsIgnore = question.PositionId != positionId;
                    break;

                case AccountEntryStatus.Cashier:
                    if (casePositions.Any(s => s.PostionId == CommonConstants.Cashier))
                    {
                        answer.IsIgnore = positionId != CommonConstants.Cashier;
                    }
                    else
                    {
                        answer.IsIgnore = (positionId != CommonConstants.AccountingManager && positionId != CommonConstants.FinanceManager);
                    }
                    break;

                case AccountEntryStatus.AccountingManager:
                    answer.IsIgnore = (positionId != CommonConstants.AccountingManager && positionId != CommonConstants.FinanceManager);
                    break;

                case AccountEntryStatus.Audit:
                    answer.IsIgnore = true;
                    break;

                default:
                    answer.IsIgnore = true;
                    break;

            }

            if (!answer.IsIgnore)
            {
                var certificateTopic = new YssxCertificateTopicNew();

                if (accountEntryStatus == AccountEntryStatus.None && !answer.IsSubmit)//是否制单人提交制单
                {
                    answer.AccountEntryStatus = AccountEntryStatus.None;
                }
                else if (accountEntryStatus == AccountEntryStatus.Cashier && answer.IsCancel)//是否出纳退回制单
                {
                    answer.AccountEntryStatus = AccountEntryStatus.None;
                    answer.AccountEntryAuditStatus = AccountEntryAuditStatus.Back;
                }
                else if (accountEntryStatus == AccountEntryStatus.AccountingManager && answer.IsCancel)//是否主管退回制单
                {
                    if (answer.IsBackToCashier)
                    {
                        certificateTopic = await DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>().Where(s => s.TopicId == answer.QuestionId)
                                                .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{answer.QuestionId}", true, 10 * 60);

                        if (certificateTopic.IsDisableAuditor == Status.Enable)
                        {
                            answer.AccountEntryStatus = AccountEntryStatus.Cashier;
                        }
                        else
                        {
                            answer.AccountEntryStatus = AccountEntryStatus.None;
                        }
                    }
                    else
                    {
                        answer.AccountEntryStatus = AccountEntryStatus.None;
                    }
                    answer.AccountEntryAuditStatus = AccountEntryAuditStatus.Back;
                }
                else
                {
                    certificateTopic = await DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>().Where(s => s.TopicId == answer.QuestionId)
                                                .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{answer.QuestionId}", true, 10 * 60);

                    if (accountEntryStatus == AccountEntryStatus.AccountingManager)
                    {
                        answer.AccountEntryAuditStatus = AccountEntryAuditStatus.Audit;
                    }
                    else
                    {
                        answer.AccountEntryAuditStatus = AccountEntryAuditStatus.Wait;
                    }
                    answer.AccountEntryStatus = GetAccountEntryStatus(accountEntryStatus, certificateTopic);
                }
            }
            else
            {
                answer.AccountEntryStatus = accountEntryStatus;
                answer.AccountEntryAuditStatus = accountEntryAuditStatus;
            }
        }

        private async Task IgnoreMainQuestion(QuestionAnswerNew answer, List<SubQuestionDto> multiQuestions, YssxTopicNew question, long positionId)
        {
            foreach (var item in answer.MultiQuestionAnswers)
            {
                var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                if (subQuestion == null || subQuestion.Id == 0)
                {
                    continue;
                }
                if (subQuestion.QuestionType == QuestionType.AccountEntry)
                {
                    await IgnoreQuestion(item, new YssxTopicNew() { Id = subQuestion.Id, CaseId = subQuestion.CaseId, QuestionType = subQuestion.QuestionType, PositionId = question.PositionId }, positionId);
                }
                else
                {
                    item.IsIgnore = question.PositionId != positionId;
                }
            }
        }

        /// <summary>
        /// 获取当前题目的下一个状态
        /// </summary>
        /// <param name="currentStatus"></param>
        /// <param name="certificateTopic"></param>
        /// <returns></returns>
        private AccountEntryStatus GetAccountEntryStatus(AccountEntryStatus currentStatus, YssxCertificateTopicNew certificateTopic)
        {
            switch (currentStatus)
            {
                case AccountEntryStatus.None:

                    if (certificateTopic.IsDisableCashier == Status.Enable)
                    {
                        return AccountEntryStatus.Cashier;
                    }

                    return AccountEntryStatus.AccountingManager;

                //if (certificateTopic.IsDisableAM == Status.Enable)
                //{
                //    return AccountEntryStatus.AccountingManager;
                //}

                //if (certificateTopic.IsDisableAuditor == Status.Enable)
                //{
                //    return AccountEntryStatus.Audit;
                //}
                //else
                //{
                //    return AccountEntryStatus.Over;
                //}

                case AccountEntryStatus.Cashier:

                    return AccountEntryStatus.AccountingManager;
                //if (certificateTopic.IsDisableAM == Status.Enable)
                //{
                //    return AccountEntryStatus.AccountingManager;
                //}

                //if (certificateTopic.IsDisableAuditor == Status.Enable)
                //{
                //    return AccountEntryStatus.Audit;
                //}
                //else
                //{
                //    return AccountEntryStatus.Over;
                //}

                case AccountEntryStatus.AccountingManager:

                    return AccountEntryStatus.AccountingManager;

                //if (certificateTopic.IsDisableAuditor == Status.Enable)
                //{
                //    return AccountEntryStatus.Audit;
                //}
                //else
                //{
                //    return AccountEntryStatus.Over;
                //}

                //case AccountEntryStatus.Audit:

                //    return AccountEntryStatus.Over;

                default:
                    return AccountEntryStatus.None;
            }
        }

        /// <summary>
        /// 获取题目信息基础函数
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfoNew>> GetQuestionInfoBase(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user, bool isBigDataSubmit = false, bool isView = false)
        {
            var result = new ResponseContext<ExamQuestionInfoNew>();

            var examId = exam?.Id;
            var gradeId = grade?.Id;
            var questionId = question.Id;
            // TODO MapTo配置文件
            var questionDTO = question.MapTo<ExamQuestionInfoNew>();
            questionDTO.QuestionId = questionId;

            //加载题目信息
            if (questionDTO.QuestionType == QuestionType.SingleChoice
                || questionDTO.QuestionType == QuestionType.MultiChoice
                || questionDTO.QuestionType == QuestionType.Judge)
            {
                //添加选项信息
                var options = await DbContext.FreeSql.GetRepository<YssxAnswerOptionNew>().Where(a => a.TopicId == questionId).OrderBy(a => a.Sort).ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId}{questionId}", true, 10 * 60);
                var optionList = new List<ChoiceOptionNew>();
                options.ForEach(a =>
                {
                    optionList.Add(new ChoiceOptionNew()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Text = a.AnswerOption,
                        AttatchImgUrl = a.AnswerFileUrl
                    });
                });
                questionDTO.Options = optionList;
            }
            else
            {
                //复杂题型
                //1.添加附件信息
                var fileList = new List<QuestionFileNew>();
                var fileIds = question.TopicFileIds;
                if (!string.IsNullOrEmpty(fileIds))
                {
                    var fileIdArr = fileIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (fileIdArr.Any())
                    {
                        var files = await DbContext.FreeSql.GetRepository<YssxTopicFileNew>().Where(a => fileIdArr.Contains(a.Id)).Select(a => new QuestionFileNew() { Name = a.Name, Url = a.Url, Sort = a.Sort }).ToListAsync($"{CommonConstants.Cache_GetTopicFilesByTopicId}{questionId}", true, 10 * 60);
                        questionDTO.QuestionFile = files;
                    }
                }
                //2.添加子题目
                if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                {
                    var subQuestions = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.ParentId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId}{questionId}", true, 10 * 60, true, false).Result
                        .Select(s =>
                        new ExamQuestionInfoNew()
                        {
                            QuestionId = s.Id,
                            QuestionType = s.QuestionType,
                            QuestionContentType = s.QuestionContentType,
                            CalculationType = s.CalculationType,
                            Hint = s.Hint,
                            Content = s.Content,
                            TopicContent = s.TopicContent,
                            FullContent = s.FullContent,
                            AnswerValue = s.AnswerValue,
                            Score = s.Score,
                            Sort = s.Sort,
                            IsCopy = s.IsCopy,
                            IsCopyBigData = s.IsCopyBigData,
                            IsDisorder = s.IsDisorder
                        }).ToList();

                    var detailList = new List<ExamStudentGradeDetailNew>();
                    if (gradeId > 0)
                    {
                        detailList = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(s => s.GradeId == gradeId && s.ParentQuestionId == questionId)
                            .ToListAsync(s => new ExamStudentGradeDetailNew()
                            {
                                Answer = s.Answer,
                                AnswerCompareInfo = s.AnswerCompareInfo,
                                AccountEntryStatus = s.AccountEntryStatus,
                                QuestionId = s.QuestionId
                            });
                    }

                    if (subQuestions.Any() && detailList.Any())
                    {
                        subQuestions.ForEach(s =>
                        {
                            var detail = detailList.FirstOrDefault(a => a.QuestionId == s.QuestionId);
                            if (detail != null && detail.QuestionId > 0)
                            {
                                s.StudentAnswer = detail.Answer;
                                s.AnswerCompareInfo = detail.AnswerCompareInfo;
                                s.AccountEntryStatus = detail.AccountEntryStatus;
                            }
                        });
                    }

                    var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                    || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge).Select(a => a.QuestionId).ToArray();

                    if (choiceQuestionIds.Any())
                    {
                        var optionAll = await DbContext.FreeSql.GetRepository<YssxAnswerOptionNew>().Where(a => choiceQuestionIds.Contains(a.TopicId)).ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId}{questionId}", true, 10 * 60);
                        subQuestions.ForEach(a =>
                        {
                            var options = optionAll.Where(c => c.TopicId == a.QuestionId).Select(d => new ChoiceOptionNew()
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Text = d.AnswerOption,
                                AttatchImgUrl = d.AnswerFileUrl
                            });
                            if (options.Any())
                            {
                                a.Options = options.ToList();
                            }
                        });
                    }

                    if (subQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                    {
                        subQuestions.ForEach(s =>
                        {
                            if (s.QuestionType == QuestionType.AccountEntry)
                            {
                                s.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>().Select.Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                                .Select(a => new CertificateTopicViewNew())
                                .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{s.QuestionId}", true, 10 * 60);
                            }
                        });
                    }

                    questionDTO.SubQuestion = subQuestions;
                }
                else if (question.QuestionType == QuestionType.AccountEntry)//分录题
                {
                    questionDTO.CertificateTopic = await DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .Select(s => new CertificateTopicViewNew())
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{questionId}", true, 10 * 60);
                }
            }

            #region 附上答题信息及答案显示

            var gradeDetail = new ExamStudentGradeDetailNew();
            if (gradeId > 0)
            {
                gradeDetail = DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(a => a.QuestionId == questionId && a.GradeId == gradeId)
                    .First(a => new ExamStudentGradeDetailNew { Status = a.Status, Answer = a.Answer, AnswerCompareInfo = a.AnswerCompareInfo, AccountEntryStatus = a.AccountEntryStatus });
            }

            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || (isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition) || isView)
            {
                if (gradeDetail != null)
                {
                    if ((isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition))//大数据岗位要隐藏答案对比信息和作答状态（如果有值，前端会展示答题状态）
                    {
                        if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
                        {
                            questionDTO.SubQuestion.ForEach(a =>
                            {
                                a.AnswerCompareInfo = null;
                            });
                        }
                    }
                    else
                    {
                        if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
                        {
                            var subGradeDetails = DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(a => a.GradeId == gradeId && a.QuestionId != a.ParentQuestionId && a.ParentQuestionId == questionId)
                            .ToList(a => new { a.QuestionId, a.Status });
                            questionDTO.SubQuestion.ForEach(a =>
                            {
                                var subGradeDetail = subGradeDetails.Where(s => s.QuestionId == a.QuestionId).FirstOrDefault();
                                a.AnswerResultStatus = subGradeDetail != null ? subGradeDetail.Status : AnswerResultStatus.None;
                            });
                        }
                        questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
                        questionDTO.AnswerResultStatus = gradeDetail.Status;
                    }
                }
            }
            else
            {
                questionDTO.Hint = null;
                questionDTO.FullContent = null;
                questionDTO.AnswerValue = null;
                questionDTO.AnswerCompareInfo = null;

                if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Count > 0)
                {
                    questionDTO.SubQuestion.ForEach(a =>
                    {
                        a.Hint = null;
                        a.FullContent = null;
                        a.AnswerValue = null;
                        a.AnswerCompareInfo = null;
                    });
                }

                questionDTO.AnswerResultStatus = gradeDetail == null ? AnswerResultStatus.None : AnswerResultStatus.Pending;
            }
            questionDTO.AccountEntryStatus = gradeDetail == null ? AccountEntryStatus.None : gradeDetail.AccountEntryStatus;
            questionDTO.StudentAnswer = gradeDetail?.Answer;

            #endregion

            questionDTO.IsSettled = grade.IsSettled;
            result.Data = questionDTO;
            result.Msg = "操作成功";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        #endregion

        #region 提交考卷 - 私有方法

        /// <summary>
        /// 提交考卷--自主练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfoNew>> SubmitPracticeExam(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }

        ///// <summary>
        ///// 提交考卷--真题模拟
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <returns></returns>
        //private async Task<ResponseContext<GradeInfoNew>> SubmitEmulationExam(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user, ExamPaperGroupStudentNew studentGrade = null, bool isAutoSubmit = false)
        //{
        //    return await SubmitExamBase(exam, grade, user, studentGrade);
        //}

        ///// <summary>
        ///// 提交考卷--校内赛
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <returns></returns>
        //private async Task<ResponseContext<GradeInfoNew>> SubmitSchoolExam(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user, ExamPaperGroupStudentNew studentGrade = null, bool isAutoSubmit = false)
        //{
        //    return await SubmitExamBase(exam, grade, user, studentGrade, isAutoSubmit);
        //}

        ///// <summary>
        ///// 提交考卷--省内赛
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <returns></returns>
        //private async Task<ResponseContext<GradeInfoNew>> SubmitProvinceExam(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user, ExamPaperGroupStudentNew studentGrade = null, bool isAutoSubmit = false)
        //{
        //    return await SubmitExamBase(exam, grade, user, studentGrade, isAutoSubmit);
        //}

        /// <summary>
        /// 提交考卷基础函数
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfoNew>> SubmitExamBase(ExamPaperNew exam, ExamStudentGradeNew grade, UserTicket user, ExamPaperGroupStudentNew studentGrade = null, bool isAutoSubmit = false)
        {
            var result = new ResponseContext<GradeInfoNew>();
            try
            {
                //提交考卷
                if (studentGrade != null && studentGrade.Id > 0)//团队赛
                {
                    await SubmitExamGroupGrade(exam, studentGrade, isAutoSubmit);
                    result.Data = new GradeInfoNew()
                    {
                        GradeId = studentGrade.GradeId,
                        ExamScore = exam.TotalScore,
                        GradeScore = studentGrade.Score,
                        UsedSeconds = (long)studentGrade.UsedSeconds,
                        CorrectCount = studentGrade.CorrectCount,
                        ErrorCount = studentGrade.ErrorCount,
                        PartRightCount = studentGrade.PartRightCount,
                        BlankCount = studentGrade.BlankCount,
                        GroupStatus = studentGrade.Status,
                        StudentExamStatus = studentGrade.StudentExamStatus,
                    };
                }
                else
                {
                    SubmitExamGrade(exam, grade, isAutoSubmit);

                    result.Data = new GradeInfoNew()
                    {
                        GradeId = grade.Id,
                        ExamScore = exam.TotalScore,
                        GradeScore = grade.Score,
                        UsedSeconds = (long)grade.UsedSeconds,
                        CorrectCount = grade.CorrectCount,
                        ErrorCount = grade.ErrorCount,
                        PartRightCount = grade.PartRightCount,
                        BlankCount = grade.BlankCount,
                        IsPass = grade.Score > exam.PassScore,
                        StudentExamStatus = grade.Status,
                    };
                }
            }
            catch (Exception ex)
            {
                result.Msg = "提交考卷异常，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"提交考卷异常:gradeId-{grade.Id},UserId:{user.Id}";
                CommonLogger.Error(msg, ex);
            }

            if (result.Code == CommonConstants.SuccessCode)
                result.Msg = "提交成功";
            return result;
        }

        /// <summary>
        /// 提交组员考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="studentGrade"></param>
        /// <param name="isAutoSubmit"></param>
        /// <param name="isBatchSubmit">是否批量提交</param>
        private async Task SubmitExamGroupGrade(ExamPaperNew exam, ExamPaperGroupStudentNew studentGrade, bool isAutoSubmit = false, bool isBatchSubmit = false)
        {
            studentGrade.IsManualSubmit = !isAutoSubmit;
            studentGrade.StudentExamStatus = StudentExamStatus.End;
            studentGrade.UpdateTime = DateTime.Now;
            studentGrade.SubmitTime = DateTime.Now;
            studentGrade.LeftSeconds = GetLeftSeconds(studentGrade.LeftSeconds, studentGrade.RecordTime);
            studentGrade.UsedSeconds = studentGrade.TotalMinutes * 60 - studentGrade.LeftSeconds;

            var studentTotalQuestionCount = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPositionTopicCountByCaseId}{exam.CaseId}", () => DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.CaseId == exam.CaseId && s.PositionId == studentGrade.PostionId && s.ParentId == 0 && s.IsDelete == CommonConstants.IsNotDelete).Count().ToString(), 10 * 60, false);

            //Done:计算考卷分数，返回考试成绩信息
            var studentAnswerCount = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(a => a.GradeId == studentGrade.GradeId && a.PostionId == studentGrade.PostionId && a.QuestionId == a.ParentQuestionId)
                .ToListAsync(a => new { a.QuestionId, a.Status, a.Score });
            studentGrade.CorrectCount = studentAnswerCount.Count(a => a.Status == AnswerResultStatus.Right);
            studentGrade.ErrorCount = studentAnswerCount.Count(a => a.Status == AnswerResultStatus.Error);
            studentGrade.PartRightCount = studentAnswerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            studentGrade.BlankCount = int.Parse(studentTotalQuestionCount) - studentAnswerCount.Count;

            if (studentGrade.BlankCount <= 0)
                studentGrade.BlankCount = 0;
            studentGrade.Score = studentAnswerCount.Sum(a => a.Score);

            if (!isBatchSubmit)//非批量提交
            {
                //加组锁
                RedisLock.WaitLock(studentGrade.GroupId.ToString(), () =>
                {
                    var submitCount = DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(s => s.GroupId == studentGrade.GroupId && s.StudentExamStatus == StudentExamStatus.End && s.UserId != studentGrade.UserId).CountAsync().Result;

                    var grade = new ExamStudentGradeNew();

                    if (submitCount == 3)
                    {
                        grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == studentGrade.GradeId).First();
                    }

                    DbContext.FreeSql.Transaction(() =>
                    {
                        DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Update(studentGrade);

                        if (submitCount == 3)//成员都已交卷，修改主作答记录状态 
                        {
                            studentGrade.Status = GroupStatus.End;

                            DbContext.FreeSql.GetRepository<ExamPaperGroupNew>().UpdateDiy
                            .UpdateColumns(s => s.Status)
                            .Set(s => s.Status, GroupStatus.End).Where(s => s.Id == studentGrade.GroupId).ExecuteAffrows();

                            DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy
                            .UpdateColumns(s => s.Status)
                            .Set(s => s.Status, GroupStatus.End).Where(s => s.GroupId == studentGrade.GroupId).ExecuteAffrows();

                            SubmitExamGrade(exam, grade, isAutoSubmit);

                            RedisHelper.Del($"{CommonConstants.ImMessagePre}{studentGrade.GroupId.ToString()}");

                            ImHelper.SendChanMessage(Guid.Empty, studentGrade.GroupId.ToString(), "{\"messageType\":\"examEnded\"}");
                        }
                    });

                    if (submitCount < 3)
                    {
                        ImHelper.SendChanMessage(Guid.Empty, studentGrade.GroupId.ToString(), "{\"messageType\":\"examSubmit\",\"content\":{\"positionId\":" + studentGrade.PostionId + "}}");
                    }

                }, timeoutSeconds: 3, true);
            }

            if (studentGrade.PostionId == CommonConstants.BigDataPosition)
            {
                await RedisHelper.SetAsync($"{CommonConstants.BigDataPositionStatus}{studentGrade.GroupId}", 1, 18000);
            }
        }

        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isAutoSubmit">是否自动提交</param>
        /// <param name="isBatchSubmit">是否批量提交</param>
        private void SubmitExamGrade(ExamPaperNew exam, ExamStudentGradeNew grade, bool isAutoSubmit = false, bool isBatchSubmit = false)
        {
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isAutoSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.CreateTime).TotalSeconds;
                    break;

                    //case ExamType.EmulationTest:
                    //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                    //    {
                    //        grade.LeftSeconds = GetLeftSeconds(grade.LeftSeconds, grade.RecordTime);
                    //        grade.UsedSeconds = exam.TotalMinutes * 60 - grade.LeftSeconds;
                    //    }
                    //    break;

                    //case ExamType.SchoolCompetition:
                    //case ExamType.ProvincialCompetition:
                    //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                    //    {
                    //        grade.LeftSeconds = GetLeftSeconds(exam);
                    //        grade.UsedSeconds = exam.TotalMinutes * 60 - grade.LeftSeconds;
                    //    }
                    //    break;
            }

            var totalQuestionCount = exam.TotalQuestionCount;

            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(a => a.GradeId == grade.Id && a.QuestionId == a.ParentQuestionId).Master()
                .ToList(a => new { a.QuestionId, a.Status, a.Score });
            grade.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
            grade.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
            grade.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            grade.BlankCount = totalQuestionCount - answerCount.Count;

            if (grade.BlankCount <= 0)
                grade.BlankCount = 0;
            grade.Score = answerCount.Sum(a => a.Score);

            if (!isBatchSubmit)//非批量提交
            {
                DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Update(grade);
            }

        }

        #endregion

        #region 操作

        /// <summary>
        /// 自主练习清空作答记录
        /// </summary>
        /// <param name="examId">examId>0，表示只清空当前测试的记录，否则清空所有测试记录</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ClearExamGradeInfo(UserTicket userTicket, long examId = 0)
        {
            var result = new ResponseContext<bool>(true);

            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            List<long> examIdList = new List<long>();
            if (examId > 0)
            {
                if (DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.Any(s => s.Id == examId && s.ExamType == ExamType.PracticeTest && s.TenantId == tenantId))
                {
                    examIdList.Add(examId);
                }
            }
            else
            {
                examIdList = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.Where(s => s.ExamType == ExamType.PracticeTest && s.TenantId == tenantId).ToListAsync(s => s.Id);
            }

            if (examIdList.Any())
            {
                var idArr = examIdList.ToArray();
                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Delete(s => idArr.Contains(s.ExamId) && s.UserId == userId && s.TenantId == tenantId);
                    DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Delete(s => idArr.Contains(s.ExamId) && s.UserId == userId && s.TenantId == tenantId);
                    DbContext.FreeSql.GetRepository<ExamStudentLastGradeNew>().Delete(s => idArr.Contains(s.ExamId) && s.UserId == userId && s.TenantId == tenantId);
                });
            }
            else
            {
                result.Msg = $"没找到相应的记录 ";
                result.Code = CommonConstants.NotFund;
                return result;
            }

            return result;
        }

        /// <summary>
        /// 自主练习清空单条作答记录
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ClearGradeInfo(long gradeId, UserTicket userTicket)
        {
            var result = new ResponseContext<bool>(true);

            if (gradeId == 0)
            {
                result.Msg = $"gardeId不能为0  ";
                result.Code = CommonConstants.BadRequest;
                return result;
            }

            var userId = userTicket.Id;
            var tenantId = userTicket.TenantId;

            var grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.UserId == userId && s.TenantId == tenantId).FirstAsync(s => new { s.Id, s.ExamId });
            if (grade != null && grade.Id > 0)
            {
                if (DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.Any(s => s.Id == grade.ExamId && s.ExamType == ExamType.PracticeTest))
                {
                    DbContext.FreeSql.Transaction(() =>
                    {
                        DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Delete(s => s.GradeId == grade.Id);
                        DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Delete(s => s.Id == grade.Id);
                        DbContext.FreeSql.GetRepository<ExamStudentLastGradeNew>().Delete(s => s.ExamId == grade.ExamId && s.UserId == userId);
                    });
                }
            }
            else
            {
                result.Msg = $"没找到相应的记录 ";
                result.Code = CommonConstants.NotFund;
                return result;
            }

            return result;
        }

        /// <summary>
        /// 分录题退回制单(已禁用 )
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<AccountEntryStatus>> AccountEntryCancel(long gradeId, long questionId, UserTicket userTicket)
        {
            var result = new ResponseContext<AccountEntryStatus>();

            #region 校验

            if (gradeId == 0 || questionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "所有参数都不能为0";
                return result;
            }

            var grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = "考试已结束";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetTopicById}{questionId}", true, 10 * 60, true);

            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"题目不存在";
                return result;
            }

            if (question.QuestionType != QuestionType.AccountEntry)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"只有分录题才能退回制单";
                return result;
            }

            if (exam.CaseId != question.CaseId)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"题目和试卷信息不匹配";
                return result;
            }

            var user = userTicket;

            var groupStudent = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.Where(s => s.GradeId == grade.Id && s.UserId == user.Id).FirstAsync(s => new ExamPaperGroupStudentViewNew());
            if (groupStudent == null || groupStudent.GroupId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"考试信息不存在";
                return result;
            }

            if (groupStudent.StudentExamStatus == StudentExamStatus.End)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"您已交卷";
                return result;
            }

            #endregion

            var detail = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(s => s.GradeId == grade.Id && s.QuestionId == question.Id).FirstAsync(s => new { s.Id, s.AccountEntryStatus });
            if (detail == null || detail.Id == 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"答题记录不存在";
                return result;
            }

            var accountEntryStatus = detail.AccountEntryStatus;
            var isValid = true;
            switch (accountEntryStatus)
            {
                case AccountEntryStatus.None:
                    isValid = false;
                    break;

                case AccountEntryStatus.Cashier:
                    isValid = (groupStudent.PostionId == CommonConstants.Cashier);

                    break;

                case AccountEntryStatus.AccountingManager:
                    isValid = (groupStudent.PostionId == CommonConstants.AccountingManager || groupStudent.PostionId == CommonConstants.FinanceManager);

                    break;

                case AccountEntryStatus.Audit:
                    isValid = false;
                    break;

                default:
                    isValid = false;
                    break;

            }
            if (!isValid)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"没有权限作答该题目！";
                return result;
            }

            accountEntryStatus = AccountEntryStatus.None;

            await DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().UpdateDiy
                .UpdateColumns(s => s.AccountEntryStatus)
                .Set(s => s.AccountEntryStatus, accountEntryStatus).Where(s => s.Id == detail.Id).ExecuteAffrowsAsync();

            result.Data = accountEntryStatus;
            return result;
        }

        #endregion

        #region 答题相关接口

        #region 获取试卷基本信息

        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperBasicInfoNew>> GetExamPaperBasicInfo(ExamPaperBasicInfoRequestDto query, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperBasicInfoNew>();
            var examId = query.ExamId;
            var gradeId = query.GradeId;
            if (examId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "examId不能为0";
                return result;
            }

            //查询试卷信息
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>()
                .Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById}{examId}", true, 10 * 60);

            if (exam != null)//试卷存在
            {
                switch (exam.ExamType)
                {
                    case ExamType.PracticeTest://自主练习

                        return await GetPracticeExamPaperBasicInfo(exam, query, userTicket);

                    //case ExamType.EmulationTest:

                    //    return await GetEmulationExamPaperBasicInfo(exam, gradeId, userTicket);

                    //case ExamType.SchoolCompetition:

                    //    if (exam.CompetitionType == CompetitionType.TeamCompetition)
                    //    {
                    //        if (gradeId == 0)
                    //        {
                    //            result.Code = CommonConstants.BadRequest;
                    //            result.Msg = "gradeId不能为0";
                    //            return result;
                    //        }
                    //    }

                    //    return await GetSchoolExamPaperBasicInfo(exam, gradeId, userTicket);

                    //case ExamType.ProvincialCompetition:

                    //    if (gradeId == 0)
                    //    {
                    //        result.Code = CommonConstants.BadRequest;
                    //        result.Msg = "gradeId不能为0";
                    //        return result;
                    //    }

                    //    return await GetProvinceExamPaperBasicInfo(exam, gradeId, userTicket);

                    default:

                        throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
                }
            }

            result.Code = CommonConstants.NotFund;
            result.Msg = $"考试信息不存在 ";

            return result;
        }

        /// <summary>
        /// 获取自主练习试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperBasicInfoNew>> GetPracticeExamPaperBasicInfo(ExamPaperNew exam, ExamPaperBasicInfoRequestDto query, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperBasicInfoNew>();

            var grade = new ExamStudentGradeNew();

            var user = userTicket;
            var gradeId = query.GradeId;
            //if (gradeId == 0 && exam.TaskId > 0)
            //{
            //    result.Code = CommonConstants.NotFund;
            //    result.Msg = $"答题记录不存在 ";
            //    return result;
            //}
            if (gradeId > 0)//作答记录Id等于0 ，用户已经有作答记录
            {
                grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>()
                    .Where(s => s.Id == gradeId && s.UserId == user.Id)
                    .FirstAsync();

                if (grade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"答题记录不存在 ";
                    return result;
                }

                if (grade.Status == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"本次测试已经结束了！";
                    return result;
                }
            }
            else//没有作答记录，生成作答记录信息
            {
                //加用户锁
                var gradeResult = RedisLock.WaitLock(user.Id.ToString(), () =>
                {
                    return InsertGrade(exam, new ExamPaperGroupStudentDtoNew() { UserId = user.Id, TenantId = user.TenantId });
                });

                if (gradeResult.Code == CommonConstants.SuccessCode)
                {
                    grade = gradeResult.Data;
                }
                else
                {
                    result.Code = gradeResult.Code;
                    result.Msg = gradeResult.Msg;
                    return result;
                }
            }

            return await GetExamPaperBasicInfoBase(exam, grade, query);
        }

        ///// <summary>
        ///// 获取真题模拟试卷信息（包括试卷基本信息和试卷包含的题目信息）
        ///// </summary>
        ///// <returns></returns>
        //private async Task<ResponseContext<ExamPaperBasicInfoNew>> GetEmulationExamPaperBasicInfo(ExamPaperNew exam, long gradeId, UserTicket userTicket)
        //{
        //    var result = new ResponseContext<ExamPaperBasicInfoNew>();

        //    var user = userTicket;

        //    var grade = new ExamStudentGradeNew();

        //    if (gradeId > 0)
        //    {
        //        if (exam.CompetitionType == CompetitionType.IndividualCompetition)
        //        {
        //            grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.UserId == user.Id).FirstAsync();
        //        }
        //        else
        //        {
        //            grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Select.InnerJoin<ExamPaperGroupStudentNew>((a, b) => a.Id == b.GradeId && b.UserId == user.Id).Where(s => s.Id == gradeId).FirstAsync();
        //        }

        //        if (grade == null || grade.Id == 0)
        //        {
        //            result.Code = CommonConstants.NotFund;
        //            result.Msg = $"答题记录不存在 ";
        //            return result;
        //        }

        //        if (grade.ExamId != exam.Id)
        //        {
        //            result.Code = CommonConstants.BadRequest;
        //            result.Msg = $"考试信息和作答纪录不匹配 ";
        //            return result;
        //        }
        //    }
        //    else
        //    {
        //        if (exam.CompetitionType == CompetitionType.TeamCompetition)//团队赛
        //        {
        //            //查询当前考试是否有正在进行或者准备开始的团队赛
        //            var groupStudent = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select
        //                .Where(s => s.ExamId == exam.Id && s.UserId == user.Id && (s.Status == GroupStatus.AllReady || s.Status == GroupStatus.Complete))
        //                .FirstAsync(s => new ExamPaperGroupStudentNew() { GroupId = s.GroupId, GradeId = s.GradeId, Status = s.Status });

        //            if (groupStudent == null)
        //            {
        //                result.Code = CommonConstants.ErrorCode;
        //                result.Msg = $"没有有效的分组信息 ";
        //                goto End;
        //            }

        //            var groupId = groupStudent.GroupId;
        //            if (groupId > 0)
        //            {
        //                gradeId = groupStudent.GradeId;
        //                if (gradeId > 0)
        //                {
        //                    grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Select.Where(s => s.Id == gradeId).FirstAsync();
        //                }
        //                else
        //                {
        //                    //加组锁,新增答题记录
        //                    RedisLock.WaitLock(groupId.ToString(), () =>
        //                    {
        //                        var groupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.From<YssxCaseNew, YssxCasePositionNew>((a, b, c) => a
        //                        .InnerJoin(aa => aa.CaseId == b.Id)
        //                        .LeftJoin(aa => aa.CaseId == c.CaseId && aa.PostionId == c.PostionId && b.IsDelete == CommonConstants.IsNotDelete))
        //                        .Where((a, b, c) => a.GroupId == groupId)
        //                        .ToList((a, b, c) =>
        //                            new ExamPaperGroupStudentDtoNew
        //                            {
        //                                Id = a.Id,
        //                                GroupId = a.GroupId,
        //                                PostionId = a.PostionId,
        //                                UserId = a.UserId,
        //                                TenantId = a.TenantId,
        //                                GradeId = a.GradeId,
        //                                Status = a.Status,
        //                                TotalTime = b.TotalTime,
        //                                TimeMinutes = c.TimeMinutes
        //                            }
        //                        );

        //                        if (groupStudentList.Any())
        //                        {
        //                            switch (groupStudentList[0].Status)
        //                            {
        //                                case GroupStatus.Started://组员未满，继续返回组队
        //                                    result.Code = CommonConstants.ErrorCode;
        //                                    result.Msg = $"组员未满 ";
        //                                    break;

        //                                case GroupStatus.Full://组员未准备完毕，继续返回组队
        //                                    result.Code = CommonConstants.ErrorCode;
        //                                    result.Msg = $"组员未准备完毕 ";
        //                                    break;

        //                                case GroupStatus.AllReady://新增答题记录
        //                                    var gradeResult = InsertGrade(exam, groupStudentList.ToArray());
        //                                    result.Code = gradeResult.Code;
        //                                    if (gradeResult.Code == CommonConstants.SuccessCode)
        //                                    {
        //                                        grade = gradeResult.Data;
        //                                    }

        //                                    break;
        //                                case GroupStatus.Complete:
        //                                case GroupStatus.End:
        //                                    grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Select.Where(s => s.Id == groupStudentList[0].GradeId).First();
        //                                    break;
        //                            }
        //                        }
        //                    });
        //                }
        //            }
        //        }
        //        else//个人赛
        //        {
        //            //加用户锁
        //            RedisLock.WaitLock(user.Id.ToString(), () =>
        //            {
        //                grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Select.InnerJoin<ExamStudentLastGradeNew>((a, b) => a.Id == b.GradeId)
        //               .Where(s => s.ExamId == exam.Id && s.UserId == user.Id && s.Status != StudentExamStatus.End)
        //               .First();//获取最近的一次未结束的考试

        //                if (grade == null)//没有未结束的考试，新增答题记录，并更新gradeId到ExamStudentLastGradeNew
        //                {
        //                    var gradeResult = InsertGrade(exam, new ExamPaperGroupStudentDtoNew() { UserId = user.Id, TenantId = user.TenantId });
        //                    result.Code = gradeResult.Code;
        //                    if (gradeResult.Code == CommonConstants.SuccessCode)
        //                    {
        //                        grade = gradeResult.Data;
        //                    }
        //                }
        //            });
        //        }
        //    }

        //    End:

        //    if (result.Code == CommonConstants.ErrorCode)
        //    {
        //        return result;
        //    }

        //    if (grade == null || grade.Id == 0)
        //    {
        //        result.Code = CommonConstants.NotFund;
        //        result.Msg = $"答题记录不存在 ";
        //        return result;
        //    }

        //    if (grade.Status == StudentExamStatus.End)
        //    {
        //        result.Code = CommonConstants.ErrorCode;
        //        result.Msg = $"您已经提交过考卷了！";
        //        return result;
        //    }

        //    var basicInfo = await GetExamPaperBasicInfoBase(exam, grade, null);

        //    if (exam.CompetitionType == CompetitionType.TeamCompetition && basicInfo.Data.StudentExamStatus != StudentExamStatus.End)//团队赛且当前开始状态未结束
        //    {
        //        var groupStudentList = await GetExamGroupStudentInfos(exam, grade, basicInfo.Data.QuestionInfoList, user);//用户分组信息

        //        basicInfo.Data.ExamPaperGroupStudents = groupStudentList;

        //        var groupStudent = groupStudentList.FirstOrDefault(s => s.IsSelf == true);

        //        basicInfo.Data.LeftSeconds = (long)groupStudent.LeftSeconds;
        //        basicInfo.Data.UsedSeconds = (long)groupStudent.UsedSeconds;

        //        var positionId = groupStudent.PostionId;

        //        basicInfo.Data.QuestionInfoList.ForEach(s =>
        //        {
        //            s.IsOwner = s.PositionId == positionId;
        //        });
        //    }

        //    return basicInfo;
        //}

        /// <summary>
        /// 获取校内赛试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        //private async Task<ResponseContext<ExamPaperBasicInfoNew>> GetSchoolExamPaperBasicInfo(ExamPaperNew exam, long gradeId, UserTicket userTicket)
        //{
        //    var result = new ResponseContext<ExamPaperBasicInfoNew>();

        //    if (exam.BeginTime > DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试未开始 ";
        //        return result;
        //    }
        //    if (exam.EndTime < DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试已结束";
        //        return result;
        //    }

        //    var user = userTicket;

        //    var grade = new ExamStudentGradeNew();

        //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
        //    {
        //        RedisLock.WaitLock(user.Id.ToString(), () =>
        //        {
        //            if (gradeId == 0)
        //            {
        //                grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.ExamId == exam.Id && s.UserId == user.Id).First();

        //                if (grade == null)
        //                {
        //                    var gradeResult = InsertGrade(exam, new ExamPaperGroupStudentDtoNew() { UserId = user.Id, TenantId = user.TenantId });
        //                    if (gradeResult.Code == CommonConstants.SuccessCode)
        //                    {
        //                        grade = gradeResult.Data;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.UserId == user.Id).First();

        //                if (grade != null && grade.Status == StudentExamStatus.Wait)
        //                {
        //                    if (grade.ExamId != exam.Id)
        //                    {
        //                        result.Code = CommonConstants.BadRequest;
        //                        result.Msg = $"考试信息和作答纪录不匹配 ";
        //                    }
        //                    else
        //                    {
        //                        DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy.SetSource(grade)
        //                        .UpdateColumns(s => s.Status)
        //                        .Set(s => s.Status, StudentExamStatus.Started).ExecuteAffrows();
        //                    }
        //                }
        //            }
        //        }, timeoutSeconds: 3, true);
        //    }
        //    else
        //    {
        //        grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId).FirstAsync();

        //        if (grade != null)
        //        {
        //            if (grade.ExamId != exam.Id)
        //            {
        //                result.Code = CommonConstants.BadRequest;
        //                result.Msg = $"考试信息和作答纪录不匹配 ";
        //            }

        //            if (grade.Status == StudentExamStatus.Wait)
        //            {
        //                //加组锁
        //                RedisLock.WaitLock(gradeId.ToString(), () =>
        //                {
        //                    var groupStudentList = DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Select.From<YssxCaseNew, YssxCasePositionNew>((a, b, c) => a
        //                        .InnerJoin(aa => aa.CaseId == b.Id)
        //                        .LeftJoin(aa => aa.CaseId == c.CaseId && aa.PostionId == c.PostionId && b.IsDelete == CommonConstants.IsNotDelete))
        //                        .Where((a, b, c) => a.GroupId == grade.GroupId)
        //                        .ToList((a, b, c) =>
        //                            new ExamPaperGroupStudentDtoNew
        //                            {
        //                                Id = a.Id,
        //                                GroupId = a.GroupId,
        //                                PostionId = a.PostionId,
        //                                UserId = a.UserId,
        //                                TenantId = a.TenantId,
        //                                GradeId = a.GradeId,
        //                                Status = a.Status,
        //                                TotalTime = b.TotalTime,
        //                                TimeMinutes = c.TimeMinutes
        //                            }
        //                        );

        //                    if (groupStudentList != null && groupStudentList.Any())
        //                    {
        //                        switch (groupStudentList[0].Status)
        //                        {
        //                            case GroupStatus.Started://组员未满，继续返回组队
        //                                result.Code = CommonConstants.ErrorCode;
        //                                result.Msg = $"组员未满 ";
        //                                break;

        //                            case GroupStatus.Full://组员已满，未准备完毕
        //                                result.Code = CommonConstants.ErrorCode;
        //                                result.Msg = $"组员未准备完毕 ";
        //                                break;

        //                            case GroupStatus.AllReady://准备完毕，开始考试
        //                                DbContext.FreeSql.Transaction(() =>
        //                                {
        //                                    groupStudentList.ForEach(s =>
        //                                    {
        //                                        s.TotalTime = s.TimeMinutes > 0 ? s.TimeMinutes : s.TotalTime;
        //                                        var leftSeconds = s.TotalTime * 60d;
        //                                        DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy
        //                                        .UpdateColumns(a => new { a.Status, a.GradeId, a.StudentExamStatus, a.TotalMinutes, a.LeftSeconds, a.RecordTime })
        //                                        .Set(a => a.Status == GroupStatus.Complete)
        //                                        .Set(a => a.GradeId == gradeId)
        //                                        .Set(a => a.StudentExamStatus == StudentExamStatus.Started)
        //                                        .Set(a => a.TotalMinutes == s.TotalTime)
        //                                        .Set(a => a.LeftSeconds == leftSeconds)
        //                                        .Set(a => a.RecordTime == exam.BeginTime)
        //                                        .Where(a => a.Id == s.Id).ExecuteAffrows();
        //                                    });

        //                                    DbContext.FreeSql.GetRepository<ExamPaperGroupNew>().UpdateDiy.UpdateColumns(s => s.Status).Set(s => s.Status, GroupStatus.Complete).Where(s => s.Id == grade.GroupId).ExecuteAffrows();

        //                                    grade.Status = StudentExamStatus.Started;
        //                                    DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy.SetSource(grade).UpdateColumns(s => s.Status).ExecuteAffrows();
        //                                });

        //                                break;
        //                            case GroupStatus.Complete:
        //                            case GroupStatus.End:
        //                                break;
        //                        }
        //                    }
        //                }, timeoutSeconds: 5, autoDelay: true);
        //            }
        //        }
        //    }

        //    if (result.Code != CommonConstants.SuccessCode)
        //    {
        //        return result;
        //    }

        //    if (grade == null || grade.Id == 0)
        //    {
        //        result.Code = CommonConstants.NotFund;
        //        result.Msg = $"答题记录不存在 ";
        //        return result;
        //    }

        //    if (grade.Status == StudentExamStatus.End)
        //    {
        //        result.Code = CommonConstants.ErrorCode;
        //        result.Msg = $"考试已结束！";
        //        return result;
        //    }

        //    var basicInfo = await GetExamPaperBasicInfoBase(exam, grade, null);

        //    if (exam.CompetitionType == CompetitionType.TeamCompetition && basicInfo.Data.StudentExamStatus != StudentExamStatus.End)//团队赛且当前开始状态未结束
        //    {
        //        var groupStudentList = await GetExamGroupStudentInfos(exam, grade, basicInfo.Data.QuestionInfoList, user);//用户分组信息

        //        basicInfo.Data.ExamPaperGroupStudents = groupStudentList;

        //        var groupStudent = groupStudentList.FirstOrDefault(s => s.IsSelf == true);

        //        basicInfo.Data.LeftSeconds = (long)groupStudent.LeftSeconds;
        //        basicInfo.Data.UsedSeconds = (long)groupStudent.UsedSeconds;

        //        var positionId = groupStudent.PostionId;

        //        basicInfo.Data.QuestionInfoList.ForEach(s =>
        //        {
        //            s.IsOwner = s.PositionId == positionId;
        //        });
        //    }

        //    return basicInfo;
        //}

        /// <summary>
        /// 获取省内赛试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        //private async Task<ResponseContext<ExamPaperBasicInfoNew>> GetProvinceExamPaperBasicInfo(ExamPaperNew exam, long gradeId, UserTicket userTicket)
        //{
        //    var result = new ResponseContext<ExamPaperBasicInfoNew>();

        //    if (exam.BeginTime > DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试未开始 ";
        //        return result;
        //    }
        //    if (exam.EndTime < DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试已结束";
        //        return result;
        //    }

        //    var user = userTicket;

        //    var grade = new ExamStudentGradeNew();

        //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
        //    {
        //        RedisLock.WaitLock(gradeId.ToString(), () =>
        //        {
        //            grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.UserId == user.Id).First();

        //            if (grade != null && grade.Status == StudentExamStatus.Wait)
        //            {
        //                if (grade.ExamId != exam.Id)
        //                {
        //                    result.Code = CommonConstants.BadRequest;
        //                    result.Msg = $"考试信息和作答纪录不匹配 ";
        //                }
        //                else
        //                {
        //                    DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy.SetSource(grade)
        //                    .UpdateColumns(s => s.Status)
        //                    .Set(s => s.Status, StudentExamStatus.Started).ExecuteAffrows();
        //                }
        //            }
        //        }, timeoutSeconds: 3, true);
        //    }
        //    else
        //    {
        //        grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Select.InnerJoin<ExamPaperGroupStudentNew>((a, b) => a.Id == b.GradeId && b.UserId == user.Id).Where(s => s.Id == gradeId).FirstAsync();

        //        if (grade != null)
        //        {
        //            if (grade.ExamId != exam.Id)
        //            {
        //                result.Code = CommonConstants.BadRequest;
        //                result.Msg = $"考试信息和作答纪录不匹配 ";
        //            }
        //        }
        //    }

        //    if (result.Code != CommonConstants.SuccessCode)
        //    {
        //        return result;
        //    }

        //    if (grade == null || grade.Id == 0)
        //    {
        //        result.Code = CommonConstants.NotFund;
        //        result.Msg = $"答题记录不存在 ";
        //        return result;
        //    }

        //    if (grade.Status == StudentExamStatus.End)
        //    {
        //        result.Code = CommonConstants.ErrorCode;
        //        result.Msg = $"考试已结束！";
        //        return result;
        //    }

        //    var basicInfo = await GetExamPaperBasicInfoBase(exam, grade, null);

        //    if (exam.CompetitionType == CompetitionType.TeamCompetition && basicInfo.Data.StudentExamStatus != StudentExamStatus.End)//团队赛且当前开始状态未结束
        //    {
        //        var groupStudentList = await GetExamGroupStudentInfos(exam, grade, basicInfo.Data.QuestionInfoList, user);//用户分组信息

        //        basicInfo.Data.ExamPaperGroupStudents = groupStudentList;

        //        var groupStudent = groupStudentList.FirstOrDefault(s => s.IsSelf == true);

        //        if (groupStudent.StudentExamStatus == StudentExamStatus.Wait)
        //        {
        //            await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy.Set(s => s.StudentExamStatus, StudentExamStatus.Started).Where(s => s.Id == groupStudent.Id).ExecuteAffrowsAsync();
        //        }

        //        basicInfo.Data.LeftSeconds = (long)groupStudent.LeftSeconds;
        //        basicInfo.Data.UsedSeconds = (long)groupStudent.UsedSeconds;

        //        var positionId = groupStudent.PostionId;

        //        basicInfo.Data.QuestionInfoList.ForEach(s =>
        //        {
        //            s.IsOwner = s.PositionId == positionId;
        //        });
        //    }

        //    return basicInfo;
        //}

        /// <summary>
        /// 获取试卷信息基础函数（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperBasicInfoNew>> GetExamPaperBasicInfoBase(ExamPaperNew exam, ExamStudentGradeNew grade, ExamPaperBasicInfoRequestDto query)
        {
            var result = new ResponseContext<ExamPaperBasicInfoNew>();

            var gradeId = grade.Id;

            #region 考试基本信息

            var caseInfo = await DbContext.FreeSql.GetRepository<YssxCaseNew>().Select// ????????????????????????????????????????? 为什么要查询案例信息，却没有使用
                .Where(s => s.Id == exam.CaseId)
                .FirstAsync($"{CommonConstants.Cache_GetCaseById}{exam.CaseId}", true, 10 * 60, true);

            result.Data = new ExamPaperBasicInfoNew()
            {
                ExamId = exam.Id,
                //RollUpType = caseInfo.RollUpType,
                //EnterpriseInfo = caseInfo.EnterpriseInfo,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,
                StudentExamStatus = grade.Status
            };

            if (grade.Status != StudentExamStatus.End)
            {
                result.Data.QuestionInfoList = await GetExamQuestions(exam, grade, query);//获取题目信息

                if (exam.ExamType == ExamType.PracticeTest)
                {
                    result.Data.UsedSeconds = (long)(DateTime.Now - grade.CreateTime).TotalSeconds;
                }
                else
                {
                    result.Data.LeftSeconds = (long)GetLeftSeconds(grade.LeftSeconds, grade.RecordTime);
                    result.Data.UsedSeconds = (long)grade.LeftSeconds - result.Data.LeftSeconds;
                }

                result.Data.GradeId = gradeId;
            }

            #endregion

            #region 考试结束，显示考试详情

            if (grade.Status == StudentExamStatus.End)
            {
                result.Msg = "考试已结束";
                result.Data.UsedSeconds = (long)grade.UsedSeconds;
                result.Data.StudentGradeInfo = new StudentGradeInfoNew()
                {
                    GradeId = grade.Id,
                    Score = grade.Score,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score > exam.PassScore,
                    Status = grade.Status,
                };
            }

            #endregion

            result.Msg = "操作成功";
            return result;
        }

        #endregion

        #region 获取题目信息

        /// <summary>
        /// 查询已作答题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetQuestionByGradeDtoNew>> GetQuestionByGrade(long gradeId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var gradeEntity = DbContext.FreeSql.Select<ExamStudentGradeNew>().Where(m => m.Id == gradeId && m.UserId == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (gradeEntity == null)
                    return new PageResponse<GetQuestionByGradeDtoNew> { Code = CommonConstants.ErrorCode, Msg = "未找到相关主作答记录" };

                var selectData = DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Select.From<YssxTopicNew>((a, b) =>
                    a.InnerJoin(aa => aa.QuestionId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.GradeId == gradeId && b.ParentId == 0)
                    .OrderBy((a, b) => b.Sort)
                    .ToList((a, b) => new GetQuestionByGradeDtoNew
                    {
                        QuestionId = a.QuestionId,
                        Title = b.Title
                    });
                return new PageResponse<GetQuestionByGradeDtoNew>() { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 获取题目信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamQuestionInfoNew>> GetQuestionInfo(long gradeId, long questionId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamQuestionInfoNew>(CommonConstants.BadRequest, $"");
            try
            {
                var user = userTicket;

                #region 校验

                if (gradeId == 0 || questionId == 0)
                {
                    result.Code = CommonConstants.BadRequest;
                    result.Msg = "所有参数都不能为0";
                    return result;
                }

                var grade = DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

                if (grade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = "没找到答题记录";
                    return result;
                }

                if (grade.Status == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"考试已结束！";
                    return result;
                }

                var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);

                if (exam == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"考试信息不存在";
                    return result;
                }

                var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetTopicById}{questionId}", true, 10 * 60, true);

                if (question == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"题目不存在";
                    return result;
                }

                if (exam.CaseId != question.CaseId)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"题目和试卷信息不匹配";
                    return result;
                }

                YssxTopicNew settledQuestion = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.CaseId == exam.CaseId && s.QuestionType == QuestionType.SettleAccounts && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetTopicById}{exam.CaseId}_1", true, 10 * 60, true);

                var validateResult = await ValidateQuestion(exam, grade, user);

                if (validateResult.Code != CommonConstants.SuccessCode)
                {
                    result.Code = validateResult.Code;
                    result.Msg = validateResult.Msg;
                    return result;
                }

                var isBigDataSubmit = false;
                var groupStudent = validateResult.Data;
                if (groupStudent != null)
                {
                    isBigDataSubmit = await RedisHelper.ExistsAsync($"{CommonConstants.BigDataPositionStatus}{groupStudent.GroupId}");
                }


                #endregion

                if (exam != null)
                {
                    switch (exam.ExamType)
                    {
                        case ExamType.PracticeTest:
                            result = await GetPracticeQuestionInfo(exam, grade, question, user);
                            break;
                        //case ExamType.EmulationTest:

                        //    result = await GetEmulationQuestionInfo(exam, grade, question, user, isBigDataSubmit);
                        //    break;

                        //case ExamType.SchoolCompetition:

                        //    result = await GetSchoolQuestionInfo(exam, grade, question, user, isBigDataSubmit);
                        //    break;

                        //case ExamType.ProvincialCompetition:

                        //    result = await GetProvinceQuestionInfo(exam, grade, question, user, isBigDataSubmit);
                        //    break;

                        default:

                            throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
                    }

                    bool isCanSubmit = false;
                    if (null != settledQuestion)
                    {
                        if (!result.Data.IsSettled && question.Sort < settledQuestion.Sort)
                        {
                            isCanSubmit = true;
                        }
                        else if (result.Data.IsSettled && question.Sort > settledQuestion.Sort)
                        {
                            isCanSubmit = true;
                        }
                        else if (question.Sort == settledQuestion.Sort)
                        {
                            isCanSubmit = true;
                        }

                        result.Data.IsCanSubmit = isCanSubmit;

                    }
                    else
                    {
                        result.Data.IsCanSubmit = true;
                    }

                    if (result.Code == CommonConstants.SuccessCode)
                    {
                        if (groupStudent != null && groupStudent.GroupId > 0)
                        {
                            if (groupStudent.StudentExamStatus == StudentExamStatus.Started)
                            {
                                result.Data.LeftSeconds = (long)GetLeftSeconds(groupStudent.LeftSeconds, groupStudent.RecordTime);
                            }

                            result.Data.StudentExamStatus = groupStudent.StudentExamStatus;
                            result.Data.IsBigDataSubmit = isBigDataSubmit;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(JsonHelper.SerializeObject(ex));
            }

            return result;
        }

        /// <summary>
        /// 获取题目基本信息--自主练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfoNew>> GetPracticeQuestionInfo(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user)
        {
            return await GetQuestionInfoBase(exam, grade, question, user);
        }

        ///// <summary>
        ///// 获取题目基本信息--真题模拟
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <param name="question"></param>
        ///// <param name="user"></param>
        ///// <param name="isBigDataSubmit">大数据岗位是否已交卷</param>
        ///// <returns></returns>
        //private async Task<ResponseContext<ExamQuestionInfoNew>> GetEmulationQuestionInfo(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user, bool isBigDataSubmit = false)
        //{
        //    return await GetQuestionInfoBase(exam, grade, question, user, isBigDataSubmit);
        //}

        ///// <summary>
        ///// 获取题目基本信息--校内赛
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <param name="question"></param>
        ///// <param name="user"></param>
        ///// <param name="isBigDataSubmit">大数据岗位是否已交卷</param>
        ///// <returns></returns>
        //private async Task<ResponseContext<ExamQuestionInfoNew>> GetSchoolQuestionInfo(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user, bool isBigDataSubmit = false)
        //{
        //    var result = new ResponseContext<ExamQuestionInfoNew>();
        //    if (exam.BeginTime > DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试未开始 ";
        //        return result;
        //    }
        //    if (exam.EndTime < DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试已结束";
        //        return result;
        //    }

        //    result = await GetQuestionInfoBase(exam, grade, question, user, isBigDataSubmit);
        //    //校内赛--个人赛
        //    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
        //        result.Data.StudentExamStatus = grade.Status;
        //    return result;
        //}

        ///// <summary>
        ///// 获取题目基本信息--省赛
        ///// </summary>
        ///// <param name="exam"></param>
        ///// <param name="grade"></param>
        ///// <param name="question"></param>
        ///// <param name="user"></param>
        ///// <param name="isBigDataSubmit">大数据岗位是否已交卷</param>
        ///// <returns></returns>
        //private async Task<ResponseContext<ExamQuestionInfoNew>> GetProvinceQuestionInfo(ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user, bool isBigDataSubmit = false)
        //{
        //    var result = new ResponseContext<ExamQuestionInfoNew>();
        //    if (exam.BeginTime > DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试未开始 ";
        //        return result;
        //    }
        //    if (exam.EndTime < DateTime.Now)
        //    {
        //        result.Code = CommonConstants.BadRequest;
        //        result.Msg = "考试已结束";
        //        return result;
        //    }
        //    return await GetQuestionInfoBase(exam, grade, question, user, isBigDataSubmit);
        //}

        #endregion

        #region 提交答案

        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        public async Task<ResponseContext<QuestionResultNew>> SubmitAnswer(QuestionAnswerNew model, UserTicket userTicket)
        {
            var result = new ResponseContext<QuestionResultNew>(CommonConstants.BadRequest, "");
            var user = userTicket;

            #region 校验

            if (model.GradeId == 0 || model.QuestionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "GradeId,QuestionId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"考试已结束！";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == model.QuestionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            //var question = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.Id == model.QuestionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetTopicById}{model.QuestionId}", true, 10 * 60, true);
            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "题目不存在";
                return result;
            }

            if (exam.Id != grade.ExamId || exam.CaseId != question.CaseId)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = "参数信息不匹配 ";
                return result;
            }

            var validateQuestion = await ValidateQuestion(exam, grade, user, question, model);

            if (validateQuestion.Code != CommonConstants.SuccessCode)
            {
                result.Code = validateQuestion.Code;
                result.Msg = validateQuestion.Msg;
                return result;
            }

            #endregion

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:

                    return await SubmitPracticeAnswer(model, exam, grade, question, user);

                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }
        }

        /// <summary>
        /// 提交答案--自主练习
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResultNew>> SubmitPracticeAnswer(QuestionAnswerNew answer, ExamPaperNew exam, ExamStudentGradeNew grade, YssxTopicNew question, UserTicket user)
        {
            if (exam.EndTime <= DateTime.Now || exam.Status == ExamStatus.End)
            {
                var result = new ResponseContext<QuestionResultNew>(CommonConstants.BadRequest, "");
                result.Code = CommonConstants.BadRequest;
                result.Msg = "考试已结束!";
                return result;
            }
            return await SubmitAnswerBase(answer, exam, question, user);
        }


        /// <summary>
        /// 提交答案基础函数
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResultNew>> SubmitAnswerBase(QuestionAnswerNew answer, ExamPaperNew exam, YssxTopicNew question, UserTicket user)
        {
            var result = new ResponseContext<QuestionResultNew>() { Data = new QuestionResultNew() { QuestionId = question.Id } };
            try
            {
                List<YssxExamCertificateDataRecordNew> AddAccountEntryList(QuestionAnswerNew ans, long detailId)
                {
                    var entryList = new List<YssxExamCertificateDataRecordNew>();
                    if (ans.AccountEntryList != null && ans.AccountEntryList.Any())
                    {
                        entryList = ans.AccountEntryList.Select(a => a.MapTo<YssxExamCertificateDataRecordNew>()).ToList();
                        entryList.ForEach(a =>
                        {
                            a.Id = IdWorker.NextId();
                            a.ExamId = exam.Id;
                            a.GradeId = answer.GradeId;
                            a.GradeDetailId = detailId;
                            a.QuestionId = ans.QuestionId;
                            a.CaseId = exam.CaseId;
                            a.TenantId = exam.TenantId;

                            a.BorrowAmount = a.BorrowAmount;
                            a.CreditorAmount = a.CreditorAmount;
                        });
                    }

                    return entryList;
                }

                #region 提交答案

                var accountEntryList = new List<YssxExamCertificateDataRecordNew>();

                result = RedisLock.SkipLock(user.Id.ToString(), () =>
                {
                    var lockResult = new ResponseContext<QuestionResultNew>() { Data = new QuestionResultNew() };

                    try
                    {
                        var gradeDetailList = DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Where(a => a.GradeId == answer.GradeId && a.ParentQuestionId == answer.QuestionId)
                        .Master().ToList(a => new { a.Id, a.ExamId, a.QuestionId, a.Answer });
                        if (gradeDetailList == null || gradeDetailList.Count == 0)
                        {
                            #region 插入作答信息

                            var detailInsertList = new List<ExamStudentGradeDetailNew>();
                            //var detailDeleteList = new List<ExamStudentGradeDetailNew>();
                            DateTime nowTime = DateTime.Now;
                            //var sourceData = JsonHelper.SerializeObject(answer);
                            var mainDetail = new ExamStudentGradeDetailNew()
                            {
                                Id = IdWorker.NextId(),
                                GradeId = answer.GradeId,
                                ExamId = exam.Id,
                                TenantId = user.TenantId,
                                QuestionId = answer.QuestionId,
                                ParentQuestionId = answer.QuestionId,
                                PostionId = question.PositionId,
                                UserId = user.Id,
                                StudentId = CommonConstants.StudentId,
                                Answer = answer.AnswerValue,
                                CreateTime = nowTime,
                                //SourceDataInfo = sourceData
                            };
                            answer.GradeDetailId = mainDetail.Id;

                            if (question.QuestionType == QuestionType.MainSubQuestion)//多题型 
                            {
                                var multiQuestions = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.ParentId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                                    .OrderBy(s => s.Sort)
                                    .ToListAsync().Result
                                    .Select(s =>
                                    new SubQuestionDto()
                                    {
                                        Id = s.Id,
                                        QuestionType = s.QuestionType,
                                        PositionId = s.PositionId
                                    });

                                foreach (var item in answer.MultiQuestionAnswers)
                                {
                                    var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                                    var subDetail = new ExamStudentGradeDetailNew()
                                    {
                                        Id = IdWorker.NextId(),
                                        TenantId = user.TenantId,
                                        ExamId = exam.Id,
                                        GradeId = answer.GradeId,
                                        QuestionId = item.QuestionId,
                                        PostionId = subQuestion.PositionId,
                                        UserId = user.Id,
                                        StudentId = CommonConstants.StudentId,
                                        Answer = item.AnswerValue,
                                        ParentQuestionId = answer.QuestionId,
                                        CreateTime = nowTime,
                                        //SourceDataInfo = sourceData
                                    };

                                    item.GradeDetailId = subDetail.Id;

                                    #region 分录题特殊处理

                                    if (subQuestion.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                    {
                                        subDetail.AccountEntryAuditStatus = item.AccountEntryAuditStatus;
                                        subDetail.AccountEntryStatus = item.AccountEntryStatus;
                                        accountEntryList.AddRange(AddAccountEntryList(item, subDetail.Id));
                                    }

                                    #endregion

                                    detailInsertList.Add(subDetail);

                                    //detailDeleteList.Add(new ExamStudentGradeDetailNew() { GradeId = subDetail.GradeId, ParentQuestionId = subDetail.ParentQuestionId, QuestionId = subDetail.QuestionId }) ;

                                }
                            }
                            else if (question.QuestionType == QuestionType.AccountEntry)//分录题特殊处理
                            {
                                mainDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                                mainDetail.AccountEntryStatus = answer.AccountEntryStatus;
                                accountEntryList = AddAccountEntryList(answer, mainDetail.Id);
                            }

                            detailInsertList.Add(mainDetail);
                            //detailDeleteList.Add(new ExamStudentGradeDetailNew() { GradeId = mainDetail.GradeId, ParentQuestionId = mainDetail.ParentQuestionId, QuestionId = mainDetail.QuestionId });
                            DbContext.FreeSql.Transaction(() =>
                            {
                                detailInsertList.ForEach(s =>
                                {
                                    DbContext.FreeSql.Delete<ExamStudentGradeDetailNew>().Where(b => b.GradeId == s.GradeId && b.ParentQuestionId == s.ParentQuestionId && b.QuestionId == s.QuestionId).ExecuteAffrows();
                                });

                                DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Insert(detailInsertList);
                                if (accountEntryList.Any())
                                {
                                    DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecordNew>().Insert(accountEntryList);
                                }

                                if (question.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理
                                {
                                    DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy
                                    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                    .Set(a => a.UpdateTime, DateTime.Now)
                                    .Set(a => a.IsSettled, true).Where(a => a.Id == answer.GradeId).ExecuteAffrows();
                                }
                            });

                            #endregion
                        }
                        else
                        {
                            #region 更新作答信息

                            foreach (var detail in gradeDetailList)
                            {
                                if (detail.QuestionId == answer.QuestionId)
                                {
                                    answer.GradeDetailId = detail.Id;
                                    if (question.QuestionType == QuestionType.MainSubQuestion)//多题型
                                    {
                                        var questionIds = gradeDetailList.Select(s => s.QuestionId).Where(s => s != answer.QuestionId);

                                        foreach (var item in gradeDetailList)
                                        {
                                            if (item.QuestionId == question.Id)
                                            {
                                                continue;
                                            }

                                            var dtoDetail = answer.MultiQuestionAnswers.FirstOrDefault(a => a.QuestionId == item.QuestionId);
                                            if (dtoDetail == null || dtoDetail.QuestionId == 0)
                                            {
                                                continue;
                                            }
                                            if (dtoDetail.IsIgnore)
                                            {
                                                dtoDetail.AnswerValue = item.Answer;
                                            }
                                            dtoDetail.GradeDetailId = item.Id;

                                            #region 分录题特殊处理

                                            if (dtoDetail.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                            {
                                                accountEntryList.AddRange(AddAccountEntryList(dtoDetail, item.Id));
                                            }

                                            #endregion
                                        }

                                        //先删除之前录入凭证数据
                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecordNew>().Delete(a => a.GradeId == answer.GradeId && questionIds.Contains(a.QuestionId));
                                                DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecordNew>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    else
                                    {
                                        #region 结账题特殊处理（有答题记录时取消结账--即删除答题记录）

                                        if (question.QuestionType == QuestionType.SettleAccounts)
                                        {
                                            //删除答案
                                            DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().Delete(s => s.Id == detail.Id);
                                            DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy
                                            .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                            .Set(a => a.UpdateTime, DateTime.Now)
                                            .Set(a => a.IsSettled, false).Where(a => a.Id == answer.GradeId).ExecuteAffrows();

                                            //zhcc  20190824
                                            var questionResult = new QuestionResultNew()
                                            {
                                                No = question.Sort,
                                                QuestionId = question.Id,
                                                AnswerResult = AnswerResultStatus.None
                                            };
                                            lockResult.Data = questionResult;
                                            return lockResult;
                                        }

                                        #endregion

                                        #region 分录题：录入凭证存储更新（用于账簿查询）

                                        if (question.QuestionType == QuestionType.AccountEntry)
                                        {
                                            accountEntryList = AddAccountEntryList(answer, detail.Id);
                                        }

                                        #endregion

                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                //先删除之前录入凭证数据
                                                DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecordNew>().Delete(a => a.GradeId == answer.GradeId && a.QuestionId == answer.QuestionId);
                                                DbContext.FreeSql.GetRepository<YssxExamCertificateDataRecordNew>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"提交答案失败";
                        CommonLogger.Error($"答题异常", ex);
                    }

                    return lockResult;

                }, timeoutSeconds: 5);

                if (result == null || result.Code != CommonConstants.SuccessCode)
                {
                    return result;
                }

                if (result.Data.QuestionId > 0)//取消结账，直接返回
                {
                    goto End;
                }

                #endregion

                //TODO:计算题目分数
                var task = Task.Run(() =>
                {
                    var compareRst = CompareTopicAnswer(question, answer);

                    return compareRst;
                }).ConfigureAwait(false);
                if (exam.CanShowAnswerBeforeEnd)
                {
                    result.Data = await task;
                }

                if (exam.ExamType != ExamType.PracticeTest && exam.CompetitionType == CompetitionType.TeamCompetition)
                {
                    if (question.QuestionType == QuestionType.MainSubQuestion)
                    {
                        result.Data.AccountEntryStatusDic = answer.MultiQuestionAnswers.Where(s => s.QuestionType == QuestionType.AccountEntry).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                    }
                    else if (question.QuestionType == QuestionType.AccountEntry)
                    {
                        result.Data.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() { new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(answer.QuestionId, answer.AccountEntryStatus, answer.AccountEntryAuditStatus, answer.Sort) };
                    }
                }

                result.Msg = "成功提交答案1";

            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"答题异常:GradeId-{answer.GradeId},QuestionId-{answer.QuestionId},UserId:{user.Id}";
                CommonLogger.Error(msg + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
            }

            End:
            return result;
        }

        #endregion

        #region 提交考卷

        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GradeInfoNew>> SubmitExam(long gradeId, UserTicket userTicket, bool isAutoSubmit = false)
        {
            var user = userTicket;

            var result = new ResponseContext<GradeInfoNew>();

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"GradeId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"您已经提交过考卷了！";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            var studentGrade = new ExamPaperGroupStudentNew();

            if (exam.ExamType != ExamType.PracticeTest & exam.CompetitionType == CompetitionType.TeamCompetition)//团队赛
            {
                studentGrade = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(s => s.GroupId == grade.GroupId && s.UserId == user.Id && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (studentGrade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = "没找到答题记录";
                    return result;
                }

                if (studentGrade.StudentExamStatus == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"您已经提交过考卷了！";
                    return result;
                }
                else
                {
                    if (exam.ExamType != ExamType.EmulationTest)
                    {
                        if (studentGrade.RecordTime.AddMinutes(60d) > DateTime.Now)
                        {
                            result.Code = CommonConstants.ErrorCode;
                            result.Msg = $"开考60分钟后才能交卷！";
                            return result;
                        }
                    }
                }
            }

            #endregion

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:

                    return await SubmitPracticeExam(exam, grade, user);

                //case ExamType.EmulationTest:

                //    return await SubmitEmulationExam(exam, grade, user, studentGrade.Id > 0 ? studentGrade : null, isAutoSubmit);

                //case ExamType.SchoolCompetition:

                //    return await SubmitSchoolExam(exam, grade, user, studentGrade.Id > 0 ? studentGrade : null, isAutoSubmit);

                //case ExamType.ProvincialCompetition:

                //    return await SubmitProvinceExam(exam, grade, user, studentGrade.Id > 0 ? studentGrade : null, isAutoSubmit);

                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }
        }

        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <param name="examId"></param>
        public async Task<ResponseContext<long>> SubmitExamBatch(long examId)
        {
            var result = new ResponseContext<long>();

            try
            {
                #region 校验

                if (examId == 0)
                {
                    result.Code = CommonConstants.BadRequest;
                    result.Msg = $"ExamId不能为0 ";
                    return result;
                }

                var exam = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{examId}", true, 10 * 60);
                if (exam == null)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"考试信息不存在 ";
                    return result;
                }

                if (DateTime.Now < exam.BeginTime)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"考试未开始！";
                    return result;
                }
                else if (DateTime.Now < exam.EndTime)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"考试未结束，不能批量交卷！";
                    return result;
                }

                #endregion

                if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                {
                    goto GradeSubmit;
                }
                else
                {
                    var gradeList = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(a => a.ExamId == examId && a.StudentExamStatus == StudentExamStatus.Started).ToListAsync();

                    if (gradeList != null && gradeList.Any())
                    {
                        foreach (var studentGrade in gradeList)
                        {
                            studentGrade.Status = GroupStatus.End;
                            //提交考卷
                            await SubmitExamGroupGrade(exam, studentGrade, true, true);
                        }

                        await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy.SetSource(gradeList).ExecuteAffrowsAsync();
                    }
                }

                GradeSubmit://主作答纪录
                {
                    var gradeList = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(a => a.ExamId == examId && a.Status == StudentExamStatus.Started).ToListAsync();

                    if (gradeList != null && gradeList.Any())
                    {
                        foreach (var grade in gradeList)
                        {
                            //批量提交考卷
                            SubmitExamGrade(exam, grade, true, true);
                        }

                        await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy.SetSource(gradeList).ExecuteAffrowsAsync();

                        if (exam.CompetitionType == CompetitionType.TeamCompetition)
                        {
                            var groupIds = gradeList.Where(s => s.GroupId > 0).Select(s => s.GroupId.ToString()).ToList();
                            if (groupIds != null && groupIds.Any())
                            {
                                await DbContext.FreeSql.GetRepository<ExamPaperGroupNew>().UpdateDiy.Set(s => s.Status, GroupStatus.End).UpdateColumns(s => s.Status).Where(s => s.ExamId == exam.Id).ExecuteAffrowsAsync();

                                groupIds.ForEach(groupId =>
                                {
                                    ImHelper.SendChanMessage(Guid.Empty, groupId, "{\"messageType\":\"examEnded\"}");
                                    groupId = $"{CommonConstants.ImMessagePre}{groupId}";
                                });


                                RedisHelper.Del(groupIds.ToArray());
                            }
                        }
                        else
                        {
                            ImHelper.SendChanMessage(Guid.Empty, exam.Id.ToString(), "{\"messageType\":\"examEnded\"}");
                        }

                    }
                }

                exam.Status = ExamStatus.End;
                await DbContext.FreeSql.GetRepository<ExamPaperNew>().UpdateDiy.SetSource(exam).UpdateColumns(s => s.Status).ExecuteAffrowsAsync();
            }
            catch (Exception ex)
            {
                result.Msg = "批量提交考卷异常，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"批量交卷异常:examId-{examId}";
                CommonLogger.Error(msg, ex);
            }

            if (result.Code == CommonConstants.SuccessCode)
            {
                result.Msg = $"操作成功！";
            }

            return result;
        }

        /// <summary>
        /// 自动交卷-定时任务
        /// </summary>
        public async Task<ResponseContext<long>> SubmitExamTask(UserTicket userTicket)
        {
            var result = new ResponseContext<long>();
            try
            {
                var user = userTicket;

                #region 交卷

                var examList = await DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(s => s.Status == ExamStatus.Wait && s.BeginTime < DateTime.Now && (s.ExamType == ExamType.ProvincialCompetition || s.ExamType == ExamType.SchoolCompetition) && s.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync(s => new ExamPaperNew() { Id = s.Id, CaseId = s.CaseId, TotalQuestionCount = s.TotalQuestionCount, CompetitionType = s.CompetitionType, EndTime = s.EndTime, Status = ExamStatus.End });
                if (examList != null && examList.Any())
                {
                    var updateGradeList = new List<ExamStudentGradeNew>();
                    var examUpdateList = new List<ExamPaperNew>();

                    foreach (var exam in examList)
                    {
                        var updateStudentGradeList = new List<ExamPaperGroupStudentNew>();

                        if (exam.CompetitionType == CompetitionType.TeamCompetition)
                        {
                            //所有过期未提交
                            var studentGradeList = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(a => a.ExamId == exam.Id && a.RecordTime.AddMinutes(a.TotalMinutes) <= DateTime.Now && a.StudentExamStatus == StudentExamStatus.Started).ToListAsync();

                            if (studentGradeList != null && studentGradeList.Any())
                            {
                                foreach (var studentGrade in studentGradeList)
                                {
                                    //提交考卷
                                    await SubmitExamGroupGrade(exam, studentGrade, true, true);

                                    updateStudentGradeList.Add(studentGrade);

                                }

                                await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy.SetSource(updateStudentGradeList).ExecuteAffrowsAsync();

                                updateStudentGradeList.ForEach(s =>
                                {
                                    ImHelper.SendChanMessage(Guid.Empty, s.GroupId.ToString(), "{\"messageType\":\"examSubmit\",\"content\":{\"positionId\":" + s.PostionId + "}}");
                                });
                            }
                        }

                        if (exam.EndTime <= DateTime.Now)//考试已结束，修改主作答纪录
                        {
                            if (exam.CompetitionType == CompetitionType.TeamCompetition)
                            {
                                await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy.Set(s => s.Status, GroupStatus.End).UpdateColumns(s => s.Status).Where(s => s.ExamId == exam.Id).ExecuteAffrowsAsync();

                                await DbContext.FreeSql.GetRepository<ExamPaperGroupNew>().UpdateDiy.Set(s => s.Status, GroupStatus.End).UpdateColumns(s => s.Status).Where(s => s.ExamId == exam.Id).ExecuteAffrowsAsync();
                            }
                            var gradeList = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Where(a => a.ExamId == exam.Id && a.Status == StudentExamStatus.Started).ToListAsync();

                            if (gradeList != null && gradeList.Any())
                            {
                                foreach (var grade in gradeList)
                                {
                                    //提交考卷
                                    SubmitExamGrade(exam, grade, true, true);
                                    updateGradeList.Add(grade);
                                }
                            }

                            examUpdateList.Add(exam);
                        }
                    }

                    if (updateGradeList.Any())
                    {
                        await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().UpdateDiy.SetSource(updateGradeList).ExecuteAffrowsAsync();

                        var groupIds = updateGradeList.Where(s => s.GroupId > 0).Select(s => s.GroupId.ToString()).ToList();
                        if (groupIds != null && groupIds.Any())
                        {
                            groupIds.ForEach(groupId =>
                            {

                                ImHelper.SendChanMessage(Guid.Empty, groupId, "{\"messageType\":\"examEnded\"}");
                                groupId = $"{CommonConstants.ImMessagePre}{groupId}";
                            });

                            RedisHelper.Del(groupIds.ToArray());
                        }

                        var examIds = updateGradeList.Where(s => s.GroupId == 0).Select(s => s.ExamId.ToString()).Distinct().ToList();
                        if (examIds != null && examIds.Any())
                        {
                            examIds.ForEach(examId =>
                            {
                                ImHelper.SendChanMessage(Guid.Empty, examId, "{\"messageType\":\"examEnded\"}");
                            });
                        }
                    }

                    if (examUpdateList.Any())
                    {
                        await DbContext.FreeSql.GetRepository<ExamPaperNew>().UpdateDiy.SetSource(examUpdateList).UpdateColumns(s => s.Status).ExecuteAffrowsAsync();
                    }
                }

                result.Msg = "自动交卷任务执行成功";
                CommonLogger.Error("自动交卷任务执行成功 ");

                #endregion
            }
            catch (Exception ex)
            {
                var msg = $"自动交卷任务执行异常";
                result.Msg = msg;
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(msg, ex);
            }

            return result;
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 新增作答记录
        /// </summary>
        /// <param name="exam">考试信息</param>
        /// <param name="tuples">(userId,tenantId,groupId)</userId>
        /// <returns></returns>
        private ResponseContext<ExamStudentGradeNew> InsertGrade(ExamPaperNew exam, params ExamPaperGroupStudentDtoNew[] list)
        {
            var result = new ResponseContext<ExamStudentGradeNew>() { Code = CommonConstants.SuccessCode };

            try
            {
                //插入学生考试作答主表
                var gradeId = IdWorker.NextId();
                var grade = new ExamStudentGradeNew()
                {
                    Id = gradeId,
                    ExamId = exam.Id,
                    ExamPaperScore = exam.TotalScore,
                    LeftSeconds = exam.TotalMinutes * 60,
                    RecordTime = DateTime.Now,
                    UserId = list[0].UserId,
                    TenantId = list[0].TenantId,
                    GroupId = list[0].GroupId,
                    StudentId = CommonConstants.StudentId,
                    Status = StudentExamStatus.Started
                };

                var lastGradeList = new List<ExamStudentLastGradeNew>();

                switch (exam.ExamType)
                {
                    case ExamType.PracticeTest:
                        var userIds = list.Select(s => s.UserId).ToArray();
                        var lastGrades = DbContext.FreeSql.GetRepository<ExamStudentLastGradeNew>().Select.Where(s => s.ExamId == exam.Id && userIds.Contains(s.UserId))
                            .ToList(s => new { s.Id, s.UserId });

                        list.ToList().ForEach(t =>
                        {
                            var lastGradeId = lastGrades.FirstOrDefault(s => s.UserId == t.UserId)?.Id ?? 0;

                            var lastGrade = new ExamStudentLastGradeNew();
                            if (lastGradeId > 0)
                            {
                                lastGrade.Id = lastGradeId;
                                lastGrade.GradeId = gradeId;
                                lastGrade.UpdateTime = DateTime.Now;
                            }
                            else
                            {
                                lastGrade = new ExamStudentLastGradeNew()
                                {
                                    GradeId = gradeId,
                                    ExamId = exam.Id,
                                    GroupId = t.GroupId,
                                    UserId = t.UserId,
                                    TenantId = t.TenantId,
                                    CaseId = exam.CaseId
                                };
                            }
                            lastGradeList.Add(lastGrade);
                        });
                        break;
                    //case ExamType.EmulationTest:
                    //case ExamType.SchoolCompetition:
                    //case ExamType.ProvincialCompetition:
                    //    grade.RecordTime = exam.BeginTime;
                    //    break;
                    default:
                        throw new NotImplementedException($"还没有实现 {exam.ExamType}考试类型,稍等稍等");
                }

                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.GetRepository<ExamStudentGradeNew>().Insert(grade);
                    if (lastGradeList.Any())
                    {
                        lastGradeList.ForEach(s =>
                        {
                            if (s.Id > 0)
                            {
                                DbContext.FreeSql.GetRepository<ExamStudentLastGradeNew>().UpdateDiy.SetSource(s).UpdateColumns(a => new { a.GradeId, a.UpdateTime }).ExecuteAffrows();
                            }
                            else
                            {
                                s.Id = IdWorker.NextId();
                                DbContext.FreeSql.GetRepository<ExamStudentLastGradeNew>().Insert(s);
                            }
                        });
                    }

                    if (list.Length > 1 && list[0].GroupId > 0)//多条用户信息&&groupId>0，修改group状态为已完成complete
                    {
                        var groupId = list[0].GroupId;
                        var nowTime = DateTime.Now;
                        list.ToList().ForEach(s =>
                        {
                            s.TotalTime = s.TimeMinutes > 0 ? s.TimeMinutes : s.TotalTime;
                            var leftSeconds = s.TotalTime * 60d;
                            DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().UpdateDiy
                            .UpdateColumns(a => new { a.Status, a.GradeId, a.StudentExamStatus, a.TotalMinutes, a.LeftSeconds, a.RecordTime })
                            .Set(a => a.Status == GroupStatus.Complete)
                            .Set(a => a.GradeId == gradeId)
                            .Set(a => a.StudentExamStatus == StudentExamStatus.Started)
                            .Set(a => a.TotalMinutes == s.TotalTime)
                            .Set(a => a.LeftSeconds == leftSeconds)
                            .Set(a => a.RecordTime == nowTime)
                            .Where(a => a.Id == s.Id).ExecuteAffrows();
                        });
                        DbContext.FreeSql.GetRepository<ExamPaperGroupNew>().UpdateDiy
                        .UpdateColumns(a => new { a.Status, a.GradeId })
                        .Set(s => s.Status == GroupStatus.Complete)
                        .Set(s => s.GradeId == gradeId).Where(s => s.Id == groupId).ExecuteAffrows();
                    }
                });

                result.Data = grade;
            }
            catch (Exception ex)
            {
                result.Msg = "新增答题记录失败，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"新增答题记录常:examId-{exam.Id},UserId:{string.Join(',', list.Select(s => s.UserId))}";
                CommonLogger.Error(msg, ex);
                return result;
            }

            return result;
        }

        ///// <summary>
        ///// 获取剩余考试时间
        ///// </summary>
        ///// <param name="grade"></param>
        ///// <returns></returns>
        //private double GetLeftSeconds(double leftSeconds, DateTime recordTime)
        //{
        //    var left = leftSeconds - (DateTime.Now - recordTime).TotalSeconds;
        //    if (left <= 0)
        //    {
        //        return 0;
        //    }
        //    return left;
        //}

        ///// <summary>
        ///// 获取剩余考试时间
        ///// </summary>
        ///// <param name="grade"></param>
        ///// <returns></returns>
        //private double GetLeftSeconds(ExamPaperNew exam)
        //{
        //    var left = (exam.EndTime.Value - DateTime.Now).TotalSeconds;
        //    if (left <= 0)
        //    {
        //        return 0;
        //    }
        //    return left;
        //}
        private async Task<ExamPaperGroupStudentViewNew> GetExamGroupStudentInfo(ExamStudentGradeNew grade, UserTicket user)
        {
            var groupStudent = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(s => s.GradeId == grade.Id && s.UserId == user.Id)
               .FirstAsync(s => new ExamPaperGroupStudentViewNew()
               {
                   GroupId = s.GroupId,
                   PostionId = s.PostionId,
                   UserId = s.UserId,
                   UserName = user.RealName,
                   TotalMinutes = s.TotalMinutes,
                   StudentExamStatus = s.StudentExamStatus,
                   RecordTime = s.RecordTime,
                   LeftSeconds = s.LeftSeconds,
               });

            var leftSeconds = GetLeftSeconds(groupStudent.LeftSeconds, groupStudent.RecordTime);
            groupStudent.UsedSeconds = groupStudent.LeftSeconds - leftSeconds;
            groupStudent.LeftSeconds = leftSeconds;

            return groupStudent;
        }

        private async Task<List<ExamPaperGroupStudentViewNew>> GetExamGroupStudentInfos(ExamPaperNew exam, ExamStudentGradeNew grade, List<QuestionInfoNew> QuestionInfoList, UserTicket user)
        {
            var groupStudents = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudentNew>().Where(s => s.GradeId == grade.Id)
               .ToListAsync(s => new ExamPaperGroupStudentViewNew()
               {
                   Id = s.Id,
                   GroupId = s.GroupId,
                   PostionId = s.PostionId,
                   UserId = s.UserId,
                   UserName = user.RealName,
                   TotalMinutes = s.TotalMinutes,
                   StudentExamStatus = s.StudentExamStatus,
                   RecordTime = s.RecordTime,
                   LeftSeconds = s.LeftSeconds
               });

            var positions = await DbContext.FreeSql.GetRepository<YssxCasePositionNew>()
                .Where(s => s.CaseId == exam.CaseId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync($"{CommonConstants.Cache_GetCasePositionsByCaseId}{exam.CaseId}", true, 10 * 60, true);

            groupStudents.ForEach(s =>
            {
                var leftSeconds = GetLeftSeconds(s.LeftSeconds, s.RecordTime);
                s.UsedSeconds = s.LeftSeconds - leftSeconds;
                s.LeftSeconds = leftSeconds;

                s.PostionName = positions.FirstOrDefault(a => a.PostionId == s.PostionId).PostionName;

                s.QuestionCount = QuestionInfoList.Where(a => a.PositionId == s.PostionId).Count();
                s.QuestionAnswerCount = QuestionInfoList.Where(a => a.PositionId == s.PostionId && a.QuestionAnswerStatus != AnswerDTOStatus.NoAnswer).Count();
                s.QuestionNoAnswerCount = s.QuestionCount - s.QuestionAnswerCount;

                if (s.UserId == user.Id)
                {
                    s.IsSelf = true;
                }
            });

            return groupStudents;
        }

        #endregion

        #region 题目算分相关方法

        /// <summary>
        /// 比较答案
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="answer"></param>
        /// <param name="caseModel"></param>
        /// <returns></returns>
        private QuestionResultNew CompareTopicAnswer(YssxTopicNew topic, QuestionAnswerNew answer)
        {
            var examResultDto = new QuestionResultNew { AnswerResult = AnswerResultStatus.Error, No = answer.Sort, QuestionId = answer.QuestionId };
            if (topic.QuestionType != QuestionType.MainSubQuestion && string.IsNullOrEmpty(answer.AnswerValue))
            {
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            if (topic.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理，直接得分
            {
                examResultDto.AnswerResult = AnswerResultStatus.Right;
                examResultDto.AnswerScore = topic.Score;
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            var score = topic.Score;
            var standardAnswerVal = HttpUtility.UrlDecode(topic.AnswerValue);
            var answerTheValue = HttpUtility.UrlDecode(answer.AnswerValue);
            AnswerResultStatus status;
            decimal answerScore = 0;
            switch (topic.QuestionType)
            {
                case QuestionType.SingleChoice:
                case QuestionType.Judge:
                    status = answerTheValue == standardAnswerVal ? AnswerResultStatus.Right : AnswerResultStatus.Error;
                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MultiChoice:
                    var standardVal = standardAnswerVal.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var answerVal = answerTheValue.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    status = CommonMethod.CompareArrayElement(standardVal, answerVal) ? AnswerResultStatus.Right : AnswerResultStatus.Error;

                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.AccountEntry:
                case QuestionType.GridFillBank:
                case QuestionType.FillBlank:
                case QuestionType.FillGrid:
                case QuestionType.FillGraphGrid:
                case QuestionType.FinancialStatements:
                    var standardJObj = JsonHelper.DeserializeObject<Answer>(standardAnswerVal);
                    var answerJObj = JsonHelper.DeserializeObject<Answer>(answerTheValue);
                    var answerCountInfo = AnswerCompareHelperV2.Compare(standardJObj, answerJObj, 500, 600);

                    var caseSet = new YssxCaseNew();
                    if (topic.QuestionType == QuestionType.AccountEntry)//分录题获取非完整性得分及分录题占比设置
                    {
                        caseSet = DbContext.FreeSql.GetRepository<YssxCaseNew>().Where(s => s.Id == topic.CaseId).FirstAsync($"{CommonConstants.Cache_GetCaseById}{topic.CaseId}", true, 10 * 60, true).Result;
                    }
                    var scale = 0M;// topic.QuestionType == QuestionType.AccountEntry ? caseSet.AccountEntryScale : 0M; 说明：案例中同步实训“分录题分数占比”没有 20200611 lwj
                    switch (topic.CalculationType)
                    {
                        case CalculationType.None:
                        case CalculationType.AvgCellCount:
                            var aroundScored = 0m;
                            int aroundVaildCount = answerCountInfo.HeaderCountInfo.ValidCount + answerCountInfo.FooterCountInfo.ValidCount;
                            int aroundRightCount = answerCountInfo.HeaderCountInfo.RightCount + answerCountInfo.FooterCountInfo.RightCount;
                            if (aroundVaildCount != 0)//只有分录题才有外围
                            {
                                var aroundPercent = 0m;
                                //说明：案例中同步实训 统一设置权重 没有 20200611 lwj
                                //if (caseSet.SetType == AccountEntryCalculationType.SetAll)//案例设置为“统一设置权重”
                                //{
                                //    aroundPercent = Math.Round((decimal)caseSet.OutVal / (decimal)(caseSet.InVal + caseSet.OutVal), 4, MidpointRounding.AwayFromZero);
                                //}
                                //else//案例设置为“单独设置权重”
                                {
                                    var certificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>()
                                        .Where(s => s.TopicId == topic.Id)
                                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicIdAll}{topic.Id}", true, 10 * 60)
                                        .Result;

                                    aroundPercent = Math.Round((decimal)certificateTopic.OutVal / (decimal)(certificateTopic.InVal + certificateTopic.OutVal), 4, MidpointRounding.AwayFromZero);
                                }
                                var aroundTotalScore = Math.Round(score * aroundPercent, 4, MidpointRounding.AwayFromZero);
                                score = score - aroundTotalScore;
                                aroundScored = aroundTotalScore / aroundVaildCount * aroundRightCount;
                            }
                            var otherValidCount = answerCountInfo.TotalCountInfo.ValidCount - aroundVaildCount;
                            var otherRightCount = answerCountInfo.TotalCountInfo.RightCount - aroundRightCount;
                            if (otherValidCount != 0)
                                answerScore = score / otherValidCount * otherRightCount;
                            answerScore = Math.Round(answerScore + aroundScored, 4, MidpointRounding.AwayFromZero);
                            break;
                        case CalculationType.FreeCalculation:
                            answerScore = answerCountInfo.TotalCountInfo.Scored;
                            break;
                        case CalculationType.CalculatedRow:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlRows, score);
                            break;
                        case CalculationType.CalculatedColumn:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlColumns, score);
                            break;
                    }
                    answerScore = GetNonholonomicScore(answerCountInfo, answerScore, scale);
                    status = answerCountInfo.TotalCountInfo.ValidCount == answerCountInfo.TotalCountInfo.RightCount ? AnswerResultStatus.Right : (answerCountInfo.TotalCountInfo.RightCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
                    examResultDto.AnswerCountInfo = answerCountInfo;
                    examResultDto.AnswerResultJson = HttpUtility.UrlEncode(JsonHelper.SerializeObject(answerJObj));
                    examResultDto.AnswerResult = status;
                    //if (examResultDto.AnswerResult == AnswerResultStatus.Right)
                    //{
                    //    examResultDto.AnswerScore = score;
                    //}
                    //else
                    //{
                    //    examResultDto.AnswerScore = answerScore;
                    //}

                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MainSubQuestion:
                    examResultDto = MainSubQuestionCompare(answer, examResultDto);

                    break;

            }
            UpdateScores(answer, examResultDto);
            return examResultDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        private static decimal CalcRowsScored(List<AnswerRow> rows, decimal score)
        {
            if (rows == null || rows.Count == 0)
                return 0;
            var columnTotalCount = (decimal)rows.Count;
            //2019-9-7
            //var rigthColumnCount = rows.Count(m => m.Items.All(i => i.IsRight && i.IsValid));
            var rigthColumnCount = (decimal)rows.Count(m => m.Items.All(i => i.IsRight || !i.IsValid) && m.Items.Any(i => i.IsRight));
            return Math.Round(rigthColumnCount / columnTotalCount * score, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 获取非完整得分
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="scored"></param>
        /// <param name="accountEntryScale"></param>
        /// <param name="fillGridScale"></param>
        /// <returns></returns>
        private static decimal GetNonholonomicScore(AnswerCountInfo answerCountInfo, decimal scored, decimal scale)
        {
            if (scale == 0)
                return scored;
            if (answerCountInfo.TotalCountInfo.ValidCount != answerCountInfo.TotalCountInfo.RightCount)//非完整性得分判断
            {
                return Math.Round(scored * scale / 100m, 4, MidpointRounding.AwayFromZero);
            }
            return Math.Round(scored, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 多题型答案比较
        /// </summary>
        /// <param name="examResultDto"></param>
        private QuestionResultNew MainSubQuestionCompare(QuestionAnswerNew answer, QuestionResultNew examResultDto)
        {
            var allItemCount = 0;
            var rightItemCount = 0;
            var answerScore = 0m;
            examResultDto.MultiQuestionResult = new List<QuestionResultNew>();
            if (answer.MultiQuestionAnswers == null || answer.MultiQuestionAnswers.Count == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }

            var multiTopic = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.ParentId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync().Result;

            //var multiTopic = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(s => s.ParentId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
            //           .OrderBy(s => s.Sort)
            //           .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId}{answer.QuestionId}", true, 10 * 60, true).Result;

            foreach (var multiItem in multiTopic)
            {
                var answerItem = answer.MultiQuestionAnswers.FirstOrDefault(m => m.QuestionId == multiItem.Id);
                if (answerItem == null)
                    continue;
                var compareRst = CompareTopicAnswer(multiItem, answerItem);
                if (compareRst.AnswerCountInfo == null)
                {
                    if (compareRst.AnswerResult == AnswerResultStatus.Right)
                        rightItemCount++;
                    allItemCount++;
                }
                else
                {
                    allItemCount += compareRst.AnswerCountInfo.TotalCountInfo.ValidCount;
                    rightItemCount += compareRst.AnswerCountInfo.TotalCountInfo.RightCount;
                }
                examResultDto.MultiQuestionResult.Add(compareRst);
            }
            if (allItemCount == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }
            answerScore = examResultDto.MultiQuestionResult.Sum(s => s.AnswerScore);
            examResultDto.AnswerResult = allItemCount == rightItemCount ? AnswerResultStatus.Right : (rightItemCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
            examResultDto.AnswerScore = answerScore;
            return examResultDto;
        }

        /// <summary>
        /// 添加作答记录
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="examResultDto"></param>
        private void UpdateScores(QuestionAnswerNew answer, QuestionResultNew examResultDto)
        {
            if (answer.IsIgnore)
            {
                return;
            }

            var gradeDetailId = answer.GradeDetailId;
            if (gradeDetailId > 0)
            {
                var gradeDetail = new ExamStudentGradeDetailNew() { Id = gradeDetailId };
                gradeDetail.Answer = answer.AnswerValue;
                gradeDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                gradeDetail.AccountEntryStatus = answer.AccountEntryStatus;
                gradeDetail.UpdateTime = DateTime.Now;
                gradeDetail.Status = examResultDto.AnswerResult;
                gradeDetail.Score = examResultDto.AnswerScore;
                gradeDetail.AnswerCompareInfo = examResultDto.AnswerResultJson;

                DataMergeHelper.PushData("UpdateScores-gradeDetailNew", gradeDetail, (arr) =>
                {
                    var details = arr.Cast<ExamStudentGradeDetailNew>().ToList();
                    DbContext.FreeSql.GetRepository<ExamStudentGradeDetailNew>().UpdateDiy.SetSource(details)
                  .UpdateColumns(a => new { a.Status, a.Score, a.AnswerCompareInfo, a.Answer, a.AccountEntryAuditStatus, a.AccountEntryStatus, a.UpdateTime }).ExecuteAffrows();
                });
            }
        }

        #endregion

        #endregion
    }
}
