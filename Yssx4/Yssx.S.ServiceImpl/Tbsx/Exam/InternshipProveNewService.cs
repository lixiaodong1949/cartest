﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 实习证书 - 同步实训
    /// </summary>
    public class InternshipProveNewService : IInternshipProveNewService
    {
        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<JobTrainingEndRecordDto>> GetJobTrainingEndRecord(JobTrainingEndRecordRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetGuidRepository<ExamStudentGradeNew>().Select
                    .From<ExamPaperNew, YssxCaseNew, YssxUser, YssxInternshipProve>((a, b, c, d, e) =>
                        a.InnerJoin(aa => aa.ExamId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => b.CaseId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(aa => aa.Id == e.GradeId && e.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d, e) => a.ExamId == model.ExamId && a.UserId == currentUserId && a.Status == StudentExamStatus.End && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, b, c, d, e) => a.CreateTime)
                    .ToList((a, b, c, d, e) => new JobTrainingEndRecordDto
                    {
                        GradeId = a.Id,
                        ExamId = b.Id,
                        Name = c.Name,
                        SubmitTime = a.SubmitTime.Value,
                        ExamPaperScore = a.ExamPaperScore,
                        Score = a.Score,
                        SN = e.SN,
                        Url = e.Url
                    });

                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<JobTrainingEndRecordDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }


        /// <summary>
        /// 实习列表 - 同步实习
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetSyncInternshipListDto>> GetSyncInternshipList(GetInternshipListRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.GetRepository<YssxCaseNew>().Select.From<ExamPaperNew>((a, b) =>
                    a.InnerJoin(aa => aa.Id == b.CaseId && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.IsActive && b.UserId == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .OrderByDescending((a, b) => b.CreateTime);

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new GetSyncInternshipListDto
                {
                    CaseId = a.Id,
                    ExamId = b.Id,
                    GradeId = 0,
                    ProductId = 0,
                    Img = a.Img,
                    LogoUrl = a.LogoUrl,
                    CaseName = a.Name,
                    ShortDesc = a.BriefName,
                    BeginTime = b.BeginTime,
                    EndTime = b.EndTime
                });
                //明细数据
                foreach (var item in selectData)
                {
                    //作答记录
                    var rGradeEntity = DbContext.FreeSql.GetGuidRepository<ExamStudentGradeNew>()
                        .Where(x => x.ExamId == item.ExamId && x.Status != StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                        .OrderByDescending(x => x.CreateTime).First();
                    item.GradeId = rGradeEntity == null ? 0 : rGradeEntity.Id;
                    //课程
                    var rCourseBindCase = DbContext.FreeSql.GetRepository<YssxCourseBindCase>().Select.Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CaseId == item.CaseId).First();
                    item.CourseId = rCourseBindCase == null ? 0 : rCourseBindCase.CourseId;
                    if (item.CourseId > 0)
                    {
                        var rCourseEntity = DbContext.FreeSql.GetRepository<YssxCourseNew>().Select.Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.Id == item.CourseId).First();
                        item.CourseName = rCourseEntity == null ? "" : rCourseEntity.CourseName;
                    }
                    //产品ID
                    var rProductId = DbContext.FreeSql.Select<YssxMallProductItem, YssxMallProductItem>()
                        .InnerJoin((a, b) => a.ProductId == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                        .Where((a, b) => a.TargetId == item.CaseId && a.Category == 1 && a.Source == 2 && a.IsDelete == CommonConstants.IsNotDelete)
                        .GroupBy((a, b) => a.ProductId).Having(x => x.Count() == 1)
                        .Select(x => x.Key).FirstOrDefault();
                    item.ProductId = rProductId;
                }

                return new PageResponse<GetSyncInternshipListDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 获取游客模式试卷信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GetSyncInternshipExamInfoDto>> GetSyncInternshipExamInfo(long caseId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                GetSyncInternshipExamInfoDto resData = new GetSyncInternshipExamInfoDto();
                //试卷ID
                var rExamPaperNew = DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.Where(x => x.CaseId == caseId && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                resData.IsPaid = rExamPaperNew != null && currentUserId != CommonConstants.DefaultUserId ? true : false;
                if (rExamPaperNew == null)
                    rExamPaperNew = DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.Where(x => x.CaseId == caseId && x.UserId == CommonConstants.DefaultUserId && x.IsDelete == CommonConstants.IsNotDelete).First();

                resData.ExamId = rExamPaperNew == null ? 0 : rExamPaperNew.Id;
                //作答记录
                if (resData.ExamId > 0 && currentUserId != CommonConstants.DefaultUserId)
                {
                    var rGradeEntity = DbContext.FreeSql.GetGuidRepository<ExamStudentGradeNew>()
                        .Where(x => x.ExamId == resData.ExamId && x.Status != StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                        .OrderByDescending(x => x.CreateTime).First();
                    resData.GradeId = rGradeEntity == null ? 0 : rGradeEntity.Id;
                }
                //课程
                var rCourseBindCase = DbContext.FreeSql.GetRepository<YssxCourseBindCase>().Select.Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CaseId == caseId).First();
                resData.CourseId = rCourseBindCase == null ? 0 : rCourseBindCase.CourseId;

                var courseData = DbContext.FreeSql.GetRepository<YssxCourseNew>().Select.Where(x => x.Id == resData.CourseId).First();
                resData.CourseName = courseData == null ? null : courseData.CourseName;

                //产品ID
                var rProductId = DbContext.FreeSql.Select<YssxMallProductItem, YssxMallProductItem>()
                    .InnerJoin((a, b) => a.ProductId == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.TargetId == caseId && a.Category == 1 && a.Source == 2 && a.IsDelete == CommonConstants.IsNotDelete)
                    .GroupBy((a, b) => a.ProductId).Having(x => x.Count() == 1)
                    .Select(x => x.Key).FirstOrDefault();
                ////第二种拆解步骤方法
                //var rProductItem = DbContext.FreeSql.Select<YssxMallProductItem>().Where(x => x.TargetId == caseId && x.Category == 1 && x.IsDelete == CommonConstants.IsNotDelete).ToList(x => x.ProductId);
                //var rProductId = DbContext.FreeSql.Select<YssxMallProductItem>().Where(x => rProductItem.Contains(x.ProductId) && x.IsDelete == CommonConstants.IsNotDelete)
                //    .GroupBy(x => x.ProductId).Having(x => x.Count() == 1).Select(x => x.Key).FirstOrDefault();

                resData.ProductId = rProductId;

                return new ResponseContext<GetSyncInternshipExamInfoDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        #region 增加案例访问量
        /// <summary>
        /// 增加案例访问量
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddCasePageview(long caseId)
        {
            bool state = false;

            YssxResourceActivity entity = await DbContext.FreeSql.GetRepository<YssxResourceActivity>().Where(x => x.ResourceId == caseId && x.ActivityType == ActivityType.Read
                && x.StatisticsResourceType == StatisticsResourceType.Case).FirstAsync();

            if (entity == null)
            {
                entity = new YssxResourceActivity();
                entity.Id = IdWorker.NextId();
                entity.ResourceId = caseId;
                entity.StatisticsResourceType = StatisticsResourceType.Case;
                entity.ActivityType = ActivityType.Read;
                entity.Number = 1;
                entity.CreateTime = DateTime.Now;
                entity.UpdateTime = DateTime.Now;

                YssxResourceActivity model = await DbContext.FreeSql.GetRepository<YssxResourceActivity>().InsertAsync(entity);
                state = model != null;
            }
            else
            {
                entity.Number = entity.Number + 1;
                entity.UpdateTime = DateTime.Now;

                state = await DbContext.FreeSql.GetRepository<YssxResourceActivity>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.Number,
                    x.UpdateTime,
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取公司案例列表(开启状态并被绑定课程)
        /// <summary>
        /// 获取公司案例列表(开启状态并被绑定课程)
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetCompanyCaseListDto>>> GetCompanyCaseList()
        {
            ResponseContext<List<GetCompanyCaseListDto>> response = new ResponseContext<List<GetCompanyCaseListDto>>();

            List<GetCompanyCaseListDto> list = await DbContext.FreeSql.Select<YssxCaseNew>().From<YssxCourseBindCase, YssxCourseNew, YssxResourceActivity>((a, b, c, d) =>
                 a.InnerJoin(x => x.Id == b.CaseId)
                  .InnerJoin(x => b.CourseId == c.Id)
                .LeftJoin(x => x.Id == d.ResourceId && d.ActivityType == ActivityType.Read && d.StatisticsResourceType == StatisticsResourceType.Case))
                .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete && a.IsActive == true && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b, c, d) => new GetCompanyCaseListDto
                {
                    CaseId = a.Id,
                    CaseName = a.Name,
                    CourseName = c.CourseName,
                    ShortDesc = c.Intro,
                    LogoUrl = a.LogoUrl,
                    ReadNumber = d.Number
                });

            response.Data = list;

            return response;
        }
        #endregion

    }
}
