﻿using AspectCore.DynamicProxy;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.Service
{
    /// <summary>
    /// 题目业务服务
    /// </summary>
    public class TopicServiceNew : ITopicServiceNew
    {
        #region 单选题、多选题、判断题、表格题、填空题、票据题

        /// <summary>
        /// 添加简单类型题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequestNew model, bkUserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;
            YssxTopicNew oldTopic = null;
            #region 校验
            if (model.Id > 0)
            {
                oldTopic = await DbContext.FreeSql.GetRepository<YssxTopicNew>()
                .Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

                if (oldTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }
            }

            var flow = await DbContext.FreeSql.GetRepository<YssxFlow>()
              .Where(m => m.Id == model.FlowId && m.IsDelete == CommonConstants.IsNotDelete)
              .FirstAsync();

            if (flow == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "流程不存在" };
            }
            #endregion
            var topic = model.MapTo<YssxTopicNew>();
            topic.IsGzip = CommonConstants.IsGzip;
            topic.DrillTypeId = flow.DrillTypeId;
            topic.CreateBy = opreationId;
            topic.CreateTime = dtNow;
            if (model.Id == 0) { topic.Id = IdWorker.NextId(); }

            #region 处理选项排序
            //选项信息
            List<YssxAnswerOptionNew> addAnswerOptions = null;
            List<YssxAnswerOptionNew> updateAnswerOptions = null;
            var tuple = CreateAnswerOption(model.Options, topic);
            if (tuple != null)
            {
                addAnswerOptions = tuple.Item1;
                updateAnswerOptions = tuple.Item2;
            }
            #endregion

            if (model.Id == 0)//新增
            {
                #region 添加题目，选项，题目类别关联信息

                //添加到数据库
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加题目
                        await uow.GetRepository<YssxTopicNew>().InsertAsync(topic);
                        //题目选项和答案
                        if (addAnswerOptions != null && addAnswerOptions.Count > 1)
                            await uow.GetRepository<YssxAnswerOptionNew>().InsertAsync(addAnswerOptions);

                        // 修改任务题目总数
                        UpdateFlowTopic(uow, topic.FlowId, topic.Score, 1, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }
            else//修改
            {
                topic.UpdateTime = dtNow;
                topic.UpdateBy = opreationId;

                #region 取删除的选项

                List<long> existOptionId = null;
                if (updateAnswerOptions != null && updateAnswerOptions.Count > 0)
                    existOptionId = updateAnswerOptions.Select(a => a.Id).ToList();

                var delOption = await DbContext.FreeSql.GetRepository<YssxAnswerOptionNew>(m => m.IsDelete == CommonConstants.IsNotDelete)
                  .Where(m => m.TopicId == topic.Id && m.IsDelete == CommonConstants.IsNotDelete)
                  .WhereIf(existOptionId != null && existOptionId.Count > 0, m => !existOptionId.Contains(m.Id))
                  .ToListAsync();

                if (delOption.Count > 0)
                {
                    delOption.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
                #endregion

                #region 更新题目,选项(更新，添加)。题目类别关联信息不允许修改
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_t = DbContext.FreeSql.GetRepository<YssxTopicNew>();
                        var repo_ao = DbContext.FreeSql.GetRepository<YssxAnswerOptionNew>();

                        //更新题目
                        await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();

                        #region 选项(更新，添加)
                        //添加
                        if (addAnswerOptions != null && addAnswerOptions.Count > 0)
                        {
                            await repo_ao.InsertAsync(addAnswerOptions);
                        }
                        //删除的选项
                        if (delOption != null && delOption.Count > 0)
                        {
                            await repo_ao.UpdateDiy.SetSource(delOption).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //更新选项
                        if (updateAnswerOptions != null && updateAnswerOptions.Count > 0)
                            await repo_ao.UpdateDiy.SetSource(updateAnswerOptions).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();

                        #endregion

                        // 修改任务题目总数
                        UpdateFlowTopic(uow, topic.FlowId, topic.Score - oldTopic.Score, 0, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score - oldTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion

            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };

        }

        /// <summary>
        ///  添加复杂类型题目（表格题、填空题、票据题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequestNew model, bkUserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;

            var repo_t2 = DbContext.FreeSql.GetRepository<YssxTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete);
            var flow = await DbContext.FreeSql.GetRepository<YssxFlow>()
             .Where(m => m.Id == model.FlowId && m.IsDelete == CommonConstants.IsNotDelete)
             .FirstAsync();

            if (flow == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "流程不存在" };
            }

            var topic = model.MapTo<YssxTopicNew>();
            topic.DrillTypeId = flow.DrillTypeId;
            topic.IsGzip = CommonConstants.IsGzip;
            if (model.Id <= 0)//新增
            {
                topic.Id = IdWorker.NextId();
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;
            }
            else
            {
                topic.UpdateTime = DateTime.Now;
                topic.UpdateBy = opreationId;
            }

            #region 文件排序
            //添加附件
            var files = new List<YssxTopicFileNew>();
            topic.TopicFileIds = CreateTopicFile(model.QuestionFile, topic, files);//生成题目附件数据
            #endregion

            if (model.Id == 0)//新增
            {
                //添加到数据库
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加题目
                        await uow.GetRepository<YssxTopicNew>().InsertAsync(topic);
                        //添加附件
                        await uow.GetRepository<YssxTopicFileNew>().InsertAsync(files);

                        // 修改流程的题目总数
                        UpdateFlowTopic(uow, topic.FlowId, topic.Score, 1, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            else//修改
            {
                var oldTopic = await DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (oldTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }

                #region 取删除的文件
                var delTopicFile = new List<YssxTopicFileNew>();
                if (!String.IsNullOrEmpty(oldTopic.TopicFileIds))
                {
                    var rOldfileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                    var rNewFileIds = files.Select(m => m.Id);
                    var rDelFileIds = rOldfileIds.Where(m => !rNewFileIds.Contains(m));
                    if (rDelFileIds.Count() > 0)
                    {
                        delTopicFile = await DbContext.FreeSql.Select<YssxTopicFileNew>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rDelFileIds.Contains(m.Id)).ToListAsync();
                        if (delTopicFile.Count > 0)
                        {
                            delTopicFile.ForEach(a =>
                            {
                                a.IsDelete = CommonConstants.IsDelete;
                                a.UpdateBy = opreationId;
                                a.UpdateTime = dtNow;
                            });
                        }
                    }
                }
                #endregion

                #region 更新题目，更新文件顺序字段，添加文件顺序
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_tf = uow.GetRepository<YssxTopicFileNew>();
                        var repo_t = uow.GetRepository<YssxTopicNew>();
                        if (delTopicFile.Count > 0)
                        {
                            await repo_tf.UpdateDiy.SetSource(delTopicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //添加附件
                        await repo_tf.InsertAsync(files);

                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //更新题目
                        await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();

                        //修改任务题目总数
                        UpdateFlowTopic(uow, topic.FlowId, (topic.Score - oldTopic.Score), 0, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score - oldTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };
        }

        #endregion

        #region 综合题  添加 修改

        /// <summary>
        /// 添加多题型题目（综合题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequestNew model, bkUserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;

            #region 校验
            //TODO 校验
            var rSxCase = await DbContext.FreeSql.GetRepository<YssxCaseNew>().Where(m => m.Id == model.CaseId && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (rSxCase == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到案例信息" };
            }
            var oldTopic = new YssxTopicNew();
            if (model.Id > 0)
            {
                var rSxTopic = DbContext.FreeSql.GetRepository<YssxTopicNew>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                if (!rSxTopic.Any())
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }
                oldTopic = await rSxTopic.FirstAsync();
            }

            var flow = await DbContext.FreeSql.GetRepository<YssxFlow>()
             .Where(m => m.Id == model.FlowId && m.IsDelete == CommonConstants.IsNotDelete)
             .FirstAsync();

            if (flow == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "流程不存在" };
            }

            if (model.SubQuestion.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目为0" };
            //验证题目类型是否有效
            var rIsInvalidQuestionType = model.SubQuestion
                .Where(m => m.QuestionType == QuestionType.SingleChoice
                        || m.QuestionType == QuestionType.MultiChoice
                        || m.QuestionType == QuestionType.Judge
                        || m.QuestionType == QuestionType.FillGrid
                        || m.QuestionType == QuestionType.AccountEntry
                        || m.QuestionType == QuestionType.FillBlank
                        || m.QuestionType == QuestionType.FillGraphGrid).Count();
            if (rIsInvalidQuestionType != model.SubQuestion.Count)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目类型有误" };
            }
            #endregion

            //支持选择题-选项
            var mainTopic = model.MapTo<YssxTopicNew>();
            mainTopic.IsGzip = CommonConstants.IsGzip;
            mainTopic.DrillTypeId = flow.DrillTypeId;

            if (model.Id == 0)
            {
                mainTopic.CreateBy = opreationId;
                mainTopic.CreateTime = dtNow;
                mainTopic.Id = IdWorker.NextId();
                mainTopic.GroupId = IdWorker.NextId();
            }
            else
            {
                mainTopic.CaseId = oldTopic.CaseId;
                mainTopic.GroupId = oldTopic.GroupId;
                mainTopic.UpdateTime = dtNow;
                mainTopic.UpdateBy = opreationId;
            }

            #region 文件排序
            //添加附件--用于排序需要
            var files = new List<YssxTopicFileNew>();
            if (model.QuestionFile != null)
            {
                mainTopic.TopicFileIds = CreateTopicFile(model.QuestionFile, mainTopic, files);//生成题目附件数据
            }

            #endregion

            if (model.Id == 0)//新增题目
            {
                //子题目 排序 添加父节点ID
                #region 构建子题数据
                List<YssxTopicNew> subTopics = null;
                List<YssxCertificateTopicNew> certificateTopics = null;
                List<YssxCertificateDataRecordNew> certificateDataRecords = null;
                List<YssxAnswerOptionNew> answerOptions = null;
                var tuple1 = CreateSubQuestion(model.SubQuestion, mainTopic);
                if (tuple1 != null)
                {
                    subTopics = tuple1.Item1;
                    certificateTopics = tuple1.Item2;
                    certificateDataRecords = tuple1.Item3;
                    answerOptions = tuple1.Item4;
                }
                #endregion

                #region 添加到数据库中
                //添加到数据库中
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_t = uow.GetRepository<YssxTopicNew>();
                        var repo_tf = uow.GetRepository<YssxTopicFileNew>();
                        var repo_ao = uow.GetRepository<YssxAnswerOptionNew>();
                        var repo_ct = uow.GetRepository<YssxCertificateTopicNew>();
                        var repo_cdr = uow.GetRepository<YssxCertificateDataRecordNew>();
                        //添加题目
                        await repo_t.InsertAsync(mainTopic);

                        if (files != null && files.Count > 0)  //添加附件                              
                            await repo_tf.InsertAsync(files);

                        //添加子题目
                        if (subTopics != null && subTopics.Count > 0)
                            await repo_t.InsertAsync(subTopics);

                        //添加子题目-选项
                        if (answerOptions != null && answerOptions.Count > 0)
                            await repo_ao.InsertAsync(answerOptions);

                        //添加分录题内容
                        if (certificateTopics != null && certificateTopics.Count > 0)
                        {
                            repo_ct.Insert(certificateTopics);
                            repo_cdr.Insert(certificateDataRecords);
                        }

                        // 修改任务题目总数
                        UpdateFlowTopic(uow, mainTopic.FlowId, mainTopic.Score, 1, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, mainTopic.DrillTypeId, mainTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }
            else//修改题目
            {
                //子题目 排序 添加父节点ID
                #region 处理数据
                var certificateDataRecords = new List<YssxCertificateDataRecordNew>();
                var addanswerOptions = new List<YssxAnswerOptionNew>(); ;//题目选项和答案
                var delanswerOptions = new List<YssxAnswerOptionNew>();//题目选项和答案
                var updateanswerOptions = new List<YssxAnswerOptionNew>();//题目选项和答案                                       
                List<YssxTopicNew> addSubTopic = null;//需要新添加的子题
                List<YssxTopicNew> updateSubTopic = null; //需要修改的子题
                List<YssxTopicNew> rDelSubQuestion = null; //需要删除的子题

                List<YssxCertificateTopicNew> addcertificateTopics = new List<YssxCertificateTopicNew>();
                List<YssxCertificateTopicNew> updatecertificateTopics = new List<YssxCertificateTopicNew>();

                var addSubQuestion = model.SubQuestion//需要新添加的子题
                  .Where(a => a.Id == 0).ToList();
                //所有新添加的子题
                var tuple1 = CreateSubQuestion(addSubQuestion, mainTopic);
                if (tuple1 != null)//
                {
                    addSubTopic = tuple1.Item1;
                    addcertificateTopics = tuple1.Item2;
                    if (tuple1.Item3 != null)
                        certificateDataRecords.AddRange(tuple1.Item3);
                    if (tuple1.Item4 != null)
                        addanswerOptions = tuple1.Item4;
                }

                //所有修改的子题
                var updateSubQuestion = model.SubQuestion //需要修改的子题
                  .Where(a => a.Id > 0).ToList();
                var tuple2 = CreateSubQuestion(updateSubQuestion, mainTopic);
                if (tuple2 != null)
                {
                    updateSubTopic = tuple2.Item1;
                    updatecertificateTopics = tuple2.Item2;
                    if (tuple1.Item3 != null)
                        certificateDataRecords.AddRange(tuple2.Item3);
                    if (tuple2.Item4 != null)
                        addanswerOptions.AddRange(tuple2.Item4);

                    updateanswerOptions = tuple2.Item5;
                }

                #endregion

                #region 子题目 取待删除数据
                List<long> existIds = null;
                if (updateSubQuestion != null && updateSubQuestion.Count > 0)
                {
                    existIds = updateSubQuestion.Select(a => a.Id).ToList();
                }
                rDelSubQuestion = await DbContext.FreeSql.GetRepository<YssxTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(m => m.ParentId == mainTopic.Id)
                .WhereIf(existIds != null && existIds.Count > 0, m => !existIds.Contains(m.Id))
                .ToListAsync();

                if (rDelSubQuestion != null && rDelSubQuestion.Count > 0)
                    rDelSubQuestion.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                #endregion
                #region 取子题目-选项 取待删除数据 
                //
                List<long> topicIds = new List<long>();
                List<long> optionIds = new List<long>();
                if (updateanswerOptions != null && updateanswerOptions.Count > 0)
                {
                    var utopicIds = updateanswerOptions.Select(a => a.TopicId).Distinct().ToList();
                    if (utopicIds != null && utopicIds.Count > 0)
                        topicIds.AddRange(utopicIds);

                    optionIds = updateanswerOptions.Select(a => a.Id).ToList();

                }
                if (rDelSubQuestion != null && rDelSubQuestion.Count > 0)
                {
                    var d = rDelSubQuestion.Select(e => e.Id).ToList();
                    if (d != null && d.Count > 0)
                        topicIds.AddRange(d);
                }
                if (topicIds != null && topicIds.Count > 0)
                {
                    delanswerOptions = await DbContext.FreeSql.GetRepository<YssxAnswerOptionNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(a => topicIds.Contains(a.TopicId))
                    .WhereIf(optionIds != null && optionIds.Count > 0, a => !optionIds.Contains(a.Id))
                    .ToListAsync();
                    if (delanswerOptions != null && delanswerOptions.Count > 0)
                        delanswerOptions.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                }

                #endregion

                #region 更新题目，更新文件顺序字段，添加文件顺序
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        List<long> delDrTopicids = new List<long>();

                        var repo_t = uow.GetRepository<YssxTopicNew>();
                        var repo_tf = uow.GetRepository<YssxTopicFileNew>();
                        var repo_ao = uow.GetRepository<YssxAnswerOptionNew>();
                        var repo_ct = uow.GetRepository<YssxCertificateTopicNew>();
                        var repo_cdr = uow.GetRepository<YssxCertificateDataRecordNew>();

                        #region 附件 处理 删除旧附件 再添加新的附件
                        if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                        {
                            var fileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                            await repo_tf.DeleteAsync(a => fileIds.Contains(a.Id));

                            //删除旧的题目附件
                            await repo_tf.UpdateDiy.Set(c => new YssxTopicFileNew
                            {
                                IsDelete = CommonConstants.IsDelete,
                                UpdateTime = DateTime.Now,
                                UpdateBy = currentUser.Id,
                            }).Where(a => fileIds.Contains(a.Id)).ExecuteAffrowsAsync();
                        }
                        //添加附件
                        await repo_tf.InsertAsync(files);
                        #endregion

                        //更新主题目
                        await repo_t.UpdateDiy.SetSource(mainTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.QuestionContentType, a.GroupId }).ExecuteAffrowsAsync();

                        #region 子题目操作 删除不存在的题目  添加新题目 更新已存在题目
                        //删除不存在的题目
                        if (rDelSubQuestion != null && rDelSubQuestion.Any())
                        {
                            await repo_t.UpdateDiy.SetSource(rDelSubQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //添加新题目
                        if (addSubTopic != null && addSubTopic.Any())
                        {
                            await repo_t.InsertAsync(addSubTopic);
                        }
                        //更新已存在题目
                        if (updateSubTopic != null && updateSubTopic.Any())
                        {
                            var ids = updateSubTopic.Where(e => e.QuestionType == QuestionType.AccountEntry).Select(e => e.Id).ToList();
                            if (ids != null && ids.Count > 0)
                                delDrTopicids.AddRange(ids);

                            //更新题目内容
                            await repo_t.UpdateDiy.SetSource(updateSubTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                        }
                        #endregion

                        #region 子题目-选择题-选项 选项(删除，更新，添加)
                        //删除选项
                        if (delanswerOptions != null && delanswerOptions.Any())
                        {
                            await repo_ao.UpdateDiy.SetSource(delanswerOptions).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //添加
                        if (addanswerOptions != null && addanswerOptions.Any())
                        {
                            await repo_ao.InsertAsync(addanswerOptions);
                        }
                        //更新选项
                        if (updateanswerOptions != null && updateanswerOptions.Any())
                        {
                            await repo_ao.UpdateDiy.SetSource(updateanswerOptions).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                        }
                        #endregion
                        #region 分录题
                        if (addcertificateTopics != null && addcertificateTopics.Count > 0)
                        {
                            await repo_ct.InsertAsync(addcertificateTopics);
                        }
                        if (updatecertificateTopics != null && updatecertificateTopics.Count > 0)
                        {

                            await repo_ct.UpdateDiy.SetSource(updatecertificateTopics).IgnoreColumns(e => new { e.CreateBy, e.CreateTime }).ExecuteAffrowsAsync();
                        }

                        if (rDelSubQuestion != null && rDelSubQuestion.Count > 0)
                        {
                            List<long> delIds = rDelSubQuestion.Where(e => e.QuestionType == QuestionType.AccountEntry).Select(e => e.Id).ToList();

                            if (delIds != null && delIds.Count > 0)
                            {
                                delDrTopicids.AddRange(delIds);
                                await repo_ct.UpdateDiy.SetSource(new YssxCertificateTopicNew
                                {
                                    IsDelete = CommonConstants.IsDelete,
                                    UpdateTime = DateTime.Now,
                                    UpdateBy = currentUser.Id,
                                }).Where(e => e.IsDelete == CommonConstants.IsNotDelete && delIds.Contains(e.TopicId)).ExecuteAffrowsAsync();
                            }
                        }

                        if (delDrTopicids != null && delDrTopicids.Count > 0)
                        {
                            await repo_cdr.UpdateDiy.SetSource(new YssxCertificateDataRecordNew
                            {
                                IsDelete = CommonConstants.IsDelete,
                                UpdateTime = DateTime.Now,
                                UpdateBy = currentUser.Id,
                            }).Where(e => e.IsDelete == CommonConstants.IsNotDelete && delDrTopicids.Contains(e.TopicId)).ExecuteAffrowsAsync();
                        }
                        if (certificateDataRecords != null && certificateDataRecords.Count > 0)
                        {
                            await repo_cdr.InsertAsync(certificateDataRecords);
                        }

                        #endregion
                        // 修改任务题目总数
                        UpdateFlowTopic(uow, mainTopic.FlowId, mainTopic.Score - oldTopic.Score, 0, currentUser);
                        //修改实训类型题目总分
                        UpdateDrillTypeTopic(uow, mainTopic.DrillTypeId, mainTopic.Score - oldTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = mainTopic.Id.ToString() };

        }

        private static Tuple<List<YssxTopicNew>, List<YssxCertificateTopicNew>, List<YssxCertificateDataRecordNew>, List<YssxAnswerOptionNew>, List<YssxAnswerOptionNew>> CreateSubQuestion
            (List<SubQuestionNew> subQuestions, YssxTopicNew mainTopic)
        {
            var topics = new List<YssxTopicNew>();

            if (subQuestions == null && mainTopic == null)
                return null;

            var certificateTopics = new List<YssxCertificateTopicNew>();
            var certificateDatas = new List<YssxCertificateDataRecordNew>();
            var answerOptions = new List<YssxAnswerOptionNew>();
            //选项信息
            var addAnswerOptions = new List<YssxAnswerOptionNew>();
            var updateAnswerOptions = new List<YssxAnswerOptionNew>();

            subQuestions.ForEach(a =>
            {
                YssxTopicNew topic = CreateSubQuestion(a, mainTopic);
                topics.Add(topic);

                if (topic.QuestionType == QuestionType.AccountEntry)//分录题，构建分录独立的数据
                {
                    var certificateTopic = CreateYssxCertificateTopic(a, topic);

                    if (a.DataRecords != null && a.DataRecords.Count > 0)
                    {
                        // 构建录题凭证数据记录表
                        var yssxCertificateDatas = CreateCertificateDataRecord(a.DataRecords, certificateTopic);
                        if (yssxCertificateDatas != null && yssxCertificateDatas.Count > 0)
                            certificateDatas.AddRange(yssxCertificateDatas);
                    }
                    certificateTopics.Add(certificateTopic);

                }
                #region 题目答案
                if (a.Options != null && a.Options.Any())
                {
                    var tuple = CreateAnswerOption(a.Options, topic);
                    if (tuple != null)
                    {
                        if (tuple.Item1 != null && tuple.Item1.Count > 0)
                            addAnswerOptions.AddRange(tuple.Item1);
                        if (tuple.Item1 != null && tuple.Item1.Count > 0)
                            updateAnswerOptions.AddRange(tuple.Item2);
                    }
                }
                #endregion
            });
            return new Tuple<List<YssxTopicNew>, List<YssxCertificateTopicNew>, List<YssxCertificateDataRecordNew>, List<YssxAnswerOptionNew>, List<YssxAnswerOptionNew>>
                (topics, certificateTopics, certificateDatas, addAnswerOptions, updateAnswerOptions);
        }

        /// <summary>
        /// 构建综合题的子题对象，新增和修改
        /// </summary>
        /// <param name="subQuestion"></param>
        /// <param name="mainTopic"></param>
        /// <returns></returns>
        private static YssxTopicNew CreateSubQuestion(SubQuestionNew subQuestion, YssxTopicNew mainTopic)
        {
            var item = subQuestion.MapTo<YssxTopicNew>();

            item.ParentId = mainTopic.Id;
            item.GroupId = mainTopic.GroupId;
            item.CaseId = mainTopic.CaseId;
            item.IsGzip = CommonConstants.IsGzip;
            item.PositionId = 0;
            item.FlowId = mainTopic.FlowId;
            item.DrillTypeId = mainTopic.DrillTypeId;
            if (subQuestion.Id <= 0)//新增
            {
                item.Id = IdWorker.NextId();
            }
            return item;
        }

        private static YssxCertificateTopicNew CreateYssxCertificateTopic(SubQuestionNew subQuestion, YssxTopicNew topic)
        {
            var certificateTopic = subQuestion.MapTo<YssxCertificateTopicNew>();
            certificateTopic.TopicId = topic.Id;
            certificateTopic.CaseId = topic.CaseId;
            certificateTopic.FlowId = topic.FlowId;
            certificateTopic.DrillTypeId = topic.DrillTypeId;

            if (subQuestion.Id <= 0)
            {
                certificateTopic.Id = IdWorker.NextId();
                certificateTopic.CreateBy = topic.CreateBy;
            }
            else
            {
                certificateTopic.UpdateBy = topic.UpdateBy;
                certificateTopic.UpdateTime = DateTime.Now;

                var old = DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(e => e.TopicId == topic.Id)
                    .First();

                certificateTopic.Id = old.Id;
            }

            return certificateTopic;
        }

        /// <summary>
        /// 构建录题凭证数据记录表
        /// </summary>
        /// <param name="dataRecords"></param>
        /// <param name="sxCertificateTopic"></param>
        /// <returns></returns>
        private static List<YssxCertificateDataRecordNew> CreateCertificateDataRecord(List<CertificateDataRecordNew> dataRecords, YssxCertificateTopicNew sxCertificateTopic)
        {
            List<YssxCertificateDataRecordNew> yssxCertificateDatas = null;
            if (dataRecords != null && dataRecords.Count > 0)
            {
                yssxCertificateDatas = new List<YssxCertificateDataRecordNew>();

                for (int i = 0; i < dataRecords.Count; i++)
                {
                    var item = dataRecords[i];

                    yssxCertificateDatas.Add(new YssxCertificateDataRecordNew
                    {
                        Id = IdWorker.NextId(),//设置id
                        BorrowAmount = item.BorrowAmount,
                        CreditorAmount = item.CreditorAmount,
                        SubjectId = item.SubjectId,
                        SummaryInfo = item.SummaryInfo,
                        CreateBy = sxCertificateTopic.CreateBy,
                        CaseId = sxCertificateTopic.CaseId,
                        FlowId = sxCertificateTopic.FlowId,
                        TopicId = sxCertificateTopic.TopicId,
                        CertificateTopicId = sxCertificateTopic.Id,
                        CertificateDate = sxCertificateTopic.CertificateDate,
                        CertificateNo = sxCertificateTopic.CertificateNo,
                        DrillTypeId = sxCertificateTopic.DrillTypeId,
                        //CertificateWord = sxCertificateTopic.CertificateWord.GetDescription(),
                    });
                }
            }

            return yssxCertificateDatas;
        }

        /// <summary>
        /// 构建题目的选项+答案对象，新增和修改
        /// </summary>
        /// <param name="choiceOption"></param>
        /// <param name="topic"></param>
        /// <param name="options"></param>
        private static Tuple<List<YssxAnswerOptionNew>, List<YssxAnswerOptionNew>> CreateAnswerOption(List<ChoiceOptionNew> choiceOption, YssxTopicNew topic)
        {
            if (choiceOption == null)
                return null;
            List<YssxAnswerOptionNew> addoptions = new List<YssxAnswerOptionNew>();
            List<YssxAnswerOptionNew> updateoptions = new List<YssxAnswerOptionNew>();
            var loop = 0;
            choiceOption.ForEach(a =>
            {
                var option = a.MapTo<YssxAnswerOptionNew>();
                option.AnswerKeysContent = topic.AnswerValue;
                option.AnswerOption = a.Text?.Trim();
                option.TopicId = topic.Id;
                option.TopicParentId = topic.ParentId;
                option.CaseId = topic.CaseId;
                option.FlowId = topic.FlowId;
                option.AnswerKeysContent = "";
                option.AnswerFileUrl = "";
                option.DrillTypeId = topic.DrillTypeId;

                if (option.Sort == 0)
                {
                    option.Sort = ++loop;
                }

                if (a.Id <= 0)//新增
                {
                    option.Id = IdWorker.NextId();
                    addoptions.Add(option);
                }
                else
                {
                    updateoptions.Add(option);
                }
            });
            return new Tuple<List<YssxAnswerOptionNew>, List<YssxAnswerOptionNew>>(addoptions, updateoptions);
        }

        /// <summary>
        /// 生成题目附件数据
        /// </summary>
        /// <param name="files">附件源数据</param>
        /// <param name="caseId">案例id</param>
        /// <param name="topicId">主题目id</param>
        /// <param name="topicFiles">返回的题目附件集合</param>
        /// <param name="parentTopicId">父题目id</param>
        /// <returns></returns>
        private static string CreateTopicFile(List<QuestionFileNew> files, YssxTopicNew topic, List<YssxTopicFileNew> topicFiles)
        {
            if (files == null || files.Count == 0)//附件为空           
                return null;

            StringBuilder stringBuilder = new StringBuilder();

            if (topicFiles == null)
                topicFiles = new List<YssxTopicFileNew>();
            var loop = 0;
            foreach (var item in files)
            {
                YssxTopicFileNew file = item.MapTo<YssxTopicFileNew>();

                file.Id = IdWorker.NextId();
                file.CaseId = topic.CaseId;
                file.TopicId = topic.Id;
                file.FlowId = topic.FlowId;
                file.DrillTypeId = topic.DrillTypeId;
                file.CreateBy = topic.CreateBy;

                if (file.Sort == 0)
                    file.Sort = ++loop;

                topicFiles.Add(file);
                stringBuilder.Append(file.Id).Append(",");
            }

            return stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
        }

        #endregion

        #region 分录题 添加 修改

        /// <summary>
        /// 保存分录题 添加和修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequestNew model, bkUserTicket currentUser)
        {
            if (model.Id <= 0)
            {
                return await AddCertificateTopic(model, currentUser);
            }
            else
            {
                return await UpdateCertificateTopic(model, currentUser);
            }
        }

        /// <summary>
        /// 分录题 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> AddCertificateTopic(CertificateTopicRequestNew model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            var flow = await DbContext.FreeSql.GetRepository<YssxFlow>()
             .Where(m => m.Id == model.FlowId && m.IsDelete == CommonConstants.IsNotDelete)
             .FirstAsync();

            if (flow == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "流程不存在" };
            }


            List<YssxTopicFileNew> topicFiles = new List<YssxTopicFileNew>();

            #region 记账凭证题目
            //记账凭证题目（分录题）
            var tuple = CreateCertificateTopic(model, topicFiles, currentUser, flow);

            YssxTopicNew topic = tuple.Item1;
            YssxCertificateTopicNew yssxCore = tuple.Item2;
            List<YssxCertificateDataRecordNew> yssxCertificateData = tuple.Item3;
            //List<YssxSxCertificateAssistAccountingNew> assistAccountings = tuple.Item4;//科目辅助核算信息


            #endregion

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetGuidRepository<YssxTopicNew>();
                    var repo_ct = uow.GetGuidRepository<YssxCertificateTopicNew>();
                    var repo_tf = uow.GetGuidRepository<YssxTopicFileNew>();
                    var repo_cdr = uow.GetGuidRepository<YssxCertificateDataRecordNew>();
                    //var repo_tpai = uow.GetGuidRepository<SxTopicPayAccountInfo>();

                    await repo_t.InsertAsync(topic);//记账凭证内容
                    await repo_ct.InsertAsync(yssxCore);//记账凭证内容

                    if (yssxCertificateData != null)
                        await repo_cdr.InsertAsync(yssxCertificateData);//记账凭证内容
                                                                        //if (assistAccountings != null)
                                                                        //    await uow.GetGuidRepository<YssxSxCertificateAssistAccountingNew>().InsertAsync(assistAccountings);//科目辅助核算信息

                    if (topicFiles.Count > 0)
                        await repo_tf.InsertAsync(topicFiles);//题目附件，包含主题和子题的所有附件

                    // 修改任务题目总数
                    UpdateFlowTopic(uow, topic.FlowId, topic.Score, 1, currentUser);
                    //修改实训类型题目总分
                    UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score, currentUser);
                    uow.Commit();

                    result.Msg = topic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            return result;
        }

        /// <summary>
        /// 分录题 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> UpdateCertificateTopic(CertificateTopicRequestNew model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            List<YssxTopicFileNew> topicFiles = new List<YssxTopicFileNew>();
            var repo_t = DbContext.FreeSql.GetRepository<YssxTopicNew>(e => e.IsDelete != CommonConstants.IsDelete);
            #region 记账凭证
            var oldtopic = await repo_t
                      .Where(e => e.Id == model.Id).FirstAsync();

            if (oldtopic == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{model.Id}", false);

            var oldyssxCoreId = await DbContext.FreeSql.GetRepository<YssxCertificateTopicNew>()
                               .Where(e => e.TopicId == model.Id && e.IsDelete != CommonConstants.IsDelete).FirstAsync(e => e.Id);

            if (oldyssxCoreId <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，sxCertificateTopic Id:{model.Id}", false);

            var flow = await DbContext.FreeSql.GetRepository<YssxFlow>()
               .Where(m => m.Id == model.FlowId && m.IsDelete == CommonConstants.IsNotDelete)
               .FirstAsync();

            if (flow == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "流程不存在" };
            }

            //记账凭证题目（分录题）
            var tuple = CreateCertificateTopic(model, topicFiles, currentUser, flow, oldyssxCoreId);// 构建修改分录题的修改对象
            var topic = tuple.Item1;
            var yssxCore = tuple.Item2;
            var yssxCertificateData = tuple.Item3;
            //List<YssxSxCertificateAssistAccountingNew> assistAccountings = tuple.Item4;//科目辅助核算信息

            #endregion

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_tf = uow.GetRepository<YssxTopicFileNew>();
                    var repo_cdr = uow.GetRepository<YssxCertificateDataRecordNew>();
                    //var repo_taa = uow.GetGuidRepository<YssxCertificateAssistAccountingNew>();
                    var repo_t2 = uow.GetRepository<YssxTopicNew>();

                    #region 记账凭证
                    //修改题目内容 Topic
                    await repo_t2.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync();
                    //修改记账凭证内容 CertificateTopic
                    await uow.GetRepository<YssxCertificateTopicNew>().UpdateDiy.SetSource(yssxCore).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                    //删除旧的录题凭证数据记录
                    await repo_cdr.UpdateDiy.Set(c => new YssxCertificateDataRecordNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.CertificateTopicId == yssxCore.Id).ExecuteAffrowsAsync();
                    //添加新的录题凭证数据
                    if (yssxCertificateData != null)
                        await repo_cdr.InsertAsync(yssxCertificateData);

                    #endregion

                    #region 题目附件
                    //删除旧的题目附件
                    await repo_tf.UpdateDiy.Set(c => new YssxTopicFileNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(a => a.TopicId == topic.Id).ExecuteAffrowsAsync();
                    //添加新的题目文件
                    if (topicFiles.Count > 0)
                        await repo_tf.InsertAsync(topicFiles);//题目附件，包含主题和子题的所有附件
                    #endregion

                    UpdateFlowTopic(uow, topic.FlowId, topic.Score - oldtopic.Score, 0, currentUser);
                    //修改实训类型题目总分
                    UpdateDrillTypeTopic(uow, topic.DrillTypeId, topic.Score - oldtopic.Score, currentUser);

                    uow.Commit();
                    result.Msg = topic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion
            return result;
        }

        /// <summary>
        /// 生成记账凭证 (添加和修改)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mainTopic"></param>
        /// <param name="topicFiles"></param>
        /// <param name="certificateTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="certificateTopicId">certificateTopic表中的id(修改时需要赋值)</param>
        private static Tuple<YssxTopicNew, YssxCertificateTopicNew, List<YssxCertificateDataRecordNew>> CreateCertificateTopic
            (CertificateTopicRequestNew model, List<YssxTopicFileNew> topicFiles, bkUserTicket currentUser, YssxFlow flow, long certificateTopicId = 0)
        {
            //记账凭证
            var obj = model.MapTo<YssxTopicNew>();
            obj.DrillTypeId = flow.DrillTypeId;

            obj.QuestionType = QuestionType.AccountEntry;
            //obj.TaskId = mainTopic.TaskId;
            //obj.IsShowHint = mainTopic.IsShowHint;
            //obj.IsShowAnswer = mainTopic.IsShowAnswer;
            //obj.TopicRelationStatus = mainTopic.TopicRelationStatus;
            //obj.SettlementType = mainTopic.SettlementType;

            YssxCertificateTopicNew certificateTopic = model.MapTo<YssxCertificateTopicNew>();
            certificateTopic.Title = model.Title;
            //certificateTopic.SummaryInfos = model.SummaryInfos;
            certificateTopic.CaseId = obj.CaseId;
            certificateTopic.FlowId = obj.FlowId;
            certificateTopic.DepartmentId = obj.DepartmentId;

            if (obj.Id <= 0) //新增
            {
                obj.Id = IdWorker.NextId();//获取唯一id
                obj.CreateBy = currentUser.Id;

                certificateTopicId = certificateTopic.Id = IdWorker.NextId();//获取唯一id
                certificateTopic.CreateBy = currentUser.Id;
            }
            else//修改
            {
                obj.UpdateBy = currentUser.Id;
                obj.UpdateTime = DateTime.Now;

                certificateTopic.Id = certificateTopicId;//修改的时候赋值原id
                certificateTopic.UpdateBy = currentUser.Id;
                certificateTopic.UpdateTime = DateTime.Now;
            }

            certificateTopic.TopicId = obj.Id; //不能移动
            // 构建录题凭证数据记录表
            List<YssxCertificateDataRecordNew> yssxCertificateDatas = CreateCertificateDataRecord(model.DataRecords, certificateTopic);
            ////科目辅助核算信息
            //List<SxCertificateAssistAccounting> assistAccountings = CreateCertificateAssistAccounting(model.AssistAccountings, caseId, obj.Id, mainTopic.CreateBy, obj.TaskId);

            if (model.QuestionFile != null && model.QuestionFile.Count > 0)
            {
                obj.TopicFileIds = CreateTopicFile(model.QuestionFile, obj, topicFiles);//生成票据附件数据

                certificateTopic.InvoicesNum = model.QuestionFile.Count;
            }

            return new Tuple<YssxTopicNew, YssxCertificateTopicNew, List<YssxCertificateDataRecordNew>>(obj, certificateTopic, yssxCertificateDatas);
        }


        #endregion

        #region 题目查询

        /// <summary>
        /// 获取分录题 信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CertificateTopicRequestNew>> GetCertificateById(long id)
        {
            var result = new ResponseContext<CertificateTopicRequestNew>(CommonConstants.ErrorCode, "", null);

            var repo_t = DbContext.FreeSql.GetGuidRepository<YssxTopicNew>(e => e.IsDelete != CommonConstants.IsDelete);

            //主题目内容
            var obj = await repo_t
                .Where(e => e.Id == id)
                .FirstAsync();

            if (obj == null)
            {
                result.Msg = "题目不存在。";
                return result;
            }


            var topic = obj.MapTo<CertificateTopicRequestNew>();//记账凭证内容

            var sxCertificateTopic = await DbContext.FreeSql.GetGuidRepository<YssxCertificateTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                  .Where(e => e.TopicId == obj.Id)
                  .FirstAsync();

            if (sxCertificateTopic != null)
            {
                topic.AccountingManager = sxCertificateTopic.AccountingManager;
                topic.Auditor = sxCertificateTopic.Auditor;
                topic.Cashier = sxCertificateTopic.Cashier;
                topic.OutVal = sxCertificateTopic.OutVal;
                topic.InVal = sxCertificateTopic.InVal;
                topic.IsDisableAM = sxCertificateTopic.IsDisableAM;
                topic.IsDisableAuditor = sxCertificateTopic.IsDisableAuditor;
                topic.IsDisableCashier = sxCertificateTopic.IsDisableCashier;
                topic.IsDisableCreator = sxCertificateTopic.IsDisableCreator;
                topic.Creator = sxCertificateTopic.Creator;
                topic.CertificateNo = sxCertificateTopic.CertificateNo;
                topic.CertificateDate = sxCertificateTopic.CertificateDate;

                //单据数据记录
                topic.DataRecords = await DbContext.FreeSql.GetGuidRepository<YssxCertificateDataRecordNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(e => e.CertificateTopicId == sxCertificateTopic.Id)
                    .ToListAsync<CertificateDataRecordNew>();
            }

            //题目附件，包含主题和子题的所有附件
            if (!string.IsNullOrWhiteSpace(obj.TopicFileIds))//文件不为空，获取文件信息
            {
                topic.QuestionFile = await DbContext.FreeSql.GetGuidRepository<YssxTopicFileNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(e => e.TopicId == obj.Id)
                    .ToListAsync<QuestionFileNew>();//题目附件，包含主题和子题的所有附件
            }

            result.Data = topic;
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicInfoDtoNew>> GetTopicInfoById(long id)
        {
            return await Task.Run(() =>
            {
                var question = DbContext.FreeSql.Select<YssxTopicNew>()
                .Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete)
                .First();

                if (question == null)
                {
                    return new ResponseContext<TopicInfoDtoNew> { Code = CommonConstants.ErrorCode, Msg = "未找到相关题目" };
                }

                var questionDTO = question.MapTo<TopicInfoDtoNew>();

                if (questionDTO.QuestionType == QuestionType.SingleChoice
                    || questionDTO.QuestionType == QuestionType.MultiChoice
                    || questionDTO.QuestionType == QuestionType.Judge)
                {
                    //添加选项信息
                    var options = DbContext.FreeSql.Select<YssxAnswerOptionNew>()
                .Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete)
                .ToList();
                    questionDTO.Options = new List<ChoiceOptionNew>();
                    foreach (var item in options)
                    {
                        questionDTO.Options.Add(new ChoiceOptionNew()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Text = item.AnswerOption,
                            AttatchImgUrl = item.AnswerFileUrl
                        });
                    }
                }
                else
                {
                    //添加附件信息
                    questionDTO.QuestionFile = new List<QuestionFileNew>();

                    if (!string.IsNullOrEmpty(question.TopicFileIds))
                    {
                        var fileIds = question.TopicFileIds.SplitToArray<long>(',');
                        var files = DbContext.FreeSql.Select<YssxTopicFileNew>()
                        .Where(a => fileIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(m => m.Sort)
                        .OrderBy(m => m.CreateTime)
                        .ToList();
                        foreach (var item in files)
                        {
                            questionDTO.QuestionFile.Add(new QuestionFileNew()
                            {
                                Url = item.Url,
                                Name = item.Name,
                                //Sort = item.Sort
                            });
                        }
                    }
                    if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                    {
                        var subQuestions = DbContext.FreeSql.Select<YssxTopicNew>()
                            .Where(m => m.ParentId == questionDTO.Id && m.IsDelete == CommonConstants.IsNotDelete)
                            .OrderBy(m => m.Sort)
                            .OrderBy(m => m.CreateTime)
                            .ToList(a => new SubQuestionNew(a.Id,
                                                         a.QuestionType,
                                                         a.QuestionContentType,
                                                         "",
                                                         a.Hint, a.Content,
                                                         a.TopicContent,
                                                         a.FullContent,
                                                         a.AnswerValue,
                                                         a.Sort, a.Score, a.PartialScore,
                                                         a.CalculationType));

                        var choiceQuestionIds = subQuestions
                            .Where(a => a.QuestionType == QuestionType.SingleChoice
                                || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge)
                            .Select(a => a.Id)
                            .ToArray();

                        if (choiceQuestionIds.Any())
                        {
                            var optionAll = DbContext.FreeSql.Select<YssxAnswerOptionNew>().
                            Where(a => choiceQuestionIds.Contains(a.TopicId) && a.IsDelete == CommonConstants.IsNotDelete)
                            .ToList();

                            subQuestions.ForEach(a =>
                            {
                                var options = optionAll.Where(c => c.TopicId == a.Id).Select(d => new ChoiceOptionNew()
                                {
                                    Id = d.Id,
                                    Name = d.Name,
                                    Text = d.AnswerOption,
                                    AttatchImgUrl = d.AnswerFileUrl
                                });
                                if (options.Any())
                                {
                                    a.Options = new List<ChoiceOptionNew>();
                                    a.Options.AddRange(options);
                                }
                            });
                        }
                        //分录题处理
                        subQuestions.ForEach(a =>
                        {
                            if (a.QuestionType == QuestionType.AccountEntry)
                            {
                                var sxCertificateTopic = DbContext.FreeSql.GetGuidRepository<YssxCertificateTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                                        .Where(e => e.TopicId == a.Id)
                                        .First();

                                if (sxCertificateTopic != null)
                                {
                                    a.AccountingManager = sxCertificateTopic.AccountingManager;
                                    a.OutVal = sxCertificateTopic.OutVal;
                                    a.InVal = sxCertificateTopic.InVal;
                                    a.Auditor = sxCertificateTopic.Auditor;
                                    a.Cashier = sxCertificateTopic.Cashier;
                                    a.IsDisableAM = sxCertificateTopic.IsDisableAM;
                                    a.IsDisableAuditor = sxCertificateTopic.IsDisableAuditor;
                                    a.IsDisableCashier = sxCertificateTopic.IsDisableCashier;
                                    a.IsDisableCreator = sxCertificateTopic.IsDisableCreator;
                                    a.Creator = sxCertificateTopic.Creator;
                                    a.CertificateNo = sxCertificateTopic.CertificateNo;
                                    a.CertificateDate = sxCertificateTopic.CertificateDate;
                                    //单据数据记录
                                    a.DataRecords = DbContext.FreeSql.GetGuidRepository<YssxCertificateDataRecordNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                                        .Where(e => e.CertificateTopicId == sxCertificateTopic.Id)
                                        .ToList<CertificateDataRecordNew>();
                                }
                            }
                        });

                        questionDTO.SubQuestion = new List<SubQuestionNew>();
                        questionDTO.SubQuestion.AddRange(subQuestions);
                    }
                }

                return new ResponseContext<TopicInfoDtoNew> { Code = CommonConstants.SuccessCode, Data = questionDTO };
            });
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto)
        {
            var result = new ResponseContext<TopicListPageDto>(CommonConstants.SuccessCode, "", null);

            if (queryDto.CaseId <= 0)
            {
                result.Msg = "CaseId无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            var repo_t = DbContext.FreeSql.GetRepository<YssxTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == queryDto.CaseId);

            //数据统计和总数统计
            var selectStatistics = repo_t
                .Where(e => e.ParentId == 0)
                .WhereIf(queryDto.FlowId > 0, e => e.FlowId == queryDto.FlowId)
                .WhereIf(queryDto.DrillTypeId > 0, e => e.DrillTypeId == queryDto.DrillTypeId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value);

            //分页查询符合条件的数据
            var selectData = repo_t
                .Where(e => e.ParentId == 0)
                .WhereIf(queryDto.FlowId > 0, e => e.FlowId == queryDto.FlowId)
                .WhereIf(queryDto.DrillTypeId > 0, e => e.DrillTypeId == queryDto.DrillTypeId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value);

            long totalCount = 0;
            //分页查询符合条件的数据
            var rTopicData = await selectData
                .Count(out totalCount)
                .OrderBy(m => m.Sort)
                .OrderBy(m => m.CreateTime)
                .From<YssxPositionNew>((a, b) => a.LeftJoin(aa => aa.PositionId == b.Id))
                .Page(queryDto.PageIndex, queryDto.PageSize)
                .ToListAsync((a, b) => new TopicListDataDto
                {
                    Id = a.Id,
                    QuestionType = a.QuestionType,
                    PositionId = a.PositionId,
                    Score = a.Score,
                    Status = a.Status,
                    Title = a.Title,
                    Sort = a.Sort,
                    PositionName = b.Name,
                });

            //统计每个公司岗位题目分数
            var totalScore = await selectStatistics.SumAsync(e => e.Score);//统计总分



            //构建返回值
            result.Data = new TopicListPageDto
            {
                TotalCount = totalCount,
                TotalScore = totalScore,
                //PositionQuestionDetail = positions,
                Detail = new PageResponse<TopicListDataDto>
                {
                    PageSize = queryDto.PageSize,
                    PageIndex = queryDto.PageIndex,
                    RecordCount = totalCount,
                    Data = rTopicData
                }
            };

            return result;
        }

        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto)
        {
            //分页查询符合条件的数据
            var selectData = DbContext.FreeSql.GetRepository<YssxTopicNew>()
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.ParentId == 0)
                .WhereIf(queryDto.CaseId > 0, e => e.CaseId == queryDto.CaseId)
                //.WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .OrderBy(e => e.Sort)
                .OrderBy(e => e.CreateTime);

            //分页查询符合条件的数据
            var rTopicData = await selectData
                .ToListAsync(a => new TopicListDataDto
                {
                    Id = a.Id,
                    //BusinessDate = a.BusinessDate,
                    QuestionType = a.QuestionType,
                    PositionId = a.PositionId,
                    Score = a.Score,
                    Status = a.Status,
                    Title = a.Title,
                    Sort = a.Sort,
                });

            return new ResponseContext<List<TopicListDataDto>>(CommonConstants.SuccessCode, "", rTopicData);
        }

        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto)
        {
            //分页查询符合条件的数据
            var list = await DbContext.FreeSql.GetRepository<YssxTopicNew>()
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(queryDto.CaseId > 0, e => e.CaseId == queryDto.CaseId)
                //.WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .OrderBy(e => e.Sort)
                .OrderBy(e => e.CreateTime)
                .ToListAsync<TopicNamesResponseDto>();

            var rTopicData = list.Where(a => a.ParentId == 0).ToList();//查询父题目

            rTopicData.ForEach(a =>
            {

                a.Subs = list.Where(b => b.ParentId == a.Id).Select(b => new TopicNamesSubResponseDto
                {
                    Id = b.Id,
                    QuestionTypeName = b.QuestionType.GetDescription(),
                    ParentId = b.ParentId,
                    QuestionType = b.QuestionType,
                    Title = b.Title
                }).ToList();

            });


            return new ResponseContext<List<TopicNamesResponseDto>>(CommonConstants.SuccessCode, "", rTopicData);
        }

        #endregion

        #region 题目删除

        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTopicById(long id, bkUserTicket currentUser)
        {
            var r = new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };

            //校验
            //TODO-题目删除限制，临时去掉！！！！
            //var any = DbContext.FreeSql.Select<sxExamStudentGradeDetail>().Any(a => a.QuestionId == id && a.IsDelete == CommonConstants.IsNotDelete);
            //if (any)
            //{
            //    return new ResponseContext<bool>(CommonConstants.ErrorCode, "该题目已经被用户使用，无法删除！", false);
            //}

            #region 删除题目
            //取题目
            var question = await DbContext.FreeSql.Select<YssxTopicNew>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (question == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到题目信息" };
            }

            switch (question.QuestionType)
            {
                case QuestionType.AccountEntry:
                    r = await DeleteCertificateTopic(question, currentUser);
                    break;
                case QuestionType.FinancialStatements:
                case QuestionType.SingleChoice:
                case QuestionType.MultiChoice:
                case QuestionType.Judge:
                case QuestionType.FillGrid:
                case QuestionType.FillBlank:
                case QuestionType.FillGraphGrid:
                case QuestionType.MainSubQuestion:
                case QuestionType.SettleAccounts:
                case QuestionType.GridFillBank:
                default:
                    r = await DeleteTopicById(question, currentUser);
                    break;
            }

            return r;

        }

        /// <summary>
        /// 默认删除题目方法
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteTopicById(YssxTopicNew topic, bkUserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;
            long id = topic.Id;

            topic.IsDelete = CommonConstants.IsDelete;
            topic.UpdateBy = opreationId;
            topic.UpdateTime = dtNow;
            //取选项
            var option = new List<YssxAnswerOptionNew>();
            if (topic.QuestionType == QuestionType.SingleChoice || topic.QuestionType == QuestionType.MultiChoice || topic.QuestionType == QuestionType.Judge)
            {
                option = await DbContext.FreeSql.Select<YssxAnswerOptionNew>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                if (option.Count == 0)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到选项信息" };
                }
                option.ForEach(a =>
                                {
                                    a.IsDelete = CommonConstants.IsDelete;
                                    a.UpdateBy = opreationId;
                                    a.UpdateTime = dtNow;
                                });
            }
            //取子题目（题目列表、题目选项、题目附件）
            var subQuestion = new List<YssxTopicNew>();
            var subOptionList = new List<YssxAnswerOptionNew>();
            var subTopicFileList = new List<YssxTopicFileNew>();
            var subCertificateTopicList = new List<YssxCertificateTopicNew>();
            var rCertificateTopic = new YssxCertificateTopicNew();

            //综合题
            if (topic.QuestionType == QuestionType.MainSubQuestion)
            {
                subQuestion = await DbContext.FreeSql.Select<YssxTopicNew>()
                    .Where(m => m.ParentId == id && m.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();

                if (subQuestion.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到子题目信息" }; }
                foreach (var itemSub in subQuestion)
                {
                    itemSub.IsDelete = CommonConstants.IsDelete;
                    itemSub.UpdateBy = opreationId;
                    itemSub.UpdateTime = dtNow;

                    //取子题目 - 选项
                    if (itemSub.QuestionType == QuestionType.SingleChoice || itemSub.QuestionType == QuestionType.MultiChoice || itemSub.QuestionType == QuestionType.Judge)
                    {
                        //简单题
                        var subOption = await DbContext.FreeSql.Select<YssxAnswerOptionNew>()
                            .Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete)
                            .ToListAsync();
                        if (subOption.Count > 0) subOptionList.AddRange(subOption);
                    }
                    //取文件
                    if (!string.IsNullOrEmpty(itemSub.TopicFileIds))
                    {
                        var idArr = itemSub.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                        if (idArr.Any())
                        {
                            var subTopicFile = await DbContext.FreeSql.Select<YssxTopicFileNew>()
                                .Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete)
                                .ToListAsync();
                            if (subTopicFile.Count > 0) subTopicFileList.AddRange(subTopicFile);
                        }
                    }
                }
                //更新字段
                if (subOptionList.Count > 0)
                {
                    subOptionList.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
                if (subTopicFileList.Count > 0)
                {
                    subTopicFileList.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
            }
            //取文件
            var topicFile = new List<YssxTopicFileNew>();
            if (!string.IsNullOrEmpty(topic.TopicFileIds))
            {
                var idArr = topic.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                if (idArr.Any())
                {
                    topicFile = await DbContext.FreeSql.Select<YssxTopicFileNew>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                }
                if (topicFile.Count > 0)
                {
                    topicFile.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
            }
            #endregion
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                var repo_t = uow.GetRepository<YssxTopicNew>();
                var repo_ao = uow.GetRepository<YssxAnswerOptionNew>();
                var repo_tf = uow.GetRepository<YssxTopicFileNew>();

                //删除题目
                await repo_t.UpdateDiy.SetSource(topic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                //删除选项
                if (topic.QuestionType == QuestionType.SingleChoice || topic.QuestionType == QuestionType.MultiChoice || topic.QuestionType == QuestionType.Judge)
                {
                    await repo_ao.UpdateDiy.SetSource(option).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }
                //删除文件
                if (topicFile.Count > 0)
                {
                    await repo_tf.UpdateDiy.SetSource(topicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }

                //删除【综合题】
                if (topic.QuestionType == QuestionType.MainSubQuestion)
                {
                    //删除题目
                    await repo_t.UpdateDiy.SetSource(subQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();

                    //删除简单题·选项
                    if (subOptionList.Count > 0)
                        await repo_ao.UpdateDiy.SetSource(subOptionList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();

                    //删除文件
                    if (subTopicFileList.Count > 0)
                        await repo_tf.UpdateDiy.SetSource(subTopicFileList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }
                // 修改任务题目总数
                UpdateFlowTopic(uow, topic.FlowId, -topic.Score, -1, currentUser);
                //修改实训类型题目总分
                UpdateDrillTypeTopic(uow, topic.DrillTypeId, -topic.Score, currentUser);

                uow.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };
        }

        /// <summary>
        /// 删除综合分录题
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteCertificateTopic(YssxTopicNew topic, bkUserTicket currentUser)
        {
            long topicId = topic.Id;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetRepository<YssxTopicNew>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_tf = uow.GetRepository<YssxTopicFileNew>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cft = uow.GetRepository<YssxCertificateTopicNew>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cdr = uow.GetRepository<YssxCertificateDataRecordNew>(c => c.IsDelete == CommonConstants.IsNotDelete);

                    await repo_t.UpdateDiy.Set(c => new YssxTopicNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(c => c.Id == topicId).ExecuteAffrowsAsync();

                    //删除题目图片
                    if (!string.IsNullOrWhiteSpace(topic.TopicFileIds))
                    {
                        await repo_tf.UpdateDiy.Set(c => new YssxTopicFileNew
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(c => c.TopicId == topicId).ExecuteAffrowsAsync();
                    }

                    await repo_cft.UpdateDiy.Set(c => new YssxCertificateTopicNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();
                    await repo_cdr.UpdateDiy.Set(c => new YssxCertificateDataRecordNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();


                    // 修改任务题目总数
                    UpdateFlowTopic(uow, topic.FlowId, -topic.Score, -1, currentUser);
                    //修改实训类型题目总分
                    UpdateDrillTypeTopic(uow, topic.DrillTypeId, -topic.Score, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topicId.ToString() };
        }

        #endregion

        /// <summary>
        /// 修改流程相关的信息
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="num">增加的题目数量（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateFlowTopic(FreeSql.IRepositoryUnitOfWork uow, long flowId, decimal score, int num, bkUserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (flowId == 0)
                throw new ArgumentException("long flowId 无效");

            if (currentUser == null)
                throw new NullReferenceException("bkUserTicket currentUser");

            var repo_task = uow.GetRepository<YssxFlow>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new YssxFlow
            {
                TopicTotalNum = c.TopicTotalNum + num,
                TopicTotalSore = c.TopicTotalSore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == flowId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改实训类型题目总分 前台
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateDrillTypeTopic(FreeSql.IRepositoryUnitOfWork uow, long drillTypeId, decimal score, bkUserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (drillTypeId == 0)
                throw new ArgumentException("long drillTypeId 无效");

            if (currentUser == null)
                throw new NullReferenceException("bkUserTicket currentUser");

            var repo_task = uow.GetRepository<YssxDrillType>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new YssxDrillType
            {
                TopicTotalSore = c.TopicTotalSore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == drillTypeId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<QueryCaseTopicDto>>> GetTopicList(QueryTopicRequestDto queryDto)
        {
            var result = new ResponseContext<List<QueryCaseTopicDto>>(CommonConstants.SuccessCode, "", null);

            if (queryDto.CaseId <= 0)
            {
                result.Msg = "CaseId无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            var repo_t = DbContext.FreeSql.GetRepository<YssxTopicNew>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == queryDto.CaseId);

            //查询符合条件的数据
            var rTopicData = await repo_t
                .Where(e => e.ParentId == 0)
                .WhereIf(queryDto.DrillTypeId > 0, e => e.DrillTypeId == queryDto.DrillTypeId)
                .WhereIf(queryDto.FlowId > 0, e => e.FlowId == queryDto.FlowId)
                .WhereIf(queryDto.DepartmentId > 0, e => e.DepartmentId == queryDto.DepartmentId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .From<YssxFlow, YssxDepartmentNew>((a, b, c) => a.InnerJoin(aa => aa.FlowId == b.Id && b.Status == Status.Enable).LeftJoin(aa => aa.DepartmentId == c.Id))
                .ToListAsync((a, b, c) => new CaseTopicDto
                {
                    TopicId = a.Id,
                    FlowId = a.FlowId,
                    FlowName = b.Name,
                    AdviceTime = a.AdviceTime,
                    DepartmentId = a.DepartmentId,
                    DepartmentName = c.DepartmentName,
                    Score = a.Score,
                    Title = a.Title,
                    TopicSort = a.Sort,
                    FlowSort = b.Sort,
                });

            var rdata = new List<QueryCaseTopicDto>();

            if (rTopicData != null && rTopicData.Count > 0)
            {
                foreach (var item in rTopicData.GroupBy(e => e.FlowId))
                {
                    var topic = item.FirstOrDefault();

                    rdata.Add(new QueryCaseTopicDto
                    {
                        FlowId = item.Key,
                        FlowName = topic?.FlowName,
                        Sort = topic == null ? 0 : topic.FlowSort,
                        Topics = item.OrderBy(e => e.TopicSort).ToList()
                    });
                }
                rdata = rdata.OrderBy(e => e.Sort).ToList();
            }

            //构建返回值
            result.Data = rdata;

            return result;
        }
    }
}
