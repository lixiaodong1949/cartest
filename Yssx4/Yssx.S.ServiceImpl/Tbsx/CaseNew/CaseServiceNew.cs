﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.Service
{
    /// <summary>
    /// 案例
    /// </summary>
    public class CaseServiceNew : ICaseServiceNew
    {
        #region 案例
        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCase(long id, bkUserTicket currentUser)
        {
            YssxCaseNew sxCase = await DbContext.FreeSql.Select<YssxCaseNew>().Where(c => c.Id == id).FirstAsync();
            if (sxCase == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该案例！", false);
            DbContext.FreeSql.Transaction(() =>
            {
                DbContext.FreeSql.GetRepository<YssxCaseNew>().UpdateDiy.Set(c => new YssxCaseNew
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUser.Id
                }).Where(c => c.Id == id).ExecuteAffrowsAsync();
                DbContext.FreeSql.GetRepository<YssxCaseRegulationNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new YssxCaseRegulationNew
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUser.Id
                }).Where(c => c.CaseId == id).ExecuteAffrowsAsync();
            });
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CaseDtoNew>> GetCase(long id)
        {
            var querySxCase = await DbContext.FreeSql.GetRepository<YssxCaseNew>(c => c.IsDelete == CommonConstants.IsNotDelete)
                .Where(c => c.Id == id)
                .FirstAsync<CaseDtoNew>();

            if (querySxCase == null)
            {
                return new ResponseContext<CaseDtoNew>(CommonConstants.ErrorCode, "案例不存在。", querySxCase);
            }

            //案例制度
            var caseRegulation = await DbContext.FreeSql.GetRepository<YssxCaseRegulationNew>(c => c.IsDelete == CommonConstants.IsNotDelete)
                .Where(c => c.CaseId == id)
                .FirstAsync<YssxCaseRegulationNew>();

            if (caseRegulation != null)
            {
                querySxCase.FinancialRegulation = caseRegulation.Content;
            }

            //案例岗位
            querySxCase.PositionDtos = await DbContext.FreeSql.GetRepository<YssxCasePositionNew>(c => c.IsDelete == CommonConstants.IsNotDelete)
              .Where(c => c.CaseId == id)
              .From<YssxPositionNew>((a, b) => a.LeftJoin(aa => aa.PostionId == b.Id))
              .ToListAsync((a, b) => new PositionManageDtoNew
              {
                  Name = b.Name,
                  PositionId = a.PostionId,
                  Remark = b.Describe
              });

            return new ResponseContext<CaseDtoNew>(CommonConstants.SuccessCode, "", querySxCase);
        }

        /// <summary>
        /// 根据查询条件获取案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<GetCaseListDtoNew>> GetCaseList(GetCaseInput input)
        {
            var query = DbContext.FreeSql.GetRepository<YssxCaseNew>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Name), c => c.Name.Contains(input.Name));
            //    .WhereIf(input.IndustryId.HasValue, c => c.IndustryId == input.IndustryId)
            //    .WhereIf(!string.IsNullOrWhiteSpace(input.Scale), c => c.Scale.Contains(input.Scale))
            //    .WhereIf(input.Taxpayer.HasValue, c => c.Taxpayer == input.Taxpayer);

            long totalCount = 0;

            var list = await query
                .Count(out totalCount)
                .OrderBy(a => a.Sort)
                .OrderBy(a => a.Id)
                .Page(input.PageIndex, input.PageSize)
                .ToListAsync<GetCaseListDtoNew>();

            var pageData = new PageResponse<GetCaseListDtoNew>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = list };

            return pageData;
        }

        /// <summary>
        /// 查询案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetCaseNameListDto>>> GetCaseNameList(QueryCaseNameRequest input)
        {
            var list = await DbContext.FreeSql.GetRepository<YssxCaseNew>(c => c.IsDelete == CommonConstants.IsNotDelete)
                  .WhereIf(input != null && !string.IsNullOrWhiteSpace(input.Name), c => c.Name.Contains(input.Name))
             .ToListAsync<GetCaseNameListDto>();

            return new ResponseContext<List<GetCaseNameListDto>> { Code = CommonConstants.SuccessCode, Data = list };
        }

        public async Task<ResponseContext<bool>> SaveCase(CaseDtoNew input, bkUserTicket currentUser)
        {

            YssxCaseNew sxCase = input.MapTo<YssxCaseNew>();
            if (input.Id == 0)
            {
                sxCase.Id = IdWorker.NextId();
            }
            //案例制度
            YssxCaseRegulationNew caseRegulation = new YssxCaseRegulationNew
            {
                Content = input.FinancialRegulation,
                CaseId = sxCase.Id,
                Id = IdWorker.NextId(),
            };

            //案例岗位
            var sxCasePositions = new List<YssxCasePositionNew>();
            if (input.PositionDtos != null && input.PositionDtos.Count > 0)
            {
                input.PositionDtos.ForEach(p =>
                {
                    var sxCasePosition = new YssxCasePositionNew
                    {
                        Id = IdWorker.NextId(),
                        CaseId = sxCase.Id,
                        PostionId = p.PositionId,
                        CreateBy = currentUser.Id
                    };
                    sxCasePositions.Add(sxCasePosition);
                });
            }

            var result = input.Id > 0 ? await UpdateCase(sxCase, caseRegulation, sxCasePositions) : await AddCase(sxCase, caseRegulation, sxCasePositions);
            return result;

        }

        private async Task<ResponseContext<bool>> AddCase(YssxCaseNew sxCase, YssxCaseRegulationNew caseRegulation, List<YssxCasePositionNew> casePosition)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            //获取科目信息集合并关联案例-caseid
            List<YssxSubjectNew> subjects = GetSubjects(sxCase.Id);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var repo_c = uow.GetRepository<YssxCaseNew>();
                    var repo_cr = uow.GetRepository<YssxCaseRegulationNew>();
                    var repo_cp = uow.GetRepository<YssxCasePositionNew>();
                    var repo_s = uow.GetRepository<YssxSubjectNew>();

                    if (sxCase != null)
                        await repo_c.InsertAsync(sxCase);
                    if (caseRegulation != null)
                        await repo_cr.InsertAsync(caseRegulation);
                    if (casePosition != null && casePosition.Count > 0)
                        await repo_cp.InsertAsync(casePosition);
                    if (subjects != null && subjects.Count > 0)
                    {
                        await repo_s.InsertAsync(subjects);
                    }
                    uow.Commit();
                    result.Msg = sxCase.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            return result;
        }

        private async Task<ResponseContext<bool>> UpdateCase(YssxCaseNew sxCase, YssxCaseRegulationNew caseRegulation, List<YssxCasePositionNew> casePositions)
        {
            if (!DbContext.FreeSql.Select<YssxCaseNew>().Any(c => c.Id == sxCase.Id))
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "该企业不存在！", false);

            var oldcr = await DbContext.FreeSql.GetRepository<YssxCaseRegulationNew>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.CaseId == sxCase.Id)
                .FirstAsync();

            if (oldcr != null)
            {
                caseRegulation.Id = oldcr.Id;
            }

            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var repo_c = uow.GetRepository<YssxCaseNew>();
                    var repo_cr = uow.GetRepository<YssxCaseRegulationNew>();
                    var repo_cp = uow.GetRepository<YssxCasePositionNew>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    await repo_c.UpdateDiy.SetSource(sxCase).IgnoreColumns(e => new { e.IsActive, e.CreateBy, e.CreateTime }).ExecuteAffrowsAsync();

                    if (caseRegulation.Id > 0)
                        await repo_cr.UpdateDiy.SetSource(caseRegulation).IgnoreColumns(e => new { e.CreateBy, e.CreateTime }).ExecuteAffrowsAsync();
                    else
                        await repo_cr.InsertAsync(caseRegulation);
                    if (casePositions != null && casePositions.Count > 0)
                    {
                        await repo_cp.UpdateDiy.Set(c => new YssxCasePositionNew
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now
                        }).Where(cp => cp.CaseId == sxCase.Id).ExecuteAffrowsAsync();

                        await repo_cp.InsertAsync(casePositions);
                    }

                    uow.Commit();
                    result.Msg = sxCase.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存是异常！", false);
                }
            }
            return result;
        }

        private static List<YssxSubjectNew> GetSubjects(long caseId)
        {
            List<YssxSubjectTemp> list = DbContext.FreeSql.GetRepository<YssxSubjectTemp, long>().Select.ToList();
            List<YssxSubjectNew> subjects = list.Select(x => new YssxSubjectNew
            {
                Id = IdWorker.NextId(),
                AssistCode = x.AssistCode,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType,
                Code1 = x.Code1,
                Direction = x.Direction,
                EnterpriseId = caseId,
                Code2 = "",
                Code3 = "",
                Code4 = ""
            }).ToList();
            return subjects;
        }
        /// <summary>
        /// 启用或禁用案例
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input, bkUserTicket currentUser)
        {
            var sxCase = await DbContext.FreeSql.GetRepository<YssxCaseNew>()
                .Where(c => c.Id == input.Id && c.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

            if (sxCase == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "该案例不存在！", false);

            sxCase.UpdateBy = currentUser.Id;
            sxCase.UpdateTime = DateTime.Now;
            sxCase.IsActive = input.IsActive;
            await DbContext.FreeSql.Update<YssxCaseNew>(sxCase).SetSource(sxCase).ExecuteAffrowsAsync();

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        #endregion


        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 同步实训
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model)
        {
            DateTime dtNow = DateTime.Now;
            var rEndTime = dtNow.AddYears(1);
            //验证
            if (model.Category != 1)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "分类入参错误");
            if (model.TargetId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "标的物Id参数错误");
            if (model.UserId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "目标用户ID参数错误");

            //查找企业
            var rCaseInfo = DbContext.FreeSql.GetRepository<YssxCaseNew>().Where(x => x.Id == model.TargetId && x.IsActive && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rCaseInfo == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "未找到该企业");

            var rSxExamPaper = DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(x => x.CaseId == model.TargetId && x.UserId == model.UserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSxExamPaper != null)
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = true };
            //生成试卷
            var rExamPaper = new ExamPaperNew
            {
                Id = IdWorker.NextId(),
                Name = rCaseInfo.Name,
                UserId = model.UserId,
                CaseId = rCaseInfo.Id,
                ExamType = ExamType.PracticeTest,
                CompetitionType = CompetitionType.IndividualCompetition,
                CanShowAnswerBeforeEnd = true,
                TotalScore = 0,
                PassScore = 0,
                TotalQuestionCount = 0,
                BeginTime = dtNow,
                EndTime = rEndTime,
                Sort = 1,
                IsRelease = true,
                Status = ExamStatus.Wait,
                ExamSourceType = ExamSourceType.Order,
                CreateBy = model.UserId,
                CreateTime = dtNow
            };
            //试卷题目
            var rTopicList = DbContext.FreeSql.Select<YssxTopicNew>().Where(x => x.CaseId == rCaseInfo.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (rTopicList.Count > 0)
            {
                var rTotalScore = rTopicList.Sum(x => x.Score);
                rExamPaper.TotalScore = rTotalScore;
                rExamPaper.PassScore = rTotalScore * (decimal)0.6;
                rExamPaper.TotalQuestionCount = rTopicList.Count;
            }

            return await Task.Run(() =>
            {
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Insert(rExamPaper).ExecuteAffrows() > 0;
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }
        #endregion

        #region 获取案例(同步实训)个人购买列表
        /// <summary>
        /// 获取案例(同步实训)个人购买列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<CaseIndividualPurchaseViewModel>> GetCaseIndividualPurchaseList(CaseIndividualPurchaseQueryModel query, long currentUserId)
        {
            PageResponse<CaseIndividualPurchaseViewModel> response = new PageResponse<CaseIndividualPurchaseViewModel>();

            if (query == null)
                return response;

            var select = DbContext.FreeSql.GetRepository<ExamPaperNew>().Select.From<YssxCaseNew>(
                (a, b) =>
                a.InnerJoin(x => x.CaseId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete);

            if (string.IsNullOrEmpty(query.Name))
                select = select.Where((a, b) => b.Name.Contains(query.Name));

            var totalCount = await select.CountAsync();
            var selectData = await select.Page(query.PageIndex, query.PageSize).ToListAsync((a, b) => new CaseIndividualPurchaseViewModel
            {
                ExamId = a.Id,
                CaseId = b.Id,
                GradeId = 0,
                BriefName = b.BriefName,
                Img = b.LogoUrl,
                CaseName = b.Name,
            });

            //明细数据
            foreach (var item in selectData)
            {
                //作答记录
                var rGradeEntity = await DbContext.FreeSql.GetRepository<ExamStudentGradeNew>()
                    .Where(x => x.ExamId == item.ExamId && x.Status != StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending(x => x.CreateTime).FirstAsync();
                item.GradeId = rGradeEntity == null ? 0 : rGradeEntity.Id;
            }

            response.Data = selectData;
            response.Code = CommonConstants.SuccessCode;
            response.PageSize = query.PageSize;
            response.PageIndex = query.PageIndex;
            response.RecordCount = totalCount;
            return response;
        }
        #endregion

    }
}
