﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.Repository.Extensions;
using Yssx.S.Poco;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程备课服务实现
    /// </summary>
    public class CoursePrepareService : ICoursePrepareService
    {
        #region 新增/编辑课程备课
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddOrEditCoursePrepare(CoursePrepareRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "数据不允许为空!" };

            YssxCoursePrepare entity = new YssxCoursePrepare
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CoursePrepareName = dto.CoursePrepareName,
                CourseId = dto.CourseId,
                Sort = dto.Sort,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                TenantId = user.TenantId,
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                YssxCoursePrepare lessonModel = await DbContext.FreeSql.GetRepository<YssxCoursePrepare>().InsertAsync(entity);
                state = lessonModel != null;
            }
            else
            {
                state = await DbContext.FreeSql.GetRepository<YssxCoursePrepare>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.CoursePrepareName,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程备课列表
        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CoursePrepareViewModel>>> GetCoursePrepareList(CoursePrepareQuery query, UserTicket user)
        {
            ResponseContext<List<CoursePrepareViewModel>> response = new ResponseContext<List<CoursePrepareViewModel>>();
            if (query == null)
                return response;

            var select = DbContext.FreeSql.GetRepository<YssxCoursePrepare>()
                .Where(x => x.CourseId == query.CourseId && x.CreateBy == user.Id && x.IsDelete == CommonConstants.IsNotDelete);

            if (!string.IsNullOrEmpty(query.CoursePrepareName))
                select.Where(x => x.CoursePrepareName.Contains(query.CoursePrepareName));

            List<CoursePrepareViewModel> list = await select.ToListAsync(x => new CoursePrepareViewModel
            {
                Id = x.Id,
                CoursePrepareName = x.CoursePrepareName,
                CourseId = x.CourseId,
                Sort = x.Sort,
                CreateTime = x.CreateTime,
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region 删除课程备课
        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCoursePrepareById(long id, UserTicket user)
        {
            YssxCoursePrepare data = await DbContext.FreeSql.GetRepository<YssxCoursePrepare>().Where(x => x.Id == id).FirstAsync();

            if (data == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            data.IsDelete = CommonConstants.IsDelete;
            data.UpdateTime = DateTime.Now;
            data.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxCoursePrepare>().UpdateDiy.SetSource(data)
                .UpdateColumns(x => new
                {
                    x.IsDelete,
                    x.UpdateTime,
                    x.UpdateBy
                }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 新增课程备课文件(引用课程资源)
        /// <summary>
        /// 新增课程备课文件(引用课程资源)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddCoursePrepareFiles(List<CoursePrepareFilesRequestModel> requestList, UserTicket user)
        {
            if (requestList == null || requestList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择资源!" };

            bool state = false;

            List<YssxCoursePrepareFiles> list = new List<YssxCoursePrepareFiles>();

            requestList.ForEach(x =>
            {
                YssxCoursePrepareFiles entity = new YssxCoursePrepareFiles();
                entity.Id = IdWorker.NextId();
                entity.CourseId = x.CourseId;
                entity.CoursePrepareId = x.CoursePrepareId;
                entity.CourseFileId = x.CourseFileId;
                entity.FileName = x.FileName;
                entity.File = x.File;
                entity.FilesType = x.FilesType;
                entity.ResourceType = x.ResourceType;
                entity.ResourceBelong = x.ResourceBelong;
                entity.IsFile = x.IsFile;
                entity.KnowledgePointIds = x.KnowledgePointIds;
                entity.KnowledgePointNames = x.KnowledgePointNames;
                entity.Remark = x.Remark;
                entity.Sort = x.Sort;

                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                list.Add(entity);
            });

            List<YssxCoursePrepareFiles> modelList = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>().InsertAsync(list);
            state = modelList != null;

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取课程预习/备课文件列表
        /// <summary>
        /// 获取课程预习/备课文件列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CoursePrepareFilesViewModel>>> GetCoursePrepareFilesList(CoursePrepareFilesQuery query)
        {
            ResponseContext<List<CoursePrepareFilesViewModel>> response = new ResponseContext<List<CoursePrepareFilesViewModel>>();
            if (query == null)
                return response;

            var select = DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>()
                .Where(x => x.CoursePrepareId == query.CoursePrepareId && x.IsDelete == CommonConstants.IsNotDelete);

            if (query.ResourceType.HasValue)
                select.Where(x => x.ResourceType == query.ResourceType);
            if (query.ResourceBelong.HasValue)
                select.Where(x => x.ResourceBelong == query.ResourceBelong);
            if (!string.IsNullOrEmpty(query.FileName))
                select.Where(x => x.FileName.Contains(query.FileName));

            List<CoursePrepareFilesViewModel> list = await DbContext.FreeSql.Ado.QueryAsync<CoursePrepareFilesViewModel>(select.ToSql());

            response.Data = list;

            return response;
        }
        #endregion

        #region 获取课程备课选中课程题目列表
        /// <summary>
        /// 获取课程备课选中课程题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CoursePrepareSelectCourseTopicViewModel>>> GetPrepareSelectCourseTopicList(long couPrepareId)
        {
            ResponseContext<List<CoursePrepareSelectCourseTopicViewModel>> response = new ResponseContext<List<CoursePrepareSelectCourseTopicViewModel>>();

            var list = await DbContext.FreeSql.Select<YssxTopicPublic>().From<YssxCoursePrepareTopic>(
               (a, b) =>
               a.InnerJoin(x => x.Id == b.QuestionId))
               .Where((a, b) => b.CoursePrepareId == couPrepareId && b.TopicSource == CoursePrepareTopicSource.Course
               && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
               .ToListAsync((a, b) => new CoursePrepareSelectCourseTopicViewModel
               {
                   Id = b.Id,
                   QuestionId = a.Id,
                   QuestionName = a.Title,
                   QuestionContent = a.Content,
                   QuestionType = a.QuestionType,
                   CenterKnowledgePointIds = a.CenterKnowledgePointIds,
                   CenterKnowledgePointNames = a.CenterKnowledgePointNames,
                   Score = a.Score,
                   DifficultyLevel = a.DifficultyLevel,
                   Sort = b.Sort
               });

            response.Data = list;

            return response;
        }
        #endregion

        #region 获取课程备课选中案例题目列表
        /// <summary>
        /// 获取课程备课选中案例题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CoursePrepareSelectCaseTopicIdsViewModel>>> GetPrepareSelectCaseTopicIdList(long couPrepareId)
        {
            ResponseContext<List<CoursePrepareSelectCaseTopicIdsViewModel>> response = new ResponseContext<List<CoursePrepareSelectCaseTopicIdsViewModel>>();

            var list = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>()
                .Where(x => x.CoursePrepareId == couPrepareId && x.TopicSource == CoursePrepareTopicSource.Case && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new CoursePrepareSelectCaseTopicIdsViewModel
                {
                    Id = x.Id,
                    QuestionId = x.QuestionId
                });

            response.Data = list;

            return response;
        }
        #endregion

        #region 删除课程备课文件(根据主键)
        /// <summary>
        /// 删除课程备课文件(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCoursePrepareFilesById(long id, UserTicket user)
        {
            YssxCoursePrepareFiles entity = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除课程备课文件(根据文件主键)
        /// <summary>
        /// 删除课程备课文件(根据文件主键)
        /// </summary>
        /// <param name="couPreId">课程备课Id</param>
        /// <param name="fileId">课件库Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCoursePrepareFilesByFileId(long couPreId, long fileId, UserTicket user)
        {
            YssxCoursePrepareFiles entity = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>().Where(x => x.CoursePrepareId == couPreId
                && x.CourseFileId == fileId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 新增课程备课题目(引用课程题目)
        /// <summary>
        /// 新增课程备课题目(引用课程题目)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddCoursePrepareTopic(List<CoursePrepareTopicRequestModel> requestList, UserTicket user)
        {
            if (requestList == null || requestList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择题目!" };

            foreach (var item in requestList)
            {
                YssxCoursePrepareTopic model = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>().Where(x => x.CoursePrepareId ==
                    item.CoursePrepareId && x.QuestionId == item.QuestionId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (model != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "已引用该道题目,不能重复引用!" };
            }

            bool state = false;

            List<YssxCoursePrepareTopic> list = new List<YssxCoursePrepareTopic>();

            requestList.ForEach(x =>
            {
                YssxCoursePrepareTopic entity = new YssxCoursePrepareTopic();
                entity.Id = IdWorker.NextId();
                entity.CourseId = x.CourseId;
                entity.CoursePrepareId = x.CoursePrepareId;
                entity.QuestionId = x.QuestionId;
                entity.QuestionName = x.QuestionName;
                entity.QuestionType = x.QuestionType;
                entity.Score = x.Score;
                entity.KnowledgePointIds = x.KnowledgePointIds;
                entity.KnowledgePointNames = x.KnowledgePointNames;
                entity.TopicSource = x.TopicSource;
                entity.CourseOrCaseId = x.CourseOrCaseId;
                entity.CourseOrCaseName = x.CourseOrCaseName;
                entity.ModuleType = x.ModuleType;
                entity.Sort = x.Sort;

                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                list.Add(entity);
            });

            List<YssxCoursePrepareTopic> modelList = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>().InsertAsync(list);
            state = modelList != null;

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 发布预习
        /// <summary>
        /// 发布预习
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> PublishCoursePreview(CoursePreviewRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.ClassList == null || dto.ClassList.Count == 0)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "数据不允许为空!" };

            if (dto.ClassList.Distinct().Count() != dto.ClassList.Count)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "班级不能重复!" };

            YssxCoursePreview entity = new YssxCoursePreview
            {
                Id = IdWorker.NextId(),
                CourseId = dto.CourseId,
                CoursePrepareId = dto.CoursePrepareId,
                CoursePreviewName = dto.CoursePreviewName,
                Remark = dto.Remark,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                TenantId = user.TenantId,
            };

            //预习班级列表
            List<YssxCoursePreviewClass> classList = new List<YssxCoursePreviewClass>();
            dto.ClassList.ForEach(x =>
            {
                YssxCoursePreviewClass classData = new YssxCoursePreviewClass
                {
                    Id = IdWorker.NextId(),
                    CourseId = dto.CourseId,
                    CoursePrepareId = dto.CoursePrepareId,
                    CoursePreviewId = entity.Id,
                    ClassId = x.ClassId,
                    ClassName = x.ClassName,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    TenantId = user.TenantId,
                };

                classList.Add(classData);
            });

            #region 新增数据
            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //课程预习
                    var previewResult = await uow.GetRepository<YssxCoursePreview>().InsertAsync(entity);
                    //课程预习参与班级表
                    var classResult = await uow.GetRepository<YssxCoursePreviewClass>().InsertAsync(classList);

                    state = previewResult != null && classResult != null;

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程教师预习列表
        /// <summary>
        /// 获取课程教师预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<CoursePreviewToTeacherViewModel>> GetTeacherCoursePreviewList(CoursePreviewToTeacherQuery query, UserTicket user)
        {
            var result = new PageResponse<CoursePreviewToTeacherViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.GetRepository<YssxCoursePreview>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.CourseId == query.CourseId && a.CreateBy == user.Id);

            if (!string.IsNullOrEmpty(query.CoursePreviewName))
                select.Where(a => a.CoursePreviewName.Contains(query.CoursePreviewName));

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending(a => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToList(x => new CoursePreviewToTeacherViewModel
                {
                    Id = x.Id,
                    CourseId = x.CourseId,
                    CoursePrepareId = x.CoursePrepareId,
                    CoursePreviewName = x.CoursePreviewName,
                    Remark = x.Remark,
                    PublishDate = x.CreateTime
                });

            //预习Ids
            List<long> previewIds = items.Select(x => x.Id).ToList();

            //获取预习班级列表
            var classList = await DbContext.FreeSql.Select<YssxCoursePreviewClass>().From<YssxClass>(
                (a, b) => a.InnerJoin(x => x.ClassId == b.Id))
                .Where((a, b) => previewIds.Contains(a.CoursePreviewId) && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new CoursePreviewClassViewModel
                {
                    CoursePreviewId = a.CoursePreviewId,
                    ClassId = b.Id,
                    ClassName = b.Name,
                });

            items.ForEach(x =>
            {
                List<CoursePreviewClassViewModel> classes = classList.Where(a => a.CoursePreviewId == x.Id).ToList();

                //赋值班级列表
                x.ClassList = classes;
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 删除课程预习(根据主键)
        /// <summary>
        /// 删除课程预习(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCoursePreviewById(long id, UserTicket user)
        {
            YssxCoursePreview entity = await DbContext.FreeSql.GetRepository<YssxCoursePreview>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxCoursePreview>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程学生预习列表
        /// <summary>
        /// 获取课程学生预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<CoursePreviewToStudentViewModel>> GetStudentCoursePreviewList(CoursePreviewToStudentQuery query, UserTicket user)
        {
            var result = new PageResponse<CoursePreviewToStudentViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxCoursePreview>().From<YssxCoursePreviewClass, YssxStudent, YssxStudentViewPreviewRecord>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.Id == b.CoursePreviewId)
                .InnerJoin(x => b.ClassId == c.ClassId)
                .LeftJoin(x => x.Id == d.CoursePreviewId && c.UserId == d.UserId))
                .Where((a, b, c, d) => a.CourseId == query.CourseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                c.IsDelete == CommonConstants.IsNotDelete && c.UserId == user.Id).Distinct();

            if (!string.IsNullOrEmpty(query.CoursePreviewName))
                select.Where((a, b, c, d) => a.CoursePreviewName.Contains(query.CoursePreviewName));

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending((a, b, c, d) => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b, c, d) => new CoursePreviewToStudentViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    CoursePrepareId = a.CoursePrepareId,
                    CoursePreviewName = a.CoursePreviewName,
                    Remark = a.Remark,
                    ViewRecordId = d.Id,
                    PublishDate = a.CreateTime
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 学生首次进入课程预习记录
        /// <summary>
        /// 学生首次进入课程预习记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> StudentFirstCheckCoursePreview(StudentViewPreviewRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.CourseId == 0 || dto.CoursePrepareId == 0 || dto.CoursePreviewId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不允许为空!" };

            YssxStudentViewPreviewRecord data = await DbContext.FreeSql.GetRepository<YssxStudentViewPreviewRecord>().Where(x => x.CoursePreviewId == dto.CoursePreviewId
                && x.UserId == user.Id).FirstAsync();

            if (data != null)
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "已存在预览记录!" };

            data = new YssxStudentViewPreviewRecord
            {
                Id = IdWorker.NextId(),
                CourseId = dto.CourseId,
                CoursePrepareId = dto.CoursePrepareId,
                CoursePreviewId = dto.CoursePreviewId,
                UserId = user.Id,
                TenantId = user.TenantId,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
            };

            YssxStudentViewPreviewRecord model = await DbContext.FreeSql.GetRepository<YssxStudentViewPreviewRecord>().InsertAsync(data);
            bool state = model != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取学生查看预习详情列表
        /// <summary>
        /// 获取学生查看预习详情列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentPreviewCourseDetailViewModel>> GetStudentPreviewCourseDetailList(StudentPreviewCourseDetailQuery query)
        {
            var result = new PageResponse<StudentPreviewCourseDetailViewModel>();

            if (query == null)
                return result;

            List<StudentPreviewCourseDetailViewModel> list = new List<StudentPreviewCourseDetailViewModel>();
            int totalCount = 0;

            //无状态查询
            if (!query.CheckStatus.HasValue)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCoursePreviewClass, YssxCoursePreview, YssxStudentViewPreviewRecord>(
                    (a, b, c, d) =>
                    a.InnerJoin(x => x.ClassId == b.ClassId)
                    .InnerJoin(x => b.CoursePreviewId == c.Id)
                    .LeftJoin(x => c.Id == d.CoursePreviewId && x.UserId == d.UserId && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => c.Id == query.CoursePreviewId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                    c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d) => b.ClassId == query.ClassId);
                if (!string.IsNullOrEmpty(query.KeyWord))
                    select.Where((a, b, c, d) => a.Name.Contains(query.KeyWord) || a.StudentNo.Contains(query.KeyWord));

                totalCount = select.ToList().Count();

                list = await select.OrderBy((a, b, c, d) => a.StudentNo).Page(query.PageIndex, query.PageSize)
                    .ToListAsync((a, b, c, d) => new StudentPreviewCourseDetailViewModel
                    {
                        StudentName = a.Name,
                        StudentNo = a.StudentNo,
                        ClassName = b.ClassName,
                        ViewRecordId = d.Id,
                    });
            }

            //查询已查看记录
            if (query.CheckStatus.HasValue && query.CheckStatus == CheckStatus.HadCheck)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCoursePreviewClass, YssxCoursePreview, YssxStudentViewPreviewRecord>(
                    (a, b, c, d) =>
                    a.InnerJoin(x => x.ClassId == b.ClassId)
                    .InnerJoin(x => b.CoursePreviewId == c.Id)
                    .InnerJoin(x => c.Id == d.CoursePreviewId && x.UserId == d.UserId && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => c.Id == query.CoursePreviewId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                    c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d) => b.ClassId == query.ClassId);
                if (!string.IsNullOrEmpty(query.KeyWord))
                    select.Where((a, b, c, d) => a.Name.Contains(query.KeyWord) || a.StudentNo.Contains(query.KeyWord));

                totalCount = select.ToList().Count();

                list = await select.OrderBy((a, b, c, d) => a.StudentNo).Page(query.PageIndex, query.PageSize)
                    .ToListAsync((a, b, c, d) => new StudentPreviewCourseDetailViewModel
                    {
                        StudentName = a.Name,
                        StudentNo = a.StudentNo,
                        ClassName = b.ClassName,
                        ViewRecordId = d.Id,
                    });
            }

            //查询未查看记录
            if (query.CheckStatus.HasValue && query.CheckStatus == CheckStatus.DidNotCheck)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCoursePreviewClass, YssxCoursePreview, YssxStudentViewPreviewRecord>(
                    (a, b, c, d) =>
                    a.InnerJoin(x => x.ClassId == b.ClassId)
                    .InnerJoin(x => b.CoursePreviewId == c.Id)
                    .LeftJoin(x => c.Id == d.CoursePreviewId && x.UserId != d.UserId && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => c.Id == query.CoursePreviewId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                    c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d) => b.ClassId == query.ClassId);
                if (!string.IsNullOrEmpty(query.KeyWord))
                    select.Where((a, b, c, d) => a.Name.Contains(query.KeyWord) || a.StudentNo.Contains(query.KeyWord));

                totalCount = select.ToList().Count();

                list = await select.OrderBy((a, b, c, d) => a.StudentNo).Page(query.PageIndex, query.PageSize)
                    .ToListAsync((a, b, c, d) => new StudentPreviewCourseDetailViewModel
                    {
                        StudentName = a.Name,
                        StudentNo = a.StudentNo,
                        ClassName = b.ClassName,
                        ViewRecordId = 0,
                    });
            }

            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取学生数量及查看预习数量记录
        /// <summary>
        /// 获取学生数量及查看预习数量记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<StudentNumberAndHadCheckNumberViewModel>> GetStudentNumberAndHadCheckNumberRecord(StudentNumberAndHadCheckNumberQuery query)
        {
            ResponseContext<StudentNumberAndHadCheckNumberViewModel> result = new ResponseContext<StudentNumberAndHadCheckNumberViewModel>();

            StudentNumberAndHadCheckNumberViewModel data = new StudentNumberAndHadCheckNumberViewModel();

            if (query == null)
                return result;

            //查看学生总数            
            var stuSelect = DbContext.FreeSql.Select<YssxStudent>().From<YssxCoursePreviewClass, YssxCoursePreview>(
               (a, b, c) =>
               a.InnerJoin(x => x.ClassId == b.ClassId)
               .InnerJoin(x => b.CoursePreviewId == c.Id))
               .Where((a, b, c) => c.Id == query.CoursePreviewId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
               c.IsDelete == CommonConstants.IsNotDelete).Distinct();

            //查看学生已查看预习数量
            var viewSelect = DbContext.FreeSql.Select<YssxStudent>().From<YssxCoursePreviewClass, YssxCoursePreview, YssxStudentViewPreviewRecord>(
                 (a, b, c, d) =>
                 a.InnerJoin(x => x.ClassId == b.ClassId)
                 .InnerJoin(x => b.CoursePreviewId == c.Id)
                 .InnerJoin(x => c.Id == d.CoursePreviewId && x.UserId == d.UserId))
                 .Where((a, b, c, d) => c.Id == query.CoursePreviewId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                 c.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete).Distinct();

            if (query.ClassId.HasValue)
            {
                stuSelect.Where((a, b, c) => b.ClassId == query.ClassId);
                viewSelect.Where((a, b, c, d) => b.ClassId == query.ClassId);
            }

            data.StudentNumber = (int)await stuSelect.CountAsync();
            data.HadCheckNumber = (int)await viewSelect.CountAsync();

            result.Data = data;
            return result;
        }
        #endregion

        #region 添加学生课程资源不懂记录
        /// <summary>
        /// 添加学生课程资源不懂记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddStudentDoubtCourseFileRecord(StudentDoubtCourseFileRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.CourseId == 0 || dto.CoursePrepareId == 0 || dto.CoursePreviewId == 0 || dto.FileId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不允许为空!" };

            YssxStudentDoubtCourseFileRecord data = new YssxStudentDoubtCourseFileRecord
            {
                Id = IdWorker.NextId(),
                CourseId = dto.CourseId,
                CoursePrepareId = dto.CoursePrepareId,
                CoursePreviewId = dto.CoursePreviewId,
                UserId = user.Id,
                FileId = dto.FileId,
                FileName = dto.FileName,
                File = dto.File,
                FilesType = dto.FilesType,
                ResourceType = dto.ResourceType,
                IsFile = dto.IsFile,
                QuestionDescribe = dto.QuestionDescribe,
                TenantId = user.TenantId,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
            };

            YssxStudentDoubtCourseFileRecord model = await DbContext.FreeSql.GetRepository<YssxStudentDoubtCourseFileRecord>().InsertAsync(data);
            bool state = model != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取学生不懂(留言)详情列表
        /// <summary>
        /// 获取学生不懂(留言)详情列表
        /// </summary>
        /// <param name="couPreviewId">课程预习Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<StudentDoubtDetailViewModel>>> GetStudentDoubtDetailList(long couPreviewId)
        {
            ResponseContext<List<StudentDoubtDetailViewModel>> response = new ResponseContext<List<StudentDoubtDetailViewModel>>();

            List<StudentDoubtDetailViewModel> listData = new List<StudentDoubtDetailViewModel>();

            //获取课程预习下学生不懂(留言)记录
            var originalList = await DbContext.FreeSql.Select<YssxStudentDoubtCourseFileRecord>().From<YssxStudent>(
                (a, b) => a.InnerJoin(x => x.UserId == b.UserId))
               .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CoursePreviewId == couPreviewId)
               .ToListAsync((a, b) => new
               {
                   UserId = a.UserId,
                   FileId = a.FileId,
                   FileName = a.FileName,
                   File = a.File,
                   FilesType = a.FilesType,
                   ResourceType = a.ResourceType,
                   QuestionDescribe = a.QuestionDescribe,
                   StudentName = b.Name,
                   PublishDate = a.CreateTime
               });

            var fileList = originalList.GroupBy(a => new { a.FileId, a.FileName, a.File, a.FilesType, a.ResourceType }).Select(
                 x => new
                 {
                     FileId = x.Key.FileId,
                     FileName = x.Key.FileName,
                     File = x.Key.File,
                     FilesType = x.Key.FilesType,
                     ResourceType = x.Key.ResourceType,
                 });

            fileList.ToList().ForEach(x =>
            {
                StudentDoubtDetailViewModel model = new StudentDoubtDetailViewModel();
                model.FileName = x.FileName;
                model.FilesType = x.FilesType;
                model.ResourceType = x.ResourceType;
                //学生数量
                model.StudentNumber = originalList.Where(a => a.FileId == x.FileId).GroupBy(a => a.UserId).ToList().Count;

                model.LeaveWordList = originalList.Where(a => a.FileId == x.FileId).Select(a => new StudentLeaveWordDetailViewModel
                {
                    StudentName = a.StudentName,
                    QuestionDescribe = a.QuestionDescribe,
                    PublishDate = a.PublishDate,
                }).ToList();

                listData.Add(model);
            });

            response.Data = listData;
            response.Code = CommonConstants.SuccessCode;
            return response;
        }
        #endregion

        #region 获取预习某一资源 学生不懂详情记录
        /// <summary>
        /// 获取预习某一资源 学生不懂详情记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<FileStudentDoubtInfoViewModel>>> GetFileStudentDoubtInfoRecord(FileStudentDoubtInfoQuery query, UserTicket user)
        {
            ResponseContext<List<FileStudentDoubtInfoViewModel>> response = new ResponseContext<List<FileStudentDoubtInfoViewModel>>();

            //获取课程预习下学生不懂(留言)记录
            response.Data = await DbContext.FreeSql.Select<YssxStudentDoubtCourseFileRecord>().From<YssxUser>(
                (a, b) => a.InnerJoin(x => x.UserId == b.Id))
               .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CoursePreviewId == query.CoursePreviewId && a.FileId == query.FileId && a.UserId == user.Id)
               .ToListAsync((a, b) => new FileStudentDoubtInfoViewModel
               {
                   UserName = b.RealName,
                   QuestionDescribe = a.QuestionDescribe,
                   PublishDate = a.CreateTime,
               });

            return response;
        }
        #endregion

        #region 获取课程备课题目列表(课程资源来源)
        /// <summary>
        /// 获取课程备课题目列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<CoursePrepareTopicViewModel>> GetCoursePrepareTopicList(CoursePrepareTopicQuery query)
        {
            var result = new PageResponse<CoursePrepareTopicViewModel>();

            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxTopicPublic>().From<YssxSectionTopic>(
               (a, b) =>
               a.InnerJoin(x => x.Id == b.QuestionId))
               .Where((a, b) => b.CourseId == query.CourseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

            if (query.SectionId.HasValue)
                select.Where((a, b) => b.SectionId == query.SectionId);
            if (query.CenterKnowledgePointId.HasValue)
                select.Where((a, b) => a.CenterKnowledgePointIds.Contains(query.CenterKnowledgePointId.Value.ToString()));
            if (!string.IsNullOrEmpty(query.QuestionTypes))
                select.Where((a, b) => query.QuestionTypes.Contains("," + ((int)a.QuestionType).ToString() + ","));
            if (!string.IsNullOrEmpty(query.DifficultyLevels))
                select.Where((a, b) => query.DifficultyLevels.Contains(a.DifficultyLevel.ToString()));

            var totalCount = select.ToList().Count();

            var items = await select.OrderBy((a, b) => a.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b) => new CoursePrepareTopicViewModel
                {
                    QuestionId = a.Id,
                    QuestionName = a.Title,
                    QuestionContent = a.Content,
                    QuestionType = a.QuestionType,
                    CenterKnowledgePointIds = a.CenterKnowledgePointIds,
                    CenterKnowledgePointNames = a.CenterKnowledgePointNames,
                    Score = a.Score,
                    DifficultyLevel = a.DifficultyLevel,
                    Sort = a.Sort,
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取课程备课文件资源列表(课程资源来源)
        /// <summary>
        /// 获取课程备课文件资源列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<CourseFilesResourceViewModel>> GetCoursePrepareFileList(CourseFilesResourceQuery query)
        {
            var result = new PageResponse<CourseFilesResourceViewModel>();

            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxSectionFiles>().Where(x => x.CourseId == query.CourseId
                        && (x.SectionType == 1 || x.SectionType == 2 || x.SectionType == 3 || x.SectionType == 6) && x.IsDelete == CommonConstants.IsNotDelete);

            if (query.SectionId.HasValue)
                select.Where(x => x.SectionId == query.SectionId.Value);
            if (query.SectionType.HasValue)
                select.Where(x => x.SectionType == query.SectionType.Value);
            if (query.CenterKnowledgePointId.HasValue)
                select.Where(x => x.KnowledgePointId.Contains(query.CenterKnowledgePointId.Value.ToString()));

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new CourseFilesResourceViewModel
                {
                    Id = x.Id,
                    CourseFileId = x.CourseFileId,
                    FileName = x.FileName,
                    File = x.File,
                    FilesType = x.FilesType,
                    SectionType = x.SectionType,
                    IsFile = x.IsFile,
                    KnowledgePointId = x.KnowledgePointId,
                    KnowledgePointNames = x.KnowledgePointNames,
                    Sort = x.Sort,
                    Remark = x.Remark
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

    }
}
