﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程备课上课服务
    /// </summary>
    public class PlanningAttendClassService : IPlanningAttendClassService
    {
        #region 课程备课上课
        /// <summary>
        /// 课程备课上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CreatePlanningAttendClassRecord(PlanningAttendClassRequestModel dto, UserTicket user)
        {
            bool state = false;

            if (dto == null || dto.ClassList == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "对象是空的!" };
            if (!dto.CourseId.HasValue || !dto.LessonPlanId.HasValue || !dto.SemesterId.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "参数不能为空!" };

            #region 初始化数据
            YssxPlanningAttendClassRecord recordEntity = new YssxPlanningAttendClassRecord
            {
                Id = IdWorker.NextId(),
                CourseId = dto.CourseId.Value,
                CourseName = dto.CourseName,
                LessonPlanId = dto.LessonPlanId.Value,
                LessonPlanName = dto.LessonPlanName,
                SemesterId = dto.SemesterId,
                TeacherUserId = user.Id,
                TenantId = user.TenantId,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            List<YssxPlanningAttendClassClass> classList = new List<YssxPlanningAttendClassClass>();

            dto.ClassList.ForEach(x =>
            {
                YssxPlanningAttendClassClass model = new YssxPlanningAttendClassClass
                {
                    Id = IdWorker.NextId(),
                    AttendClassRecordId = recordEntity.Id,
                    CollegeId = x.CollegeId,
                    CollegeName = x.CollegeName,
                    ProfessionId = x.ProfessionId,
                    ProfessionName = x.ProfessionName,
                    ClassId = x.ClassId,
                    ClassName = x.ClassName,
                    TenantId = user.TenantId,

                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                classList.Add(model);
            });
            #endregion

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //生成上课记录
                    var record = await uow.GetRepository<YssxPlanningAttendClassRecord>().InsertAsync(recordEntity);
                    //生成上课记录班级关系
                    if (classList.Count > 0)
                        await uow.GetRepository<YssxPlanningAttendClassClass>().InsertAsync(classList);

                    state = record != null;

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取备课课堂记录
        /// <summary>
        /// 获取备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<PlanningClassRecordViewModel>> GetPlanningClassRecordList(PlanningClassRecordQuery query, UserTicket user)
        {
            var result = new PageResponse<PlanningClassRecordViewModel>();
            if (query == null)
                return result;

            FreeSql.ISelect<YssxPlanningAttendClassRecord> select = DbContext.FreeSql.GetRepository<YssxPlanningAttendClassRecord>().Select
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CourseId == query.CourseId);

            if (query.StartTime.HasValue)
                select.Where(x => x.CreateTime >= query.StartTime.Value.Date);
            if (query.EndTime.HasValue)
                select.Where(x => x.CreateTime < query.EndTime.Value.AddDays(1).Date);
            if (query.SemesterId.HasValue)
                select.Where(x => x.SemesterId == query.SemesterId);

            var totalCount = await select.CountAsync();

            var sql = select.From<YssxSemester>((a, b) => a.LeftJoin(aa => aa.SemesterId == b.Id)).OrderByDescending((a, b) => a.CreateTime)
              .Page(query.PageIndex, query.PageSize).ToSql("a.Id AS AttendClassRecordId,a.CourseName,a.CreateTime AS AttendClassTime,b.Name AS SemesterName");

            var items = await DbContext.FreeSql.Ado.QueryAsync<PlanningClassRecordViewModel>(sql);

            //上课记录Ids
            List<long> attendClassRecordIds = items.Select(x => x.AttendClassRecordId).ToList();

            List<PlanningClassRecordClassViewModel> recordClassList = await DbContext.FreeSql.Select<YssxPlanningAttendClassClass>()
                     .Where(x => attendClassRecordIds.Contains(x.AttendClassRecordId) && x.IsDelete == CommonConstants.IsNotDelete)
                     .ToListAsync(x => new PlanningClassRecordClassViewModel
                     {
                         AttendClassRecordId = x.AttendClassRecordId,
                         ClassId = x.ClassId,
                         ClassName = x.ClassName,
                     });

            items.ForEach(x =>
            {
                x.TeacherName = user.RealName;
                //上课课堂班级列表
                x.RecordClassList = recordClassList.Where(a => a.AttendClassRecordId == x.AttendClassRecordId).ToList();
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 删除备课课堂记录列表
        /// <summary>
        /// 删除备课课堂记录列表
        /// </summary>
        /// <param name="attendClassRecordIds"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePlanningClassRecordList(List<long> attendClassRecordIds, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<YssxPlanningAttendClassRecord>().Where(x => attendClassRecordIds.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = 1;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<YssxPlanningAttendClassRecord>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 导出备课课堂记录
        /// <summary>
        /// 导出备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExportYssxClassRecordViewModel>>> ExportPlanningClassRecord(PlanningClassRecordQuery query, UserTicket user)
        {
            ResponseContext<List<ExportYssxClassRecordViewModel>> response = new ResponseContext<List<ExportYssxClassRecordViewModel>>();

            List<ExportYssxClassRecordViewModel> listData = new List<ExportYssxClassRecordViewModel>();
            if (query == null || user == null)
                return response;

            FreeSql.ISelect<YssxPlanningAttendClassRecord> select = DbContext.FreeSql.GetRepository<YssxPlanningAttendClassRecord>().Select
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CourseId == query.CourseId);

            if (query.StartTime.HasValue)
                select.Where(x => x.CreateTime >= query.StartTime.Value.Date);
            if (query.EndTime.HasValue)
                select.Where(x => x.CreateTime < query.EndTime.Value.AddDays(1).Date);
            if (query.SemesterId.HasValue)
                select.Where(x => x.SemesterId == query.SemesterId);

            var dataSource = await select.From<YssxSemester>((a, b) => a.LeftJoin(aa => aa.SemesterId == b.Id)).OrderByDescending((a, b) => a.CreateTime)
                .ToListAsync((a, b) => new PlanningClassRecordViewModel
                {
                    AttendClassRecordId = a.Id,
                    CourseName = a.CourseName,
                    AttendClassTime = a.CreateTime,
                    SemesterName = b.Name
                });

            if (dataSource == null || dataSource.Count == 0)
                return response;

            //上课记录Ids
            List<long> attendClassRecordIds = dataSource.Select(x => x.AttendClassRecordId).ToList();

            List<PlanningClassRecordClassViewModel> recordClassList = await DbContext.FreeSql.Select<YssxPlanningAttendClassClass>()
                     .Where(x => attendClassRecordIds.Contains(x.AttendClassRecordId) && x.IsDelete == CommonConstants.IsNotDelete)
                     .ToListAsync(x => new PlanningClassRecordClassViewModel
                     {
                         AttendClassRecordId = x.AttendClassRecordId,
                         ClassId = x.ClassId,
                         ClassName = x.ClassName,
                     });

            dataSource.ForEach(x =>
            {
                //上课课堂班级列表
                x.RecordClassList = recordClassList.Where(a => a.AttendClassRecordId == x.AttendClassRecordId).ToList();

                x.RecordClassList.ForEach(a =>
                {
                    ExportYssxClassRecordViewModel model = new ExportYssxClassRecordViewModel
                    {
                        CourseName = x.CourseName,
                        TeacherName = user.RealName,
                        AttendClassTime = x.AttendClassTime,
                        SemesterName = x.SemesterName,
                        ClassName = a.ClassName,
                    };

                    listData.Add(model);
                });
            });

            response.Data = listData;

            return response;
        }
        #endregion

    }
}
