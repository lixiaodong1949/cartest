﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 获取历届考试列表 - 入参
    /// </summary>
    public class GetPreviouExamListRequest : PageRequest
    {

        /// <summary>
        /// 证书类型
        /// </summary>
        public PreviouExamType PreviouExamType { get; set; } = PreviouExamType.DEFAULT;

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }


    }
}
