﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 
    /// </summary>
   public class QuestionInfoDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目名称
        /// </summary>

        public string Title { get; set; }
        /// <summary>
        /// 题干
        /// </summary>
        public string Content { get; set; }

    }
}
