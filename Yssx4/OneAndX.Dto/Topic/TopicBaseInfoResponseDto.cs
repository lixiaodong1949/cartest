﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    ///题目基本信息
    /// </summary>
    public class TopicBaseInfoResponseDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int? DifficultyLevel { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }


    }
}
