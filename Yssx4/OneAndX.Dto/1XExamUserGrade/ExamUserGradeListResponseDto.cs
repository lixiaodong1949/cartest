﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试成绩
    /// </summary>
    public class ExamUserGradeListResponseDto
    {
        public long Id { get; set; }

        ///// <summary>
        ///// 用户Id
        ///// </summary>
        //public long UserId { get; set; }

        /// <summary>
        /// 准考证Id
        /// </summary>
        public long ExamTicketId { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamTicketNo { get; set; }

        ///// <summary>
        ///// 试卷Id
        ///// </summary>
        //public long ExamId { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        ///// <summary>
        ///// 试卷总分
        ///// </summary>
        //public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 用户身份证号码
        /// </summary>
        public string UserIDNumber { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 是否考试通过
        /// </summary>
        public bool IsPass { get; set; }

    }
}
