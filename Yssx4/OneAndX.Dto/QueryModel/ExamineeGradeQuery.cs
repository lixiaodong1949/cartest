﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考生成绩查询实体
    /// </summary>
    public class ExamineeGradeQuery : PageRequest
    {
        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// 考点Id
        /// </summary>
        public Nullable<long> ExamSiteId { get; set; }

        /// <summary>
        /// 考场Id
        /// </summary>
        public Nullable<long> ExamRoomId { get; set; }

        /// <summary>
        /// 关键字搜索(姓名/准考证/身份证)
        /// </summary>
        public string Keyword { get; set; }
    }
}
