﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 删除已分配考试计划考场请求实体
    /// </summary>
    public class DeleteDistributeExamSiteOrRoomDto
    {
        /// <summary>
        /// 计划Id
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// 删除数据类型 0:考场 1:考点
        /// </summary>
        public int DeleteDataType { get; set; }

        /// <summary>
        /// 考点或考场Ids
        /// </summary>
        public List<long> SiteIdsOrRoomIds = new List<long>();
    }
}
