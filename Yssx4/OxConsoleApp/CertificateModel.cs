﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OxConsoleApp
{
    public class CertificateModel
    {
        public long ExamId { get; set; }

        public string Name { get; set; }

        public long CertificateNum { get; set; }

        public string CertificateDate { get; set; }

        public int Year { get; set; }
    }
}
