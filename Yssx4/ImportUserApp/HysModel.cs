﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImportUserApp
{
    public class HysModel
    {
        public long Id { get; set; }

        public string QuestionName { get; set; }

        public long ParentId { get; set; }

        public long RootId { get; set; }

        public int QuetionsType { get; set; }

        public int Status { get; set; }
    }
}
