﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImportUserApp
{
    [Table(Name = "sc_school")]
    public class ScSchoolEntity
    {
        public long Id { get; set; }

        public string SchoolFullName { get; set; }

        public string SchoolName { get; set; }
    }

    [Table(Name = "sc_user")]
    public class ScUser
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public long StudentId { get; set; }

        public long ClassNo { get; set; }

        /// <summary>
        /// 系Id
        /// </summary>
        public long TieNo { get; set; }

        public long SchoolId { get; set; }

        /// <summary>
        /// 1:学生 2:老师
        /// </summary>
        public int UserTypeId { get; set; }

        public string NickName { get; set; }

        /// <summary>
        /// 1：启用 0：停用
        /// </summary>
        public int UserState { get; set; }
    }

    [Table(Name = "sc_class")]
    public class ScClass
    {
        public long ClassNo { get; set; }

        public long TieNo { get; set; }

        public string Name { get; set; }
    }

    [Table(Name = "sc_tie")]
    public class ScTie
    {
        public long TieNo { get; set; }

        public long SchoolNo { get; set; }

        public string Name { get; set; }
    }
}
