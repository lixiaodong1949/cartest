﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;

namespace PushMessageService
{
    /// <summary>
    /// 消息推送服务
    /// </summary>
    public interface IPushMessageService
    {
        ///// <summary>
        ///// 根据 cid 推送穿透消息
        ///// </summary>
        ///// <param name="pushMessage"></param>
        ///// <returns></returns>
        //Task<ResponseContext<bool>> PushTransmissionByCId(UserTicket user, TransmissionBody transmissionBody);

        /// <summary>
        /// 根据别名推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> VerifyRoleActCodePushMessage(long userid);

        /// <summary>
        /// 根据课程发布课堂测验任务推送消息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PushCourseTestTaskMessage(long courseId);
    }
    /// <summary>
    /// 消息推送  用户绑定 服务
    /// </summary>
    public interface IPushUserService
    {
        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindUserAlias(string cid,string deviceid, UserTicket user);
        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UnBindUserAlias(string cid, string deviceid, UserTicket user);
    }


    /// <summary>
    /// 消息推送  用户绑定 服务
    /// </summary>
    public interface IThirdPushUserService
    {
        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindUserAlias(string cid, string alias);
        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UnBindUserAlias(string cid, string alias);
    }

    /// <summary>
    /// 消息推送服务
    /// </summary>
    public interface IThirdMessageService
    {
        /// <summary>
        /// 根据 cid 推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PushTransmissionByCId(PushTransmissionMessage pushMessage);

        /// <summary>
        /// 根据别名推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PushTransmissionByAlias(PushTransmissionMessage pushMessage);

    }
}
