﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushMessageService.Gt
{
    internal class GtBaseResponse
    {
        public int code { get; set; }
        public string msg { get; set; }

    }
    internal class GtBaseResponse<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }
    internal class GtTokenResponse
    {
        /// <summary>
        /// token过期时间，ms时间戳
        /// </summary>
        public long expire_time { get; set; }
        public string token { get; set; }

    }
}
