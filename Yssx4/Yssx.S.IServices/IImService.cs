﻿using System;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 实时消息相关服务
    /// </summary>
    public interface IImService
    {
        /// <summary>
        /// 获取服务器地址
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<string>> GetConnectServerUrl(Guid clientId, string clientIp);

        /// <summary>
        /// 群聊,加入群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> JoinGroup(string groupId, Guid clientId);

        /// <summary>
        /// 群聊,离开群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> LeaveGroup(string groupId, Guid clientId);

        /// <summary>
        /// 群聊，获取群聊所有客户端id
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        Task<ResponseContext<Guid[]>> GetGroupClientList(string groupId);

        /// <summary>
        /// 群聊，发送群聊消息(群聊id,消息内容,groupid,message)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SendGroupMsg(ImDto model, Guid clientId);

        /// <summary>
        /// 单聊(接收者,发送内容,是否需要回执-receiveUserId,message,isReceipt)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SendMsg(ImDto model, Guid clientId);

        /// <summary>
        /// 获取群聊聊天记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<string[]>> GetGroupMsgHistory(ImMsgRequest model);
    }
}
