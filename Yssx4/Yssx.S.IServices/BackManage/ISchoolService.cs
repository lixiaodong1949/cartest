﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;

namespace Yssx.M.IServices
{
    /// <summary>
    /// 学校管理相关接口
    /// </summary>
    public interface ISchoolService
    {
        #region 学校
        /// <summary>
        /// 添加 更新 学校
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateSchool(SchoolDto model, long Id);

        /// <summary>
        /// 删除学校
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteSchool(SchoolDto model, long Id);

        /// <summary>
        /// 获取学校列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<SchoolDto>>> GetSchoolList(SchoolPageRequest model);

        /// <summary>
        /// 导出学校
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        Task<IActionResult> ExportSchool(long tenantId, string name);
        #endregion

        #region 院系
        /// <summary>
        /// 添加 更新 院系
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateSchoolCollege(CollegeDto model, long Id);

        /// <summary>
        /// 删除院系
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteSchoolCollege(CollegeDto model, long Id);

        /// <summary>
        /// 获取 学校-院系 列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<CollegeDto>>> GetSchoolColleges(SchoolCollegeRequest model);

        /// <summary>
        /// 导出院系
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        Task<IActionResult> ExportCollege(long tenantId, string name);
        #endregion

        #region 专业
        /// <summary>
        /// 添加 更新 专业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateCollegeProfession(ProfessionDto model, long Id);

        /// <summary>
        /// 删除专业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCollegeProfession(ProfessionDto model, long Id);

        /// <summary>
        /// 获取 学校-院系-专业 列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<ProfessionDto>>> GetCollegeProfessions(CollegeProfessionsRequest model);

        /// <summary>
        /// 导出专业
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        Task<IActionResult> ExportProfession(long tenantId, long collegesId, string name);
        #endregion

        #region 班级
        /// <summary>
        /// 添加 更新 班级
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateClass(ClassDto model, long currentUserId);
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteClass(ClassDto model, long currentUserId);
        /// <summary>
        /// 获取 学校-院系-专业-班级 列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<ClassDto>>> GetClasses(ClassRequest model);

        /// <summary>
        /// 添加 更新 班级 - 教师专用
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateClassOnTeacher(ClassDto model, long currentUserId);
        #endregion

        /// <summary>
        /// 学校激活记录
        /// </summary>
        /// <param name="model">搜索条件</param>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<ActivationRecordDto>>> ActivationRecord(ActivationRecordPageRequest model);

        /// <summary>
        /// 禁用,启用,换码
        /// </summary>
        /// <param name="id">禁用启用传UserId，换码传0</param>
        /// <param name="type">0禁用1启用3换码</param>
        /// <param name="code">激活码</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateActivationRecord(long id,int type,string code);


        /// <summary>
        /// 获取学校年级
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SchoolYearDto>>> GetSchoolYearList(long id);
    }
}
