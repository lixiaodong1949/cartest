﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tas.Common.Cache;
using Tas.Common.Utils;
using Yssx.Redis;

namespace Yssx.M.IServices.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class FreeSqlCacheExtension
    {
        /// <summary>
        /// 
        /// </summary>
        private const string CachePrefix = "FreeSql:Cache:{0}";

        private const string MultiCachePrefix = "Multi:{0}";
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="func"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        public static T GetCache<T>(string cacheKey, Func<T> func, int expireSeconds, bool isDataCompress = false, bool isMultiCache = true) where T : class
        {
            if (isMultiCache)
            {
                return MultiCacheCache(cacheKey, (int)Math.Ceiling(expireSeconds / 2m) + 1, func, isDataCompress);
            }
            var val = FromCache<T>(cacheKey, isDataCompress);
            if (val == null)
            {
                return GetOrSetCache(cacheKey, func, expireSeconds, isDataCompress, true);
            }
            return val;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="func"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        private static T GetOrSetCache<T>(string cacheKey, Func<T> func, int expireSeconds, bool isDataCompress, bool isWaitlock)
        {
            if (isWaitlock)
            {
                return RedisLock.WaitLock(cacheKey, () =>
                {
                    var val = FromCache<T>(cacheKey, isDataCompress);
                    if (val != null)
                        return val;
                    val = func();
                    if (val == null)
                        return val;
                    SetCache(cacheKey, val, expireSeconds, isDataCompress);
                    return val;
                });
            }
            return RedisLock.SkipLock(cacheKey, () =>
            {
                var val = func();
                if (val == null)
                    return val;
                SetCache(cacheKey, val, expireSeconds, isDataCompress);
                return val;
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="select"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        private static string GetCacheKey<T>(string cacheKey, ISelect<T> select) where T : class
        {
            return string.IsNullOrEmpty(cacheKey) ? string.Format(CachePrefix, select.ToSql().GetHashCode()) : cacheKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="returnValue"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        private static void SetCache<T>(string cacheKey, T returnValue, int expireSeconds, bool isDataCompress)
        {
            if (returnValue == null)
                return;

            if (isDataCompress)
            {
                var bytes = CompressHelper.BrotliCompress(returnValue);
                if (bytes == null)
                    return;
                RedisHelper.Set(cacheKey, bytes, expireSeconds);
                return;
            }
            RedisHelper.Set(cacheKey, returnValue, expireSeconds);
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="cacheKey"></param> 
        /// <param name="isDataCompress"></param> 
        /// <returns></returns>
        private static T MultiCacheCache<T>(string cacheKey, int expireSeconds, Func<T> func, bool isDataCompress) where T : class
        {
            var data = MemoryCacheHelper.Get<T>(cacheKey);
            if (data == null)
            {
                data = FromCache<T>(cacheKey, isDataCompress);
                if (data != null)
                {
                    MemoryCacheHelper.Set(cacheKey, data, (int)Math.Ceiling(expireSeconds / 2m) + 1);
                    return data;
                }
                var mutilCacheKey = string.Format(MultiCachePrefix, cacheKey);
                data = FromCache<T>(mutilCacheKey, isDataCompress);

                T Exec(bool isWaitlock)
                {
                    var now = DateTime.Now;
                    var mutilTtl = (int)(now.AddDays(1).Date - now).TotalSeconds;
                    mutilTtl = mutilTtl < 3600 ? 24 * 3600 : mutilTtl;
                    var tmpData = GetOrSetCache(mutilCacheKey, func, mutilTtl, isDataCompress, isWaitlock);
                    if (tmpData == null)
                        return tmpData;
                    MemoryCacheHelper.Set(cacheKey, tmpData, (int)Math.Ceiling(expireSeconds / 2m) + 1);
                    SetCache(cacheKey, tmpData, expireSeconds, isDataCompress);
                    return tmpData;
                }
                if (data == null)
                {
                    return Exec(true);
                }
                else
                    Task.Run(() => Exec(false)).ConfigureAwait(false);
                return data;
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary> 
        /// <param name="cacheKey"></param> 
        /// <param name="isDataCompress"></param> 
        /// <returns></returns>
        private static T FromCache<T>(string cacheKey, bool isDataCompress)
        {
            using (var pfm = new PerformanceMoniter($"redis缓存key为:{cacheKey}"))
            {
                if (isDataCompress)
                {
                    var bytes = RedisHelper.Get<byte[]>(cacheKey);
                    if (bytes == null)
                        return default;
                    return CompressHelper.BrotliDecompress<T>(bytes);
                }
                return RedisHelper.Get<T>(cacheKey);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="select"></param>
        /// <param name="cacheKey"></param>
        /// <param name="isCache"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this ISelect<T> select, string cacheKey, bool isCache = false, int expireSeconds = 60, bool isDataCompress = false, bool isMultiCache = true) where T : class
        {
            if (isCache)
            {
                var key = GetCacheKey(cacheKey, select);
                if (isMultiCache)
                {
                    List<T> func()
                    {
                        return select.ToList();
                    }
                    List<T> retList = MultiCacheCache(key, (int)Math.Ceiling(expireSeconds / 2m) + 1, func, isDataCompress);
                    return retList;
                }
                var list = FromCache<List<T>>(key, isDataCompress);
                if (list != null)
                    return list;
                return RedisLock.WaitLock(key, () =>
                {
                    list = FromCache<List<T>>(key, isDataCompress);
                    if (list != null)
                        return list;
                    list = select.ToList();
                    if (list == null)
                        return list;
                    SetCache(key, list, expireSeconds, isDataCompress);
                    return list;
                });
            }
            return select.ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="select"></param>
        /// <param name="cacheKey"></param>
        /// <param name="isCache"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        public static async Task<List<T>> ToListAsync<T>(this ISelect<T> select, string cacheKey, bool isCache = false, int expireSeconds = 60, bool isDataCompress = false, bool isMultiCache = true) where T : class
        {
            return await Task.Run(() => select.ToList(cacheKey, isCache, expireSeconds, isDataCompress, isMultiCache));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="select"></param>
        /// <param name="cacheKey"></param>
        /// <param name="isCache"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        public static T First<T>(this ISelect<T> select, string cacheKey, bool isCache = false, int expireSeconds = 60, bool isDataCompress = false, bool isMultiCache = true) where T : class
        {
            if (isCache)
            {
                var key = GetCacheKey(cacheKey, select);
                if (isMultiCache)
                {
                    T func()
                    {
                        return select.First();
                    }
                    T data = MultiCacheCache(key, (int)Math.Ceiling(expireSeconds / 2m) + 1, func, isDataCompress);
                    return data;
                }
                var list = FromCache<T>(key, isDataCompress);
                if (list != null)
                    return list;
                return RedisLock.WaitLock(key, () =>
                {
                    list = FromCache<T>(key, isDataCompress);
                    if (list != null)
                        return list;
                    list = select.First();
                    if (list == null)
                        return list;
                    SetCache(key, list, expireSeconds, isDataCompress);
                    return list;
                });
            }
            return select.First();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="select"></param>
        /// <param name="cacheKey"></param>
        /// <param name="isCache"></param>
        /// <param name="expireSeconds"></param>
        /// <param name="isDataCompress"></param>
        /// <returns></returns>
        public static async Task<T> FirstAsync<T>(this ISelect<T> select, string cacheKey, bool isCache = false, int expireSeconds = 60, bool isDataCompress = false, bool isMultiCache = true) where T : class
        {
            return await Task.Run(() => select.First(cacheKey, isCache, expireSeconds, isDataCompress, isMultiCache));
        }
    }
}
