﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.M.IServices
{
    /// <summary>
    /// 教师
    /// </summary>
    public interface ITeacherService
    {
        #region 基础操作
        /// <summary>
        /// 添加 更新 教师
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateTeacher(TeacherDto model, long Id);

        /// <summary>
        /// 删除老师
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTeacher(TeacherDto model, long Id);

        /// <summary>
        /// 获取教师列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<TeacherDto>>> GetTeachers(TeacherRequest model);

        /// <summary>
        /// 获取指定教师信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TeacherDto>> GetTeacherById(long Id);

        /// <summary>
        /// 导入老师
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ImportTeacherResponse>>> ImportTeacher(List<IFormFile> form, long Id);

        /// <summary>
        /// 导出老师
        /// </summary>
        /// <returns></returns>
        //[Microsoft.AspNetCore.Authorization.AllowAnonymous]
        //Task<IActionResult> ExportTeacher(TeacherType TeacherType, long TenantId, string KeyWord);
        #endregion

        #region 其他操作
        /// <summary>
        /// 学生审核 (只需要传入Id、Type和Status 三个个参数)
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> TeacherAudit(TeacherDto model, long Id);

        /// <summary>
        /// 获取全部教师列表
        /// </summary>
        Task<ResponseContext<PageResponse<TeacherDto>>> GetWhole(TeacherRequest model);

        /// <summary>
        /// 修改指定教师 用户状态
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ChangeTeacherStatus(long Id, long Oid);

        /// <summary>
        /// 绑定班级
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddClassToTeacher(TeacherClassRequest model, long Id);

        #endregion


    }
}
