﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;


namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程备课上课服务接口
    /// </summary>
    public interface IPlanningAttendClassService
    {
        /// <summary>
        /// 课程备课上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CreatePlanningAttendClassRecord(PlanningAttendClassRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<PlanningClassRecordViewModel>> GetPlanningClassRecordList(PlanningClassRecordQuery query, UserTicket user);

        /// <summary>
        /// 删除备课课堂记录列表
        /// </summary>
        /// <param name="attendClassRecordIds"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePlanningClassRecordList(List<long> attendClassRecordIds, UserTicket user);

        /// <summary>
        /// 导出备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExportYssxClassRecordViewModel>>> ExportPlanningClassRecord(PlanningClassRecordQuery query, UserTicket user);

    }
}
