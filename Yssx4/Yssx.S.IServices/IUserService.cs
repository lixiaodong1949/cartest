﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Pocos;

namespace Yssx.S.IServices
{
    public interface IUserService
    {
        Task<ResponseContext<bool>> FindUserByName(string name);

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <param name="can">参数</param>
        /// <param name="type">类型（1，修改昵称，2，修改头像，3，修改真实姓名）</param>
        /// <param name="currentUserId">用户Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ModifyUserInfo(
            string can,
            int type,
            long currentUserId);


        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        Task<ResponseContext<YssxUser>> UserInformation(long userId);

        /// <summary>
        /// 用户基本信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserInfoDto>> GetUserInfo(UserTicket user);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateUserPassword(UpdateUserPasswordDto model, long userId);

        /// <summary>
        /// 修改班级
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="newId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateClass(long userId, UpdateClassDto dto);

        /// <summary>
        /// 是否付费用户
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> IsVip(long uid);


        /// <summary>
        /// 获取用户积分
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> GetUserIntegral(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="moblile"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUserInfo(string name,string moblile);

        /// <summary>
        /// 合伙人名单
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<PartnerDto>>> GetPartnerList();

        /// <summary>
        /// 设置是否已经联系合伙人
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetPartnerStatus(long id);
        /// <summary>
        /// 老师修改学生姓名学号信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ModifyStudentInfoByUserId(StudentInfoRequest model, long currentUserId);
    }
}
