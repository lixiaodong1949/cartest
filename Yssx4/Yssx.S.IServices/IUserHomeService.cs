﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 用户首页信息服务接口
    /// </summary>
    public interface IUserHomeService
    {
        /// <summary>
        /// 获取用户自主训练
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxUserAutoTrainingViewModel>> GetUserAutoTrainingList(UserTicket user);

        /// <summary>
        /// 获取学生任务列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxStudentTaskInfoViewModel>>> GetStudentTaskList(UserTicket user);

        /// <summary>
        /// 获取教师任务列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherTaskInfoViewModel>>> GetTeacherTaskList(UserTicket user);

        /// <summary>
        /// 获取教师资源详情
        /// </summary> 
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherResourceViewModel>>> GetTeacherResourceList(UserTicket user);

        /// <summary>
        /// 获取教师实训资源详情
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherTrainResourceViewModel>>> GetTeacherTrainResourceList(UserTicket user);

        /// <summary>
        /// 获取首页案例列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<UserCaseViewModel>>> GetUserCaseList();

    }
}
