﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices.Topics
{
    /// <summary>
    /// 岗位管理
    /// </summary> 
    public interface IPositionService
    {
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetPositionListResponse>>> GetPositionList();

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<GetPositionListResponse>>> GetPositionListByPage(GetPositionListRequest model);

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns> 
        Task<ResponseContext<bool>> SavePosition(PositionRequest model, long userId);

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePosition(long id, long userId);

    }
}
