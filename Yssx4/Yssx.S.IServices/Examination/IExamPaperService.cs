﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto.ExamPaper;

namespace Yssx.S.IServices.Examination
{
    /// <summary>
    /// 教师/教务 试卷相关服务
    /// </summary> 
    public interface IExamPaperService
    {
        /// <summary>
        /// 获取考试岗位信息
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamPaperPostionView>>> GetExamPaperPostions(long examId);
    }
}
