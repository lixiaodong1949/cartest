﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 学期服务接口
    /// </summary>
    public interface ISemesterService
    {
        /// <summary>
        /// 新增/编辑学期
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSemester(SemesterDto dto, long currentUserId);

        /// <summary>
        /// 获取学期列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSemesterViewModel>>> GetSemesterList(YssxSemesterQuery query);

        /// <summary>
        /// 删除学期
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSemester(long id, long currentUserId);

    }
}
