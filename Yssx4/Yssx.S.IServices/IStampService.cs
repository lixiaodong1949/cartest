﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 印章服务接口
    /// </summary>
    public interface IStampService
    {
        /// <summary>
        /// 保存印章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveStamp(StampRequest model,long userId);
        /// <summary>
        /// 删除印章
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteStamp(long id);
        /// <summary>
        /// 获取印章列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<PageResponse<StampDto>> GetStampList(StampListRequest model, long userId);


    }
}
