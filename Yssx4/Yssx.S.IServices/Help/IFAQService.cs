﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 常见问题
    /// </summary>
    public interface IFAQService
    {
        #region 问题分类
        /// <summary>
        /// 添加/编辑 问题分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditFAQClass(FAQClassDto model, long currentUserId);

        /// <summary>
        /// 删除 问题分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteFAQClass(long id, long currentUserId);

        /// <summary>
        /// 获取问题分类列表
        /// </summary>
        Task<PageResponse<FAQClassDto>> GetFAQClassByPage(GetFAQClassByPageRequest model);

        #endregion

        #region 常见问题

        /// <summary>
        /// 添加/编辑 常见问题
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditFAQInfo(FAQInfoDto model, long currentUserId);

        /// <summary>
        /// 删除 常见问题
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteFAQInfo(long id, long currentUserId);

        /// <summary>
        /// 获取常见问题列表
        /// </summary>
        Task<PageResponse<FAQInfoDto>> GetFAQInfoByPage(GetFAQInfoByPageRequest model);

        /// <summary>
        /// 获取指定常见问题
        /// </summary>
        Task<ResponseContext<FAQInfoOneDto>> GetFAQInfoById(long id);

        /// <summary>
        /// 获取角色分类问题列表
        /// </summary>
        Task<ResponseContext<FAQRoleInfoDto>> GetFAQRoleList();

        #endregion

    }
}
