﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 会计证订单处理
    /// </summary>
    public interface IZhenShuOrderService
    {
        /// <summary>
        /// 课程订单添加
        /// </summary>
        /// <returns></returns>
        Task<bool> AddCourseOrder();


        Task<bool> AddCourseOrder(List<long> userIds);
    }
}
