using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 签到接口服务类
    /// </summary>
    public interface ISigninService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> SaveSignin(long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<SigninDto>> GetSignin(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> DeleteSignin(long currentUserId, params long[] id);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<SigninPageResponseDto>> GetSigninListPage(SigninPageRequestDto query);   

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<SigninListResponseDto>> GetSigninList(SigninListRequestDto query);
    
        /// <summary>
        /// 是否签到
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> IsSignin(long userId);
    }
}