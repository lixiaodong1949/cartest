﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 流程管理接口
    /// </summary>
    public interface IFlowService
    {
        /// <summary>
        /// 删除流程信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteById(long id, bkUserTicket currentUser);
        /// <summary>
        /// 根据Id获取单条流程信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<FlowDto>> GetInfoById(long id);
        /// <summary>
        /// 保存流程信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(FlowDto model, bkUserTicket currentUser);

        /// <summary>
        /// 获取父级流程信息列表 分页
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<FlowListDto>>> GetList(FlowQueryDto query);
        /// <summary>
        /// 获取流程名称列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<FlowNameListDto>>> GetNameList(FlowNameQueryRequestDto query);
    }
}
