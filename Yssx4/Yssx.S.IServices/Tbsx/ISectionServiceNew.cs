﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程章节
    /// </summary>
    public interface ISectionServiceNew
    {
        /// <summary>
        /// 新增,编辑章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSection(YssxSectionDtoNew dto);

        /// <summary>
        ///  新增,编辑课程课件,教案
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionFile(YssxSectionFilesDtoNew dto,UserTicket user);

        /// <summary>
        /// 新增,编辑教材
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>

        Task<ResponseContext<bool>> AddOrEditSectionTextBook(YssxSectionTextBookDtoNew dto);

        /// <summary>
        /// 新增课程教案
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionCase(YssxSectionCaseDtoNew dto);

        /// <summary>
        ///  新增,编辑课程习题
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionTopic(YssxSectionTopicDtoNew dto);

        /// <summary>
        /// 删除章节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSection(long id);

        /// <summary>
        /// 删除教案
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionCase(long id);

        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionTopic(long id);

        /// <summary>
        /// 删除课件，教案，视频
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionFile(long id);


        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTopicDtoNew>>> GetYssxSectionTopicList(long sid);

        /// <summary>
        /// 根据节获取习题
        /// 一般情况下，获取到所有的习题后，PC端可以自己筛选数据，不需要再次请求接口进行查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTopicDtoNew>>> GetYssxSectionTopicistForPC(SectionTopicQueryNew query);

        /// <summary>
        /// 获取课程下面的所有章节
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionDtoNew>>> GetSectionList(long courseId);

        /// <summary>
        /// 获取章节下面的课件,教案,视频
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDtoNew>>> GetSectionFilesList(long courseId, long sid);

        /// <summary>
        ///  获取章节下面的课件,教案,视频按类型筛选
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sid"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        Task<ResponseContext<SectionFileListDtoNew>> GetSectionFilesListByType(long courseId, long sid, int fileType);

        /// <summary>
        /// 删除教材
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionTextBook(long id);

        /// <summary>
        /// 获取教材内容
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTextBookDtoNew>>> GetSectionTextBookList(long sid);

        /// <summary>
        /// 获取章节下面的概要
        /// </summary>
        /// <param name="sid">章节Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionSummaryDtoNew>>> GetCourseSummaryList(long sid);

        /// <summary>
        ///  新增,编辑课程场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionSceneTraining(YssxSectionSceneTrainingDtoNew dto);

        /// <summary>
        /// 删除课程场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionSceneTraininge(long id);

        /// <summary>
        /// 获取课程下所有的课件,教案,视频按类型筛选 （包含课程章节下的）
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDtoNew>>> GetSectionFilesListByCourseId(long courseId);

        /// <summary>
        /// 获取课程场景实训列表(根据章节Id)
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionSceneTrainingViewModelNew>>> GetYssxSectionSceneTrainingList(long sid);

    }
   
}

