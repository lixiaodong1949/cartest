﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 资源配置接口
    /// </summary>
    public interface IResourceSettingServiceNew
    {

        //#region 案例标签
        ///// <summary>
        ///// 获取案例标签列表
        ///// </summary>
        ///// <returns></returns>
        //Task<PageResponse<TagDto>> GetTagList(TagRequest model);

        ///// <summary>
        ///// 新增或修改案例标签
        ///// </summary>
        ///// <returns></returns>
        //Task<ResponseContext<long>> AddOrEditTag(TagDto model, long currentUserId);

        ///// <summary>
        ///// 删除案例标签
        ///// </summary>
        ///// <returns></returns>
        //Task<ResponseContext<bool>> RemoveTag(long id, long currentUserId);

        //#endregion

        #region 知识点
        /// <summary>
        /// 获取知识点下拉列表
        /// </summary>
        /// <param name="ignoreId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<KnowledgePointDtoNew>>> GetKnowledgePointList(long ignoreId);

        /// <summary>
        /// 新增，编辑知识点
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CurrentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditKnowledgePoint(KnowledgePointDtoNew model, long CurrentUserId);

        /// <summary>
        /// 删除知识点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveKnowledgePoint(long id, long currentUserId);

        #endregion

        #region 行业
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IndustryDtoNew>> GetIndustryList(IndustryRequestNew model);

        /// <summary>
        /// 新增或修改行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDtoNew model, long currentUserId);

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveIndustry(long id, long currentUserId);

        #endregion

        #region 业务场景
        /// <summary>
        /// 获取业务场景列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<BusinessSceneDtoNew>> GetBusinessSceneList(BusinessSceneRequestNew model);

        /// <summary>
        /// 新增或修改业务场景
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditBusinessScene(BusinessSceneDtoNew model, long currentUserId);

        /// <summary>
        /// 删除业务场景
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveBusinessScene(long id, long currentUserId);

        #endregion

        #region 操作日志
        /// <summary>
        /// 获取 操作日志
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<PageResponse<OperationLogDtoNew>> OperationLog(OperationLogRequestNew model, long oId);
        #endregion

    }
}
