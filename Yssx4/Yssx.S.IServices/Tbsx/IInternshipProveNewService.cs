﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 实习证书 - 同步实训
    /// </summary>
    public interface IInternshipProveNewService
    {
        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<JobTrainingEndRecordDto>> GetJobTrainingEndRecord(JobTrainingEndRecordRequest model, long currentUserId);

        /// <summary>
        /// 实习列表 - 同步实习
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetSyncInternshipListDto>> GetSyncInternshipList(GetInternshipListRequest model, long currentUserId);

        /// <summary>
        /// 获取游客模式试卷信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GetSyncInternshipExamInfoDto>> GetSyncInternshipExamInfo( long caseId, long currentUserId);

        /// <summary>
        /// 增加案例访问量
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddCasePageview(long caseId);

        /// <summary>
        /// 获取公司案例列表(开启状态并被绑定课程)
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetCompanyCaseListDto>>> GetCompanyCaseList();
    }
}
