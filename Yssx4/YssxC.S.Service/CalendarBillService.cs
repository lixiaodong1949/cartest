﻿using Microsoft.Extensions.DependencyModel.Resolution;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 日历票据服务
    /// </summary>
    public class CalendarBillService : ICalendarBillService
    {
        #region 2020-06-22 逻辑修改 修改理由:显示学生作答记录
        //票据题学生作答
        //1、审核通过 sx_exam_user_grade_detail 表中 Answer 存 1
        //2、审核退回(退回理由必填) sx_exam_user_grade_detail 表中 Answer 存 2,退回理由Id
        #endregion

        #region 生成用户主作答记录(包含创建账套)
        /// <summary>
        /// 生成用户主作答记录(包含创建账套)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> CreateUserMasterAnswerRecord(UserCreateMasterAnswerDto dto, UserTicket user)
        {
            bool state = false;

            //购买过任务
            if (dto.IsBought)
            {
                //查询试卷下用户是否存在未完成的主作答记录
                var gradeList = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.ExamId == dto.ExamId && x.UserId == user.Id && x.Status != StudentExamStatus.End
                    && x.IsDelete == CommonConstants.IsNotDelete && x.Platform == dto.Platform).ToListAsync();
                if (gradeList != null && gradeList.Count > 0)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "存在未完成的作答记录!" };
            }

            var dataSource = await DbContext.FreeSql.Select<SxExamPaper>().From<SxTask>(
                 (a, b) =>
                 a.InnerJoin(x => x.MonthTaskId == b.Id)
             ).Where((a, b) => a.Status != ExamStatus.End && a.Id == dto.ExamId && a.IsDelete == CommonConstants.IsNotDelete)
             .ToListAsync((a, b) => new
             {
                 CaseId = a.CaseId,
                 TotalScore = a.TotalScore,
                 IsCreateSetBook = b.RequireAccountSet
             });
            if (dataSource == null || dataSource.Count == 0)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "试卷不存在!" };

            #region 初始化数据

            //初始化主作答记录
            SxExamUserGrade gradeData = new SxExamUserGrade
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = dto.ExamId,
                ExamPaperScore = dataSource[0].TotalScore,
                Status = StudentExamStatus.Started,
                Platform = dto.Platform,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                TenantId = user.TenantId,
            };
            if (!dataSource[0].IsCreateSetBook)
                gradeData.CreateSetBookStatus = CreateSetBookStatus.NotCreate;
            else
                gradeData.CreateSetBookStatus = CreateSetBookStatus.HasCreated;

            //初始化最近作答记录表
            SxExamUserLastGrade lastData = await DbContext.FreeSql.GetRepository<SxExamUserLastGrade>()
                .Where(x => x.ExamId == dto.ExamId && x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete && x.Platform == dto.Platform)
                .FirstAsync();
            if (lastData != null)
            {
                lastData.GradeId = gradeData.Id;
                lastData.UpdateBy = user.Id;
                lastData.UpdateTime = DateTime.Now;
            }
            else
            {
                lastData = new SxExamUserLastGrade();

                lastData.ExamId = dto.ExamId;
                lastData.UserId = user.Id;
                lastData.GradeId = gradeData.Id;
                lastData.Platform = dto.Platform;

                lastData.CreateBy = user.Id;
                lastData.CreateTime = DateTime.Now;
                lastData.UpdateBy = user.Id;
                lastData.UpdateTime = DateTime.Now;
                lastData.TenantId = user.TenantId;
            }

            //初始化用户作答信息资产表
            List<SxExamUserGradeAsset> assetList = new List<SxExamUserGradeAsset>();
            //获取公司资产账户
            List<SxBankAccount> accountList = await DbContext.FreeSql.GetRepository<SxBankAccount>().Where(x => x.CaseId == dataSource[0].CaseId
                 && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            accountList.ForEach(x =>
            {
                assetList.Add(new SxExamUserGradeAsset
                {
                    Id = IdWorker.NextId(),
                    UserId = user.Id,
                    ExamId = dto.ExamId,
                    GradeId = gradeData.Id,
                    BankAccountId = x.Id,
                    OpeningBank = x.OpeningBank,
                    SubBranch = x.Branch,
                    BankAccountNo = x.AccountNo,
                    InitialAccountAmount = x.Balance,
                    TotalAccount = x.Balance,
                    AccountUsedAmount = 0,
                    AccountBalance = x.Balance,

                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    TenantId = user.TenantId,
                });
            });

            #endregion

            #region 新增数据            
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //生成主作答记录
                    var gradeModel = await uow.GetRepository<SxExamUserGrade>().InsertAsync(gradeData);

                    //更新最近作答记录
                    if (lastData.Id > 0)
                    {
                        await uow.GetRepository<SxExamUserLastGrade>().UpdateDiy.SetSource(lastData).UpdateColumns(x => new
                        {
                            x.GradeId,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();
                    }
                    //生成最近作答记录
                    else
                    {
                        lastData.Id = IdWorker.NextId();
                        await uow.GetRepository<SxExamUserLastGrade>().InsertAsync(lastData);
                    }

                    //生成用户作答信息资产记录
                    if (assetList.Count > 0)
                        await uow.GetRepository<SxExamUserGradeAsset>().InsertAsync(assetList);

                    if (gradeModel != null)
                    {
                        state = true;
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "生成失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<long>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state ? gradeData.Id : 0,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取日历任务列表
        /// <summary>
        /// 获取日历任务列表
        /// </summary>
        /// <param name="monthTaskId"></param>
        /// <param name="gradeId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CalendarTaskViewModel>> GetCalendarTaskList(long monthTaskId, long gradeId, UserTicket user)
        {
            ResponseContext<CalendarTaskViewModel> response = new ResponseContext<CalendarTaskViewModel>();
            if (monthTaskId == 0 || gradeId == 0) return new ResponseContext<CalendarTaskViewModel> { Code = CommonConstants.ErrorCode, Msg = "请传有效参数!" };

            //查询月份任务
            SxTask taskData = await DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.Id == monthTaskId).FirstAsync();
            if (taskData == null) return new ResponseContext<CalendarTaskViewModel> { Code = CommonConstants.ErrorCode, Msg = "月份任务不存在!" };
            //查询日任务
            List<SxTaskItem> itemList = await DbContext.FreeSql.GetRepository<SxTaskItem>().Where(x => x.TaskId == monthTaskId &&
                x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.IssueDate).ToListAsync();
            if (itemList == null && itemList.Count == 0) return response;

            CalendarTaskViewModel data = new CalendarTaskViewModel();
            data.TaskName = taskData.Name;
            data.TaskDate = taskData.IssueDate;
            data.ReviewPattern = (int)taskData.WorkPattern;

            List<CalendarDateModel> calendarList = new List<CalendarDateModel>();
            itemList.ForEach(x =>
            {
                CalendarDateModel calendar = new CalendarDateModel();
                calendar.Date = x.IssueDate;
                calendar.JobDescription = x.Description;

                calendarList.Add(calendar);
            });

            data.CalendarDateList = calendarList;

            //月份第一天
            DateTime firstDay = new DateTime(taskData.IssueDate.Year, taskData.IssueDate.Month, 1);

            //已完成逻辑：
            //1、非干扰票据+干扰即刻推送票据 已完成(主管审核)
            //2、干扰票选择时间推送 当前日期已作答
            //3、推送题已完成(主管审核)

            //查询月份下题目列表
            List<SxTopic> topicList = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.CaseId == taskData.CaseId && (x.QuestionType ==
                QuestionType.CertificateMainSubTopic || x.QuestionType == QuestionType.TeticketTopic) && x.IsDelete == CommonConstants.IsNotDelete
                && x.TaskId == monthTaskId).ToListAsync();

            //查询月份下推送题目列表
            List<SxBillPush> pushList = await DbContext.FreeSql.GetRepository<SxBillPush>().Where(x => x.GradeId == gradeId && x.UserId == user.Id).ToListAsync();

            //取出分录综合题已作答完成的题目  作答完成条件:会计主管盖章
            List<SxExamUserGradeDetail> mainGradeDetailList = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().From<SxTopic>(
                   (a, b) =>
                   a.InnerJoin(x => x.QuestionId == b.Id))
                    .Where((a, b) => a.GradeId == gradeId && a.UserId == user.Id && a.AccountEntryStatus == AccountEntryStatus.AccountingManager && b.CaseId == taskData.CaseId &&
                    b.TaskId == monthTaskId && b.QuestionType == QuestionType.CertificateMainSubTopic && a.IsDelete == CommonConstants.IsNotDelete &&
                    b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //取出已作答题目  票据题
            List<SxExamUserGradeDetail> entryGradeDetailList = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().From<SxTopic>(
                   (a, b) =>
                   a.InnerJoin(x => x.QuestionId == b.Id))
                    .Where((a, b) => a.GradeId == gradeId && a.UserId == user.Id && b.CaseId == taskData.CaseId && b.TaskId == monthTaskId && b.QuestionType ==
                    QuestionType.TeticketTopic && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            DateTime startDate = firstDay;

            data.CalendarDateList.ForEach(x =>
            {
                DateTime closeDate = x.Date.AddDays(1);

                //1、非干扰票据+干扰即刻推送票据
                List<SxTopic> dayTopics = topicList.Where(a => a.BusinessDate >= startDate && a.BusinessDate < closeDate && a.QuestionType == QuestionType.CertificateMainSubTopic
                    && ((!a.PushDate.HasValue && !a.IsTrap) || (a.PushDate == a.BusinessDate && a.IsTrap))).ToList();

                List<long> dayIds = dayTopics.Select(a => a.Id).ToList();
                //分录综合题已作答题数
                int hasAnsweredMainCount = mainGradeDetailList.Where(a => dayIds.Contains(a.QuestionId)).Count();

                //2、干扰票特殊逻辑 业务日期5-1,推送日期5-7 票据题5-1号做 分录题5-7号做
                //5-1 票据题已做,已完成
                List<SxTopic> trapTopics = topicList.Where(a => a.IsTrap && a.PushDate > a.BusinessDate && a.BusinessDate >= startDate && a.BusinessDate < closeDate
                     && a.QuestionType == QuestionType.TeticketTopic).ToList();
                List<long> entryIds = trapTopics.Select(a => a.Id).ToList();
                //票据题已作答题数
                int hasAnsweredEntryCount = entryGradeDetailList.Where(a => entryIds.Contains(a.QuestionId)).Count();

                //3、推送题
                List<SxBillPush> pushTopics = pushList.Where(a => a.PushDate >= startDate && a.PushDate < closeDate).ToList();
                List<long> pushIds = pushTopics.Select(a => a.ParentQuestionId).ToList();
                //推送父级分录综合题已作答题数
                int hasAnsweredPushCount = mainGradeDetailList.Where(a => pushIds.Contains(a.QuestionId)).Count();

                //题数==已作答数 状态已完成
                if (hasAnsweredMainCount == dayTopics.Count && hasAnsweredEntryCount == trapTopics.Count && hasAnsweredPushCount == pushTopics.Count)
                    x.JobStatus = (int)DateJobStatus.Done;

                startDate = x.Date.AddDays(1);
            });

            response.Data = data;

            return response;
        }
        #endregion

        #region 获取用户票据题列表
        /// <summary>
        /// 获取用户票据题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserBillTopicViewModel>>> GetUserBillTopicList(UserCalendarTopicQuery query, UserTicket user)
        {
            ResponseContext<List<UserBillTopicViewModel>> response = new ResponseContext<List<UserBillTopicViewModel>>();
            List<UserBillTopicViewModel> listData = new List<UserBillTopicViewModel>();

            if (query == null || !query.PositionId.HasValue || !query.StartDate.HasValue || !query.EndDate.HasValue)
                return new ResponseContext<List<UserBillTopicViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            if (query.StartDate > query.EndDate)
                return new ResponseContext<List<UserBillTopicViewModel>> { Code = CommonConstants.ErrorCode, Msg = "日历开始日期不能大于日历结束日期!" };

            //结束日期加1天
            query.EndDate = query.EndDate.Value.AddDays(1);

            //查询月份任务
            SxTask taskData = await DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.Id == query.MonthTaskId).FirstAsync();
            if (taskData == null) return new ResponseContext<List<UserBillTopicViewModel>> { Code = CommonConstants.ErrorCode, Msg = "月份任务不存在!" };
            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<List<UserBillTopicViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            #region 未审核票据
            if (query.BillStatus == (int)BillStatus.Unreviewed)
            {
                #region 1、查询月份日期范围内并未在作答记录中的题目列表

                //查询月份日期范围内题目列表
                List<SxTopic> dateTopics = new List<SxTopic>();

                //非干扰票据+干扰即刻推送票据
                List<SxTopic> noObstructs = await DbContext.FreeSql.GetRepository<SxTopic>().Where(a => a.BusinessDate >= query.StartDate && a.BusinessDate < query.EndDate &&
                    ((!a.PushDate.HasValue && !a.IsTrap) || (a.PushDate == a.BusinessDate && a.IsTrap)) && a.CaseId == taskData.CaseId && a.QuestionType == QuestionType.TeticketTopic
                    && a.TaskId == query.MonthTaskId && a.PositionId == query.PositionId && a.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                dateTopics.AddRange(noObstructs);

                //干扰推送票据 业务日期在查询日期范围内
                List<SxTopic> obstructs = await DbContext.FreeSql.GetRepository<SxTopic>().Where(a => a.BusinessDate >= query.StartDate && a.BusinessDate < query.EndDate
                     && a.IsTrap && a.PushDate > a.BusinessDate && a.CaseId == taskData.CaseId && a.QuestionType == QuestionType.TeticketTopic &&
                    a.TaskId == query.MonthTaskId && a.PositionId == query.PositionId && a.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                dateTopics.AddRange(obstructs);

                //查询作答记录中的题目列表
                List<SxTopic> gardeTopics = await DbContext.FreeSql.Select<SxTopic>().From<SxExamUserGradeDetail>(
                    (a, b) =>
                    a.InnerJoin(x => x.Id == b.QuestionId))
                    .Where((a, b) => b.GradeId == query.GradeId && b.UserId == user.Id && b.QuestionType == QuestionType.TeticketTopic
                    && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                List<long> dateTopicIds = dateTopics.Select(x => x.Id).ToList();
                List<long> gradeTopicIds = gardeTopics.Select(x => x.Id).ToList();

                //未作答的题目Ids
                List<long> noAnswerTopicIds = dateTopicIds.Except(gradeTopicIds).ToList();

                //需要审核的题目(未审核题目/未在作答记录中的题目列表)
                List<SxTopic> needAnswerTopics = dateTopics.Where(x => noAnswerTopicIds.Contains(x.Id)).ToList();

                //非干扰票
                List<UserBillTopicViewModel> noObListData = await QueryBillFiles(needAnswerTopics, null, 0);
                listData.AddRange(noObListData);

                //干扰票
                List<UserBillTopicViewModel> obListData = await QueryBillFiles(needAnswerTopics, null, 1);
                listData.AddRange(obListData);

                #endregion

                #region 2、查询推送日期在查询日期范围内的推送列表

                //查询推送表 推送日期在查询日期范围内
                List<SxTopic> pushTopics = await DbContext.FreeSql.Select<SxTopic>().From<SxBillPush>(
                    (a, b) =>
                    a.InnerJoin(x => x.Id == b.QuestionId))
                    .Where((a, b) => b.PushDate >= query.StartDate && b.PushDate < query.EndDate && b.UserId == user.Id && b.GradeId == query.GradeId &&
                    b.PositionId == query.PositionId && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                //推送票据
                List<UserBillTopicViewModel> pushData = await QueryBillFiles(null, pushTopics, 2);

                listData.AddRange(pushData);
                #endregion
            }
            #endregion

            #region 已审核票据
            if (query.BillStatus == (int)BillStatus.Checked)
            {
                var checkedTopics = await DbContext.FreeSql.Select<SxTopic>().From<SxAuditedBill>(
                   (a, b) =>
                   a.InnerJoin(x => x.Id == b.QuestionId))
                    .Where((a, b) => b.BusinessDate >= query.StartDate && b.BusinessDate < query.EndDate && b.UserId == user.Id && b.GradeId == query.GradeId &&
                    b.PositionId == query.PositionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new
                    {
                        Id = a.Id,
                        ParentId = a.ParentId,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        IsHasReceivePayment = b.IsHasReceivePayment, //是否有收付款 false:没有 true:有
                        IsDoneReceivePayment = b.IsDoneReceivePayment, //收付款是否已完成 false:未完成 true:已完成
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });

                //综合分录题 Ids
                List<long> parentIds = checkedTopics.Select(x => x.ParentId).ToList();
                //票据分录题作答列表

                #region 分录综合题下的分录题作答明细没有记录ParentId 不能这样查询
                /*
                List<SxExamUserGradeDetail> gardeAccountList = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().Where(x => parentIds.Contains(x.ParentQuestionId) &&
                 x.GradeId == query.GradeId && x.UserId == user.Id && x.QuestionType == QuestionType.CertifMSub_AccountEntry && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();
                */
                #endregion

                var gardeAccountList = await DbContext.FreeSql.Select<SxTopic>().From<SxAuditedBill, SxExamUserGradeDetail>(
                   (a, b, c) =>
                   a.InnerJoin(x => x.ParentId == b.ParentQuestionId)
                   .InnerJoin(x => x.Id == c.QuestionId))
                    .Where((a, b, c) => b.BusinessDate >= query.StartDate && b.BusinessDate < query.EndDate && b.UserId == user.Id && b.GradeId == query.GradeId &&
                    b.PositionId == query.PositionId && c.GradeId == query.GradeId && c.UserId == user.Id && c.QuestionType == QuestionType.CertifMSub_AccountEntry
                    && a.QuestionType == QuestionType.CertifMSub_AccountEntry && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();

                //票据题详情 包含回执单                
                List<SxTopicFile> topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => parentIds.Contains(x.ParentTopicId)
                    && (x.Type == TopicFileType.Normal || x.Type == TopicFileType.Push || x.Type == TopicFileType.Reply) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                checkedTopics.ForEach(x =>
                {
                    UserBillTopicViewModel model = new UserBillTopicViewModel();
                    model.ParentQuestionId = x.ParentId;
                    model.BillQuestionId = x.Id;
                    model.TopicName = x.Title;
                    model.BusinessDate = x.BusinessDate;
                    model.TeacherHint = x.TeacherHint;
                    model.Sort = x.Sort;
                    model.CreateTime = x.CreateTime;

                    //分录题有作答记录 则标记已做凭证
                    SxTopic gradeDetail = gardeAccountList.Where(a => x.ParentId == a.ParentId).FirstOrDefault();
                    if (gradeDetail != null)
                        model.IsDoCertificate = true;

                    //题目下票据详情列表
                    List<SxTopicFile> files = new List<SxTopicFile>();

                    //没有收付款或者未完成 没有回执单
                    if (!x.IsDoneReceivePayment)
                    {
                        files = topicFileList.Where(a => a.TopicId == model.BillQuestionId && a.Type != TopicFileType.Reply).ToList();
                    }
                    //有收付款并且已完成
                    if (x.IsDoneReceivePayment)
                    {
                        files = topicFileList.Where(a => a.TopicId == model.BillQuestionId).ToList();
                    }

                    //设置排序(封面/底面)
                    files = SetBillTopicFileSort(files);

                    files.ForEach(a =>
                    {
                        model.BillFileList.Add(new BillFileViewModel
                        {
                            Url = a.Url,
                            //IsWatermark = a.IsWatermark,
                            CoverBack = (Nullable<int>)a.CoverBack,
                        });
                    });

                    listData.Add(model);
                });
            }
            #endregion

            #region 已退回票据
            if (query.BillStatus == (int)BillStatus.Returned)
            {
                var sendbackTopics = await DbContext.FreeSql.Select<SxTopic>().From<SxSendBackBill, SxReasonDetails>(
                   (a, b, c) =>
                   a.InnerJoin(x => x.Id == b.QuestionId)
                   .InnerJoin(x => x.AnswerValue == c.Id.ToString()))
                    .Where((a, b, c) => b.BusinessDate >= query.StartDate && b.BusinessDate < query.EndDate && b.UserId == user.Id && b.GradeId == query.GradeId &&
                    b.PositionId == query.PositionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b, c) => new
                    {
                        Id = a.Id,
                        ParentId = a.ParentId,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        AnswerDetail = c.Details,
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });

                //票据题Ids
                List<long> billIds = sendbackTopics.Select(x => x.Id).ToList();
                //票据题详情
                List<SxTopicFile> topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => billIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                  || x.Type == TopicFileType.Fault) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                sendbackTopics.ForEach(x =>
                {
                    UserBillTopicViewModel model = new UserBillTopicViewModel();
                    model.ParentQuestionId = x.ParentId;
                    model.BillQuestionId = x.Id;
                    model.TopicName = x.Title;
                    model.BusinessDate = x.BusinessDate;
                    model.TeacherHint = x.TeacherHint;
                    model.AnswerValue = x.AnswerDetail;
                    model.Sort = x.Sort;
                    model.CreateTime = x.CreateTime;

                    //题目下票据详情列表
                    List<SxTopicFile> files = new List<SxTopicFile>();

                    files = topicFileList.Where(a => a.TopicId == model.BillQuestionId).ToList();

                    //设置排序(封面/底面)
                    files = SetBillTopicFileSort(files);

                    files.ForEach(a =>
                    {
                        model.BillFileList.Add(new BillFileViewModel
                        {
                            Url = a.Url,
                            //IsWatermark = a.IsWatermark,
                            CoverBack = (Nullable<int>)a.CoverBack,
                        });
                    });

                    listData.Add(model);
                });
            }
            #endregion

            #region 查询题目是否被标记
            //题目Ids
            List<long> queIds = listData.Select(x => x.BillQuestionId).ToList();
            List<SxTopicMark> markList = await DbContext.FreeSql.GetRepository<SxTopicMark>().Where(x => queIds.Contains(x.QuestionId) && x.GradeId == query.GradeId
                && x.UserId == user.Id).ToListAsync();
            listData.ForEach(x =>
            {
                if (markList.Where(a => a.QuestionId == x.BillQuestionId).Count() > 0)
                    x.IsMark = true;
            });
            #endregion

            //排序
            listData = listData.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();

            response.Data = listData;

            return response;
        }
        #endregion

        #region 获取票据题退回理由列表
        /// <summary>
        /// 获取票据题退回理由列表
        /// </summary>
        /// <param name="billTopicId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<BillTopicSendBackReasonViewModel>>> GetSendBackReasonListByBillId(long billTopicId)
        {
            //查询逻辑：
            //1、正确票据 随机取四条理由
            //2、干扰票   随机取三条理由+1个正确答案

            ResponseContext<List<BillTopicSendBackReasonViewModel>> response = new ResponseContext<List<BillTopicSendBackReasonViewModel>>();

            //是否是干扰票
            bool isObstructBill = false;

            //查询题目信息
            SxTopic topicData = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.Id == billTopicId).FirstAsync();
            if (topicData == null)
                return new ResponseContext<List<BillTopicSendBackReasonViewModel>> { Code = CommonConstants.ErrorCode, Msg = "题目不存在!" };
            if (topicData.IsTrap && !topicData.PushDate.HasValue)
                return new ResponseContext<List<BillTopicSendBackReasonViewModel>> { Code = CommonConstants.ErrorCode, Msg = "题目错误,请联系管理员!" };
            if (topicData.IsTrap && topicData.PushDate.HasValue && string.IsNullOrEmpty(topicData.AnswerValue))
                return new ResponseContext<List<BillTopicSendBackReasonViewModel>> { Code = CommonConstants.ErrorCode, Msg = "题目错误,请联系管理员!" };

            SxReasonDetails reasonData = new SxReasonDetails();
            if (topicData.IsTrap && topicData.PushDate.HasValue)
            {
                long answerValueId = 0;
                long.TryParse(topicData.AnswerValue, out answerValueId);
                reasonData = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == answerValueId).FirstAsync();
                if (reasonData == null)
                    return new ResponseContext<List<BillTopicSendBackReasonViewModel>> { Code = CommonConstants.ErrorCode, Msg = "题目错误,请联系管理员!" };
                isObstructBill = true;
            }

            string sql = "";

            //非干扰票
            if (!isObstructBill)
            {
                sql = string.Format("SELECT a.Id AS SendBackReasonId,a.Details AS SendBackDetail  FROM sx_reasondetails a WHERE a.IsDelete = 0 ORDER BY RAND() LIMIT {0}", 4);
            }
            //干扰票
            else
            {
                sql = string.Format("SELECT a.Id AS SendBackReasonId,a.Details AS SendBackDetail  FROM sx_reasondetails a WHERE a.IsDelete = 0 AND a.Id!={0} ORDER BY RAND() LIMIT {1}",
                    reasonData.Id, 3);
            }

            var items = await DbContext.FreeSql.Ado.QueryAsync<BillTopicSendBackReasonViewModel>(sql);

            if (reasonData != null && reasonData.Id > 0)
            {
                items.Add(new BillTopicSendBackReasonViewModel
                {
                    SendBackReasonId = reasonData.Id,
                    SendBackDetail = reasonData.Details,
                });
            }

            response.Data = items;

            return response;
        }
        #endregion

        #region 审核票据
        /// <summary>
        /// 审核票据
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserCheckBillTopicViewModel>> CheckBillTopicOperate(UserCheckBillTopicDto dto, UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 参数验证
            if (dto.CheckBillType == (int)CheckBillType.SendBack && !dto.SendBackReasonId.HasValue)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "退回票据必须选择退回理由!" };
            if (dto.IsPushBill && dto.CheckBillType == (int)CheckBillType.SendBack)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "推送票据不能退回!" };

            //查询试卷信息
            var examData = await DbContext.FreeSql.Select<SxExamPaper>().Where(a => a.Status != ExamStatus.End && a.Id == dto.ExamId && a.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();
            if (examData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "试卷不存在!" };

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == dto.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };
            if (gradeData.Status == StudentExamStatus.End)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "答题结束,不允许作答!" };
            if (gradeData.IsSettled)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "已结账,不允许作答!" };

            //查询题目信息
            SxTopic topicData = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.Id == dto.BillQuestionId).FirstAsync();
            if (topicData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不存在!" };
            if (topicData.QuestionType != QuestionType.TeticketTopic)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不是票据题!" };
            if (topicData.PositionId != dto.PositionId && topicData.PointPositionId != dto.PositionId)//综岗，分岗
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "做题岗位和题目岗位不匹配!" };
            SxTopic parentTopicData = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.Id == topicData.ParentId).FirstAsync();
            if (parentTopicData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "父级题目不存在!" };

            if (!dto.IsPushBill)
            {
                //查询作答记录明细
                SxExamUserGradeDetail gradeDetailData = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id
                    && x.QuestionId == dto.BillQuestionId && x.IsDelete == CommonConstants.IsNotDelete).Master().FirstAsync();
                if (gradeDetailData != null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目已作答!" };
            }
            else
            {
                //查询作答记录明细
                SxExamUserGradeDetail gradeDetailData = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id
                    && x.QuestionId == dto.BillQuestionId).Master().FirstAsync();
                if (gradeDetailData == null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目未作答,不是推送题!" };
            }

            if (dto.CheckBillType == (int)CheckBillType.SendBack && dto.SendBackReasonId.HasValue)
            {
                //查询退回理由
                SxReasonDetails answerReasonData = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == dto.SendBackReasonId).FirstAsync();
                if (answerReasonData == null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "退回理由不存在!" };
            }

            //如果有关联题
            if (topicData.TopicRelationStatus)
            {
                //查看关联题是否有作答详情
                SxExamUserGradeDetail gradeDetail = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().From<SxTopicRelation>(
                    (a, b) =>
                    a.InnerJoin(x => x.QuestionId == b.PreTopicId))
                    .Where((a, b) => a.GradeId == dto.GradeId && a.UserId == user.Id && b.TopicId == topicData.ParentId
                        && a.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (gradeDetail == null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "请先作答关联题目!" };
            }
            #endregion

            #region 审核票据

            #region 非推送票据审核通过
            if (dto.CheckBillType == (int)CheckBillType.Pass && !dto.IsPushBill)
            {
                //1、非干扰票 作答正确
                if (!topicData.IsTrap && !topicData.PushDate.HasValue)
                {
                    response = await PassCommonBill(examData, topicData, parentTopicData, dto, user);
                }

                //2、干扰票 即刻推送 作答错误
                if (topicData.IsTrap && topicData.PushDate.HasValue && topicData.PushDate == topicData.BusinessDate)
                {
                    response = await PassObstructRightAwayBill(examData, topicData, parentTopicData, dto, user);
                }

                //3、干扰票 选择时间推送 作答错误
                if (topicData.IsTrap && topicData.PushDate.HasValue && topicData.PushDate != topicData.BusinessDate)
                {
                    response = await PassObstructSelectTimeBill(examData, topicData, parentTopicData, dto, user);
                }
            }
            #endregion

            #region 推送票据审核通过
            if (dto.CheckBillType == (int)CheckBillType.Pass && dto.IsPushBill)
            {
                response = await PassPushTimeBill(examData, topicData, parentTopicData, dto, user);
            }
            #endregion

            #region 非推送票据退回
            if (dto.CheckBillType == (int)CheckBillType.SendBack)
            {
                //1、非干扰票 作答错误
                if (!topicData.IsTrap && !topicData.PushDate.HasValue)
                {
                    response = await SendBackCommonBill(examData, topicData, parentTopicData, dto, user);
                }

                //2、干扰票退回 即刻推送
                if (topicData.IsTrap && topicData.PushDate.HasValue && topicData.PushDate == topicData.BusinessDate)
                {
                    response = await SendBackObstructRightAwayBill(examData, topicData, parentTopicData, dto, user);
                }

                //3、干扰票退回 选择时间推送
                if (topicData.IsTrap && topicData.PushDate.HasValue && topicData.PushDate != topicData.BusinessDate)
                {
                    response = await SendBackObstructSelectTimeBill(examData, topicData, parentTopicData, dto, user);
                }
            }
            #endregion

            #endregion

            return response;
        }
        #endregion

        #region 累加散户作答记录次数
        /// <summary>
        /// 累加散户作答记录次数
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddedRetailAnswerNumber(RetailGradeCountDto dto, UserTicket user)
        {
            if (dto == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            //初始化散户作答记录计数表
            SxExamRetailGradeCount countData = await DbContext.FreeSql.GetRepository<SxExamRetailGradeCount>().Where(x => x.GradeId == dto.GradeId &&
                x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (countData != null)
            {
                countData.AnswerNumber = countData.AnswerNumber + 1;
                countData.UpdateBy = user.Id;
                countData.UpdateTime = DateTime.Now;
            }
            else
            {
                countData = new SxExamRetailGradeCount();

                countData.UserId = user.Id;
                countData.GradeId = dto.GradeId;
                countData.AnswerNumber = 1;
                countData.CreateBy = user.Id;
                countData.CreateTime = DateTime.Now;
                countData.UpdateBy = user.Id;
                countData.UpdateTime = DateTime.Now;
                countData.TenantId = user.TenantId;
            }

            bool state = false;

            //更新散户作答记录计数
            if (countData.Id > 0)
            {
                state = await DbContext.FreeSql.GetRepository<SxExamRetailGradeCount>().UpdateDiy.SetSource(countData).UpdateColumns(x => new
                {
                    x.AnswerNumber,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }
            //生成散户作答记录计数
            else
            {
                countData.Id = IdWorker.NextId();
                SxExamRetailGradeCount returnData = await DbContext.FreeSql.GetRepository<SxExamRetailGradeCount>().InsertAsync(countData);
                state = returnData != null;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取散户作答次数
        /// <summary>
        /// 获取散户作答次数
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<int>> GetRetailAnswerNumber(long gradeId, UserTicket user)
        {
            //获取散户作答记录计数
            SxExamRetailGradeCount countData = await DbContext.FreeSql.GetRepository<SxExamRetailGradeCount>().Where(x => x.GradeId == gradeId && x.UserId == user.Id &&
                x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (countData == null)
                return new ResponseContext<int> { Data = 0 };

            return new ResponseContext<int> { Data = countData.AnswerNumber };
        }
        #endregion

        #region 获取突发机制
        /// <summary>
        /// 获取突发机制
        /// </summary>
        /// <param name="emergencyType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<EmergencyMechanismViewModel>> GetEmergencyMechanism(int emergencyType)
        {
            ResponseContext<EmergencyMechanismViewModel> response = new ResponseContext<EmergencyMechanismViewModel>();

            EmergencyMechanismViewModel data = new EmergencyMechanismViewModel();

            //查询突发机制类别
            SxEmergency emergencyData = await DbContext.FreeSql.GetRepository<SxEmergency>().Where(x => x.CategoryMark == emergencyType
                && x.Status == 0).FirstAsync();
            if (emergencyData == null)
                return new ResponseContext<EmergencyMechanismViewModel> { Code = CommonConstants.ErrorCode, Data = data, Msg = "不存在突发机制!" };

            //随机查询一条突发机制
            string sql = string.Format("SELECT a.Details,a.ImgUrl FROM sx_emergencydetails a WHERE TenantId={0} AND a.IsDelete = 0 ORDER BY RAND() LIMIT 1",
                    emergencyData.Id);
            var emergencyDetails = await DbContext.FreeSql.Ado.QueryAsync<EmergencyMechanismViewModel>(sql);
            if (emergencyDetails != null && emergencyDetails.Count > 0)
            {
                data.Details = emergencyDetails[0].Details;
                data.ImgUrl = emergencyDetails[0].ImgUrl;
            }

            response.Data = data;

            return response;
        }
        #endregion

        #region 私有方法

        #region 查询票据详情列表
        /// <summary>
        /// 查询票据详情列表
        /// </summary>
        /// <param name="needAnswerTopics">未审核票据列表</param>
        /// <param name="pushTopics">推送票据列表</param>
        /// <param name="questionType">题目类型 0:非干扰票据 1:干扰票据 2:推送票据</param>
        /// <returns></returns>
        private async Task<List<UserBillTopicViewModel>> QueryBillFiles(List<SxTopic> needAnswerTopics, List<SxTopic> pushTopics, int questionType)
        {
            List<UserBillTopicViewModel> resultData = new List<UserBillTopicViewModel>();

            //题目来源
            int questionSource = (int)BillQuestionSource.Unreviewed;

            List<SxTopic> topicList = new List<SxTopic>();
            List<long> topicIds = new List<long>();

            if (questionType == 0)
            {
                topicList = needAnswerTopics.Where(x => !x.IsTrap && !x.PushDate.HasValue).ToList();
                if (topicList == null || topicList.Count == 0)
                    return resultData;
                topicIds = needAnswerTopics.Where(x => !x.IsTrap && !x.PushDate.HasValue).Select(x => x.Id).ToList();
            }
            if (questionType == 1)
            {
                topicList = needAnswerTopics.Where(x => x.IsTrap && x.PushDate.HasValue).ToList();
                if (topicList == null || topicList.Count == 0)
                    return resultData;
                topicIds = needAnswerTopics.Where(x => x.IsTrap && x.PushDate.HasValue).Select(x => x.Id).ToList();
            }
            if (questionType == 2)
            {
                questionSource = (int)BillQuestionSource.HasBeenAnswered;
                if (pushTopics == null || pushTopics.Count == 0)
                    return resultData;
                topicList = pushTopics;
                topicIds = pushTopics.Select(x => x.Id).ToList();
            }

            topicList.ForEach(x =>
             {
                 UserBillTopicViewModel model = new UserBillTopicViewModel();
                 model.ParentQuestionId = x.ParentId;
                 model.BillQuestionId = x.Id;
                 model.TopicName = x.Title;
                 model.BusinessDate = x.BusinessDate;
                 model.TeacherHint = x.TeacherHint;
                 model.BillQuestionSource = questionSource;
                 model.Sort = x.Sort;
                 model.CreateTime = x.CreateTime;

                 resultData.Add(model);
             });

            List<SxTopicFile> topicFileList = new List<SxTopicFile>();

            if (questionType == 0)
            {
                //非干扰票据 取正确票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && x.Type == TopicFileType.Normal
                  && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            if (questionType == 1)
            {
                //干扰票据 取正确票据+错误票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                   || x.Type == TopicFileType.Fault) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            if (questionType == 2)
            {
                //推送票据 取正确票据+推送票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                   || x.Type == TopicFileType.Push) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }

            resultData.ForEach(x =>
            {
                //题目下票据详情列表
                List<SxTopicFile> files = topicFileList.Where(a => a.TopicId == x.BillQuestionId).ToList();

                //设置排序(封面/底面)
                files = SetBillTopicFileSort(files);

                files.ForEach(a =>
                {
                    x.BillFileList.Add(new BillFileViewModel
                    {
                        Url = a.Url,
                        //IsWatermark = a.IsWatermark,
                        CoverBack = (Nullable<int>)a.CoverBack,
                    });
                });
            });

            return resultData;
        }
        #endregion

        #region 设置票据详情列表排序(封面/底面)
        /// <summary>
        /// 设置票据详情列表排序(封面/底面)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<SxTopicFile> SetBillTopicFileSort(List<SxTopicFile> list)
        {
            List<SxTopicFile> result = new List<SxTopicFile>();
            if (list == null || list.Count == 0)
                return result;

            list = list.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();
            int listCount = list.Count;

            //封面
            SxTopicFile coverModel = list.FirstOrDefault(a => a.CoverBack == TopicFileCoverBack.Cover);
            if (coverModel != null)
                list.Remove(coverModel);
            ////底面
            //SxTopicFile backModel = list.FirstOrDefault(a => a.CoverBack == TopicFileCoverBack.Back);
            //if (backModel != null)
            //    list.Remove(backModel);

            if (coverModel != null)
                result.Add(coverModel);

            result.AddRange(list);

            //if (backModel != null)
            //    result.Add(backModel);

            return result;
        }
        #endregion

        #region 查询突发机制
        /// <summary>
        /// 查询突发机制
        /// </summary>
        /// <param name="emergencyType">类别标记</param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> QueryEmergencyMechanism(int emergencyType)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            //查询突发机制类别
            SxEmergency emergencyData = await DbContext.FreeSql.GetRepository<SxEmergency>().Where(x => x.CategoryMark == emergencyType
                && x.Status == 0).FirstAsync();
            if (emergencyData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Data = data, Msg = "不存在突发机制!" };

            //随机查询一条突发机制
            string sql = string.Format("SELECT a.Details AS ErrorWriting,a.ImgUrl FROM sx_emergencydetails a WHERE TenantId={0} AND a.IsDelete = 0 ORDER BY RAND() LIMIT 1",
                    emergencyData.Id);
            var emergencyDetails = await DbContext.FreeSql.Ado.QueryAsync<UserCheckBillTopicViewModel>(sql);
            if (emergencyDetails != null && emergencyDetails.Count > 0)
            {
                data.ErrorWriting = emergencyDetails[0].ErrorWriting;
                data.ImgUrl = emergencyDetails[0].ImgUrl;
            }

            response.Data = data;

            return response;
        }
        #endregion

        #region 查询正确答案
        /// <summary>
        /// 查询正确答案
        /// </summary>
        /// <param name="answerValue"></param>
        /// <returns></returns>
        private async Task<SxReasonDetails> QueryRightAnswers(string answerValue)
        {
            //查询正确答案
            long answerValueId = 0;
            long.TryParse(answerValue, out answerValueId);
            SxReasonDetails reasonData = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == answerValueId).FirstAsync();

            return reasonData;
        }
        #endregion

        #region 非干扰票据审核通过(作答正确)
        /// <summary>
        /// 非干扰票据审核通过(作答正确)
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> PassCommonBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData, UserCheckBillTopicDto dto,
            UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                //记录分数
                Score = topicData.Score,
                Status = AnswerResultStatus.Right,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = "1", // 2020-06-22 审核通过 存 1

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已审核票据记录
            SxAuditedBill auditeBill = new SxAuditedBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录业务日期
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                ReceivePaymentType = parentTopicData.ReceivePaymentType,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成已审核票据记录
                    SxAuditedBill audRecord = await uow.GetRepository<SxAuditedBill>().InsertAsync(auditeBill);

                    if (comGrade != null && topGrade != null && audRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.IsRightAnswers = true;
            data.Score = topicData.Score;
            data.BillTopicType = (int)BillTopicType.CommonBill;
            data.BusinessDate = topicData.BusinessDate;

            response.Data = data;

            return response;
        }
        #endregion

        #region 干扰即刻推送票据审核通过(作答错误)
        /// <summary>
        /// 干扰即刻推送票据审核通过(作答错误)
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> PassObstructRightAwayBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData,
            UserCheckBillTopicDto dto, UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                //记录分数
                Score = 0,
                Status = AnswerResultStatus.Error,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = "1", // 2020-06-22 审核通过 存 1

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已审核票据记录
            SxAuditedBill auditeBill = new SxAuditedBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录推送日期
                BusinessDate = topicData.PushDate.Value,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                ReceivePaymentType = parentTopicData.ReceivePaymentType,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已退回票据记录
            SxSendBackBill sendBackBill = new SxSendBackBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录已退票据业务日期
                BusinessDate = topicData.BusinessDate,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //查询正确答案
            SxReasonDetails reasonData = await QueryRightAnswers(topicData.AnswerValue);
            if (reasonData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,答案不存在,请联系管理员!" };

            //查询错误票据通过突发机制
            ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.PassageErrorBill);
            if (emResponse.Code == CommonConstants.SuccessCode)
            {
                data.ErrorWriting = emResponse.Data.ErrorWriting;
                data.ImgUrl = emResponse.Data.ImgUrl;
            }

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成已审核票据记录
                    SxAuditedBill audRecord = await uow.GetRepository<SxAuditedBill>().InsertAsync(auditeBill);
                    //生成已退回票据记录
                    SxSendBackBill sendRecord = await uow.GetRepository<SxSendBackBill>().InsertAsync(sendBackBill);

                    if (comGrade != null && topGrade != null && audRecord != null && sendRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.IsRightAnswers = false;
            data.Score = 0;
            data.BillTopicType = (int)BillTopicType.ObstructBill;
            data.BusinessDate = topicData.BusinessDate;
            data.PushDate = topicData.PushDate;
            data.IsRightAwayPush = true;
            //正确答案提示
            data.RightAnswers = reasonData.Details;

            response.Data = data;

            return response;
        }
        #endregion

        #region 干扰选择时间推送票据审核通过(作答错误)
        /// <summary>
        /// 干扰选择时间推送票据审核通过(作答错误)
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> PassObstructSelectTimeBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData,
            UserCheckBillTopicDto dto, UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                //记录分数
                Score = 0,
                Status = AnswerResultStatus.Error,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = "1", // 2020-06-22 审核通过 存 1

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化推送票据记录
            SxBillPush pushBill = new SxBillPush
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                PushDate = topicData.PushDate.Value,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已退回票据记录
            SxSendBackBill sendBackBill = new SxSendBackBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录已退票据业务日期
                BusinessDate = topicData.BusinessDate,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //查询正确答案
            SxReasonDetails reasonData = await QueryRightAnswers(topicData.AnswerValue);
            if (reasonData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,答案不存在,请联系管理员!" };

            //查询错误票据通过突发机制
            ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.PassageErrorBill);
            if (emResponse.Code == CommonConstants.SuccessCode)
            {
                data.ErrorWriting = emResponse.Data.ErrorWriting;
                data.ImgUrl = emResponse.Data.ImgUrl;
            }

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成推送票据记录
                    SxBillPush pushRecord = await uow.GetRepository<SxBillPush>().InsertAsync(pushBill);
                    //生成已退回票据记录
                    SxSendBackBill sendRecord = await uow.GetRepository<SxSendBackBill>().InsertAsync(sendBackBill);

                    if (comGrade != null && topGrade != null && pushRecord != null && sendRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.IsRightAnswers = false;
            data.Score = 0;
            data.BillTopicType = (int)BillTopicType.ObstructBill;
            data.BusinessDate = topicData.BusinessDate;
            data.PushDate = topicData.PushDate;
            data.IsRightAwayPush = false;
            //正确答案提示
            data.RightAnswers = reasonData.Details;

            response.Data = data;

            return response;
        }
        #endregion

        #region 推送票据审核通过(作答正确 不生成作答明细)
        /// <summary>
        /// 推送票据审核通过(作答正确 不生成作答明细)
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> PassPushTimeBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData, UserCheckBillTopicDto dto,
            UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            //查询票据推送信息
            SxBillPush pushData = await DbContext.FreeSql.GetRepository<SxBillPush>().Where(x => x.QuestionId == dto.BillQuestionId && x.GradeId == dto.GradeId &&
               x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (pushData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目推送记录不存在!" };

            #region 初始化数据

            pushData.IsDelete = CommonConstants.IsDelete;
            pushData.UpdateBy = user.Id;
            pushData.UpdateTime = DateTime.Now;

            //初始化已审核票据记录
            SxAuditedBill auditeBill = new SxAuditedBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录推送日期
                BusinessDate = topicData.PushDate.Value,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                ReceivePaymentType = parentTopicData.ReceivePaymentType,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //生成已审核票据记录
                    SxAuditedBill audRecord = await uow.GetRepository<SxAuditedBill>().InsertAsync(auditeBill);

                    //更新票据推送信息(删除)
                    int executeRow = await uow.GetRepository<SxBillPush>().UpdateDiy.SetSource(pushData).UpdateColumns(x => new
                    {
                        x.IsDelete,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();

                    if (audRecord != null && executeRow > 0)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.IsRightAnswers = true;

            data.BillTopicType = (int)BillTopicType.PushBill;
            data.BusinessDate = topicData.BusinessDate;
            data.PushDate = topicData.PushDate;
            data.IsRightAwayPush = false;

            response.Data = data;

            return response;
        }
        #endregion

        #region 非干扰票据退回(作答错误)
        /// <summary>
        /// 非干扰票据退回(作答错误)
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> SendBackCommonBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData, UserCheckBillTopicDto dto,
            UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                //记录分数
                Score = 0,
                Status = AnswerResultStatus.Error,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = string.Format("2,{0}", dto.SendBackReasonId), // 2020-06-22 审核退回 存 2,退回理由Id

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已审核票据记录
            SxAuditedBill auditeBill = new SxAuditedBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录业务日期
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                ReceivePaymentType = parentTopicData.ReceivePaymentType,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //查询正确票据退回突发机制
            ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.CorrectBillReturn);
            if (emResponse.Code == CommonConstants.SuccessCode)
            {
                data.ErrorWriting = emResponse.Data.ErrorWriting;
                data.ImgUrl = emResponse.Data.ImgUrl;
            }

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成已审核票据记录
                    SxAuditedBill audRecord = await uow.GetRepository<SxAuditedBill>().InsertAsync(auditeBill);

                    if (comGrade != null && topGrade != null && audRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.IsRightAnswers = false;
            data.Score = 0;
            data.BillTopicType = (int)BillTopicType.CommonBill;
            data.BusinessDate = topicData.BusinessDate;

            response.Data = data;

            return response;
        }
        #endregion

        #region 干扰即刻推送票据退回
        /// <summary>
        /// 干扰即刻推送票据退回
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> SendBackObstructRightAwayBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData,
            UserCheckBillTopicDto dto, UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //作答理由
            SxReasonDetails answerReasonData = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == dto.SendBackReasonId).FirstAsync();
            if (answerReasonData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "退回理由不存在!" };

            //是否是正确答案
            bool isAnswerRight = false;

            SxReasonDetails reasonData = new SxReasonDetails();
            long answerValueId = 0;
            long.TryParse(topicData.AnswerValue, out answerValueId);
            if (dto.SendBackReasonId == answerValueId)
            {
                //答案正确
                isAnswerRight = true;
            }
            else
            {
                //查询正确答案
                reasonData = await QueryRightAnswers(topicData.AnswerValue);
                if (reasonData == null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,答案不存在,请联系管理员!" };
            }

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = string.Format("2,{0}", dto.SendBackReasonId), // 2020-06-22 审核退回 存 2,退回理由Id

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };
            //记录分数
            if (isAnswerRight)
            {
                topicGradeDetail.Score = topicData.Score;
                topicGradeDetail.Status = AnswerResultStatus.Right;
            }
            else
            {
                topicGradeDetail.Score = 0;
                topicGradeDetail.Status = AnswerResultStatus.Error;
            }

            //初始化已审核票据记录
            SxAuditedBill auditeBill = new SxAuditedBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录推送日期
                BusinessDate = topicData.PushDate.Value,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                ReceivePaymentType = parentTopicData.ReceivePaymentType,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已退回票据记录
            SxSendBackBill sendBackBill = new SxSendBackBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录已退票据业务日期
                BusinessDate = topicData.BusinessDate,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成已审核票据记录
                    SxAuditedBill audRecord = await uow.GetRepository<SxAuditedBill>().InsertAsync(auditeBill);
                    //生成已退回票据记录
                    SxSendBackBill sendRecord = await uow.GetRepository<SxSendBackBill>().InsertAsync(sendBackBill);

                    if (comGrade != null && topGrade != null && audRecord != null && sendRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            #region 返回数据

            //作答正确
            if (isAnswerRight)
            {
                data.IsRightAnswers = true;
                data.Score = topicData.Score;

            }
            //作答错误
            else
            {
                //查询错误票据退回(原因错误)突发机制
                ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.IncorrectBillReturnedByWrongReason);
                if (emResponse.Code == CommonConstants.SuccessCode)
                {
                    data.ErrorWriting = emResponse.Data.ErrorWriting;
                    data.ImgUrl = emResponse.Data.ImgUrl;
                }

                data.IsRightAnswers = false;
                data.Score = 0;
                //用户作答答案
                data.UserAnswer = answerReasonData.Details;
                //正确答案
                data.RightAnswers = reasonData.Details;
            }

            data.BillTopicType = (int)BillTopicType.ObstructBill;
            data.BusinessDate = topicData.BusinessDate;
            data.PushDate = topicData.PushDate;
            data.IsRightAwayPush = true;

            #endregion

            response.Data = data;

            return response;
        }
        #endregion

        #region 干扰选择时间推送票据退回
        /// <summary>
        /// 干扰选择时间推送票据退回
        /// </summary>
        /// <param name="examData"></param>
        /// <param name="topicData"></param>
        /// <param name="parentTopicData"></param>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> SendBackObstructSelectTimeBill(SxExamPaper examData, SxTopic topicData, SxTopic parentTopicData,
            UserCheckBillTopicDto dto, UserTicket user)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            #region 初始化数据

            //作答理由
            SxReasonDetails answerReasonData = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == dto.SendBackReasonId).FirstAsync();
            if (answerReasonData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "退回理由不存在!" };

            //是否是正确答案
            bool isAnswerRight = false;

            SxReasonDetails reasonData = new SxReasonDetails();
            long answerValueId = 0;
            long.TryParse(topicData.AnswerValue, out answerValueId);
            if (dto.SendBackReasonId == answerValueId)
            {
                //答案正确
                isAnswerRight = true;
            }
            else
            {
                //查询正确答案
                reasonData = await QueryRightAnswers(topicData.AnswerValue);
                if (reasonData == null)
                    return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,答案不存在,请联系管理员!" };
            }

            //初始化综合分录题作答明细
            SxExamUserGradeDetail comprehensiveGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = topicData.ParentId,
                ParentQuestionId = topicData.ParentId,
                QuestionType = QuestionType.CertificateMainSubTopic,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = parentTopicData.IsReceipt,
                Answer = string.Format("2,{0}", dto.SendBackReasonId), // 2020-06-22 审核退回 存 2,退回理由Id

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };
            //记录分数
            if (isAnswerRight)
            {
                topicGradeDetail.Score = topicData.Score;
                topicGradeDetail.Status = AnswerResultStatus.Right;
            }
            else
            {
                topicGradeDetail.Score = 0;
                topicGradeDetail.Status = AnswerResultStatus.Error;
            }

            //初始化推送票据记录
            SxBillPush pushBill = new SxBillPush
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                PushDate = topicData.PushDate.Value,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            //初始化已退回票据记录
            SxSendBackBill sendBackBill = new SxSendBackBill
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                MonthTaskId = examData.MonthTaskId,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.BillQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                BusinessName = topicData.Title,
                //记录已退票据业务日期
                BusinessDate = topicData.BusinessDate,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(comprehensiveGradeDetail);
                    //生成题目作答明细
                    SxExamUserGradeDetail topGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //生成推送票据记录
                    SxBillPush pushRecord = await uow.GetRepository<SxBillPush>().InsertAsync(pushBill);
                    //生成已退回票据记录
                    SxSendBackBill sendRecord = await uow.GetRepository<SxSendBackBill>().InsertAsync(sendBackBill);

                    if (comGrade != null && topGrade != null && pushRecord != null && sendRecord != null)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            #region 返回数据

            //作答正确
            if (isAnswerRight)
            {
                data.IsRightAnswers = true;
                data.Score = topicData.Score;

            }
            //作答错误
            else
            {
                //查询错误票据退回(原因错误)突发机制
                ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.IncorrectBillReturnedByWrongReason);
                if (emResponse.Code == CommonConstants.SuccessCode)
                {
                    data.ErrorWriting = emResponse.Data.ErrorWriting;
                    data.ImgUrl = emResponse.Data.ImgUrl;
                }

                data.IsRightAnswers = false;
                data.Score = 0;
                //用户作答答案
                data.UserAnswer = answerReasonData.Details;
                //正确答案
                data.RightAnswers = reasonData.Details;
            }

            data.BillTopicType = (int)BillTopicType.ObstructBill;
            data.BusinessDate = topicData.BusinessDate;
            data.PushDate = topicData.PushDate;
            data.IsRightAwayPush = false;

            #endregion

            response.Data = data;

            return response;
        }
        #endregion

        #endregion

    }
}
