using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;
using Yssx.Repository.Extensions;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto.ExamPaper;
using YssxC.S.Pocos.Subject;
using System.Runtime.InteropServices.ComTypes;

namespace YssxC.S.Service
{
    /// <summary>
    /// 游戏关卡题目服务
    /// </summary>
    public class GameTopicService : IGameTopicService
    {
        #region 后台

        #region 添加关卡题目
        /// <summary>
        /// 关卡题目添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(GameTopicDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            #region 获取所选题目相关信息
            //获取题目信息，获取题目的大题，并且包含子题，方便后台直接通过 SxGameTopic 表进行计算
            var topics = await DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => model.TopicIds.Contains(e.Id) || model.TopicIds.Contains(e.ParentId))//需要查询综合题分录题的子题，用于计算节点
                 .ToListAsync(e => new SxGameTopic
                 {
                     CaseId = e.CaseId,
                     TaskId = e.TaskId,
                     TopicId = e.Id,
                     PointPositionId = e.PointPositionId,
                     Sort = e.Sort,
                     TopicTitle = e.Title,
                     QuestionType = e.QuestionType,
                     Score = e.Score,
                     Title = e.Title,
                     TopicParentId = e.ParentId,
                 });

            if (!topics.Any())
            {
                result.Msg = "选择的题目不存在";
                return result;
            }

            #endregion

            #region 查询关卡题目中所有题目，包含综合分录题的子题 

            //所有题目，包含子题相关信息
            var topicAll = topics.Where(e => e.PointPositionId > 0).Distinct()
                .Select(e => new PositionAndQuestionType { PointPositionId = e.PointPositionId, QuestionType = e.QuestionType })
                .ToList();

            #endregion

            #region 关卡题目中包含的所有题目节点
            //计算关卡题目节点
            var topicNodes = GetGameLevelTopicNodes(topicAll);
            if (topicNodes == null && topicNodes.Count == 0)
            {
                result.Msg = "关卡题目节点计算错误，请联系技术人员！";
                return result;
            }

            string strtopicNodes = topicNodes.SerializeObject();
            if (string.IsNullOrEmpty(strtopicNodes))
            {
                result.Msg = "关卡题目节点计算错误，请联系技术人员！";
                return result;
            }

            #endregion

            #region 关卡题目中包含的所有岗位
            var positionIdList = new List<PositionIdAndOperation>();
            var questionTypeList = new List<QuestionType>();

            topicNodes.ForEach(e =>
            {
                questionTypeList.AddRange(e.QuestionTypes);
                positionIdList.AddRange(e.PositionIds);
            });

            var positionIds = positionIdList.Distinct().ToArray();
            //var strPointPositionIds = string.Join(',', positionIds);
            var questionTypes = questionTypeList.Distinct().ToArray();
            var strquestionTypes = string.Join(",", questionTypes);

            if (positionIds == null || positionIds.Length == 0)
            {
                result.Msg = "请设置题目3D岗位。";
                return result;
            }
            #endregion

            topics.ForEach(e =>
            {
                e.Id = IdWorker.NextId();//获取唯一Id
                e.CreateBy = currentUserId;
                e.GameLevelId = model.GameLevelId;
            });

            var totalScore = topics.Where(e => e.TopicParentId == 0).Sum(e => e.Score);//计算该关卡中题目总分

            #region 保存到数据库中

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo = uow.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete);
                    var repo_l = uow.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    //删除该关卡的所有题目
                    await repo.UpdateDiy.Set(c => new SxGameTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUserId,
                    }).Where(c => c.GameLevelId == model.GameLevelId).ExecuteAffrowsAsync();

                    await repo.InsertAsync(topics);//添加到数据库

                    //修改关卡中的岗位
                    await repo_l.UpdateDiy.Set(c => new SxGameLevel
                    {
                        TopicNodes = strtopicNodes,
                        QuestionTypes = strquestionTypes,
                        //Positions = strPointPositionIds,
                        UpdateTime = DateTime.Now,
                        TotalScore = totalScore,
                        UpdateBy = currentUserId,
                    }).Where(c => c.Id == model.GameLevelId).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.SetSuccess(true);//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 计算题目列表
        /// </summary>
        /// <param name="questionTypes"></param>
        /// <returns></returns>
        private static List<GameLevelTopicNodeDto> GetGameLevelTopicNodes(List<PositionAndQuestionType> questionTypes)
        {
            var topicNodes = new List<GameLevelTopicNodeDto>();

            foreach (var questionType in questionTypes)
            {
                var node = GetGameTopicNode(questionType.QuestionType);
                AddTopicNodeAndPositionId(node, questionType, topicNodes);
            }

            //主管审核，有综合分录题的必定有主管审核
            if (questionTypes.Any(e => e.QuestionType == QuestionType.CertifMSub_AccountEntry))
            {
                var positionAndQuestionType = new PositionAndQuestionType
                {
                    PointPositionId = CommonConstants.FinanceManager_C3D,
                    QuestionType = QuestionType.CertifMSub_AccountEntry,
                    //Operation = 2
                };
                AddTopicNodeAndPositionId(GameTopicNode.ZGSH, positionAndQuestionType, topicNodes);
            }

            //出纳审核 只有有付款题时才需要出纳审核
            var ch = questionTypes.Where(e => e.QuestionType == QuestionType.PayTopic || e.QuestionType == QuestionType.ReceiptTopic).ToList();
            if (ch != null && ch.Count > 0)
            {
                ch.ForEach(e =>
                {
                    e.QuestionType = QuestionType.CertifMSub_AccountEntry;
                    AddTopicNodeAndPositionId(GameTopicNode.CNSH, e, topicNodes);
                });
            }
            topicNodes = topicNodes.OrderBy(e => e.TopicNode).ToList();

            return topicNodes;
        }

        /// <summary>
        /// 添加题目节点和题目岗位
        /// </summary>
        /// <param name="gameTopicNode"></param>
        /// <param name="positionAndQuestionType"></param>
        /// <param name="topicNodes"></param>
        private static void AddTopicNodeAndPositionId(GameTopicNode gameTopicNode, PositionAndQuestionType positionAndQuestionType, List<GameLevelTopicNodeDto> topicNodes)
        {
            var a = topicNodes.FirstOrDefault(e => e.TopicNode == gameTopicNode);
            var positionId = positionAndQuestionType.PointPositionId;
            var questionType = positionAndQuestionType.QuestionType;
            //var operation = positionAndQuestionType.Operation;
            if (a == null)
            {
                topicNodes.Add(new GameLevelTopicNodeDto
                {
                    TopicNode = gameTopicNode,
                    PositionIds = new List<PositionIdAndOperation> { new PositionIdAndOperation {
                        //Operation = operation,
                        PositionId = positionId, QuestionType = questionType } },
                    QuestionTypes = new List<QuestionType> { questionType }
                });
            }
            else
            {
                if (!a.PositionIds.Any(e => e.PositionId == positionId && questionType == e.QuestionType))
                    a.PositionIds.Add(new PositionIdAndOperation
                    {
                        //Operation = operation,
                        PositionId = positionId,
                        QuestionType = questionType
                    });

                if (!a.QuestionTypes.Any(e => e == questionType))
                    a.QuestionTypes.Add(questionType);
            }
        }

        #endregion

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GameTopicDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<GameTopicDto>();

            return new ResponseContext<GameTopicDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxGameTopic
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new SxGameTopic
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(c => c.Id == id).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GameTopicPageResponseDto>> ListPage(GameTopicPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<SxGameTopic>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.TopicName), e => e.TopicTitle.Contains(query.TopicName))
                   .OrderByDescending(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<GameTopicPageResponseDto>();

            return new PageResponse<GameTopicPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameTopicListResponseDto>> List(GameTopicListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxGameTopic>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.GameLevelId == query.GameLevelId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.TopicName), e => e.TopicTitle.Contains(query.TopicName))
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<GameTopicListResponseDto>();

            return new ListResponse<GameTopicListResponseDto>(entitys);
        }

        /// <summary>
        /// 获取任务中可选的题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameChoosableTopicDto>> GetChoosableTopicList(long taskId)
        {
            var result = new ListResponse<GameChoosableTopicDto>(CommonConstants.ErrorCode, "");

            var task = await DbContext.FreeSql.Select<SxTask>().Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.Id == taskId).FirstAsync(e => new SxTask { CaseId = e.CaseId, Id = e.Id });
            if (task == null)
            {
                result.SetError("任务不存在。");
                return result;
            }
            //获取任务中的所有题目
            var rTopicData = await DbContext.FreeSql.Select<SxTopic>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.ParentId == 0 && a.TaskId == taskId && a.CaseId == task.CaseId)
                .OrderBy(a => a.Sort)
                .OrderBy(a => a.CreateTime)
                .ToListAsync(a => new GameChoosableTopicDto
                {
                    Id = a.Id,
                    BusinessDate = a.BusinessDate,
                    QuestionType = a.QuestionType,
                    PositionId = a.PositionId,
                    Score = a.Score,
                    Status = a.Status,
                    Title = a.Title,
                    Sort = a.Sort,
                });

            //获取游戏关卡已选择的题目
            var gameTopic = await DbContext.FreeSql.Select<SxGameTopic>()
                .Where(b => b.TaskId == taskId && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(a => a.TopicId);

            rTopicData.ForEach(a =>
            {
                a.IsSelected = gameTopic.Any(b => b == a.Id);
            });

            result.SetSuccess(rTopicData);
            return result;
        }

        #endregion

        #region 前台

        #region 票据题目相关
        /// <summary>
        /// 票据题目列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<UserBillTopicViewModel>> GetGameUserBillTopicList(UserGameTopicListRequestDto query, long userId)
        {
            List<UserBillTopicViewModel> listData = new List<UserBillTopicViewModel>();

            if (query == null || !query.PositionId.HasValue)
                return new ListResponse<UserBillTopicViewModel>() { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            var ishavegl = await DbContext.FreeSql.GetRepository<SxGameLevel>().Where(x => x.Id == query.GameLevelId).AnyAsync();
            if (!ishavegl) return new ListResponse<UserBillTopicViewModel>() { Code = CommonConstants.ErrorCode, Msg = "关卡不存在!" };

            //查询主作答记录
            var gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == userId).AnyAsync();
            if (!gradeData)
                return new ListResponse<UserBillTopicViewModel>() { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            //获取关卡中设计的业务日期
            var bdate = await GetGameLevelDate(query.GameLevelId);
            var startDate = bdate.StartDate;
            var endDate = bdate.EndDate;

            #region 未审核票据
            if (query.BillStatus == (int)BillStatus.Unreviewed)
            {
                #region 1、查询月份日期范围内并未在作答记录中的题目列表

                //查询月份日期范围内题目列表
                var dateTopics = new List<SxTopic>();

                //非干扰票据+干扰即刻推送票据
                List<SxTopic> noObstructs = await DbContext.FreeSql.GetRepository<SxTopic>()
                    .Where(a => //a.BusinessDate >= startDate && a.BusinessDate < endDate &&
                                ((!a.PushDate.HasValue && !a.IsTrap) || (a.PushDate == a.BusinessDate && a.IsTrap)) && a.QuestionType == QuestionType.TeticketTopic
                                && a.PointPositionId == query.PositionId && a.IsDelete == CommonConstants.IsNotDelete)
                    .From<SxGameTopic>((a, b) => a.InnerJoin(aa => aa.ParentId == b.TopicId && b.GameLevelId == query.GameLevelId && b.IsDelete == CommonConstants.IsNotDelete))
                    .ToListAsync((a, b) => new SxTopic
                    {
                        IsTrap = a.IsTrap,
                        PushDate = a.PushDate,
                        ParentId = a.ParentId,
                        Id = a.Id,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });
                dateTopics.AddRange(noObstructs);

                //干扰推送票据 业务日期在查询日期范围内
                List<SxTopic> obstructs = await DbContext.FreeSql.GetRepository<SxTopic>()
                    .Where(a => a.BusinessDate >= startDate && a.BusinessDate < endDate
                                 && a.IsTrap && a.PushDate > a.BusinessDate && a.QuestionType == QuestionType.TeticketTopic
                                 && a.PointPositionId == query.PositionId && a.IsDelete == CommonConstants.IsNotDelete)
                    .From<SxGameTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.TopicId && b.Id == query.GameLevelId && b.IsDelete == CommonConstants.IsNotDelete))
                    .ToListAsync((a, b) => new SxTopic
                    {
                        IsTrap = a.IsTrap,
                        PushDate = a.PushDate,
                        ParentId = a.ParentId,
                        Id = a.Id,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });
                dateTopics.AddRange(obstructs);

                //查询作答记录中的题目列表
                List<long> gradeTopicIds = await DbContext.FreeSql.Select<SxExamUserGradeDetail>()
                    .From<SxGameTopic>((a, b) => a.InnerJoin(aa => aa.QuestionId == b.TopicId && b.GameLevelId == query.GameLevelId && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.GradeId == query.GradeId && a.UserId == userId && a.QuestionType == QuestionType.TeticketTopic && a.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => a.QuestionId);

                List<long> dateTopicIds = dateTopics.Select(x => x.Id).ToList();

                //未作答的题目Ids
                var noAnswerTopicIds = dateTopicIds.Except(gradeTopicIds).ToList();

                //需要审核的题目(未审核题目/未在作答记录中的题目列表)
                var needAnswerTopics = dateTopics.Where(x => noAnswerTopicIds.Contains(x.Id)).ToList();

                //非干扰票
                List<UserBillTopicViewModel> noObListData = await QueryBillFiles(needAnswerTopics, null, 0);
                listData.AddRange(noObListData);

                //干扰票
                List<UserBillTopicViewModel> obListData = await QueryBillFiles(needAnswerTopics, null, 1);
                listData.AddRange(obListData);

                #endregion

                #region 2、查询推送日期在查询日期范围内的推送列表  业务已设定 推送日期 都为当天的。此处不需要了

                //查询推送表 推送日期在查询日期范围内  业务已设定 推送日期 都为当天的。此处不需要了
                //List<SxTopic> pushTopics = await DbContext.FreeSql.Select<SxTopic>()
                //    .From<SxBillPush>((a, b) => a.InnerJoin(x => x.Id == b.QuestionId))
                //    .Where((a, b) => b.PushDate >= startDate && b.PushDate < endDate && b.UserId == userId && b.GradeId == query.GradeId &&
                //        b.PositionId == query.PositionId && b.IsDelete == CommonConstants.IsNotDelete)
                //    .ToListAsync();

                ////推送票据
                //List<UserBillTopicViewModel> pushData = await QueryBillFiles(null, pushTopics, 2);

                //listData.AddRange(pushData);
                #endregion
            }
            #endregion

            #region 已审核票据
            if (query.BillStatus == (int)BillStatus.Checked)
            {
                var checkedTopics = await DbContext.FreeSql.Select<SxTopic>()
                    .InnerJoin<SxGameTopic>((a, c) => a.Id == c.TopicId && c.GameLevelId == query.GameLevelId && c.IsDelete == CommonConstants.IsNotDelete)
                    .From<SxAuditedBill>((a, b) => a.InnerJoin(x => x.Id == b.QuestionId))
                    .Where((a, b) => b.UserId == userId && b.GradeId == query.GradeId && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new
                    {
                        Id = a.Id,
                        ParentId = a.ParentId,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        IsHasReceivePayment = b.IsHasReceivePayment, //是否有收付款 false:没有 true:有
                        IsDoneReceivePayment = b.IsDoneReceivePayment, //收付款是否已完成 false:未完成 true:已完成
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });

                //综合分录题 Ids
                List<long> parentIds = checkedTopics.Select(x => x.ParentId).ToList();
                //票据分录题作答列表

                #region 分录综合题下的分录题作答明细没有记录ParentId 不能这样查询
                /*
                List<SxExamUserGradeDetail> gardeAccountList = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().Where(x => parentIds.Contains(x.ParentQuestionId) &&
                 x.GradeId == query.GradeId && x.UserId == userId && x.QuestionType == QuestionType.CertifMSub_AccountEntry && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();
                */
                #endregion
                //查询票据题所属父题目中的分录题是否有作答记录
                var gardeAccountList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>(c => c.IsDelete == CommonConstants.IsNotDelete)
                    .Where(c => parentIds.Contains(c.ParentQuestionId) && c.GradeId == query.GradeId && c.UserId == userId && c.QuestionType == QuestionType.CertifMSub_AccountEntry)
                    .ToListAsync(c => new SxTopic { ParentId = c.ParentQuestionId, Id = c.QuestionId });

                //票据题详情 包含回执单                
                List<SxTopicFile> topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => parentIds.Contains(x.ParentTopicId)
                    && (x.Type == TopicFileType.Normal || x.Type == TopicFileType.Push || x.Type == TopicFileType.Reply) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                checkedTopics.ForEach(x =>
                {
                    UserBillTopicViewModel model = new UserBillTopicViewModel();
                    model.ParentQuestionId = x.ParentId;
                    model.BillQuestionId = x.Id;
                    model.TopicName = x.Title;
                    model.BusinessDate = x.BusinessDate;
                    model.TeacherHint = x.TeacherHint;
                    model.Sort = x.Sort;
                    model.CreateTime = x.CreateTime;

                    //分录题有作答记录 则标记已做凭证
                    var hasgardeAccount = gardeAccountList.Where(a => x.ParentId == a.ParentId).Any();
                    if (hasgardeAccount)
                        model.IsDoCertificate = true;

                    //题目下票据详情列表
                    List<SxTopicFile> files = new List<SxTopicFile>();

                    //没有收付款或者未完成 没有回执单
                    if (!x.IsDoneReceivePayment)
                    {
                        files = topicFileList.Where(a => a.TopicId == model.BillQuestionId && a.Type != TopicFileType.Reply).ToList();
                    }
                    //有收付款并且已完成
                    if (x.IsDoneReceivePayment)
                    {
                        files = topicFileList.Where(a => a.TopicId == model.BillQuestionId).ToList();
                    }

                    //设置排序(封面/底面)
                    files = SetBillTopicFileSort(files);

                    files.ForEach(a =>
                    {
                        model.BillFileList.Add(new BillFileViewModel
                        {
                            Url = a.Url,
                            //IsWatermark = a.IsWatermark,
                            CoverBack = (Nullable<int>)a.CoverBack,
                        });
                    });

                    listData.Add(model);
                });
            }
            #endregion

            #region 已退回票据
            if (query.BillStatus == (int)BillStatus.Returned)
            {
                var sendbackTopics = await DbContext.FreeSql.Select<SxTopic>().From<SxSendBackBill, SxReasonDetails>(
                   (a, b, c) =>
                   a.InnerJoin(x => x.Id == b.QuestionId)
                   .InnerJoin(x => x.AnswerValue == c.Id.ToString()))
                    .Where((a, b, c) => b.BusinessDate >= startDate && b.BusinessDate < endDate && b.UserId == userId && b.GradeId == query.GradeId &&
                    b.PositionId == query.PositionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b, c) => new
                    {
                        Id = a.Id,
                        ParentId = a.ParentId,
                        Title = a.Title,
                        BusinessDate = a.BusinessDate,
                        TeacherHint = a.TeacherHint,
                        AnswerDetail = c.Details,
                        Sort = a.Sort,
                        CreateTime = a.CreateTime,
                    });

                //票据题Ids
                List<long> billIds = sendbackTopics.Select(x => x.Id).ToList();
                //票据题详情
                List<SxTopicFile> topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => billIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                  || x.Type == TopicFileType.Fault) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                sendbackTopics.ForEach(x =>
                {
                    UserBillTopicViewModel model = new UserBillTopicViewModel();
                    model.ParentQuestionId = x.ParentId;
                    model.BillQuestionId = x.Id;
                    model.TopicName = x.Title;
                    model.BusinessDate = x.BusinessDate;
                    model.TeacherHint = x.TeacherHint;
                    model.AnswerValue = x.AnswerDetail;
                    model.Sort = x.Sort;
                    model.CreateTime = x.CreateTime;

                    //题目下票据详情列表
                    List<SxTopicFile> files = new List<SxTopicFile>();

                    files = topicFileList.Where(a => a.TopicId == model.BillQuestionId).ToList();

                    //设置排序(封面/底面)
                    files = SetBillTopicFileSort(files);

                    files.ForEach(a =>
                    {
                        model.BillFileList.Add(new BillFileViewModel
                        {
                            Url = a.Url,
                            //IsWatermark = a.IsWatermark,
                            CoverBack = (Nullable<int>)a.CoverBack,
                        });
                    });

                    listData.Add(model);
                });
            }
            #endregion

            #region 查询题目是否被标记
            //题目Ids
            List<long> queIds = listData.Select(x => x.BillQuestionId).ToList();
            List<SxTopicMark> markList = await DbContext.FreeSql.GetRepository<SxTopicMark>().Where(x => queIds.Contains(x.QuestionId) && x.GradeId == query.GradeId
                && x.UserId == userId).ToListAsync();
            listData.ForEach(x =>
            {
                if (markList.Where(a => a.QuestionId == x.BillQuestionId).Count() > 0)
                    x.IsMark = true;
            });
            #endregion

            //排序
            listData = listData.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();

            return new ListResponse<UserBillTopicViewModel>(listData);
        }

        /// <summary>
        /// 查询票据详情列表
        /// </summary>
        /// <param name="needAnswerTopics">未审核票据列表</param>
        /// <param name="pushTopics">推送票据列表</param>
        /// <param name="questionType">题目类型 0:非干扰票据 1:干扰票据 2:推送票据</param>
        /// <returns></returns>
        private async Task<List<UserBillTopicViewModel>> QueryBillFiles(List<SxTopic> needAnswerTopics, List<SxTopic> pushTopics, int questionType)
        {
            List<UserBillTopicViewModel> resultData = new List<UserBillTopicViewModel>();

            //题目来源
            int questionSource = (int)BillQuestionSource.Unreviewed;

            List<SxTopic> topicList = new List<SxTopic>();
            List<long> topicIds = new List<long>();

            if (questionType == 0)
            {
                topicList = needAnswerTopics.Where(x => !x.IsTrap && !x.PushDate.HasValue).ToList();
                if (topicList == null || topicList.Count == 0)
                    return resultData;
                topicIds = needAnswerTopics.Where(x => !x.IsTrap && !x.PushDate.HasValue).Select(x => x.Id).ToList();
            }
            if (questionType == 1)
            {
                topicList = needAnswerTopics.Where(x => x.IsTrap && x.PushDate.HasValue).ToList();
                if (topicList == null || topicList.Count == 0)
                    return resultData;
                topicIds = needAnswerTopics.Where(x => x.IsTrap && x.PushDate.HasValue).Select(x => x.Id).ToList();
            }
            if (questionType == 2)
            {
                questionSource = (int)BillQuestionSource.HasBeenAnswered;
                if (pushTopics == null || pushTopics.Count == 0)
                    return resultData;
                topicList = pushTopics;
                topicIds = pushTopics.Select(x => x.Id).ToList();
            }

            topicList.ForEach(x =>
            {
                UserBillTopicViewModel model = new UserBillTopicViewModel();
                model.ParentQuestionId = x.ParentId;
                model.BillQuestionId = x.Id;
                model.TopicName = x.Title;
                model.BusinessDate = x.BusinessDate;
                model.TeacherHint = x.TeacherHint;
                model.BillQuestionSource = questionSource;
                model.Sort = x.Sort;
                model.CreateTime = x.CreateTime;

                resultData.Add(model);
            });

            List<SxTopicFile> topicFileList = new List<SxTopicFile>();

            if (questionType == 0)
            {
                //非干扰票据 取正确票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && x.Type == TopicFileType.Normal
                  && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            if (questionType == 1)
            {
                //干扰票据 取正确票据+错误票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                   || x.Type == TopicFileType.Fault) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            if (questionType == 2)
            {
                //推送票据 取正确票据+推送票据
                topicFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => topicIds.Contains(x.TopicId) && (x.Type == TopicFileType.Normal
                   || x.Type == TopicFileType.Push) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }

            resultData.ForEach(x =>
            {
                //题目下票据详情列表
                List<SxTopicFile> files = topicFileList.Where(a => a.TopicId == x.BillQuestionId).ToList();

                //设置排序(封面/底面)
                files = SetBillTopicFileSort(files);

                files.ForEach(a =>
                {
                    x.BillFileList.Add(new BillFileViewModel
                    {
                        Url = a.Url,
                        //IsWatermark = a.IsWatermark,
                        CoverBack = (Nullable<int>)a.CoverBack,
                    });
                });
            });

            return resultData;
        }

        /// <summary>
        /// 设置票据详情列表排序(封面/底面)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<SxTopicFile> SetBillTopicFileSort(List<SxTopicFile> list)
        {
            List<SxTopicFile> result = new List<SxTopicFile>();
            if (list == null || list.Count == 0)
                return result;

            list = list.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();
            int listCount = list.Count;

            //封面
            SxTopicFile coverModel = list.FirstOrDefault(a => a.CoverBack == TopicFileCoverBack.Cover);
            if (coverModel != null)
                list.Remove(coverModel);
            ////底面
            //SxTopicFile backModel = list.FirstOrDefault(a => a.CoverBack == TopicFileCoverBack.Back);
            //if (backModel != null)
            //    list.Remove(backModel);

            if (coverModel != null)
                result.Add(coverModel);

            result.AddRange(list);

            //if (backModel != null)
            //    result.Add(backModel);

            return result;
        }
        #endregion

        #region 付款题列表

        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList(GameUserCalendarReceivingQuery query, UserTicket user)
        {
            var response = new ResponseContext<List<UserReceivingBillViewModel>>();
            var listData = new List<UserReceivingBillViewModel>();

            var ishavegl = await DbContext.FreeSql.GetRepository<SxGameLevel>().Where(x => x.Id == query.GameLevelId).AnyAsync();
            if (!ishavegl)
            {
                response.SetError("关卡不存在");
                return response;
            }

            //查询主作答记录
            var gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).AnyAsync();
            if (!gradeData)
            {
                response.SetError("主作答记录不存在");
                return response;
            }

            //获取关卡中的收付款题目
            var topics = await DbContext.FreeSql.GetRepository<SxTopic>(a => a.IsDelete == CommonConstants.IsNotDelete).Select
                 .InnerJoin<SxGameTopic>((a, c) => a.Id == c.TopicId && c.GameLevelId == query.GameLevelId && c.IsDelete == CommonConstants.IsNotDelete)
                 .Where(a => a.PointPositionId == query.PositionId)
                 .WhereIf(query.TopicType == (int)ReceivePaymentType.PaymentTopic, a => a.QuestionType == QuestionType.PayTopic)
                 .WhereIf(query.TopicType == (int)ReceivePaymentType.ReceiveTopic, a => a.QuestionType == QuestionType.ReceiptTopic)
                 .OrderBy(a => a.Sort)
                 .ToListAsync(a => new
                 {
                     a.Id,
                     ParentQuestionId = a.ParentId,
                     TopicName = a.Title,
                 });

            //获取作答记录，去除已作答的完的题目
            var grades = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .WhereCascade(b => b.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin<SxGameTopic>((b, c) => b.QuestionId == c.TopicId && c.GameLevelId == query.GameLevelId && c.PointPositionId == query.PositionId)
                .Where(b => b.UserId == user.Id && b.GradeId == query.GradeId)
                .WhereIf(query.TopicType == (int)ReceivePaymentType.PaymentTopic, b => b.QuestionType == QuestionType.PayTopic)
                .WhereIf(query.TopicType == (int)ReceivePaymentType.ReceiveTopic, b => b.QuestionType == QuestionType.ReceiptTopic)
                .ToListAsync(b => b.QuestionId);

            var t = topics.Where(e => !grades.Contains(e.Id)).ToList();
            var parentids = new List<long>();
            foreach (var item in t.GroupBy(e => e.ParentQuestionId))
            {
                var model = new UserReceivingBillViewModel
                {
                    ParentQuestionId = item.Key,
                    TopicName = item.FirstOrDefault()?.TopicName,
                    TopicType = query.TopicType,
                };
                parentids.Add(item.Key);

                listData.Add(model);
            }

            // 已作答的题目的 回执单详情
            var receiptFileList = await DbContext.FreeSql.GetRepository<SxTopicFile>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => parentids.Contains(e.ParentTopicId) && (e.Type == TopicFileType.Normal || e.Type == TopicFileType.Push))
                .OrderBy(e => e.Sort)
                .ToListAsync(e => new
                {
                    e.ParentTopicId,
                    e.Url,
                    e.CoverBack
                });

            listData.ForEach(e =>
            {
                e.BillReceiptFileList = receiptFileList
                    .Where(a => a.ParentTopicId == e.ParentQuestionId)
                    .Select(a =>
                        new BillReceiptFileViewModel
                        {
                            CoverBack = (int?)a.CoverBack,
                            Url = a.Url
                        }).ToList();
            });

            response.Data = listData;

            return response;
        }

        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetGamePaymentTopicInfoList(GameReceivePaymentTopicInfoQuery query, UserTicket user)
        {
            var response = new ResponseContext<List<PaymentTopicInfoViewModel>>();
            List<PaymentTopicInfoViewModel> listData = new List<PaymentTopicInfoViewModel>();

            if (query == null)
                return new ResponseContext<List<PaymentTopicInfoViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            var ishavegl = await DbContext.FreeSql.GetRepository<SxGameLevel>().Where(x => x.Id == query.GameLevelId).AnyAsync();
            if (!ishavegl)
            {
                response.SetError("关卡不存在");
                return response;
            }

            //查询主作答记录
            var gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).AnyAsync();
            if (!gradeData)
            {
                response.SetError("主作答记录不存在");
                return response;
            }

            //获取题目列表
            var topicList = await DbContext.FreeSql.Select<SxTopic>()
                .From<SxTopicPayAccountInfo, SxAssistAccounting, SxExamUserGradeAsset, SxGameTopic>((a, b, c, d, e) =>
                    a.InnerJoin(x => x.Id == b.TopicId)
                    .InnerJoin(x => b.PayeeCompanyId == c.Id)
                    .InnerJoin(x => b.PayBankId == d.BankAccountId)
                    .InnerJoin(x => x.Id == e.TopicId && e.GameLevelId == query.GameLevelId && e.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c, d, e) => a.ParentId == query.ParentQuestionId && a.QuestionType == QuestionType.PayTopic && d.GradeId == query.GradeId && d.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                     && d.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b, c, d, e) => new
                {
                    QuestionId = a.Id,
                    ReceivingBankId = c.Id,
                    Type = c.Type,
                    ReceivingBankName = c.Name,
                    ReceivingOpenBank = c.BankName,
                    ReceivingBranchBank = c.BankBranchName,
                    ReceivingAccount = c.BankAccountNo,
                    PaymentOpenBank = d.OpeningBank,
                    PaymentSubBank = d.SubBranch,
                    PaymentAccount = d.BankAccountNo,
                    PaymentAccountBalance = d.AccountBalance,
                });

            //获取已作答题目回执单
            List<SxTopicFile> receiptList = await DbContext.FreeSql.Select<SxTopicFile>().From<SxExamUserGradeDetail>(
                (a, b) =>
                a.InnerJoin(x => x.TopicId == b.QuestionId))
                .Where((a, b) => b.GradeId == query.GradeId && b.UserId == user.Id && b.QuestionType == QuestionType.PayTopic && b.ParentQuestionId == query.ParentQuestionId
                && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            topicList.ForEach(x =>
            {
                PaymentTopicInfoViewModel model = new PaymentTopicInfoViewModel();
                model.PaymentQuestionId = x.QuestionId;
                model.ReceivingBankId = x.ReceivingBankId;
                model.Type = (Nullable<int>)x.Type;
                model.ReceivingBankName = x.ReceivingBankName;
                model.ReceivingOpenBank = x.ReceivingOpenBank;
                model.ReceivingBranchBank = x.ReceivingBranchBank;
                model.ReceivingAccount = x.ReceivingAccount;
                model.PaymentOpenBank = x.PaymentOpenBank;
                model.PaymentSubBank = x.PaymentSubBank;
                model.PaymentAccount = x.PaymentAccount;
                model.PaymentAccountBalance = x.PaymentAccountBalance;

                SxTopicFile receiptData = receiptList.Where(a => a.TopicId == x.QuestionId).FirstOrDefault();
                if (receiptData != null)
                {
                    model.ReceiptUrl = receiptData.Url;
                    model.IsPaid = true;
                }

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }

        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryGameUserHasDoneReceivingList(GameUserHasDoneReceivingQuery query, UserTicket user)
        {
            var response = new ResponseContext<List<UserReceivingBillViewModel>>();
            var listData = new List<UserReceivingBillViewModel>();

            //查询主作答记录
            var gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                .Where(x => x.Id == query.GradeId && x.UserId == user.Id).AnyAsync();
            if (!gradeData)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };


            //查询票据已审核的 收付款 票据、回单
            List<SxTopicFile> billFileList = await DbContext.FreeSql.Select<SxTopicFile>().WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .From<SxAuditedBill>((a, b) => a.InnerJoin(x => x.ParentTopicId == b.ParentQuestionId))
                .Where((a, b) => b.IsHasReceivePayment && b.IsDoneReceivePayment && b.UserId == user.Id && b.GradeId == query.GradeId
                             && (a.Type == TopicFileType.Normal || a.Type == TopicFileType.Push || a.Type == TopicFileType.Reply))
                .ToListAsync();

            //题目父级Id
            var parentIds = billFileList.Select(x => x.ParentTopicId).Distinct().ToList();

            var topicList = await DbContext.FreeSql.Select<SxTopic>().Where(x => parentIds.Contains(x.Id)).ToListAsync();
            topicList.ForEach(x =>
            {
                var model = new UserReceivingBillViewModel();
                model.ParentQuestionId = x.Id;
                model.TopicName = x.Title;
                model.TopicType = (int)x.ReceivePaymentType;

                var fileList = billFileList.Where(a => a.ParentTopicId == x.Id).ToList();
                //设置封面排序
                fileList = SetBillTopicFileSort(fileList);
                fileList.ForEach(a =>
                 model.BillReceiptFileList.Add(new BillReceiptFileViewModel
                 {
                     Url = a.Url,
                     CoverBack = (Nullable<int>)a.CoverBack,
                 }));

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }

        #endregion

        #region 分录题列表

        /// <summary>
        /// 获取分录题列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAccountEntryTopicList(GameAccountEntryTopicRequest model)
        {
            var result = new ResponseContext<List<AccountingEntryTopicDto>>();

            #region 参数校验
            if (model.PositionId <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "岗位ID必须大于0";
                return result;
            }
            var hasGradeExamId = await DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                .Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).AnyAsync();

            if (!hasGradeExamId)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }
            #endregion

            //获取所有题目Id集合
            var questionInfoList = await DbContext.FreeSql.GetRepository<SxTopic>(b => b.IsDelete == CommonConstants.IsNotDelete).Select
                .InnerJoin<SxGameTopic>((b, c) => b.Id == c.TopicId && c.GameLevelId == model.GameLevelId && c.IsDelete == CommonConstants.IsNotDelete)
                .Where(b => b.PointPositionId == model.PositionId && b.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .OrderBy(b => b.Sort)
                .ToListAsync(b => new AccountingEntryTopicDto
                {
                    BusinessDate = b.BusinessDate,
                    QuestionId = b.Id,
                    TopicFileIds = b.TopicFileIds,
                    Title = b.Title,
                    TeacherHint = b.TeacherHint
                });

            //获取用户作答记录
            var questionIdList = questionInfoList.Select(s => s.QuestionId).ToList();
            var gradeDetailIdList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                .Where(s => questionIdList.Contains(s.QuestionId) && s.GradeId == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(a => new { a.QuestionId, a.AccountEntryStatus });

            questionInfoList.ForEach(s =>
            {
                var files = GetTopicFilesByTopicId(s.TopicFileIds, s.QuestionId);

                var gradeDetail = gradeDetailIdList.Where(p => p.QuestionId == s.QuestionId).FirstOrDefault();
                s.IsAnswered = gradeDetail != null;
                s.AccountEntryStatus = gradeDetail != null ? gradeDetail.AccountEntryStatus : AccountEntryStatus.None;
                s.QuestionFiles = files;
            });

            return new ResponseContext<List<AccountingEntryTopicDto>>(questionInfoList);
        }

        /// <summary>
        /// 获取分录题列表(APP专用)
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAccountEntryTopicList_APP(GameAccountEntryTopicRequest model)
        {
            var result = new ResponseContext<List<AccountingEntryTopicAPPDto>>();

            #region 参数校验
            if (model.PositionId <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "岗位ID必须大于0";
                return result;
            }
            var hasGradeExamId = await DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                .Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).AnyAsync();

            if (!hasGradeExamId)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }
            #endregion

            //获取所有题目Id集合
            var questionInfoList = await DbContext.FreeSql.GetRepository<SxTopic>().Select.WhereCascade(b => b.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin<SxGameTopic>((b, c) => b.Id == c.TopicId && c.GameLevelId == model.GameLevelId)
                .Where(b => b.PointPositionId == model.PositionId && b.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .OrderBy(b => b.Sort)
                .ToListAsync(b => new AccountingEntryTopicAPPDto
                {
                    QuestionId = b.Id,
                    Title = b.Title,
                });

            //获取用户作答记录
            var questionIdList = questionInfoList.Select(s => s.QuestionId).ToList();
            var gradeDetailIdList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                .Where(s => questionIdList.Contains(s.QuestionId) && s.GradeId == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(a => new { a.QuestionId, a.AccountEntryStatus });

            questionInfoList.ForEach(s =>
            {
                var files = GetTopicFilesByTopicId(s.TopicFileIds, s.QuestionId);
                s.CoverUrl = files.FirstOrDefault()?.Url;
                var gradeDetail = gradeDetailIdList.Where(p => p.QuestionId == s.QuestionId).FirstOrDefault();
                s.IsAnswered = gradeDetail != null;
                s.AccountEntryStatus = gradeDetail != null ? gradeDetail.AccountEntryStatus : AccountEntryStatus.None;
            });

            return new ResponseContext<List<AccountingEntryTopicAPPDto>>(questionInfoList);
        }

        /// <summary>
        /// 审核分录列表出纳(会计主管) auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAuditAccountEntryTopicList(GameAuditAccountEntryTopicRequest model)
        {
            var result = new ResponseContext<List<AccountingEntryTopicDto>>();
            var auditType = model.AuditType;
            if (auditType != 0 && auditType != 1)
            {
                result.SetError("审核类型参数有误");
                return result;
            }
            if (auditType == 0 && model.PositionId != CommonConstants.Cashier_C3D)
            {
                result.SetError("岗位Id不正确");
                return result;
            }
            if (auditType == 1 && model.PositionId != CommonConstants.FinanceManager_C3D)
            {
                result.SetError("岗位Id不正确");
                return result;
            }
            var gradeExamId = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync(s => s.ExamId);

            if (gradeExamId == 0)
            {
                result.SetError("没找到答题记录");
                return result;
            }

            var data = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>(a => a.IsDelete == CommonConstants.IsNotDelete).Select
                .From<SxTopic, SxGameTopic>((a, b, c) => a.InnerJoin(aa => aa.QuestionId == b.Id)
                    .InnerJoin(aa => b.Id == c.TopicId && c.GameLevelId == model.GameLevelId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => a.GradeId == model.GradeId && b.IsDelete == CommonConstants.IsNotDelete && b.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .WhereIf(auditType == 0, (a, b, c) => a.IsHasReceivePayment || b.SettlementType == 1)//出纳审核，有收付款题或者综合分录题为现金收款
                .WhereIf(auditType == 1, (a, b, c) => a.AccountEntryStatus == AccountEntryStatus.Cashier || a.AccountEntryStatus == AccountEntryStatus.AccountingManager || (!a.IsHasReceivePayment && b.SettlementType == 0))//主管审核
                .OrderBy((a, b, c) => b.Sort)
                .ToListAsync((a, b, c) => new AccountingEntryTopicDto
                {
                    BusinessDate = a.BusinessDate,
                    QuestionId = b.Id,
                    TopicFileIds = b.TopicFileIds,
                    Title = b.Title,
                    TeacherHint = b.TeacherHint,
                    AccountEntryStatus = a.AccountEntryStatus
                });

            data.ForEach(s =>
            {
                var files = GetTopicFilesByTopicId(s.TopicFileIds, s.QuestionId);

                s.QuestionFiles = files;
                if (auditType == 0 && s.AccountEntryStatus == AccountEntryStatus.Cashier)
                {
                    s.IsAnswered = true;
                }
                if (auditType == 1 && s.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                {
                    s.IsAnswered = true;
                }
            });

            return new ResponseContext<List<AccountingEntryTopicDto>>(data);
        }

        /// <summary>
        /// 审核分录列表出纳(会计主管) auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAuditAccountEntryTopicList_APP(GameAuditAccountEntryTopicRequest model)
        {
            var result = new ResponseContext<List<AccountingEntryTopicAPPDto>>();
            var auditType = model.AuditType;
            if (auditType != 0 && auditType != 1)
            {
                result.SetError("审核类型参数有误");
                return result;
            }
            if (auditType == 0 && model.PositionId != CommonConstants.Cashier_C3D)
            {
                result.SetError("岗位Id不正确");
                return result;
            }
            if (auditType == 1 && model.PositionId != CommonConstants.FinanceManager_C3D)
            {
                result.SetError("岗位Id不正确");
                return result;
            }
            var gradeExamId = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync(s => s.ExamId);

            if (gradeExamId == 0)
            {
                result.SetError("没找到答题记录");
                return result;
            }

            var data = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>(a => a.IsDelete == CommonConstants.IsNotDelete).Select
                .From<SxTopic, SxGameTopic>((a, b, c) => a.InnerJoin(aa => aa.QuestionId == b.Id)
                    .InnerJoin(aa => b.Id == c.TopicId && c.GameLevelId == model.GameLevelId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => a.GradeId == model.GradeId && b.IsDelete == CommonConstants.IsNotDelete && b.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .WhereIf(auditType == 0, (a, b, c) => a.IsHasReceivePayment || b.SettlementType == 1)//出纳审核，有收付款题或者综合分录题为现金收款
                .WhereIf(auditType == 1, (a, b, c) => a.AccountEntryStatus == AccountEntryStatus.Cashier || a.AccountEntryStatus == AccountEntryStatus.AccountingManager || (!a.IsHasReceivePayment && b.SettlementType == 0))//主管审核
                .OrderBy((a, b, c) => b.Sort)
                .ToListAsync((a, b, c) => new AccountingEntryTopicAPPDto
                {
                    QuestionId = b.Id,
                    Title = b.Title,
                    AccountEntryStatus = a.AccountEntryStatus,
                    TopicFileIds = b.TopicFileIds,
                    //CoverUrl = d.Url,
                });

            data.ForEach(s =>
            {
                var files = GetTopicFilesByTopicId(s.TopicFileIds, s.QuestionId);
                s.CoverUrl = files.FirstOrDefault()?.Url;
                if (auditType == 0 && s.AccountEntryStatus == AccountEntryStatus.Cashier)
                {
                    s.IsAnswered = true;
                }
                if (auditType == 1 && s.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                {
                    s.IsAnswered = true;
                }
            });

            return new ResponseContext<List<AccountingEntryTopicAPPDto>>(data);
        }

        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<AccountingEntryAnsweredDto>> GetGameAccountingEntryAnsweredList(GameAccountingEntryTopicRequest model)
        {
            //查询业务日期在时间段内的分录题和结转损溢题
            var result = new PageResponse<AccountingEntryAnsweredDto>();
            //if (model.PositionId <= 0)
            //{
            //    result.Code = CommonConstants.NotFund;
            //    result.Msg = "岗位ID必须大于0";
            //    return result;
            //}
            var auditType = model.AuditType;

            var grade = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }

            int certificateNo = -1;
            int.TryParse(model.KeyWords, out certificateNo);
            var totalCount = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .From<SxTopic>((a, b) =>
                 a.LeftJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.ExamId == exam.Id && a.GradeId == model.GradeId)
                .WhereIf(auditType == 1, (a, b) => a.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                .WhereIf(auditType == 0, (a, b) => a.AccountEntryStatus != AccountEntryStatus.AccountingManager)
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo == 0, (a, b) => model.KeyWords.Contains(b.Title))
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo > 0, (a, b) => a.CertificateNo == certificateNo)
                .Where((a, b) => (b.QuestionType == QuestionType.CertifMSub_AccountEntry || b.QuestionType == QuestionType.ProfitLoss))
                .CountAsync();

            var data = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .From<SxTopic, SxExamCertificateDataRecord>((a, b, c) =>
                 a.LeftJoin(aa => aa.QuestionId == b.Id)
                 .LeftJoin(aa => aa.QuestionId == c.QuestionId))
                .Where((a, b, c) => a.ExamId == exam.Id && a.GradeId == model.GradeId)
                .WhereIf(auditType == 1, (a, b, c) => a.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                .WhereIf(auditType == 0, (a, b, c) => a.AccountEntryStatus != AccountEntryStatus.AccountingManager)
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo == 0, (a, b, c) => model.KeyWords.Contains(b.Title))
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo > 0, (a, b, c) => a.CertificateNo == certificateNo)
                .Where((a, b, c) => (b.QuestionType == QuestionType.CertifMSub_AccountEntry || b.QuestionType == QuestionType.ProfitLoss))
                .Where((a, b, c) => c.GradeId == model.GradeId && c.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { a.CreateTime, a.QuestionId, a.BusinessDate, a.CertificateWord, a.CertificateNo, b.Title, a.AccountEntryStatus })
                .OrderBy(a => a.Key.CreateTime)
                .Page(model.PageIndex, model.PageSize)
                .ToListAsync(a => new AccountingEntryAnsweredDto
                {
                    QuestionId = a.Key.QuestionId,
                    BusinessDate = a.Key.BusinessDate,
                    CertificateWord = a.Key.CertificateWord,
                    CertificateNo = a.Key.CertificateNo,
                    Title = a.Key.Title,
                    AccountEntryStatus = a.Key.AccountEntryStatus,
                    TotalBorrowAmount = Convert.ToDecimal("sum(c.BorrowAmount)"),
                    TotalCreditorAmount = Convert.ToDecimal("sum(c.CreditorAmount)")
                });

            #region 查询题目是否被标记
            //题目Ids
            var queIds = data.Select(x => x.QuestionId).ToList();
            var markList = await DbContext.FreeSql.GetRepository<SxTopicMark>().Where(x => queIds.Contains(x.QuestionId) && x.GradeId == model.GradeId).ToListAsync();
            var summaryInfoList = await DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>()
                .Where(s => queIds.Contains(s.QuestionId) && s.GradeId == model.GradeId).Select(s => new { s.QuestionId, s.SummaryInfo }).ToListAsync();
            data.ForEach(x =>
            {
                if (markList.Where(a => a.QuestionId == x.QuestionId).Count() > 0)
                    x.IsMark = true;
                x.SummaryInfo = string.Join(",", summaryInfoList.Where(a => a.QuestionId == x.QuestionId && a.SummaryInfo != "").Select(a => a.SummaryInfo).ToList());
            });
            #endregion
            result.Data = data;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = totalCount;
            result.PageSize = model.PageSize;
            result.PageIndex = model.PageIndex;
            return result;
        }
        #endregion

        #region  结转损溢

        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="caseId"></param>
        /// <param name="gameLevelId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamProfitLossTopicDto>> GetGameProfitLossTopicInfo(long gradeId, long caseId, long gameLevelId)
        {
            var result = new ResponseContext<ExamProfitLossTopicDto>();
            //查找月度任务下结转损溢题
            var topicId = await DbContext.FreeSql.GetRepository<SxTopic>().Select
                .InnerJoin<SxGameTopic>((a, b) => a.Id == b.TopicId && b.GameLevelId == gameLevelId && b.IsDelete == CommonConstants.IsNotDelete)
                .Where(t => t.QuestionType == QuestionType.ProfitLoss && t.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(t => t.Id);

            //作答记录
            var gradeDetail = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                .Where(a => a.GradeId == gradeId && a.QuestionId == topicId)
                .FirstAsync(a => new { a.Id, a.QuestionId });

            //查找公司损溢科目
            var profitLossSubjects = await DbContext.FreeSql.GetRepository<SxSubject>()
                .Where(s => (s.SubjectType == 6 || s.SubjectName == CommonConstants.ProfitSubjectName) && s.CaseId == caseId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync();

            var sIds = profitLossSubjects.Select(s => s.Id).ToList();
            //TODO:去除结转损溢本身的科目
            var examCertificateDataRecords = await DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Select
                .From<SxSubject>((a, b) => a.LeftJoin(aa => aa.SubjectId == b.Id))
                .Where((a, b) => sIds.Contains(a.SubjectId) && a.GradeId == gradeId && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    a.SubjectId,
                    a.BorrowAmount,
                    a.CreditorAmount,
                    InitialBorrowAmount = b.BorrowAmount,
                    InitialCreditorAmount = b.CreditorAmount,
                    b.ProfitLossSubjectType
                });
            var mergeProfitLossTopicSubjectInfos = new List<ProfitLossTopicSubjectInfo>();//合并结转
            var groupList = examCertificateDataRecords.GroupBy(s => new { s.SubjectId, s.ProfitLossSubjectType, s.InitialBorrowAmount, s.InitialCreditorAmount }).
                        Select(s => new ProfitLossTopicSubjectInfo
                        {
                            SubjectId = s.Key.SubjectId,
                            BorrowAmount = s.Sum(c => c.BorrowAmount) + s.Key.InitialBorrowAmount,
                            CreditorAmount = s.Sum(c => c.CreditorAmount) + s.Key.InitialCreditorAmount,
                            Amount = (s.Sum(c => c.BorrowAmount) + s.Key.InitialBorrowAmount) - (s.Sum(c => c.CreditorAmount) + s.Key.InitialCreditorAmount),
                            ProfitLossSubjectType = s.Key.ProfitLossSubjectType
                        }).ToList();
            var profitSubject = profitLossSubjects.Where(s => s.SubjectName == CommonConstants.ProfitSubjectName).FirstOrDefault();

            var totalBorrowAmount = groupList.Sum(s => s.BorrowAmount);
            var totalCreditorAmount = groupList.Sum(s => s.CreditorAmount);

            var totalAmount = totalBorrowAmount > totalCreditorAmount ? totalBorrowAmount : totalCreditorAmount;
            //不分开结转时本年利润金额 如果是正数则在借方 负数则在贷方
            var profitAmount = totalBorrowAmount - totalCreditorAmount;

            //不分开结转：本年利润科目信息
            var profit = new ProfitLossTopicSubjectInfo();
            profit.SubjectId = profitSubject != null ? profitSubject.Id : 0;
            profit.Amount = profitAmount;

            var partProfitLossTopicSubjectInfos = groupList.GroupBy(x => x.ProfitLossSubjectType).Select(x => x.ToList()).ToList();
            partProfitLossTopicSubjectInfos.ForEach(p =>
            {
                var partTotalBorrowAmount = groupList.Sum(s => s.BorrowAmount);
                var partTotalCreditorAmount = groupList.Sum(s => s.CreditorAmount);

                //var partTotalAmount = partTotalBorrowAmount > partTotalCreditorAmount ? partTotalBorrowAmount : partTotalCreditorAmount;
                //不分开结转时本年利润金额 如果是正数则在借方 负数则在贷方
                var partProfitAmount = partTotalBorrowAmount - partTotalCreditorAmount;

                //不分开结转：本年利润科目信息
                var partProfit = new ProfitLossTopicSubjectInfo();
                profit.SubjectId = profitSubject != null ? profitSubject.Id : 0;
                profit.Amount = profitAmount;
                p.Add(partProfit);
            });
            mergeProfitLossTopicSubjectInfos = groupList;

            mergeProfitLossTopicSubjectInfos.Add(profit);
            var examProfitLossTopicDto = new ExamProfitLossTopicDto();
            examProfitLossTopicDto.QuestionId = topicId;
            examProfitLossTopicDto.TotalAmount = totalAmount;
            examProfitLossTopicDto.IsCarryForward = gradeDetail != null;
            examProfitLossTopicDto.MergeProfitLossTopicSubjectInfos = mergeProfitLossTopicSubjectInfos;
            examProfitLossTopicDto.PartProfitLossTopicSubjectInfos = partProfitLossTopicSubjectInfos;
            result.Data = examProfitLossTopicDto;
            return result;
        }

        #endregion

        #region  申报表题目列表

        /// <summary>
        /// 申报表题目列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DeclarationParentDto>>> GetGameDeclarationList(GameDeclarationRequest input)
        {
            var result = new ResponseContext<List<DeclarationParentDto>>();

            var taskid = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                .Where(a => a.Id == input.GameLevelId)
                .FirstAsync(a => a.TaskId);

            if (taskid == 0)
            {
                result.SetError("关卡信息不正确。");
                return result;
            }

            var declarationParentDtos = await DbContext.FreeSql.Select<SxDeclarationParent>()
                    .Where(d => d.TaskId == taskid && d.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(d => d.Sort)
                    .ToListAsync<DeclarationParentDto>();

            if (declarationParentDtos != null && declarationParentDtos.Count > 0)
            {
                //申报主表Ids
                List<long> declareIds = declarationParentDtos.Select(x => x.Id).ToList();
                //查询申报记录表
                List<SxDeclarationRecord> records = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>()
                    .Where(x => x.GradeId == input.GradeId && declareIds.Contains(x.DeclareParentId) && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();
                declarationParentDtos.ForEach(x =>
                {
                    SxDeclarationRecord model = records.FirstOrDefault(a => a.DeclareParentId == x.Id);

                    //未填写
                    if (model == null)
                        x.DeclareStatus = (int)DeclareStatus.NotFilled;
                    else
                        x.DeclareStatus = model.DeclareStatus;
                });

                return new ResponseContext<List<DeclarationParentDto>>(CommonConstants.SuccessCode, "", declarationParentDtos);
            }
            return new ResponseContext<List<DeclarationParentDto>>(CommonConstants.SuccessCode, "没有找到申报表主表!", null);
        }

        #endregion

        #region 其他题型的题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)

        /// <summary>
        /// 获取关卡其他题型的题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskId"></param>
        /// <param name="questionType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetGameOtherTopicList(GameOtherTopicRequest model)
        {
            var result = new ResponseContext<List<ExamOtherQuestionInfo>>();

            var topicIdList = await DbContext.FreeSql.GetRepository<SxTopic>().Select
                .From<SxGameTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.TopicId && b.GameLevelId == model.GameLevelId && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.QuestionType == model.QuestionType && a.IsDelete == CommonConstants.IsNotDelete && a.PointPositionId == model.PositionId)
                .WhereIf(model.QuestionType == QuestionType.DeclarationTpoic, (a, b) => a.DeclarationId == model.DeclarationId)
                .ToListAsync((a, b) => new ExamOtherQuestionInfo
                {
                    QuestionId = a.Id,
                    PositionId = a.PositionId,
                    Title = a.Title,
                    TeacherHint = a.TeacherHint,
                    DeclarationDateStart = a.DeclarationDateStart,
                    DeclarationDateEnd = a.DeclarationDateEnd,
                    DeclarationLimitDate = a.DeclarationLimitDate
                });
            result.Data = topicIdList;
            return result;
        }

        #endregion

        #region 关卡交卷

        /// <summary>
        /// 关卡提交
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GameLevelGradeResponseDto>> SubmitGameExam(SubmitGameExamRequestDto request, long userId)
        {
            long gradeId = request.GradeId;//作答记录id
            long gamegradeId = request.GameGradeId;//作答记录id
            long gameLevelId = request.GameLevelId;//关卡id
            bool isAutoSubmit = request.IsAutoSubmit;//是否自动提交
            var result = new ResponseContext<GameLevelGradeResponseDto>();

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"GradeId都不能为0";
                return result;
            }
            var gameGrade = await DbContext.FreeSql.GetRepository<SxGameUserGrade>()
                .Where(s => s.Id == gamegradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(s => new SxGameUserGrade { Id = s.Id, UserId = s.UserId, GameLevelId = s.GameLevelId, Status = s.Status, GradeId = s.GradeId, UsedSeconds = s.UsedSeconds, TotalMinutes = s.TotalMinutes, TotalPoint = s.TotalPoint, TotalScore = s.TotalScore, });
            //.FirstAsync();
            if (gameGrade == null || gameGrade.UserId != userId)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到闯关答题记录";
                return result;
            }
            else if (gameGrade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"您已经结束闯关！";
                return result;
            }

            #endregion

            try
            {
                //提交考卷,计算本次答卷得分，并保存到数据库中
                var userSkills = await SubmitExamGrade(gameGrade, isAutoSubmit);

                //用户当前关卡历史最佳记录
                var userGameLevelGradeNo1 = await UserGameLevelGradeNo1(gameGrade.GameLevelId, userId);

                //当前关卡历史最佳记录
                var currentGameLevelGradeNo1 = await CurrentGameLevelGradeNo1(gameGrade.GameLevelId);

                result.Data = new GameLevelGradeResponseDto()
                {
                    CurrentGameLevelGrade = new GameGradeInfoDto
                    {
                        GameGradeId = gameGrade.Id,
                        Integral = gameGrade.Point,
                        Score = gameGrade.Score,
                        UsedSeconds = gameGrade.UsedSeconds,
                    },
                    UserGameLevelGradeNo1 = userGameLevelGradeNo1,
                    CurrentGameLevelGradeNo1 = currentGameLevelGradeNo1,
                    UserSkills = userSkills,
                };
            }
            catch (Exception ex)
            {
                result.Msg = "结束闯关失败，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"结束闯关异常:gradeId-{gameGrade.Id},UserId:{userId}";
                CommonLogger.Error(msg, ex);
            }

            return result;
        }

        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="grade"></param>
        /// <param name="gameGrade"></param>
        /// <param name="isAutoSubmit">是否自动提交</param>
        /// <param name="isBatchSubmit">是否批量提交</param>
        private async Task<List<GameGradeUserSkillDto>> SubmitExamGrade(SxGameUserGrade gameGrade, bool isAutoSubmit = false, bool isBatchSubmit = false)
        {
            //用户作答主记录
            var grade = await DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                .Where(s => s.Id == gameGrade.GradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

            //Done:计算考卷分数，返回考试成绩信息 获取本关的作答记录得分 计算总分
            var gameGradeDetails = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .From<SxTopic>((a, c) => a.InnerJoin(aa => aa.QuestionId == c.Id))
                .Where((a, c) => a.GradeId == grade.Id
                        && (a.QuestionId == a.ParentQuestionId //综合分录题中的子题数据存储格式不统一，分录题QuestionId==ParentQuestionId，其他子题不相等
                            || (a.QuestionType == QuestionType.TeticketTopic || a.QuestionType == QuestionType.ReceiptTopic || a.QuestionType == QuestionType.PayTopic)))
                .Master()
                .ToListAsync((a, c) => new GameGradeDetailDto { QuestionId = a.QuestionId, Status = a.Status, Score = a.Score, SkillId = c.SkillId });

            #region 修改作答记录相关信息
            gameGrade.Score = gameGradeDetails.Sum(a => a.Score);
            gameGrade.Status = StudentExamStatus.End;//答题结束，改关卡已完成

            grade.UsedSeconds = gameGrade.UsedSeconds;

            grade.Score = gameGrade.Score;
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isAutoSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            //计算本次作答获得积分
            gameGrade.Point = await ComputeExamIntegral(gameGrade);

            #endregion

            if (!isBatchSubmit)//非批量提交
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_eug = uow.GetRepository<SxExamUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        var repo_gug = uow.GetRepository<SxGameUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete);

                        //if (gameOver) //更新用户总的作答记录
                        await repo_eug.UpdateAsync(grade);

                        //更新本关作答记录
                        int gugcount = await repo_gug.UpdateDiy.SetSource(gameGrade).UpdateColumns(a => new { a.Status, a.Point, a.Score, a.UpdateTime, a.UsedSeconds }).ExecuteAffrowsAsync();

                        if (gugcount == 1)
                            uow.Commit();
                        else
                        {
                            uow.Rollback();
                            throw new Exception("结束闯关异常");
                        }
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            #region 技能
            var rgameGradeDetails = gameGradeDetails.Where(e => e.Status == AnswerResultStatus.Right).ToList();
            //计算用户本次获得的技能
            var userSkillIds = await AddUserSkill(rgameGradeDetails, gameGrade.UserId);

            #endregion
            return userSkillIds;
        }
        /// <summary>
        /// 计算用户本次获得的技能
        /// </summary>
        /// <param name="gameGradeDetails"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<List<GameGradeUserSkillDto>> AddUserSkill(List<GameGradeDetailDto> gameGradeDetails, long userId)
        {
            try
            {
                //本次作答获得技能数据
                var topicSkillIds = gameGradeDetails.Where(e => e.SkillId > 0)
                    .GroupBy(e => new { e.SkillId, e.QuestionType })
                    .Select(e => new { SkillId = e.Key.SkillId, QuestionType = e.Key.QuestionType, Count = e.Count() })
                    .ToList();
                //用户技能数据
                var userSkills = DbContext.FreeSql.GetRepository<SxUserSkill>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(e => e.UserId == userId).ToList(e => new SxUserSkill { Id = e.Id, SkillId = e.SkillId, Score = e.Score });

                //获取技能配置数据
                var skills = FreeSqlCacheExtension.GetCache(CommonConstants.Cache_GetSkills_C3D,
                    () => DbContext.FreeSql.GetRepository<SxSkill>(e => e.IsDelete == CommonConstants.IsNotDelete).Select
                    .ToList(e => new GameGradeUserSkillDto { SkillId = e.Id, SkillName = e.Name, Max = e.Max, })
                    , 180 * 60, false, false);

                var adduserSkills = new List<SxUserSkill>();//用户本次需要更新的技能
                var updateuserSkills = new List<SxUserSkill>();//用户本次需要更新的技能
                foreach (var item in skills)
                {
                    //计算本次闯关获得某个技能的总值
                    var score = topicSkillIds.Where(e => e.SkillId == item.SkillId).Select(e => QuestionTypeToSkillValue(e.QuestionType) * e.Count).FirstOrDefault();
                    var userSkill = userSkills.FirstOrDefault(e => e.SkillId == item.SkillId);
                    if (score > 0)
                    {
                        if (userSkill == null)//用户不存在
                        {
                            var us = new SxUserSkill
                            {
                                Id = IdWorker.NextId(),
                                SkillId = item.SkillId,
                                SkillName = item.SkillName,
                                Score = score,
                                UserId = userId,
                            };
                            adduserSkills.Add(us);
                        }
                        else
                        {
                            userSkill.Score += score;
                            updateuserSkills.Add(userSkill);
                        }
                    }
                    item.AddScore = score;
                    item.Score = userSkill == null ? score : userSkill.Score;
                }

                var repo_us = DbContext.FreeSql.GetRepository<SxUserSkill>(e => e.IsDelete == CommonConstants.IsNotDelete);
                foreach (var item in adduserSkills)
                {
                    await repo_us.InsertAsync(item);
                }
                foreach (var item in updateuserSkills)
                {
                    await DbContext.FreeSql.GetRepository<SxUserSkill>(e => e.IsDelete == CommonConstants.IsNotDelete)
                              .UpdateDiy.Set(c => new SxUserSkill
                              {
                                  Score = item.Score,
                                  UpdateTime = DateTime.Now,
                              }).Where(e => e.Id == item.Id).ExecuteAffrowsAsync();
                };
                return skills;
            }
            catch (Exception ex)
            {
                var msg = $"计算技能异常:,UserId:{userId}";
                CommonLogger.Error(msg, ex);
                return null;
            }
        }

        private decimal QuestionTypeToSkillValue(QuestionType questionType)
        {
            decimal score = 0;
            switch (questionType)
            {
                case QuestionType.CertificateMainSubTopic:
                case QuestionType.TeticketTopic:
                case QuestionType.ReceiptTopic:
                case QuestionType.PayTopic:
                case QuestionType.CertifMSub_AccountEntry: score = 0.1M; break;

                case QuestionType.DeclarationTpoic: score = 0.5M; break;

                case QuestionType.FinancialStatements: score = 2M; break;

                case QuestionType.FinancialStatementsDeclaration: score = 0.5M; break;

                case QuestionType.MainSubQuestion: score = 0.1M; break;

                case QuestionType.ProfitLoss: score = 0.5M; break;

                default: score = 0M; break;
            }
            return score;
        }

        /// <summary>
        /// 计算本次交卷获得积分
        /// </summary>
        /// <param name="gameGrade"></param>
        /// <returns></returns>
        private static async Task<int> ComputeExamIntegral(SxGameUserGrade gameGrade)
        {
            decimal currtIntegral = 0;//本次获得的积分

            try
            {
                if (gameGrade.Score <= 0)
                    return 0;

                ////查询该次关卡总分
                //var gameLevel = await DbContext.FreeSql.GetGuidRepository<SxGameLevel>(x => x.IsDelete == CommonConstants.IsNotDelete)
                //     .Where(e => e.Id == gameGrade.GameLevelId)
                //     .FirstAsync();
                //查询该用户是否已经有得分记录，是否已经作答过（得0分也会有得分记录,有重置游戏功能所以这里，不能过滤掉删除的游戏主作答记录）
                var hasUserIntegrals = await DbContext.FreeSql.GetGuidRepository<SxGameUserGrade>()
                    .Where(e => e.UserId == gameGrade.UserId && e.GameLevelId == gameGrade.GameLevelId && e.Status == StudentExamStatus.End)
                    .AnyAsync();

                #region 积分计算规则
                /*
                 关卡积分：
                    2关卡积分：每个关卡单独设置积分，
                    用户闯关得到积分为：
                    得到积分=【用时比*30%权重+分数比*70%权重】*积分总数
                    积分最小单位为1，结果小数点四舍五入。
                    30%和70%，权重是可设置修改的
                    比如第2关积分是20分，
                    用时6分钟（计时5），得分8分（总分10）
                    【（8/10）*70%+（5/6）*30%】*20=16积分
                    积分获取：
                    第一次按上述比例获取*100%
                    第二次按上述比例的10%获取
                    之后没有积分
                 */
                decimal notFirstRate = 0.1M;//非第一次作答获取积分比率
                decimal scoreWeight = 0.7M;//积分权重
                decimal usedTimeWeight = 1 - scoreWeight;//用时权重
                decimal totalIntegral = gameGrade.TotalPoint;//总积分
                decimal totalScore = gameGrade.TotalScore;//总题目分数
                int totalMinutes = gameGrade.TotalMinutes;//总建议用时

                decimal scoreRate = gameGrade.Score / totalScore;//分数比率
                if (gameGrade.UsedSeconds == 0)
                    gameGrade.UsedSeconds = totalMinutes;
                decimal usedTimeRate = totalMinutes / ((decimal)gameGrade.UsedSeconds) * 60;//用时比率

                usedTimeRate = usedTimeRate > 1 ? 1 : usedTimeRate;
                scoreRate = scoreRate > 1 ? 1 : scoreRate;

                currtIntegral = totalIntegral * (scoreRate * scoreWeight + usedTimeRate * usedTimeWeight);//本次可得分

                if (hasUserIntegrals)//用户已有作答，获取过积分 ，第二次之后默认 得10%
                    currtIntegral = currtIntegral * notFirstRate;
                #endregion

                return (int)currtIntegral;
            }
            catch (Exception ex)
            {
                var msg = $"计算积分异常:,GradeId:{gameGrade.GradeId},UserId:{gameGrade.UserId}";
                CommonLogger.Error(msg, ex);
            }
            return 0;
        }

        #endregion

        #region 公用方法
        /// <summary>
        /// 获取关卡中设计的业务日期范围
        /// </summary>
        /// <returns></returns>
        private async Task<(DateTime StartDate, DateTime EndDate)> GetGameLevelDate(long gameLevelId)
        {
            var topicDates = await DbContext.FreeSql.GetRepository<SxGameTopic>()
                .Where(x => x.GameLevelId == gameLevelId)
                .From<SxTopic>((a, b) => a.InnerJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .ToListAsync((a, b) => b.BusinessDate);
            var glbdate = topicDates.Distinct().Where(e => e != null && e != new DateTime()).OrderBy(e => e).ToList();

            var startDate = glbdate[0];
            var endDate = DateTime.Now;
            if (glbdate.Count > 1)
            {
                endDate = glbdate[glbdate.Count - 1];
            }
            else
            {
                endDate = startDate;
            }
            endDate = endDate.AddDays(1);//追加一天，所以使用时，不能使用等于，只能使用>、<

            return (startDate, endDate);
        }

        /// <summary>
        /// 用户当前关卡历史最佳记录
        /// </summary>
        /// <param name="gameLevelId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        internal static async Task<GameGradeInfoDto> UserGameLevelGradeNo1(long gameLevelId, long userId)
        {
            var gameGrade = await DbContext.FreeSql.GetRepository<SxGameUserGrade>(s => s.IsDelete == CommonConstants.IsNotDelete)
                .Where(s => s.GameLevelId == gameLevelId && s.UserId == userId && s.Status == StudentExamStatus.End)
                .OrderByDescending(s => s.Score)
                .OrderBy(s => s.UsedSeconds)
                .OrderByDescending(s => s.CreateTime)
                .FirstAsync(s => new GameGradeInfoDto
                {
                    GameGradeId = s.Id,
                    Score = s.Score,
                    UsedSeconds = s.UsedSeconds
                });
            return gameGrade;
        }

        /// <summary>
        /// 当前关卡历史最佳记录
        /// </summary>
        /// <param name="gameLevelId">关卡id</param>
        /// <param name="top">前top名</param>
        /// <returns></returns>
        internal static async Task<CurrentGameLevelGradeNo1> CurrentGameLevelGradeNo1(long gameLevelId)
        {
            var datas = await GameLevelGradeTop(gameLevelId, 1);
            if (datas == null || datas.Count == 0)
                return null;
            else
                return datas.FirstOrDefault();
        }

        /// <summary>
        /// 当前关卡历史最佳记录
        /// </summary>
        /// <param name="gameLevelId">关卡id</param>
        /// <param name="top">前top名</param>
        /// <returns></returns>
        private static async Task<List<CurrentGameLevelGradeNo1>> GameLevelGradeTop(long gameLevelId, int top = 1)
        {
            var gameGrades = await DbContext.FreeSql.GetRepository<SxGameUserGrade>(s => s.IsDelete == CommonConstants.IsNotDelete)
                .Where(s => s.GameLevelId == gameLevelId && s.Status == StudentExamStatus.End && s.Score > 0)
                .OrderByDescending(s => s.Score)
                .OrderBy(s => s.UsedSeconds)
                .OrderByDescending(s => s.CreateTime)
                .Limit(top)
                .ToListAsync(s => new CurrentGameLevelGradeNo1
                {
                    UserId = s.UserId,
                    GameGradeId = s.Id,
                    Score = s.Score,
                    UsedSeconds = s.UsedSeconds,
                    CreateTime = s.CreateTime
                });
            var userids = gameGrades.Select(e => e.UserId).ToArray();
            var users = await DbContext.SxFreeSql.GetRepository<YssxUser>(s => s.IsDelete == CommonConstants.IsNotDelete)
                  .Where(s => userids.Contains(s.Id)).ToListAsync(e => new YssxUser { Id = e.Id, NikeName = e.NikeName, Photo = e.Photo });

            gameGrades.ForEach(e =>
            {
                var user = users.Where(f => f.Id == e.UserId).FirstOrDefault();
                if (user != null)
                {
                    e.UserName = user.NikeName ?? "匿名英雄";
                    e.Photo = user.Photo;
                }
            });

            return gameGrades;
        }
        #endregion

        #region 开始闯关、关卡作答进度

        /// <summary>
        /// 获取用户关卡作答进度
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameLevelTopicNodeDto>> GetUserGameGradeProgress(long gameGradeId, long userId)
        {
            var result = new ListResponse<GameLevelTopicNodeDto>(CommonConstants.ErrorCode, "");
            long gameLevelId = 0;
            long gradeId = 0;

            #region 参数校验
            var gameGrade = await DbContext.FreeSql.GetRepository<SxGameUserGrade>().Select
                .Where(s => s.Id == gameGradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(e => new SxGameUserGrade { Id = e.Id, GradeId = e.GradeId, GameLevelId = e.GameLevelId, UserId = e.UserId });

            if (gameGrade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "关卡不存在!";
                return result;
            }

            gameLevelId = gameGrade.GameLevelId;
            gradeId = gameGrade.GradeId;

            var gameLevel = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                .Where(s => s.Id == gameLevelId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync(s => new SxGameLevel { Id = s.Id, TopicNodes = s.TopicNodes, });

            if (gameLevel == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "关卡不存在!";
                return result;
            }

            if (string.IsNullOrWhiteSpace(gameLevel.TopicNodes))
            {
                result.SetError("关卡设置不正确。");
                return result;
            }
            #endregion

            //获取关卡所有节点
            var currLevelTopicNodes = gameLevel.TopicNodes.DeserializeObject<List<GameLevelTopicNodeDto>>();
            currLevelTopicNodes = currLevelTopicNodes.OrderBy(e => e.TopicNode).ToList();//排序

            var userCurrentNode = await GetUserGameLevelGrade(gameLevelId, gradeId, currLevelTopicNodes); //计算当前用户作答节点

            result.SetSuccess(userCurrentNode);

            return result;
        }

        /// <summary>
        /// 开始闯关
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<UserGameLevelGrade>> GameLeveLStart(GameLeveStartRequestDto model, UserTicket user)
        {
            var result = new ResponseContext<UserGameLevelGrade>(CommonConstants.ErrorCode, "");
            var userCurrentNodes = new List<GameLevelTopicNodeDto>();
            SxGameLevel gameLevel = null;//当前关卡信息
            long gradeId = 0;//当前试卷作答记录信息
            CreateSetBookStatus createSetBookStatus = CreateSetBookStatus.NotCreate;//当前试卷作答记录信息
            SxGameUserGrade gameGrade = null; //当前关卡作答记录信息
            List<GameLevelTopicNodeDto> currLevelTopicNodes = null; //当前关卡拥有的题目节点
            var gameGradeId = 0L;
            var usedSeconds = 0D;

            #region 参数校验
            var dataSource = await DbContext.FreeSql.Select<SxExamPaper>().WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Where(a => a.Status != ExamStatus.End && a.Id == model.ExamId)
                .From<SxTask>((a, b) => a.InnerJoin(x => x.MonthTaskId == b.Id))
                .Limit(1)
                .ToListAsync((a, b) => new GameExamPaperDto { CaseId = a.CaseId, TotalScore = a.TotalScore, IsCreateSetBook = b.RequireAccountSet, ExamId = a.Id, });

            if (dataSource == null || dataSource.Count == 0)
            {
                result.SetError("试卷不存在!");
                return result;
            }
            var examPaper = dataSource[0];

            gameLevel = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                .Where(s => s.Id == model.GameLevelId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(s => new SxGameLevel { Id = s.Id, TopicNodes = s.TopicNodes, Sort = s.Sort, CaseId = s.CaseId, TaskId = s.TaskId, TotalMinutes = s.TotalMinutes, Point = s.Point, TotalScore = s.TotalScore, });

            if (gameLevel == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "关卡不存在!";
                return result;
            }

            if (string.IsNullOrWhiteSpace(gameLevel.TopicNodes))
            {
                result.SetError("关卡设置不正确。");
                return result;
            }

            #endregion

            currLevelTopicNodes = gameLevel.TopicNodes.DeserializeObject<List<GameLevelTopicNodeDto>>();
            currLevelTopicNodes = currLevelTopicNodes.OrderBy(e => e.TopicNode).ToList();//排序

            //当前关卡作答记录信息
            gameGrade = await DbContext.FreeSql.GetRepository<SxGameUserGrade>()
                      .Where(s => s.GameLevelId == model.GameLevelId && s.UserId == user.Id
                               && s.Status == StudentExamStatus.Started && s.IsDelete == CommonConstants.IsNotDelete)
                      .FirstAsync(e => new SxGameUserGrade { Id = e.Id, GradeId = e.GradeId, CreateSetBookStatus = e.CreateSetBookStatus, UsedSeconds = e.UsedSeconds });

            if (gameGrade == null)//用户没有作答记录，从改关卡的第一个节点做起
            {
                //创建用户的当前关卡作答主记录
                var tuple = await CreateGameUserGrade(examPaper, user, gameLevel);
                gameGradeId = tuple.GameGradeId;
                gradeId = tuple.GradeId;
                createSetBookStatus = tuple.CreateSetBookStatus;
                userCurrentNodes.Add(currLevelTopicNodes[0]);
            }
            else //用户有作答记录，
            {
                gameGradeId = gameGrade.Id;
                gradeId = gameGrade.GradeId;
                createSetBookStatus = gameGrade.CreateSetBookStatus;
                //计算当前用户作答节点
                userCurrentNodes = await GetUserGameLevelGrade(model.GameLevelId, gameGrade.GradeId, currLevelTopicNodes);
                usedSeconds = gameGrade.UsedSeconds;
            }

            var userGameLevelGrade = new UserGameLevelGrade
            {
                GameGradeId = gameGradeId,
                GradeId = gradeId,
                IsCreateSetBook = (createSetBookStatus == CreateSetBookStatus.NoCreate),
                TopicNodes = currLevelTopicNodes,
                UserCurrentNodes = userCurrentNodes,
                UsedSeconds = usedSeconds,
            };

            result.SetSuccess(userGameLevelGrade);

            return result;
        }

        /// <summary>
        /// 创建用户的当前关卡作答主记录
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="user"></param>
        /// <param name="gameLevel"></param>
        /// <returns></returns>
        private async Task<(long GameGradeId, long GradeId, CreateSetBookStatus CreateSetBookStatus)> CreateGameUserGrade(GameExamPaperDto examPaper, UserTicket user, SxGameLevel gameLevel)
        {
            int platform = 1;//平台：0：网页版 ，1：3D版,兼容以前网页版系统必须设置默认值0
            long examId = examPaper.ExamId;
            #region 初始化数据

            #region 主作答记录
            //初始化主作答记录
            SxExamUserGrade gradeData = new SxExamUserGrade
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examId,
                ExamPaperScore = examPaper.TotalScore,
                Status = StudentExamStatus.Started,
                Platform = platform,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                TenantId = user.TenantId,
            };
            //是否创建账套，第一关是才创建账套
            if (examPaper.IsCreateSetBook && gameLevel.Sort == 1)
            {
                //当前关卡作答记录信息
                var has = await DbContext.FreeSql.GetRepository<SxGameUserGrade>(s => s.IsDelete == CommonConstants.IsNotDelete)
                          .Where(s => s.GameLevelId == gameLevel.Id && s.UserId == user.Id && s.CreateSetBookStatus == CreateSetBookStatus.HasCreated)
                          .AnyAsync();
                if (has)//重复闯关时，也不需要重新创建账套
                    gradeData.CreateSetBookStatus = CreateSetBookStatus.NoCreate;
                else
                    gradeData.CreateSetBookStatus = CreateSetBookStatus.NotCreate;
            }
            else
            {
                gradeData.CreateSetBookStatus = CreateSetBookStatus.NotCreate;
            }
            #endregion

            #region 关卡作答记录
            //生成关卡作答记录
            var gameUserGrade = new SxGameUserGrade
            {
                Id = IdWorker.NextId(),//获取唯一Id
                UserId = user.Id,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                GameLevelId = gameLevel.Id,
                GradeId = gradeData.Id,
                CaseId = gameLevel.CaseId,
                TaskId = gameLevel.TaskId,
                ExamId = examId,
                Status = StudentExamStatus.Started,
                CreateSetBookStatus = gradeData.CreateSetBookStatus,
                IsNew = true,
                TotalScore = gameLevel.TotalScore,
                GameLevelSort = gameLevel.Sort,
                TotalMinutes = gameLevel.TotalMinutes,
                TotalPoint = gameLevel.Point,
            };
            #endregion

            #region 初始化最近作答记录表
            //初始化最近作答记录表
            SxExamUserLastGrade lastData = await DbContext.FreeSql.GetRepository<SxExamUserLastGrade>()
                .Where(x => x.ExamId == examId && x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete && x.Platform == platform)
                .FirstAsync();
            if (lastData != null)
            {
                lastData.GradeId = gradeData.Id;
                lastData.UpdateBy = user.Id;
                lastData.UpdateTime = DateTime.Now;
            }
            else
            {
                lastData = new SxExamUserLastGrade
                {
                    ExamId = examId,
                    UserId = user.Id,
                    GradeId = gradeData.Id,
                    Platform = platform,

                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    TenantId = user.TenantId
                };
            }
            #endregion

            #region  用户作答信息资产表
            //初始化用户作答信息资产表
            List<SxExamUserGradeAsset> assetList = new List<SxExamUserGradeAsset>();
            //获取公司资产账户
            List<SxBankAccount> accountList = null;
            //如果是第一关，去公司资产账户初始值
            if (gameLevel.Sort == 1)
            {
                accountList = await DbContext.FreeSql.GetRepository<SxBankAccount>().Where(x => x.CaseId == examPaper.CaseId
                      && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                accountList.ForEach(x =>
                {
                    assetList.Add(new SxExamUserGradeAsset
                    {
                        Id = IdWorker.NextId(),
                        UserId = user.Id,
                        ExamId = examId,
                        GradeId = gradeData.Id,
                        BankAccountId = x.Id,
                        OpeningBank = x.OpeningBank,
                        SubBranch = x.Branch,
                        BankAccountNo = x.AccountNo,
                        InitialAccountAmount = x.Balance,
                        TotalAccount = x.Balance,
                        AccountUsedAmount = 0,
                        AccountBalance = x.Balance,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                        TenantId = user.TenantId,
                    });
                });
            }
            else //如果不是第一关，获取上一个关用户作答后的公司资金账户    
            {
                var prevgameLevel = gameLevel.Sort - 1;
                var prevUserAset = await DbContext.FreeSql.GetRepository<SxExamUserGradeAsset>().Select.WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                         .From<SxGameUserGrade, SxGameLevel>((a, b, c) => a.InnerJoin(aa => aa.GradeId == b.GradeId)
                            .InnerJoin(bb => b.GameLevelId == c.Id && b.GameLevelSort == prevgameLevel && b.TaskId == gameLevel.TaskId))
                         .Where((a, b, c) => b.UserId == user.Id)
                         .ToListAsync<SxExamUserGradeAsset>();

                prevUserAset.ForEach(x =>
                    assetList.Add(new SxExamUserGradeAsset
                    {
                        Id = IdWorker.NextId(),
                        UserId = user.Id,
                        ExamId = examId,
                        GradeId = gradeData.Id,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                        TenantId = user.TenantId,
                        BankAccountId = x.BankAccountId,
                        OpeningBank = x.OpeningBank,
                        SubBranch = x.SubBranch,
                        BankAccountNo = x.BankAccountNo,
                        InitialAccountAmount = x.InitialAccountAmount,
                        TotalAccount = x.TotalAccount,
                        AccountUsedAmount = x.AccountUsedAmount,
                        AccountBalance = x.AccountBalance,
                    }));
            }

            #endregion

            #endregion

            #region 新增数据            
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var repo = uow.GetRepository<SxGameUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete);
                    //生成关卡主作答记录
                    var gradeModel = await uow.GetRepository<SxExamUserGrade>().InsertAsync(gradeData);

                    //更新当前用户在该关卡的最新作答记录
                    await repo.UpdateDiy.Set(c => new SxGameUserGrade
                    {
                        IsNew = false,
                        UpdateTime = DateTime.Now,
                    }).Where(e => e.GameLevelId == gameLevel.Id && e.UserId == user.Id && e.IsNew == true).ExecuteAffrowsAsync();

                    //关卡作答记录的关系记录
                    await repo.InsertAsync(gameUserGrade);//添加到数据库

                    //更新最近作答记录
                    if (lastData.Id > 0)
                    {
                        await uow.GetRepository<SxExamUserLastGrade>().UpdateDiy.SetSource(lastData).UpdateColumns(x => new
                        {
                            x.GradeId,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();
                    }
                    //生成最近作答记录
                    else
                    {
                        lastData.Id = IdWorker.NextId();
                        await uow.GetRepository<SxExamUserLastGrade>().InsertAsync(lastData);
                    }

                    //生成用户作答信息资产记录
                    if (assetList.Count > 0)
                        await uow.GetRepository<SxExamUserGradeAsset>().InsertAsync(assetList);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
            }
            #endregion

            return (gameUserGrade.Id, gameUserGrade.GradeId, gameUserGrade.CreateSetBookStatus);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currLevelTopicNodes">当前关卡拥有的题目节点</param>
        /// <param name="gameLevelId">关卡id</param>
        /// <param name="gradeId">作答记录id</param>
        /// <returns></returns>
        //private async Task<List<GameLevelTopicNodeDto>> GetUserGameLevelGrade(List<GameLevelTopicNodeDto> currLevelTopicNodes, long gameLevelId, long gradeId)
        //{
        //    //计算用户当前做题的对应节点，根据用户做的最后一道题目的类型来判断
        //    var userCurrentNodes = new List<GameLevelTopicNodeDto>();
        //    var ishas = false;//该题型下是否还有题目未做
        //    var currNode = GameTopicNode.Other;//用户当前应该做题的节点
        //    var nextQuestionType = new List<QuestionType>();//下一题题型,默认票据题

        //    //从作答记录中获取当前用户本关最新的做题题型
        //    var lastQuestions = await DbContext.FreeSql.GetRepository<SxGameTopic>().Select
        //            .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
        //            .Where(a => a.GameLevelId == gameLevelId)
        //            .From<SxExamUserGradeDetail>((a, b) => a.InnerJoin(aa => aa.TopicId == b.QuestionId && b.GradeId == gradeId))
        //            //.OrderByDescending((a, b) => a.TopicId)
        //            //.Limit(1)
        //            .ToListAsync((a, b) => new GameLevelQuestionBaseDto { TopicId = a.TopicId, Sort = a.Sort, QuestionType = a.QuestionType, AccountEntryStatus = b.AccountEntryStatus, IsHasReceivePayment = b.IsHasReceivePayment, AccountEntryAuditStatus = b.AccountEntryAuditStatus, });

        //    var lastQuestion = lastQuestions.FirstOrDefault();

        //    if (lastQuestion == null) //当前关卡的最新作答记录中还没提交答案
        //    {
        //        var start = currLevelTopicNodes[0];
        //        //用户当前应该做题的节点
        //        currNode = start.TopicNode;
        //        nextQuestionType = start.QuestionTypes;// QuestionType.TeticketTopic;
        //        userCurrentNodes.Add(start);
        //    }
        //    else
        //    {
        //        var lastQuestionType = lastQuestion.QuestionType;

        //        if (lastQuestionType == QuestionType.CertifMSub_AccountEntry)//如果当前节点为分录，独立处理
        //        {
        //            currNode = GetGameTopicNode(lastQuestionType);//题目类型对应的题目节点
        //            var curr = currLevelTopicNodes.Where(e => e.TopicNode == currNode).FirstOrDefault();
        //            userCurrentNodes = await GetAccountEntryGameTopicNode(lastQuestion, curr, currLevelTopicNodes, gameLevelId, gradeId);
        //        }
        //        else//其他节点计算进度
        //        {
        //            ishas = await GetCurrentNodeHasDotDoTopic(lastQuestion, gameLevelId);
        //            currNode = GetGameTopicNode(lastQuestionType);//题目类型对应的题目节点
        //            var curr = currLevelTopicNodes.Where(e => e.TopicNode == currNode).FirstOrDefault();
        //            if (ishas)//如果当前节点还有题目未做完
        //            {
        //                //curr.TopicNode = currNode;
        //                //curr.QuestionTypes.Add(lastQuestionType);
        //                nextQuestionType.Add(lastQuestionType);
        //                // 获取用户当前需要做的节点中还有题目的岗位的id
        //                curr.PositionIds = await GetUserCurrentTopicNodePositions(gameLevelId, currNode, nextQuestionType, lastQuestion.Sort);// 获取用户当前需要做的节点中有题目的岗位
        //                userCurrentNodes.Add(curr);
        //            }
        //            else//没有题目了，直接做下一个节点
        //            {

        //                var index = currLevelTopicNodes.IndexOf(curr) + 1;
        //                if (index < currLevelTopicNodes.Count)
        //                {
        //                    userCurrentNodes.Add(currLevelTopicNodes[index]);
        //                }
        //                else//当前关卡所有节点已做完
        //                {
        //                    userCurrentNodes = null;
        //                    //userCurrentNodes.Add(curr);
        //                }
        //            }
        //        }
        //    }

        //    return userCurrentNodes;
        //}

        /// <summary>
        /// 判断改题是否为关卡最后一道题
        /// </summary>
        /// <param name="lastQuestion"></param>
        /// <param name="gameLevelId"></param>
        /// <returns></returns>
        public async Task<bool> GetCurrentNodeHasDotDoTopic(GameLevelQuestionBaseDto lastQuestion, long gameLevelId)
        {

            //当前最新作答题目所属题目类型在关卡中是否还有未做题目
            bool v = await DbContext.FreeSql.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                                      .Where(e => e.GameLevelId == gameLevelId && e.QuestionType == lastQuestion.QuestionType && e.TopicId > lastQuestion.TopicId)
                                      .OrderByDescending(e => e.Id)
                                      .AnyAsync();

            return v;
        }


        /// <summary>
        /// 计算当前关卡用户作答最新题目
        /// </summary>
        /// <param name="lastQuestion"></param>
        /// <param name="gameLevelId"></param>
        /// <returns></returns>
        public async Task<List<GameLevelTopicNodeDto>> GetUserGameLevelGrade(long gameLevelId, long gradeId, List<GameLevelTopicNodeDto> currLevelTopicNodes)
        {
            var isHasTopicDo = false;//是否有题目未做

            //计算用户当前做题的对应节点，根据用户做的最后一道题目的类型来判断
            var userCurrentNodes = new List<GameLevelTopicNodeDto>();
            var currNode = GameTopicNode.Other;//用户当前应该做题的节点
            var nextQuestionType = new List<QuestionType>();//下一题题型,默认票据题

            //获取关卡的当前作答记录详情
            var gradeDetails = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                    .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                    .Where(a => a.GradeId == gradeId && a.QuestionType != QuestionType.CertificateMainSubTopic)
                    .ToListAsync(a => new GameLevelQuestionBaseDto
                    {
                        TopicId = a.QuestionId,
                        //Sort = a.Sort,
                        QuestionType = a.QuestionType,
                        AccountEntryStatus = a.AccountEntryStatus,
                        IsHasReceivePayment = a.IsHasReceivePayment,
                        AccountEntryAuditStatus = a.AccountEntryAuditStatus,
                        CreateTime = a.CreateTime,
                    });

            if (gradeDetails == null || gradeDetails.Count == 0)//没有作答记录
            {
                var start = currLevelTopicNodes[0];
                //用户当前应该做题的节点
                currNode = start.TopicNode;
                nextQuestionType = start.QuestionTypes;// QuestionType.TeticketTopic;
                userCurrentNodes.Add(start);
                return userCurrentNodes;
            }

            var lastQuestionType = gradeDetails.OrderByDescending(e => e.CreateTime).FirstOrDefault().QuestionType;//获取最后一个题目的题目类型

            currNode = GetGameTopicNode(lastQuestionType);//题目类型对应的题目节点
            var curr = currLevelTopicNodes.Where(e => e.TopicNode == currNode).FirstOrDefault();
            if (lastQuestionType == QuestionType.CertifMSub_AccountEntry)//如果当前节点为分录，独立处理
            {
                userCurrentNodes = await GetAccountEntryGameTopicNode(curr, gradeDetails, gameLevelId, gradeId);
            }
            else//其他节点计算进度
            {
                var tuple = await GetCurrentNodeHasDotDoTopic(gameLevelId, curr, gradeDetails);

                isHasTopicDo = tuple.Item1;

                if (isHasTopicDo)//如果当前节点还有题目未做完
                {
                    // 获取用户当前需要做的节点中还有题目的岗位的id
                    curr.PositionIds = tuple.Item2;
                    userCurrentNodes.Add(curr);
                }
                else//没有题目了，直接做下一个节点
                {

                    var index = currLevelTopicNodes.IndexOf(curr) + 1;
                    if (index < currLevelTopicNodes.Count)
                    {
                        userCurrentNodes.Add(currLevelTopicNodes[index]);
                    }
                    else//当前关卡所有节点已做完
                    {
                        userCurrentNodes = null;
                        //userCurrentNodes.Add(curr);
                    }
                }
            }

            return userCurrentNodes;
        }

        private async Task<(bool, List<PositionIdAndOperation>)> GetCurrentNodeHasDotDoTopic(long gameLevelId, GameLevelTopicNodeDto CurrNode, List<GameLevelQuestionBaseDto> gradeDetails)
        {
            var questionTypes = CurrNode.PositionIds.Select(e => e.QuestionType).ToArray();
            var isHasTopicDo = false;
            List<PositionIdAndOperation> positionAndQuestionType = null;
            //获取当前关卡该题型的所有题目
            var gameTopics = await DbContext.FreeSql.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                                      .Where(e => e.GameLevelId == gameLevelId && questionTypes.Contains(e.QuestionType))
                                      .ToListAsync(e => new SxGameTopic { PointPositionId = e.PointPositionId, TopicId = e.TopicId, QuestionType = e.QuestionType });

            if (gameTopics != null && gameTopics.Count > 0)
            {
                var gradeTopic = gradeDetails.Where(e => questionTypes.Contains(e.QuestionType)).ToList();
                if (gradeTopic.Count < gameTopics.Count)//作答题目总数小于
                {
                    isHasTopicDo = true;
                    var gradeTopicIds = gradeTopic.Select(e => e.TopicId).ToArray();
                    positionAndQuestionType = gameTopics.Where(e => !gradeTopicIds.Contains(e.TopicId))
                         .GroupBy(e => new { e.PointPositionId, e.QuestionType })
                         .Select(e => new PositionIdAndOperation { PositionId = e.Key.PointPositionId, QuestionType = e.Key.QuestionType }).ToList();
                }
            }
            //用户当前应该做题的岗位id
            return (isHasTopicDo, positionAndQuestionType);
        }
        /// <summary>
        /// 获取用户当前需要做的节点中有的岗位(此方法不用考虑 分录节点下的分录题)
        /// </summary>
        /// <param name="gameLevelId"></param>
        /// <param name="currNode"></param>
        /// <param name="questionTypes"></param>
        /// <param name="currSort"></param>
        /// <returns></returns>
        private static async Task<List<PositionIdAndOperation>> GetUserCurrentTopicNodePositions(long gameLevelId, GameTopicNode currNode, List<QuestionType> questionTypes, int currSort)
        {
            List<PositionIdAndOperation> positionIdAndOperations = null;
            //if (currNode == GameTopicNode.SFK)
            //{
            //    questionTypes = new List<QuestionType> { QuestionType.PayTopic, QuestionType.ReceiptTopic };
            //}

            //用户当前应该做题的岗位id
            var currentPositionsIds = await DbContext.FreeSql.GetRepository<SxGameTopic>().Select
                .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Where(a => a.PointPositionId > 0 && a.GameLevelId == gameLevelId && a.Sort > currSort)
                .Where(e => questionTypes.Contains(e.QuestionType))
                .OrderBy(a => a.Sort).OrderBy(a => a.Id)
                .ToListAsync(a => new { a.PointPositionId, a.QuestionType });

            if (currentPositionsIds != null)
                positionIdAndOperations = currentPositionsIds.Distinct().Select(e => new PositionIdAndOperation { PositionId = e.PointPositionId, QuestionType = e.QuestionType }).ToList();
            return positionIdAndOperations;
        }

        /// <summary>
        /// 分录题 的题目节点  到该步骤，分录已做完应该计算出出纳审核、主管审核对应的岗位
        /// </summary>
        /// <param name="currTopicNode">当前题目节点</param>
        /// <param name="gameLevelId">关卡id</param>
        /// <param name="gradeId">作答记录id</param>
        /// <returns></returns>
        private async Task<List<GameLevelTopicNodeDto>> GetAccountEntryGameTopicNode(GameLevelTopicNodeDto currTopicNode, List<GameLevelQuestionBaseDto> gradeDetails, long gameLevelId, long gradeId)
        {
            //分录题做题规则：
            /*
             1.填写分录》出纳审核（有收付款时）》主管审核
             2.填写一道题后，出纳就可以开始审核，出纳审核一道题后，主管可以审核
             */
            QuestionType questionType = QuestionType.CertifMSub_AccountEntry;

            var tuple = await GetCurrentNodeHasDotDoTopic(gameLevelId, currTopicNode, gradeDetails);

            var ishasNextQuestion = tuple.Item1;
            var positionIds = new List<PositionIdAndOperation>();
            var currTopicNodes = new List<GameLevelTopicNodeDto>();
            if (ishasNextQuestion)//分录没有做完,获取所有分录题的岗位
            {
                currTopicNode.PositionIds = tuple.Item2;
                currTopicNodes.Add(currTopicNode);
            }
            //分录填写已填完 开始审核
            #region  出纳审核
            //作答记录中有已填完分录的并且需要收付款或者现金进行收付款的，并且分录题状态是草稿的时候 出纳开始审核
            var dataCashier = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .From<SxTopic>((a, b) => a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.GradeId == gradeId && b.QuestionType == questionType && a.AccountEntryStatus == AccountEntryStatus.None)
                .Where((a, b) => a.IsHasReceivePayment || b.SettlementType == 1)
                .ToListAsync((a, b) => new { b.PointPositionId, a.QuestionId, });

            if (dataCashier != null && dataCashier.Count > 0)//需要出纳审核
            {
                //var postionids = dataCashier.Select(e => e.PointPositionId).Distinct().Select(e => new PositionIdAndOperation
                //{
                //    PositionId = e,
                //    //Operation = 1,
                //    QuestionType = questionType
                //}).ToList();


                var postionids = new List<PositionIdAndOperation> { new PositionIdAndOperation { PositionId = CommonConstants.Cashier_C3D, QuestionType = questionType } };

                //positionIds.AddRange(postionids);
                currTopicNodes.Add(new GameLevelTopicNodeDto { PositionIds = postionids, QuestionTypes = new List<QuestionType> { QuestionType.CertifMSub_AccountEntry }, TopicNode = GameTopicNode.CNSH });
            }
            #endregion

            #region 主管审核 分录题最后始终有主管审核节点

            // 作答记录中填完的分录题（审核状态为出纳审核）或者（为审核状态为草稿并且不需要收付款的题目）
            var dataAM = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
               .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
               .From<SxTopic>((a, b) => a.InnerJoin(aa => aa.QuestionId == b.Id))
               .Where((a, b) => a.GradeId == gradeId && b.QuestionType == questionType)
               //.Where((a, b) => a.AccountEntryStatus == AccountEntryStatus.Cashier || (a.AccountEntryStatus == AccountEntryStatus.None && a.IsHasReceivePayment == false))
               .Where((a, b) => a.AccountEntryStatus == AccountEntryStatus.Cashier || (a.AccountEntryStatus == AccountEntryStatus.None && !a.IsHasReceivePayment && b.SettlementType == 0))
               .AnyAsync();

            if (dataAM)//需要主管审核
            {
                var aMpositionIds = GetAMPositions(questionType); //赋值主管审核对应的岗位
                currTopicNodes.Add(new GameLevelTopicNodeDto { PositionIds = aMpositionIds, QuestionTypes = new List<QuestionType> { QuestionType.CertifMSub_AccountEntry }, TopicNode = GameTopicNode.ZGSH });
            }

            //if (currTopicNodes.Count == 0)//分录题 
            //{
            //    var aMpositionIds = GetAMPositions(questionType); //赋值主管审核对应的岗位
            //    currTopicNodes.Add(new GameLevelTopicNodeDto { PositionIds = aMpositionIds, QuestionTypes = new List<QuestionType> { QuestionType.CertifMSub_AccountEntry }, TopicNode = GameTopicNode.ZGSH });

            //}
            if (currTopicNodes.Count == 0)
            {
                currTopicNodes = null;
            }
            #endregion
            return currTopicNodes;
        }

        /// <summary>
        /// 获取主管审核的岗位信息
        /// </summary>
        /// <returns></returns>
        private List<PositionIdAndOperation> GetAMPositions(QuestionType questionType)
        {
            return new List<PositionIdAndOperation> {
                    //new PositionIdAndOperation {  Operation = 2, PositionId = CommonConstants.AccountingManager_C3D ,QuestionType= questionType },
                    new PositionIdAndOperation {  
                        //Operation = 2,
                        PositionId = CommonConstants.FinanceManager_C3D ,QuestionType= questionType } };
        }

        /// <summary>
        /// 题目类型对应的题目节点
        /// </summary>
        /// <param name="questionType"></param>
        /// <returns></returns>
        internal static GameTopicNode GetGameTopicNode(QuestionType questionType)
        {
            //出纳审核 和主管审核 无法根据题目类型计算出来，通过另外的方法计算
            switch (questionType)
            {
                case QuestionType.TeticketTopic: return GameTopicNode.SP;

                case QuestionType.ReceiptTopic: return GameTopicNode.SK;

                case QuestionType.PayTopic: return GameTopicNode.FK;

                case QuestionType.CertifMSub_AccountEntry: return GameTopicNode.FL;

                case QuestionType.SettleAccounts: return GameTopicNode.JZ;

                case QuestionType.ProfitLoss: return GameTopicNode.JZSY;

                default: return GameTopicNode.Other;
            }
        }

        #endregion

        #region 作答记录

        /// <summary>
        /// 获取关卡闯关记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameUserGradeDto>> GetGameLeveLGradeList(long gameLeveLId, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "");

            var entitys = await DbContext.FreeSql.GetRepository<SxGameUserGrade>()
                    .Where(e => e.IsDelete != CommonConstants.IsDelete && e.GameLevelId == gameLeveLId && e.UserId == userId && e.Status == StudentExamStatus.End)
                    .OrderByDescending(e => e.CreateTime)
                    .ToListAsync<GameUserGradeDto>();

            return new ListResponse<GameUserGradeDto>(entitys);
        }

        /// <summary>
        /// 获取关卡闯关记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GameUserGradeDetailsDto>> GetGameLeveLGradeTopicList(long gradeId)
        {
            var result = new ResponseContext<GameUserGradeDetailsDto>(CommonConstants.ErrorCode, "");
            //GetGameGradeTopicList(gameGradeId);

            //获取题目信息，缓存10分钟
            var data = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetGameLeveLGradeTopicList_C3D}{gradeId}", () => GetGameGradeTopicList(gradeId), 10 * 60, true, false);

            result.SetSuccess(data);

            return result;
        }

        /// <summary>
        /// 根据主作答记录获取所有答题记录
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        private GameUserGradeDetailsDto GetGameGradeTopicList(long gradeId)
        {
            //获取本次作答记录总信息：得分用时等
            var result = DbContext.FreeSql.GetRepository<SxGameUserGrade>().Select
                 .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.GradeId == gradeId)
                 .First<GameUserGradeDetailsDto>();

            if (result == null)
                return null;

            //获取本关中所有题目信息
            var questionList = DbContext.FreeSql.GetRepository<SxTopic>().Select.WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                   .From<SxGameTopic, SxGameUserGrade>((a, b, c) => a.InnerJoin(aa => aa.Id == b.TopicId).InnerJoin(aa => b.GameLevelId == c.GameLevelId && c.GradeId == gradeId))
                   .Where((a, b, c) => a.ParentId == 0 || a.QuestionType == QuestionType.CertifMSub_AccountEntry)//说明：本来直接查询出父题目Id进行统计即可，但是这里单独查询出#CertifMSub_AccountEntry，是由于作答详情中综合分录题中的分录题父题目Id填写的是自己的题目Id 导致无法统计到。
                   .ToList((a, b, c) => new GameUserGradeTopicsDto()
                   {
                       QuestionId = a.Id,
                       ParentQuestionId = a.ParentId,
                       QuestionType = a.QuestionType,
                       Title = a.Title,
                       DeclarationId = a.DeclarationId,
                       ParentDeclarationId = a.ParentDeclarationId,
                       Score = a.Score,
                       Sort = a.Sort,
                   });

            #region 学生题目作答信息
            //var topicids = questionList.Select(e => e.QuestionId).ToArray();
            //获取学生作答记录
            var studentGradeDetails = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                 .WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                 .Where((a) => a.GradeId == gradeId)
                 .ToList((a) => new GradeDtailDto
                 {
                     Id = a.Id,
                     QuestionId = a.QuestionId,
                     ParentQuestionId = a.ParentQuestionId,
                     Status = a.Status,
                     AccountEntryStatus = a.AccountEntryStatus,
                     AccountEntryAuditStatus = a.AccountEntryAuditStatus,
                     Score = a.Score,
                     Sort = -1
                 });
            #endregion

            #region 用户作答信息赋值到题目信息上

            var topics = new List<GameUserGradeTopicsDto>();
            if (questionList != null && questionList.Count > 0)
            {
                List<long> certificateQuestionIds = null;//综合分录题的子分录题
                foreach (var item in questionList)
                {
                    switch (item.QuestionType)
                    {
                        case QuestionType.CertificateMainSubTopic:
                            certificateQuestionIds = questionList.Where(e => e.ParentQuestionId == item.QuestionId).Select(e => e.QuestionId).ToList();
                            topics.Add(item);
                            break;
                        case QuestionType.MainSubQuestion:
                        case QuestionType.FinancialStatements:
                        case QuestionType.ProfitLoss:
                        case QuestionType.DeclarationTpoic:
                        case QuestionType.FinancialStatementsDeclaration:
                            topics.Add(item);
                            break;
                    }
                    if (item.ParentQuestionId == 0 && studentGradeDetails != null && studentGradeDetails.Count > 0)
                    {
                        ExamGradeDetailsToQuestion(item, studentGradeDetails, certificateQuestionIds);//列表的作答信息，赋值
                    }
                }
            }

            result.Topics = topics.OrderBy(e => e.Sort).ToList();

            #endregion

            return result;
        }

        /// <summary>
        /// 作答记录列表：将作答记录赋值到题目信息中
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="questionInfo"></param>
        /// <param name="studentGradeDetails"></param>
        private static void ExamGradeDetailsToQuestion(GameUserGradeTopicsDto questionInfo, List<GradeDtailDto> studentGradeDetails, List<long> certificateQuestionIds = null)
        {
            if (questionInfo.ParentQuestionId == 0 && studentGradeDetails != null && studentGradeDetails.Count > 0)
            {
                var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == questionInfo.QuestionId);
                if (gradeDetail != null)
                {
                    switch (gradeDetail.Status)//设置答题状态
                    {
                        case AnswerResultStatus.Error:
                            questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Error;
                            break;
                        case AnswerResultStatus.Right:
                            questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Right;
                            break;
                        case AnswerResultStatus.PartRight:
                            questionInfo.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                            break;
                        default:
                            questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            break;
                    }
                    //设置得分
                    if (questionInfo.QuestionType == QuestionType.CertificateMainSubTopic)//综合分录题，答题记录中父题目没有记录得分，所以需要统计, 
                    {
                        //说明：这里单独查询出#CertifMSub_AccountEntry，是由于作答详情中综合分录题中的分录题父题目Id填写的是自己的题目Id 导致无法统计到。
                        if (certificateQuestionIds == null)
                        {
                            certificateQuestionIds = new List<long>();
                        }
                        questionInfo.AnswerScore = studentGradeDetails.Where(b => b.ParentQuestionId == questionInfo.QuestionId || certificateQuestionIds.Contains(b.QuestionId)).Sum(b => b.Score);//统计综合分录题总得分
                    }
                    else
                    {
                        questionInfo.AnswerScore = gradeDetail.Score;
                    }
                }
                else
                {
                    questionInfo.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                }
            }
        }

        /// <summary>
        /// 修改关卡作答使用时间
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UdpateGameGradeUseTime(long gameGradeId, int usedSeconds)
        {

            await DbContext.FreeSql.GetRepository<SxGameUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete)
               .UpdateDiy.Set(c => new SxGameUserGrade
               {
                   UsedSeconds = c.UsedSeconds + usedSeconds,
                   UpdateTime = DateTime.Now,
               }).Where(e => e.Id == gameGradeId).ExecuteAffrowsAsync();

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "");
        }
        #endregion

        /// <summary>
        /// 创建账套
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CreateSetBook(long gameGradeId)
        {
            await DbContext.FreeSql.GetRepository<SxGameUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxGameUserGrade
                {
                    CreateSetBookStatus = CreateSetBookStatus.HasCreated,
                    UpdateTime = DateTime.Now,
                }).Where(e => e.Id == gameGradeId).ExecuteAffrowsAsync();

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "");

        }

        /// <summary>
        /// 重置游戏
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ResetGame(long taskId, long userId)
        {
            await DbContext.FreeSql.GetRepository<SxGameUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxGameUserGrade
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                }).Where(e => e.TaskId == taskId && e.UserId == userId).ExecuteAffrowsAsync();

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 关卡作答排行榜
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<CurrentGameLevelGradeNo1>>> GameLevelGradeTops(long gameLevelId, int top)
        {
            var data = await GameLevelGradeTop(gameLevelId, top);

            return new ResponseContext<List<CurrentGameLevelGradeNo1>>(data);
        }

        /// <summary>
        /// 获取案例的任务信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetGameMonthTaskListDto>>> GetGameMonthTaskList(long id, long userId, int userType)
        {
            var selectData = await DbContext.FreeSql.Select<SxTask>().WhereCascade(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Where(a => a.CaseId == id)
                .WhereIf(userType != 6, a => a.IsActive)
                .From<SxExamPaper, SxGameUserTrain>((a, b, c) => a.LeftJoin(aa => aa.Id == b.MonthTaskId && b.UserId == userId).LeftJoin(aa => aa.Id == c.TaskId && c.UserId == userId))
                .OrderBy((a, b, c) => a.IssueDate)
                .OrderByDescending((a, b, c) => a.CreateTime)
                .ToListAsync((a, b, c) => new GetGameMonthTaskListDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    Target = a.Target,
                    ExamId = b.Id,
                    TrainFinishId = c.Id,
                    CaseId = a.CaseId
                });

            if (selectData != null && selectData.Count > 0)
            {
                var taskids = selectData.Select(e => e.Id).ToArray();
                //获取默认用户的试卷Id
                var rSxExamPaper = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetGameExamPapers_C3D}{id}",
                   () => DbContext.FreeSql.Select<SxExamPaper>()
                          .Where(x => x.CaseId == id && taskids.Contains(x.MonthTaskId) && x.UserId == CommonConstants.DefaultUserId && x.IsDelete == CommonConstants.IsNotDelete)
                          .OrderByDescending(x => x.CreateTime).ToList(x => new { ExamId = x.Id, TaskId = x.MonthTaskId }), 10 * 60, false, false);

                selectData.ForEach(e =>
                {
                    e.IsTrainFinish = e.TrainFinishId > 0;

                    if (e.ExamId > 0)
                        e.IsPaid = true;
                    else
                    {
                        e.IsPaid = false;
                        e.ExamId = rSxExamPaper.Where(f => f.TaskId == e.Id).Select(f => f.ExamId).FirstOrDefault();
                    }
                });
            }

            return new ResponseContext<List<GetGameMonthTaskListDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }

        #endregion

        /// <summary>
        /// 获取题目附件列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<QuestionFile>>> GetTopicFiles(long topicId)
        {
            var files = await GetTopicFilesByTopicId(topicId);

            return new ResponseContext<List<QuestionFile>>(files);
        }

        /// <summary>
        /// 获取题目附件列表
        /// </summary>
        /// <returns></returns>
        private async Task<List<QuestionFile>> GetTopicFilesByTopicId(long topicId)
        {
            var topicFileIds = await DbContext.FreeSql.GetRepository<SxTopic>().Where(a => a.Id == topicId).Select(e => e.TopicFileIds).FirstAsync();

            return GetTopicFilesByTopicId(topicFileIds, topicId);


        }
        /// <summary>
        /// 获取题目附件列表
        /// </summary>
        /// <returns></returns>
        private List<QuestionFile> GetTopicFilesByTopicId(string topicFileIds, long questionId)
        {
            if (string.IsNullOrWhiteSpace(topicFileIds))
                return null;

            var tfids = topicFileIds.Split(",");
            long[] ids = tfids.Select(e => long.Parse(e)).ToArray();

            var files = DbContext.FreeSql.GetRepository<SxTopicFile>().Where(a => ids.Contains(a.Id))
                   .OrderBy(a => a.CoverBack == TopicFileCoverBack.Cover ? -1 : a.Sort)
                   .Select(a => new QuestionFile() { Id = a.Id, Name = a.Name, Url = a.Url, Sort = a.Sort, Type = a.Type, QuestionId = a.TopicId })
            .ToList($"{CommonConstants.Cache_GetTopicFilesByTopicId_C}{questionId}", true, 10 * 60);


            return files;
        }

    }
}