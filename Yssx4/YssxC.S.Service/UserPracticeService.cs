﻿using NPOI.SS.Formula.Functions;
using Renci.SshNet.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.AnswerCompare;
using Yssx.Framework.AnswerCompare.EntitysV2;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using Yssx.Framework.Utils;
using Yssx.Redis;
using Yssx.Repository.Extensions;
using YssxC.S.Dto;
using YssxC.S.Dto.ExamPaper;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Exam;
using YssxC.S.Pocos.Subject;

namespace Yssx.S.ServiceImpl.Examination
{
    /// <summary>
    /// 学生实习相关服务 
    /// </summary>
    public class UserPracticeService : IUserPracticeService
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public UserPracticeService()
        {

        }

        #region 查询(填写)分录
        /// <summary>
        /// 填写分录
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAccountingEntryTopicList(AccountingEntryTopicRequest model)
        {
            var result = new ResponseContext<List<AccountingEntryTopicDto>>();
            if (model.PositionId <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "岗位ID必须大于0";
                return result;
            }
            var grade = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }


            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }
            var endDate = model.EndDate.AddDays(1);
            //获取所有题目Id集合
            var questionInfoList = await DbContext.FreeSql.GetRepository<SxAuditedBill>().Select
                .From<SxTopic>((a, b) =>
                a.LeftJoin(aa => aa.ParentQuestionId == b.ParentId))
                .Where((a, b) => a.BusinessDate >= model.StartDate && a.BusinessDate < endDate && b.PositionId == model.PositionId && a.ExamId == exam.Id && a.GradeId == model.GradeId && a.IsDelete == CommonConstants.IsNotDelete)
                // .Where((a, b) => c.GradeId==model.GradeId && c.IsDelete==CommonConstants.IsNotDelete)
                .Where((a, b) => (!a.IsHasReceivePayment || (a.IsHasReceivePayment && a.IsDoneReceivePayment)) && b.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .OrderBy((a, b) => a.CreateTime)
                .ToListAsync((a, b) => new AccountingEntryTopicDto
                {
                    BusinessDate = a.BusinessDate,
                    QuestionId = b.Id,
                    TopicFileIds = b.TopicFileIds,
                    Title = b.Title,
                    TeacherHint = b.TeacherHint
                });
            var questionIdList = questionInfoList.Select(s => s.QuestionId).ToList();
            var gradeDetailIdList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                .Where(s => questionIdList.Contains(s.QuestionId) && s.GradeId == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(a => new { a.QuestionId ,a.AccountEntryStatus});
            questionInfoList.ForEach(s =>
            {
                var files = DbContext.FreeSql.GetRepository<SxTopicFile>().Where(a => s.TopicFileIds.Contains(a.Id.ToString()))
                    .OrderBy(a => a.CoverBack == TopicFileCoverBack.Cover ? -1 : a.Sort)
                    .Select(a => new QuestionFile() { Id = a.Id, Name = a.Name, Url = a.Url, Sort = a.Sort, Type = a.Type, QuestionId = a.TopicId })
                    .ToList($"{CommonConstants.Cache_GetTopicFilesByTopicId_C}{s.QuestionId}", true, 10 * 60);
                var gradeDetail = gradeDetailIdList.Where(p => p.QuestionId == s.QuestionId).FirstOrDefault();
                s.IsAnswered = gradeDetail!=null;
                s.AccountEntryStatus = gradeDetail != null ? gradeDetail.AccountEntryStatus: AccountEntryStatus.None;
                s.QuestionFiles = files;
            });

            return new ResponseContext<List<AccountingEntryTopicDto>>(questionInfoList);
        }
        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="gradeId"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        public async Task<PageResponse<AccountingEntryAnsweredDto>> GetAccountingEntryAnsweredList(AccountingEntryTopicRequest model, int auditType = -1)
        {
            //查询业务日期在时间段内的分录题和结转损溢题
            var result = new PageResponse<AccountingEntryAnsweredDto>();
            if (model.PositionId <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "岗位ID必须大于0";
                return result;
            }
            var grade = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }


            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }
            int certificateNo = -1;
            int.TryParse(model.KeyWords, out certificateNo);
            var totalCount = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .From<SxTopic>((a, b) =>
                 a.LeftJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.PositionId == model.PositionId && a.ExamId == exam.Id && a.GradeId == model.GradeId)//a.BusinessDate >= model.StartDate && a.BusinessDate < endDate &&
                .WhereIf(auditType == 1, (a, b) => a.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                .WhereIf(auditType == 0, (a, b) => a.AccountEntryStatus != AccountEntryStatus.AccountingManager)
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo == 0, (a, b) => model.KeyWords.Contains(b.Title))
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo > 0, (a, b) => a.CertificateNo == certificateNo)
                .Where((a, b) => (b.QuestionType == QuestionType.CertifMSub_AccountEntry || b.QuestionType == QuestionType.ProfitLoss))
                .CountAsync();
            var data = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .From<SxTopic, SxExamCertificateDataRecord>((a, b, c) =>
                 a.LeftJoin(aa => aa.QuestionId == b.Id)
                 .LeftJoin(aa => aa.QuestionId == c.QuestionId))
                .Where((a, b, c) => a.PositionId == model.PositionId && a.ExamId == exam.Id && a.GradeId == model.GradeId)//a.BusinessDate >= model.StartDate && a.BusinessDate < endDate &&
                .WhereIf(auditType == 1, (a, b, c) => a.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                .WhereIf(auditType == 0, (a, b, c) => a.AccountEntryStatus != AccountEntryStatus.AccountingManager)
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo == 0, (a, b, c) => model.KeyWords.Contains(b.Title))
                .WhereIf(!string.IsNullOrEmpty(model.KeyWords) && certificateNo > 0, (a, b, c) => a.CertificateNo == certificateNo)
                .Where((a, b, c) => (b.QuestionType == QuestionType.CertifMSub_AccountEntry || b.QuestionType == QuestionType.ProfitLoss))
                .Where((a, b, c) => c.GradeId == model.GradeId && c.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { a.CreateTime, a.QuestionId, a.BusinessDate, a.CertificateWord, a.CertificateNo, b.Title, a.AccountEntryStatus })
                .OrderBy(a => a.Key.CreateTime)
                .Page(model.PageIndex, model.PageSize)
                .ToListAsync(a => new AccountingEntryAnsweredDto
                {
                    QuestionId = a.Key.QuestionId,
                    BusinessDate = a.Key.BusinessDate,
                    CertificateWord = a.Key.CertificateWord,
                    CertificateNo = a.Key.CertificateNo,
                    Title = a.Key.Title,
                    AccountEntryStatus = a.Key.AccountEntryStatus,
                    //SummaryInfo = a.Key.SummaryInfos,
                    TotalBorrowAmount = Convert.ToDecimal("sum(c.BorrowAmount)"),
                    TotalCreditorAmount = Convert.ToDecimal("sum(c.CreditorAmount)")
                });
            #region 查询题目是否被标记
            //题目Ids
            var queIds = data.Select(x => x.QuestionId).ToList();
            var markList = await DbContext.FreeSql.GetRepository<SxTopicMark>().Where(x => queIds.Contains(x.QuestionId) && x.GradeId == model.GradeId).ToListAsync();
            var summaryInfoList = await DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>()
                .Where(s => queIds.Contains(s.QuestionId) && s.GradeId == model.GradeId).Select(s => new { s.QuestionId, s.SummaryInfo }).ToListAsync();
            data.ForEach(x =>
            {
                if (markList.Where(a => a.QuestionId == x.QuestionId).Count() > 0)
                    x.IsMark = true;
                x.SummaryInfo = string.Join(",", summaryInfoList.Where(a => a.QuestionId == x.QuestionId && a.SummaryInfo != "").Select(a => a.SummaryInfo).ToList());
            });
            #endregion
            result.Data = data;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = totalCount;
            result.PageSize = model.PageSize;
            result.PageIndex = model.PageIndex;
            return result;
        }
        /// <summary>
        /// 出纳(会计主管)审核 auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAuditAccountingEntryTopicList(AccountingEntryTopicRequest model, int auditType)
        {
            var result = new ResponseContext<List<AccountingEntryTopicDto>>();

            if (auditType != 0 && auditType != 1)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "审核类型参数有误！";
                return result;
            }
            if (auditType == 0 && model.PositionId != CommonConstants.Cashier_C)
            {
                result.Code = CommonConstants.SuccessCode;
                result.Msg = "";
                return result;
            }
            if (auditType == 1 && model.PositionId != CommonConstants.AccountingManager_C && model.PositionId != CommonConstants.FinanceManager_C)
            {
                result.Code = CommonConstants.SuccessCode;
                result.Msg = "";
                return result;
            }
            var grade = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }


            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }
            var endDate = model.EndDate.AddDays(1);
            var data = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Select
                .From<SxTopic>((a, b) => a.LeftJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.BusinessDate >= model.StartDate && a.BusinessDate < endDate && a.ExamId == exam.Id && a.GradeId == model.GradeId)
                .WhereIf(auditType == 0, (a, b) => b.QuestionType == QuestionType.CertifMSub_AccountEntry && (a.IsHasReceivePayment || b.SettlementType == 1))
                .WhereIf(auditType == 1, (a, b) => (a.AccountEntryStatus == AccountEntryStatus.Cashier || a.AccountEntryStatus == AccountEntryStatus.AccountingManager || !a.IsHasReceivePayment))
                .WhereIf(auditType == 1, (a, b) => (b.QuestionType == QuestionType.CertifMSub_AccountEntry))
                .OrderBy((a, b) => a.CreateTime)
                .ToListAsync((a, b) => new AccountingEntryTopicDto
                {
                    BusinessDate = a.BusinessDate,
                    QuestionId = b.Id,
                    TopicFileIds = b.TopicFileIds,
                    Title = b.Title,
                    TeacherHint = b.TeacherHint,
                    AccountEntryStatus = a.AccountEntryStatus
                });
            data.ForEach(s =>
            {
                var files = DbContext.FreeSql.GetRepository<SxTopicFile>().Where(a => s.TopicFileIds.Contains(a.Id.ToString()))
                    .OrderBy(a => a.CoverBack == TopicFileCoverBack.Cover ? -1 : a.Sort)
                    .Select(a => new QuestionFile() { Id = a.Id, Name = a.Name, Url = a.Url, Sort = a.Sort, Type = a.Type, QuestionId = a.TopicId })
                    .ToList($"{CommonConstants.Cache_GetTopicFilesByTopicId_C}{s.QuestionId}", true, 10 * 60);

                s.QuestionFiles = files;
                if (auditType == 0 && s.AccountEntryStatus == AccountEntryStatus.Cashier)
                {
                    s.IsAnswered = true;
                }
                if (auditType == 1 && s.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                {
                    s.IsAnswered = true;
                }
            });

            return new ResponseContext<List<AccountingEntryTopicDto>>(data);
        }

        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="caseId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamProfitLossTopicDto>> GetProfitLossTopicInfo(long gradeId, long caseId, long taskId)
        {
            var result = new ResponseContext<ExamProfitLossTopicDto>();
            //查找月度任务下结转损溢题
            var topicId = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(t => t.TaskId == taskId && t.QuestionType == QuestionType.ProfitLoss && t.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(t => t.Id);
            var gradeDetail = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(a => a.GradeId == gradeId && a.QuestionId == topicId)
                        .FirstAsync(a => new { a.Id, a.QuestionId });
            //查找公司损溢科目
            var profitLossSubjects = await DbContext.FreeSql.GetRepository<SxSubject>()
                .Where(s => (s.SubjectType == 6 || s.SubjectName == CommonConstants.ProfitSubjectName) && s.CaseId == caseId && s.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            var sIds = profitLossSubjects.Select(s => s.Id).ToList();
            //TODO:去除结转损溢本身的科目
            var examCertificateDataRecords = await DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Select
                .From<SxSubject>((a, b) => a.LeftJoin(aa => aa.SubjectId == b.Id))
                .Where((a, b) => sIds.Contains(a.SubjectId) && a.GradeId == gradeId && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    a.SubjectId,
                    a.BorrowAmount,
                    a.CreditorAmount,
                    InitialBorrowAmount = b.BorrowAmount,
                    InitialCreditorAmount = b.CreditorAmount,
                    b.ProfitLossSubjectType
                });
            var mergeProfitLossTopicSubjectInfos = new List<ProfitLossTopicSubjectInfo>();//合并结转
            var groupList = examCertificateDataRecords.GroupBy(s => new { s.SubjectId, s.ProfitLossSubjectType, s.InitialBorrowAmount, s.InitialCreditorAmount }).
                        Select(s => new ProfitLossTopicSubjectInfo
                        {
                            SubjectId = s.Key.SubjectId,
                            BorrowAmount = s.Sum(c => c.BorrowAmount) + s.Key.InitialBorrowAmount,
                            CreditorAmount = s.Sum(c => c.CreditorAmount) + s.Key.InitialCreditorAmount,
                            Amount = (s.Sum(c => c.BorrowAmount) + s.Key.InitialBorrowAmount) - (s.Sum(c => c.CreditorAmount) + s.Key.InitialCreditorAmount),
                            ProfitLossSubjectType = s.Key.ProfitLossSubjectType
                        }).ToList();
            var profitSubject = profitLossSubjects.Where(s => s.SubjectName == CommonConstants.ProfitSubjectName).FirstOrDefault();

            var totalBorrowAmount = groupList.Sum(s => s.BorrowAmount);
            var totalCreditorAmount = groupList.Sum(s => s.CreditorAmount);

            var totalAmount = totalBorrowAmount > totalCreditorAmount ? totalBorrowAmount : totalCreditorAmount;
            //不分开结转时本年利润金额 如果是正数则在借方 负数则在贷方
            var profitAmount = totalBorrowAmount - totalCreditorAmount;

            //不分开结转：本年利润科目信息
            var profit = new ProfitLossTopicSubjectInfo();
            profit.SubjectId = profitSubject!=null?profitSubject.Id:0;
            profit.Amount = profitAmount;

            var partProfitLossTopicSubjectInfos = groupList.GroupBy(x => x.ProfitLossSubjectType).Select(x => x.ToList()).ToList();
            partProfitLossTopicSubjectInfos.ForEach(p =>
            {
                var partTotalBorrowAmount = groupList.Sum(s => s.BorrowAmount);
                var partTotalCreditorAmount = groupList.Sum(s => s.CreditorAmount);

                //var partTotalAmount = partTotalBorrowAmount > partTotalCreditorAmount ? partTotalBorrowAmount : partTotalCreditorAmount;
                //不分开结转时本年利润金额 如果是正数则在借方 负数则在贷方
                var partProfitAmount = partTotalBorrowAmount - partTotalCreditorAmount;

                //不分开结转：本年利润科目信息
                var partProfit = new ProfitLossTopicSubjectInfo();
                profit.SubjectId = profitSubject != null ? profitSubject.Id : 0;
                profit.Amount = profitAmount;
                p.Add(partProfit);
            });
            mergeProfitLossTopicSubjectInfos = groupList;

            mergeProfitLossTopicSubjectInfos.Add(profit);
            var examProfitLossTopicDto = new ExamProfitLossTopicDto();
            examProfitLossTopicDto.QuestionId = topicId;
            examProfitLossTopicDto.TotalAmount = totalAmount;
            examProfitLossTopicDto.IsCarryForward = gradeDetail != null;
            examProfitLossTopicDto.MergeProfitLossTopicSubjectInfos = mergeProfitLossTopicSubjectInfos;
            examProfitLossTopicDto.PartProfitLossTopicSubjectInfos = partProfitLossTopicSubjectInfos;
            result.Data = examProfitLossTopicDto;
            return result;
        }
        /// <summary>
        /// 获取月度任务题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskId"></param>
        /// <param name="questionType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetOtherTopicList(AccountingEntryTopicRequest model, long taskId, long declarationId, QuestionType questionType)
        {
            var result = new ResponseContext<List<ExamOtherQuestionInfo>>();
            if (taskId <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "taskId必须大于0";
                return result;
            }
            var endDate = model.EndDate.AddDays(1);

            var topicIdList = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(t => t.QuestionType == questionType && t.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(questionType == QuestionType.DeclarationTpoic, t => t.DeclarationId == declarationId)
                .WhereIf(questionType != QuestionType.DeclarationTpoic, t => t.TaskId == taskId && t.PositionId == model.PositionId)
                .ToListAsync(t => new ExamOtherQuestionInfo
                {
                    QuestionId = t.Id,
                    PositionId = t.PositionId,
                    Title = t.Title,
                    TeacherHint = t.TeacherHint,
                    DeclarationDateStart = t.DeclarationDateStart,
                    DeclarationDateEnd = t.DeclarationDateEnd,
                    DeclarationLimitDate = t.DeclarationLimitDate
                });
            result.Data = topicIdList;
            return result;
        }
        #endregion

        #region 预览

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfoView(long gradeId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperBasicInfo>();
            var user = userTicket;

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "gradeId不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null || grade.Id == 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"答题记录不存在 ";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            #endregion

            #region  案例信息

            var caseInfo = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetCaseById_C}{exam.CaseId}",
                () => DbContext.FreeSql.GetRepository<SxCase>().Select.Where(s => s.Id == exam.CaseId).First(), 10 * 60, true, false);

            var SxIndustry = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetIndustryById_C}{caseInfo.IndustryId}",
                () => DbContext.FreeSql.GetRepository<SxIndustry>().Select.Where(s => s.Id == caseInfo.IndustryId).First(), 10 * 60, true, false);

            var examCase = new ExamCaseInfo
            {
                CaseName = caseInfo.Name,
                IndustryName = SxIndustry.Name,
            };

            #endregion

            #region 考试 题目 作答记录等基本信息

            //获取本次答卷获得积分的记录
            var userIntegral = await DbContext.FreeSql.GetGuidRepository<SxExamIntegral>(x => x.IsDelete == CommonConstants.IsNotDelete)
              .Where(e => e.UserId == grade.UserId && e.TaskId == exam.MonthTaskId && e.GradeId == gradeId)
              .FirstAsync();

            var tuple = await GetExamQuestionsByCaseId(exam, grade); //获取题目信息和作答信息

            List<BusinessQuestionInfo> businessTopics = tuple.Item1;//业务题
            List<ExamRecordQuestionInfo> Financials = tuple.Item2;//报表题
            List<ExamRecordQuestionInfo> declarationTpoics = tuple.Item3;//纳税申报题

            result.Data = new ExamPaperBasicInfo()
            {
                ExamId = exam.Id,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,
                StudentExamStatus = grade.Status,
                LeftSeconds = (long)grade.LeftSeconds,
                UsedSeconds = (long)grade.UsedSeconds,
                GradeId = gradeId,
                BusinessTopics = businessTopics,
                DeclarationTpoics = declarationTpoics,
                FinancialTopics = Financials,
                CaseInfo = examCase,
                StudentGradeInfo = new StudentGradeInfo()
                {
                    GradeId = grade.Id,
                    Score = grade.Score,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score >= exam.PassScore,
                    Status = grade.Status,
                    Integral = userIntegral.Integral,
                    TotalIntegral = userIntegral.TotalIntegral,
                },
            };
            #endregion

            return result;
        }

        /// <summary>
        /// 获取题目信息和作答信息
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private async Task<Tuple<List<BusinessQuestionInfo>, List<ExamRecordQuestionInfo>, List<ExamRecordQuestionInfo>>> GetExamQuestionsByCaseId(SxExamPaper exam, SxExamUserGrade grade)
        {
            //获取题目信息，缓存10分钟
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicByTaskId_C}{exam.MonthTaskId}", () => DbContext.FreeSql.GetRepository<SxTopic>().Select
                 //.From<SxPositionManage>((a, b) => a.LeftJoin(aa => aa.PositionId == b.Id))
                 .Where(a => a.TaskId == exam.MonthTaskId && a.IsDelete == CommonConstants.IsNotDelete && (a.ParentId == 0 || a.QuestionType == QuestionType.CertifMSub_AccountEntry))//说明：这里单独查询出#CertifMSub_AccountEntry，是由于作答详情中综合分录题中的分录题父题目Id填写的是自己的题目Id 导致无法统计到。
                 .ToList(a => new ExamRecordQuestionInfo()
                 {
                     QuestionId = a.Id,
                     ParentQuestionId = a.ParentId,
                     QuestionType = a.QuestionType,
                     //PositionId = a.PositionId,
                     //PositionName = b.Name,
                     Title = a.Title,
                     DeclarationId = a.DeclarationId,
                     ParentDeclarationId = a.ParentDeclarationId,
                     Score = a.Score,
                     Sort = a.Sort,
                 }), 10 * 60, true, false);

            //学生题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                //获取学生作答记录
                studentGradeDetails = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                    .Where(a => a.GradeId == grade.Id)
                    .ToListAsync(a => new GradeDtailDto
                    {
                        Id = a.Id,
                        QuestionId = a.QuestionId,
                        ParentQuestionId = a.ParentQuestionId,
                        Status = a.Status,
                        AccountEntryStatus = a.AccountEntryStatus,
                        AccountEntryAuditStatus = a.AccountEntryAuditStatus,
                        Score = a.Score,
                        Sort = -1
                    });
            }

            List<BusinessQuestionInfo> businessTopics = null;//业务题
            List<ExamRecordQuestionInfo> Financials = null;//报表题
            List<ExamRecordQuestionInfo> declarationTpoics = null;//纳税申报题

            if (questionList != null && questionList.Count > 0)
            {
                List<ExamRecordQuestionInfo> CertificateMainSubTopics = new List<ExamRecordQuestionInfo>();
                Financials = new List<ExamRecordQuestionInfo>();
                declarationTpoics = new List<ExamRecordQuestionInfo>();
                List<long> certificateQuestionIds = null;//综合分录题的子分录题

                foreach (var item in questionList)
                {
                    switch (item.QuestionType)
                    {
                        //case QuestionType.SingleChoice:
                        //case QuestionType.MultiChoice:
                        //case QuestionType.Judge:
                        //case QuestionType.FillGrid:
                        //case QuestionType.AccountEntry:
                        //case QuestionType.FillBlank:
                        //case QuestionType.FillGraphGrid:
                        //case QuestionType.SettleAccounts:
                        //case QuestionType.GridFillBank:
                        case QuestionType.CertificateMainSubTopic:
                            certificateQuestionIds = questionList.Where(e => e.ParentQuestionId == item.QuestionId).Select(e => e.QuestionId).ToList();
                            CertificateMainSubTopics.Add(item);
                            break;
                        //case QuestionType.TeticketTopic:
                        //case QuestionType.PayTopic:
                        //case QuestionType.ReceiptTopic:
                        //case QuestionType.CertifMSub_AccountEntry:
                        //    mainSubQuestion_subs.Add(item);
                        //    break;
                        case QuestionType.MainSubQuestion:
                        case QuestionType.FinancialStatements:
                        case QuestionType.ProfitLoss:
                            Financials.Add(item);
                            break;
                        case QuestionType.DeclarationTpoic:
                        case QuestionType.FinancialStatementsDeclaration:
                            declarationTpoics.Add(item);
                            break;
                        default:
                            break;
                    }
                    if (item.ParentQuestionId == 0 && studentGradeDetails != null && studentGradeDetails.Count > 0)
                    {
                        ExamGradeDetailsToQuestion(exam, grade, item, studentGradeDetails, certificateQuestionIds);//列表的作答信息，赋值
                    }
                }

                declarationTpoics = declarationTpoics.OrderBy(e => e.QuestionType).ThenBy(e => e.Sort).ToList();

                Financials = Financials.OrderBy(e => e.QuestionType).ThenBy(e => e.Sort).ToList();

                //构建综合分录题
                if (CertificateMainSubTopics != null && CertificateMainSubTopics.Count > 0)
                {
                    businessTopics = businessTopics = new List<BusinessQuestionInfo>();
                    foreach (var item in CertificateMainSubTopics.GroupBy(e => e.BusinessDate).OrderBy(e => e.Key))
                    {
                        var questionInfos = item
                        .OrderBy(b => b.Sort)
                        .ToList();

                        businessTopics.Add(new BusinessQuestionInfo
                        {
                            BusinessDate = item.Key,
                            QuestionInfos = questionInfos
                        });
                    }
                }
            }

            return new Tuple<List<BusinessQuestionInfo>, List<ExamRecordQuestionInfo>, List<ExamRecordQuestionInfo>>(businessTopics, Financials, declarationTpoics);
        }
        /// <summary>
        /// 作答记录列表：将作答记录赋值到题目信息中
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="questionInfo"></param>
        /// <param name="studentGradeDetails"></param>
        private static void ExamGradeDetailsToQuestion(SxExamPaper exam, SxExamUserGrade grade, ExamRecordQuestionInfo questionInfo, List<GradeDtailDto> studentGradeDetails, List<long> certificateQuestionIds = null)
        {
            if (questionInfo.ParentQuestionId == 0 && studentGradeDetails != null && studentGradeDetails.Count > 0)
            {
                var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == questionInfo.QuestionId);
                if (gradeDetail != null)
                {
                    //if (a.QuestionType == QuestionType.MainSubQuestion)
                    //{
                    //    var details = studentGradeDetails.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionId != a.QuestionId);

                    //    if (details != null && details.Any())
                    //    {
                    //        var accountQuestions = questionList.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionType == QuestionType.AccountEntry).ToList();

                    //        if (accountQuestions != null && accountQuestions.Any())
                    //        {
                    //            accountQuestions.ForEach(s =>
                    //            {
                    //                var detail = details.FirstOrDefault(d => d.QuestionId == s.QuestionId);
                    //                if (detail != null && detail.Id > 0)
                    //                {
                    //                    detail.Sort = s.Sort;
                    //                }
                    //            });
                    //            a.AccountEntryStatusDic = details.Where(s => s.Sort >= 0).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                    //        }
                    //    }
                    //}
                    //else if (a.QuestionType == QuestionType.AccountEntry)
                    //{
                    //    a.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>()
                    //    {
                    //        new Tuple<long, AccountEntryStatus,AccountEntryAuditStatus,int>(gradeDetail.QuestionId,gradeDetail.AccountEntryStatus,gradeDetail.AccountEntryAuditStatus,a.Sort)
                    //    };
                    //}

                    //设置答题状态
                    if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End)
                    {
                        switch (gradeDetail.Status)
                        {
                            case AnswerResultStatus.Error:
                                questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                break;
                            case AnswerResultStatus.Right:
                                questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                break;
                            case AnswerResultStatus.PartRight:
                                questionInfo.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                break;
                            default:
                                questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                break;
                        }
                        //设置得分
                        if (questionInfo.QuestionType == QuestionType.CertificateMainSubTopic)//综合分录题，答题记录中父题目没有记录得分，所以需要统计, 
                        {
                            //说明：这里单独查询出#CertifMSub_AccountEntry，是由于作答详情中综合分录题中的分录题父题目Id填写的是自己的题目Id 导致无法统计到。
                            if (certificateQuestionIds == null)
                            {
                                certificateQuestionIds = new List<long>();
                            }
                            questionInfo.AnswerScore = studentGradeDetails.Where(b => b.ParentQuestionId == questionInfo.QuestionId || certificateQuestionIds.Contains(b.QuestionId)).Sum(b => b.Score);//统计综合分录题总得分
                        }
                        else
                        {
                            questionInfo.AnswerScore = gradeDetail.Score;
                        }
                    }
                    else
                    {
                        questionInfo.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                    }
                }
                else
                {
                    questionInfo.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                }
            }
        }



        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoView(long gradeId, long questionId, long userId)
        {
            var result = new ResponseContext<ExamQuestionInfo>(CommonConstants.BadRequest, $"");

            //var user = userTicket;

            #region 校验

            if (gradeId == 0 || questionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "所有参数都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                .Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>()
                .Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetTopicById_C}{questionId}", true, 10 * 60, true);

            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"题目不存在";
                return result;
            }

            if (exam.CaseId != question.CaseId)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"题目和试卷信息不匹配";
                return result;
            }

            #endregion

            result = await GetExamQuestionInfo(exam, grade, question);

            return result;
        }
        #endregion

        #region 答题相关接口

        #region 获取题目信息

        /// <summary>
        /// 获取题目信息--接口方法（TODO：增加结转损溢题获取）
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId, long userId)
        {
            var result = new ResponseContext<ExamQuestionInfo>(CommonConstants.BadRequest, $"");
            try
            {
                //var user = userTicket;

                #region 校验

                if (gradeId == 0 || questionId == 0)
                {
                    result.Code = CommonConstants.BadRequest;
                    result.Msg = "所有参数都不能为0";
                    return result;
                }

                var grade = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).First();

                if (grade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = "没找到答题记录";
                    return result;
                }

                if (grade.Status == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"考试已结束！";
                    return result;
                }

                var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);

                if (exam == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"考试信息不存在";
                    return result;
                }

                var question = await DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetTopicById_C}{questionId}", true, 10 * 60, true);

                if (question == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"题目不存在";
                    return result;
                }

                if (exam.CaseId != question.CaseId)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"题目和试卷信息不匹配";
                    return result;
                }

                #endregion

                if (exam != null)
                {
                    switch (exam.ExamType)
                    {
                        case ExamType.PracticeTest:

                            return await GetPracticeQuestionInfo(exam, grade, question, userId);

                        default:

                            throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = $"查看题目时系统异常{ex.Message}";
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(JsonHelper.SerializeObject(ex));
            }

            return result;
        }

        /// <summary>
        /// 获取题目基本信息--自主练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfo>> GetPracticeQuestionInfo(SxExamPaper exam, SxExamUserGrade grade, SxTopic question, long userId)
        {
            return await GetQuestionInfoBase(exam, grade, question, userId);
        }

        /// <summary>
        /// 获取答题记录详情，题目信息和作答信息 (专用方法)
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfo>> GetExamQuestionInfo(SxExamPaper exam, SxExamUserGrade grade, SxTopic question)
        {
            var result = new ResponseContext<ExamQuestionInfo>();

            var examId = exam?.Id;
            var gradeId = grade?.Id;
            var questionId = question.Id;
            // TODO MapTo配置文件
            var questionDTO = question.MapTo<ExamQuestionInfo>();
            questionDTO.QuestionId = questionId;
            var msg = string.IsNullOrEmpty(questionDTO.Content) ? "题目内容content为空" : "";

            #region 加载题目基本信息
            //加载题目信息
            if (questionDTO.QuestionType == QuestionType.SingleChoice
                || questionDTO.QuestionType == QuestionType.MultiChoice
                || questionDTO.QuestionType == QuestionType.Judge)
            {
                //添加选项信息
                var options = await DbContext.FreeSql.GetRepository<SxAnswerOption>()
                    .Where(a => a.TopicId == questionId).OrderBy(a => a.Sort)
                    .ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId_C}{questionId}", true, 10 * 60);

                var optionList = new List<ChoiceOption>();
                options.ForEach(a =>
                {
                    optionList.Add(new ChoiceOption()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Text = a.AnswerOption,
                        AttatchImgUrl = a.AnswerFileUrl
                    });
                });
                questionDTO.Options = optionList;
            }
            else
            {
                //复杂题型
                //1.添加附件信息
                var fileList = new List<QuestionFile>();
                var fileIds = question.TopicFileIds;
                if (!string.IsNullOrEmpty(fileIds))
                {
                    var fileIdArr = fileIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (fileIdArr.Any())
                    {
                        var files = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(a => fileIdArr.Contains(a.Id))
                            .OrderBy(a => a.CoverBack == TopicFileCoverBack.Cover ? -1 : a.Sort)
                            .Select(a => new QuestionFile()
                            {
                                Name = a.Name,
                                Url = a.Url,
                                Sort = a.Sort,
                                Type = a.Type,
                                QuestionId = a.TopicId
                            })
                            .ToListAsync($"{CommonConstants.Cache_GetTopicFilesByTopicId_C}{questionId}", true, 10 * 60);

                        questionDTO.QuestionFile = files;
                    }
                }
                //2.添加子题目 获取子题目信息、作答记录
                if (questionDTO.QuestionType == QuestionType.MainSubQuestion || questionDTO.QuestionType == QuestionType.CertificateMainSubTopic)
                {
                    var subQuestions = DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.ParentId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId_C}{questionId}", true, 10 * 60, true, false).Result
                        .Select(s =>
                        new ExamQuestionInfo()
                        {
                            QuestionId = s.Id,
                            QuestionType = s.QuestionType,
                            QuestionContentType = s.QuestionContentType,
                            CalculationType = s.CalculationType,
                            Hint = s.Hint,
                            Content = s.Content,
                            TopicContent = s.TopicContent,
                            FullContent = s.FullContent,
                            AnswerValue = s.AnswerValue,
                            Score = s.Score,
                            Sort = s.Sort,
                            IsCopy = s.IsCopy,
                            IsCopyBigData = s.IsCopyBigData,
                            IsDisorder = s.IsDisorder,
                            DisableAMScore = s.DisableAMScore,
                            DisableCashierScore = s.DisableCashierScore,
                        }).ToList();
                    //子题目 作答记录
                    var detailList = new List<SxExamUserGradeDetail>();
                    if (gradeId > 0)
                    {
                        var subquestionIds = subQuestions.Select(a => a.QuestionId).ToList();
                        detailList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                            .Where(s => s.GradeId == gradeId && subquestionIds.Contains(s.QuestionId))
                            .ToListAsync(a => new SxExamUserGradeDetail()
                            {
                                QuestionId = a.QuestionId,
                                Status = a.Status,
                                Answer = a.Answer,
                                AnswerCompareInfo = a.AnswerCompareInfo,
                                AccountEntryStatus = a.AccountEntryStatus,
                                SummaryInfos = a.SummaryInfos,
                                CertificateNo = a.CertificateNo,
                                AssistNames = a.AssistNames,
                                //ProfitLossUserAnswerValues = a.ProfitLossUserAnswerValues,
                                Score = a.Score,
                            });
                    }
                    //子题目 赋值作答记录
                    if (subQuestions.Any() && detailList.Any())
                    {
                        subQuestions.ForEach(s =>
                        {
                            var detail = detailList.FirstOrDefault(a => a.QuestionId == s.QuestionId);

                            if (s.QuestionType == QuestionType.CertifMSub_AccountEntry)//分录题
                            {
                                s.CertificateTopic = DbContext.FreeSql.GetRepository<SxCertificateTopic>().Select
                                .Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                                .Select(a => new CertificateTopicView())
                                .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId_C}{s.QuestionId}", true, 10 * 60);

                                s.StandardSummaryInfos = s.CertificateTopic?.SummaryInfos;
                                var refusalRecords = DbContext.FreeSql.GetRepository<SxRefusalRecord>()
                                    .Where(a => a.GradeId == gradeId && s.QuestionId == questionId)
                                    .OrderByDescending(a => a.CreateTime)
                                    .ToList<RefusalRecordDto>();
                                s.RefusalRecords = refusalRecords;

                                s.Score += s.DisableAMScore + s.DisableCashierScore;//综合分录题中的子分录题分数需要加上审核的分数
                            }

                            if (detail != null && detail.QuestionId > 0)
                            {
                                s.AnswerCompareInfo = detail.AnswerCompareInfo;
                                s.AnswerResultStatus = detail.Status;
                                s.AccountEntryStatus = detail.AccountEntryStatus;
                                s.StudentAnswer = detail.Answer;
                                s.SummaryInfos = detail.SummaryInfos;
                                s.AssistNames = detail.AssistNames;
                                s.CertificateNo = detail.CertificateNo;
                                s.AnswerScore = detail.Score;
                            }
                        });
                    }

                    var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                    || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge).Select(a => a.QuestionId).ToArray();

                    if (choiceQuestionIds.Any())
                    {
                        var optionAll = await DbContext.FreeSql.GetRepository<SxAnswerOption>()
                            .Where(a => choiceQuestionIds.Contains(a.TopicId))
                            .ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId_C}{questionId}", true, 10 * 60);

                        subQuestions.ForEach(a =>
                        {
                            var options = optionAll.Where(c => c.TopicId == a.QuestionId).Select(d => new ChoiceOption()
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Text = d.AnswerOption,
                                AttatchImgUrl = d.AnswerFileUrl
                            });
                            if (options.Any())
                            {
                                a.Options = options.ToList();
                            }
                        });
                    }

                    //if (subQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                    //{
                    //    subQuestions.ForEach(s =>
                    //    {
                    //        if (s.QuestionType == QuestionType.AccountEntry)//分录题
                    //        {
                    //            s.CertificateTopic = DbContext.FreeSql.GetRepository<SxCertificateTopic>().Select
                    //            .Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                    //            .Select(a => new CertificateTopicView())
                    //            .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId_C}{s.QuestionId}", true, 10 * 60);
                    //        }
                    //    });
                    //}

                    questionDTO.SubQuestion = subQuestions;
                }
                else if (question.QuestionType == QuestionType.AccountEntry || question.QuestionType == QuestionType.ProfitLoss)//分录题
                {
                    //if (question.QuestionType == QuestionType.CertifMSub_AccountEntry)
                    //{
                    //    var parentQuestion = await DbContext.FreeSql.GetRepository<SxTopic>().Select
                    //        .Where(s => s.Id == question.ParentId && s.IsDelete == CommonConstants.IsNotDelete)
                    //    .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByParentId_C}{question.ParentId}", true, 10 * 60);

                    //    questionDTO.IsReceipt = parentQuestion.IsReceipt;
                    //}
                    if (question.QuestionType == QuestionType.ProfitLoss)
                    {
                        var certificateQuantity = await DbContext.FreeSql.GetRepository<SxCertificateAnswer>()
                            .Where(s => s.TopicId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                            .CountAsync();

                        questionDTO.CertificateQuantity = certificateQuantity;
                    }
                    questionDTO.CertificateTopic = await DbContext.FreeSql.GetRepository<SxCertificateTopic>().Select
                        .Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .Select(s => new CertificateTopicView())
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicId_C}{questionId}", true, 10 * 60);

                    questionDTO.StandardSummaryInfos = questionDTO.CertificateTopic?.SummaryInfos;
                    if (gradeId > 0)
                    {
                        var refusalRecords = await DbContext.FreeSql.GetRepository<SxRefusalRecord>()
                            .Where(s => s.GradeId == gradeId && s.QuestionId == questionId)
                            .OrderByDescending(s => s.CreateTime)
                            .ToListAsync<RefusalRecordDto>();
                        questionDTO.RefusalRecords = refusalRecords;
                    }
                }
            }
            #endregion

            #region 附上答题信息及答案显示

            var gradeDetail = new SxExamUserGradeDetail();
            if (gradeId > 0)
            {
                if (questionDTO.QuestionType != QuestionType.CertificateMainSubTopic)//综合分录题答案存在子题中
                {
                    gradeDetail = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                         .Where(a => a.QuestionId == questionId && a.GradeId == gradeId)
                         .First(a => new SxExamUserGradeDetail
                         {
                             Status = a.Status,
                             Answer = a.Answer,
                             AnswerCompareInfo = a.AnswerCompareInfo,
                             AccountEntryStatus = a.AccountEntryStatus,
                             SummaryInfos = a.SummaryInfos,
                             CertificateNo = a.CertificateNo,
                             AssistNames = a.AssistNames,
                             ProfitLossUserAnswerValues = a.ProfitLossUserAnswerValues,
                             Score = a.Score,
                         });

                    if (gradeDetail != null)
                    {
                        questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
                        questionDTO.AnswerResultStatus = gradeDetail.Status;
                        questionDTO.AccountEntryStatus = gradeDetail.AccountEntryStatus;
                        questionDTO.StudentAnswer = gradeDetail.Answer;
                        questionDTO.SummaryInfos = gradeDetail.SummaryInfos;
                        questionDTO.AssistNames = gradeDetail.AssistNames;
                        questionDTO.ProfitLossUserAnswerValues = gradeDetail.ProfitLossUserAnswerValues;
                        questionDTO.CertificateNo = gradeDetail.CertificateNo;
                        questionDTO.AnswerScore = gradeDetail.Score;
                    }
                }
            }

            //if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || (isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition) || isView)
            //{
            //    if (gradeDetail != null)
            //    {
            //        if ((isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition))//大数据岗位要隐藏答案对比信息和作答状态（如果有值，前端会展示答题状态）
            //        {
            //            if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
            //            {
            //                questionDTO.SubQuestion.ForEach(a =>
            //                {
            //                    a.AnswerCompareInfo = null;
            //                });
            //            }
            //        }
            //        else
            //        {
            //            if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
            //            {
            //                var subGradeDetails = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(a => a.GradeId == gradeId && a.QuestionId != a.ParentQuestionId && a.ParentQuestionId == questionId)
            //                .ToList(a => new { a.QuestionId, a.Status });
            //                questionDTO.SubQuestion.ForEach(a =>
            //                {
            //                    var subGradeDetail = subGradeDetails.Where(s => s.QuestionId == a.QuestionId).FirstOrDefault();
            //                    a.AnswerResultStatus = subGradeDetail != null ? subGradeDetail.Status : AnswerResultStatus.None;
            //                });
            //            }
            //            questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
            //            questionDTO.AnswerResultStatus = gradeDetail.Status;
            //        }
            //    }
            //}
            //else
            //{
            //    questionDTO.Hint = null;
            //    questionDTO.FullContent = null;
            //    questionDTO.AnswerValue = null;
            //    questionDTO.AnswerCompareInfo = null;

            //    if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Count > 0)
            //    {
            //        questionDTO.SubQuestion.ForEach(a =>
            //        {
            //            a.Hint = null;
            //            a.FullContent = null;
            //            a.AnswerValue = null;
            //            a.AnswerCompareInfo = null;
            //        });
            //    }

            //    questionDTO.AnswerResultStatus = gradeDetail == null ? AnswerResultStatus.None : AnswerResultStatus.Pending;
            //}
            #endregion

            questionDTO.IsSettled = grade.IsSettled;
            result.Data = questionDTO;
            result.Msg = $"操作成功{msg}";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        /// <summary>
        /// 获取题目信息基础函数
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoBase(SxExamPaper exam, SxExamUserGrade grade, SxTopic question, long userId, bool isBigDataSubmit = false, bool isView = false)
        {
            var result = new ResponseContext<ExamQuestionInfo>();

            var examId = exam?.Id;
            var gradeId = grade?.Id;
            var questionId = question.Id;
            // TODO MapTo配置文件
            var questionDTO = question.MapTo<ExamQuestionInfo>();
            questionDTO.QuestionId = questionId;
            var msg = string.IsNullOrEmpty(questionDTO.Content) ? "题目内容content为空" : "";
            //加载题目信息
            if (questionDTO.QuestionType == QuestionType.SingleChoice
                || questionDTO.QuestionType == QuestionType.MultiChoice
                || questionDTO.QuestionType == QuestionType.Judge)
            {
                //添加选项信息
                var options = await DbContext.FreeSql.GetRepository<SxAnswerOption>().Where(a => a.TopicId == questionId).OrderBy(a => a.Sort).ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId_C}{questionId}", true, 10 * 60);
                var optionList = new List<ChoiceOption>();
                options.ForEach(a =>
                {
                    optionList.Add(new ChoiceOption()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Text = a.AnswerOption,
                        AttatchImgUrl = a.AnswerFileUrl
                    });
                });
                questionDTO.Options = optionList;
            }
            else
            {
                //复杂题型
                //1.添加附件信息
                var fileList = new List<QuestionFile>();
                var fileIds = question.TopicFileIds;
                if (!string.IsNullOrEmpty(fileIds))
                {
                    var fileIdArr = fileIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (fileIdArr.Any())
                    {
                        var files = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(a => fileIdArr.Contains(a.Id))
                            .OrderBy(a => a.CoverBack == TopicFileCoverBack.Cover ? -1 : a.Sort)
                            .Select(a => new QuestionFile() { Name = a.Name, Url = a.Url, Sort = a.Sort, Type = a.Type, QuestionId = a.TopicId })
                            .ToListAsync($"{CommonConstants.Cache_GetTopicFilesByTopicId_C}{questionId}", true, 10 * 60);
                        questionDTO.QuestionFile = files;
                    }
                }
                //2.添加子题目
                if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                {
                    var subQuestions = DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.ParentId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId_C}{questionId}", true, 10 * 60, true, false).Result
                        .Select(s =>
                        new ExamQuestionInfo()
                        {
                            QuestionId = s.Id,
                            QuestionType = s.QuestionType,
                            QuestionContentType = s.QuestionContentType,
                            CalculationType = s.CalculationType,
                            Hint = s.Hint,
                            Content = s.Content,
                            TopicContent = s.TopicContent,
                            FullContent = s.FullContent,
                            AnswerValue = s.AnswerValue,
                            Score = s.Score,
                            Sort = s.Sort,
                            IsCopy = s.IsCopy,
                            IsCopyBigData = s.IsCopyBigData,
                            IsDisorder = s.IsDisorder
                        }).ToList();

                    var detailList = new List<SxExamUserGradeDetail>();
                    if (gradeId > 0)
                    {
                        detailList = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(s => s.GradeId == gradeId && s.ParentQuestionId == questionId)
                            .ToListAsync(s => new SxExamUserGradeDetail()
                            {
                                Answer = s.Answer,
                                AnswerCompareInfo = s.AnswerCompareInfo,
                                AccountEntryStatus = s.AccountEntryStatus,
                                QuestionId = s.QuestionId
                            });
                    }

                    if (subQuestions.Any() && detailList.Any())
                    {
                        subQuestions.ForEach(s =>
                        {
                            var detail = detailList.FirstOrDefault(a => a.QuestionId == s.QuestionId);
                            if (detail != null && detail.QuestionId > 0)
                            {
                                s.StudentAnswer = detail.Answer;
                                s.AnswerCompareInfo = detail.AnswerCompareInfo;
                                s.AccountEntryStatus = detail.AccountEntryStatus;
                            }
                        });
                    }

                    var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                    || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge).Select(a => a.QuestionId).ToArray();

                    if (choiceQuestionIds.Any())
                    {
                        var optionAll = await DbContext.FreeSql.GetRepository<SxAnswerOption>().Where(a => choiceQuestionIds.Contains(a.TopicId)).ToListAsync($"{CommonConstants.Cache_GetOptionsByTopicId_C}{questionId}", true, 10 * 60);
                        subQuestions.ForEach(a =>
                        {
                            var options = optionAll.Where(c => c.TopicId == a.QuestionId).Select(d => new ChoiceOption()
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Text = d.AnswerOption,
                                AttatchImgUrl = d.AnswerFileUrl
                            });
                            if (options.Any())
                            {
                                a.Options = options.ToList();
                            }
                        });
                    }

                    if (subQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                    {
                        subQuestions.ForEach(s =>
                        {
                            if (s.QuestionType == QuestionType.AccountEntry)
                            {
                                s.CertificateTopic = DbContext.FreeSql.GetRepository<SxCertificateTopic>().Select.Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                                .Select(a => new CertificateTopicView())
                                .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId_C}{s.QuestionId}", true, 10 * 60);
                            }
                        });
                    }

                    questionDTO.SubQuestion = subQuestions;
                }
                else if (question.QuestionType == QuestionType.AccountEntry || question.QuestionType == QuestionType.CertifMSub_AccountEntry || question.QuestionType == QuestionType.ProfitLoss)//分录题
                {
                    if (question.QuestionType == QuestionType.CertifMSub_AccountEntry)
                    {
                        var parentQuestion = await DbContext.FreeSql.GetRepository<SxTopic>().Select.Where(s => s.Id == question.ParentId && s.IsDelete == CommonConstants.IsNotDelete)
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByParentId_C}{question.ParentId}", true, 10 * 60);
                        questionDTO.IsReceipt = parentQuestion.IsReceipt;
                    }
                    if (question.QuestionType == QuestionType.ProfitLoss)
                    {
                        var certificateQuantity = await DbContext.FreeSql.GetRepository<SxCertificateAnswer>().Where(s => s.TopicId == questionId && s.IsDelete == CommonConstants.IsNotDelete).CountAsync();
                        questionDTO.CertificateQuantity = certificateQuantity;
                    }
                    questionDTO.CertificateTopic = await DbContext.FreeSql.GetRepository<SxCertificateTopic>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .Select(s => new CertificateTopicView())
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicByTopicId_C}{questionId}", true, 10 * 60);
                    questionDTO.StandardSummaryInfos = questionDTO.CertificateTopic?.SummaryInfos;
                    if (gradeId > 0)
                    {
                        var refusalRecords = await DbContext.FreeSql.GetRepository<SxRefusalRecord>()
                            .Where(s => s.GradeId == gradeId && s.QuestionId == questionId)
                            .OrderByDescending(s => s.CreateTime)
                            .ToListAsync<RefusalRecordDto>();
                        questionDTO.RefusalRecords = refusalRecords;
                    }
                }
            }

            #region 附上答题信息及答案显示

            var gradeDetail = new SxExamUserGradeDetail();
            if (gradeId > 0)
            {
                gradeDetail = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(a => a.QuestionId == questionId && a.GradeId == gradeId)
                    .First(a => new SxExamUserGradeDetail { Status = a.Status, Answer = a.Answer, AnswerCompareInfo = a.AnswerCompareInfo, AccountEntryStatus = a.AccountEntryStatus, SummaryInfos = a.SummaryInfos, CertificateNo = a.CertificateNo, AssistNames = a.AssistNames, ProfitLossUserAnswerValues = a.ProfitLossUserAnswerValues });
            }

            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || (isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition) || isView)
            {
                if (gradeDetail != null)
                {
                    if ((isBigDataSubmit && question.PositionId == CommonConstants.BigDataPosition))//大数据岗位要隐藏答案对比信息和作答状态（如果有值，前端会展示答题状态）
                    {
                        if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
                        {
                            questionDTO.SubQuestion.ForEach(a =>
                            {
                                a.AnswerCompareInfo = null;
                            });
                        }
                    }
                    else
                    {
                        if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Any())
                        {
                            var subGradeDetails = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(a => a.GradeId == gradeId && a.QuestionId != a.ParentQuestionId && a.ParentQuestionId == questionId)
                            .ToList(a => new { a.QuestionId, a.Status });
                            questionDTO.SubQuestion.ForEach(a =>
                            {
                                var subGradeDetail = subGradeDetails.Where(s => s.QuestionId == a.QuestionId).FirstOrDefault();
                                a.AnswerResultStatus = subGradeDetail != null ? subGradeDetail.Status : AnswerResultStatus.None;
                            });
                        }
                        questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
                        questionDTO.AnswerResultStatus = gradeDetail.Status;

                    }
                }
            }
            else
            {
                questionDTO.Hint = null;
                questionDTO.FullContent = null;
                questionDTO.AnswerValue = null;
                questionDTO.AnswerCompareInfo = null;

                if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Count > 0)
                {
                    questionDTO.SubQuestion.ForEach(a =>
                    {
                        a.Hint = null;
                        a.FullContent = null;
                        a.AnswerValue = null;
                        a.AnswerCompareInfo = null;
                    });
                }

                questionDTO.AnswerResultStatus = gradeDetail == null ? AnswerResultStatus.None : AnswerResultStatus.Pending;
            }
            questionDTO.AccountEntryStatus = gradeDetail == null ? AccountEntryStatus.None : gradeDetail.AccountEntryStatus;
            questionDTO.StudentAnswer = gradeDetail?.Answer;
            questionDTO.SummaryInfos = gradeDetail?.SummaryInfos;
            questionDTO.AssistNames = gradeDetail?.AssistNames;
            questionDTO.ProfitLossUserAnswerValues = gradeDetail?.ProfitLossUserAnswerValues;
            questionDTO.CertificateNo = gradeDetail != null ? gradeDetail.CertificateNo : 0;
            #endregion

            questionDTO.IsSettled = grade.IsSettled;
            result.Data = questionDTO;
            result.Msg = $"操作成功{msg}";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        #endregion

        #region 提交答案

        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model, long userId)
        {
            var result = new ResponseContext<QuestionResult>(CommonConstants.BadRequest, "");
            //var user = userTicket;

            #region 校验

            if (model.GradeId == 0 || model.QuestionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "GradeId,QuestionId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"考试已结束！";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.Id == model.QuestionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetTopicById_C}{model.QuestionId}", true, 10 * 60, true);
            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "题目不存在";
                return result;
            }

            if (exam.Id != grade.ExamId || exam.CaseId != question.CaseId)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = "参数信息不匹配 ";
                return result;
            }

            #endregion

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:

                    return await SubmitPracticeAnswer(model, exam, grade, question, userId);
                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }
        }

        /// <summary>
        /// 提交答案--自主练习
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitPracticeAnswer(QuestionAnswer answer, SxExamPaper exam, SxExamUserGrade grade, SxTopic question, long userId)
        {
            if (exam.Status == ExamStatus.End)
            {
                var result = new ResponseContext<QuestionResult>(CommonConstants.BadRequest, "");
                result.Code = CommonConstants.BadRequest;
                result.Msg = "考试已结束!";
                return result;
            }
            return await SubmitAnswerBase(answer, exam, question, userId);
        }

        /// <summary>
        /// 提交答案基础函数
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitAnswerBase(QuestionAnswer answer, SxExamPaper exam, SxTopic question, long userId)
        {
            var result = new ResponseContext<QuestionResult>() { Data = new QuestionResult() { QuestionId = question.Id } };

            try
            {
                List<SxExamCertificateDataRecord> AddAccountEntryList(QuestionAnswer ans, long detailId, string inCertificateName, string inCertificateNo)
                {
                    var entryList = new List<SxExamCertificateDataRecord>();
                    if (ans.AccountEntryList != null && ans.AccountEntryList.Any())
                    {
                        entryList = ans.AccountEntryList.Select(a => a.MapTo<SxExamCertificateDataRecord>()).ToList();
                        entryList.ForEach(a =>
                        {
                            a.Id = IdWorker.NextId();
                            a.ExamId = exam.Id;
                            a.GradeId = answer.GradeId;
                            a.GradeDetailId = detailId;
                            a.QuestionId = ans.QuestionId;
                            a.CaseId = exam.CaseId;
                            a.TenantId = exam.TenantId;
                            a.BorrowAmount = a.BorrowAmount;
                            a.CreditorAmount = a.CreditorAmount;
                            a.CertificateName = inCertificateName;
                            a.CertificateNo = inCertificateNo;
                        });
                    }

                    return entryList;
                }

                #region 提交答案

                var accountEntryList = new List<SxExamCertificateDataRecord>();
                var certificateName = "";
                var certificateNo = "";
                result = RedisLock.SkipLock(userId.ToString(), () =>
                {
                    var lockResult = new ResponseContext<QuestionResult>() { Data = new QuestionResult() };

                    try
                    {
                        var gradeDetailList = DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(a => a.GradeId == answer.GradeId && a.ParentQuestionId == answer.QuestionId)
                        .Master().ToList(a => new { a.Id, a.ExamId, a.QuestionId, a.Answer });
                        SxCertificateNoRecord certificateNoRecord = null;
                        if (gradeDetailList == null || gradeDetailList.Count == 0)
                        {
                            #region 插入作答信息

                            var detailInsertList = new List<SxExamUserGradeDetail>();
                            //var detailDeleteList = new List<ExamStudentGradeDetail>();
                            DateTime nowTime = DateTime.Now;
                            //var sourceData = JsonHelper.SerializeObject(answer);
                            var mainDetail = new SxExamUserGradeDetail()
                            {
                                Id = IdWorker.NextId(),
                                GradeId = answer.GradeId,
                                ExamId = exam.Id,
                                TenantId = 0,//user.TenantId,
                                QuestionId = answer.QuestionId,
                                ParentQuestionId = answer.QuestionId,
                                PositionId = question.PositionId,
                                UserId = userId,
                                //StudentId = CommonConstants.StudentId,
                                Answer = answer.AnswerValue,
                                CreateTime = nowTime,
                                QuestionType = question.QuestionType,
                                BusinessDate = answer.BusinessDate,
                                SummaryInfos = answer.SummaryInfos,
                                AssistNames = answer.AssistNames,
                                ProfitLossUserAnswerValues = answer.ProfitLossUserAnswerValues
                                //SourceDataInfo = sourceData
                            };
                            answer.GradeDetailId = mainDetail.Id;

                            if (question.QuestionType == QuestionType.MainSubQuestion)//多题型 
                            {
                                var multiQuestions = DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.ParentId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                                    .OrderBy(s => s.Sort)
                                    .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId_C}{question.Id}", true, 10 * 60, true).Result
                                    .Select(s =>
                                    new SubQuestionDto()
                                    {
                                        Id = s.Id,
                                        QuestionType = s.QuestionType,
                                        PositionId = s.PositionId
                                    });

                                foreach (var item in answer.MultiQuestionAnswers)
                                {
                                    var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                                    var subDetail = new SxExamUserGradeDetail()
                                    {
                                        Id = IdWorker.NextId(),
                                        TenantId = 0, //user.TenantId,
                                        ExamId = exam.Id,
                                        GradeId = answer.GradeId,
                                        QuestionId = item.QuestionId,
                                        PositionId = subQuestion.PositionId,
                                        UserId = userId,
                                        //StudentId = CommonConstants.StudentId,
                                        Answer = item.AnswerValue,
                                        ParentQuestionId = answer.QuestionId,
                                        CreateTime = nowTime
                                        //SourceDataInfo = sourceData
                                    };

                                    item.GradeDetailId = subDetail.Id;

                                    #region 分录题特殊处理

                                    if (subQuestion.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                    {
                                        subDetail.AccountEntryAuditStatus = item.AccountEntryAuditStatus;
                                        subDetail.AccountEntryStatus = item.AccountEntryStatus;
                                        accountEntryList.AddRange(AddAccountEntryList(item, subDetail.Id, "", ""));
                                    }

                                    #endregion

                                    detailInsertList.Add(subDetail);

                                    //detailDeleteList.Add(new ExamStudentGradeDetail() { GradeId = subDetail.GradeId, ParentQuestionId = subDetail.ParentQuestionId, QuestionId = subDetail.QuestionId }) ;

                                }
                            }
                            else if (question.QuestionType == QuestionType.AccountEntry || question.QuestionType == QuestionType.CertifMSub_AccountEntry || question.QuestionType == QuestionType.ProfitLoss)//分录题特殊处理
                            {
                                mainDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                                mainDetail.AccountEntryStatus = answer.AccountEntryStatus;
                                #region 凭证号处理 
                                var certificateTopic = DbContext.FreeSql.GetRepository<SxCertificateTopic>().Where(s => s.TopicId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .First($"{CommonConstants.Cache_GetCertificateTopicAllByTopicId_C}{answer.QuestionId}", true, 10 * 60, true);
                                var certificateWord = certificateTopic.CertificateWord;

                                switch (certificateTopic.CertificateWord)
                                {
                                    case CertificateWord.Bank:
                                        certificateName = "银行";
                                        break;
                                    case CertificateWord.Cash:
                                        certificateName = "现金";
                                        break;
                                    case CertificateWord.Transfer:
                                        certificateName = "转账";
                                        break;
                                    case CertificateWord.Remember:
                                        certificateName = "记";
                                        break;
                                    default:
                                        break;
                                }
                                certificateNoRecord = DbContext.FreeSql.GetRepository<SxCertificateNoRecord>()
                                .Where(s => s.CertificateType == certificateWord && s.ExamId == exam.Id && s.GradeId == answer.GradeId).First();
                                mainDetail.CertificateWord = certificateWord;
                                if (certificateNoRecord != null)
                                {
                                    certificateNo = (certificateNoRecord.LastCertificateNo + 1).ToString();
                                    mainDetail.CertificateNo = certificateNoRecord.LastCertificateNo + 1;
                                    certificateNoRecord.LastCertificateNo += 1;
                                }
                                else
                                {
                                    mainDetail.CertificateNo = 1;
                                    certificateNoRecord = new SxCertificateNoRecord();
                                    //certificateNoRecord.Id = IdWorker.NextId();
                                    certificateNoRecord.ExamId = exam.Id;
                                    certificateNoRecord.GradeId = answer.GradeId;
                                    certificateNoRecord.CertificateType = certificateWord;
                                    certificateNoRecord.LastCertificateNo = 1;
                                    certificateNo = "1";
                                }
                                #endregion
                                if (question.QuestionType == QuestionType.CertifMSub_AccountEntry)
                                {
                                    var parentQuestion = DbContext.FreeSql.GetRepository<SxTopic>().Select.Where(s => s.Id == question.ParentId && s.IsDelete == CommonConstants.IsNotDelete)
                                    .First($"{CommonConstants.Cache_GetCertificateTopicByParentId_C}{question.ParentId}", true, 10 * 60);
                                    mainDetail.IsHasReceivePayment = parentQuestion.IsReceipt;
                                }

                                accountEntryList = AddAccountEntryList(answer, mainDetail.Id, certificateName, certificateNo);
                            }

                            detailInsertList.Add(mainDetail);
                            //detailDeleteList.Add(new ExamStudentGradeDetail() { GradeId = mainDetail.GradeId, ParentQuestionId = mainDetail.ParentQuestionId, QuestionId = mainDetail.QuestionId });
                            DbContext.FreeSql.Transaction(() =>
                    {
                        detailInsertList.ForEach(s =>
                {
                    DbContext.FreeSql.Delete<SxExamUserGradeDetail>().Where(b => b.GradeId == s.GradeId && b.ParentQuestionId == s.ParentQuestionId && b.QuestionId == s.QuestionId).ExecuteAffrows();
                });

                        DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Insert(detailInsertList);

                        if (certificateNoRecord != null)
                        {
                            if (certificateNoRecord.Id > 0)
                            {
                                DbContext.FreeSql.GetRepository<SxCertificateNoRecord>().UpdateDiy
                        .UpdateColumns(a => new { a.UpdateTime, a.LastCertificateNo })
                        .Set(a => a.UpdateTime, DateTime.Now)
                        .Set(a => a.LastCertificateNo, certificateNoRecord.LastCertificateNo).Where(a => a.Id == certificateNoRecord.Id).ExecuteAffrows();
                            }
                            else
                            {
                                certificateNoRecord.Id = IdWorker.NextId();
                                DbContext.FreeSql.GetRepository<SxCertificateNoRecord>().Insert(certificateNoRecord);
                            }
                        }
                        if (accountEntryList.Any())
                        {
                            DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Insert(accountEntryList);
                        }

                        if (question.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理
                        {
                            DbContext.FreeSql.GetRepository<SxExamUserGrade>().UpdateDiy
                    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                    .Set(a => a.UpdateTime, DateTime.Now)
                    .Set(a => a.IsSettled, true).Where(a => a.Id == answer.GradeId).ExecuteAffrows();
                        }
                        if (question.QuestionType == QuestionType.CertifMSub_AccountEntry && answer.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                        {
                            UpdateMainAccountEntryStatus(question.ParentId, answer.GradeId, AccountEntryStatus.AccountingManager);
                        }
                    });

                            #endregion
                        }
                        else
                        {
                            #region 更新作答信息

                            foreach (var detail in gradeDetailList)
                            {
                                if (detail.QuestionId == answer.QuestionId)
                                {
                                    answer.GradeDetailId = detail.Id;
                                    if (question.QuestionType == QuestionType.MainSubQuestion)//多题型
                                    {
                                        var questionIds = gradeDetailList.Select(s => s.QuestionId).Where(s => s != answer.QuestionId);

                                        foreach (var item in gradeDetailList)
                                        {
                                            if (item.QuestionId == question.Id)
                                            {
                                                continue;
                                            }

                                            var dtoDetail = answer.MultiQuestionAnswers.FirstOrDefault(a => a.QuestionId == item.QuestionId);
                                            if (dtoDetail == null || dtoDetail.QuestionId == 0)
                                            {
                                                continue;
                                            }
                                            if (dtoDetail.IsIgnore)
                                            {
                                                dtoDetail.AnswerValue = item.Answer;
                                            }
                                            dtoDetail.GradeDetailId = item.Id;

                                            #region 分录题特殊处理

                                            if (dtoDetail.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                            {
                                                accountEntryList.AddRange(AddAccountEntryList(dtoDetail, item.Id, "", ""));
                                            }

                                            #endregion
                                        }

                                        //先删除之前录入凭证数据
                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && questionIds.Contains(a.QuestionId));
                                                DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    else
                                    {
                                        #region 结账题特殊处理（有答题记录时取消结账--即删除答题记录）

                                        if (question.QuestionType == QuestionType.SettleAccounts)
                                        {
                                            //删除答案
                                            DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Delete(s => s.Id == detail.Id);
                                            DbContext.FreeSql.GetRepository<SxExamUserGrade>().UpdateDiy
                                            .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                            .Set(a => a.UpdateTime, DateTime.Now)
                                            .Set(a => a.IsSettled, false).Where(a => a.Id == answer.GradeId).ExecuteAffrows();

                                            //zhcc  20190824
                                            var questionResult = new QuestionResult()
                                            {
                                                No = question.Sort,
                                                QuestionId = question.Id,
                                                AnswerResult = AnswerResultStatus.None
                                            };
                                            lockResult.Data = questionResult;
                                            return lockResult;
                                        }

                                        #endregion

                                        #region 分录题：录入凭证存储更新（用于账簿查询）

                                        if (question.QuestionType == QuestionType.AccountEntry || question.QuestionType == QuestionType.CertifMSub_AccountEntry || question.QuestionType == QuestionType.ProfitLoss)
                                        {
                                            accountEntryList = AddAccountEntryList(answer, detail.Id, "", "");
                                            if (question.QuestionType == QuestionType.CertifMSub_AccountEntry)
                                            {
                                                if (answer.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                                                {
                                                    UpdateMainAccountEntryStatus(question.ParentId, answer.GradeId, AccountEntryStatus.AccountingManager);
                                                }
                                                else
                                                {
                                                    UpdateMainAccountEntryStatus(question.ParentId, answer.GradeId, AccountEntryStatus.None);
                                                }
                                            }
                                        }

                                        #endregion

                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                //查找原有凭证字和号
                                                var dataRecord = DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>()
                                        .Where(a => a.GradeId == answer.GradeId && a.QuestionId == answer.QuestionId).First();
                                                certificateName = dataRecord?.CertificateName;
                                                certificateNo = dataRecord?.CertificateNo;
                                                accountEntryList.ForEach(a =>
                                                {
                                                    a.CertificateNo = certificateNo;
                                                    a.CertificateName = certificateName;
                                                });
                                                //先删除之前录入凭证数据
                                                DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && a.QuestionId == answer.QuestionId);
                                                DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"提交答案失败";
                        CommonLogger.Error($"答题异常", ex);
                    }

                    return lockResult;

                }, timeoutSeconds: 5);

                if (result == null || result.Code != CommonConstants.SuccessCode)
                {
                    return result;
                }

                if (result.Data.QuestionId > 0)//取消结账，直接返回
                {
                    goto End;
                }

                #endregion

                //TODO:计算题目分数
                var task = Task.Run(() =>
                {
                    var compareRst = CompareTopicAnswer(question, answer);

                    return compareRst;
                }).ConfigureAwait(false);
                if (exam.CanShowAnswerBeforeEnd)
                {
                    result.Data = await task;
                }

                if (exam.ExamType != ExamType.PracticeTest && exam.CompetitionType == CompetitionType.TeamCompetition)
                {
                    if (question.QuestionType == QuestionType.MainSubQuestion)
                    {
                        result.Data.AccountEntryStatusDic = answer.MultiQuestionAnswers.Where(s => s.QuestionType == QuestionType.AccountEntry).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                    }
                    else if (question.QuestionType == QuestionType.AccountEntry)
                    {
                        result.Data.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() { new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(answer.QuestionId, answer.AccountEntryStatus, answer.AccountEntryAuditStatus, answer.Sort) };
                    }
                }
                result.Data.CertificateNo = certificateNo;
                result.Msg = "成功提交答案";

            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"答题异常:GradeId-{answer.GradeId},QuestionId-{answer.QuestionId},UserId:{userId}";
                CommonLogger.Error(msg + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
            }

            End:
            return result;
        }

        #endregion
        #region 修改综合分录大题状态
        private void UpdateMainAccountEntryStatus(long parentQuestionId, long gradeId, AccountEntryStatus status)
        {
            DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().UpdateDiy
                    .UpdateColumns(a => new { a.UpdateTime, a.AccountEntryStatus })
                    .Set(a => a.UpdateTime, DateTime.Now)
                    .Set(a => a.AccountEntryStatus, status)
                    .Where(a => a.QuestionId == parentQuestionId && a.GradeId == gradeId).ExecuteAffrows();
        }
        #endregion
        #region 提交考卷

        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, long userId, bool isAutoSubmit = false)
        {
            //var user = userTicket;

            var result = new ResponseContext<GradeInfo>();

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"GradeId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"您已经提交过考卷了！";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById_C}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            //var studentGrade = new SxExamPaperGroupStudent();

            //if (exam.ExamType != ExamType.PracticeTest & exam.CompetitionType == CompetitionType.TeamCompetition)//团队赛
            //{
            //    studentGrade = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Where(s => s.GroupId == grade.GroupId && s.UserId == user.Id && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            //    if (studentGrade == null)
            //    {
            //        result.Code = CommonConstants.NotFund;
            //        result.Msg = "没找到答题记录";
            //        return result;
            //    }

            //    if (studentGrade.StudentExamStatus == StudentExamStatus.End)
            //    {
            //        result.Code = CommonConstants.ErrorCode;
            //        result.Msg = $"您已经提交过考卷了！";
            //        return result;
            //    }
            //}

            #endregion

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:

                    return await SubmitPracticeExam(exam, grade, userId);

                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }
        }

        /*
          /// <summary>
          /// 一键交卷
          /// </summary>
          /// <param name="examId"></param>
          public async Task<ResponseContext<long>> SubmitExamBatch(long examId)
          {
              var result = new ResponseContext<long>();

              try
              {
                  #region 校验

                  if (examId == 0)
                  {
                      result.Code = CommonConstants.BadRequest;
                      result.Msg = $"ExamId不能为0 ";
                      return result;
                  }

                  var exam = await DbContext.FreeSql.GetRepository<SxExamPaper>().Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById_C}{examId}", true, 10 * 60);
                  if (exam == null)
                  {
                      result.Code = CommonConstants.ErrorCode;
                      result.Msg = $"考试信息不存在 ";
                      return result;
                  }

                  if (DateTime.Now < exam.BeginTime)
                  {
                      result.Code = CommonConstants.ErrorCode;
                      result.Msg = $"考试未开始！";
                      return result;
                  }
                  else if (DateTime.Now < exam.EndTime)
                  {
                      result.Code = CommonConstants.ErrorCode;
                      result.Msg = $"考试未结束，不能批量交卷！";
                      return result;
                  }

                  #endregion

                  if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                  {
                      goto GradeSubmit;
                  }
                  else
                  {
                      //var gradeList = await DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().Where(a => a.ExamId == examId && a.StudentExamStatus == StudentExamStatus.Started).ToListAsync();

                      //if (gradeList != null && gradeList.Any())
                      //{
                      //    foreach (var studentGrade in gradeList)
                      //    {
                      //        studentGrade.Status = GroupStatus.End;
                      //        //提交考卷
                      //        await SubmitExamGroupGrade(exam, studentGrade, true, true);
                      //    }

                      //    await DbContext.FreeSql.GetRepository<ExamPaperGroupStudent>().UpdateDiy.SetSource(gradeList).ExecuteAffrowsAsync();
                      //}
                  }

                  GradeSubmit://主作答纪录
                  {
                      var gradeList = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(a => a.ExamId == examId && a.Status == StudentExamStatus.Started).ToListAsync();

                      if (gradeList != null && gradeList.Any())
                      {
                          foreach (var grade in gradeList)
                          {
                              //批量提交考卷
                              await SubmitExamGrade(exam, grade, true, true);
                          }

                          await DbContext.FreeSql.GetRepository<SxExamUserGrade>().UpdateDiy.SetSource(gradeList).ExecuteAffrowsAsync();

                          //if (exam.CompetitionType == CompetitionType.TeamCompetition)
                          //{
                          //    var groupIds = gradeList.Where(s => s.GroupId > 0).Select(s => s.GroupId.ToString()).ToList();
                          //    if (groupIds != null && groupIds.Any())
                          //    {
                          //        await DbContext.FreeSql.GetRepository<ExamPaperGroup>().UpdateDiy.Set(s => s.Status, GroupStatus.End).UpdateColumns(s => s.Status).Where(s => s.ExamId == exam.Id).ExecuteAffrowsAsync();

                          //        groupIds.ForEach(groupId =>
                          //        {
                          //            ImHelper.SendChanMessage(Guid.Empty, groupId, "{\"messageType\":\"examEnded\"}");
                          //            groupId = $"{CommonConstants.ImMessagePre}{groupId}";
                          //        });


                          //        RedisHelper.Del(groupIds.ToArray());
                          //    }
                          //}
                          //else
                          //{
                          //    ImHelper.SendChanMessage(Guid.Empty, exam.Id.ToString(), "{\"messageType\":\"examEnded\"}");
                          //}

                      }
                  }

                  exam.Status = ExamStatus.End;
                  await DbContext.FreeSql.GetRepository<SxExamPaper>().UpdateDiy.SetSource(exam).UpdateColumns(s => s.Status).ExecuteAffrowsAsync();
              }
              catch (Exception ex)
              {
                  result.Msg = "批量提交考卷异常，请联系管理员！";
                  result.Code = CommonConstants.ErrorCode;
                  var msg = $"批量交卷异常:examId-{examId}";
                  CommonLogger.Error(msg, ex);
              }

              if (result.Code == CommonConstants.SuccessCode)
              {
                  result.Msg = $"操作成功！";
              }

              return result;
          }
          */

        /// <summary>
        /// 提交考卷--自主练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitPracticeExam(SxExamPaper exam, SxExamUserGrade grade, long userId)
        {
            return await SubmitExamBase(exam, grade, userId);
        }

        /// <summary>
        /// 提交考卷基础函数
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitExamBase(SxExamPaper exam, SxExamUserGrade grade, long userId, bool isAutoSubmit = false)
        {
            var result = new ResponseContext<GradeInfo>();
            try
            {
                //提交考卷,计算本次答卷得分，并保存到数据库中
                var currtExamIntegral = await SubmitExamGrade(exam, grade, isAutoSubmit);

                //查询出老师评语
                var teacherComments = await DbContext.FreeSql.GetGuidRepository<SxMastersComments>(x => x.IsDelete == CommonConstants.IsNotDelete)
                    .Where(e => grade.Score >= e.MinScore && grade.Score <= e.MaxScore)
                    .FirstAsync(e => e.Details);

                result.Data = new GradeInfo()
                {
                    ExamScore = exam.TotalScore,
                    TeacherComments = teacherComments,
                    GradeId = grade.Id,
                    GradeScore = grade.Score,
                    UsedSeconds = (long)grade.UsedSeconds,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score > exam.PassScore,
                    StudentExamStatus = grade.Status,
                    Integral = currtExamIntegral.ToString("#0.00"),
                };
            }
            catch (Exception ex)
            {
                result.Msg = "提交考卷异常，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"提交考卷异常:gradeId-{grade.Id},UserId:{userId}";
                CommonLogger.Error(msg, ex);
            }

            if (result.Code == CommonConstants.SuccessCode)
                result.Msg = "提交成功";
            return result;
        }
        /// <summary>
        /// 计算本次交卷获得积分
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private static async Task<SxExamIntegral> ComputeExamIntegral(SxExamPaper exam, SxExamUserGrade grade)
        {
            #region 计算获取积分
            decimal currtIntegral = 0;//本次获得的积分

            //查询该次任务总分
            var taskTotalIntegral = await DbContext.FreeSql.GetGuidRepository<SxTask>(x => x.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => e.Id == exam.MonthTaskId)
                 .FirstAsync(e => e.Integral);
            //查询是否已经有得分记录，是否已经作答过（得0分也会有得分记录）
            var hasUserIntegrals = await DbContext.FreeSql.GetGuidRepository<SxExamIntegral>(x => x.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.UserId == grade.UserId && e.TaskId == exam.MonthTaskId)
                .AnyAsync();

            if (exam.TotalScore > 0)
            {
                currtIntegral = grade.Score / exam.TotalScore * (decimal)(hasUserIntegrals ? taskTotalIntegral * 0.1m : taskTotalIntegral);//积分规则：得分/总分*任务积分（第二次以及以后作答任务积分*01）
            }

            SxExamIntegral sxExamPoint = new SxExamIntegral
            {
                Id = IdWorker.NextId(),
                CaseId = exam.CaseId,
                Integral = currtIntegral,
                ExamId = exam.Id,
                GradeId = grade.Id,
                TaskId = exam.MonthTaskId,
                TenantId = grade.TenantId,
                UserId = grade.UserId,
                TotalIntegral = taskTotalIntegral,
            };
            #endregion
            return sxExamPoint;
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 新增作答记录
        /// </summary>
        /// <param name="exam">考试信息</param>
        /// <param name="tuples">(userId,tenantId,groupId)</userId>
        /// <returns></returns>
        private ResponseContext<SxExamUserGrade> InsertGrade(SxExamPaper exam, params ExamPaperGroupStudentDto[] list)
        {
            var result = new ResponseContext<SxExamUserGrade>() { Code = CommonConstants.SuccessCode };

            try
            {
                //插入学生考试作答主表
                var gradeId = IdWorker.NextId();
                var grade = new SxExamUserGrade()
                {
                    Id = gradeId,
                    ExamId = exam.Id,
                    ExamPaperScore = exam.TotalScore,
                    LeftSeconds = exam.TotalMinutes * 60,
                    //RecordTime = DateTime.Now,
                    UserId = list[0].UserId,
                    TenantId = list[0].TenantId,
                    //GroupId = list[0].GroupId,
                    //StudentId = CommonConstants.StudentId,
                    Status = StudentExamStatus.Started
                };

                var lastGradeList = new List<SxExamUserLastGrade>();

                switch (exam.ExamType)
                {
                    case ExamType.PracticeTest:
                    case ExamType.EmulationTest:
                        var userIds = list.Select(s => s.UserId).ToArray();
                        var lastGrades = DbContext.FreeSql.GetRepository<SxExamUserLastGrade>().Select.Where(s => s.ExamId == exam.Id && userIds.Contains(s.UserId))
                            .ToList(s => new { s.Id, s.UserId });

                        list.ToList().ForEach(t =>
                        {
                            var lastGradeId = lastGrades.FirstOrDefault(s => s.UserId == t.UserId)?.Id ?? 0;

                            var lastGrade = new SxExamUserLastGrade();
                            if (lastGradeId > 0)
                            {
                                lastGrade.Id = lastGradeId;
                                lastGrade.GradeId = gradeId;
                                lastGrade.UpdateTime = DateTime.Now;
                            }
                            else
                            {
                                lastGrade = new SxExamUserLastGrade()
                                {
                                    GradeId = gradeId,
                                    ExamId = exam.Id,
                                    //GroupId = t.GroupId,
                                    UserId = t.UserId,
                                    TenantId = t.TenantId
                                };
                            }
                            lastGradeList.Add(lastGrade);
                        });

                        break;
                    default:
                        throw new NotImplementedException($"还没有实现 {exam.ExamType}考试类型,稍等稍等");
                }

                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.GetRepository<SxExamUserGrade>().Insert(grade);
                    if (lastGradeList.Any())
                    {
                        lastGradeList.ForEach(s =>
                        {
                            if (s.Id > 0)
                            {
                                DbContext.FreeSql.GetRepository<SxExamUserLastGrade>().UpdateDiy.SetSource(s).UpdateColumns(a => new { a.GradeId, a.UpdateTime }).ExecuteAffrows();
                            }
                            else
                            {
                                s.Id = IdWorker.NextId();
                                DbContext.FreeSql.GetRepository<SxExamUserLastGrade>().Insert(s);
                            }
                        });
                    }
                });

                result.Data = grade;
            }
            catch (Exception ex)
            {
                result.Msg = "新增答题记录失败，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"新增答题记录常:examId-{exam.Id},UserId:{string.Join(',', list.Select(s => s.UserId))}";
                CommonLogger.Error(msg, ex);
                return result;
            }

            return result;
        }

        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isAutoSubmit">是否自动提交</param>
        /// <param name="isBatchSubmit">是否批量提交</param>
        private async Task<decimal> SubmitExamGrade(SxExamPaper exam, SxExamUserGrade grade, bool isAutoSubmit = false, bool isBatchSubmit = false)
        {
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isAutoSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            switch (exam.ExamType)
            {
                case ExamType.PracticeTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.CreateTime).TotalSeconds;
                    break;

                case ExamType.EmulationTest:
                    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                    {
                        grade.LeftSeconds = GetLeftSeconds(grade.LeftSeconds, grade.RecordTime);
                        grade.UsedSeconds = exam.TotalMinutes * 60 - grade.LeftSeconds;
                    }
                    break;

                case ExamType.SchoolCompetition:
                case ExamType.ProvincialCompetition:
                    if (exam.CompetitionType == CompetitionType.IndividualCompetition)
                    {
                        grade.LeftSeconds = GetLeftSeconds(exam);
                        grade.UsedSeconds = exam.TotalMinutes * 60 - grade.LeftSeconds;
                    }
                    break;
            }

            var totalQuestionCount = exam.TotalQuestionCount;

            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>()
                .Where(a => a.GradeId == grade.Id
                && (a.QuestionId == a.ParentQuestionId
                    || (a.QuestionType == QuestionType.TeticketTopic || a.QuestionType == QuestionType.ReceiptTopic || a.QuestionType == QuestionType.PayTopic)//综合分录题中的子题数据存储格式不统一，分录题QuestionId==ParentQuestionId，其他子题不相等
                   )
                    ).Master()
                .ToListAsync(a => new { a.QuestionId, a.Status, a.Score });
            //grade.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
            //grade.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
            //grade.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            //grade.BlankCount = totalQuestionCount - answerCount.Count;

            //if (grade.BlankCount <= 0)
            //    grade.BlankCount = 0;

            grade.Score = answerCount.Sum(a => a.Score);

            //计算本次作答获得积分
            SxExamIntegral currtExamIntegral = await ComputeExamIntegral(exam, grade);

            if (!isBatchSubmit)//非批量提交
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_eug = uow.GetRepository<SxExamUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        var repo_ei = uow.GetRepository<SxExamIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete);

                        //更新用户作答记录
                        await repo_eug.UpdateAsync(grade);
                        //添加本次积分
                        await repo_ei.InsertAsync(currtExamIntegral);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            decimal integral = 0;//本次获得积分
            if (currtExamIntegral != null)
                integral = currtExamIntegral.Integral;

            return integral;
        }

        /// <summary>
        /// 获取剩余考试时间
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private double GetLeftSeconds(double leftSeconds, DateTime recordTime)
        {
            var left = leftSeconds - (DateTime.Now - recordTime).TotalSeconds;
            if (left <= 0)
            {
                return 0;
            }
            return left;
        }

        /// <summary>
        /// 获取剩余考试时间
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private double GetLeftSeconds(SxExamPaper exam)
        {
            var left = (exam.EndTime.Value - DateTime.Now).TotalSeconds;
            if (left <= 0)
            {
                return 0;
            }
            return left;
        }

        #endregion

        #region 题目算分相关方法

        /// <summary>
        /// 比较答案
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="answer"></param>
        /// <param name="caseModel"></param>
        /// <returns></returns>
        private QuestionResult CompareTopicAnswer(SxTopic topic, QuestionAnswer answer)
        {
            var examResultDto = new QuestionResult { AnswerResult = AnswerResultStatus.Error, No = answer.Sort, QuestionId = answer.QuestionId };
            if (topic.QuestionType != QuestionType.MainSubQuestion && string.IsNullOrEmpty(answer.AnswerValue))
            {
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            if (topic.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理，直接得分
            {
                examResultDto.AnswerResult = AnswerResultStatus.Right;
                examResultDto.AnswerScore = topic.Score;
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            var score = topic.Score;
            var standardAnswerVal = HttpUtility.UrlDecode(topic.AnswerValue);
            var answerTheValue = HttpUtility.UrlDecode(answer.AnswerValue);
            AnswerResultStatus status;
            decimal answerScore = 0;
            switch (topic.QuestionType)
            {
                case QuestionType.SingleChoice:
                case QuestionType.Judge:
                    status = answerTheValue == standardAnswerVal ? AnswerResultStatus.Right : AnswerResultStatus.Error;
                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MultiChoice:
                    var standardVal = standardAnswerVal.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var answerVal = answerTheValue.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    status = CommonMethod.CompareArrayElement(standardVal, answerVal) ? AnswerResultStatus.Right : AnswerResultStatus.Error;

                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.AccountEntry:
                case QuestionType.GridFillBank:
                case QuestionType.FillBlank:
                case QuestionType.FillGrid:
                case QuestionType.FillGraphGrid:
                case QuestionType.FinancialStatements:
                case QuestionType.CertifMSub_AccountEntry:
                case QuestionType.ProfitLoss:
                case QuestionType.DeclarationTpoic:
                case QuestionType.FinancialStatementsDeclaration:
                    var standardJObj = JsonHelper.DeserializeObject<Answer>(standardAnswerVal);
                    var answerJObj = JsonHelper.DeserializeObject<Answer>(answerTheValue);
                    var answerCountInfo = AnswerCompareHelperV2.Compare(standardJObj, answerJObj, 500, 600);

                    //var caseSet = new SxCase();
                    //if (topic.QuestionType == QuestionType.AccountEntry)//分录题获取非完整性得分及分录题占比设置
                    //{
                    //    caseSet = DbContext.FreeSql.GetRepository<SxCase>().Where(s => s.Id == topic.CaseId).FirstAsync($"{CommonConstants.Cache_GetCaseById_C}{topic.CaseId}", true, 10 * 60, true).Result;
                    //}
                    var scale = topic.QuestionType == QuestionType.AccountEntry ? 0M : 0M;//TODO 分录题占比和计分方式等是否去掉了？
                    switch (topic.CalculationType)
                    {
                        case CalculationType.None:
                        case CalculationType.AvgCellCount:
                            var aroundScored = 0m;
                            int aroundVaildCount = answerCountInfo.HeaderCountInfo.ValidCount + answerCountInfo.FooterCountInfo.ValidCount;
                            int aroundRightCount = answerCountInfo.HeaderCountInfo.RightCount + answerCountInfo.FooterCountInfo.RightCount;

                            var otherValidCount = answerCountInfo.TotalCountInfo.ValidCount - aroundVaildCount;
                            var otherRightCount = answerCountInfo.TotalCountInfo.RightCount - aroundRightCount;
                            if (aroundVaildCount != 0)//只有分录题才有外围
                            {
                                if (aroundRightCount > 0 && otherValidCount > 0 && otherValidCount == otherRightCount)
                                {
                                    var amScore = topic.DisableAMScore;//会计主管分数
                                    var cashierScore = topic.DisableCashierScore;//出纳审核分数
                                    if (answer.AccountEntryStatus == AccountEntryStatus.AccountingManager)
                                    {
                                        aroundScored = amScore + cashierScore;
                                    }
                                    else if (answer.AccountEntryStatus == AccountEntryStatus.Cashier)
                                    {
                                        aroundScored = cashierScore;
                                    }
                                }
                            }

                            if (otherValidCount != 0)
                            {
                                answerScore = score / otherValidCount * otherRightCount;
                            }
                            answerScore = Math.Round(answerScore + aroundScored, 4, MidpointRounding.AwayFromZero);
                            break;
                        case CalculationType.FreeCalculation:
                            answerScore = answerCountInfo.TotalCountInfo.Scored;
                            break;
                        case CalculationType.CalculatedRow:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlRows, score);
                            break;
                        case CalculationType.CalculatedColumn:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlColumns, score);
                            break;
                    }
                    answerScore = GetNonholonomicScore(answerCountInfo, answerScore, scale);
                    status = answerCountInfo.TotalCountInfo.ValidCount == answerCountInfo.TotalCountInfo.RightCount ? AnswerResultStatus.Right : (answerCountInfo.TotalCountInfo.RightCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
                    examResultDto.AnswerCountInfo = answerCountInfo;
                    examResultDto.AnswerResultJson = HttpUtility.UrlEncode(JsonHelper.SerializeObject(answerJObj));
                    examResultDto.AnswerResult = status;
                    //if (examResultDto.AnswerResult == AnswerResultStatus.Right)
                    //{
                    //    examResultDto.AnswerScore = score;
                    //}
                    //else
                    //{
                    //    examResultDto.AnswerScore = answerScore;
                    //}

                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MainSubQuestion:
                    examResultDto = MainSubQuestionCompare(answer, examResultDto);

                    break;

            }
            UpdateScores(answer, examResultDto);
            return examResultDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        private static decimal CalcRowsScored(List<AnswerRow> rows, decimal score)
        {
            if (rows == null || rows.Count == 0)
                return 0;
            var columnTotalCount = (decimal)rows.Count;
            //2019-9-7
            //var rigthColumnCount = rows.Count(m => m.Items.All(i => i.IsRight && i.IsValid));
            var rigthColumnCount = (decimal)rows.Count(m => m.Items.All(i => i.IsRight || !i.IsValid) && m.Items.Any(i => i.IsRight));
            return Math.Round(rigthColumnCount / columnTotalCount * score, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 获取非完整得分
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="scored"></param>
        /// <param name="accountEntryScale"></param>
        /// <param name="fillGridScale"></param>
        /// <returns></returns>
        private static decimal GetNonholonomicScore(AnswerCountInfo answerCountInfo, decimal scored, decimal scale)
        {
            if (scale == 0)
                return scored;
            if (answerCountInfo.TotalCountInfo.ValidCount != answerCountInfo.TotalCountInfo.RightCount)//非完整性得分判断
            {
                return Math.Round(scored * scale / 100m, 4, MidpointRounding.AwayFromZero);
            }
            return Math.Round(scored, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 多题型答案比较
        /// </summary>
        /// <param name="examResultDto"></param>
        private QuestionResult MainSubQuestionCompare(QuestionAnswer answer, QuestionResult examResultDto)
        {
            var allItemCount = 0;
            var rightItemCount = 0;
            var answerScore = 0m;
            examResultDto.MultiQuestionResult = new List<QuestionResult>();
            if (answer.MultiQuestionAnswers == null || answer.MultiQuestionAnswers.Count == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }

            var multiTopic = DbContext.FreeSql.GetRepository<SxTopic>().Where(s => s.ParentId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubTopicByParentId_C}{answer.QuestionId}", true, 10 * 60, true).Result;

            foreach (var multiItem in multiTopic)
            {
                var answerItem = answer.MultiQuestionAnswers.FirstOrDefault(m => m.QuestionId == multiItem.Id);
                if (answerItem == null)
                    continue;
                var compareRst = CompareTopicAnswer(multiItem, answerItem);
                if (compareRst.AnswerCountInfo == null)
                {
                    if (compareRst.AnswerResult == AnswerResultStatus.Right)
                        rightItemCount++;
                    allItemCount++;
                }
                else
                {
                    allItemCount += compareRst.AnswerCountInfo.TotalCountInfo.ValidCount;
                    rightItemCount += compareRst.AnswerCountInfo.TotalCountInfo.RightCount;
                }
                examResultDto.MultiQuestionResult.Add(compareRst);
            }
            if (allItemCount == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }
            answerScore = examResultDto.MultiQuestionResult.Sum(s => s.AnswerScore);
            examResultDto.AnswerResult = allItemCount == rightItemCount ? AnswerResultStatus.Right : (rightItemCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
            examResultDto.AnswerScore = answerScore;
            return examResultDto;
        }


        private void UpdateScores(QuestionAnswer answer, QuestionResult examResultDto)
        {
            if (answer.IsIgnore)
            {
                return;
            }

            var gradeDetailId = answer.GradeDetailId;
            if (gradeDetailId > 0)
            {
                var gradeDetail = new SxExamUserGradeDetail() { Id = gradeDetailId };
                gradeDetail.Answer = answer.AnswerValue;
                gradeDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                gradeDetail.AccountEntryStatus = answer.AccountEntryStatus;
                gradeDetail.UpdateTime = DateTime.Now;
                gradeDetail.Status = examResultDto.AnswerResult;
                gradeDetail.Score = examResultDto.AnswerScore;
                gradeDetail.AnswerCompareInfo = examResultDto.AnswerResultJson;
                gradeDetail.SummaryInfos = answer.SummaryInfos;
                gradeDetail.AssistNames = answer.AssistNames;
                gradeDetail.ProfitLossUserAnswerValues = answer.ProfitLossUserAnswerValues;

                DataMergeHelper.PushData("UpdateScores-gradeDetail", gradeDetail, (arr) =>
                {
                    var details = arr.Cast<SxExamUserGradeDetail>().ToList();
                    DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().UpdateDiy.SetSource(details)
                  .UpdateColumns(a => new { a.Status, a.Score, a.AnswerCompareInfo, a.Answer, a.AccountEntryAuditStatus, a.AccountEntryStatus, a.UpdateTime, a.SummaryInfos, a.AssistNames, a.ProfitLossUserAnswerValues }).ExecuteAffrows();
                });
            }
        }

        #endregion

        #endregion

        #region 拒绝
        /// <summary>
        /// 新增拒绝记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddRefusalRecord(RefusalRecordDto model)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            var refusalRecord = model.MapTo<SxRefusalRecord>();
            refusalRecord.Id = IdWorker.NextId();
            await DbContext.FreeSql.Insert(refusalRecord).ExecuteAffrowsAsync();
            return result;
        }
        #endregion

        #region 申报题目记录草稿
        /// <summary>
        /// 申报题目记录草稿
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeclareSaveDrafts(UserDeclareDto dto, UserTicket user)
        {
            if (dto == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };
            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == dto.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };
            //查询申报主表
            SxDeclarationParent declarationParent = await DbContext.FreeSql.GetRepository<SxDeclarationParent>().Where(x => x.Id == dto.DeclareParentId).FirstAsync();
            if (declarationParent == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "申报主表不存在!" };
            if (declarationParent.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "申报主表已删除!" };

            //查看申报子表题目列表
            List<SxTopic> topicList = await DbContext.FreeSql.Select<SxTopic>().From<SxDeclarationChild>(
                (a, b) =>
                a.InnerJoin(x => x.DeclarationId == b.Id && x.ParentDeclarationId == b.ParentId))
                .Where((a, b) => a.ParentDeclarationId == dto.DeclareParentId && b.ParentId == dto.DeclareParentId && a.IsDelete == CommonConstants.IsNotDelete
                && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (topicList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有子题目,题目错误!" };

            //题目Ids
            List<long> topicIds = topicList.Select(x => x.Id).ToList();
            //题目作答明细列表
            List<SxExamUserGradeDetail> gardeDetails = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId
                && topicIds.Contains(x.QuestionId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //查询申报记录表
            SxDeclarationRecord declareRecord = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().Where(x => x.GradeId == dto.GradeId
                && x.DeclareParentId == dto.DeclareParentId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            bool state = false;

            //题目未做完
            if (topicList.Count > gardeDetails.Count)
            {
                if (declareRecord != null)
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "成功!" };

                declareRecord = new SxDeclarationRecord
                {
                    Id = IdWorker.NextId(),
                    GradeId = dto.GradeId,
                    DeclareParentId = dto.DeclareParentId,
                    DeclareStatus = (int)DeclareStatus.NotFilled,

                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now
                };

                SxDeclarationRecord returnData = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().InsertAsync(declareRecord);

                state = returnData != null;
            }

            //题目已做完
            if (topicList.Count == gardeDetails.Count)
            {
                if (declareRecord != null)
                {
                    declareRecord.DeclareStatus = (int)DeclareStatus.ToDeclare;
                    declareRecord.UpdateBy = user.Id;
                    declareRecord.UpdateTime = DateTime.Now;

                    state = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().UpdateDiy.SetSource(declareRecord).UpdateColumns(x => new
                    {
                        x.DeclareStatus,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync() > 0;
                }
                else
                {
                    declareRecord = new SxDeclarationRecord
                    {
                        Id = IdWorker.NextId(),
                        GradeId = dto.GradeId,
                        DeclareParentId = dto.DeclareParentId,
                        DeclareStatus = (int)DeclareStatus.ToDeclare,

                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now
                    };

                    SxDeclarationRecord returnData = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().InsertAsync(declareRecord);

                    state = returnData != null;
                }
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 用户申报
        /// <summary>
        /// 用户申报
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UserDeclare(UserDeclareDto dto, UserTicket user)
        {
            if (dto == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };
            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == dto.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };
            //查询申报主表
            SxDeclarationParent declarationParent = await DbContext.FreeSql.GetRepository<SxDeclarationParent>().Where(x => x.Id == dto.DeclareParentId).FirstAsync();
            if (declarationParent == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "申报主表不存在!" };
            if (declarationParent.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "申报主表已删除!" };

            //查看申报子表题目列表
            List<SxTopic> topicList = await DbContext.FreeSql.Select<SxTopic>().From<SxDeclarationChild>(
                (a, b) =>
                a.InnerJoin(x => x.DeclarationId == b.Id && x.ParentDeclarationId == b.ParentId))
                .Where((a, b) => a.ParentDeclarationId == dto.DeclareParentId && b.ParentId == dto.DeclareParentId && a.IsDelete == CommonConstants.IsNotDelete
                && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (topicList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有子题目,无法申报!" };

            //题目Ids
            List<long> topicIds = topicList.Select(x => x.Id).ToList();
            //题目作答明细列表
            List<SxExamUserGradeDetail> gardeDetails = await DbContext.FreeSql.Select<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId
                && topicIds.Contains(x.QuestionId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (topicList.Count > gardeDetails.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "子题目未做完,无法申报!" };
            if (topicList.Count < gardeDetails.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "作答有误,请联系管理员!" };

            //查询申报记录表
            SxDeclarationRecord declareRecord = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().Where(x => x.GradeId == dto.GradeId
                && x.DeclareParentId == dto.DeclareParentId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            bool state = false;

            if (declareRecord != null)
            {
                declareRecord.DeclareStatus = (int)DeclareStatus.HasDeclared;
                declareRecord.UpdateBy = user.Id;
                declareRecord.UpdateTime = DateTime.Now;

                state = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().UpdateDiy.SetSource(declareRecord).UpdateColumns(x => new
                {
                    x.DeclareStatus,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }
            else
            {
                declareRecord = new SxDeclarationRecord
                {
                    Id = IdWorker.NextId(),
                    GradeId = dto.GradeId,
                    DeclareParentId = dto.DeclareParentId,
                    DeclareStatus = (int)DeclareStatus.HasDeclared,

                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now
                };

                SxDeclarationRecord returnData = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().InsertAsync(declareRecord);

                state = returnData != null;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取其他任务列表
        /// <summary>
        /// 获取其他实习工作列表
        /// </summary>
        /// <param name="currtTaskId">用户Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetOtherTasksResponseDto>>> GetOtherTasks(long currtTaskId)
        {
            var list = await DbContext.FreeSql.GetRepository<SxCase>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(a => a.IsActive == true)
                   .From<SxTask, SxIndustry>((a, b, c) => a.InnerJoin(aa => aa.Id == b.CaseId && b.Id != currtTaskId).LeftJoin(aa => aa.IndustryId == c.Id))
                   .Limit(5)
                   .OrderBy((a, b, c) => a.Sort)
                   .OrderBy((a, b, c) => a.Id)
                   .OrderBy((a, b, c) => b.CreateTime)
                   .OrderBy((a, b, c) => b.Id)
                   .ToListAsync((a, b, c) => new GetOtherTasksResponseDto
                   {
                       CaseId = a.Id,
                       CaseName = a.Name,
                       Rate = a.Rate,
                       TaskId = b.Id,
                       TaskMonth = b.IssueDate.Month,
                       Integral = b.Integral,
                       TaskDescription = b.Description,
                       IndustryName = c.Name,
                   });

            return new ResponseContext<List<GetOtherTasksResponseDto>>(list);
        }
        #endregion

    }
}
