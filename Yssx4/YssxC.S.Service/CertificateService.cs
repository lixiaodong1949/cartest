﻿using Aop.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Subject;

namespace YssxC.S.Service
{
    public class CertificateService : ICertificateService
    {
        private const string StageBalance = "期初余额", StageTotal = "本期合计", YearStageTotal = "本年累计";

        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type, long taskId = 0)
        {
            List<SxSubject> yssxCoreSubjects = await GetSearchSubjects(id, subjectType, keyword);

            return await GetBalanceList(yssxCoreSubjects, gradeId, id, type, taskId);
        }

        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type, string dateStr, long taskId = 0)
        {
            List<SxSubject> yssxCoreSubjects = await GetSearchSubjects(id, 0, null); //await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id).ToListAsync($"{CommonConstants.Cache_GetCertificateListByCaseId}{id}", true, 10 * 60);
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                var enterprise = DbContext.FreeSql.Select<SxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                stageStr = "2020-01";//enterprise.AccountingPeriodDate;
            }
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.ErrorCode, Data = null, Msg = "案例没有设置会计期间!" };
            }
            List<CertificateDto> allCertificateDtos = GetAllSubsidiary(yssxCoreSubjects, gradeId, id, type, taskId, subjectId);
            var rst = GetSubjectLedgerSummer(yssxCoreSubjects, subjectId, allCertificateDtos, stageStr);
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = rst };

        }

        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr, long taskId = 0)
        {
            List<SxSubject> yssxCoreSubjects = await GetSearchSubjects(id, subjectType, keyword);
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                var enterprise = DbContext.FreeSql.Select<SxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                stageStr = "2020-01";//enterprise.AccountingPeriodDate;
            }

            List<SxSubject> p_list = yssxCoreSubjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            List<CertificateDto> allCertificateDtos = GetAllSubsidiary(yssxCoreSubjects, gradeId, id, type, taskId);

            var summaries = new[] { StageBalance, StageTotal, YearStageTotal };
            var subjects = new List<CertificateDto>();

            if (string.IsNullOrWhiteSpace(stageStr))
            {
                return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.ErrorCode, Data = null, Msg = "案例没有设置会计期间!" };
            }
            foreach (var rootItem in p_list)
            {
                var rst = GetSubjectLedgerSummer(yssxCoreSubjects, rootItem.Id, allCertificateDtos, stageStr);

                foreach (var dto in rst)
                {
                    if (!summaries.Contains(dto.SummaryInfo))
                        continue;
                    dto.Stage = stageStr;
                    subjects.Add(dto);
                }
            }
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = subjects };
        }

        #region 私有方法
        private void FindChildNode(long id, List<SxSubject> retList, List<SxSubject> enterpriseSubjects, long assistacId = 0)
        {
            if (id == 0) return;
            //List<SxSubject> yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToList();
            List<SxSubject> yssxCoreSubjects = new List<SxSubject>();
            if (assistacId == 0)
                yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToList();
            else
                yssxCoreSubjects = enterpriseSubjects.Where(x => x.AssistacParentId == assistacId).OrderBy(x => x.SubjectCode).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, retList, enterpriseSubjects, item.AssistacId);
            }
        }

        /// <summary>
        /// 获取科目余额数据列表
        /// </summary>
        /// <param name="yssxCoreSubjects"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="gradeId"></param>
        /// <param name="type">0：后端，1：前端</param>
        /// <returns></returns>
        private async Task<ResponseContext<List<CertificateBalanceDto>>> GetBalanceList(List<SxSubject> yssxCoreSubjects, long gradeId, long enterpriseId, int type, long taskId, long subjectId = 0, int isAssis = 0)
        {
            ResponseContext<List<CertificateBalanceDto>> response = new ResponseContext<List<CertificateBalanceDto>>();

            //科目余额数据集合
            List<CertificateBalanceDto> yssxSubjetBalances = new List<CertificateBalanceDto>();
            List<SxExamCertificateDataRecord> exam_certificateDataRecords = new List<SxExamCertificateDataRecord>();
            List<SxCertificateDataRecord> certificateDataRecords = new List<SxCertificateDataRecord>();
            if (type == 1)
            {
                exam_certificateDataRecords = await DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Where(x => x.CaseId == enterpriseId && x.GradeId == gradeId).ToListAsync();
            }
            else
            {
                certificateDataRecords = await DbContext.FreeSql.GetRepository<SxCertificateDataRecord>().Where(x => x.CaseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete && x.TaskId == taskId).ToListAsync();
            }

            foreach (var item in yssxCoreSubjects)
            {
                CertificateBalanceDto coreSubjetBalance = new CertificateBalanceDto();
                //coreSubjetBalance.CoreSubjectId = item.Id;
                coreSubjetBalance.Id = item.Id;// IdWorker.NextId();
                coreSubjetBalance.SubjectCode = item.SubjectCode;
                coreSubjetBalance.AssistacId = item.AssistacId;
                coreSubjetBalance.ParentId = item.ParentId;
                coreSubjetBalance.CaseId = item.CaseId;
                long id = item.AssistacId > 0 ? item.AssistacId : item.Id;
                var d = GetCertificateData(id, type, exam_certificateDataRecords, certificateDataRecords);
                coreSubjetBalance.CurrentBorrowAmount = d.Item1; //本期借方数据
                coreSubjetBalance.CurrentCreditorAmount = d.Item2;//本期贷方数据
                coreSubjetBalance.BorrowAmount = item.BorrowAmount;
                coreSubjetBalance.CreditorAmount = item.CreditorAmount;
                coreSubjetBalance.IsHaveCheck = item.IsHaveCheck;
                //计算期末数据
                double _finalAmount = ((double)item.BorrowAmount + coreSubjetBalance.CurrentBorrowAmount) - ((double)item.CreditorAmount + coreSubjetBalance.CurrentCreditorAmount);

                if (_finalAmount > 0)
                {
                    coreSubjetBalance.FinalBorrowAmount = _finalAmount;
                }
                else if (_finalAmount < 0)
                {
                    coreSubjetBalance.FinalCreditorAmount = -_finalAmount;
                }

                yssxSubjetBalances.Add(coreSubjetBalance);
            }

            //子级目录相加
            SetChildList(yssxSubjetBalances);

            ///最后数据设置
            List<SxSubject> retLis = new List<SxSubject>();
            if (subjectId == 0)
            {
                List<SxSubject> coreSubjects_temps = yssxCoreSubjects.ToList();

                List<SxSubject> subjects = yssxCoreSubjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();

                foreach (var item in subjects)
                {
                    List<SxSubject> list3 = coreSubjects_temps.Where(o => o.ParentId == item.Id).ToList();
                    if (list3.Count > 0)
                    {
                        item.BorrowAmount = list3.Sum(b => b.BorrowAmount);
                        item.CreditorAmount = list3.Sum(b => b.CreditorAmount);
                    }
                    retLis.Add(item);
                    FindChildNode(item.Id, retLis, yssxCoreSubjects, item.AssistacId);
                }
            }
            else
            {
                retLis = yssxCoreSubjects;
            }

            response.Data = SetLastList(retLis, yssxSubjetBalances, isAssis);
            response.Code = CommonConstants.SuccessCode;
            return response;
        }

        /// <summary>
        /// 设置子目录相加
        /// </summary>
        private void SetChildList(List<CertificateBalanceDto> yssxSubjetBalances)
        {
            List<CertificateBalanceDto> list1 = yssxSubjetBalances.Where(o => o.ParentId > 0).OrderByDescending(x => x.SubjectCode).ToList();
            //var index = list1.FindIndex(x => x.SubjectCode == 112502);
            list1.ForEach(x =>
            {
                List<CertificateBalanceDto> temps = x.AssistacId == 0 ? yssxSubjetBalances.Where(o => o.ParentId == x.Id).ToList() : new List<CertificateBalanceDto> { };

                if (temps.Count > 0)
                {
                    x.CurrentCreditorAmount = temps.Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = temps.Sum(c => c.CurrentBorrowAmount);
                    double _finalAmount = ((double)temps.Sum(c => c.BorrowAmount) + temps.Sum(c => c.CurrentBorrowAmount)) - ((double)temps.Sum(c => c.CreditorAmount) + temps.Sum(c => c.CurrentCreditorAmount));
                    if (_finalAmount > 0)
                    {
                        x.FinalBorrowAmount = _finalAmount;
                    }
                    else if (_finalAmount < 0)
                    {
                        x.FinalCreditorAmount = -_finalAmount;
                    }
                    else
                    {
                        x.FinalBorrowAmount = _finalAmount;
                        x.FinalCreditorAmount = _finalAmount;
                    }
                    //x.FinalBorrowAmount = temps.Sum(c => c.FinalBorrowAmount);
                    //x.FinalCreditorAmount = temps.Sum(c => c.FinalCreditorAmount);
                }
            });

            List<CertificateBalanceDto> list2 = yssxSubjetBalances.Where(o => o.ParentId == 0).ToList();
            list2.ForEach(x =>
            {
                List<CertificateBalanceDto> temps = list1.Where(o => o.ParentId == x.Id).ToList();

                if (temps.Count > 0)
                {
                    x.CurrentCreditorAmount = temps.Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = temps.Sum(c => c.CurrentBorrowAmount);
                    double _finalAmount = ((double)temps.Sum(c => c.BorrowAmount) + temps.Sum(c => c.CurrentBorrowAmount)) - ((double)temps.Sum(c => c.CreditorAmount) + temps.Sum(c => c.CurrentCreditorAmount));
                    if (_finalAmount > 0)
                    {
                        x.FinalBorrowAmount = _finalAmount;
                    }
                    else if (_finalAmount < 0)
                    {
                        x.FinalCreditorAmount = -_finalAmount;
                    }
                }
            });

            yssxSubjetBalances.ForEach(x =>
            {
                if (x.ParentId == 0)
                {
                    x.CurrentCreditorAmount = list2.Where(o => o.Id == x.Id).Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = list2.Where(o => o.Id == x.Id).Sum(c => c.CurrentBorrowAmount);
                }
                else if (x.AssistacId == 0)
                {
                    x.CurrentCreditorAmount = list1.Where(o => o.Id == x.Id && o.AssistacId == 0).Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = list1.Where(o => o.Id == x.Id && o.AssistacId == 0).Sum(c => c.CurrentBorrowAmount);
                }
                else
                {
                    x.CurrentCreditorAmount = list1.Where(o => o.AssistacId == x.AssistacId).Sum(c => c.CurrentCreditorAmount);
                    x.CurrentBorrowAmount = list1.Where(o => o.AssistacId == x.AssistacId).Sum(c => c.CurrentBorrowAmount);
                }
            });
        }

        /// <summary>
        /// 计算最后的结果
        /// </summary>
        /// <param name="retLis"></param>
        /// <param name="certificateBalances"></param>
        /// <returns></returns>
        private List<CertificateBalanceDto> SetLastList(List<SxSubject> retLis, List<CertificateBalanceDto> certificateBalances, int type = 0)
        {
            List<CertificateBalanceDto> _dtoList = DataToModel(retLis, type);

            _dtoList.ForEach(x =>
            {
                try
                {
                    CertificateBalanceDto yb = x.AssistacId == 0 ? certificateBalances.Where(o => o.Id == x.Id).FirstOrDefault() : certificateBalances.Where(o => o.AssistacId == x.AssistacId).FirstOrDefault();
                    if (yb != null)
                    {
                        x.CurrentBorrowAmount = yb.CurrentBorrowAmount;
                        x.CurrentCreditorAmount = yb.CurrentCreditorAmount;
                        x.FinalBorrowAmount = yb.FinalBorrowAmount;
                        x.FinalCreditorAmount = yb.FinalCreditorAmount;
                        x.CurrentBorrowAmountStr = yb.CurrentBorrowAmount == 0 ? null : yb.CurrentBorrowAmount.ToString("0.00");
                        x.CurrentCreditorAmountStr = yb.CurrentCreditorAmount == 0 ? null : yb.CurrentCreditorAmount.ToString("0.00");
                        if (yb.ParentId == 0)
                        {
                            double _finalAmount = ((double)x.BorrowAmount + yb.CurrentBorrowAmount) - ((double)x.CreditorAmount + yb.CurrentCreditorAmount);

                            if (_finalAmount > 0)
                            {
                                x.FinalBorrowAmount = _finalAmount;
                                x.FinalBorrowAmountStr = x.FinalBorrowAmount > 0 ? x.FinalBorrowAmount.ToString("0.00") : null;
                            }
                            else if (_finalAmount < 0)
                            {
                                x.FinalCreditorAmount = -_finalAmount;
                                x.FinalCreditorAmountStr = x.FinalCreditorAmount > 0 ? x.FinalCreditorAmount.ToString("0.00") : null;
                            }
                        }
                        else
                        {
                            x.FinalBorrowAmountStr = yb.FinalBorrowAmount > 0 ? yb.FinalBorrowAmount.ToString("0.00") : null;
                            x.FinalCreditorAmountStr = yb.FinalCreditorAmount > 0 ? yb.FinalCreditorAmount.ToString("0.00") : null;
                        }

                        if (x.BorrowAmount > 0) x.BorrowAmountStr = x.BorrowAmount.ToString("0.00");
                        else if (x.BorrowAmount < 0) x.BorrowAmountStr = x.BorrowAmount.ToString("0.00");

                        if (x.CreditorAmount > 0) x.CreditorAmountStr = x.CreditorAmount.ToString("0.00");
                        else if (x.CreditorAmount < 0) x.CreditorAmountStr = x.CreditorAmount.ToString("0.00");

                        //x.BorrowAmountStr = x.BorrowAmount > 0 ? x.BorrowAmount.ToString() :  null;
                        //x.CreditorAmountStr = x.CreditorAmount > 0 ? x.CreditorAmount.ToString() : null;
                    }
                }
                catch (Exception ex)
                {

                }


            });
            return _dtoList;
        }

        private List<CertificateBalanceDto> DataToModel(List<SxSubject> sources, int type)
        {
            if (null == sources) return null;
            List<CertificateBalanceDto> list = new List<CertificateBalanceDto>();
            if (type == 0)
            {
                list = sources.Where(x => x.AssistacId == 0).Select(x => new CertificateBalanceDto
                {
                    Id = x.Id,
                    BorrowAmount = x.BorrowAmount,
                    CreditorAmount = x.CreditorAmount,
                    CaseId = x.CaseId,
                    ParentId = x.ParentId,
                    AssistacId = x.AssistacId,
                    SubjectCode = x.SubjectCode,
                    SubjectName = x.SubjectName,
                    SubjectType = x.SubjectType,
                    IsHaveCheck = x.IsHaveCheck,
                }).ToList();
            }
            else
            {
                list = sources.Select(x => new CertificateBalanceDto
                {
                    Id = x.Id,
                    BorrowAmount = x.BorrowAmount,
                    CreditorAmount = x.CreditorAmount,
                    CaseId = x.CaseId,
                    ParentId = x.ParentId,
                    AssistacId = x.AssistacId,
                    SubjectCode = x.SubjectCode,
                    SubjectName = x.SubjectName,
                    SubjectType = x.SubjectType,
                    IsHaveCheck = x.IsHaveCheck,
                }).ToList();
            }

            return list;
        }

        private Tuple<double, double> GetCertificateData(long id, int type, List<SxExamCertificateDataRecord> exam_certificateDataRecords, List<SxCertificateDataRecord> certificateDataRecords)
        {
            double temp_cBorrowAmount = 0, temp_cCreditorAmount = 0;
            if (type == 1)
            {
                List<SxExamCertificateDataRecord> _dataRecords = exam_certificateDataRecords.Where(x => x.SubjectId == id).ToList();
                if (_dataRecords.Count > 0)
                {
                    temp_cBorrowAmount = (double)_dataRecords.Sum(x => x.BorrowAmount);//本期借方数据
                    temp_cCreditorAmount = (double)_dataRecords.Sum(x => x.CreditorAmount);//本期贷方数据
                }
            }
            else
            {
                List<SxCertificateDataRecord> temp_list = certificateDataRecords.Where(x => x.SubjectId == id).ToList();
                if (temp_list.Count > 0)
                {
                    temp_cBorrowAmount = (double)temp_list.Sum(x => x.BorrowAmount);//本期借方数据
                    temp_cCreditorAmount = (double)temp_list.Sum(x => x.CreditorAmount);//本期贷方数据
                }
            }

            return new Tuple<double, double>(temp_cBorrowAmount, temp_cCreditorAmount);
        }

        public ResponseContext<List<CertificateDto>> GetGeneralLedger2(long gradeId, long enterpriseId, int type = 0)
        {

            var rootItems = DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete && x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            var enterprise = DbContext.FreeSql.Select<SxCase>().Where(x => x.Id == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete).First();
            var summaries = new[] { StageBalance, StageTotal, YearStageTotal };
            var subjects = new List<CertificateDto>();
            var stageStr = "2020-01";//enterprise.AccountingPeriodDate;//没有默认我会计期间

            var allSubsidiaryLedgerDtos = GetAllSubsidiary(rootItems, gradeId, enterpriseId, type, 0);
            var allEnterpriseSubjects = DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            foreach (var rootItem in rootItems)
            {
                var rst = GetSubjectLedgerSummer(allEnterpriseSubjects, rootItem.Id, allSubsidiaryLedgerDtos, stageStr);

                foreach (var dto in rst)
                {
                    dto.SubjectType = rootItem.SubjectType;
                    dto.Stage = stageStr;
                    subjects.Add(dto);
                }

            }
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = subjects };
        }

        public List<CertificateDto> GetSubjectLedgerSummer(List<SxSubject> allEnterpriseSubjects, long subjectId, List<CertificateDto> allSubsidiaryLedgerDtos, string stageStr, long assistacId = 0)
        {
            List<CertificateDto> respones = new List<CertificateDto>();

            SxSubject rootItem = allEnterpriseSubjects.FirstOrDefault(m => m.Id == subjectId);

            List<SxSubject> list = allEnterpriseSubjects.Where(x => x.ParentId == subjectId).ToList();
            if (assistacId > 0)
            {
                rootItem = allEnterpriseSubjects.FirstOrDefault(m => m.AssistacId == assistacId);
                list = allEnterpriseSubjects.Where(x => x.AssistacParentId == assistacId).ToList();
            }
            //期初余额
            CertificateDto rootCoreSubject = new CertificateDto
            {
                BorrowAmount = rootItem.BorrowAmount,
                SubjectName = rootItem.SubjectName,
                SubjectType = rootItem.SubjectType,
                SubjectCode = rootItem.SubjectCode,
                Direction = rootItem.Direction,
                ParentSubjectId = rootItem.ParentId,
                CreditorAmount = rootItem.CreditorAmount,
            };// rootItem.MapTo<SubsidiaryLedgerDto>();
            if (list.Count == 0)
            {
                list.Add(rootItem);
            }
            DateTime rootDate = DateTime.Parse($"{stageStr}-01");
            rootCoreSubject.CertificateNo = "0";
            rootCoreSubject.SubjectId = rootItem.Id;
            rootCoreSubject.SummaryInfo = StageBalance;
            string direction = rootItem.Direction;
            rootCoreSubject.DateStr = $"{stageStr}-01";
            if (rootItem.Direction == "贷")
            {
                rootCoreSubject.Balance = list.Sum(x => x.CreditorAmount) != 0 ? list.Sum(x => x.CreditorAmount) : -list.Sum(x => x.BorrowAmount);
            }
            else
            {
                rootCoreSubject.Balance = list.Sum(x => x.CreditorAmount) != 0 ? -list.Sum(x => x.CreditorAmount) : list.Sum(x => x.BorrowAmount);
            }
            if (rootItem.CreditorAmount == 0 && rootItem.BorrowAmount == 0)
            {
                rootCoreSubject.Direction = "平";
            }
            rootCoreSubject.BorrowAmount = 0;
            rootCoreSubject.CreditorAmount = 0;
            rootCoreSubject.CreditorAmountStr = "";
            rootCoreSubject.BorrowAmountStr = "";
            var coreSubjects = new List<CertificateDto>
            {
                rootCoreSubject
            };
            List<SxSubject> c_list = new List<SxSubject>();
            c_list.Add(new SxSubject { Id = subjectId });
            FindChildNode(subjectId, c_list, allEnterpriseSubjects, assistacId);
            List<CertificateDto> t_list = new List<CertificateDto>();
            if (c_list.Count > 1)
            {
                c_list = c_list.Where(x => x.AssistacId == 0).ToList();
                List<CertificateDto> temps = new List<CertificateDto>();
                foreach (var item in c_list)
                {

                    List<CertificateDto> datas = allSubsidiaryLedgerDtos.Where(o => o.ParentSubjectId == item.Id).ToList();
                    if (datas.Count > 0)
                    {
                        temps.AddRange(datas);
                    }

                }

                temps = temps.OrderBy(x => x.CertificateDate).ThenBy(x => x.CertificateWord).ThenBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();
                foreach (var item2 in temps)
                {
                    string c_date = item2.CertificateDate.ToString("yyyy-MM-dd");
                    if (c_date != "0001-01-01")
                    {
                        item2.DateStr = c_date;
                    }
                    item2.CreditorAmountStr = item2.CreditorAmount.ToString();
                    item2.BorrowAmountStr = item2.BorrowAmount.ToString();
                    coreSubjects.Add(item2);
                    var tuple = GetSubjectBalance(coreSubjects);
                    item2.Direction = tuple.Item2 == 0 ? "平" : item2.Direction;
                    item2.Balance = tuple.Item2;
                    t_list.Add(item2);
                }
            }
            else
            {
                var temps = allSubsidiaryLedgerDtos.Where(o => o.SubjectId == subjectId).OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();

                foreach (var item2 in temps)
                {
                    string c_date = item2.CertificateDate.ToString("yyyy-MM-dd");
                    if (c_date != "0001-01-01")
                    {
                        item2.DateStr = c_date;
                    }
                    item2.CreditorAmountStr = item2.CreditorAmount.ToString();
                    item2.BorrowAmountStr = item2.BorrowAmount.ToString();
                    coreSubjects.Add(item2);
                    var tuple = GetSubjectBalance(coreSubjects);
                    item2.Direction = tuple.Item2 == 0 ? "平" : item2.Direction;
                    item2.Balance = tuple.Item2;
                    t_list.Add(item2);
                }
            }
            string dirction = "";
            decimal _balance = 0;
            if (coreSubjects.Count == 1)
            {
                dirction = coreSubjects[0].Direction;
                _balance = coreSubjects[0].Balance;
            }
            else
            {
                int len = coreSubjects.Count;
                var balance = coreSubjects[len - 1];
                dirction = balance.Direction;
                _balance = balance.Balance;
            }

            coreSubjects = coreSubjects.OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();
            decimal c = t_list.Sum(x => x.CreditorAmount);
            decimal b = t_list.Sum(x => x.BorrowAmount);
            var currentIssue = new CertificateDto  //本期合计
            {
                DateStr = rootDate.GetMonthLastDay().ToString("yyyy-MM-dd"),
                SubjectCode = rootItem.SubjectCode,
                CertificateNo = "",
                SubjectName = rootCoreSubject.SubjectName,
                SummaryInfo = StageTotal,
                Direction = dirction,
                Balance = _balance,
                BorrowAmount = b,
                CreditorAmount = c,
                CertificateDate = rootDate,
                CreditorAmountStr = c.ToString(),
                BorrowAmountStr = b.ToString(),
            };
            coreSubjects.Add(currentIssue);

            var currentYear = new CertificateDto //本年累计
            {
                DateStr = rootDate.GetMonthLastDay().ToString("yyyy-MM-dd"),
                SubjectCode = rootItem.SubjectCode,
                SubjectName = rootCoreSubject.SubjectName,
                SummaryInfo = YearStageTotal,
                CertificateNo = "",
                Direction = dirction,
                Balance = _balance,
                BorrowAmount = b,
                CreditorAmount = c,
                CertificateDate = rootDate,
                CreditorAmountStr = c.ToString(),
                BorrowAmountStr = b.ToString(),
                //CreditorAmount = balance.Item4
            };
            coreSubjects.Add(currentYear);
            coreSubjects.FirstOrDefault(x => x.CertificateNo == "0").CertificateNo = "";
            respones = coreSubjects;
            return respones;
        }



        /// <summary>
        /// 获取所有期数数据
        /// </summary>
        /// <returns></returns>
        private List<CertificateDto> GetAllSubsidiary(List<SxSubject> sxSubjects, long gradeId, long enterpriseId, int type, long taskId, long subjectId = 0)
        {
            string sql = string.Empty;
            if (type == 1)
            {
                //sql = DbContext.FreeSql.Select<SxExamCertificateDataRecord>().LeftJoin<SxSubject>((a, b) => a.SubjectId == b.Id && a.GradeId == gradeId
                //).Where(a => a.CaseId == enterpriseId && a.GradeId == gradeId).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,b.Id SubjectId,b.SubjectCode,b.SubjectName,b.Direction,b.ParentId ParentSubjectId");

                sql = DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Where(a => a.CaseId == enterpriseId && a.GradeId == gradeId).ToSql("a.CertificateNo,a.QuestionId,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,a.SubjectId,a.CertificateName CertificateWord");
            }
            else
            {
                //sql = DbContext.FreeSql.Select<SxCertificateDataRecord>().LeftJoin<SxSubject>((a, b) => a.SubjectId == b.Id).Where(x => x.CaseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete && x.TaskId == taskId).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,b.Id SubjectId,b.SubjectCode,b.SubjectName,b.Direction,b.ParentId ParentSubjectId");
                sql = DbContext.FreeSql.GetRepository<SxCertificateDataRecord>().Where(x => x.CaseId == enterpriseId && x.IsDelete == CommonConstants.IsNotDelete && x.TaskId == taskId).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,a.SubjectId,a.CertificateWord");
            }

            var allSubsidiaryLedgerDtos = DbContext.FreeSql.Ado.Query<CertificateDto>(sql).OrderBy(x => x.CertificateNo).ToList();
            List<CertificateDto> datas = new List<CertificateDto>();
            allSubsidiaryLedgerDtos.ForEach(x =>
            {
                if (string.IsNullOrWhiteSpace(x.CertificateNo))
                {
                    x.CertificateNo = "0";
                }
                else
                {
                    x.CertificateNo = $"{x.CertificateWord}-{x.CertificateNo}";
                }
                SxSubject sx = sxSubjects.FirstOrDefault(o => o.Id == x.SubjectId || o.AssistacId == x.SubjectId);
                if (null != sx)
                {
                    x.SubjectName = sx.SubjectName;
                    if (sx.AssistacId > 0)
                    {
                        x.SubjectName = sxSubjects.FirstOrDefault(k => k.Id == sx.Id).SubjectName;
                    }

                    x.ParentSubjectId = sx.ParentId;
                    x.Direction = sx.Direction;
                }
            });
            //if (subjectId > 0)
            //{
            //    datas = allSubsidiaryLedgerDtos.Where(x => x.ParentSubjectId == subjectId || x.SubjectId == subjectId).ToList();
            //}
            //else
            //{
            //    datas = allSubsidiaryLedgerDtos;
            //}
            datas = allSubsidiaryLedgerDtos;
            return datas;
        }

        private async Task<List<CertificateDto>> GetAssistacDataRecord(long caseId, long gradeId, long assisId)
        {
            List<CertificateDto> list = new List<CertificateDto>();
            string sql = DbContext.FreeSql.GetRepository<SxExamCertificateDataRecord>().Select.From<SxAssistAccounting>((a, b) => a.LeftJoin(x => x.AssistId == b.Id)).Where((a, b) => a.CaseId == caseId && a.GradeId == gradeId && a.SubjectId == assisId && b.IsDelete == CommonConstants.IsNotDelete).ToSql("a.CertificateNo,a.SummaryInfo,a.BorrowAmount,a.CreditorAmount,a.CertificateDate,a.QuestionId,b.SubjectId,b.SubjectCode,b.Name,b.ParentId ParentSubjectId");
            list = await DbContext.FreeSql.Ado.QueryAsync<CertificateDto>(sql);
            list = list.OrderBy(x => x.CertificateNo).ToList();
            return list;
        }



        private Tuple<string, decimal, decimal, decimal> GetSubjectBalance(List<CertificateDto> coreSubjects)
        {
            var sumBorrowAmount = 0m;
            var sumCreditorAmount = 0m;
            int length = coreSubjects.Count;
            CertificateDto tempData = coreSubjects[0];
            decimal blance = 0;
            int index = 0;
            coreSubjects = coreSubjects.OrderBy(x => int.Parse(Regex.Match(x.CertificateNo, "\\d+").Value)).ToList();
            foreach (var item in coreSubjects)
            {
                sumBorrowAmount += item.BorrowAmount;
                sumCreditorAmount += item.CreditorAmount;

                if (index == 0) { index++; continue; }

                if (tempData.Direction == "借" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "借" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "平" && item.Direction == "借")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;

                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;

                    }
                }
                else if (tempData.Direction == "平" && item.Direction == "贷")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;
                    }
                }
                else if (tempData.Direction == "贷" && item.Direction == "平")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance - item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance + item.CreditorAmount;
                    }
                }
                else if (tempData.Direction == "借" && item.Direction == "平")
                {
                    if (item.BorrowAmount != 0)
                    {
                        blance = tempData.Balance + item.BorrowAmount;
                    }
                    else if (item.CreditorAmount != 0)
                    {
                        blance = tempData.Balance - item.CreditorAmount;
                    }
                }

                item.Balance = blance;
                tempData = item;

            }

            return new Tuple<string, decimal, decimal, decimal>(blance > 0 ? "借" : "贷", blance, sumBorrowAmount, sumCreditorAmount);
        }


        private async Task<List<SxSubject>> GetSearchSubjects(long id, int subjectType, string keyword)
        {
            List<SxSubject> yssxCoreSubjects = new List<SxSubject>();
            List<SxSubject> assistacSubjects = new List<SxSubject>();
            if (subjectType == 0 && string.IsNullOrWhiteSpace(keyword))
            {
                yssxCoreSubjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            else
            {
                if (string.IsNullOrWhiteSpace(keyword))
                {
                    yssxCoreSubjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.SubjectType == subjectType && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                }
                else
                {
                    long code = 0;
                    if (long.TryParse(keyword, out code))
                    {
                        List<SxSubject> temps = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.SubjectType == subjectType && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                        List<SxSubject> retLis = new List<SxSubject>();
                        SxSubject subject = temps.FirstOrDefault(x => x.Id == code);

                        List<SxSubject> list3 = temps.Where(o => o.ParentId == subject.Id).ToList();
                        if (list3.Count > 0)
                        {
                            subject.BorrowAmount = list3.Sum(b => b.BorrowAmount);
                            subject.CreditorAmount = list3.Sum(b => b.CreditorAmount);
                        }
                        retLis.Add(subject);
                        FindChildNode(subject.Id, retLis, temps, subject.AssistacId);
                        yssxCoreSubjects = retLis;

                    }
                    else
                    {
                        yssxCoreSubjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.SubjectName.Contains(keyword) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                    }
                }
            }
            assistacSubjects = await GetAssistacCountList(yssxCoreSubjects, id, subjectType);
            yssxCoreSubjects.AddRange(assistacSubjects);
            return yssxCoreSubjects;
        }

        private async Task<List<SxSubject>> GetAssistacCountList(List<SxSubject> sxSubjects, long caseId, int type)
        {
            List<SxAssistAccounting> list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.CaseId == caseId && x.IsDelete == CommonConstants.IsNotDelete && x.SubjectId > 0).ToListAsync();

            List<SxSubject> subjects = list.Select(x => new SxSubject
            {
                Id = x.SubjectId != null ? (long)x.SubjectId : 0,
                ParentId = (long)x.SubjectId,
                AssistacParentId = x.ParentId,
                SubjectName = x.Name,
                AssistacId = x.Id,
                SubjectType = type,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount,

            }).ToList();
            List<SxSubject> list2 = new List<SxSubject>();
            foreach (var x in subjects)
            {
                SxSubject sx = sxSubjects.FirstOrDefault(o => o.Id == x.Id);
                if (sx != null)
                {
                    x.Direction = sx.Direction;
                    x.SubjectName = sx.SubjectName;
                    list2.Add(x);
                }
            }
            //subjects.ForEach(x =>
            //{
            //    SxSubject sx = sxSubjects.FirstOrDefault(o => o.Id == x.Id);
            //    if (sx != null)
            //    {
            //        x.Direction = sx.Direction;
            //        x.SubjectName = sx.SubjectName;
            //    }
            //});
            return list2;
        }

        public async Task<ResponseContext<List<CertificateDto>>> GetAssistacDetailDataList(long id, long gradeId, string dateStr, long subjectId, string assisName, int isHaveCheck)
        {
            List<CertificateDto> rst = new List<CertificateDto>();
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                stageStr = "2020-01";
            }

            if (isHaveCheck == 1)
            {

                List<SxSubject> sxSubjects = await GetSxSubjectsForAssisac(id, -1);

                SxSubject subject = sxSubjects.FirstOrDefault(x => x.SubjectName == assisName && x.Id == subjectId);
                if (subject == null) return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.ErrorCode, Msg = "没有找到数据!" };
                List<CertificateDto> allCertificateDtos = await GetAssistacDataRecord(id, gradeId, subject.AssistacId);
                rst = GetSubjectLedgerSummer(sxSubjects, subject.Id, allCertificateDtos, stageStr, subject.AssistacId);
            }
            else
            {
                ResponseContext<List<CertificateDto>> response = await GetCertificateDetailDataList(id, subjectId, gradeId, 1, dateStr);
                rst = response.Data;
            }
            return new ResponseContext<List<CertificateDto>> { Code = CommonConstants.SuccessCode, Data = rst };
        }

        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetAssistacSummerDataList(long id, long gradeId, string dateStr, int assistType)
        {
            List<SxSubject> yssxCoreSubjects = await GetSxSubjectsForAssisac(id, assistType);
            string stageStr = dateStr;
            if (string.IsNullOrWhiteSpace(stageStr))
            {
                stageStr = "2020-01";//enterprise.AccountingPeriodDate;
            }

            List<SxSubject> p_list = yssxCoreSubjects.Where(x => x.AssistacParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            //List<CertificateDto> allCertificateDtos = await GetAssistacDataRecord(id, gradeId);

            //var summaries = new[] { StageBalance, StageTotal, YearStageTotal };
            var subjects = new List<CertificateBalanceDto>();
            return await GetBalanceList(yssxCoreSubjects, gradeId, id, 1, 0, 1);

        }

        private async Task<List<SxSubject>> GetSxSubjectsForAssisac(long caseId, int assistType)
        {
            List<AssistacCertificateDto> yssxCoreSubjects = new List<AssistacCertificateDto>();
            yssxCoreSubjects = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Select.From<SxSubject>((a, b) => a.InnerJoin(x => x.SubjectId == b.Id)).Where((a, b) => a.CaseId == caseId && a.SubjectId > 0 && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new AssistacCertificateDto
            {
                SubjectId = b.Id,
                Name = a.Name,
                BorrowAmount = a.BorrowAmount,
                CreditorAmount = a.CreditorAmount,
                AssistacId = a.Id,
                ParentSubjectId = a.ParentId,
                AssistType = a.Type,
                Direction = b.Direction,
            });
            if (assistType > -1)
            {
                yssxCoreSubjects = yssxCoreSubjects.Where(x => x.AssistType == (AssistAccountingType)assistType).ToList();
            }


            List<SxSubject> sxSubjects = yssxCoreSubjects.Select(x => new SxSubject
            {
                Id = x.SubjectId,
                AssistacParentId = x.ParentSubjectId,
                SubjectName = x.Name,
                Direction = x.Direction,
                AssistacId = x.AssistacId,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount
            }).ToList();

            return sxSubjects;
        }

        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceById(long id, int subjectType, long subjectId, long gradeId, string assisName)
        {
            List<SxSubject> subjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete && x.SubjectType == subjectType).ToListAsync();


            List<SxAssistAccounting> list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.Name == assisName && x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            List<SxSubject> subjects2 = list.Select(x => new SxSubject
            {
                Id = x.SubjectId != null ? (long)x.SubjectId : 0,
                ParentId = (long)x.SubjectId,
                AssistacParentId = x.ParentId,
                SubjectName = x.Name,
                AssistacId = x.Id,
                SubjectType = subjectType,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount,

            }).ToList();
            subjects2.ForEach(x =>
            {
                SxSubject sx = subjects.FirstOrDefault(o => o.Id == x.Id);
                if (sx != null)
                {
                    x.Direction = sx.Direction;
                    x.SubjectName = sx.SubjectName;
                }
            });
            subjects.AddRange(subjects2);

            List<SxSubject> retList = new List<SxSubject>();
            retList.Add(subjects.Find(x => x.Id == subjectId && x.AssistacId == 0));
            FindChildNode(subjectId, retList, subjects);
            return await GetBalanceList(retList, gradeId, id, 1, 0, subjectId);
        }
        #endregion
    }
}
