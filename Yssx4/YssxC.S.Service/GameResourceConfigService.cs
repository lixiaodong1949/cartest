using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;

namespace YssxC.S.Service
{
    /// <summary>
    /// 游戏资源设置服务
    /// </summary>
    public class GameResourceConfigService : IGameResourceConfigService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> SaveGameResourceConfig(GameResourceConfigDto model, long currentUserId)
        {

            return null;
            //}
            //else //修改
            //{
            //    return await Update(model, currentUserId);
            //}
        }

        /// <summary>
        /// 游戏资源设置 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public static async Task<ResponseContext<bool>> SetGameResourceConfig(List<GameResourceConfigDto> models, long caseId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
            if (models == null)
            {
                result.SetSuccess();
                return result;
            }
            var repo = DbContext.FreeSql.GetRepository<SxGameResourceConfig>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == caseId);
            var keys = models.Select(e => e.GameResourceKey).ToArray();

            var configs = await repo.Where(e => keys.Contains(e.GameResourceKey)).ToListAsync();

            foreach (var model in models)
            {
                var config = configs.Where(e => e.GameResourceKey == model.GameResourceKey).FirstOrDefault();

                if (config == null)
                {
                    config = model.MapTo<SxGameResourceConfig>();
                    config.Id = IdWorker.NextId();//获取唯一Id
                    config.KeyName = nameof(model.GameResourceKey);
                    config.CaseId = caseId;
                }
                else
                {
                    config.Value = model.Value;
                }

                #region 保存到数据库中

                await repo.InsertOrUpdateAsync(config);


                #endregion
            }
            result.SetSuccess();
            return result;
        }

        /// <summary>
        /// 游戏资源设置 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        //private async Task<ResponseContext<bool>> Update(GameResourceConfigDto model, long currentUserId)
        //{
        //    var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

        //    var repo = DbContext.FreeSql.GetRepository<SxGameResourceConfig>(e => e.IsDelete == CommonConstants.IsNotDelete);

        //    #region 参数校验
        //    var hasobj = await repo
        //           .Where(e => e.Id == model.Id)
        //           .AnyAsync();

        //    if (!hasobj)
        //        return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏资源设置不存在，Id:{model.Id}", false);

        //    var hasName = await repo
        //                 .Where(e => e.Name == model.Name && e.Id != model.Id)
        //                 .AnyAsync();

        //    if (hasName)
        //        return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏资源设置名称重复。", false);

        //    #endregion
        //    //赋值
        //    var updateObj = model.MapTo<SxGameResourceConfig>();

        //    #region 保存到数据库中

        //    await repo.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

        //    //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

        //    //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
        //    //{
        //    //    try
        //    //    {
        //    //        var repo_f = uow.GetRepository<SxGameResourceConfig>();

        //    //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

        //    //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

        //    //        uow.Commit();
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        uow.Rollback();
        //    //        CommonLogger.Error(ex.ToString());//写错误日志
        //    //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
        //    //    }
        //    //}
        //    #endregion

        //    result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

        //    return result;
        //}

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> DeleteGameResourceConfig(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxGameResourceConfig>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxGameResourceConfig
                {
                    IsDelete = CommonConstants.IsDelete,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<SxGameResourceConfig>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new SxGameResourceConfig
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }


        /// <summary>
        /// 获取案例游戏资源配置
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <returns></returns>
        public async Task<ListResponse<GameResourceConfigResponseDto>> GetGameResourceConfigList(long caseId)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxGameResourceConfig>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == caseId)
                   .ToListAsync<GameResourceConfigResponseDto>();

            return new ListResponse<GameResourceConfigResponseDto>(entitys);
        }
    }
}