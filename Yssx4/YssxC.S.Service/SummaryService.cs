﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class SummaryService : ISummaryService
    {
      

        public async Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long id)
        {
            var data = await DbContext.FreeSql.GetRepository<SxAbstract>().Where(x => x.CaseId == id && x.IsDelete==CommonConstants.IsNotDelete).ToListAsync();
            return new ResponseContext<List<SummaryDto>> { Data = DataToModel(data) };
        }

       

        #region 私有方法
        private List<SummaryDto> DataToModel(List<SxAbstract> sources)
        {
            if (sources == null) return null;
            return sources.Select(x => new SummaryDto { Id = x.Id, CaseId = x.CaseId, Name = x.Name, Sort = x.Sort }).OrderBy(x => x.Sort).ToList();
        }
        #endregion
    }
}
