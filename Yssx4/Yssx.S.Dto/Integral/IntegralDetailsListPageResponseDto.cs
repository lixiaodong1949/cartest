using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取列表 返回值Dto
    /// </summary>
    public class IntegralDetailsListPageResponseDto
    {

        public long UserId { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }


        public long Number { get; set; }

        /// <summary>
        /// 状态：1：收入，2消费
        /// </summary>
        public int StatusType { get; set; }

        /// <summary>
        /// 来源 1:登录，2:做题，3：分享，4：阅读
        /// </summary>
        public int FromType { get; set; }

        /// <summary>
        /// 收入和消费描述
        /// </summary>
        public string Desc { get; set; }

        /// <summary>
        /// 关联Id
        /// </summary>
        public long RelatedId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }
        public string RealName { get; set; }


        /// <summary>
        /// 当前总分
        /// </summary>
        public long IntegralSum { get; set; }
    }
}