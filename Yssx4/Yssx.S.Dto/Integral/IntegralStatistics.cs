﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class IntegralStatistics
    {
        ///// <summary>
        ///// 积分总分
        ///// </summary>
        //public long IntegralSum { get; set; }
        /// <summary>
        /// 商品兑换总次数
        /// </summary>
        public int ProductExchangeSum { get; set; }
        /// <summary>
        /// 总共已使用的积分
        /// </summary>
        public long UsedIntegralSum { get; set; }

        /// <summary>
        /// 总可用积分
        /// </summary>
        public long UsableIntegralSum { get; set; }
        
    }
}
