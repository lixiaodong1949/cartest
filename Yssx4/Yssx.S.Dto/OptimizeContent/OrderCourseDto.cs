﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程
    /// </summary>
    public class OrderCourseDto
    {
        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 学历对应课程集合
        /// </summary>
        public List<YssxCourseListDto> CourseList { get; set; }
    }

}
