﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 章节列表dto
    /// </summary>
    public class SectionListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 章节标题
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 0:章，1：节
        /// </summary>
        public int SectionType { get; set; }
        ///// <summary>
        ///// 章节对应附件集合
        ///// </summary>
        //public List<SectionFileDto> SectionFiles { get; set; }
        public string Title { get { return this.SectionTitle; } }
        public int Level { get; set; }

        public bool ShowChildren { get; set; }=false;

        public bool showTitleInput { get; set; } = false;
        /// <summary>
        /// 子集
        /// </summary>
        public List<SectionListDto> Children { get; set; }
    }
    /// <summary>
    /// 附件Dto
    /// </summary>
    public class SectionFileDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件类型0:Excel,1:Word,2:PDF,3:PPT,4:Img,5:ZIP,6:视频
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 1：课件，2：教案
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CourseFileId { get; set; }
        /// <summary>
        /// 来源0：课件库.1:新增课件
        /// </summary>
        public int FromType { get; set; }
    }
}
