﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程的教材dto
    /// </summary>
    public class CourseFileListNewDto
    {
        /// <summary>
        /// 课程的教材列表
        /// </summary>
        public List<YssxSectionFilesDto> CourseFileList { get; set; }
    }
}
