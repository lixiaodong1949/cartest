﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 章节下面的文件
    /// </summary>
    public class SectionFileListNewDto
    {
        /// <summary>
        /// 课件
        /// </summary>
        public List<YssxSectionFilesDto> CoursewareList { get; set; }


        /// <summary>
        /// 视频
        /// </summary>
        public List<YssxSectionFilesDto> VideoList { get; set; }

        /// <summary>
        /// 教案
        /// </summary>
        public List<YssxSectionFilesDto> TeachingPlanList { get; set; }
        /// <summary>
        /// 思政案例
        /// </summary>
        public List<YssxSectionFilesDto> SZCaseList { get; set; }
    }
}
