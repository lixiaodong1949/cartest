﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户登录认证信息
    /// </summary>
    public class UserAuthentication
    {
        /// <summary>
        /// 手机号码/学号/工号/QQ/WeChat
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 登录方式 0:PC 1:App
        /// </summary>
        public int LoginType { get; set; }
    }
}
