﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 已结束课程测验简答题详情查询实体
    /// </summary>
    public class CourseTestShortAnswerFinishedQuery
    {
        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 班级主键Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }

        ///// <summary>
        ///// 作答状态 1:已作答 2:未作答
        ///// </summary>
        //public Nullable<int> AnswerStatus { get; set; }
    }
}
