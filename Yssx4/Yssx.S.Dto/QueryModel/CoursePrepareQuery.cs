﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课查询实体
    /// </summary>
    public class CoursePrepareQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 备课名称
        /// </summary>
        public string CoursePrepareName { get; set; }

    }
}
