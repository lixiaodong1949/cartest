﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程文件资源查询实体
    /// </summary>
    public class CourseFilesResourceQuery : PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 章节Id
        /// </summary>
        public Nullable<long> SectionId { get; set; }

        /// <summary>
        /// 1:课件 2:教案 3:视频 6:思政案例
        /// </summary>
        public Nullable<int> SectionType { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public Nullable<long> CenterKnowledgePointId { get; set; }

    }
}
