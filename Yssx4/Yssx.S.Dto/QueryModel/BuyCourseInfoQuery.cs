﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 购买课程查询实体
    /// </summary>
    public class BuyCourseInfoQuery : PageRequest
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
    }
}
