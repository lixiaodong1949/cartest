﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生总数及查看预习数量查询实体
    /// </summary>
    public class StudentNumberAndHadCheckNumberQuery
    {
        /// <summary>
        /// 课程预习Id
        /// </summary>
        public long CoursePreviewId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }
    }
}
