﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 资源学生不懂详情查询实体
    /// </summary>
    public class FileStudentDoubtInfoQuery
    {
        /// <summary>
        /// 课程预习Id
        /// </summary>
        public long CoursePreviewId { get; set; }

        /// <summary>
        /// 资源Id
        /// </summary>
        public long FileId { get; set; }
    }
}
