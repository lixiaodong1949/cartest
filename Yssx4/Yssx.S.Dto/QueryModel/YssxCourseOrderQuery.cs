﻿using System;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 购买课程查询实体
    /// </summary>
    public class YssxCourseOrderQuery : PageRequest
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 学历 0:中职 1:高职 2:本科
        /// </summary>
        public Nullable<int> Education { get; set; }

        /// <summary>
        /// 课程类型 0:常规课
        /// </summary>
        public Nullable<int> CourseType { get; set; }

        /// <summary>
        /// 订单来源 0:系统赠送 4:客户创建(购买)
        /// </summary>
        public Nullable<int> OrderSource { get; set; }

    }
}
