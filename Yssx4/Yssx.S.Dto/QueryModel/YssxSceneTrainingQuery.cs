﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程场景实训查询实体(购买)
    /// </summary>
    public class YssxSceneTrainingQuery : PageRequest
    {        
        /// <summary>
        /// 关键字(实训名称/公司名称)
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 业务场景
        /// </summary>
        public Nullable<long> BusinessSceneId { get; set; }
    }
}
