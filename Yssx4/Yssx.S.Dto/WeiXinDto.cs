﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
   public class WeiXinDto
    {
        public string Access_Token { get; set; }

        public long Expires_In { get; set; }

        public string Refresh_Token { get; set; }

        public string Openid { get; set; }

        public string Scope { get; set; }

        public DateTime Date { get; set; }
    }

    public class WeiXinTicketDto
    {
        public string Ticket { get; set; }

        public string Expires_in { get; set; }

        public int Errcode { get; set; }
    }

    public class WeiXinInfoDto: WeiXinDto
    {
        public string NickName { get; set; }

        public string Unionid { get; set; }
    }

    public class WeiXinAppletsDto
    {
        public string Session_Key { get; set; }

        public string Openid { get; set; }

        public string Unionid { get; set; }
    }

    public class WeiXinDecodeUserDto
    {
        public string PhoneNumber { get; set; }

        public string PurePhoneNumber { get; set; }

        public string CountryCode { get; set; }

        public string Unionid { get; set; }
    }

    public class AppletsType
    {
        public int Index { get; set; }

        public string AppId { get; set; }

        public string Secret { get; set; }

        public string Desc { get; set; }
    }
}
