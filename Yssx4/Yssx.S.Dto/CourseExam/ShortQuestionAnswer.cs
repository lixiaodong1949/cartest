﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 简答答题作答信息
    /// </summary>
    public class ShortQuestionAnswer
    {
        ///// <summary>
        ///// 作答记录主键
        ///// </summary>
        //public long GradeDetailId { get; set; }

        /// <summary>
        /// 作答ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 评语
        /// </summary>
        public string Comment { get; set; }
    }
}
