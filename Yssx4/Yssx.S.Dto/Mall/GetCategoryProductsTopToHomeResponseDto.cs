﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Mall
{
    /// <summary>
    /// 
    /// </summary>
    public class GetCategoryProductsTopToHomeResponseDto
    {
        /// <summary>
        /// 分类Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary> 
        public int Sort { get; set; }

        /// <summary>
        /// 布局类型
        /// * 0 九宫格带图（通用）
        /// * 1 横版带图
        /// * 2 横版不带图
        /// </summary>
        public int LayoutType { get; set; }

        /// <summary>
        /// 商品
        /// </summary>
        public List<ProductBaseResponseDto> Products { get; set; }

    }
    /// <summary>
    /// 商品
    /// </summary>
    public class ProductBaseResponseDto
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary> 
        public int LikesNumber { get; set; }

        /// <summary>
        /// 观看数
        /// </summary> 
        public int ViewsNumber { get; set; }

    }
}
