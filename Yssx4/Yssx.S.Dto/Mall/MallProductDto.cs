﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Mall
{
    public class GetProductPriceInputDto
    {
        public GetProductPriceInputDtoItem[] Items { get; set; }
    }

    public class GetProductPriceInputDtoItem
    {
        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 分类 1.案例 2.月度任务 3.单个商品
        /// </summary>
        public int Category { get; set; }
    }

    public class GetProductPriceOutputDto
    {
        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 分类 1.案例 2.月度任务 3.单个商品
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }
    }
}
