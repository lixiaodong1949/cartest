﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 保存商品分类 - 入参
    /// </summary>
    public class SaveMallCategoryDto
    {
        /// <summary>
        /// 主键ID
        /// </summary> 
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 父级分类ID
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 布局类型
        /// * 0 九宫格带图（通用）
        /// * 1 横版带图
        /// * 2 横版不带图
        /// </summary>
        public int LayoutType { get; set; } = 0;

    }
}
