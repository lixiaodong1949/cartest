﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 设置商品分类信息 - 入参
    /// </summary>
    public class SetProductCategoryInfoDto
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品分类ID（多个分类以,分隔）
        /// </summary>
        public string Category { get; set; }
    }

    /// <summary>
    /// 设置商品排序信息 - 入参
    /// </summary>
    public class SetProductSortInfoDto
    {
        /// <summary>
        /// 一级分类数据
        /// </summary>
        public List<GetMallCategoryListDto> Detail { get; set; }

    }


}
