﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class UpdateClassDto
    {

        /// <summary>
        /// 院系Id
        /// </summary>
        public long CollegeId { get; set; }

        public string CollegeName { get; set; }


        /// <summary>
        /// 专业Id
        /// </summary>
        public long ProfessionId { get; set; }
        /// <summary>
        /// 原用户班级Id
        /// </summary>
        public long ClassId { get; set; } 

        /// <summary>
        /// 新班级Id
        /// </summary>
       public  long NewId { get; set; }
    }
}
