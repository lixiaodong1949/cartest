﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 生成学校随机码
    /// </summary>
    public class YssxRandomCodeDao
    {
        /// <summary>
        /// 随机码（12位机构代号(数字+字母)）
        /// </summary>
        public string RandomCodeDao { get; set; }
    }
}
