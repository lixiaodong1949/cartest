﻿using System;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// 用户类型
        /// </summary> 
        public UserTypeEnums UserType { get; set; }
        /// <summary>
        /// 用户状态
        /// </summary> 
        public Status Status { get; set; }
        /// <summary>
        /// 学校ID
        /// </summary>
        public long TenantId { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }
        /// <summary>
        /// 工号
        /// </summary>
        public string TeacherNo { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IdNumber { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }


    }
}
