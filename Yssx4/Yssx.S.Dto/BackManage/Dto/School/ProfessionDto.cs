﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 专业
    /// </summary>
    public class ProfessionDto : BizBaseDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 专业名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary>
        public long CollegeId { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
    }
}
