﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// Banner图DTO
    /// </summary>
    public class BannerInfoDto
    {
        /// <summary>
        /// 跳转url
        /// </summary>
        public string BannerUrl { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 终端类型
        /// </summary>
        public ClientType ClientType { get; set; }
    }
}
