﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Report
{
    /// <summary>
    /// 错题分析
    /// </summary>
    public class StudentErrorQuestionDto
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 题号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 题目名称
        /// </summary>
        public string Title{ get; set; }
        /// <summary>
        /// 做题总次数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 做错次数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 出错率
        /// </summary>
        public decimal ErrorRatio { get; set; }
    }
}
