﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Report
{
    /// <summary>
    /// 学生
    /// </summary>
    public class CompetitionDetailDto
    {
        ///// <summary>
        ///// 作答记录Id
        ///// </summary>
        //public long GradeId { get; set; }
        ///// <summary>
        ///// 组Id
        ///// </summary>
        //public string GroupId { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        public string GroupNo { get; set; }
        ///// <summary>
        ///// 案例ID
        ///// </summary>
        //public long CaseId { get; set; }
        ///// <summary>
        ///// 考试Id
        ///// </summary>
        //public long ExamId { get; set; }

        ///// <summary>
        ///// 开始时间
        ///// </summary>
        //public DateTime StartTime { get; set; }
        ///// <summary>
        ///// 结束时间
        ///// </summary>
        //public DateTime EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<CompetitionTeamInfo> TeamInfos { get; set; }
        /// <summary>
        /// 总成绩
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 团队排名
        /// </summary>
        public int Rank { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CompetitionTeamInfo
    {
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 案例年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 场次名称
        /// </summary>
        public string MatchName { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public  decimal TotalSocre { get; set; }

        /// <summary>
        /// 组用户信息
        /// </summary>
       public List<GroupUserInfo> GroupUserInfos { get; set; }

       
        
    }
    /// <summary>
    /// 组用户信息
    /// </summary>
    public class GroupUserInfo
    {
        /// <summary>
        /// 场次名称
        /// </summary>
        public string MatchName { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalSocre { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostionName { get; set; }
        /// <summary>
        /// 该岗位对应作答人
        /// </summary>
        public string StudentName { get; set; }
        /// <summary>
        /// 个人用时
        /// </summary>
        public double Minutes { get; set; }
        /// <summary>
        /// 个人得分
        /// </summary>
        public decimal Score { get; set; }

    }
    public class CompetitionGroupUserInfo
    {
        ///// <summary>
        ///// 总成绩
        ///// </summary>
        //public decimal TotalScore { get; set; }
        
        /// <summary>
        /// 房间号
        /// </summary>
        public string GroupNo { get; set; }
        /// <summary>
        /// 场次名称
        /// </summary>
        public string MatchName { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostionName { get; set; }
        /// <summary>
        /// 该岗位对应作答人
        /// </summary>
        public string StudentName { get; set; }
        /// <summary>
        /// 个人用时
        /// </summary>
        public string Minutes { get; set; }
        /// <summary>
        /// 个人得分
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public decimal SingleTotalSocre { get; set; }
        /// <summary>
        /// 团队排名
        /// </summary>
        public int Rank { get; set; }
    }
}
