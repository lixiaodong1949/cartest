﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出-学校激活码
    /// </summary>
    public class ExportActivationC
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        [Description("学校")]
        public string school { get; set; }

        /// <summary>
        /// 角色代号-教务（如果是修改不用传）
        /// </summary>
        [Description("角色代号-教务")]
        public string EducationalCode { get; set; }

        /// <summary>
        ///  教务账号数量
        /// </summary>
        [Description("教务账号数量")]
        public string EducationalNumber { get; set; }

        /// <summary>
        /// 角色代号-教师（如果是修改不用传）
        /// </summary>
        [Description("角色代号-教师")]
        public string TeacherCode { get; set; }

        /// <summary>
        ///  教师账号数量
        /// </summary>
        [Description("教师账号数量")]
        public string TeacherNumber { get; set; }

        /// <summary>
        /// 角色代号-学生（如果是修改不用传）
        /// </summary>
        [Description("角色代号-学生")]
        public string StudentCode { get; set; }

        /// <summary>
        ///  学生账号数量
        /// </summary>
        [Description("学生账号数量")]
        public string StudentNumber { get; set; }

        /// <summary>
        ///  激活截至时间
        /// </summary>
        [Description("激活截至时间")]
        public DateTime ActivationDeadline { get; set; }

        /// <summary>
        ///  备注
        /// </summary>
        [Description("备注")]
        public string Remarks { get; set; }

    }
}
