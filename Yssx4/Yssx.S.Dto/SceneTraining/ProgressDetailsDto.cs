﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 进度详情dto
    /// </summary>
    public class ProgressDetailsDto
    {
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 流程集合
        /// </summary>
        public List<ProcessDetailsLcxhAndName> ProcessDetailsLcxhAndNameList { get; set; }
        /// <summary>
        /// 总共获取资料
        /// </summary>
        public List<Sczl> Sczl { get; set; }
        /// <summary>
        /// 总共获取掌握技能和注意事项
        /// </summary>
        public List<string> Jnsx { get; set; }
        /// <summary>
        /// 显示内容
        /// </summary>
        public Xsnr Xsnr { get; set; }
        /// <summary>
        /// 步数
        /// </summary>
        public int Bs { get; set; }
        /// <summary>
        /// 总共题目集合
        /// </summary>
        public List<Tm> ZgtmList { get; set; }
        /// <summary>
        /// 流程详情Id
        /// </summary>
        public long LcxqId { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int Lcxh { get; set; }

        public ProgressDetailsDto(){
            this.ProcessDetailsLcxhAndNameList = new List<ProcessDetailsLcxhAndName>();
            this.ZgtmList = new List<Tm>();
            this.Sczl = new List<Sczl>();
            this.Jnsx = new List<string>();
        }
    }

    /// <summary>
    /// 流程
    /// </summary>
    public class ProcessDetailsLcxhAndName {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int Lcxh { get; set; }
    }

    /// <summary>
    /// 显示内容
    /// </summary>
    public class Xsnr {
        /// <summary>
        /// 上传资料集合
        /// </summary>
        public List<Sczl> Sczl { get; set; }
        /// <summary>
        /// 注意事项
        /// </summary>
        public string Zysx { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string XzbmName { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string XzryName { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Lcmc { get; set; }
        /// <summary>
        /// 图标路径
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 当前显示的题目Id
        /// </summary>
        public long TmId { get; set; }
        /// <summary>
        /// 题目列表
        /// </summary>
        public List<Tjtm> Tmlist { get; set; }
        /// <summary>
        /// 题目数量
        /// </summary>
        public int Tmsl { get; set; }
        public Xsnr() {
            this.Sczl = new List<Sczl>();
            this.Tmlist = new List<Tjtm>();
        }
    }

    /// <summary>
    /// 题目列表
    /// </summary>
    public class Tm {
        /// <summary>
        /// 题目名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 状态(0,未交卷，1，已交卷)
        /// </summary>
        public int Statu { get; set; }
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
    }
}
