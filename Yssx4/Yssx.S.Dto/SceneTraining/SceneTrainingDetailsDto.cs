﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    public class SceneTrainingDetailsDto
    {
        /// <summary>
        /// 流程Id
        /// </summary>
        public long LcId { get; set; }
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int Lcxh { get; set; }
        /// <summary>
        /// 第一步数据
        /// </summary>
        public FirstStepData FirstStepData { get; set; }
        /// <summary>
        /// 第二步数据
        /// </summary>
        public StepTwoData StepTwoData { get; set; }
    }

    /// <summary>
    /// 第一步数据
    /// </summary>
    public class FirstStepData {
        /// <summary>
        /// 选择部门
        /// </summary>
        public long Xzbm { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string XzbmName { get; set; }
        /// <summary>
        /// 选择人员
        /// </summary>
        public long Xzry { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string XzryName { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 资料图片
        /// </summary>
        public string Zltp { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 步数
        /// </summary>
        public int Bs { get; set; }
    }

    /// <summary>
    /// 第二步数据
    /// </summary>
    public class StepTwoData {
        /// <summary>
        /// 上传资料
        /// </summary>
        public List<Sczl> Sczls { get; set; }
        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        public string Jnsx { get; set; }
        /// <summary>
        /// 资料图片
        /// </summary>
        public string Zltp { get; set; }
        /// <summary>
        /// 步数
        /// </summary>
        public int Bs { get; set; }
    }
}
