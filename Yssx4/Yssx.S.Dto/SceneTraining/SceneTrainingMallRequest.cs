﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{

    /// <summary>
    /// 列表--返回参数
    /// </summary>
    public class SceneTrainingMallResult
    {
        /// <summary>
        /// 广告位
        /// </summary>
        public List<AdvertisingPosition> AdvertisingPosition { get; set; }

        /// <summary>
        /// 行业
        /// </summary>
        public List<Industrys> Industrys { get; set; }

        /// <summary>
        /// 场景
        /// </summary>
        public List<Scenes> Scenes { get; set; }

        /// <summary>
        /// 热门场景实训
        /// </summary>
        public List<HotTraining> HotTraining { get; set; }

        /// <summary>
        /// 最新场景实训
        /// </summary>
        public List<LatestTraining> LatestTraining { get; set; }

        /// <summary>
        /// 列表详情
        /// </summary>
        public List<SceneTrainingMall> SceneTrainingMall { get; set; }
    }

    /// <summary>
    /// 列表详情--返回参数
    /// </summary>
    public class SceneTrainingMall
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Img { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png";

        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CorporateName { get; set; } = "阿里屎屎";

        /// <summary>
        /// 场景
        /// </summary>
        public string Scene { get; set; } = "杀猪的";

        /// <summary>
        /// 行业
        /// </summary>
        public string Industry { get; set; } = "互联网";

        /// <summary>
        /// 规模
        /// </summary>
        public string Scale { get; set; } = "宇宙上市公司";

        /// <summary>
        /// 纳税
        /// </summary>
        public string PayTaxes { get; set; } = "不纳税公司";

        /// <summary>
        /// 阅读
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 使用
        /// </summary>
        public int PurchaseTimes { get; set; }

        /// <summary>
        /// 流程步骤
        /// </summary>
        public int ProcessSteps { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public int Enclosure { get; set; }

        /// <summary>
        /// 问题
        /// </summary>
        public int Problem { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

    }

    /// <summary>
    /// 详情--返回参数
    /// </summary>
    public class SceneTrainingMallRequest
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Img { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png";

        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CorporateName { get; set; } = "阿里屎屎";

        /// <summary>
        /// 场景
        /// </summary>
        public string Scene { get; set; } = "杀猪的";

        /// <summary>
        /// 行业
        /// </summary>
        public string Industry { get; set; } = "互联网";

        /// <summary>
        /// 规模
        /// </summary>
        public string Scale { get; set; } = "宇宙上市公司";

        /// <summary>
        /// 纳税
        /// </summary>
        public string PayTaxes { get; set; } = "不纳税公司";

        /// <summary>
        /// 阅读
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 使用
        /// </summary>
        public int PurchaseTimes { get; set; }

        /// <summary>
        /// 实训描述
        /// </summary>
        public string Describe { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png 啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊";

        /// <summary>
        /// 公司简介
        /// </summary>
        public string CompanyProfile { get; set; } = "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";

        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }

        /// <summary>
        /// 业务流程
        /// </summary>
        public List<string>  Ywlc { get; set; } = new List<string>() { "1.办公室提交考勤" };

        /// <summary>
        /// 购买类型 NULL没买 0赠送1购买
        /// </summary>
        public int? PurchaseType { get; set; }

        /// <summary>
        /// 热门场景实训
        /// </summary>
        public List<HotTraining> HotTraining { get; set; }

        /// <summary>
        /// 最新场景实训
        /// </summary>
        public List<LatestTraining> LatestTraining { get; set; }

    }

    /// <summary>
    /// 广告位
    /// </summary>
    public class AdvertisingPosition {

        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 广告位主键ID
        /// </summary>
        public long? Gid { get; set; }

        /// <summary>
        /// 实训图片/广告位图片
        /// </summary>
        public string Img { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png";

        /// <summary>
        /// 实训名称/广告位名称
        /// </summary>
        public string Name { get; set; } = "阿里屎屎";

        /// <summary>
        /// 实训内容/广告位内容
        /// </summary>
        public string Content { get; set; } = "是打算即可打开圣诞节卡djkahskdj说的话咯技术的卡号是看得见哈卡11111";

    }

    /// <summary>
    /// 行业
    /// </summary>
    public class Industrys
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 行业
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 场景
    /// </summary>
    public class Scenes
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long BusinessId { get; set; }

        /// <summary>
        /// 场景
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 热门场景实训
    /// </summary>
    public class HotTraining
    {

        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Img { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png";

        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CorporateName { get; set; } = "阿里屎屎";

        /// <summary>
        /// 阅读
        /// </summary>
        public int Read { get; set; }

    }

    /// <summary>
    /// 最新场景实训
    /// </summary>
    public class LatestTraining
    {

        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Img { get; set; } = "http://cdn-ccoptc.yunsx.com/2019-12-11/ddf9661bbddb40aa883115884b25f46e.png!png";

        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CorporateName { get; set; } = "阿里屎屎";

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

    }

}
