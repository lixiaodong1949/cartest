﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Task
{
    public class AddTestTaskDto
    {
        public long TaskId { get; set; }

        public long ExamId { get; set; }
    }
}
