﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询课堂测验列表 - 教师端
    /// </summary>
    public class GetTestingTaskByPageDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long OriginalCourseId { get; set; } = 0;
        /// <summary>
        /// 状态（-2.所有 0.未开始 1.进行中 2.已结束）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否筛选开始时间（1.开始时间 0.结束时间）
        /// </summary>
        public int IsBeginTime { get; set; } = CommonConstants.IsYes;
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }
    }



}
