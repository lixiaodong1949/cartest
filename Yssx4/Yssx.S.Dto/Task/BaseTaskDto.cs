﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务 - 基类
    /// </summary>
    public class BaseTaskDto
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>        
        public string Name { get; set; }
        /// <summary>
        /// 任务类型（0.作业 1.任务 2.考试 3.课堂测验）
        /// </summary>
        public TaskType TaskType { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 源课程ID
        /// </summary>
        public long OriginalCourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime { get; set; }
        /// <summary>
        /// 任务时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }
    }
}
