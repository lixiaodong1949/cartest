﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 同步实训 摘要
    /// </summary>
    public class SummaryDtoNew
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long EnterpriseId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 摘要名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        public string AssistCode { get; set; }

        public long CreateBy { get; set; }

        public int Sort { get; set; }
    }
}
