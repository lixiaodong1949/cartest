﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  获取试卷信息（包括试卷基本信息和试卷包含的题目信息） 请求dto
    /// </summary>
    public class ExamPaperBasicInfoRequestDto
    {
        /// <summary>
        /// 实训类型
        /// </summary>
        public long? DrillTypeId { get; set; }
        /// <summary>
        /// 流程Id
        /// </summary>
        public long? FlowId { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long? DepartmentId { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType? QuestionType { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; } = 0;
        /// <summary>
        /// 题目是否已完成（0未完成，1已完成）
        /// </summary>
        public AnswerDTOStatus? TopicGradeStatus { get; set; }
    }
}
