﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 题目查询Dto
    /// </summary>
    public class TopicQueryDto
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 业务开始日期
        /// </summary>
        public DateTime? BusinessStartDate { get; set; }
        /// <summary>
        /// 业务结束日期
        /// </summary>
        public DateTime? BusinessEndDate { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType? QuestionType { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
    }
}
