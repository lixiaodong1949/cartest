﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 上下架dto
    /// </summary>
    public class UpperAndLowerFramesDtoNew
    {
        /// <summary>
        /// Id集合
        /// </summary>
        public List<long> IdList { get; set; }
        /// <summary>
        /// 类型（1，课程，2，课件，3，案例，4，题目,5，场景实训）
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 1，上架，0，下架(违规处理,撤销)2，删除  (类型传过来为4题目时，做打开关闭操作0，关闭，1，打开)
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public string CaseLabel { get; set; }
    }
}
