﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class BeginningDataDtoNew
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 是否正确(平衡)
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// 误差数据
        /// </summary>
        public decimal ErrorData { get; set; }
    }

    public class SubjectDataDtoNew
    {
        public List<BeginningDataDtoNew> BeginningDatas { get; set; }
    }
}
