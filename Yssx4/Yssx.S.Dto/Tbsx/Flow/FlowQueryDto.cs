﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 流程查询请求Dto
    /// </summary>
    public class FlowQueryDto : PageRequest
    {
        /// <summary>
        /// 实训题型Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
    /// <summary>
    /// 流程查询返回值Dto
    /// </summary>
    public class FlowListDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 实训类型Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 题目总数
        /// </summary>
        public int TopicTotalNum { get; set; }
        /// <summary>
        /// 题目总分
        /// </summary>
        public decimal TopicTotalSore { get; set; }
    }

}
