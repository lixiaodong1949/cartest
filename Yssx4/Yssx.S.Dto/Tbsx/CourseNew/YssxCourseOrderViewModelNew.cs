﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取购买课程实体
    /// </summary>
    public class YssxCourseOrderViewModelNew
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 学历标识 0:中职 1:高职 2:本科
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 上传者(应是课程创建人)
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 课程创建时间(上传时间)
        /// </summary>
        public string CourseCreateTime { get; set; }

        /// <summary>
        /// 课程更新时间
        /// </summary>
        public string CourseUpdateTime { get; set; }
        
        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public string PaymentTime { get; set; }

        /// <summary>
        /// 订单来源 0:系统赠送 1:客户创建(购买)
        /// </summary>
        public Nullable<int> OrderSource { get; set; }
        
        /// <summary>
        /// 有效期(赠送课程有有效期,有效期来自激活学校时的激活码有效期)
        /// </summary>
        public string EffectiveDate { get; set; }

        /// <summary>
        /// 课程汇总信息
        /// </summary>
        public List<YssxSectionSummaryDtoNew> SummaryListViewModel { get; set; } = new List<YssxSectionSummaryDtoNew>();

    }
}
