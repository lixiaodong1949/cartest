﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取课程场景实训实体
    /// </summary>
    public class YssxSectionSceneTrainingViewModelNew
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }

        /// <summary>
        /// 业务场景
        /// </summary>
        public long Ywcj { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public long Pxxh { get; set; }

        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }

        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }

        /// <summary>
        /// 业务场景名称
        /// </summary>
        public string YwcjName { get; set; }

    }
}
