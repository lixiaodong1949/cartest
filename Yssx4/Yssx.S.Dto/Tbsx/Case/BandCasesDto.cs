﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 绑定案例Dto
    /// </summary>
    public class BandCasesDto
    {
        /// <summary>
        /// 课程id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public List<long> CaseIds { get; set; }
    }
}
