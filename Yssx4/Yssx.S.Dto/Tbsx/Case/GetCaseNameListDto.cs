﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取案例名称列表
    /// </summary>
    public class GetCaseNameListDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 案例名称
        /// </summary>
        public string Name { get; set; }
    }
    /// <summary>
    /// 获取案例名称列表
    /// </summary>
    public class QueryCaseNameRequest
    {
        /// <summary>
        /// 案例名称
        /// </summary>
        public string Name { get; set; }
    }
}
