﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 同步实训 企业信息
    /// </summary>
    public class CaseDtoNew
    {
        /// <summary>
        /// 主键，公司Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 公司介绍
        /// </summary>
        public string BriefName { get; set; }
        /// <summary>
        /// 自定义数据
        /// </summary>
        public string CustomData { get; set; }
        /// <summary>
        /// 序号（标号）
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 产品流程是否启用
        /// </summary>
        public bool EnabledProductFlow { get; set; }
        /// <summary>
        /// 产品流程
        /// </summary>
        public string ProductFlow { get; set; }
        /// <summary>
        /// 财务制度
        /// </summary>
        public string FinancialRegulation { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountantOfficer { get; set; }

        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }

        /// <summary>
        /// 业务会计
        /// </summary>
        public string BusinessAccountant { get; set; }

        /// <summary>
        /// 税务会计
        /// </summary>
        public string TaxAccountant { get; set; }

        /// <summary>
        /// 成本会计
        /// </summary>
        public string CostAccountant { get; set; }
        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<PositionManageDtoNew> PositionDtos { get; set; }

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        public string TaxpayerNumber { get; set; }
    }

    public class GetCaseInput : PageRequest
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 行业Id
        /// </summary>
        public long? IndustryId { get; set; }

        /// <summary>
        /// 公司规模
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 纳税主体 0:一般纳税人   1:小规模纳税人
        /// </summary>
        public Taxpayer? Taxpayer { get; set; }
    }

    public class ToggleInput
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 是否启用 true:启用  false:禁用
        /// </summary>
        public bool IsActive { get; set; }
    }
    /// <summary>
    /// 保存修改获取岗位数据入参类（查询）输出类
    /// </summary>
    public class PositionManageDtoNew
    {
        /// <summary>
        /// 岗位ID
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缩略图标文字
        /// </summary>
        public string ThumbnailText { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 岗位图标地址
        /// </summary>
        public string FileUrl { get; set; }
    }

    /// <summary>
    /// 获取岗位数据入参类
    /// </summary>
    public class PositionManageRequestNew : PageRequest
    {
        /// <summary>
        /// 搜索名称
        /// </summary>
        public string Name { get; set; }
    }
}
