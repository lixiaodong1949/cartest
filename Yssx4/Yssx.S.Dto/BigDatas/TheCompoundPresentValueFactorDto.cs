﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 复利现值系数
    /// </summary>
    public class TheCompoundPresentValueFactorDto
    {
        [Description("期数")]
        public string Qs { get; set; }

        [Description("1%")]
        public string One { get; set; }

        [Description("2%")]
        public string Two { get; set; }

        [Description("3%")]
        public string Three { get; set; }

        [Description("4%")]
        public string Four { get; set; }

        [Description("5%")]
        public string Five { get; set; }


        [Description("6%")]
        public string Six { get; set; }

        [Description("7%")]
        public string Seven { get; set; }

        [Description("8%")]
        public string Eight { get; set; }

        [Description("9%")]
        public string Nine { get; set; }

        [Description("10%")]
        public string Ten { get; set; }

        [Description("11%")]
        public string Eleven { get; set; }

        [Description("12%")]
        public string Twelve { get; set; }

        [Description("13%")]
        public string Thirteen { get; set; }

        [Description("14%")]
        public string Fourteen { get; set; }

        [Description("15%")]
        public string Fifteen { get; set; }


        [Description("16%")]
        public string sixteen { get; set; }


        [Description("17%")]
        public string seventeen { get; set; }


        [Description("18%")]
        public string eighteen { get; set; }


        [Description("19%")]
        public string nineteen { get; set; }


        [Description("20%")]
        public string twenty { get; set; }


        [Description("21%")]
        public string TwentyOne { get; set; }

        [Description("22%")]
        public string TwentyTwo { get; set; }

        [Description("23%")]
        public string TwentyThree { get; set; }

        [Description("24%")]
        public string TwentyFour { get; set; }

        [Description("25%")]
        public string TwentyFive { get; set; }

        [Description("26%")]
        public string TwentySix { get; set; }

        [Description("27%")]
        public string TwentySeven { get; set; }

        [Description("28%")]
        public string TwentyEight { get; set; }

        [Description("29%")]
        public string TwentyNine { get; set; }

        [Description("30%")]
        public string Thirty { get; set; }

        [Description("31%")]
        public string ThirtyOne { get; set; }

        [Description("32%")]
        public string ThirtyTwo { get; set; }

        [Description("33%")]
        public string ThirtyThree { get; set; }

        [Description("34%")]
        public string ThirtyFour { get; set; }

        [Description("35%")]
        public string ThirtyFive { get; set; }

        [Description("36%")]
        public string ThirtySix { get; set; }

        [Description("37%")]
        public string ThirtySeven { get; set; }

        [Description("38%")]
        public string ThirtyEight { get; set; }

        [Description("39%")]
        public string ThirtyNine { get; set; }

        [Description("40%")]
        public string Forty { get; set; }

        [Description("41%")]
        public string FortyOne { get; set; }

        [Description("42%")]
        public string FortyTwo { get; set; }

        [Description("43%")]
        public string FortyThree { get; set; }

        [Description("44%")]
        public string FortyFour { get; set; }

        [Description("45%")]
        public string FortyFive { get; set; }

        [Description("46%")]
        public string FortySix { get; set; }

        [Description("47%")]
        public string FortySeven { get; set; }

        [Description("48%")]
        public string FortyEight { get; set; }

        [Description("49%")]
        public string FortyNine { get; set; }

        [Description("50%")]
        public string Fifty { get; set; }
    }
}
