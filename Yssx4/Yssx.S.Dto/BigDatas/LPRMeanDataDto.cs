﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class LPRMeanDataDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("1年LPR5日均值(%)")]
        public string LPR_5 { get; set; }

        [Description("1年LPR10日均值(%)")]
        public string LPR_10 { get; set; }

        [Description("1年LPR20日均值(%)")]
        public string LPR_20 { get; set; }
    }
}
