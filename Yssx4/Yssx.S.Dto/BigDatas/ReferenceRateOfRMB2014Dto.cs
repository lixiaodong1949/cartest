﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币参考汇率_2014
    /// </summary>
    public class ReferenceRateOfRMB2014Dto
    {
        [Description("币种")]
        public string Bz { get; set; }

        [Description("12月")]
        public string Years2014_12 { get; set; }

        [Description("11月")]
        public string Years2014_11 { get; set; }

        [Description("10月")]
        public string Years2014_10 { get; set; }

        [Description("9月")]
        public string Years2014_09 { get; set; }

        [Description("8月")]
        public string Years2014_08 { get; set; }

        [Description("7月")]
        public string Years2014_07 { get; set; }

        [Description("6月")]
        public string Years2014_06 { get; set; }

        [Description("5月")]
        public string Years2014_05 { get; set; }

        [Description("4月")]
        public string Years2014_04 { get; set; }

        [Description("3月")]
        public string Years2014_03 { get; set; }
    }
}
