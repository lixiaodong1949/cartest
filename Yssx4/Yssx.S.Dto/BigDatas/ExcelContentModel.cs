﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 返回头部和数据列
    /// </summary>
    public class ExcelContentModel
    {
        public string ExamId { get; set; }
        public string GradeId { get; set; }
        public string CoseId { get; set; }
        public string QuestionId { get; set; }
        public long Count { get; set; }
        /// <summary>
        /// 返回字段名集合
        /// </summary>
        public List<string> Cols { get; set; }

        /// <summary>
        /// 数据列
        /// </summary>
        public List<List<string>> Rows { get; set; }

        /// <summary>
        /// 总数据列
        /// </summary>
        public List<List<string>> Crows { get; set; }

    }

    /// <summary>
    /// excel导出
    /// </summary>
    public class ExcelModel
    {
        /// <summary>
        /// 返回字段名集合
        /// </summary>
        public List<string> Cols { get; set; }

        /// <summary>
        /// 数据列
        /// </summary>
        public List<List<string>> Rows { get; set; }

        /// <summary>
        /// 模块名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        //public string FilePath { get; set; }
    }
}
