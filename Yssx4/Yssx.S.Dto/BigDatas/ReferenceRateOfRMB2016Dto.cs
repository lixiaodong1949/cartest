﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币参考汇率_2016
    /// </summary>
    public class ReferenceRateOfRMB2016Dto
    {
        [Description("币种")]
        public string Bz { get; set; }

        [Description("12月")]
        public string Years2016_12 { get; set; }

        [Description("11月")]
        public string Years2016_11 { get; set; }

        [Description("10月")]
        public string Years2016_10 { get; set; }

        [Description("9月")]
        public string Years2016_09 { get; set; }

        [Description("8月")]
        public string Years2016_08 { get; set; }

        [Description("7月")]
        public string Years2016_07 { get; set; }

        [Description("6月")]
        public string Years2016_06 { get; set; }

        [Description("5月")]
        public string Years2016_05 { get; set; }

        [Description("4月")]
        public string Years2016_04 { get; set; }

        [Description("3月")]
        public string Years2016_03 { get; set; }

        [Description("2月")]
        public string Years2016_02 { get; set; }

        [Description("1月")]
        public string Years2016_01 { get; set; }
    }
}
