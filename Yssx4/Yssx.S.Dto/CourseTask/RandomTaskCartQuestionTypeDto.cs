﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 智能组卷题目类型和数量
    /// </summary>
    public class RandomTaskCartQuestionTypeDto
    {
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目数量
        /// </summary>
        public int QuestionCount { get; set; }

    }

    public class RandomTaskCartQuestionType
    { 
        public QuestionType QuestionType { get; set; }
    }
}
