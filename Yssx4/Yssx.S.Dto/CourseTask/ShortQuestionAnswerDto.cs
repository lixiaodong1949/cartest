﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生案例分析题作答记录dto
    /// </summary>
    public class ShortQuestionAnswerDto
    {
        /// <summary>
        /// 主作答记录ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 作答详情ID
        /// </summary>
        public long GradeDetailId { get; set; }
        ///// <summary>
        ///// 题目ID
        ///// </summary>
        //public long QuestionId { get; set; }
        public long UserId { get; set; }
        /// <summary>
        /// 学生姓名
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }
        /// <summary>
        /// 班级id
        /// </summary>
        public long ClassId { get; set; }
        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerValue { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 是否批阅 true已批阅
        /// </summary>
        public bool IsReadOver { get; set; } = false;

        /// <summary>
        /// 评语
        /// </summary>
        public string Comment { get; set; }

    }

}
