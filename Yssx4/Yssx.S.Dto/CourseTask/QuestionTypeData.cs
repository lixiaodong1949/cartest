﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 题目类型
    /// </summary>
    public  class QuestionTypeData 
    {
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

    }
}
