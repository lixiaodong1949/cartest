﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 单选题、多选题、判断题
    /// </summary>
    public class SimpleQuestionRequest : BaseQuestion
    {
        /// <summary>
        /// 选项
        /// </summary>
        public List<ChoiceOption> Options { get; set; }
        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        public override string AnswerValue { get; set; }
    }
    /// <summary>
    /// 选这题，选项内容
    /// </summary>
    public class ChoiceOption
    {
        /// <summary>
        /// 选项ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 选项名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选型内容
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 附件图片--技能抽查用不到，其他竞赛要用到
        /// </summary>
        public string AttatchImgUrl { get; set; }

    }
}
