﻿using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位数据入参类
    /// </summary>
    public class GetPositionListRequest : PageRequest
    {
        /// <summary>
        /// 搜索名称
        /// </summary>
        public string SearchText { get; set; }
    }
}
