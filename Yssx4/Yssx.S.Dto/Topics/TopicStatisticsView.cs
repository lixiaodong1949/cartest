﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 题目统计信息
    /// </summary>
    public class TopicStatisticsView
    {
        /// <summary>
        /// 案例总题数
        /// </summary>
        public long TotalCount { get; set; }

        /// <summary>
        /// 案例总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 案例岗位相关分数
        /// </summary>
        public List<TopicPositionView> PositionQuestionDetail { get; set; }

        /// <summary>
        /// 题目明细信息
        /// </summary>
        public PageResponse<TopicListView> Detail { get; set; }
    }
    /// <summary>
    /// 案例中-岗位分类-题目分数
    /// </summary>
    public class TopicPositionView
    {
        /// <summary>
        /// 岗位ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
    }
}
