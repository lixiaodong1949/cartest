﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 小程序登录
    /// </summary>
    public class WeChartAppletsDto
    {
        /// <summary>
        /// Openid
        /// </summary>
        public string Openid { get; set; }

        /// <summary>
        /// 微信平台唯一身份ID
        /// </summary>
        public string Unionid { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
    }
}
