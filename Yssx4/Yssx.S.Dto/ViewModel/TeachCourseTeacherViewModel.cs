﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师上课列表返回实体
    /// </summary>
    public class TeachCourseTeacherViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 上课名称
        /// </summary>
        public string TeachCourseName { get; set; }

        /// <summary>
        /// 上课状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public TeachCourseStatus TeachCourseStatus { get; set; }

        /// <summary>
        /// 上课时间
        /// </summary>
        public DateTime TeachDate { get; set; }

        /// <summary>
        /// 上课班级列表
        /// </summary>
        public List<TeachCourseClassViewModel> ClassList = new List<TeachCourseClassViewModel>();
    }

    /// <summary>
    /// 上课班级列表
    /// </summary>
    public class TeachCourseClassViewModel
    {
        /// <summary>
        /// 课程上课主键Id
        /// </summary>
        public long TeachCourseId { get; set; }

        /// <summary>
        /// 班级主键
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
