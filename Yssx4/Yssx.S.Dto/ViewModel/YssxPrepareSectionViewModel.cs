﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取备课课程章节实体
    /// </summary>
    public class YssxPrepareSectionViewModel
    {
        /// <summary>
        /// 章节Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 章节标题
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 父节点Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 0:章 1：节
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程汇总信息
        /// </summary>
        public List<YssxSectionSummaryDto> SummaryListViewModel { get; set; } = new List<YssxSectionSummaryDto>();
    }
}
