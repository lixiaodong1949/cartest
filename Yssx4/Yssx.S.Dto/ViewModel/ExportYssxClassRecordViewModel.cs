﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出课堂记录实体
    /// </summary>
    public class ExportYssxClassRecordViewModel
    {
        /// <summary>
        /// 上课课程名称
        /// </summary> 
        [Description("课程")]
        public string CourseName { get; set; }

        /// <summary>
        /// 授课教师名称
        /// </summary> 
        [Description("授课教师")]
        public string TeacherName { get; set; }

        /// <summary>
        /// 上课时间
        /// </summary>
        [Description("上课时间")]
        public DateTime AttendClassTime { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级")]
        public string ClassName { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        [Description("学期")]
        public string SemesterName { get; set; }
    }
}
