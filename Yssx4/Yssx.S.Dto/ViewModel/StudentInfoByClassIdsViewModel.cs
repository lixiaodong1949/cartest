﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 根据班级Ids获取学生实体
    /// </summary>
    public class StudentInfoByClassIdsViewModel
    {
        /// <summary>
        /// 学生用户Id
        /// </summary>
        public long StudentUserId { get; set; }

        /// <summary>
        /// 学生姓名
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

    }
}
