﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训任务试卷作答记录
    /// </summary>
    public class YssxJobTrainingTaskExamAnswerRecordViewModel
    {
        /// <summary>
        /// 交卷时间
        /// </summary>
        public Nullable<DateTime> SubmitTime { get; set; }
        
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double TotalSeconds { get; set; }

        /// <summary>
        /// 成绩总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 排名
        /// </summary>
        public string Ranking { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 考试状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public int StudentExamStatus { get; set; }

    }
}
