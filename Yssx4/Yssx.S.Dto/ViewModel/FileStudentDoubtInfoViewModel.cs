﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 资源学生不懂详情返回实体
    /// </summary>
    public class FileStudentDoubtInfoViewModel
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 疑问(不懂)描述
        /// </summary>
        public string QuestionDescribe { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishDate { get; set; }
    }
}
