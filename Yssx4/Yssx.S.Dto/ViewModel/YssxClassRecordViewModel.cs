﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂记录实体
    /// </summary>
    public class YssxClassRecordViewModel
    {
        /// <summary>
        /// 课堂记录Id
        /// </summary>
        public long RecordId { get; set; }

        /// <summary>
        /// 上课标识码(用于标识哪些班级一起上课)
        /// </summary>
        public long IdentifyCode { get; set; }

        /// <summary>
        /// 上课课程名称
        /// </summary> 
        public string CourseName { get; set; }

        /// <summary>
        /// 授课教师名称
        /// </summary> 
        public string TeacherName { get; set; }

        /// <summary>
        /// 上课时间
        /// </summary>
        public DateTime AttendClassTime { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

    }
}
