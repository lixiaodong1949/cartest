﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学期班级实体
    /// </summary>
    public class YssxSemesterClassViewModel
    {
        /// <summary>
        /// 学期Id
        /// </summary>
        public long SemesterId { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

        /// <summary>
        /// 班级个数
        /// </summary>
        public int ClassCount { get; set; }

        /// <summary>
        /// 专业列表
        /// </summary>
        public List<StudentProfession> StudentProfessionList = new List<StudentProfession>();
    }

    /// <summary>
    /// 专业
    /// </summary>
    public class StudentProfession
    {
        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级列表
        /// </summary>
        public List<StudentClass> StudentClassList = new List<StudentClass>();
    }

    /// <summary>
    /// 班级
    /// </summary>
    public class StudentClass
    {
        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 学生人数
        /// </summary>
        public int StudentCount { get; set; }

    }
}
