﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导入学生结果返回实体
    /// </summary>
    public class ImportStudentResultModel
    {
        /// <summary>
        /// 正确个数
        /// </summary>
        public int RightCount { get; set; }

        /// <summary>
        /// 错误个数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 错误数据列表
        /// </summary>
        public List<ImportStudentViewModel> ErrorList = new List<ImportStudentViewModel>();
    }
}
