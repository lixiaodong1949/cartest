﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师资源详情
    /// </summary>
    public class YssxTeacherResourceViewModel
    {
        /// <summary>
        /// 资源类型 0:情景实训 1:会计实训 2:出纳实训 3:财务大数据 4:业财税一体 5:课程实训 6:案例资源
        /// </summary>
        public int ResourceType { get; set; }

        /// <summary>
        /// 资源总计数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 已获资源数量
        /// </summary>
        public int HasWonCount { get; set; }
    }
}
