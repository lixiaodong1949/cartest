﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生课堂测验任务返回实体
    /// </summary>
    public class StudentCourseTestTaskViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 测验任务开始时间
        /// </summary>
        public DateTime TestStartDate { get; set; }

        /// <summary>
        /// 测验任务结束时间
        /// </summary>
        public Nullable<DateTime> TestEndDate { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public LessonsTaskStatus TaskStatus { get; set; }

        /// <summary>
        /// 任务来源 0:课程 1:案例
        /// </summary>
        public CourseTestTaskSource TaskSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 任务总分
        /// </summary>
        public decimal TotalScore { get; set; }
        
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 操作状态 0:开始测验 1:查看作答 2:未作答
        /// </summary>
        public CourseTestTaskStudentOperateStatus OperateStatus { get; set; }
    }
}
