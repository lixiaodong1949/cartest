﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课选中资源返回实体
    /// </summary>
    public class CoursePrepareSelectResourceViewModel
    {
        /// <summary>
        /// 文件列表
        /// </summary>
        public List<CoursePrepareSelectFileViewModel> FileList = new List<CoursePrepareSelectFileViewModel>();

        /// <summary>
        /// 题目列表
        /// </summary>
        public List<CoursePrepareSelectTopicViewModel> TopicList = new List<CoursePrepareSelectTopicViewModel>();
    }

    /// <summary>
    /// 课程备课选中文件返回实体(课件、教案、视频)
    /// </summary>
    public class CoursePrepareSelectFileViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 是否是附件(区分思政案例超链接)
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

    /// <summary>
    /// 课程备课选中题目返回实体
    /// </summary>
    public class CoursePrepareSelectTopicViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题目主键
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目内容
        /// </summary>
        public string QuestionContent { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        public string CenterKnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        public CoursePrepareTopicSource TopicSource { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int DifficultyLevel { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
