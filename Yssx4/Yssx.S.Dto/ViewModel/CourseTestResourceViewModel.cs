﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程测验资源返回实体
    /// </summary>
    public class CourseTestResourceViewModel
    {
        /// <summary>
        /// 文件列表
        /// </summary>
        public List<CourseTestFileViewModel> FileList = new List<CourseTestFileViewModel>();

        /// <summary>
        /// 题目列表
        /// </summary>
        public List<CourseTestTopicViewModel> TopicList = new List<CourseTestTopicViewModel>();
    }

    /// <summary>
    /// 课程测验文件返回实体(课件、教案、视频)
    /// </summary>
    public class CourseTestFileViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 是否是附件(区分思政案例超链接)
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

    /// <summary>
    /// 课程测验题目返回实体
    /// </summary>
    public class CourseTestTopicViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题目主键
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        public CoursePrepareTopicSource TopicSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 课程或案例名称
        /// </summary>
        public string CourseOrCaseName { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

}
