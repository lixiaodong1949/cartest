﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

//入参
namespace Yssx.S.Dto
{
    /// <summary>
    /// 人员信息采集列表(人员，资料)
    /// </summary>
    public class PersonnelListDao : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 类型 
        /// 1人员信息采集 专项附加扣除信息采集(2子女教育支出3继续教育支出,4住房贷款利息支出,5住房租金支出,6赡养老人支出) 7综合所得申报
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 人员信息采集,专项附加扣除信息采集，综合所得申报
    /// </summary>
    public class NotesDao
    {
        /// <summary>
        /// 主键Id(记录)
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 查看资料Id
        /// </summary>
        public long ViewInfoId { get; set; }

        /// <summary>
        /// 人员信息采集Id
        /// </summary>
        public long PersonnelId { get; set; }

        /// <summary>
        ///类型： 1人员信息采集  专项附加扣除信息采集(2子女教育支出3继续教育支出,4住房贷款利息支出,5住房租金支出,6赡养老人支出)  7综合所得申报
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 1人员信息采集
        /// </summary>
        public Personnel Personnel{ get; set; }

        /// <summary>
        /// 2子女教育支出
        /// </summary>
        public Children Children { get; set; }

        /// <summary>
        /// 3继续教育支出
        /// </summary>
        public Continue Continue { get; set; }

        /// <summary>
        /// 4住房贷款利息支出
        /// </summary>
        public Interest Interest { get; set; }

        /// <summary>
        /// 5住房租金支出
        /// </summary>
        public Rent Rent { get; set; }

        /// <summary>
        /// 6赡养老人支出
        /// </summary>
        public Support Support { get; set; }

        /// <summary>
        /// 7综合所得申报
        /// </summary>
        public Income Income { get; set; }

        #region 扩展
        ///// <summary>
        ///// 案例Id
        ///// </summary>
        //public long CaseId { get; set; }
        #endregion
    }

    #region 记录
    /// <summary>
    /// 人员信息采集
    /// </summary>
    public class Personnel
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件类型  0身份证 1护照
        /// </summary>
        public int DocumentType { get; set; }

        /// <summary>
        /// 性别  0男 1女
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public string Profession { get; set; }

        /// <summary>
        /// 纳税识别号
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// 残疾证号
        /// </summary>
        public string DisabilityCard { get; set; }

        /// <summary>
        /// 国籍（地区）
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string CertificateNum { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// 学历  0大学本科以下 1大学本科 2研究生
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 是否存在以下情形  0残疾 1烈属 2孤老
        /// </summary>
        public int DoesItExist { get; set; }

        /// <summary>
        /// 烈属证号
        /// </summary>
        public string MartyrNo { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }



        /// <summary>
        /// 经常居住地
        /// </summary>
        public string HabitualResidence { get; set; }

        /// <summary>
        /// 户籍地址
        /// </summary>
        public string PermanentAddress { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public long MobilePhone { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string BankOfDeposit { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 银行账号
        /// </summary>
        public long BankAccount { get; set; }



        /// <summary>
        /// 任职受雇日期
        /// </summary>
        public string Tenure { get; set; }

        /// <summary>
        ///雇员  0否 1是
        /// </summary>
        public int Employee { get; set; }

        /// <summary>
        /// 股东、投资人  0否 1是
        /// </summary>
        public int Investor { get; set; }

        /// <summary>
        /// 公司股本总额
        /// </summary>
        public decimal TotalShareCapital { get; set; }

        /// <summary>
        /// 工号
        /// </summary>
        public string JobNo { get; set; }

        /// <summary>
        /// 离职日期
        /// </summary>
        public string DepartureDate { get; set; }

        /// <summary>
        /// 特定行业  0否 1是
        /// </summary>
        public int SpecificIndustries { get; set; }

        /// <summary>
        /// 天使投资人  0否 1是
        /// </summary>
        public int AngelInvestors { get; set; }

        /// <summary>
        /// 个人投资总额
        /// </summary>
        public decimal TotalInvestment { get; set; }

        /// <summary>
        /// 人员状态  0正常 1不正常
        /// </summary>
        public int PersonnelStatus { get; set; }
    }

    /// <summary>
    /// 子女教育支出
    /// </summary>
    public class Children
    {
        /// <summary>
        /// 子女姓名
        /// </summary>
        public string ChildrenName { get; set; }

        /// <summary>
        /// 子女证件类型  0身份证 1护照
        /// </summary>
        public int ChildType { get; set; }

        /// <summary>
        /// 子女证件号码
        /// </summary>
        public string ChildNumber { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// 国籍（地区）
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 当前受教育阶段 0义务教育 1高中阶段教育 2学前教育阶段
        /// </summary>
        public int CurrentEducation { get; set; }

        /// <summary>
        /// 受教育日期起
        /// </summary>
        public string EducationBegins { get; set; }

        /// <summary>
        /// 受教育日期止
        /// </summary>
        public string EducationEnd { get; set; }

        /// <summary>
        /// 教育终止时间
        /// </summary>
        public string EducationTimeEnd { get; set; }

        /// <summary>
        /// 就读国家
        /// </summary>
        public string CountryStudy { get; set; }

        /// <summary>
        /// 当前就读学校
        /// </summary>
        public string CurrentSchool { get; set; }

        /// <summary>
        /// 本人扣除比例
        /// </summary>
        public string DeductionProportion { get; set; }
    }

    /// <summary>
    /// 继续教育支出
    /// </summary>
    public class Continue
    {
        /// <summary>
        ///入学时间起
        /// </summary>
        public string AdmissionTime { get; set; }

        /// <summary>
        /// (预计)毕业时间
        /// </summary>
        public string GraduationTime { get; set; }

        /// <summary>
        /// 教育阶段 0大学专科 1大学本科 2硕士研究生 3博士研究生
        /// </summary>
        public int EducationStage { get; set; }



        /// <summary>
        /// 继续教育类型 0技能人员职业资格 1专业技术人员职业资格
        /// </summary>
        public int EducationTypes { get; set; }

        /// <summary>
        /// 发证(批准)日期
        /// </summary>
        public string IssueDate { get; set; }

        /// <summary>
        /// 证书名称
        /// </summary>
        public string CertificateName { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 发证机关
        /// </summary>
        public string IssuingAuthority { get; set; }
    }

    /// <summary>
    /// 住房贷款利息支出
    /// </summary>
    public class Interest
    {
        /// <summary>
        ///房屋坐落地址
        /// </summary>
        public string LocationHouse { get; set; }

        /// <summary>
        ///房屋楼牌号
        /// </summary>
        public string BuildingNumber { get; set; }

        /// <summary>
        ///房屋证书类型 0房屋所有权证 1房屋预售合同 2不动产权证 3房屋买卖合同
        /// </summary>
        public int HouseType { get; set; }

        /// <summary>
        ///房屋证书号
        /// </summary>
        public string HousingCertificate { get; set; }

        /// <summary>
        ///本人是否借款人 0是 1不是
        /// </summary>
        public int Borrower { get; set; }

        /// <summary>
        ///是否婚前各自首套贷款，且婚后分别扣除50% 0否 1是
        /// </summary>
        public int Premarital { get; set; }

        /// <summary>
        ///贷款类型 0商业贷款 1公积金贷款
        /// </summary>
        public int LoanType { get; set; }

        /// <summary>
        ///贷款合同编号
        /// </summary>
        public string LoanContractNo { get; set; }

        /// <summary>
        ///贷款银行
        /// </summary>
        public string LendingBank { get; set; }

        /// <summary>
        ///首次还款日期
        /// </summary>
        public string FirstRepaymentDate { get; set; }

        /// <summary>
        ///贷款期限(月数)
        /// </summary>
        public string LoanTerm { get; set; }
    }

    /// <summary>
    /// 住房租金支出
    /// </summary>
    public class Rent
    {
        /// <summary>
        ///工作城市
        /// </summary>
        public string WorkingCity { get; set; }

        /// <summary>
        ///出租房类型 0自然人 1组织
        /// </summary>
        public int RentalType { get; set; }

        /// <summary>
        ///房屋坐落地址
        /// </summary>
        public string HouseLocation { get; set; }

        /// <summary>
        ///房屋坐落楼牌号
        /// </summary>
        public string BuildingBrand { get; set; }

        /// <summary>
        ///租赁日期起
        /// </summary>
        public string FromLeaseDate { get; set; }

        /// <summary>
        ///租赁日期止
        /// </summary>
        public string EndLeaseDate { get; set; }
    }

    /// <summary>
    /// 赡养老人支出
    /// </summary>
    public class Support
    {
        /// <summary>
        ///是否独生子女 0是 1否
        /// </summary>
        public int Only { get; set; }

        /// <summary>
        ///分摊方式
        /// </summary>
        public string Share { get; set; }

        /// <summary>
        ///本年度月扣除金额
        /// </summary>
        public string MonthlyDeduction { get; set; }



        /// <summary>
        ///被养姓名
        /// </summary>
        public string FosterName { get; set; }

        /// <summary>
        ///被养身份证件类型
        /// </summary>
        public string FosterDocumentType { get; set; }

        /// <summary>
        ///被养身份证号码
        /// </summary>
        public string FosterCertificateNum { get; set; }

        /// <summary>
        ///被养国籍地区
        /// </summary>
        public string FosterNationality { get; set; }

        /// <summary>
        ///关系
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        ///出生日期
        /// </summary>
        public string DateOfBirth { get; set; }



        /// <summary>
        ///共养姓名
        /// </summary>CoCulture
        public string CoCultureName { get; set; }

        /// <summary>
        ///共养身份证件类型
        /// </summary>
        public string CoCultureTypes { get; set; }

        /// <summary>
        ///共养身份证件号码
        /// </summary>
        public string CoCultureNum { get; set; }

        /// <summary>
        ///共养国籍（地区）
        /// </summary>
        public string CoCultureNationality { get; set; }
    }

    /// <summary>
    /// 综合所得申报
    /// </summary>
    public class Income
    {
        /// <summary>
        /// 应发工资合计
        /// </summary>
        public decimal Wages { get; set; }



        /// <summary>
        /// 养老保险
        /// </summary>
        public decimal Pension { get; set; }

        /// <summary>
        /// 医疗保险
        /// </summary>
        public decimal MedicalCare { get; set; }

        /// <summary>
        /// 失业保险
        /// </summary>
        public decimal Unemployment { get; set; }



        /// <summary>
        /// 住房公积金
        /// </summary>
        public decimal AccumulationFund { get; set; }



        /// <summary>
        /// 累计子女教育
        /// </summary>
        public decimal AccumulatedChildren { get; set; }

        /// <summary>
        /// 累计住房贷款利息
        /// </summary>
        public decimal AccumulatedInterest { get; set; }

        /// <summary>
        /// 累计租房租金
        /// </summary>
        public decimal AccumulatedRental { get; set; }

        /// <summary>
        /// 累计赡养老人
        /// </summary>
        public decimal AccumulatedSupport { get; set; }

        /// <summary>
        /// 累计继续教育
        /// </summary>
        public decimal AccumulatedContinue { get; set; }

        /// <summary>
        /// 当月个税预扣预缴税额
        /// </summary>
        public decimal WithholdingTax { get; set; }



        /// <summary>
        /// 实发工资
        /// </summary>
        public decimal NetSalary { get; set; }
    }
    #endregion

    /// <summary>
    /// 报送,获取反馈,删除
    /// </summary>
    public class IdsDao
    {
        /// <summary>
        /// 主键Id 集合
        /// </summary>
        public List<long> Ids { get; set; }

        /// <summary>
        /// 类型 
        /// 1人员信息采集 专项附加扣除信息采集(2子女教育支出3继续教育支出,4住房贷款利息支出,5住房租金支出,6赡养老人支出) 7综合所得申报
        /// </summary>
        public int Type { get; set; }
    }




    //后台
    /// <summary>
    /// 人员信息采集列表
    /// </summary>
    public class PersonnelListBkDao : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
    }

    /// <summary>
    /// 扣除和所得申报列表
    /// </summary>
    public class DeductionIncomeListBkDao : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 自然人人员信息采集Id
        /// </summary>
        public long PersonnelId { get; set; }
    }

    /// <summary>
    /// 人员信息采集
    /// </summary>
    public class PersonnelDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }



        /// <summary>
        /// 部门
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件类型  0身份证 1护照
        /// </summary>
        public int DocumentType { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string CertificateNum { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public long MobilePhone { get; set; }

        /// <summary>
        /// 国籍（地区）
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        ///雇员  0否 1是
        /// </summary>
        public int Employee { get; set; }

        /// <summary>
        /// 残疾烈属孤老  0否 1是
        /// </summary>
        public int Lonely { get; set; }

        /// <summary>
        /// 股东、投资人  0否 1是
        /// </summary>
        public int Investor { get; set; }

        /// <summary>
        /// 特定行业  0否 1是
        /// </summary>
        public int SpecificIndustries { get; set; }

        /// <summary>
        /// 天使投资人  0否 1是
        /// </summary>
        public int AngelInvestors { get; set; }



        /// <summary>
        /// 配偶情况 0没 1有
        /// </summary>
        public int Spouse { get; set; }

        /// <summary>
        /// 配偶姓名
        /// </summary>
        public string SpouseName { get; set; }

        /// <summary>
        /// 配偶证件类型  0身份证 1护照
        /// </summary>
        public int SpouseDocumentType { get; set; }

        /// <summary>
        /// 配偶证件号码
        /// </summary>
        public string SpouseCertificateNum { get; set; }



        //综合所得申请和三方协议
        /// <summary>
        /// 所得期间起（自动：计算所得期间止，缴款期限日期）
        /// </summary>
        public string StartTime { get; set; }

        //三方协议
        /// <summary>
        /// 应扣缴税款（ 应补（退）税款）
        /// </summary>
        public decimal Withholding { get; set; }
    }

    /// <summary>
    /// 子女教育支出
    /// </summary>
    public class ChildrenDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }



        /// <summary>
        /// 子女姓名
        /// </summary>
        public string ChildrenName { get; set; }

        /// <summary>
        /// 子女证件类型  0身份证 1护照
        /// </summary>
        public int ChildType { get; set; }

        /// <summary>
        /// 子女证件号码
        /// </summary>
        public string ChildNumber { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// 国籍（地区）
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 当前受教育阶段 0义务教育 1高中阶段教育 2学前教育阶段
        /// </summary>
        public int CurrentEducation { get; set; }

        /// <summary>
        /// 受教育日期起
        /// </summary>
        public string EducationBegins { get; set; }

        /// <summary>
        /// 受教育日期止
        /// </summary>
        public string EducationEnd { get; set; }

        /// <summary>
        /// 教育终止时间
        /// </summary>
        public string EducationTimeEnd { get; set; }

        /// <summary>
        /// 就读国家
        /// </summary>
        public string CountryStudy { get; set; }

        /// <summary>
        /// 当前就读学校
        /// </summary>
        public string CurrentSchool { get; set; }

        /// <summary>
        /// 本人扣除比例
        /// </summary>
        public string DeductionProportion { get; set; }
    }

    /// <summary>
    /// 继续教育支出
    /// </summary>
    public class ContinueDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }




        /// <summary>
        ///入学时间起
        /// </summary>
        public string AdmissionTime { get; set; }

        /// <summary>
        /// (预计)毕业时间
        /// </summary>
        public string GraduationTime { get; set; }

        /// <summary>
        /// 教育阶段 0大学专科 1大学本科 2硕士研究生 3博士研究生
        /// </summary>
        public int EducationStage { get; set; }



        /// <summary>
        /// 继续教育类型 0技能人员职业资格 1专业技术人员职业资格
        /// </summary>
        public int EducationTypes { get; set; }

        /// <summary>
        /// 发证(批准)日期
        /// </summary>
        public string IssueDate { get; set; }

        /// <summary>
        /// 证书名称
        /// </summary>
        public string CertificateName { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 发证机关
        /// </summary>
        public string IssuingAuthority { get; set; }
    }

    /// <summary>
    /// 住房贷款利息支出
    /// </summary>
    public class InterestDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }



        /// <summary>
        ///房屋坐落地址
        /// </summary>
        public string LocationHouse { get; set; }

        /// <summary>
        ///房屋楼牌号
        /// </summary>
        public string BuildingNumber { get; set; }

        /// <summary>
        ///房屋证书类型 0房屋所有权证 1房屋预售合同 2不动产权证 3房屋买卖合同
        /// </summary>
        public int HouseType { get; set; }

        /// <summary>
        ///房屋证书号
        /// </summary>
        public string HousingCertificate { get; set; }

        /// <summary>
        ///本人是否借款人 0是 1不是
        /// </summary>
        public int Borrower { get; set; }

        /// <summary>
        ///是否婚前各自首套贷款，且婚后分别扣除50% 0否 1是
        /// </summary>
        public int Premarital { get; set; }

        /// <summary>
        ///贷款类型 0商业贷款 1公积金贷款
        /// </summary>
        public int LoanType { get; set; }

        /// <summary>
        ///贷款合同编号
        /// </summary>
        public string LoanContractNo { get; set; }

        /// <summary>
        ///贷款银行
        /// </summary>
        public string LendingBank { get; set; }

        /// <summary>
        ///首次还款日期
        /// </summary>
        public string FirstRepaymentDate { get; set; }

        /// <summary>
        ///贷款期限(月数)
        /// </summary>
        public string LoanTerm { get; set; }
    }

    /// <summary>
    /// 住房租金支出
    /// </summary>
    public class RentDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }



        /// <summary>
        ///工作城市
        /// </summary>
        public string WorkingCity { get; set; }

        /// <summary>
        ///出租房类型 0自然人 1组织
        /// </summary>
        public int RentalType { get; set; }

        /// <summary>
        ///房屋坐落地址
        /// </summary>
        public string HouseLocation { get; set; }

        /// <summary>
        ///房屋坐落楼牌号
        /// </summary>
        public string BuildingBrand { get; set; }

        /// <summary>
        ///租赁日期起
        /// </summary>
        public string FromLeaseDate { get; set; }

        /// <summary>
        ///租赁日期止
        /// </summary>
        public string EndLeaseDate { get; set; }
    }

    /// <summary>
    /// 赡养老人支出
    /// </summary>
    public class SupportDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }



        /// <summary>
        ///是否独生子女 0是 1否
        /// </summary>
        public int Only { get; set; }

        /// <summary>
        ///分摊方式
        /// </summary>
        public string Share { get; set; }

        /// <summary>
        ///本年度月扣除金额
        /// </summary>
        public string MonthlyDeduction { get; set; }



        /// <summary>
        ///被养姓名
        /// </summary>
        public string FosterName { get; set; }

        /// <summary>
        ///被养身份证件类型
        /// </summary>
        public string FosterDocumentType { get; set; }

        /// <summary>
        ///被养身份证号码
        /// </summary>
        public string FosterCertificateNum { get; set; }

        /// <summary>
        ///被养国籍地区
        /// </summary>
        public string FosterNationality { get; set; }

        /// <summary>
        ///关系
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        ///出生日期
        /// </summary>
        public string DateOfBirth { get; set; }



        /// <summary>
        ///共养姓名
        /// </summary>CoCulture
        public string CoCultureName { get; set; }

        /// <summary>
        ///共养身份证件类型
        /// </summary>
        public string CoCultureTypes { get; set; }

        /// <summary>
        ///共养身份证件号码
        /// </summary>
        public string CoCultureNum { get; set; }

        /// <summary>
        ///共养国籍（地区）
        /// </summary>
        public string CoCultureNationality { get; set; }
    }

    /// <summary>
    /// 综合所得申报
    /// </summary>
    public class IncomeDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 人员Id
        /// </summary>
        public long PersonnelId { get; set; }



        /// <summary>
        /// 应发工资合计
        /// </summary>
        public decimal Wages { get; set; }



        /// <summary>
        /// 养老保险
        /// </summary>
        public decimal Pension { get; set; }

        /// <summary>
        /// 医疗保险
        /// </summary>
        public decimal MedicalCare { get; set; }

        /// <summary>
        /// 失业保险
        /// </summary>
        public decimal Unemployment { get; set; }



        /// <summary>
        /// 住房公积金
        /// </summary>
        public decimal AccumulationFund { get; set; }



        /// <summary>
        /// 累计子女教育
        /// </summary>
        public decimal AccumulatedChildren { get; set; }

        /// <summary>
        /// 累计住房贷款利息
        /// </summary>
        public decimal AccumulatedInterest { get; set; }

        /// <summary>
        /// 累计租房租金
        /// </summary>
        public decimal AccumulatedRental { get; set; }

        /// <summary>
        /// 累计赡养老人
        /// </summary>
        public decimal AccumulatedSupport { get; set; }

        /// <summary>
        /// 累计继续教育
        /// </summary>
        public decimal AccumulatedContinue { get; set; }

        /// <summary>
        /// 当月个税预扣预缴税额
        /// </summary>
        public decimal WithholdingTax { get; set; }



        /// <summary>
        /// 实发工资
        /// </summary>
        public decimal NetSalary { get; set; }
    }

    /// <summary>
    /// 删除
    /// </summary>
    public class IdDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 类型 
        /// 1人员信息采集 专项附加扣除信息采集(2子女教育支出3继续教育支出,4住房贷款利息支出,5住房租金支出,6赡养老人支出) 7综合所得申报
        /// </summary>
        public int Type { get; set; }
    }

    #region 扩展
    /// <summary>
    /// 纳税人信息
    /// </summary>
    public class TaxpayerInfoDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件类型  0身份证 1护照
        /// </summary>
        public int DocumentType { get; set; }

        /// <summary>
        /// 性别  0男 1女
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public string Profession { get; set; }

        /// <summary>
        /// 纳税识别号
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// 残疾证号
        /// </summary>
        public string DisabilityCard { get; set; }

        /// <summary>
        /// 国籍（地区）
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string CertificateNum { get; set; }

        /// <summary>
        /// 出生年月
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// 学历  0大学本科以下 1大学本科 2研究生
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 是否存在以下情形  0残疾 1烈属 2孤老
        /// </summary>
        public int DoesItExist { get; set; }

        /// <summary>
        /// 烈属证号
        /// </summary>
        public string MartyrNo { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }



        /// <summary>
        /// 经常居住地
        /// </summary>
        public string HabitualResidence { get; set; }

        /// <summary>
        /// 户籍地址
        /// </summary>
        public string PermanentAddress { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public long MobilePhone { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string BankOfDeposit { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 银行账号
        /// </summary>
        public long BankAccount { get; set; }



        /// <summary>
        /// 任职受雇日期
        /// </summary>
        public string Tenure { get; set; }

        /// <summary>
        ///雇员  0否 1是
        /// </summary>
        public int Employee { get; set; }

        /// <summary>
        /// 股东、投资人  0否 1是
        /// </summary>
        public int Investor { get; set; }

        /// <summary>
        /// 公司股本总额
        /// </summary>
        public decimal TotalShareCapital { get; set; }

        /// <summary>
        /// 工号
        /// </summary>
        public string JobNo { get; set; }

        /// <summary>
        /// 离职日期
        /// </summary>
        public string DepartureDate { get; set; }

        /// <summary>
        /// 特定行业  0否 1是
        /// </summary>
        public int SpecificIndustries { get; set; }

        /// <summary>
        /// 天使投资人  0否 1是
        /// </summary>
        public int AngelInvestors { get; set; }

        /// <summary>
        /// 个人投资总额
        /// </summary>
        public decimal TotalInvestment { get; set; }
    }
    #endregion
}
