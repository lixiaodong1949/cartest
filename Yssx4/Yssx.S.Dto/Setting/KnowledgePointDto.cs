﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 知识点
    /// </summary>
    public class KnowledgePointDto : BaseDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 父级知识点
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
    }
}
