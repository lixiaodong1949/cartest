﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 生成实习证书
    /// </summary>
    public class AddInternshipProveDto
    {
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string SN { get; set; }

        /// <summary>
        /// 证书地址
        /// </summary>
        public string Url { get; set; }

    }
}
