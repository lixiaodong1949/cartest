﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位实训任务请求实体
    /// </summary>
    public class JobTrainingTaskDto
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型 0:作业 1:考试
        /// </summary>
        public Nullable<int> ExamType { get; set; }

        /// <summary>
        /// 训练开始时间
        /// </summary>
        public Nullable<DateTime> TrainStartTime { get; set; }

        /// <summary>
        /// 训练结束时间
        /// </summary>
        public Nullable<DateTime> TrainEndTime { get; set; }

        /// <summary>
        /// 任务密码
        /// </summary>
        public string TaskPassword { get; set; }

        /// <summary>
        /// 实训类型 0:综合岗位实训 1:分岗位实训
        /// </summary>
        public Nullable<int> TrainType { get; set; }

        /// <summary>
        /// 分岗位实训组数
        /// </summary>
        public Nullable<int> GroupCount { get; set; }

        /// <summary>
        /// 答案与解析显示类型 0:完成后显示 1:答题中显示
        /// </summary>
        public Nullable<int> AnswerShowType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 实训套题列表
        /// </summary>
        public List<JobTrainingTaskTopicDto> TopicList { get; set; } = new List<JobTrainingTaskTopicDto>();

        /// <summary>
        /// 实训班级列表
        /// </summary>
        public List<JobTrainingTaskClassDto> ClassList { get; set; } = new List<JobTrainingTaskClassDto>();

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看
        /// </summary>
        public bool IsCanCheckExam { get; set; }
    }

    /// <summary>
    /// 岗位实训任务套题
    /// </summary>
    public class JobTrainingTaskTopicDto
    {
        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }

        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }
    }

    /// <summary>
    /// 岗位实训任务班级
    /// </summary>
    public class JobTrainingTaskClassDto
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
