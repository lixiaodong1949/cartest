using System;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.S.Dto.BigDatas;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Mall;
using Yssx.S.Dto.Order;
using Yssx.S.Dto.Topics;
using Yssx.S.Pocos;
using Yssx.S.Pocos.BackManage;
using Yssx.S.Pocos.BigData;
using Yssx.S.Pocos.Mall;
using Yssx.S.Pocos.OaxTable;
using Yssx.S.Pocos.Order;
using Yssx.S.Pocos.Topics;

namespace Yssx.S.Dto
{
    public class DtoAutoMapperProfile : AutoMapperProfile
    {
        public DtoAutoMapperProfile()
        {
            this.BidirectMapIgnoreAllNonExisting<YssxCase, CaseRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, SimpleQuestionRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, ComplexQuestionRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, MainSubQuestionRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, CertificateTopicRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, ExamQuestionInfo>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, SubQuestion>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, TopicInfoDto>().
            BidirectMapIgnoreAllNonExisting<YssxTopic, TopicListView>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopic, CertificateTopicRequest>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopic, SubQuestion>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateDataRecord, CertificateDataRecord>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateDataRecord, CertificateTopicRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, SimpleQuestionPubRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, ComplexQuestionPubRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, MainSubQuestionPubRequest>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, SubQuestionPub>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, TopicPubInfoDto>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, CertificateTopicPubRequest>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicPublic, CertificateTopicPubRequest>().
            BidirectMapIgnoreAllNonExisting<YssxPubCertificateDataRecord, PubCertificateDataRecord>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicPublic, SubQuestionPub>().
            BidirectMapIgnoreAllNonExisting<UserTicket, WeChartTicket>().
            //BidirectMapIgnoreAllNonExisting<YssxExamCertificateDataRecord, AccountEntryDTO>().
            //BidirectMapIgnoreAllNonExisting<CaseApply, CaseApplyRequest>().
            //BidirectMapIgnoreAllNonExisting<CaseOrder, CaseOrderRequest>().
            //BidirectMapIgnoreAllNonExisting<YssxSchoolCase, SchoolCaseRequest>().
            //BidirectMapIgnoreAllNonExisting<ExamSchoolCompetitionTask, SchoolCompetitionTaskRequest>().
            //BidirectMapIgnoreAllNonExisting<ExamProvincialCompetitionTask, ProvincialCompetitionTaskRequest>().
            //BidirectMapIgnoreAllNonExisting<ProUserInfo, UserTicket>().
            BidirectMapIgnoreAllNonExisting<YssxSchool, SchoolDto>().
            BidirectMapIgnoreAllNonExisting<YssxCollege, CollegeDto>().
            BidirectMapIgnoreAllNonExisting<YssxUser, YssxbkUserDao>().
            BidirectMapIgnoreAllNonExisting<YssxStamp, StampRequest>().
            BidirectMapIgnoreAllNonExisting<YssxDepartment, DepartmentRequest>().
            BidirectMapIgnoreAllNonExisting<YssxPostPersonnel, PostPersonRequest>().
            BidirectMapIgnoreAllNonExisting<YssxBanner, BannerRequest>().
            BidirectMapIgnoreAllNonExisting<CaseOrder, CaseOrderRequest>().
            BidirectMapIgnoreAllNonExisting<YssxSchoolCase, SchoolCaseRequest>().
            BidirectMapIgnoreAllNonExisting<YssxSection, YssxPrepareSection>().
            BidirectMapIgnoreAllNonExisting<YssxSectionTopic, YssxPrepareSectionTopic>().
            BidirectMapIgnoreAllNonExisting<YssxSectionSceneTraining, YssxPrepareSectionSceneTraining>().
            BidirectMapIgnoreAllNonExisting<YssxSectionTextBook, YssxPrepareSectionTextBook>().
            BidirectMapIgnoreAllNonExisting<YssxSectionFiles, YssxPrepareSectionFiles>().
            BidirectMapIgnoreAllNonExisting<YssxSectionSummary, YssxPrepareSectionSummary>().
            BidirectMapIgnoreAllNonExisting<PrepareCourseDto, YssxPrepareCourse>().
            BidirectMapIgnoreAllNonExisting<YssxPrepareSection, YssxPrepareSectionViewModel>().
            BidirectMapIgnoreAllNonExisting<PrepareSectionTopicDto, YssxPrepareSectionTopic>().
            BidirectMapIgnoreAllNonExisting<PrepareSectionSceneTrainingDto, YssxPrepareSectionSceneTraining>().
            BidirectMapIgnoreAllNonExisting<PrepareSectionTextBookDto, YssxPrepareSectionTextBook>().
            BidirectMapIgnoreAllNonExisting<YssxPrepareSectionTopic, YssxPrepareSectionTopicViewModel>().
            BidirectMapIgnoreAllNonExisting<YssxPrepareSectionTextBook, YssxPrepareSectionTextBookViewModel>().
            BidirectMapIgnoreAllNonExisting<PrepareSectionFilesDto, YssxPrepareSectionFiles>().
            BidirectMapIgnoreAllNonExisting<YssxPrepareSectionFiles, YssxPrepareSectionFilesViewModel>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, ExamQuestionInfo>().
            BidirectMapIgnoreAllNonExisting<ClassInfo, YssxTeacherCourseClass>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublic, ExamCourseQuestionInfo>().
            BidirectMapIgnoreAllNonExisting<AccountEntryDTO, YssxExamCourseCertificateDataRecord>().
            BidirectMapIgnoreAllNonExisting<AccountEntryDTO, YssxExamCertificateDataRecord>().
            BidirectMapIgnoreAllNonExisting<YssxClassRecordViewModel, ExportYssxClassRecordViewModel>().
            BidirectMapIgnoreAllNonExisting<JobTrainingTaskClassDto, YssxJobTrainingTaskClass>().
            BidirectMapIgnoreAllNonExisting<JobTrainingTaskTopicDto, YssxJobTrainingTaskTopic>().
            BidirectMapIgnoreAllNonExisting<YssxCourseClassStudentViewModel, StudentData>().
            BidirectMapIgnoreAllNonExisting<YssxCourseClassStudentViewModel, AllStudentData>().
            BidirectMapIgnoreAllNonExisting<YssxTask, TaskInfoDto>().
            BidirectMapIgnoreAllNonExisting<YssxTask, TaskGradeReportDto>().
            BidirectMapIgnoreAllNonExisting<TeacherClassRequestModel, YssxTeacherPlanningRelation>().
            BidirectMapIgnoreAllNonExisting<StudentAutoTrainingFinishedViewModel, ExportStudentAutoTrainingFinishedViewModel>().
            BidirectMapIgnoreAllNonExisting<RtpOrderDto, SxRtpOrder>().
            BidirectMapIgnoreAllNonExisting<CreateRtpOrderDto, SxRtpOrder>().
            BidirectMapIgnoreAllNonExisting<YssxMallProduct, SaveProductDto>().
            BidirectMapIgnoreAllNonExisting<YssxMallOrder, MallOrderDto>().
            BidirectMapIgnoreAllNonExisting<YssxMallProduct, MallProductDto>().
            BidirectMapIgnoreAllNonExisting<YssxMallProduct, CreateMallProductDto>().
            BidirectMapIgnoreAllNonExisting<YssxMallProductItem, CreateMallProductItemDto>().
            BidirectMapIgnoreAllNonExisting<OaxGradeRecord, OaxGradeRecordRequestDto>().
            BidirectMapIgnoreAllNonExisting<YssxTaskCart, TaskCartRequest>().
            BidirectMapIgnoreAllNonExisting<YssxCourseTask, CourseTaskInfoDto>().
            BidirectMapIgnoreAllNonExisting<YssxCourseTask, CourseTaskGradeReportDto>().

            



            #region 同步实训

            BidirectMapIgnoreAllNonExisting<YssxCaseNew, CaseDtoNew>().
            BidirectMapIgnoreAllNonExisting<YssxDrillType, DrillTypeDto>().
            BidirectMapIgnoreAllNonExisting<YssxFlow, FlowDto>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, SimpleQuestionRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, ComplexQuestionRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, MainSubQuestionRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, ExamQuestionInfoNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, SubQuestionNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, TopicInfoDtoNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, CertificateTopicRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicNew, CertificateTopicRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicNew, SubQuestionNew>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateDataRecordNew, CertificateDataRecordNew>().
            BidirectMapIgnoreAllNonExisting<YssxAnswerOptionNew, ChoiceOptionNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicNew, TopicInfoDto>().
            BidirectMapIgnoreAllNonExisting<YssxTopicFileNew, QuestionFileNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, CertificateTopicPubRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxPubCertificateDataRecordNew, PubCertificateDataRecordNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, SimpleQuestionPubRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, ComplexQuestionPubRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, MainSubQuestionPubRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, TopicPubInfoDtoNew>().
            BidirectMapIgnoreAllNonExisting<YssxTopicPublicNew, SubQuestionPubNew>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicPublicNew, SubQuestionPubNew>().
            BidirectMapIgnoreAllNonExisting<YssxCertificateTopicPublicNew, CertificateTopicPubRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxStampNew, StampRequestNew>().
            BidirectMapIgnoreAllNonExisting<YssxDepartmentNew, DepartmentDtoNew>().
            BidirectMapIgnoreAllNonExisting<YssxExamCertificateDataRecordNew, AccountEntryDtoNew>().

            #endregion

            #region 大数据
            BidirectMapIgnoreAllNonExisting<YssxBalanceSheet, BalanceSheetDto>().//大数据负债//

            BidirectMapIgnoreAllNonExisting<YssxBdActiveBondStatistics, ActiveBondStatisticsDto>().//活跃债卷统计

            BidirectMapIgnoreAllNonExisting<YssxBdAnnuityFinalValueCoefficient, AnnuityFinalValueCoefficientDto>().//年金终值系数

            BidirectMapIgnoreAllNonExisting<YssxBdAnnuityPresentValueFactor, AnnuityPresentValueFactorDto>().//年金现值系数

            BidirectMapIgnoreAllNonExisting<YssXBdAShareIndex000002, AShareIndex000002Dto>().//A股指数000002

            BidirectMapIgnoreAllNonExisting<YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions, BenchmarkRMBDepositRateOfFinancialInstitutionsDto>().//金融机构人民币存款基准利率

            BidirectMapIgnoreAllNonExisting<YssxBdBenchmarkRMBLendingRateForFinancialInstitutions, BenchmarkRMBLendingRateForFinancialInstitutionsDto>().//金融机构人民币贷款基准利率

            BidirectMapIgnoreAllNonExisting<YssxBdBondsAreDebt2017, BondsAreDebt2017Dto>().//国债

            BidirectMapIgnoreAllNonExisting<YssxBdBShareIndex000003, BShareIndex000003Dto>().//B股指数000003

            BidirectMapIgnoreAllNonExisting<YssxBdComponentBShareIndex390003, ComponentBShareIndex390003Dto>().//成份B股指数390003

            BidirectMapIgnoreAllNonExisting<YssxBdCompoundFinalValueCoefficient, CompoundFinalValueCoefficientDto>().//复利终值系数

            BidirectMapIgnoreAllNonExisting<YssxBdCorporateBonds, CorporateBondsDto>().//企业债

            BidirectMapIgnoreAllNonExisting<YssxBdDisclosureOfFinancialIndicators, DisclosureOfFinancialIndicatorsDto>().//披露财务指标

            BidirectMapIgnoreAllNonExisting<YssxBdLPRData, LPRDataDto>().//LPR数据

            BidirectMapIgnoreAllNonExisting<YssxBdLPRMeanData, LPRMeanDataDto>().//LPR均值数据

            BidirectMapIgnoreAllNonExisting<YssxBdMediumTermNotes2017, MediumTermNotes2017Dto>().//中期票据(AAA)2017

            BidirectMapIgnoreAllNonExisting<YssxBdOtherIndicators, OtherIndicatorsDto>().//其他指数

            BidirectMapIgnoreAllNonExisting<YssxBdPolicyBasedFinancialBonds2017, PolicyBasedFinancialBondsDto>().//政策性金融债（国开行）2017

            BidirectMapIgnoreAllNonExisting<YssxBdPolicyFinancialBond2017, ImportAndExportBanksAndIssuanceOfPolicyBasedFinancialBondsDto>().//政策性金融债（进出口行、开发行）2017

            BidirectMapIgnoreAllNonExisting<YssxBdProfit, ProfitDto>().//利润表

            BidirectMapIgnoreAllNonExisting<YssxBdProfitSharing, DividendDistributionModelDto>().//红利分配表

            BidirectMapIgnoreAllNonExisting<YssxBdReferenceRateOfRMB2014, ReferenceRateOfRMB2014Dto>().//人民币参考汇率_2014

            BidirectMapIgnoreAllNonExisting<YssxBdReferenceRateOfRMB2015, ReferenceRateOfRMB2015Dto>().//人民币参考汇率_2015

            BidirectMapIgnoreAllNonExisting<YssxBdReferenceRateOfRMB2016, ReferenceRateOfRMB2016Dto>().//人民币参考汇率_2016

            BidirectMapIgnoreAllNonExisting<YssxBdReferenceRateOfRMB2017, ReferenceRateOfRMB2017Dto>().//人民币参考汇率_2017

            BidirectMapIgnoreAllNonExisting<YssxBdReferenceRateOfRMB2018, ReferenceRateOfRMB2018Dto>().//人民币参考汇率_2018

            BidirectMapIgnoreAllNonExisting<YssxBdRMBExchangRateIndex, RMBExchangRateIndexDto>().//人民币汇率指数

            BidirectMapIgnoreAllNonExisting<YssxBdShenZhenComponentIndex390001, ShenZhenComponentIndex390001Dto>().//深证成份指数390001

            BidirectMapIgnoreAllNonExisting<YssxBdShenzhenCompositeIndex399106Model, ShenzhenCompositeIndex399106Dto>().//深证综指399106

            BidirectMapIgnoreAllNonExisting<YssxBdShenZhenStockIndexR390002, ShenZhenStockIndexR390002Dto>().//深证成指R390002

            BidirectMapIgnoreAllNonExisting<YssxBdShiborData, ShiborDataDto>().//Shibor数据

            BidirectMapIgnoreAllNonExisting<YssxBdShiborMeanData, ShiborMeanDataDto>().//Shibor均值数据

            BidirectMapIgnoreAllNonExisting<YssxBdShiborQuoteData, ShiborQuoteDataDto>().//Shibor报价数据

            BidirectMapIgnoreAllNonExisting<YssxBdShortTermFinancingDebt2017, ShortTermFinancingDebt2017Dto>().//短期融资债2017

            BidirectMapIgnoreAllNonExisting<YssxBdStatisticalDataCPI, StatisticalDataCPIDto>().//统计数据CPI

            BidirectMapIgnoreAllNonExisting<YssxBdTheAverageMonthlyExchangeRateOfRMB, TheAverageMonthlyExchangeRateOfRMBDto>().//人民币月平均汇率

            BidirectMapIgnoreAllNonExisting<YssxBdTheCentralParityOfRMBExchangeRate, TheCentralParityOfRMBExchangeRateDto>().//人民币汇率中间价

            BidirectMapIgnoreAllNonExisting<YssxBdTheChinextIndexIs399006, TheChinextIndexIs399006Dto>().//创业板指数399006

            BidirectMapIgnoreAllNonExisting<YssxBdTheCompoundPresentValueFactor, TheCompoundPresentValueFactorDto>().//复利现值系数

            BidirectMapIgnoreAllNonExisting<YssxBdTheCsi300IndexIs399300, TheCsi300IndexIs399300Dto>().//沪深300指数399300

            BidirectMapIgnoreAllNonExisting<YssxBdTheShanghaiCompositeIndexIs000001, TheShanghaiCompositeIndexIs000001Dto>().//上证综指000001

            BidirectMapIgnoreAllNonExisting<YssxBdTheYieldTreasuryBonds2017Model, TheYieldTreasuryBonds2017Dto>()//国债2017
                //.BidirectMapIgnoreAllNonExisting<YssxIntegral, IntegralDto>()
                .BidirectMapIgnoreAllNonExisting<YssxIntegralDetails, IntegralDetailsDto>()
                .BidirectMapIgnoreAllNonExisting<YssxIntegralProduct, IntegralProductDto>()
                //.BidirectMapIgnoreAllNonExisting<YssxIntegralProductExchange, IntegralProductExchangeDto>()
                .BidirectMapIgnoreAllNonExisting<YssxUserAddress, UserAddressDto>()
                .BidirectMapIgnoreAllNonExisting<YssxSigninDetails, SigninDto>();
            #endregion
        }
    }
}
