﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课请求实体
    /// </summary>
    public class CoursePrepareRequestModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 备课名称
        /// </summary>
        public string CoursePrepareName { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
}
