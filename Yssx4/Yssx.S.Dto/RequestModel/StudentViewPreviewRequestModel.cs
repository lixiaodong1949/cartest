﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  学生查看课程预习请求实体
    /// </summary>
    public class StudentViewPreviewRequestModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习Id
        /// </summary>
        public long CoursePreviewId { get; set; }

    }
}
