﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程学期请求实体
    /// </summary>
    public class PrepareCourseSemesterDto
    {
        /// <summary>
        /// 备课课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 学期Id
        /// </summary>
        public long SemesterId { get; set; }
    }
}
