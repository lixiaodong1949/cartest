﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程知识点请求实体
    /// </summary>
    public class CourseKnowledgePointDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程知识点列表
        /// </summary>
        public List<CourseKnowledgePointInfo> KnowledgePointList { get; set; } = new List<CourseKnowledgePointInfo>();
    }

    /// <summary>
    /// 课程知识点详情
    /// </summary>
    public class CourseKnowledgePointInfo
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Nullable<long> Id { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointName { get; set; }
    }

}
