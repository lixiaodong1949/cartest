﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程章节教材请求实体
    /// </summary>
    public class PrepareSectionTextBookDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 教材描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 知识点(冗余字段)
        /// </summary>
        public string KnowledgePointId { get; set; }
    }
}
