﻿using System;
using System.Collections.Generic;
using System.Text;
using Zdap.MQ.Messaging;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 发布课堂测验任务消息体
    /// </summary>
    [Topic("PublishCourseTestTask")]
    public class PublishCourseTestTaskMessage: Message
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
    }
}
