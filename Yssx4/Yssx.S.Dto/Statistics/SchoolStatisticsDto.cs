﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Statistics
{
    /// <summary>
    /// 
    /// </summary>
    public class SchoolStatisticsDto
    {
        public long TeacherCount { get; set; }

        public long StudentCount { get; set; }

        public long ClassCount { get; set; }


        public long SchoolCount { get; set; }

        public string SchoolType { get; set; }

        public string Regtion { get; set; }
    }

    public class SchoolStatisticsListDto
    {
        public long Id { get; set; }

        public string SchoolName { get; set; }

        public long Teacher { get; set; }

        public long Student { get; set; }

        public long Class { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
