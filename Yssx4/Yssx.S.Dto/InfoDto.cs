﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class InfoDto
    {
        public long Id { get; set; }

        public int MassgeType { get; set; }

        public string TitleImg { get; set; }

        public string TagJson { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public DateTime PublishDate { get; set; }

        public string Content { get; set; }
    }
}
