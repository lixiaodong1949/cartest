using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品 请求Dto
    /// </summary>
    public class IntegralProductListRequestDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}