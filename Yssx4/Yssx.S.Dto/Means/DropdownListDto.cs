﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 下拉Dto
    /// </summary>
   public class DropdownListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
