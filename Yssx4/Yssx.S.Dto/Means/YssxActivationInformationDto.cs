﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 激活信息DTO
    /// </summary>
    public class YssxActivationInformationDto
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Nmae { get; set; }
        /// <summary>
        /// 工号或者学号
        /// </summary>
        public string WorkNumber { get; set; }
        /// <summary>
        /// 院系
        /// </summary>
        public long Departments { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        public long Professional { get; set; }
        /// <summary>
        /// 班级
        /// </summary>
        public long? Class { get; set; }
        /// <summary>
        /// 类型(用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户)
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 激活码Id
        /// </summary>
        public long ActivationCodeId { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }


        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
    }
}
