﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 用户信息dto
    /// </summary>
    public class UserInformationDto
    {
        /// <summary>
        /// 用户账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealNam { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NikeName { get; set; }
    }
}
