﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 学校激活信息dto
    /// </summary>
    public class ActivationRecordDto
    {
        /// <summary>
        /// 记录Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 激活码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public int UserType { get; set; }

        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 工号或者学号
        /// </summary>
        public string WorkNumber { get; set; }
        /// <summary>
        /// 注册类型(1,用户激活，2，后台添加)
        /// </summary>
        public int RegistrationType { get; set; }

        /// <summary>
        /// 激活时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 激活截至日期
        /// </summary>
        public DateTime ActivationDeadline { get; set; }
        /// <summary>
        /// 用户状态 0禁用1启用2激活码禁用
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 激活码Id
        /// </summary>
        public long ActivationCodeId { get; set; }

        /// <summary>
        /// 学校Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary>
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary>
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 学校类型 -1:无效值 0:中专 1:大专 2:本科
        /// </summary>
        public int SchoolType { get; set; }
    }

    /// <summary>
    /// 激活返回信息dto
    /// </summary>
    public class FillInActivAtionInformationDto
    {
        //激活码，真实姓名，角色，学号/工号，添加方式，激活时间，有效期截止日。状态，用户Id,激活码Id,激活记录Id

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 工号或者学号
        /// </summary>
        public string WorkNumber { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }
    }
}
