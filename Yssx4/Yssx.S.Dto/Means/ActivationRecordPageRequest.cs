﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 搜索条件
    /// </summary>
    public class ActivationRecordPageRequest : PageRequest
    {
        /// <summary>
        /// 姓名/工学号/激活码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 角色(-1角色,1学生,2教师,3教务)
        /// </summary>
        public int? TypeId { get; set; }
        /// <summary>
        /// 添加方式(0,添加方式，1,用户激活，2，后台添加)
        /// </summary>
        public int? RegistrationType { get; set; }
        /// <summary>
        /// 状态（-1状态，0禁用1启用2激活码禁用，3激活码已到期）
        /// </summary>
        public int? Status { get; set; }
    }
}
