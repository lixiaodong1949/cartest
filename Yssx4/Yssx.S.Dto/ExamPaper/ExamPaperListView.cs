﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 自主练习列表显示实体 
    /// </summary>
    public class PracticeExamListView
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        public string CaseYear { get; set; }

        /// <summary>
        /// 参考次数
        /// </summary>
        public int ExamCount { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 已做题数
        /// </summary>
        public int DoQuestionCount { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 成绩id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 创建年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public StudentExamStatus Status { get; set; }
        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 公司背景图
        /// </summary>
        public string BGFileUrl { get; set; }
        /// <summary>
        /// 实训描述
        /// </summary>
        public string SxInfo { get; set; }

        /// <summary>
        /// 所属行业
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 所属行业ID
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// true开(学生不可见) false关(学生可见)
        /// </summary>
        public bool IsRelease { get; set; }

        /// <summary>
        /// 题目 false开 true关
        /// </summary>
        public bool Close { get; set; }

        /// <summary>
        /// 答案 false开 true关
        /// </summary>
        public bool AnswerClose { get; set; }
    }

    /// <summary>
    /// 全真模拟列表显示实体
    /// </summary>
    public class EmulationExamListView
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 考试类型 
        /// </summary>
        public ExamType ExamType { get; set; }

        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）
        /// </summary>
        public CompetitionType CompetitionType { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        public string CaseYear { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 成绩id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal GradeScore { get; set; }

        /// <summary>
        /// 已参考次数（已结束 ）
        /// </summary>
        public int GradeCount { get; set; }

        /// <summary>
        /// 创建年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public StudentExamStatus Status { get; set; }

        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 分组状态
        /// </summary>
        public GroupStatus GroupStatus { get; set; }
        /// <summary>
        /// 大数据分析师时长
        /// </summary>
        public int BigDataTimeMinutes { get; set; }
        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }
    }

    /// <summary>
    /// 试卷中心 - 输出类
    /// </summary>
    public class PaperCenterListView
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例费用类型（0.免费、1.已付费、2.未付费）
        /// </summary>
        public int CaseFeeType { get; set; }

        /// <summary>
        /// 热度（人数）
        /// </summary>
        public int Heat { get; set; }

        /// <summary>
        /// 学生是否开放
        /// </summary>
        public bool IsOpenToStudent { get; set; }

        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主表ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public StudentExamStatus Status { get; set; } = StudentExamStatus.Wait;

    }

}
