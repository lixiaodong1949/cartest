﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    ///考试题目信息
    /// </summary>
    public class ExamQuestionInfo
    {
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 题目内容类型
        /// </summary>
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 计分方式
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <summary>
        /// 是否收藏--预留字段
        /// </summary>
        public bool IsCollection { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 作答区内容(表格模板信息)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TopicContent { get; set; }

        /// <summary>
        /// 得分 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 答题结果状态 0待处理（初始化状态） 1处理中 2对 3部分对 4错
        /// </summary>

        public AnswerResultStatus AnswerResultStatus { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<QuestionFile> QuestionFile { get; set; }

        /// <summary>
        /// 选择题，选项列表
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ChoiceOption> Options { get; set; }

        /// <summary>
        /// 用户作答信息
        /// </summary>
        public string StudentAnswer { get; set; }

        /// <summary>
        /// 作答记录与正确答案的对比结果
        /// </summary>
        public string AnswerCompareInfo { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 模板数据源内容(含答案)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FullContent { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AnswerValue { get; set; }

        /// <summary>
        /// 答案解析
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Hint { get; set; }

        /// <summary>
        /// 是否已结账
        /// </summary>
        public bool IsSettled { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 子题目
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ExamQuestionInfo> SubQuestion { get; set; }

        /// <summary>
        /// 凭证题目相关设置
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CertificateTopicView CertificateTopic { get; set; }

        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; }
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; }
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; }

        /// <summary>
        /// 当前岗位考试状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 大数据岗位是否已提交 
        /// </summary>
        public bool IsBigDataSubmit { get; set; }

        /// <summary>
        /// 考试剩余时长
        /// </summary>
        public long LeftSeconds { get; set; }

        /// <summary>
        /// false 不能提交，true 可以提交
        /// </summary>
        public bool IsCanSubmit { get; set; }

    }
}
