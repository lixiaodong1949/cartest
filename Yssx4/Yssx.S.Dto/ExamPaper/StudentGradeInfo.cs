﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 学生成绩信息
    /// </summary>
    public class StudentGradeInfo
    {
        /// <summary>
        /// 成绩Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 考试分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 考试用时
        /// </summary>
        public string UsedTime { get; set; }

        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// 答错题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 部分对数量
        /// </summary>
        public int PartRightCount { get; set; }

        /// <summary>
        /// 未答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 正确率
        /// </summary>
        public string CorrectRate
        {
            get
            {
                var total = CorrectCount + ErrorCount;
                if (total == 0)
                    return "--";
                else
                {
                    return CorrectCount * 100 / total + "%";
                }
            }

        }
        /// <summary>
        /// 是否及格
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public StudentExamStatus Status { get; set; }
    }
}
