﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 分组维护dto
    /// </summary>
    public class ExamPaperGroupUpdateDto
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 岗位id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

    }
}
