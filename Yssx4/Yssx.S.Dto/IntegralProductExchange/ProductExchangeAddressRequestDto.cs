using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 修改收货地址 Dto
    /// </summary>
    public class ProductExchangeAddressRequestDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        public long Id { get; set; }
        /// <summary>
        /// 用户收货地址
        /// </summary>
        public string ShippingAddress { get; set; }

        /// <summary>
        /// 用户收货人姓名
        /// </summary>
        public string ShippingName { get; set; }

        /// <summary>
        /// 用户收货人联系方式
        /// </summary>
        public string UserContact { get; set; }
    }
}