using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品兑换分页查询 请求Dto
    /// </summary>
    public class IntegralProductExchangePageRequestDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 发货状态：0未发货，1已发货 2已收货
        /// </summary>
        public int? DeliveryStatus { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        [JsonIgnore]
        public long UserId { get; set; }
    }
}