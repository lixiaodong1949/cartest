﻿namespace Yssx.S.Dto
{
    /// <summary>
    /// 订单入参
    /// </summary>
    public class CaseOrderRequest
    {
        /// <summary>
        /// 所属学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 所属学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
