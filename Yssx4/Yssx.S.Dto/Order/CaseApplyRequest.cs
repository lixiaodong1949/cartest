﻿using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 申请订单入参
    /// </summary>
    public class CaseApplyRequest
    {
        /// <summary>
        /// 申请订单Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 申请用户账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 申请用户角色  0教务 1教师
        /// </summary>
        public CaseUserTypeEnums CaseUserType { get; set; }
        /// <summary>
        /// 所属学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 所属学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 待跟进0, 跟进中1, 已开通2, 已关闭 3
        /// </summary>
        public ApplyStatus Status { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
