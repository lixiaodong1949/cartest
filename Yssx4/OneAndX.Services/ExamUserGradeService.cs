﻿using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;

namespace OneAndX.Services
{
    /// <summary>
    /// 考试成绩
    /// </summary>
    public class ExamUserGradeService : IExamUserGradeService
    {
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Add(ExamUserGradeAddDto dto, long currentUserId)
        {
            decimal passScore = 60M;//通过分数
            var data = dto.MapTo<ExamUserGrade>();
            data.Id = IdWorker.NextId();
            data.CreateBy = currentUserId; 
            data.UserId = currentUserId;
            data.InitialScore = dto.Score;
            data.PassScore = passScore;
            data.TenantId = CommonConstants.OneXTenantId;

            if (data.ExamPaperScore > 0 && data.ExamPaperScore != 100)
                data.Score = data.Score / data.ExamPaperScore * 100;//转换成百分制
            data.IsPass = data.Score >= passScore;

            //switch (dto.SourceSystem)
            //{
            //    case CaseSourceSystem.HYS://行业赛 案例总分为400，统一调整为100分
            //        int i = 4;
            //        dto.Score /= i;
            //        dto.ExamPaperScore /= i;
            //        break;
            //    case CaseSourceSystem.JS:
            //    default:
            //        break;
            //}

            var examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete).Where(e => e.Id == dto.ExamPlanId).FirstAsync();

            if (examPlan == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "考试计划不存在" };
            }
            var signUp = await DbContext.FreeSql.GetRepository<SignUp>(e => e.IsDelete == CommonConstants.IsNotDelete).Where(e => e.Id == currentUserId).FirstAsync();

            if (signUp == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "报名信息不存在" };
            }

            data.ExamTime = DateTime.Now.Date;// DateTime.Parse(examPlan.BeginTime.ToString("yyyy-MM-dd"));
            data.UserIDNumber = signUp.IDNumber;
            data.UserName = signUp.Name;
            data.ExamCardNumber = signUp.ExamCardNumber;
            data.Certificate = signUp.Certificate;
            data.CertificateLevel = signUp.CertificateLevel;

            var repo_ec = DbContext.FreeSql.GetRepository<ExamUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete);

            await repo_ec.InsertAsync(data);

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = data.Id.ToString() };
        }
        /// <summary>
        /// 按条件查询
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExamUserGradeListResponseDto>>> GetList(ExamUserGradeQueryRequestDto queryDto)
        {
            //按条件查询
            var selectData = await DbContext.FreeSql.GetRepository<ExamUserGrade>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(queryDto.Certificate > 0, e => e.Certificate == queryDto.Certificate)
                .WhereIf(queryDto.CertificateLevel > 0, e => e.CertificateLevel == queryDto.CertificateLevel)
                .WhereIf(!string.IsNullOrEmpty(queryDto.UserIDNumber), e => e.UserIDNumber == queryDto.UserIDNumber)
                .WhereIf(!string.IsNullOrEmpty(queryDto.UserName), e => e.UserName == queryDto.UserName)
                .OrderBy(m => m.ExamTime)
                .ToListAsync<ExamUserGradeListResponseDto>();

            return new ResponseContext<List<ExamUserGradeListResponseDto>> { Code = CommonConstants.SuccessCode, Data = selectData, };
        }

        public async Task<ResponseContext<RandomUserGradeDto>> GetRandomUser()
        {
            var selectData = await DbContext.FreeSql.GetRepository<ExamUserGrade>().Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.ExamPlanId == 1083200282525707).ToListAsync();
            if (selectData.Count == 0) return new ResponseContext<RandomUserGradeDto> { };
            
            int ranNumer = new Random().Next(0, selectData.Count);
            if (ranNumer > 0)
            {
                ranNumer -= 1;
            }
            ExamUserGrade data= selectData[ranNumer];

            return new ResponseContext<RandomUserGradeDto> { Data = new RandomUserGradeDto { IdNumber = data.UserIDNumber, Name = data.UserName } };
        }
    }
}
