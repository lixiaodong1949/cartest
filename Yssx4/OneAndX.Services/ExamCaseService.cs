﻿using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    public class ExamCaseService : IExamCaseService
    {
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(long id, long currentUserId)
        {
            //删除
            await DbContext.FreeSql.GetRepository<ExamCase>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(e => new ExamCase
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => e.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = id.ToString() };
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<PageResponse<ExamCaseListPageResponseDto>> GetListPage(ExamCaseQueryRequestDto queryDto)
        {
            long totalCount = 0;
            //按条件查询
            var selectData = await DbContext.FreeSql.GetRepository<ExamCase>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.Certificate == queryDto.Certificate)
                .WhereIf(queryDto.CertificateLevel > 0, e => e.CertificateLevel == queryDto.CertificateLevel)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Name), e => e.Name.Contains(queryDto.Name))
                .WhereIf(queryDto.Status.HasValue, e => e.Status == queryDto.Status.Value)
                .WhereIf(queryDto.IsCanTraining.HasValue, e => e.IsCanTraining == queryDto.IsCanTraining.Value)
                .Count(out totalCount)
                .OrderBy(m => m.Certificate)
                .OrderBy(m => m.CertificateLevel)
                .OrderBy(m => m.Sort)
                .Page(queryDto.PageIndex, queryDto.PageSize)
                .ToListAsync<ExamCaseListPageResponseDto>();

            var data = new PageResponse<ExamCaseListPageResponseDto>
            {
                Code = CommonConstants.SuccessCode,
                Data = selectData,
                PageIndex = queryDto.PageIndex,
                PageSize = queryDto.PageSize,
                RecordCount = totalCount,
            };

            return data;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Update(ExamCaseUpdateDto dto, long currentUserId)
        {
            var repo_ec = DbContext.FreeSql.GetRepository<ExamCase>(e => e.IsDelete == CommonConstants.IsNotDelete);

            var ecase = await repo_ec.Where(e => e.Id == dto.Id).FirstAsync();

            if (ecase == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "试卷信息不存在。" };
            }

            //赋值
            ecase.IsCanTraining = dto.IsCanTraining;
            ecase.Level = dto.Level;
            ecase.Status = dto.Status;
            ecase.Logo = dto.Logo;
            //ecase.CertificateLevel = dto.CertificateLevel;
            //ecase.Name = dto.Name;
            //ecase.Sort = dto.Sort;
            //ecase.QuestionTypeName = dto.QuestionTypeName;
            ecase.UpdateBy = currentUserId;
            ecase.UpdateTime = DateTime.Now;

            //await repo_ec.UpdateDiy.SetSource(ecase).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();//修改内容

            //await repo_ec.UpdateDiy.SetSource(ecase).UpdateColumns(a => new { a.CertificateLevel, a.IsCanTraining, a.Level }).ExecuteAffrowsAsync();//修改内容
            await repo_ec.UpdateDiy.SetSource(ecase).ExecuteAffrowsAsync();//修改内容

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = dto.Id.ToString() };
        }

        /// <summary>
        /// 查询案例的题目列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetCaseTopics(long caseId, CaseSourceSystem sourceSystem)
        {
            switch (sourceSystem)//案例所属源系统
            {
                case CaseSourceSystem.JS://竞赛
                    return await GetJSCaseTopics(caseId);
                case CaseSourceSystem.HYS://行业赛
                    return await GetHYSCaseTopics(caseId);
                case CaseSourceSystem.YSSX://云实训
                    return await GetYssxCaseTopics(caseId);
                default:
                    return null;
            }
        }
        /// <summary>
        /// 查询竞赛题目列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetJSCaseTopics(long caseId)
        {
            //按条件查询
            var selectData = await DbContext.JSFreeSql.GetRepository<JsYssxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.CaseId == caseId && e.ParentId == 0)
                .OrderBy(e => e.Sort)
                .ToListAsync<TopicBaseInfoResponseDto>();

            return new ResponseContext<List<TopicBaseInfoResponseDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }
        /// <summary>
        /// 查询行业赛题目列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetHYSCaseTopics(long caseId)
        {
            //按条件查询
            var selectData = await DbContext.HysFreeSql.GetRepository<HysYssxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.CaseId == caseId && e.ParentId == 0)
                .OrderBy(e => e.Sort)
                .ToListAsync<TopicBaseInfoResponseDto>();

            return new ResponseContext<List<TopicBaseInfoResponseDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }
        /// <summary>
        /// 查询云实训题目列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetYssxCaseTopics(long caseId)
        {
            //按条件查询
            var selectData = await DbContext.SxFreeSql.GetRepository<SxYssxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.CaseId == caseId && e.ParentId == 0)
                .OrderBy(e => e.Sort)
                .ToListAsync<TopicBaseInfoResponseDto>();

            return new ResponseContext<List<TopicBaseInfoResponseDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }
    }
}
