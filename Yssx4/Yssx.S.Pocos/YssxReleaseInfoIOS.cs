﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// IOS发布信息
    /// </summary>
    [Table(Name = "yssx_releaseinfo_ios")]
    public class YssxReleaseInfoIOS : BizBaseEntity<long>
    {
        /// <summary>
        /// 版本号
        /// </summary>
        public string VersionNumber { get; set; }

        /// <summary>
        /// 更新内容
        /// </summary>
        public string UpdateContent { get; set; }

        /// <summary>
        /// 是否强制更新 0:不是 1:是
        /// </summary>
        public int IsForceUpdate { get; set; }

    }
}
