﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 移除课程
    /// </summary>
    [Table(Name = "yssx_remove_course")]
    public class YssxRemoveCourse : TenantBaseEntity<long>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public long CourseId { get; set; }

        
    }

}
