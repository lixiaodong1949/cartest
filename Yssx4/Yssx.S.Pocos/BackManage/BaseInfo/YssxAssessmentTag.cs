﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 考核点标签
    /// </summary>
    [Table(Name = "yssx_assessment_tag")]
    public class YssxAssessmentTag : BizBaseEntity<int>
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }
        /// <summary>
        /// 分值说明
        /// </summary>
        public string ScoreRemark { get; set; }
        /// <summary>
        /// 备注（必考点/请任选A或B模块中一个子内容）
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 上级id
        /// </summary>
        public int ParentTagId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
