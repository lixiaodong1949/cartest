﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 班级
    /// </summary>
    [Table(Name = "yssx_class")]
    public class YssxClass : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 班级名称
        /// </summary> 
        public string Name { get; set; }
        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }
        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 入学年份
        /// </summary>
        public DateTime EntranceDateTime { get; set; }
    }
}
