﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 老师-班级中间表
    /// </summary>
    [Table(Name = "yssx_teacher_class")]
    public class YssxTeacherClass : TenantBizBaseEntity<long>
    {
        public YssxTeacherClass()
        {
        }

        public YssxTeacherClass(long id, long tenantId, long teacherId, long classId)
        {
            Id = id;
            TenantId = tenantId;
            TeacherId = teacherId;
            ClassId = classId;
        }

        /// <summary>
        /// 老师ID
        /// </summary>
        public long TeacherId { get; set; }
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }
    }
}
