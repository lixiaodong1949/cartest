using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 用户收货地址表
    /// </summary>
    [Table(Name = "yssx_user_address")]
    public class YssxUserAddress : BizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        
        /// <summary>
        /// 收货人姓名
        /// </summary>
        [Column(Name = "ShippingName", DbType = "varchar(50)")]
        public string ShippingName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(50)", IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 是否默认
        /// </summary>
        [Column(Name = "IsDefault", IsNullable = true)]
        public bool IsDefault { get; set; } = false;

        /// <summary>
        /// 省
        /// </summary>
        [Column(Name = "ProvinceName", DbType = "varchar(50)")]
        public string ProvinceName { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        [Column(Name = "CityName", DbType = "varchar(50)", IsNullable = true)]
        public string CityName { get; set; }
        /// <summary>
        /// 区
        /// </summary>
        [Column(Name = "AreaName", DbType = "varchar(50)", IsNullable = true)]
        public string AreaName { get; set; }
        /// <summary>
        /// 省唯一编码
        /// </summary>
        [Column(Name = "ProvinceCode", DbType = "varchar(50)", IsNullable = true)]
        public string ProvinceCode { get; set; }
        /// <summary>
        /// 区唯一编码
        /// </summary>
        [Column(Name = "CityCode", DbType = "varchar(50)", IsNullable = true)]
        public string CityCode { get; set; }
        /// <summary>
        /// 区唯一编码
        /// </summary>
        [Column(Name = "AreaCode", DbType = "varchar(50)", IsNullable = true)]
        public string AreaCode { get; set; }
        /// <summary>
        /// 街道
        /// </summary>
        [Column(Name = "StreetName", DbType = "varchar(50)", IsNullable = true)]
        public string StreetName { get; set; }
        /// <summary>
        /// 街道唯一编码
        /// </summary>
        [Column(Name = "StreetCode", DbType = "varchar(50)", IsNullable = true)]
        public string StreetCode { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        [Column(Name = "Details", DbType = "varchar(200)")]
        public string Details { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        [Column(Name = "Postcode", DbType = "varchar(50)", IsNullable = true)]
        public string Postcode { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "Mobile", DbType = "varchar(50)")]
        public string Mobile { get; set; }

    }
}