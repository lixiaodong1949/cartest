﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 摘要信息
    /// </summary>
    [Table(Name = "yssx_summary_info_new")]
    public class YssxSummaryNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 企业Id
        /// </summary>
        public long EnterpriseId { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        public string AssistCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        public int Type { get; set; }
    }
}
