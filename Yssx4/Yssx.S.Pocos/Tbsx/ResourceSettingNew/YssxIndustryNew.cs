﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 行业
    /// </summary>
	[Table(Name = "yssx_industry_new")]
    public class YssxIndustryNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 图片附件
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; } = Status.Enable;

    }
}
