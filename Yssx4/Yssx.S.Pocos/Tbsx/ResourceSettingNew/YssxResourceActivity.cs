﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 资源活跃度
    /// </summary>
    [Table(Name = "yssx_resource_activity")]
    public class YssxResourceActivity : BizBaseEntity<long>
    {
        /// <summary>
        /// 资源Id
        /// </summary>
        public long ResourceId { get; set; }

        /// <summary>
        /// 统计资源类型
        /// </summary>
        [Column(DbType = "int")]
        public StatisticsResourceType StatisticsResourceType { get; set; }

        /// <summary>
        /// 活跃度类型
        /// </summary>
        [Column(DbType = "int")]
        public ActivityType ActivityType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; }
    }
}
