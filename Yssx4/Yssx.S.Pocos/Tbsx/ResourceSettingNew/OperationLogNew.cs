﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 操作日志
    /// </summary>
    [Table(Name = "yssx_operationlog_new")]
    public class OperationLogNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 模块 Id
        /// </summary>
        public long? ModuleId { get; set; }

        /// <summary>
        /// 模块 1课程，2课件，3题目，4场景实训，5案例，6视频，7岗位实训
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 模块 内容
        /// </summary>
        public string Content { get; set; }
    }
}
