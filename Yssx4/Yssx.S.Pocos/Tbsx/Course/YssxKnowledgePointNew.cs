﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 知识点
    /// </summary>
    [Table(Name = "yssx_knowledge_point_new")]
    public class YssxKnowledgePointNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 父级知识点
        /// </summary>
        public long ParentId { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; } = Status.Enable;
    }
}
