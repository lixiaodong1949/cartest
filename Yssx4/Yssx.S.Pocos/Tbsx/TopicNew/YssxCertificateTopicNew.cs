﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 凭证题目
    /// </summary>
    [Table(Name = "yssx_certificate_topic_new")]
    public class YssxCertificateTopicNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 实训类型Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 流程Id
        /// </summary>
        public long FlowId { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 发生日期
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// 题干
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }

        /// <summary>
        /// 单据张数
        /// </summary>
        public int InvoicesNum { get; set; }

        /// <summary>
        /// 图片文件Id
        /// </summary>
        public string ImageIds { get; set; }

        ///// <summary>
        ///// 企业Id
        ///// </summary>
        //public long EnterpriseId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 0:核心，1:B模块
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryCalculationType CalculationScoreType { get; set; }

        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }
    }
    
}
