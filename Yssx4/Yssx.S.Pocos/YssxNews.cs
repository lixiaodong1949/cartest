﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 新闻
    /// </summary>
    [Table(Name = "yssx_news")]
    public class YssxNews
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string NewsUrl { get; set; }

        public string ImgUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ContentType { get; set; }

        /// <summary>
        /// 0：所有平台 1:技能抽查,2:技能竞赛,3:专一网
        /// </summary>
        public int Platform { get; set; }

        public int Sort { get; set; }

        public int Status { get; set; }

        public int IsDelete { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
