﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 证券-指数所有数据2019
    /// </summary>
    [Table(Name = "yssx_bigdata_ashareindex2019")]
    public class YssxBdAShareIndex2019
    {
        [Description("股票代码")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zsdm { get; set; }

        [Description("日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Date { get; set; }

        [Description("名称")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Name { get; set; }

        [Description("收盘价")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Spj { get; set; }

        [Description("最高价")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Zgj { get; set; }

        [Description("最低价")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Zdj { get; set; }

        [Description("开盘价")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Kpj { get; set; }

        [Description("前收盘")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Qsp { get; set; }

        [Description("涨跌额")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Zde { get; set; }

        [Description("涨跌幅")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zdf { get; set; }

        [Description("成交量")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Cjl { get; set; }

        [Description("成交金额")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Cjje { get; set; }

    }
}
