﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币参考汇率_2018
    /// </summary>
    [Table(Name = "yssx_bigdata_referencerateofrmb2018")]

    public class YssxBdReferenceRateOfRMB2018
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("币种")]
        public string Bz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-12")]
        public string Years2018_12 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-11")]
        public string Years2018_11 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-10")]
        public string Years2018_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-09")]
        public string Years2018_09 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-08")]
        public string Years2018_08 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-07")]
        public string Years2018_07 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-06")]
        public string Years2018_06 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-05")]
        public string Years2018_05 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-04")]
        public string Years2018_04 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-03")]
        public string Years2018_03 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-03")]
        public string Years2018_02 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2018-03")]
        public string Years2018_01 { get; set; }

    }
}
