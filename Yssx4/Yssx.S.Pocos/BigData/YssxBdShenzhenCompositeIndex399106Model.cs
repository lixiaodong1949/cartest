﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 深证综指399106
    /// </summary>
    [Table(Name = "yssx_bigdata_composite399106")]

    public class YssxBdShenzhenCompositeIndex399106Model
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("指数代码")]
        public string Zsdm { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("月度标识")]
        public string Ydbs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("月开盘日期")]
        public string Ykprq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("月收盘日期")]
        public string Ysprq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("开盘指数")]
        public string Kpzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("最高指数")]
        public string Zgzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("最低指数")]
        public string Zdzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("收盘指数")]
        public string Spzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("成份证券成交量（万股）")]
        public string Cfzjvjl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("成份证券成交金额（万元）")]
        public string Cfzjcjje { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("指数回报率")]
        public string Zshbl { get; set; }
    }
}
