﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 金融机构人民币贷款基准利率
    /// </summary>
    [Table(Name = "yssx_bigdata_theloaninterestrate")]
    public class YssxBdBenchmarkRMBLendingRateForFinancialInstitutions
    {
        [Description("调整时间")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Tzsj { get; set; }

        [Description("六个月以内（含六个月）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Lgyyn { get; set; }

        [Description("六个月至一年（含一年）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Lgyzyn { get; set; }

        [Description("一至三年（含三年）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Yzsn { get; set; }

        [Description("三至五年（含五年）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Szwn { get; set; }

        [Description("五年以上")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Wnys { get; set; }
    }
}
