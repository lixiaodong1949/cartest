﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 政策性金融债（国开行）2017
    /// </summary>
    [Table(Name = "yssx_bigdata_policybasedfinancialbonds2017")]

    public class YssxBdPolicyBasedFinancialBonds2017
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("标准期限(年)")]
        public string Bzqx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("基准债券")]
        public string Jzzj { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("剩余期限(年)")]
        public string Syqx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("报买入收益率(%)")]
        public string Bmrsyl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("报卖出收益率(%)")]
        public string Bmcsyl { get; set; }
    }
}
