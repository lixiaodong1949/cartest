﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币汇率中间价
    /// </summary>
    [Table(Name = "yssx_bigdata_rmbmidpoint")]

    public class YssxBdTheCentralParityOfRMBExchangeRate
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("USD/CNY")]
        public string USD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("EUR/CNY")]
        public string EUR_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("100JPY/CNY")]
        public string JPY_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("HKD/CNY")]
        public string HKD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("GBP/CNY")]
        public string GBP_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("AUD/CNY")]
        public string AUD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("NZD/CNY")]
        public string NZD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("SGD/CNY")]
        public string SGD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CHF/CNY")]
        public string CHF_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CAD/CNY")]
        public string CAD_CNY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/MYR")]
        public string CAD_MYR { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/RUB")]
        public string CNY_RUB { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/ZAR")]
        public string CNY_ZAR { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/KRW")]
        public string CNY_KRW { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/AED")]
        public string CNY_AED { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/SAR")]
        public string CNY_SAR { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/HUF")]
        public string CNY_HUF { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/PLN")]
        public string CNY_PLN { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/DKK")]
        public string CNY_DKK { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/SEK")]
        public string CNY_SEK { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/NOK")]
        public string CNY_NOK { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/TRY")]
        public string CNY_TRY { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/MXN")]
        public string CNY_MXN { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("CNY/THB")]
        public string CNY_THB { get; set; }
    }
}
