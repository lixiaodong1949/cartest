﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// B股指数000003
    /// </summary>
    [Table(Name = "yssx_bigdata_bshareindex000003")]
    public class YssxBdBShareIndex000003
    {
        [Description("指数代码")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zsdm { get; set; }

        [Description("月度标识")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Ydbs { get; set; }

        [Description("月开盘日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Ykprq { get; set; }

        [Description("月收盘日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Ysprq { get; set; }

        [Description("开盘指数")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Kpzs { get; set; }

        [Description("最高指数")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zgzs { get; set; }

        [Description("最低指数")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zdzs { get; set; }

        [Description("收盘指数")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Spzs { get; set; }

        [Description("成份证券成交量")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Cfzjcjl { get; set; }

        [Description("成份证券成交金额")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Cdzjcjje { get; set; }

        [Description("指数回报率")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zshbl { get; set; }
    }
}
