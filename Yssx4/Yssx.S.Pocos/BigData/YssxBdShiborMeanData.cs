﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// Shibor均值数据
    /// </summary>
    [Table(Name = "yssx_bigdata_shibormeandata")]

    public class YssxBdShiborMeanData
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("均值类型")]
        public string Jzlx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("O/N")]
        public string ONW { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("1W")]
        public string OneW { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("2W")]
        public string TwoW { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("1M")]
        public string OneM { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("3M")]
        public string ThreeM { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("6M")]
        public string SixM { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("9M")]
        public string NineM { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("1Y")]
        public string OneY { get; set; }
    }
}
