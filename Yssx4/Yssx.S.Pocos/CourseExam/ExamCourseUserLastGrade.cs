﻿using System;
using System.Collections.Generic;
using System.Text;
using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    /// <summary>
    /// 学生练习最近作答记录 
    /// </summary>
    [Table(Name = "exam_course_user_last_grade")]
    public class ExamCourseUserLastGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主表ID
        /// </summary>
        public long GradeId { get; set; }

        #region paper中的值
        /// <summary>
        /// 课程id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 章节id
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
        #endregion
        ///// <summary>
        ///// 分组Id
        ///// </summary>
        //public long GroupId { get; set; }

    }
}
