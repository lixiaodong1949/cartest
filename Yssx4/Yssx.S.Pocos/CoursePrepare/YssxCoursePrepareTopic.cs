﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程备课资料库题目库
    /// </summary>
    [Table(Name = "yssx_course_prepare_topic")]
    public class YssxCoursePrepareTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 题目主键
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }
        
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        
        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 知识点Ids
        /// </summary>
        public string KnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CoursePrepareTopicSource TopicSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 课程或案例名称
        /// </summary>
        public string CourseOrCaseName { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
