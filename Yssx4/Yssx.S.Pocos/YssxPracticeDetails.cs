using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 自主实训 详情
    /// </summary>
    [Table(Name = "yssx_practice_details")]
    public class YssxPracticeDetails : BizBaseEntity<long>
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 岗位Id/课程章Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 题目 岗位/课程 true选中 false未选中
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 答案 岗位/课程 true选中 false未选中
        /// </summary>
        public bool AnswerState { get; set; }
    }

    /// <summary>
    /// 自主实训 操作记录
    /// </summary>
    [Table(Name = "yssx_practice_record")]
    public class YssxPracticeRecord : BizBaseEntity<long>
    {
        /// <summary>
        /// 自主实训 详情Id
        /// </summary>
        public long PracticeDetailsId { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// 新实训实习名称
        /// </summary>
        public string PracticeName { get; set; }

        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 岗位Id/课程章Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }
    }
}