﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    #region 备课课程章节
    /// <summary>
    /// 备课课程章节表
    /// </summary>
    [Table(Name = "yssx_prepare_section")]
    public class YssxPrepareSection : BizBaseEntity<long>
    {
        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 章节标题
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 父节点Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 0:章 1：节
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

    }
    #endregion

    #region 备课课程章节习题
    /// <summary>
    /// 备课课程章节习题表
    /// </summary>
    [Table(Name = "yssx_prepare_section_topic")]
    public class YssxPrepareSectionTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }
        
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
    #endregion

    #region 备课课程章节场景实训
    /// <summary>
    /// 备课课程场景实训表
    /// </summary>
    [Table(Name = "yssx_prepare_section_scenetraining")]
    public class YssxPrepareSectionSceneTraining : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程资源来源 0:购买课程下的资源 1:客户单独购买 2:上传私有
        /// </summary>
        [Column(Name = "DataSourceType", IsNullable = true)]
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 订单主键Id(课程、资源订单)
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// 资源有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }
    }
    #endregion

    #region 备课课程章节教材
    /// <summary>
    /// 备课课程章节教材表
    /// </summary>
    [Table(Name = "yssx_prepare_section_textbook")]
    public class YssxPrepareSectionTextBook : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 教材描述
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public string KnowledgePointId { get; set; }
    }
    #endregion

    #region 备课课程课件、视频、教案、授课计划
    /// <summary>
    /// 备课课程课件、视频、教案、授课计划表
    /// </summary>
    [Table(Name = "yssx_prepare_section_files")]
    public class YssxPrepareSectionFiles : BizBaseEntity<long>
    {       
        //1、教案依附于课程,章节Id为冗余字段
        //2、课件、视频可能依附于课程章节
        
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 文件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 1:课件 2:教案 3:视频 4:授课计划
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程资源来源 0:购买课程下的资源 1:客户单独购买 2:上传私有
        /// </summary>
        [Column(Name = "DataSourceType", IsNullable = true)]
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 订单主键Id(课程、资源订单)
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// 资源有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }
    }
    #endregion

    #region 备课课程章节统计表
    /// <summary>
    /// 课程章节汇总概要表
    /// </summary>
    [Table(Name = "yssx_prepare_section_summary")]
    public class YssxPrepareSectionSummary
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }
        
        /// <summary>
        /// 统计分类 0:教材 1:习题 2:案例 3:视频 4:章节 5:课件 6:场景实训 7:教案 8:授课计划
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 个数
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 操作更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
    #endregion

}
