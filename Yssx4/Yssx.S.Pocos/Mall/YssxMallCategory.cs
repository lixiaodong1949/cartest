﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 商城分类
    /// </summary>
    [Table(Name = "yssx_mall_category")]
    public class YssxMallCategory : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary> 
        public int Sort { get; set; }

        /// <summary>
        /// 父级分类ID
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 布局类型
        /// * 0 九宫格带图（通用）
        /// * 1 横版带图
        /// * 2 横版不带图
        /// </summary>
        public int LayoutType { get; set; }

    }
}
