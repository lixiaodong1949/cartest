﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 部门人员管理
    /// </summary>
    [Table(Name = "yssx_post_personnel")]
    public class YssxPostPersonnel : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 岗位人员
        /// </summary>
        public string Persons { get; set; }
        /// <summary>
        /// 所属部门
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
