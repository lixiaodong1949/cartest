﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例表
    /// </summary>
    [Table(Name = "yssx_case")]
    public class YssxCase : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 案例编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }
        /// <summary>
        /// 是否启用大数据中心（0不启用 1启用）
        /// </summary>
        public int IsBigData { get; set; }
        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）
        /// </summary>
        [Column(DbType = "int")]
        public CompetitionType CompetitionType { get; set; }
        /// <summary>
        /// 总时长
        /// </summary>
        public int TotalTime { get; set; }
        /// <summary>
        /// 设置类型（0统一设置  1单独设置 2 单元格计分） 统一设置时以当前案例主表外围里围为准
        /// 否则以各个分录提的设置为准
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryCalculationType SetType { get; set; }
        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }
        /// <summary>
        /// 企业相关信息
        /// </summary>
        [Column(Name = "EnterpriseInfo", DbType = "LongText", IsNullable = true)]
        public string EnterpriseInfo { get; set; }
        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）
        /// </summary>
        public int TestStatus { get; set; }
        /// <summary>
        /// 正式赛启用状态（0 不启用 1启用）
        /// </summary>
        public int OfficialStatus { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 成本会计
        /// </summary>
        public string CostAccountant { get; set; }
        /// <summary>
        /// 税务会计
        /// </summary>
        public string TaxAccountant { get; set; }
        /// <summary>
        /// 业务会计
        /// </summary>
        public string BusinessAccountant { get; set; }
        /// <summary>
        /// 分录题分数占比
        /// </summary>
        [Column(Name = "AccountEntryScale", DbType = "decimal(6,3)", IsNullable = true)]
        public decimal AccountEntryScale { get; set; }
        /// <summary>
        /// 表格题分数占比
        /// </summary>
        [Column(Name = "FillGridScale", DbType = "decimal(6,3)", IsNullable = true)]
        public decimal FillGridScale { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        [Column(Name = "AccountingPeriodDate", DbType = "varchar(50)", IsNullable = true)]
        public string AccountingPeriodDate { get; set; }
        /// <summary>
        /// 区域
        /// </summary>
        [Column(Name = "Region", DbType = "varchar(50)")]
        public string Region { get; set; }

        #region 新增属性
        /// <summary>
        /// 所属行业
        /// </summary>
        public long Industry { get; set; }
        /// <summary>
        /// 公司规模
        /// </summary>
        public string CompanyScale { get; set; }
        /// <summary>
        /// 纳税人类型
        /// </summary>
        [Column(DbType = "int")]
        public TaxPayerType TaxPayerType { get; set; }
        /// <summary>
        /// 实训描述
        /// </summary>
        [Column(Name = "SxInfo", DbType = "LongText", IsNullable = true)]
        public string SxInfo { get; set; }
        /// <summary>
        /// 公司背景图
        /// </summary>
        public string BGFileUrl { get; set; }
        /// <summary>
        /// 实训分类  0：会计实训,1:出纳实训,2：大数据,3:业财税 4:中职高考冲刺模拟题
        /// </summary>
        public int SxType { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Img { get; set; }
        #endregion

    }
}
