﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 短信验证码
    /// </summary>
    [Table(Name = "yssx_verifycode")]
    public class YssxVerifyCode : BaseEntity<long>
    {
        /// <summary>
        /// 手机
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string Code { get; set; }


        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? ExpirTime { get; set; }
        /// <summary>
        /// 0,未验证使用,1,已验证使用
        /// </summary>
        public int Status { get; set; } = 0;
    }
}
