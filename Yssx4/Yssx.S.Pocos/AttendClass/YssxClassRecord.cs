﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 上课记录表
    /// </summary>
    [Table(Name = "yssx_class_record")]
    public class YssxClassRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 上课标识码(用于标识和哪些班级一起上课)
        /// </summary>
        public long IdentifyCode { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 教师Id
        /// </summary> 
        public long TeacherUserId { get; set; }

        /// <summary>
        /// 上课课程Id
        /// </summary> 
        public long CourseId { get; set; }

        /// <summary>
        /// 上课课程名称
        /// </summary> 
        public string CourseName { get; set; }

        /// <summary>
        /// 学期主键
        /// </summary>
        [Column(Name = "SemesterId", IsNullable = true)]
        public Nullable<long> SemesterId { get; set; }
        
        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }
    }
}
