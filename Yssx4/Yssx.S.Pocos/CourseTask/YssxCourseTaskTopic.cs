﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课业任务 - 题目
    /// </summary>
    [Table(Name = "yssx_course_task_topic")]
    public class YssxCourseTaskTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 题目ID
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

    }
}
