﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Statistics
{
    /// <summary>
    /// 技能竞赛 - 用于统计
    /// </summary>
    [Table(Name = "skill_user")]
    public class SkillUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }
    }
    /// <summary>
    /// 技能抽查 - 用于统计
    /// </summary>
    [Table(Name = "yssx_user")]
    public class YssxJNUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }
    }
}
