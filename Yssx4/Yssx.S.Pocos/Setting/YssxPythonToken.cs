﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// Python大数据Token（tushare.pro平台公共账户:yunssx@163.com）
    /// </summary>
	[Table(Name = "yssx_python_token")]
    public class YssxPythonToken : BizBaseEntity<long>
    {
        /// <summary>
        /// 接口token
        /// </summary>
        public string Token { get; set; }

    }
}
