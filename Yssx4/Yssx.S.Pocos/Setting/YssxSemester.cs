﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学期
    /// </summary>
    [Table(Name = "yssx_semester")]
    public class YssxSemester : BizBaseEntity<long>
    {
        /// <summary>
        /// 学期名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否禁用 0:启用 1:禁用
        /// </summary>
        public int IsDisabled { get; set; }

    }
}
