﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 岗位实训任务套题关系表
    /// </summary>
    [Table(Name = "yssx_jobtraining_task_topic")]
    public class YssxJobTrainingTaskTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位实训任务Id
        /// </summary>
        public long JobTrainingTaskId { get; set; }

        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }
        
        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }
    }
}
