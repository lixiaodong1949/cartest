﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 岗位实训任务表
    /// </summary>
    [Table(Name = "yssx_jobtraining_task")]
    public class YssxJobTrainingTask : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型 0:作业 1:考试
        /// </summary>
        public Nullable<int> ExamType { get; set; }

        /// <summary>
        /// 训练开始时间
        /// </summary>
        public Nullable<DateTime> TrainStartTime { get; set; }

        /// <summary>
        /// 训练结束时间
        /// </summary>
        public Nullable<DateTime> TrainEndTime { get; set; }

        /// <summary>
        /// 任务密码
        /// </summary>
        public string TaskPassword { get; set; }

        /// <summary>
        /// 实训类型 0:综合岗位实训 1:分岗位实训
        /// </summary>
        public Nullable<int> TrainType { get; set; }

        /// <summary>
        /// 分岗位实训组数
        /// </summary>
        public Nullable<int> GroupCount { get; set; }

        /// <summary>
        /// 答案与解析显示类型 0:完成后显示 1:答题中显示
        /// </summary>
        public Nullable<int> AnswerShowType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string  Remark { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 是否发布 false:未发布 true:已发布
        /// </summary>
        public Nullable<bool> IsPublish { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; } = true;

    }
}
