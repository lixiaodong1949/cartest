﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 公共题目表
    /// </summary>
    [Table(Name = "yssx_topic_publictest")]
    public class YssxTopicPublictest : BizBaseEntity<long>
    {
        /// <summary>
        /// 原始数据Id
        /// </summary>
        public long OriginalDataId { get; set; }


    }
}
