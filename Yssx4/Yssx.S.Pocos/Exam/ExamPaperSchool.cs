﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    /// <summary>
    /// 试卷-考试学校表---省内赛专用
    /// </summary>
    [Table(Name = "exam_paper_school")]
    public class ExamPaperSchool : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 考试ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 参考学生数
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 实际参考学生数 
        /// </summary>
        public int JoinStudentCount { get; set; }

    }
}
