﻿using System;
using System.Collections.Generic;
using System.Text;
using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    /// <summary>
    /// 学生练习最近作答记录---自主练习和真题模拟专用 
    /// </summary>
    [Table(Name = "exam_student_last_grade")]
    public class ExamStudentLastGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主表ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

    }
}
