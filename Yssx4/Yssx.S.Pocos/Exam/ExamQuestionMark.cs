﻿using System;
using System.Collections.Generic;
using System.Text;
using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    [Table(Name = "exam_question_mark")]
    public class ExamQuestionMark : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 成绩主表id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

    }
}
