﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学生作答明细表
    /// </summary>
    [Table(Name = "exam_student_grade_detail")]
    public class ExamStudentGradeDetail : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 学生ID
        /// </summary>
        public long StudentId { get; set; }
        /// <summary>
        /// 作答信息主表ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 题目所属岗位id
        /// </summary>
        public long PostionId { get; set; }
        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 父级题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        [Column(DbType = "longtext")]
        public string Answer { get; set; }
        /// <summary>
        /// 作答与正确答案的对比信息
        /// </summary>
        [Column(DbType = "longtext")]
        public string AnswerCompareInfo { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public AnswerResultStatus Status { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 是否已交卷
        /// </summary>
        public bool IsSubmit { get; set; } = false;
        /// <summary>
        /// 入参
        /// </summary>
        [Column(DbType ="longtext")]
        public string SourceDataInfo { get; set; }

    }
}
