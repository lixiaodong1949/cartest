﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.TopicCenter
{
    /// <summary>
    /// 题目主表
    /// </summary>
    [Table(Name = "zdap_topic")]
    public class ZdapTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        [Column(Name = "BusinessDate", IsNullable = true)]
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 题目分组Id
        /// </summary>
        public long GroupId { get; set; }
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 结账类型（0 转账 1现金）(默认：0)
        /// </summary>
        public int SettlementType { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public Status Status { get; set; }

        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        [Column(Name = "DifficultyLevel", DbType = "int(255)", IsNullable = true)]
        public int? DifficultyLevel { get; set; }

        /// <summary>
        /// 模板数据源内容(不含答案)
        /// </summary>
        [Column(Name = "TopicContent", DbType = "LongText", IsNullable = true)]
        public string TopicContent { get; set; }

        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }
        /// <summary>
        /// 完整答案（包含不计分内容）
        /// </summary>
        [Column(Name = "AllAnswerValue", DbType = "LongText", IsNullable = true)]
        public string AllAnswerValue { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        [Column(Name = "Hint", DbType = "LongText", IsNullable = true)]
        public string Hint { get; set; }
        /// <summary>
        /// 题目提示（做题时的提示，分开结转提示）
        /// </summary>
        [Column(Name = "TopicHint", DbType = "LongText", IsNullable = true)]
        public string TopicHint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        [Column(Name = "TeacherHint", DbType = "LongText", IsNullable = true)]
        public string TeacherHint { get; set; }

        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        [Column(Name = "ParentId", DbType = "bigint(20)", IsNullable = true)]
        public long ParentId { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        [Column(Name = "AccountingPeriodDate", DbType = "varchar(50)", IsNullable = true)]
        public string AccountingPeriodDate { get; set; }

        /// <summary>
        /// 题目附件文件id 0:没有,1:有
        /// </summary>

        public int FileIds { get; set; }

        /// <summary>
        /// 是否是干扰题
        /// </summary>
        public bool IsTrap { get; set; } = false;
        /// <summary>
        /// 题目关联是否开启
        /// </summary>
        public bool TopicRelationStatus { get; set; }

        #region 综合分录题

        /// <summary>
        /// 是否需要收付款
        /// </summary>
        public bool IsReceipt { get; set; } = false;
        /// <summary>
        /// 收付款题类型
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public ReceivePaymentType ReceivePaymentType { get; set; } = ReceivePaymentType.None;

        /// <summary>
        /// 推送时间
        /// </summary>
        public DateTime? PushDate { get; set; }

        /// <summary>
        /// 会计主管分数 (保存在综合分录题中的子分录题中)
        /// </summary>
        public decimal DisableAMScore { get; set; }
        /// <summary>
        /// 出纳分数 (保存在综合分录题中的子分录题中)
        /// </summary>
        public decimal DisableCashierScore { get; set; }

        #endregion

        #region 财务表报题专业

        ///// <summary>
        ///// 纳税申报报表分数
        ///// </summary>
        //public decimal DeclarationScore { get; set; }
        /// <summary>
        /// 纳税申报所属期起
        /// </summary>
        public DateTime? DeclarationDateStart { get; set; }
        /// <summary>
        /// 纳税申报所属期止
        /// </summary>
        public DateTime? DeclarationDateEnd { get; set; }
        /// <summary>
        /// 纳税申报期限
        /// </summary>
        public DateTime? DeclarationLimitDate { get; set; }

        #endregion

        #region 表格题专用
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CalculationType CalculationType { get; set; } = CalculationType.None;
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = 0;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = 0;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = 0;

        #endregion

        #region 申报表题 专用

        /// <summary>
        /// 申报表Id 申报表题
        /// </summary>
        public long DeclarationId { get; set; }
        /// <summary>
        /// 申报表Id 申报表题
        /// </summary>
        public long ParentDeclarationId { get; set; }
        /// <summary>
        /// 申报表分数，仅用作后台题目列表分数统计使用
        /// </summary>
        [Column(Name = "DeclarationScore", DbType = "decimal(10,4)")]
        public decimal DeclarationScore { get; set; }

        #endregion

        /// <summary>
        /// 技能Id
        /// </summary>
        [Column(Name = "SkillId", IsNullable = true)]
        public long SkillId { get; set; } = 0;
        [Column(Name = "TenantId", IsNullable = true)]
        public long TenantId { get; set; } = 0;

        /// <summary>
        /// 题目附件ids
        /// </summary>
        [Column(Name = "TopicFileIds", DbType = "varchar(1000)", IsNullable = true)]
        public string TopicFileIds { get; set; }

        /// <summary>
        /// 是否有知识点
        /// </summary>
        public bool IsKnowledgePoint { get; set; }
        /// <summary>
        /// 是否有政策点
        /// </summary>
        public bool IsPolicyPoint { get; set; }
        /// <summary>
        /// 是否有施政点
        /// </summary>
        public bool IsGovernmentPoint { get; set; }
    }
}
