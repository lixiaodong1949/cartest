﻿using System.Configuration;
using System.Net;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Socketing;
using EQueue.Configurations;
using EQueue.NameServer;
using ECommonConfiguration = ECommon.Configurations.Configuration;
namespace Yssx.NameServerService
{
    public class Bootstrap
    {
        private static ECommonConfiguration _ecommonConfiguration;
        private static NameServerController _nameServer;
        private static ILogger iLogger;
        public static void Initialize()
        {
            InitializeEQueue();
        }
        public static void Start()
        {
            _nameServer.Start();
        }
        public static void Stop()
        {
            if (_nameServer != null)
            {
                _nameServer.Shutdown();
            }
        }

        private static void InitializeEQueue()
        {
            _ecommonConfiguration = ECommonConfiguration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog()
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .RegisterEQueueComponents()
                .BuildContainer();

            iLogger = ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Bootstrap).FullName);

            var bindingAddress = ConfigurationManager.AppSettings["bindingAddress"];
            var nameServerPort = ConfigurationManager.AppSettings["nameServerPort"];

            iLogger.Info($"bindingAddress:{bindingAddress}");
            iLogger.Info($"nameServerPort:{nameServerPort}");

            var bindingIpAddress = string.IsNullOrEmpty(bindingAddress) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(bindingAddress);
            int port = string.IsNullOrEmpty(nameServerPort) ? 9493 : int.Parse(nameServerPort);
            var setting = new NameServerSetting()
            {
                BindingAddress = new IPEndPoint(bindingIpAddress, port)
            };
            _nameServer = new NameServerController(setting);

            iLogger.Info("NameServer initialized.");
        }
    }
}
