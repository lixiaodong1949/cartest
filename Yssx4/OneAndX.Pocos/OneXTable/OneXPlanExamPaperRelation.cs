﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考试计划试卷关系表
    /// </summary>
    [Table(Name = "onex_plan_exam_paper_relation")]
    public class OneXPlanExamPaperRelation : BizBaseEntity<long>
    {
        /// <summary>
        /// 计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 针对座位号 1:奇数 2:偶数
        /// </summary>
        public int ServiceSeatType { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public Nullable<DateTime> BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 案例所属系统
        /// </summary>
        [Column(Name = "SourceSystem", DbType = "int", IsNullable = true)]
        public CaseSourceSystem SourceSystem { get; set; }
    }
}
