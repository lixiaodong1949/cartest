﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考试成绩
    /// </summary>
    [Table(Name = "exam_user_grade")]
    public class ExamUserGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id SignUp中的Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 用户身份证号码
        /// </summary>
        public string UserIDNumber { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        ///// <summary>
        ///// 准考证Id
        ///// </summary>
        //public long ExamTicketId { get; set; }
        /// <summary>
        /// 考试名称
        /// </summary>
        [Column(Name = "ExamName", DbType = "varchar(255)", IsNullable = true)]
        public long ExamName { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        [Column(Name = "ExamCardNumber", DbType = "varchar(255)", IsNullable = true)]
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 原始得分
        /// </summary>
        [Column(Name = "InitialScore", DbType = "decimal(10,4)")]
        public decimal InitialScore { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        [Column(Name = "ExamPaperScore", DbType = "decimal(10,4)")]
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 考试通过分数
        /// </summary>
        [Column(Name = "PassScore", DbType = "decimal(10,4)")]
        public decimal PassScore { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 案例原系统
        /// </summary>
        [Column(Name = "SourceSystem", DbType = "int")]
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 原案例Id
        /// </summary>
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 考试案例Id
        /// </summary>
        public long ExamCaseId { get; set; }

        /// <summary>
        /// 是否考试通过
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RollUpType { get; set; }

        public string CaseYear { get; set; }

    }
}
