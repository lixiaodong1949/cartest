﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 新闻管理
    /// </summary>
    [Table(Name = "1x_News")]
    public class News : BizBaseEntity<long>
    {

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 新闻内容
        /// </summary>
        [Column(Name = "NewsContent", DbType = "longtext", IsNullable = true)]
        public string NewsContent { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态 1开启 2关闭
        /// </summary>
        public int State { get; set; }

    }
}
