﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考点_表
    /// </summary>
    [Table(Name = "1x_ExaminationSite")]
    public class ExaminationSite : BizBaseEntity<long>
    {
        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExaminationSiteName { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; } 

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactUs { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExaminationSiteCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 考场_详情表
    /// </summary>
    [Table(Name = "1x_ExaminationSite_Details")]
    public class ExaminationSiteDetails : BaseEntity<long>
    {

        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExaminationSiteId { get; set; }

        /// <summary>
        /// 考场名
        /// </summary>
        public string ExaminationRoomName { get; set; }

        /// <summary>
        /// 座位数
        /// </summary>
        public int SeatingCapacity { get; set; }

        /// <summary>
        /// 可考模块
        /// (多选逗号隔开如2,3)
        /// 1【业财税融合成本管控职业技能等级证书】
        /// 2【业财税融合大数据投融资分析职业技能等级证书
        /// 3【业财税融合项目财务管理职业技能等级证书】
        /// </summary>
        public string Examinablemodules { get; set; }

        /// <summary>
        /// 可考等级
        /// (多选逗号隔开如2,3)
        /// 1初级2中级3高级
        /// </summary>
        public string Examinationlevel { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 证书_详情表
    /// </summary>
    [Table(Name = "1x_Certificate_Details")]
    public class CertificateDetails : BaseEntity<long>
    {
        /// <summary>
        /// 可考模块
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 可考等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考场Id
        /// </summary>
        public long ExaminationRoomId { get; set; }

        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExaminationSiteId { get; set; }
    }

    #region 
    ///// <summary>
    ///// 成本管控 true选中false不选
    ///// </summary>
    //public bool CostControl { get; set; }

    ///// <summary>
    ///// 业税财 true选中false不选
    ///// </summary>
    //public bool BusinessTax { get; set; }

    ///// <summary>
    ///// 财务管理 true选中false不选
    ///// </summary>
    //public bool FinancialManag { get; set; }

    ///// <summary>
    ///// 初级  true选中false不选
    ///// </summary>
    //public bool Primary { get; set; }

    ///// <summary>
    ///// 中级 true选中false不选
    ///// </summary>
    //public bool Intermediate { get; set; }

    ///// <summary>
    ///// 高级 true选中false不选
    ///// </summary>
    //public bool Senior { get; set; }
    #endregion
}
