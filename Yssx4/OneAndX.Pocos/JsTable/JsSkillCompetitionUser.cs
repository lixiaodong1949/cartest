﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 省内赛专用用户表
    /// </summary>
    [Table(Name = "skill_competition_user")]
    public class JsSkillCompetitionUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Column(Name = "NikeName", DbType = "varchar(255)", IsNullable = true)]
        public string NikeName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Column(Name = "Photo", DbType = "varchar(255)", IsNullable = true)]
        public string Photo { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        [Column(Name = "QQ", DbType = "varchar(255)", IsNullable = true)]
        public string QQ { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }

        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public Status Status { get; set; } = Status.Disable;

        /// <summary>
        /// 用户名称
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary> 
        public UserTypeEnums UserType { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        [Column(Name = "WeChat", DbType = "varchar(255)", IsNullable = true)]
        public string WeChat { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }
        /// <summary>
        /// 其他联系方式
        /// </summary>
        [Column(Name = "OtherContacts", DbType = "varchar(255)", IsNullable = true)]
        public string OtherContacts { get; set; }
        /// <summary>
        /// 账号开通时间
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 账号过期时间
        /// </summary>
        public DateTime ExpireDate { get; set; }
        /// <summary>
        /// 省内赛任务id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 所属组号
        /// </summary>
        public string GroupNo { get; set; }
        /// <summary>
        /// 大数据岗位ID
        /// </summary>
        public long BigDataPositionId { get; set; }
        /// <summary>
        /// 大数据岗位名称
        /// </summary>
        public string BigPositionName { get; set; }
        /// <summary>
        /// 业财税岗位ID
        /// </summary>
        public long YcsPositionId { get; set; }
        /// <summary>
        /// 业财税岗位名称
        /// </summary>
        public string YcsPositionName { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 指导老师
        /// </summary>
        public string Adviser { get; set; }
    }
}
