﻿using ECommon.Components;
using Sx.Course.Entitys;
using Sx.Course.QueryServices.FreeSqldb.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace Sx.Course.QueryServices.FreeSqldb
{
    [Component]
    public class CourseQueryService
    {
        public async Task<PageResponse<YssxCourseDto>> GetCourseListByPage(CourseSearchDto dto, UserTicket user)
        {
            var result = new PageResponse<YssxCourseDto>();
            FreeSql.ISelect<YssxCourse> select = null;

            if (dto.UseType == 0 && user.Type != (int)UserTypeEnums.Professional)
            {
                select = DbContext.FreeSql.GetRepository<YssxCourse>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.CreateBy == user.Id);
            }
            else if (user.Type == (int)UserTypeEnums.Professional && dto.UseType == 0)
            {
                select = DbContext.FreeSql.GetRepository<YssxCourse>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.TenantId == user.TenantId);
            }
            else if (user.Type == (int)UserTypeEnums.Professional && dto.UseType == 1)
            {
                select = DbContext.FreeSql.GetRepository<YssxCourse>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete);
            }

            if (!string.IsNullOrWhiteSpace(dto.Keyword))
            {
                select.Where(x => x.CourseName.Contains(dto.Keyword) || x.CourseTitle.Contains(dto.Keyword) || x.Author.Contains(dto.Keyword));
            }

            if (dto.Education >= 0)
            {
                select.Where(x => x.Education == dto.Education);
            }

            switch (dto.Status)
            {
                case 0: select.Where(x => x.Uppershelf == dto.Status); break;
                case 1: select.Where(x => x.Uppershelf == 1 && x.AuditStatus == 0); break;
                case 2: select.Where(x => x.AuditStatus == 1); break;
                case 3: select.Where(x => x.AuditStatus == 2); break;
                default:
                    break;
            }
            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(s => s.CreateTime)
              .Page(dto.PageIndex, dto.PageSize).ToSql("Id,CourseName,CourseType,CourseTitle,CourseTypeId,Author,Education,Images,PublishingHouse,PublishingDate,Intro,Description,CreateByName,AuditStatus,Uppershelf,ReadCount,AuditStatusExplain,UppershelfExplain,CreateTime,UpdateTime");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseDto>(sql);


            List<long> ids = items.Select(x => x.Id).ToList();
            List<YssxSectionSummary> yssxes = await DbContext.FreeSql.GetRepository<YssxSectionSummary>().Select.Where(x => ids.Contains(x.CourseId)).ToListAsync();

            items.ForEach(x =>
            {
                if (x.AuditStatus == 0 && x.Uppershelf == 1)
                {
                    x.Status = 1;
                }
                else if (x.AuditStatus == 2)
                {
                    x.Status = 3;
                }
                else if (x.AuditStatus == 1)
                {
                    x.Status = 2;
                }
                x.SummaryListDto = yssxes.Where(o => o.CourseId == x.Id).Select(i => new YssxSectionSummaryDto
                {
                    Id = i.Id,
                    Count = i.Count,
                    CourseId = i.CourseId,
                    SectionId = i.SectionId,
                    SummaryType = i.SummaryType
                }).ToList();
            });
            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = dto.PageSize;
            result.PageIndex = dto.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
    }
}
