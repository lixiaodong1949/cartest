﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.QueryServices.FreeSqldb.Dto
{
    /// <summary>
    /// 章节汇总信息
    /// </summary>
    public class YssxSectionSummaryDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 章节Id
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 统计分类 0:教材 1:习题 2:案例 3:视频 4:章节 5:课件 6:场景实训 7:教案
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 统计分类名称
        /// </summary>
        public string SummaryName { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }
    }
}
