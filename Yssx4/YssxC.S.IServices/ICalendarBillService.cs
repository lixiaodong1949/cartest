﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 日历票据服务接口
    /// </summary>
    public interface ICalendarBillService
    {
        /// <summary>
        /// 生成用户主作答记录(包含创建账套)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> CreateUserMasterAnswerRecord(UserCreateMasterAnswerDto dto, UserTicket user);

        /// <summary>
        /// 获取日历任务列表
        /// </summary>
        /// <param name="monthTaskId"></param>
        /// <param name="gradeId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<CalendarTaskViewModel>> GetCalendarTaskList(long monthTaskId, long gradeId, UserTicket user);

        /// <summary>
        /// 获取用户票据题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserBillTopicViewModel>>> GetUserBillTopicList(UserCalendarTopicQuery query, UserTicket user);

        /// <summary>
        /// 获取票据题退回理由列表
        /// </summary>
        /// <param name="billTopicId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<BillTopicSendBackReasonViewModel>>> GetSendBackReasonListByBillId(long billTopicId);

        /// <summary>
        /// 审核票据
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserCheckBillTopicViewModel>> CheckBillTopicOperate(UserCheckBillTopicDto dto, UserTicket user);

        /// <summary>
        /// 累加散户作答记录次数
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddedRetailAnswerNumber(RetailGradeCountDto dto, UserTicket user);

        /// <summary>
        /// 获取散户作答次数
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<int>> GetRetailAnswerNumber(long gradeId, UserTicket user);

        /// <summary>
        /// 获取突发机制
        /// </summary>
        /// <param name="emergencyType"></param>
        /// <returns></returns>
        Task<ResponseContext<EmergencyMechanismViewModel>> GetEmergencyMechanism(int emergencyType);

    }
}
