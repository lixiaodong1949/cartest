﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{

    /// <summary>
    /// 模板管理
    /// </summary>
    public interface ITemplateService
    {
        /// <summary>
        /// 获取模板树列表
        /// </summary>
        Task<PageResponse<TemplateTreeItemDto>> GetTemplateTree();

        /// <summary>
        /// 获取指定模板类型
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        Task<ResponseContext<TemplateTreeItemDto>> GetTemplateTypeById(long id);

        /// <summary>
        /// 添加/编辑模板类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateTemplateType(TemplateTreeItemDto model, long currentUserId);

        /// <summary>
        /// 删除模板类型
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTemplateType(long tId, long currentUserId);

        /// <summary>
        /// 获取模板列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TemplateDto>> GetTemplates(GetTemplatesRequest model);

        /// <summary>
        /// 获取指定模板
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        Task<ResponseContext<TemplateDto>> GetTemplateById(long id);

        /// <summary>
        /// 添加模板信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateTemplate(TemplateDto model, long currentUserId);

        /// <summary>
        /// 删除指定模板
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTemplate(long tId, long currentUserId);

        /// <summary>
        /// 获取模板树（下拉框）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<LayuiTreeDto>>> GetTemplateSelectTreeDatas();

    }
}
