﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IDeclarationChildService
    {
        /// <summary>
        /// 保存申报子表
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(DeclarationChildDto input,UserTicket currentUser);

        /// <summary>
        /// 根据id删除申报子表
        /// </summary>
        /// <param name="id">子表id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(long id, UserTicket currentUser);

        /// <summary>
        /// 根据id获取申报子表
        /// </summary>
        /// <param name="id">子表id</param>
        /// <returns></returns>
        Task<ResponseContext<DeclarationChildDto>> GetById(long id);

        /// <summary>
        /// 根据条件获取子表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DeclarationChildDto>>> GetList(GetDeclarationChildDto input);
    }
}
