﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 题目公共基础服务接口
    /// </summary>
    public interface ITopicBaseService
    {
        /// <summary>
        /// 用户标记/取消标记题目操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UserMarkOrUnMarkQuestion(UserMarkQuestionDto dto, UserTicket user);

        /// <summary>
        /// 获取公司下岗位列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserCasePositionViewModel>>> GetUserCasePositionList(long caseId, PositionType positionType);

        /// <summary>
        /// 获取公司客户列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserCaseCustomerViewModel>>> GetUserCaseCustomerList(long caseId);
    }
}
