using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YssxC.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto.ExamPaper;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 游戏关卡题目接口服务类
    /// </summary>
    public interface IGameTopicService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(GameTopicDto model, long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<GameTopicDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        // DELETE api/<GameTopicController>/5
        Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GameTopicPageResponseDto>> ListPage(GameTopicPageRequestDto query);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameTopicListResponseDto>> List(GameTopicListRequestDto query);

        /// <summary>
        /// 获取任务中可选的题目列表
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameChoosableTopicDto>> GetChoosableTopicList(long taskId);

        /// <summary>
        /// 用户获取关卡题目列表(不分页)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ListResponse<UserBillTopicViewModel>> GetGameUserBillTopicList(UserGameTopicListRequestDto query, long userId);
        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList(GameUserCalendarReceivingQuery query, UserTicket user);

        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetGamePaymentTopicInfoList(GameReceivePaymentTopicInfoQuery query, UserTicket user);

        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryGameUserHasDoneReceivingList(GameUserHasDoneReceivingQuery query, UserTicket user);

        /// <summary>
        /// 获取分录题列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAccountEntryTopicList(GameAccountEntryTopicRequest model);

        /// <summary>
        /// 获取分录题列表(APP专用)
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAccountEntryTopicList_APP(GameAccountEntryTopicRequest model);

        /// <summary>
        /// 审核分录列表出纳(会计主管) auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAuditAccountEntryTopicList(GameAuditAccountEntryTopicRequest model);

        /// <summary>
        /// 审核分录列表出纳(会计主管) auditType（0代表出纳  1代表会计主管）(APP专用)
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAuditAccountEntryTopicList_APP(GameAuditAccountEntryTopicRequest model);
        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="gradeId"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        Task<PageResponse<AccountingEntryAnsweredDto>> GetGameAccountingEntryAnsweredList(GameAccountingEntryTopicRequest model);

        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId">作答主记录</param>
        /// <param name="caseId"></param>
        /// <param name="taskId"></param>
        /// <param name="gameLevelId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamProfitLossTopicDto>> GetGameProfitLossTopicInfo(long gradeId, long caseId, long gameLevelId);

        /// <summary>
        /// 获取月度任务题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetGameOtherTopicList(GameOtherTopicRequest model);

        /// <summary>
        /// 申报表题目列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DeclarationParentDto>>> GetGameDeclarationList(GameDeclarationRequest input);

        /// <summary>
        /// 关卡提交
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GameLevelGradeResponseDto>> SubmitGameExam(SubmitGameExamRequestDto request, long userId);


        /// <summary>
        /// 开始闯关
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<UserGameLevelGrade>> GameLeveLStart(GameLeveStartRequestDto model, UserTicket user);

        /// <summary>
        /// 获取关卡闯关记录列表
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameUserGradeDto>> GetGameLeveLGradeList(long gameLeveLId, long userId);

        /// <summary>
        /// 获取关卡闯关记录列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GameUserGradeDetailsDto>> GetGameLeveLGradeTopicList(long gradeId);

        /// <summary>
        /// 创建账套
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> CreateSetBook(long gameGradeId);

        /// <summary>
        /// 获取用户关卡作答进度
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameLevelTopicNodeDto>> GetUserGameGradeProgress(long gameGradeId, long userId);

        /// <summary>
        /// 重置游戏
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ResetGame(long taskId, long userId);

        /// <summary>
        /// 关卡作答排行榜
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<CurrentGameLevelGradeNo1>>> GameLevelGradeTops(long gameLevelId, int top);

        /// <summary>
        /// 获取案例的任务信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetGameMonthTaskListDto>>> GetGameMonthTaskList(long id, long userId, int userType);

        /// <summary>
        /// 修改关卡作答使用时间
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> UdpateGameGradeUseTime(long gameGradeId, int usedSeconds);

        /// <summary>
        /// 获取题目附件列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<QuestionFile>>> GetTopicFiles(long topicId);
    }
}