﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto.Subject;

namespace YssxC.S.IServices.Subject
{
    public interface ISubjectService
    {
        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddSubject(SubjectDto model);

        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSubject(long id);

        /// <summary>
        /// 新增科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<string>> GetNextNum(long id);

        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDto>>> GetSubjectsList(long id);


        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDto>>> GetSubjectLastNodeList(long id, int type);

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        ///  <param name="normType"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDto>>> GetSubjectByType(long id, int type);


        Task<ResponseContext<List<SubjectDto>>> GetSubjectByAssistacId(long caseId, string name);

        /// <summary>
        /// 辅助核算科目
        /// </summary>
        /// <param name="id"></param>
        /// <param name="assistType"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssistacSubjectList(long id, int assistType);

        /// <summary>
        ///  获取科目下面的辅助核算科目
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssisSubjectById(long subjectId);

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddValues(SubjectValueDto model);

        /// <summary>
        /// 试算平衡
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<TrialBalancingDto>> TrialBalancing(long caseId);
    }
}
