﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface ITaskService
    {
        /// <summary>
        /// 保存任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveTask(TaskDto input, UserTicket currentUser);

        /// <summary>
        /// 根据id获取任务
        /// </summary>
        /// <param name="id">任务id</param>
        /// <returns></returns>
        Task<ResponseContext<TaskDto>> GetTask(long id);

        /// <summary>
        /// 根据条件查询任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponse<TaskDto>> GetTaskList(GetTaskInput input);

        /// <summary>
        /// 根据id删除任务
        /// </summary>
        /// <param name="id">任务id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTask(long id, UserTicket currentUser);

        /// <summary>
        /// 开启或关闭任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ToggleStatus(ToggleInput input, long currentUserId);

    }
}
