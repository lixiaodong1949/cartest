﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto.Subject;

namespace YssxC.S.IServices
{
    public interface ICertificateService
    {
        // <summary>
        /// 明细账查询
        /// </summary>
        /// <param name="subjectId">科目Id</param>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        ///  <param name="dateStr">会计期间</param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type, string dateStr,long taskId=0);


        /// <summary>
        /// 总账查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="subjectType">科目类型</param>
        /// <param name="keyword">查询关键字</param>
        ///  <param name="dateStr">会计期间</param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr, long taskId = 0);

        /// <summary>
        /// 余额查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="subjectType">科目类型</param>
        /// <param name="keyword">查询关键字</param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type, long taskId = 0);

        /// <summary>
        /// 余额查询
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceById(long id,int subjectType,long subjectId, long gradeId,string assisName);

        /// <summary>
        /// 辅助核算详情
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gradeId"></param>
        /// <param name="subjectId"></param>
        /// <param name="assisName"></param>
        /// <param name="isHaveCheck"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateDto>>> GetAssistacDetailDataList(long id, long gradeId, string dateStr, long subjectId,string assisName,int isHaveCheck);


        /// <summary>
        /// 辅助核算总账
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateBalanceDto>>> GetAssistacSummerDataList(long id, long gradeId, string dateStr, int assistType);

    }
}
