﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;

namespace OneAndX.IServices
{
    /// <summary>
    /// 考试管理接口(后台)
    /// </summary>
    public interface IExamManagerBackService
    {
        /// <summary>
        /// 获取某等级证书考场列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateExamRoomViewModel>>> GetCertificateExamRoomList(CertificateExamRoomQuery query);

        /// <summary>
        /// 分配考试计划考场
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddPlanExamRoom(PlanExamRoomDto dto);

        /// <summary>
        /// 获取已分配考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DistributeExamRoomViewModel>>> GetDistributeExamRoomList(long planId);

        /// <summary>
        /// 删除已分配考试计划考场或考点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteDistributeExamSiteOrRoom(DeleteDistributeExamSiteOrRoomDto dto);
    }
}
