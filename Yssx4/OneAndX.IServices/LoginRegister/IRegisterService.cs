﻿using OneAndX.Dto;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;

namespace OneAndX.IServices
{
    /// <summary>
    /// 注册接口
    /// </summary>
    public interface IRegisterService
    {
        /// <summary>
        /// 验证手机号码是否已注册
        /// </summary>
        /// <param name="mobilePhone">手机号码</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> VerifyPhoneNumber(string mobilePhone);

        /// <summary>
        /// 校验注册验证码
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> CheckVerificationCode(MobilePhoneVerificationCodeDto dto);
                
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<SxYssxUser>> RegisteredUsers(UserRegisterDto dto);
    }
}
