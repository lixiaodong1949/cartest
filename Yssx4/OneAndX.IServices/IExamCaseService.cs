﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    public interface IExamCaseService
    {
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Update(ExamCaseUpdateDto queryDto, long currentUserId);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(long id, long currentUserId);
        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<PageResponse<ExamCaseListPageResponseDto>> GetListPage(ExamCaseQueryRequestDto queryDto);
        /// <summary>
        /// 查询案例的题目列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetCaseTopics(long caseId, CaseSourceSystem sourceSystem);
    }  
}
