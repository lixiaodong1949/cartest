﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 历届考试
    /// </summary>
    public interface IPreviouExamService
    {
        #region 历届考试
        /// <summary>
        /// 获取历届考试列表
        /// </summary>
        Task<PageResponse<SavePreviouExamListDto>> GetPreviouExamList(GetPreviouExamListRequest model);

        /// <summary>
        /// 保存历届考试
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SavePreviouExam(SavePreviouExamListDto model);

        /// <summary>
        /// 删除历届考试
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePreviouExam(long id);

        #endregion


        #region 花絮

        /// <summary>
        /// 获取历届考试 - 花絮列表
        /// </summary>
        Task<PageResponse<GetPreviouExamTidbitDto>> GetPreviouExamTidbitList(long examId);

        /// <summary>
        /// 保存历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SavePreviouExamTidbit(SavePreviouExamTidbitDto model);

        /// <summary>
        /// 删除历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePreviouExamTidbit(long id, long examId);

        #endregion


        #region 培训资料

        /// <summary>
        /// 获取历届考试 - 培训资料列表
        /// </summary>
        Task<PageResponse<GetPreviouExamFileDto>> GetPreviouExamFileList(long examId);

        /// <summary>
        /// 保存历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SavePreviouExamFile(SavePreviouExamFileDto model);

        /// <summary>
        /// 删除历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePreviouExamFile(long id, long examId);

        #endregion


        #region 报名用户和证书

        /// <summary>
        /// 获取历届考试 - 报名用户列表
        /// </summary>
        Task<PageResponse<GetPreviouExamUserListDto>> GetPreviouExamUserList(GetPreviouExamUserListRequest model);

        /// <summary>
        /// 获取历届考试 - 证书列表
        /// </summary>
        Task<PageResponse<GetPreviouExamCertificateListDto>> GetPreviouExamCertificateList(GetPreviouExamUserListRequest model);

        #endregion


    }
}
