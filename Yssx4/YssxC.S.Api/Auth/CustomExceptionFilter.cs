﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net;

namespace YssxC.S.Api.Auth
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomExceptionFilter : Attribute, IExceptionFilter
    {
        private ILogger _logger = null;
        private IConfiguration _cfg = null;
        private IHostingEnvironment _env = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="cfg"></param>
        /// <param name="env"></param>
        public CustomExceptionFilter(ILogger<CustomExceptionFilter> logger, IConfiguration cfg, IHostingEnvironment env)
        {
            _logger = logger;
            _cfg = cfg;
            _env = env;
        }
        /// <summary>
        /// 111
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            //在这里记录错误日志，context.Exception 为异常对象
            //context.Result = ApiResult.Failed.SetMessage(context.Exception.Message); //返回给调用方
            var innerLog = context.Exception.InnerException != null ? $" \r\n{context.Exception.InnerException.Message} \r\n{ context.Exception.InnerException.StackTrace}" : "";
            _logger.LogError($"=============错误：{context.Exception.Message} \r\n{context.Exception.StackTrace}{innerLog}");

            context.ExceptionHandled = true;
            var ex = context.Exception;
            var msg = $"在执行 action[{context.ActionDescriptor.DisplayName}] 时捕捉到未处理的异常：{ex.GetType()}\nFilter已进行错误处理。异常信息：{ex}";

            context.Result = new ContentResult()
            {
                Content = msg,
                ContentType = "application/json",
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
        }
    }
}