﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices.Subject;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 科目
    /// </summary>
    public class SubjectController : BaseController
    {
        ISubjectService service;
        public SubjectController(ISubjectService s1)
        {
            service = s1;
        }


        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectsList(long id)
        {
            return await service.GetSubjectsList(id);
        }


        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="subjectType">科目类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectLastNodeList(long id, int subjectType)
        {
            return await service.GetSubjectLastNodeList(id, subjectType);
        }

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// <returns></returns>
        [HttpGet]
        //  [AllowAnonymous]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByType(long id, int type)
        {
            return await service.GetSubjectByType(id, type);
        }

        /// <summary>
        /// 辅助核算科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="assistType">账簿-辅助核算类型</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssistacSubjectList(long id, int assistType)
        {
            return await service.GetAssistacSubjectList(id, assistType);
        }

        /// <summary>
        /// 获取科目下面的辅助科目
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssisSubjectById(long subjectId)
        {
            return await service.GetAssisSubjectById(subjectId);
        }

        /// <summary>
        /// 辅助核算中所属科目信息
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <param name="name"> 账簿-辅助核算名称</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByAssistacId(long caseId, string name)
        {
            return await service.GetSubjectByAssistacId(caseId, name);
        }
    }
}
