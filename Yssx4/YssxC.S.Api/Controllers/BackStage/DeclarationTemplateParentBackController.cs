﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 申报表主表模板
    /// </summary>
    public class DeclarationTemplateParentBackController : BaseController
    {
        private readonly IDeclarationTemplateParentService _declarationTypeTemplateService;

        public DeclarationTemplateParentBackController(IDeclarationTemplateParentService declarationTypeTemplateService)
        {
            _declarationTypeTemplateService = declarationTypeTemplateService;
        }

        /// <summary>
        /// 获取所有申报表主表模板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DeclarationTemplateParentDto>>> GetAll()
        {
            return await _declarationTypeTemplateService.GetAll();
        }
    }
}