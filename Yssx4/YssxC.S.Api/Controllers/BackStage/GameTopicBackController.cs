using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 游戏关卡题目（后台）
    /// </summary>
    public class GameTopicBackController : BaseController
    {
        private IGameTopicService _service;

        public GameTopicBackController(IGameTopicService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(GameTopicDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameTopicDto>> Get(long id)
        {
            var response = new ResponseContext<GameTopicDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(params long[] id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id == null || id.Length <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<PageResponse<GameTopicPageResponseDto>> ListPage(GameTopicPageRequestDto query)
        {
            var response = new PageResponse<GameTopicPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ListResponse<GameTopicListResponseDto>> List(GameTopicListRequestDto query)
        {
            var response = new ListResponse<GameTopicListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }

        /// <summary>
        /// 获取任务中可选的题目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameChoosableTopicDto>> GetChoosableTopicList(long taskId)
        {
            var response = await _service.GetChoosableTopicList(taskId);

            return response;
        }
    }
}