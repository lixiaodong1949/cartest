﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 任务管理
    /// </summary>
    public class TaskBackController : BaseController
    {
        private ITaskService _taskService;
        IOnboardingService _onboardingService;

        public TaskBackController(ITaskService taskService, IOnboardingService onboardingService)
        {
            _taskService = taskService;
            _onboardingService = onboardingService;
        }

        #region 任务
        /// <summary>
        /// 保存任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveTask(TaskDto input)
        {
            return await _taskService.SaveTask(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTask(long id)
        {
            return await _taskService.DeleteTask(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取人
        /// </summary>
        /// <param name="id">任务id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskDto>> GetTask(long id)
        {
            return await _taskService.GetTask(id);
        }

        /// <summary>
        /// 根据条件查询任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TaskDto>> GetTaskList([FromQuery]GetTaskInput input)
        {
            return await _taskService.GetTaskList(input);
        }

        /// <summary>
        /// 启用或禁用任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ToggleStatus(ToggleInput input)
        {
            return await _taskService.ToggleStatus(input, CurrentUser.Id);
        }
        #endregion


        #region 生成试卷
        /// <summary>
        /// 根据任务ID生成试卷
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> GenerateTestPaper(long id)
        {
            return await _onboardingService.GenerateTestPaper(id, CurrentUser.Id);
        }

        /// <summary>
        /// 购买后自动生成试卷 - 云实习
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model)
        {
            return await _onboardingService.AutoGenerateTestPaper(model);
        }

        /// <summary>
        /// 根据任务ID生成试卷 - 云实习配置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> GenerateTestPaperById(AutoGenerateTestPaperDto model)
        {
            return await _onboardingService.AutoGenerateTestPaper(model, CurrentUser.Id);
        }
        #endregion


    }
}