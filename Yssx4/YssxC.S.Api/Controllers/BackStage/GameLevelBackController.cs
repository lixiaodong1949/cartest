using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 游戏关卡(后台)
    /// </summary>
    public class GameLevelBackController : BaseController
    {
        private IGameLevelService _service;

        public GameLevelBackController(IGameLevelService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(GameLevelDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            //model.UseTime =;
            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameLevelDto>> Get(long id)
        {
            var response = new ResponseContext<GameLevelDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(id, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<PageResponse<GameLevelPageResponseDto>> ListPage(GameLevelPageRequestDto query)
        {
            var response = new PageResponse<GameLevelPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ListResponse<GameLevelListResponseDto>> List(GameLevelListRequestDto query)
        {
            var response = new ListResponse<GameLevelListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }
    }
}