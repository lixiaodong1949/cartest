﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 标签
    /// </summary>
    public class TagBackController : BaseController
    {
        private ITagService _tagService;

        public TagBackController(ITagService tagService)
        {
            _tagService = tagService;
        }

        /// <summary>
        /// 保存标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveTag(TagDto input)
        {
            return await _tagService.SaveTag(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除标签
        /// </summary>
        /// <param name="id">标签id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTag(long id)
        {
            return await _tagService.DeleteTag(id, CurrentUser);
        }

        /// <summary>
        /// 启用或禁用标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input)
        {
            return await _tagService.Toggle(input, CurrentUser);
        }

        /// <summary>
        /// 根据条件查询标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TagDto>> GetTagList([FromQuery]GetTagInput input)
        {
            return await _tagService.GetTagList(input);
        }
    }
}