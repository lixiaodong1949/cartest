using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 游戏场景（后台）
    /// </summary>
    public class GameSceneBackController : BaseController
    {
        private IGameSceneService _service;

        public GameSceneBackController(IGameSceneService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(GameSceneDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameSceneDto>> Get(long id)
        {
            var response = new ResponseContext<GameSceneDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<PageResponse<GameScenePageResponseDto>> ListPage(GameScenePageRequestDto query)
        {
            var response = new PageResponse<GameScenePageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ListResponse<GameSceneListResponseDto>> List(GameSceneListRequestDto query)
        {
            var response = new ListResponse<GameSceneListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }

        /// <summary>
        /// 添加场景会话信息
        /// </summary>
        /// <returns></returns>
        /// <param name="id">场景id</param>
        /// <param name="dialogues">会话信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddGameSceneDialogues(GameSceneDialoguesDto query)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.AddGameSceneDialogues(query);

            return response;
        }
    }
}