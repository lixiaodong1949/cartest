﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 规章制度模板
    /// </summary>
    public class RegulationBackController : BaseController
    {
        IRegulationService _service;
        public RegulationBackController(IRegulationService regulationService)
        {
            _service = regulationService;
        }

        /// <summary>
        /// 删除规章制度信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteById(long id)
        {
            ResponseContext<bool> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<bool>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.DeleteById(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据Id获取单条规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<RegulationDto>> GetInfoById(long id)
        {
            ResponseContext<RegulationDto> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<RegulationDto>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.GetInfoById(id);

            return response;
        }
        /// <summary>
        /// 保存规章制度 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(RegulationDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (string.IsNullOrWhiteSpace(model.Name))
            {
                result.Msg = "Name 为无效数据。";
                return result;
            }

            result = await _service.Save(model, CurrentUser);

            return result;
        }

        /// <summary>
        /// 复制规章制度
        /// </summary>
        /// <param name="id">规章制度唯一id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Copy(long id)
        {
            if (id <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, "Id不正确！");

            var response = await _service.Copy(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据ParentId获取子类规章制度
        /// </summary>
        /// <param name="parentId">父类Id</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<RegulationListDto>>> GetListByParentId(long parentId)
        {
            ResponseContext<List<RegulationListDto>> response = null;
            if (parentId <= 0)
            {
                response = new ResponseContext<List<RegulationListDto>>(CommonConstants.BadRequest, "parentId不正确！");
                return response;
            }

            response = await _service.GetListByParentId(parentId);

            return response;
        }

        /// <summary>
        /// 获取父级规章制度列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<RegulationListDto>>> GetList(RegulationQueryDto query)
        {
            var response = await _service.GetList(query);

            return response;
        }

        /// <summary>
        /// 根据规章制度id获取应用改规章制度的案例列表，分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCaseListByRegulationIdResponse>> GetCaseListPageByRegulationId(GetCaseListByRegulationIdDto query)
        {
            var response = new PageResponse<GetCaseListByRegulationIdResponse> { Code = CommonConstants.BadRequest, Msg = "" };
            if (query == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }

            response = await _service.GetCaseListPageByRegulationId(query);

            return response;
        }

        /// <summary>
        /// 制度模板应用到案例上
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationDto model)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }
            response = await _service.SaveCaseUseRegulation(model, CurrentUser);

            return response;
        }

        /// <summary>
        /// 获取规章制度模板名称列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<RegulationNamesListDto>>> GetNamesList(RegulationNamesListQueryDto query)
        {
            var response = new ResponseContext<List<RegulationNamesListDto>> { Code = CommonConstants.BadRequest, Msg = "" };
            if (query == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }

            response = await _service.GetNamesList(query);

            return response;
        }
    }
}