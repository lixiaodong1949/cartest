using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Microsoft.AspNetCore.Authorization;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 3D游戏关卡(前台)
    /// </summary>
    public class GameLevelController : BaseController
    {
        private IGameLevelService _service;

        public GameLevelController(IGameLevelService service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据Id获取3D游戏关卡信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameLevelResponseDto>> GetGameLevel(long id)
        {
            var response = new ResponseContext<GameLevelResponseDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.GetGameLevel(id, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取3D游戏关卡列表(不分页)
        /// </summary>
        /// <param name="taskId">任务id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameLevelsResponseDto>> GetGameLevelList(long taskId)
        {
            var response = new ListResponse<GameLevelsResponseDto> { Code = CommonConstants.BadRequest };
            if (taskId <= 0)
            {
                response.Msg = "参数无效";
                return response;
            }

            response = await _service.GetGameLevelList(taskId, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 培训完成
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> TrainFinish(long taskId)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (taskId <=0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.TrainFinish(taskId, CurrentUser.Id);

            return response;
        }
    }
}