﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 日历票据服务
    /// </summary>
    public class CalendarBillController : BaseController
    {
        private readonly ICalendarBillService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public CalendarBillController(ICalendarBillService service)
        {
            _service = service;
        }

        #region 生成用户主作答记录(包含创建账套)
        /// <summary>
        /// 生成用户主作答记录(包含创建账套)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> CreateUserMasterAnswerRecord(UserCreateMasterAnswerDto dto)
        {
            return await _service.CreateUserMasterAnswerRecord(dto, this.CurrentUser);
        }
        #endregion

        #region 获取日历任务列表
        /// <summary>
        /// 获取日历任务列表
        /// </summary>
        /// <param name="monthTaskId">月度任务Id</param>
        /// <param name="gradeId">主作答记录Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CalendarTaskViewModel>> GetCalendarTaskList(long monthTaskId, long gradeId)
        {
            return await _service.GetCalendarTaskList(monthTaskId, gradeId, this.CurrentUser);
        }
        #endregion

        #region 获取用户票据题列表
        /// <summary>
        /// 获取用户票据题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UserBillTopicViewModel>>> GetUserBillTopicList([FromQuery]UserCalendarTopicQuery query)
        {
            return await _service.GetUserBillTopicList(query, this.CurrentUser);
        }
        #endregion

        #region 获取票据题退回理由列表
        /// <summary>
        /// 获取票据题退回理由列表
        /// </summary>
        /// <param name="topicId">票据题Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<BillTopicSendBackReasonViewModel>>> GetSendBackReasonListByBillId(long topicId)
        {
            return await _service.GetSendBackReasonListByBillId(topicId);
        }
        #endregion

        #region 审核票据
        /// <summary>
        /// 审核票据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<UserCheckBillTopicViewModel>> CheckBillTopicOperate(UserCheckBillTopicDto dto)
        {
            return await _service.CheckBillTopicOperate(dto, this.CurrentUser);
        }
        #endregion

        #region 累加散户作答记录次数
        /// <summary>
        /// 累加散户作答记录次数
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddedRetailAnswerNumber(RetailGradeCountDto dto)
        {
            return await _service.AddedRetailAnswerNumber(dto, this.CurrentUser);
        }
        #endregion

        #region 获取散户作答次数
        /// <summary>
        /// 获取散户作答次数
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<int>> GetRetailAnswerNumber(long gradeId)
        {
            return await _service.GetRetailAnswerNumber(gradeId, this.CurrentUser);
        }
        #endregion

        #region 获取突发机制
        /// <summary>
        /// 获取突发机制
        /// </summary>
        /// <param name="emergencyType">1:正确票据退回 2:错误票据退回,原因错误 3:错误票据通过 4:付款错误 5:无提示无答案</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<EmergencyMechanismViewModel>> GetEmergencyMechanism(int emergencyType)
        {
            return await _service.GetEmergencyMechanism(emergencyType);
        }
        #endregion

    }
}