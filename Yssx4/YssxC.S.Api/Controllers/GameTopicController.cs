using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto.ExamPaper;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 3D游戏关卡题目(前台)
    /// </summary>
    public class GameTopicController : BaseController
    {
        private IGameTopicService _service;

        public GameTopicController(IGameTopicService service)
        {
            _service = service;
        }

        /// <summary>
        /// 开始闯关
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserGameLevelGrade>> GameLeveLStart([FromQuery] GameLeveStartRequestDto model)
        {
            var response = new ResponseContext<UserGameLevelGrade> { Code = CommonConstants.BadRequest };
            if (CurrentUser.Id == CommonConstants.DefaultUserId)
            {
                response.SetError("未登录", null, CommonConstants.Unauthorized);
                return response;
            }
            response = await _service.GameLeveLStart(model, CurrentUser);

            return response;
        }

        /// <summary>
        /// 获取用户关卡作答进度
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameLevelTopicNodeDto>> GetUserGameGradeProgress(long gameGradeId)
        {
            var response = new ListResponse<GameLevelTopicNodeDto> { Code = CommonConstants.BadRequest };
            if (gameGradeId <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetUserGameGradeProgress(gameGradeId, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取用户票据题列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<UserBillTopicViewModel>> GetGameUserBillTopicList([FromQuery] UserGameTopicListRequestDto query)
        {
            var response = new ListResponse<UserBillTopicViewModel> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetGameUserBillTopicList(query, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList([FromQuery] GameUserCalendarReceivingQuery query)
        {
            return await _service.GetUserReceivingBillTopicList(query, this.CurrentUser);
        }

        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetGamePaymentTopicInfoList([FromQuery] GameReceivePaymentTopicInfoQuery query)
        {
            return await _service.GetGamePaymentTopicInfoList(query, this.CurrentUser);
        }

        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryGameUserHasDoneReceivingList([FromQuery] GameUserHasDoneReceivingQuery query)
        {
            return await _service.QueryGameUserHasDoneReceivingList(query, this.CurrentUser);
        }

        /// <summary>
        /// 获取分录题列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAccountEntryTopicList([FromQuery] GameAccountEntryTopicRequest model)
        {
            return await _service.GetGameAccountEntryTopicList(model);
        }
        /// <summary>
        /// 获取分录题列表(APP专用)
        /// </summary>
        /// <returns></returns>   
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAccountEntryTopicList_APP([FromQuery] GameAccountEntryTopicRequest model)
        {
            return await _service.GetGameAccountEntryTopicList_APP(model);
        }

        /// <summary>
        /// 审核分录列表 auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetGameAuditAccountEntryTopicList([FromQuery] GameAuditAccountEntryTopicRequest model)
        {
            return await _service.GetGameAuditAccountEntryTopicList(model);
        }
        /// <summary>
        /// 审核分录列表出纳(会计主管) auditType（0代表出纳  1代表会计主管）(APP专用)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicAPPDto>>> GetGameAuditAccountEntryTopicList_APP([FromQuery] GameAuditAccountEntryTopicRequest model)
        {
            return await _service.GetGameAuditAccountEntryTopicList_APP(model);
        }
        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="auditType">-1查全部 0查未审 1查已审</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<AccountingEntryAnsweredDto>> GetGameAccountingEntryAnsweredList(GameAccountingEntryTopicRequest model)
        {
            return await _service.GetGameAccountingEntryAnsweredList(model);
        }

        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId">作答主记录</param>
        /// <param name="caseId">案例Id</param>
        /// <param name="gameLevelId">关卡Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamProfitLossTopicDto>> GetGameProfitLossTopicInfo(long gradeId, long caseId, long gameLevelId)
        {
            return await _service.GetGameProfitLossTopicInfo(gradeId, caseId, gameLevelId);
        }

        /// <summary>
        /// 获取关卡其他题型的题目列表(questionType：7--财务分析题 8 结账题  10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetGameOtherTopicList([FromQuery] GameOtherTopicRequest model)
        {
            return await _service.GetGameOtherTopicList(model);
        }

        /// <summary>
        /// 申报表题目列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DeclarationParentDto>>> GetGameDeclarationList([FromQuery] GameDeclarationRequest input)
        {
            return await _service.GetGameDeclarationList(input);
        }


        /// <summary>
        /// 关卡提交
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameLevelGradeResponseDto>> SubmitGameExam([FromQuery] SubmitGameExamRequestDto request)
        {
            return await _service.SubmitGameExam(request, CurrentUser.Id);
        }

        /// <summary>
        /// 获取关卡闯关记录列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameUserGradeDto>> GetGameLeveLGradeList(long gameLeveLId)
        {
            var response = new ListResponse<GameUserGradeDto> { Code = CommonConstants.BadRequest };
            if (gameLeveLId == 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetGameLeveLGradeList(gameLeveLId, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取关卡作答题目列表
        /// </summary>
        /// <param name="gradeId">作答主记录Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameUserGradeDetailsDto>> GetGameLeveLGradeTopicList(long gradeId)
        {
            var response = new ResponseContext<GameUserGradeDetailsDto> { Code = CommonConstants.BadRequest };
            if (gradeId <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetGameLeveLGradeTopicList(gradeId);

            return response;
        }

        /// <summary>
        /// 创建账套
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> CreateSetBook(long gameGradeId)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (gameGradeId > 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.CreateSetBook(gameGradeId);

            return response;
        }

        /// <summary>
        /// 重置游戏
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ResetGame(long taskId)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (taskId == 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ResetGame(taskId, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 关卡作答排行榜
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CurrentGameLevelGradeNo1>>> GameLevelGradeTops(long gameLevelId, int top = 3)
        {
            var response = new ResponseContext<List<CurrentGameLevelGradeNo1>> { Code = CommonConstants.BadRequest };
            if (gameLevelId <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GameLevelGradeTops(gameLevelId, top);

            return response;
        }

        /// <summary>
        /// 获取案例的任务信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetGameMonthTaskListDto>>> GetGameMonthTaskList(long id)
        {
            var response = new ResponseContext<List<GetGameMonthTaskListDto>> { Code = CommonConstants.BadRequest };
            if (id <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetGameMonthTaskList(id, CurrentUser.Id, CurrentUser.Type);

            return response;
        }
        /// <summary>
        /// 修改关卡作答使用时间
        /// </summary>
        /// <param name="gameGradeId">关卡作答记录Id</param>
        /// <param name="usedSeconds">作答使用时间,单位：秒</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UdpateGameGradeUseTime(long gameGradeId, int usedSeconds)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (gameGradeId <= 0)
            {
                response.Msg = "关卡作答记录无效";
                return response;
            }
            if (usedSeconds <= 0)
            {
                response.Msg = "作答使用时间无效";
                return response;
            }
            response = await _service.UdpateGameGradeUseTime(gameGradeId, usedSeconds);

            return response;
        }

        /// <summary>
        /// 根据题目Id获取题目附件列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<QuestionFile>>> GetTopicFiles(long questionId)
        {
            var response = new ResponseContext<List<QuestionFile>> { Code = CommonConstants.BadRequest };
            if (questionId <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetTopicFiles(questionId);

            return response;
        }
    }
}