﻿using ECommon.Components;
using ENode.Commanding;
using ENode.EQueue;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Yssx.CourseProcessorService
{
    [Component]
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        public override string GetTopic(ICommand command)
        {
             return ConfigurationManager.AppSettings["commandTopic"];
        }
    }
}
